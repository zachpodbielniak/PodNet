/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CLog.h"

typedef LONG                            HFILE, *LPHFILE, **DLPHFILE;

typedef struct _LOG_OBJECT
{
        INHERITS_FROM_HANDLE();
        LPHANDLE                        lphFiles;
        ULONG                           ulNumberOfFiles;
        LPPRIORITY_LOG_DESCRIPTOR       lppldDescriptor;
} LOG_OBJECT, *LPLOG_OBJECT, **DLPLOG_OBJECT;




_Success_(return != FALSE, _Acquires_Shared_Lock_(hHandle->hCritSec, _Lock_Kind_Critical_Section_(hHandle)))
INTERNAL_OPERATION
BOOL
__DestroyLog(
        _In_                                    HDERIVATIVE                     hLog,
        _In_Opt_                                ULONG                           ulFlags
){
        UNREFERENCED_PARAMETER(ulFlags);
        EXIT_IF_UNLIKELY_NULL(hLog, FALSE);
        LPLOG_OBJECT lploLog;
        ULONG ulIndex;
        BOOL bRet;
        lploLog = (LPLOG_OBJECT)hLog;
        EXIT_IF_UNLIKELY_NULL(lploLog, FALSE);

        bRet = TRUE;
        for (ulIndex = 0; ulIndex < lploLog->ulNumberOfFiles; ulIndex++)
                if (CloseFile(lploLog->lphFiles[ulIndex]) != 0) bRet = FALSE;
        
        if (lploLog->lppldDescriptor != NULLPTR)
        {
                for (ulIndex = 0; ulIndex < lploLog->lppldDescriptor->ulNumDebugFiles; ulIndex++)
                        if (CloseFile(lploLog->lppldDescriptor->lphDebug[ulIndex]) != 0) bRet = FALSE;

                for (ulIndex = 0; ulIndex < lploLog->lppldDescriptor->ulNumErrorFiles; ulIndex++)
                        if (CloseFile(lploLog->lppldDescriptor->lphError[ulIndex]) != 0) bRet = FALSE;

                for (ulIndex = 0; ulIndex < lploLog->lppldDescriptor->ulNumFatalFiles; ulIndex++)
                        if (CloseFile(lploLog->lppldDescriptor->lphFatal[ulIndex]) != 0) bRet = FALSE;
                
                for (ulIndex = 0; ulIndex < lploLog->lppldDescriptor->ulNumInfoFiles; ulIndex++)
                        if (CloseFile(lploLog->lppldDescriptor->lphInfo[ulIndex]) != 0) bRet = FALSE;

                for (ulIndex = 0; ulIndex < lploLog->lppldDescriptor->ulNumWarningFiles; ulIndex++)
                        if (CloseFile(lploLog->lppldDescriptor->lphWarning[ulIndex]) != 0) bRet = FALSE;
 
                
                FreeMemory(lploLog->lppldDescriptor->lphDebug);
                FreeMemory(lploLog->lppldDescriptor->lphError);
                FreeMemory(lploLog->lppldDescriptor->lphFatal);
                FreeMemory(lploLog->lppldDescriptor->lphInfo);
                FreeMemory(lploLog->lppldDescriptor->lphWarning);
                FreeMemory(lploLog->lppldDescriptor);
        }
        FreeMemory(lploLog->lphFiles);
        return bRet;
}





_Result_Null_On_Failure_
PODNET_API
HANDLE
CreateLog(
        _In_Z_                  LPCSTR                  lpcszFileName,
        _In_Opt_                ULONG                   ulFlags
){
        UNREFERENCED_PARAMETER(ulFlags);
        EXIT_IF_UNLIKELY_NULL(lpcszFileName, 0);
        HANDLE hHandle;
        LPLOG_OBJECT lploLog;
        ULONGLONG ullID;

        lploLog = LocalAllocAndZero(sizeof(LOG_OBJECT));
	/* cppcheck-suppress memleak */
        EXIT_IF_UNLIKELY_NULL(lploLog, 0);

        lploLog->ulNumberOfFiles = 1;
        lploLog->lphFiles = LocalAlloc(sizeof(HANDLE));
	if (NULLPTR == lploLog->lphFiles)
	{
		FreeMemory(lploLog);
		return NULL_OBJECT;
	}

        lploLog->lphFiles[0] = OpenFile(lpcszFileName, FILE_PERMISSION_WRITE | FILE_PERMISSION_READ | FILE_PERMISSION_CREATE, NULL);
	if (NULL_OBJECT == lploLog->lphFiles[0])
	{
		FreeMemory(lploLog->lphFiles);
		FreeMemory(lploLog);
		SetLastGlobalError(ERROR_LOG_COULD_NOT_OPEN_FILE);
		return NULL_OBJECT;
	}

        hHandle = CreateHandleWithSingleInheritor(
                        lploLog,
                        &(lploLog->hThis),
                        HANDLE_TYPE_LOG,
                        __DestroyLog,
                        NULL,
                        NULLPTR,
                        NULL,
                        NULLPTR,
                        NULL,
                        &ullID,
                        NULL
        );

	if (NULLPTR == hHandle)
	{
		DestroyObject(lploLog->lphFiles[0]);
		FreeMemory(lploLog->lphFiles);
		FreeMemory(lploLog);
		return NULL_OBJECT;
	}
        return hHandle;
}






_Result_Null_On_Failure_
PODNET_API
HANDLE
CreateLogWithMultipleFiles(
        _In_Reads_To_Ptr_Z_(*dlpcszFileNames)   DLPCSTR                         dlpcszFileNames,
        _In_                                    ULONG                           ulNumberOfFiles,
        _In_Opt_                                ULONG                           ulFlags
){
        UNREFERENCED_PARAMETER(ulFlags);
        EXIT_IF_UNLIKELY_NULL(dlpcszFileNames, 0);
        HANDLE hHandle;
        LPLOG_OBJECT lploLog;
        ULONGLONG ullID;
        ULONG ulIndex;

        lploLog = LocalAllocAndZero(sizeof(LOG_OBJECT));
	/* cppcheck-suppress memleak */
        EXIT_IF_UNLIKELY_NULL(lploLog, 0);

        lploLog->ulNumberOfFiles = ulNumberOfFiles;
        lploLog->lphFiles = LocalAlloc(sizeof(HANDLE) * ulNumberOfFiles);

	if (NULLPTR == lploLog->lphFiles)
	{
		FreeMemory(lploLog);
		return NULL_OBJECT;
	}

        for (ulIndex = 0; ulIndex < ulNumberOfFiles; ulIndex++)        
        {
                lploLog->lphFiles[ulIndex] = OpenFile(dlpcszFileNames[ulIndex], FILE_PERMISSION_READ | FILE_PERMISSION_WRITE, NULL);

		if (NULL_OBJECT == lploLog->lphFiles[ulIndex])
		{
			SetLastGlobalError(ERROR_LOG_COULD_NOT_OPEN_FILE);
			
			for (ulIndex = 0; ulIndex < ulNumberOfFiles; ulIndex++)
			{
				if (NULL_OBJECT != lploLog->lphFiles[ulIndex])
				{ DestroyObject(lploLog->lphFiles[ulIndex]); }
			}

			FreeMemory(lploLog->lphFiles);
			FreeMemory(lploLog);
			return NULL_OBJECT;
		}
        } 

        hHandle = CreateHandleWithSingleInheritor(
                        lploLog,
                        &(lploLog->hThis),
                        HANDLE_TYPE_LOG,
                        __DestroyLog,
                        NULL,
                        NULLPTR,
                        NULL,
                        NULLPTR,
                        NULL,
                        &ullID,
                        NULL
        );

        EXIT_IF_UNLIKELY_NULL(hHandle, 0);
        return hHandle;
}



_Result_Null_On_Failure_
PODNET_API
HANDLE
CreateLogWithMultipleFilesAndDefinableProc(
        _In_Reads_To_Ptr_Z_(*dlpcszFileNames)   DLPCSTR                         dlpcszFileNames,
        _In_                                    ULONG                           ulNumberOfFiles,
        _In_                                    LPFN_HANDLE_CUSTOM_PROC         lpfnCustomProc,
        _In_Opt_                                ULONG                           ulFlags
){
        UNREFERENCED_PARAMETER(lpfnCustomProc);
        UNREFERENCED_PARAMETER(ulFlags);
        EXIT_IF_UNLIKELY_NULL(dlpcszFileNames, 0);
        EXIT_IF_UNLIKELY_NULL(ulNumberOfFiles, 0);
        HANDLE hHandle;
        LPLOG_OBJECT lploLog;
        ULONGLONG ullID;
        ULONG ulIndex;

        lploLog = LocalAllocAndZero(sizeof(LOG_OBJECT));
	/* cppcheck-suppress memleak */
        EXIT_IF_UNLIKELY_NULL(lploLog, 0);
 
        lploLog->ulNumberOfFiles = ulNumberOfFiles;
        lploLog->lphFiles = LocalAllocAndZero(sizeof(HFILE) * ulNumberOfFiles);
	if (NULLPTR == lploLog->lphFiles)
	{
		FreeMemory(lploLog);
		return NULL_OBJECT;
	}
        
        for (ulIndex = 0; ulIndex < ulNumberOfFiles; ulIndex++)        
        {
                lploLog->lphFiles[ulIndex] = OpenFile(dlpcszFileNames[ulIndex], FILE_PERMISSION_READ | FILE_PERMISSION_WRITE, NULL);
		if (NULL_OBJECT == lploLog->lphFiles[ulIndex])
		{
			SetLastGlobalError(ERROR_LOG_COULD_NOT_OPEN_FILE);
			
			for (ulIndex = 0; ulIndex < ulNumberOfFiles; ulIndex++)
			{
				if (NULL_OBJECT != lploLog->lphFiles[ulIndex])
				{ DestroyObject(lploLog->lphFiles[ulIndex]); }
			}

			FreeMemory(lploLog->lphFiles);
			FreeMemory(lploLog);
			return NULL_OBJECT;
		}
        } 
        

        hHandle = CreateHandleWithSingleInheritor(
                        lploLog,
                        &(lploLog->hThis),
                        HANDLE_TYPE_LOG,
                        __DestroyLog,
                        NULL,
                        NULLPTR,
                        NULL,
                        NULLPTR,
                        NULL,
                        &ullID,
                        NULL
        );

	if (NULL_OBJECT == hHandle)
	{
		for (ulIndex = 0; ulIndex < ulNumberOfFiles; ulIndex++)
		{
			if (NULL_OBJECT != lploLog->lphFiles[ulIndex])
			{ DestroyObject(lploLog->lphFiles[ulIndex]); }
		}

		FreeMemory(lploLog->lphFiles);
		FreeMemory(lploLog);
		return NULL_OBJECT;
	}

        return hHandle;
}



_Success_(return != FALSE, _Acquires_Shared_Lock_(hLog->hCritSec, _Lock_Kind_Critical_Section_(hLog)))
BOOL
WriteLog(
        _In_                                    HANDLE                          hLog,
        _In_Z_                                  LPCSTR                          lpcszStringToLog,
        _In_Opt_                                LOG_PRIORITY                    lgprPriority,
        _In_Opt_                                ULONG                           ulFlags
){
        EXIT_IF_UNLIKELY_NULL(hLog, 0);
        EXIT_IF_UNLIKELY_NULL(lpcszStringToLog, 0);
        EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hLog), HANDLE_TYPE_LOG, FALSE);
        LPLOG_OBJECT lploLog;
        BOOL bRet;
        ULONG ulIndex;
        CSTRING lpszBuffer[2048];
	CSTRING lpszTime[0x50];
	ULONG ulWriteFlags;
	time_t ttRawTime;
	struct tm *tmUtc;
	struct tm tmState;

        lploLog = (LPLOG_OBJECT)GetHandleDerivative(hLog);
        ZeroMemory(lpszBuffer, sizeof(CHAR) * 2048);
        bRet = TRUE;

	ulWriteFlags = 0;
	if (LOG_WRITE_FLUSH & ulFlags)
	{ ulWriteFlags = FILE_WRITE_FLUSH; }

        if (StringLength(lpcszStringToLog) > 2047) return FALSE;

        if (lgprPriority == 0)
	{
		time(&ttRawTime);
		tmUtc = gmtime_r(&ttRawTime, &tmState);
		ZeroMemory(lpszTime, sizeof(lpszTime)); 

		strftime(
			lpszTime,
			sizeof(lpszTime) - 1,
			"%Y-%m-%d %T",
			tmUtc
		);
		
		StringPrintFormatSafe(
			lpszBuffer, 
			sizeof(lpszBuffer) - 1,
			"%s UTC [%p] [NULL]:\t",
			lpszTime,
			GetCurrentThread()
		);
                
		StringConcatenateSafe(lpszBuffer, lpcszStringToLog, sizeof(lpszBuffer) - 1);
	}
        else
        {
		time(&ttRawTime);
		tmUtc = gmtime_r(&ttRawTime, &tmState);
		ZeroMemory(lpszTime, sizeof(lpszTime)); 

		strftime(
			lpszTime,
			sizeof(lpszTime) - 1,
			"%Y-%m-%d %T",
			tmUtc
		);

                switch (lgprPriority)
                {
                        case LOG_DEBUG:
                                StringPrintFormatSafe(
					lpszBuffer, 
					sizeof(lpszBuffer) - 1,
					"%s UTC [%p] [DEBUG]:\t",
					lpszTime,
					GetCurrentThread()
				);
                                break;
                        case LOG_INFO:
                                StringPrintFormatSafe(
					lpszBuffer, 
					sizeof(lpszBuffer) - 1,
					"%s UTC [%p] [INFO]:\t",
					lpszTime,
					GetCurrentThread()
				);
                                break;
                        case LOG_WARNING:
                                StringPrintFormatSafe(
					lpszBuffer, 
					sizeof(lpszBuffer) - 1,
					"%s UTC [%p] [WARNING]:\t",
					lpszTime,
					GetCurrentThread()
				);
                                break;
                        case LOG_ERROR:
                                StringPrintFormatSafe(
					lpszBuffer, 
					sizeof(lpszBuffer) - 1,
					"%s UTC [%p] [ERROR]:\t",
					lpszTime,
					GetCurrentThread()
				);
                                break;
                        case LOG_FATAL:
                                StringPrintFormatSafe(
					lpszBuffer, 
					sizeof(lpszBuffer) - 1,
					"%s UTC [%p] [FATAL]:\t",
					lpszTime,
					GetCurrentThread()
				);
				break;
                }
		
		StringConcatenateSafe(lpszBuffer, lpcszStringToLog, sizeof(lpszBuffer) - 1);
        }
        

        if (lploLog->lppldDescriptor == NULLPTR)
                for (ulIndex = 0; ulIndex < lploLog->ulNumberOfFiles; ulIndex++)
                        bRet &= WriteFile(lploLog->lphFiles[ulIndex], lpszBuffer, ulWriteFlags);
        else
        {
                switch (lgprPriority)
                {
                        case LOG_DEBUG:
                        {
                                for (ulIndex = 0; ulIndex < lploLog->lppldDescriptor->ulNumDebugFiles; ulIndex++)
                                        bRet &= WriteFile(lploLog->lppldDescriptor->lphDebug[ulIndex], lpszBuffer, ulWriteFlags);
                                break;
                        }
                        case LOG_INFO:
                        {
                                for (ulIndex = 0; ulIndex < lploLog->lppldDescriptor->ulNumInfoFiles; ulIndex++)
                                        bRet &= WriteFile(lploLog->lppldDescriptor->lphInfo[ulIndex], lpszBuffer, ulWriteFlags);
                                break;
                        }
                        case LOG_WARNING:
                        {
                                for (ulIndex = 0; ulIndex < lploLog->lppldDescriptor->ulNumWarningFiles; ulIndex++)
                                        bRet &= WriteFile(lploLog->lppldDescriptor->lphWarning[ulIndex], lpszBuffer, ulWriteFlags);
                                break;
                        }
                        case LOG_ERROR:
                        {
                                for (ulIndex = 0; ulIndex < lploLog->lppldDescriptor->ulNumErrorFiles; ulIndex++)
                                        bRet &= WriteFile(lploLog->lppldDescriptor->lphError[ulIndex], lpszBuffer, ulWriteFlags);
                                break;
                        }
                        case LOG_FATAL:
                        {
                                for (ulIndex = 0; ulIndex < lploLog->lppldDescriptor->ulNumFatalFiles; ulIndex++)
                                        bRet &= WriteFile(lploLog->lppldDescriptor->lphFatal[ulIndex], lpszBuffer, ulWriteFlags);
                                break;
                        }
                        default:
                        {
                                for (ulIndex = 0; ulIndex < lploLog->ulNumberOfFiles; ulIndex++)
                                        bRet &= WriteFile(lploLog->lphFiles[ulIndex], lpszBuffer, ulWriteFlags);
                                break;
                        }
                }
        }
       return bRet;
}


/* Must have been created with "CreateLogWithMultipleFilesAndDefinableProc" */
_Success_(return != FALSE, _Acquires_Shared_Lock_(hLog->hCritSec, _Lock_Kind_Critical_Section_(hLog)))
BOOL
WriteLogSingleFile(
        _In_                                    HANDLE                          hLog,
        _In_                                    ULONG                           ulFileIndex,
        _In_                                    LPCSTR                          lpcszStringToLog,
        _In_Opt_                                LOG_PRIORITY                    lgprPriority,
        _In_Opt_                                ULONG                           ulFlags
){
        UNREFERENCED_PARAMETER(ulFlags);
        EXIT_IF_UNLIKELY_NULL(hLog, 0);
        EXIT_IF_UNLIKELY_NULL(lpcszStringToLog, 0);
        EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hLog), HANDLE_TYPE_LOG, FALSE);
        LPLOG_OBJECT lploLog;
        CSTRING lpszBuffer[2048];

        lploLog = (LPLOG_OBJECT)GetHandleDerivative(hLog);
        ZeroMemory(lpszBuffer, sizeof(CHAR) * 2048);

        if (StringLength(lpcszStringToLog) > 2047) return FALSE;

        if (lgprPriority == 0)
                StringPrintFormat(lpszBuffer, "%s", lpcszStringToLog);
        else
        {
                CSTRING lpszPriority[16];
                ZeroMemory(lpszPriority, sizeof(CHAR) * 16);
                switch (lgprPriority)
                {
                        case LOG_DEBUG:
                                StringPrintFormat(lpszBuffer, "DEBUG: ");
                                break;
                        case LOG_INFO:
                                StringPrintFormat(lpszBuffer, "INFO: ");
                                break;
                        case LOG_WARNING:
                                StringPrintFormat(lpszBuffer, "WARNING: ");
                                break;
                        case LOG_ERROR:
                                StringPrintFormat(lpszBuffer, "ERROR: ");
                                break;
                        case LOG_FATAL:
                                StringPrintFormat(lpszBuffer, "FATAL: ");
                }
                StringConcatenate(lpszBuffer, lpcszStringToLog);
        }
        
        return WriteFile(lploLog->lphFiles[ulFileIndex], lpszBuffer, NULL);
}