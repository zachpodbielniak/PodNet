/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CLOG_H
#define CLOG_H

#include "../Prereqs.h"
#include "../Annotation.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../TypeDefs.h"
#include "../CHandle/CHandle.h"
#include "../CFile/CFile.h"


#define LOG_WRITE_FLUSH			(1<<1)


typedef enum __LOG_PRIORITY
{
        LOG_DEBUG       = 1,
        LOG_INFO        = 2,
        LOG_WARNING     = 3,
        LOG_ERROR       = 4,
        LOG_FATAL       = 5
} LOG_PRIORITY;


typedef struct __PRIORITY_LOG_DESCRIPTOR
{
        LPHANDLE        lphDebug;
        ULONG           ulNumDebugFiles;  
        LPHANDLE        lphInfo;
        ULONG           ulNumInfoFiles;
        LPHANDLE        lphWarning;
        ULONG           ulNumWarningFiles;
        LPHANDLE        lphError;
        ULONG           ulNumErrorFiles;
        LPHANDLE        lphFatal;
        ULONG           ulNumFatalFiles;
} PRIORITY_LOG_DESCRIPTOR, *LPPRIORITY_LOG_DESCRIPTOR;


/* When adding STDOUT to a Log, it will be position at index 0 */
#define LOG_FLAG_ADD_STDOUT     1<<0

_Result_Null_On_Failure_
PODNET_API
HANDLE
CreateLog(
        _In_Z_                  LPCSTR                  lpcszFileName,
        _In_Opt_                ULONG                   ulFlags
);


_Result_Null_On_Failure_
PODNET_API
HANDLE
CreateLogFromOpenedFile(
        _In_                    HANDLE                  hFile
);


_Result_Null_On_Failure_
PODNET_API
HANDLE
CreatePriorityLog(
        _In_                    LPPRIORITY_LOG_DESCRIPTOR       lpPriorityDescriptor
);


_Result_Null_On_Failure_
PODNET_API
HANDLE
CreateLogWithMultipleFiles(
        _In_Reads_To_Ptr_Z_(*dlpcszFileNames)   DLPCSTR                         dlpcszFileNames,
        _In_                                    ULONG                           ulNumberOfFiles,
        _In_Opt_                                ULONG                           ulFlags
);



_Result_Null_On_Failure_
PODNET_API
HANDLE
CreateLogWithMultipleFilesAndDefinableProc(
        _In_Reads_To_Ptr_Z_(*dlpcszFileNames)   DLPCSTR                         dlpcszFileNames,
        _In_                                    ULONG                           ulNumberOfFiles,
        _In_                                    LPFN_HANDLE_CUSTOM_PROC         lpfnCustomProc,
        _In_Opt_                                ULONG                           ulFlags
);



_Success_(return != FALSE, _Acquires_Shared_Lock_(hLog->hCritSec, _Lock_Kind_Critical_Section_(hLog)))
BOOL
WriteLog(
        _In_                                    HANDLE                          hLog,
        _In_                                    LPCSTR                          lpcszStringToLog,
        _In_Opt_                                LOG_PRIORITY                    logprPriority,
        _In_Opt_                                ULONG                           ulFlags
);


/* Must have been created with "CreateLogWithMultipleFilesAndDefinableProc" */
_Success_(return != FALSE, _Acquires_Shared_Lock_(hLog->hCritSec, _Lock_Kind_Critical_Section_(hLog)))
BOOL
WriteLogSingleFile(
        _In_                                    HANDLE                          hLog,
        _In_                                    ULONG                           ulFileIndex,
        _In_                                    LPCSTR                          lpcszStringToLog,
        _In_Opt_                                LOG_PRIORITY                    lpgprPriority,
        _In_Opt_                                ULONG                           ulFlags
);




#endif
