/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CUNITTEST_H
#define CUNITTEST_H


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../CContainers/CVector.h"
#include "../CMemory/CMemory.h"



typedef _Call_Back_ BOOL(* LPFN_UNIT_TEST_PROC)(
	VOID
);


#define TEST_CALLBACK(FUNCTION_NAME)				\
	INTERNAL_OPERATION					\
	BOOL							\
	FUNCTION_NAME(						\
		VOID 						\
	)



#define TEST_BEGIN(TEST_NAME)		\
	BOOL bPass;			\
	LPSTR lpszName;			\
	ULTRALONG_UNION ultuX, ultuY;	\
	bPass = TRUE;			\
	lpszName = TEST_NAME 


#define TEST_END()														\
	if (FALSE == bPass) 													\
	{ PrintFormat(ANSI_BLUE_BOLD "Test:" ANSI_RESET " \"%s\" -> " ANSI_RED_BOLD "FAILED!\n" ANSI_RESET, lpszName); }	\
	else															\
	{ PrintFormat(ANSI_BLUE_BOLD "Test:" ANSI_RESET " \"%s\" -> " ANSI_GREEN_BOLD "SUCCESS!\n" ANSI_RESET, lpszName); }	\
	return bPass;




/* The following are equal */

#ifdef __LP64__

#define TEST_EXPECT_VALUE_EQUAL_FAIL(X,Y)		\
	bPass = FALSE;					\
	PrintFormat("%s: got %lu, expected %lu\n", lpszName, (ULONGLONG)(X), (ULONGLONG)(Y));


#define TEST_EXPECT_VALUE_EQUAL(X,Y)			\
	if ((X) != (Y)) { TEST_EXPECT_VALUE_EQUAL_FAIL((X), (Y));  }

#else

#define TEST_EXPECT_VALUE_EQUAL_FAIL(X,Y)		\
	bPass = FALSE;					\
	PrintFormat("%s: got %llu, expected %llu\n", lpszName, (ULONGLONG)(X), (ULONGLONG)(Y));


#define TEST_EXPECT_VALUE_EQUAL(X,Y)			\
	if ((X) != (Y)) { TEST_EXPECT_VALUE_EQUAL_FAIL((X), (Y));  }

#endif




/* The following are not equal */

#ifdef __LP64__

#define TEST_EXPECT_VALUE_NOT_EQUAL_FAIL(X,Y)		\
	bPass = FALSE;					\
	PrintFormat("%s: both are %lu, expected different\n", lpszName, (ULONGLONG)(X));


#define TEST_EXPECT_VALUE_NOT_EQUAL(X,Y)		\
	if ((X) == (Y)) { TEST_EXPECT_VALUE_NOT_EQUAL_FAIL(X,Y); }

#else

#define TEST_EXPECT_VALUE_NOT_EQUAL_FAIL(X,Y)		\
	bPass = FALSE;					\
	PrintFormat("%s: both are %llu, expected different\n", lpszName, (ULONGLONG)(X));


#define TEST_EXPECT_VALUE_NOT_EQUAL(X,Y)		\
	if ((X) == (Y)) { TEST_EXPECT_VALUE_NOT_EQUAL_FAIL(X,Y); }

#endif




/* The following are for FLOATS */

#define TEST_EXPECT_FLOAT_EQUAL_FAILED(X,Y)				\
	bPass = FALSE;							\
	PrintFormat("%s: got %f, expected %f\n", lpszName, (X), (Y));


#define TEST_EXPECT_FLOAT_EQUAL(X,Y)					\
	ultuX.fX[0] = (X);						\
	ultuY.fX[0] = (Y);						\
	ultuX.ulX[0] = *(LPULONG)&(ultuX.fX[0]);			\
	ultuY.ulX[0] = *(LPULONG)&(ultuY.fX[0]); 			\
	if (ultuX.ulX[0] > ultuY.ulX[0])				\
	{								\
		if ((ultuX.ulX[0] - ultuY.ulX[0]) > EPSILON)		\
		{ TEST_EXPECT_FLOAT_EQUAL_FAILED((X), (Y)); }		\
	}								\
	else								\
	{								\
		if ((ultuY.ulX[0] - ultuX.ulX[0]) > EPSILON)		\
		{ TEST_EXPECT_FLOAT_EQUAL_FAILED((X), (Y)); }		\
	}								\
	ultuX.fX[0] = 0;						\
	ultuY.fX[0] = 0;




/* The following are for DOUBLEs */

#define TEST_EXPECT_DOUBLE_EQUAL_FAILED(X,Y)				\
	bPass = FALSE;							\
	PrintFormat("%s: got %f, expected %f\n", lpszName, (X), (Y));


#define TEST_EXPECT_DOUBLE_EQUAL(X,Y)					\
	ultuX.dbX[0] = (X);						\
	ultuY.dbX[0] = (Y);						\
	ultuX.ullX[0] = *(LPULONGLONG)&(ultuX.dbX[0]);			\
	ultuY.ullX[0] = *(LPULONGLONG)&(ultuY.dbX[0]); 			\
	if (ultuX.ullX[0] > ultuY.ullX[0])				\
	{								\
		if ((ultuX.ullX[0] - ultuY.ullX[0]) > EPSILON_D)	\
		{ TEST_EXPECT_DOUBLE_EQUAL_FAILED((X), (Y)); }		\
	}								\
	else								\
	{								\
		if ((ultuY.ullX[0] - ultuX.ullX[0]) > EPSILON_D)	\
		{ TEST_EXPECT_DOUBLE_EQUAL_FAILED((X), (Y)); }		\
	}								\
	ultuX.dbX[0] = 0;						\
	ultuY.dbX[0] = 0;




/* The following are for STRINGs */

#define TEST_EXPECT_STRING_EQUAL_FAILED(X,Y)						\
	bPass = FALSE;									\
	PrintFormat("%s: got \"%s\", expected \"%s\"\n", lpszName, (X), (Y));	


#define TEST_EXPECT_STRING_EQUAL(X,Y)							\
	if (0 != StringCompare((X),(Y)))						\
	{ TEST_EXPECT_STRING_EQUAL_FAILED((X),(Y)); }



#define TEST_EXPECT_STRING_NOT_EQUAL_FAILED(X,Y)					\
	bPass = FALSE;									\
	PrintFormat("%s: got \"%s\", expected different.\n", lpszName, (X));


#define TEST_EXPECT_STRING_NOT_EQUAL(X,Y)						\
	if (0 == StringCompare((X),(Y)))						\
	{ TEST_EXPECT_STRING_NOT_EQUAL_FAILED((X),(Y)); }



/* The following are for memory regions */

#define TEST_EXPECT_MEMORY_EQUAL_FAILED(X,Y)					\
	bPass = FALSE;								\
	PrintFormat("%s: memory region %p is not equal to %p\n", lpszName, (X), (Y));


#define TEST_EXPECT_MEMORY_EQUAL(X,Y,SZ)					\
	if (0 != CompareMemory((X),(Y),(SZ)))					\
	{ TEST_EXPECT_MEMORY_EQUAL_FAILED((X),(Y)); }




/* The following are part of the main body */

#define TEST_START(TEST_NAME)								\
	LONG Main()									\
	{										\
		HVECTOR hvTests;							\
		BOOL bPass;								\
		LPFN_UNIT_TEST_PROC lpfnCallBack;					\
		LPSTR lpszTest = TEST_NAME;						\
		hvTests = CreateVector(10, sizeof(LPFN_UNIT_TEST_PROC), 0);		\
		bPass = TRUE; 								\
		EXIT_IF_UNLIKELY_NULL(hvTests, 1)					


#define TEST_ADD(UNIT_TEST_CALL_BACK)							\
		lpfnCallBack = UNIT_TEST_CALL_BACK;					\
		VectorPushBack(hvTests, &(lpfnCallBack), 0)		



#define TEST_RUN()											\
		LPVOID lpIterator;									\
		for (											\
			lpIterator = VectorFront(hvTests);						\
			(UARCHLONG)lpIterator <= (UARCHLONG)VectorBack(hvTests);			\
			lpIterator = (LPVOID)((UARCHLONG)lpIterator + sizeof(LPFN_UNIT_TEST_PROC)) 	\
		){											\
			lpfnCallBack = *(LPFN_UNIT_TEST_PROC*)lpIterator;				\
			if (NULLPTR != lpfnCallBack)							\
			{										\
				bPass &= lpfnCallBack(); 						\
				/* PrintFormat("\n"); */						\
			}										\
		}

	

#define TEST_CONCLUDE()															\
		if (TRUE == bPass)													\
		{ PrintFormat(ANSI_GREEN_BOLD "\n\nAll %s tests have passed successfully!\n" ANSI_RESET, lpszTest); }		\
		else															\
		{ PrintFormat(ANSI_RED_BOLD "\n\nAt least one %s test has failed!\n" ANSI_RESET, lpszTest); }			\
		DestroyVector(hvTests);													\
		return !bPass;														\
	}



#endif