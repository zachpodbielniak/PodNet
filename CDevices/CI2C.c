/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CI2C.h"
#include "../CLock/CSemaphore.h"
#include "../CHandle/CHandleTable.h"


EXTERN
LONG
piGpioLayout(
        VOID
);


/* I2C Definitions */

#define I2C_SLAVE                       0x0703
#define I2C_SMBUS                       0x0720
#define I2C_SMBUS_READ                  0x01
#define I2C_SMBUS_WRITE                 0x00

/* SMBus Transaction Types */

#define I2C_SMBUS_QUICK                 0x00
#define I2C_SMBUS_BYTE                  0x01
#define I2C_SMBUS_BYTE_DATA             0x02
#define I2C_SMBUS_WORD_DATA             0x03
#define I2C_SMBUS_PROC_CALL             0x04
#define I2C_SMBUS_BLOCK_DATA            0x05
#define I2C_SMBUS_I2C_BLOCK_BROKEN      0x06
#define I2C_SMBUS_BLOCK_PROC_CALL       0x07
#define I2C_SMBUS_I2C_BLOCK_DATA        0x08

/* SMBus Messages */

#define I2C_SMBUS_BLOCK_MAX             0x20
#define I2C_SMBUS_I2C_BLOCK_MAX         0x20



/* IOCTL Specific Structs / Unions */

typedef union __I2C_SMBUS_DATA
{
        BYTE            byByte;
        USHORT          usWord;
        BYTE            byBlock[I2C_SMBUS_BLOCK_MAX + 0x02];
} I2C_SMBUS_DATA, *LPI2C_SMBUS_DATA;




typedef struct __I2C_SMBUS_IOCTL_DATA
{
        CHAR                    cReadWrite;
        BYTE                    byCommand;
        ULONG                   ulSize;
        LPI2C_SMBUS_DATA        lpisdData;        
} I2C_SMBUS_IOCTL_DATA, *LPI2C_SMBUS_IOCTL_DATA;




typedef struct __I2C
{
        INHERITS_FROM_HANDLE();
        HANDLE          hSemaphore;
        LONG            lFileDesriptor;
        ULONG           ulDevice;
} I2C, *LPI2C;


GLOBAL_VARIABLE DONT_OPTIMIZE_OUT HANDLE hHandleTable = NULLPTR;




_Success_(return != -1, ...)
INTERNAL_OPERATION
INLINE
LONG
__I2CSMBusAccess(
        _In_                    LONG                    lFileDesriptor,
        _In_                    CHAR                    cReadWrite,
        _In_                    BYTE                    byCommand,
        _In_                    ULONG                   ulSize, 
        _In_                    LPI2C_SMBUS_DATA        lpisdData 
){
        I2C_SMBUS_IOCTL_DATA isidArgs;

        isidArgs.cReadWrite = cReadWrite;
        isidArgs.byCommand = byCommand;
        isidArgs.ulSize = ulSize;
        isidArgs.lpisdData = lpisdData;

        return ioctl(lFileDesriptor, I2C_SMBUS, &isidArgs);
}




_Success_(return != FALSE, ...)
INTERNAL_OPERATION
BOOL
__CreateHandleTable(
        VOID
){
        if (NULLPTR == hHandleTable)
        { 
                hHandleTable = CreateHandleTable(0x04, NULLPTR, HANDLE_TABLE_HIGH_PERFORMANCE);
                EXIT_IF_NULL(hHandleTable, FALSE);
        }
        return TRUE;
}




_Success_(return != FALSE, ...)
INTERNAL_OPERATION
BOOL
__DestroyI2C(
        _In_                    HDERIVATIVE     hDerivative,
        _In_                    ULONG           ulFlags
){
        UNREFERENCED_PARAMETER(ulFlags);
        EXIT_IF_UNLIKELY_NULL(hDerivative, FALSE);
        LPI2C lpI2C;
        lpI2C = (LPI2C)hDerivative;

        WaitForSingleObject(lpI2C->hSemaphore, INFINITE_WAIT_TIME);

        close(lpI2C->lFileDesriptor);
        DestroyHandle(lpI2C->hSemaphore);
        DeregisterHandle(hHandleTable, lpI2C->hThis);
        FreeMemory(lpI2C);

        return TRUE;       
}




_Call_Back_
_Success_(return != FALSE, ...)
INTERNAL_OPERATION
BOOL
__I2CTableLookup(
        _In_                    HDERIVATIVE     hLookingFor,
        _In_                    HDERIVATIVE     hComparand
){
        EXIT_IF_NULL(hComparand, FALSE);

        ULONG ulDevice;
        LPI2C lpI2C;
        
        #ifdef __LP64__
        ulDevice = (ULONG)(ULONGLONG)hLookingFor;
        #else
        ulDevice = (ULONG)hLookingFor;
        #endif

        lpI2C = (LPI2C)hComparand;

        return (ulDevice == lpI2C->ulDevice) ? TRUE : FALSE;
}




_Success_(return != NULLPTR, ...)
PODNET_API
HANDLE
CreateI2CInterface(
        _In_                    ULONG           ulDeviceId,
        _Reserved_Must_Be_Null_ LPVOID          lpReserved,
        _In_                    ULONG           ulFlags
){
        UNREFERENCED_PARAMETER(lpReserved);
        UNREFERENCED_PARAMETER(ulFlags);

        HANDLE hI2C;
        LPI2C lpI2C;
        LPSTR lpszDevice;
        hI2C = NULLPTR;

        if (UNLIKELY(NULLPTR == hHandleTable))
        { EXIT_IF_UNLIKELY_NOT_VALUE(__CreateHandleTable(), TRUE, NULLPTR); }

        #ifdef __LP64__
        hI2C = GrabHandleByDerivative(hHandleTable, (LPVOID)(ULONGLONG)ulDeviceId, NULL, __I2CTableLookup);
        #else
        hI2C = GrabHandleByDerivative(hHandleTable, (LPVOID)ulDeviceId, NULL, __I2CTableLookup);
        #endif

        if (NULLPTR != hI2C)
        { return hI2C; }
        else
        { hI2C = NULLPTR; }
        
        lpszDevice = (1 == piGpioLayout()) ? "/dev/i2c-0" : "/dev/i2c-1";

        lpI2C = (LPI2C)LocalAlloc(sizeof(I2C));
	/* cppcheck-suppress memleak */
        EXIT_IF_UNLIKELY_NULL(lpI2C, NULLPTR);

        lpI2C->ulDevice = ulDeviceId;
        lpI2C->lFileDesriptor = open(lpszDevice, O_RDWR);
	if (-1 == lpI2C->lFileDesriptor)
	{
		FreeMemory(lpI2C);
		return NULL_OBJECT;
	}

        if (ioctl(lpI2C->lFileDesriptor, I2C_SLAVE, ulDeviceId) < 0)
        { 
		close(lpI2C->lFileDesriptor);
		FreeMemory(lpI2C);
		return NULL_OBJECT;
	}

        lpI2C->hSemaphore = CreateSemaphore(1U);
        if (NULL_OBJECT == lpI2C->hSemaphore)
        { 
		close(lpI2C->lFileDesriptor);
		FreeMemory(lpI2C);
		return NULL_OBJECT;
	}

        hI2C = CreateHandleWithSingleInheritor(
                        lpI2C,
                        &(lpI2C->hThis),
                        HANDLE_TYPE_DEVICE_I2C,
                        __DestroyI2C,
                        NULL,
                        NULLPTR,
                        NULL,
                        NULLPTR,
                        NULL,
                        &(lpI2C->ullId),
                        NULL
        );

        if (NULL_OBJECT == hI2C)
        { 
		DestroyObject(lpI2C->hSemaphore);
		close(lpI2C->lFileDesriptor);
		FreeMemory(lpI2C);
		return NULL_OBJECT;
	}

        RegisterHandle(hHandleTable, hI2C);
        return hI2C;
}




_Success_(return != FALSE, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_)))
PODNET_API
BOOL
WriteI2C(
        _In_                    HANDLE          hI2C,
        _In_                    BYTE            byValue
){
        EXIT_IF_UNLIKELY_NULL(hI2C, FALSE);
        EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hI2C), HANDLE_TYPE_DEVICE_I2C, FALSE);
        LPI2C lpI2C;
        LONG lRet;
        lpI2C = (LPI2C)GetHandleDerivative(hI2C);
        EXIT_IF_UNLIKELY_NULL(lpI2C, FALSE);

        WaitForSingleObject(lpI2C->hSemaphore, INFINITE_WAIT_TIME);

        lRet = __I2CSMBusAccess(lpI2C->lFileDesriptor, I2C_SMBUS_WRITE, byValue, I2C_SMBUS_BYTE, NULL);

        ReleaseSingleObject(lpI2C->hSemaphore);        

        return (0 == lRet) ? TRUE : FALSE;
}




_Success_(return != FALSE, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_)))
PODNET_API
BOOL
WriteI2CRegisterByte(
        _In_                    HANDLE          hI2C,
        _In_                    BYTE            byRegister,
        _In_                    BYTE            byValue
){
        EXIT_IF_UNLIKELY_NULL(hI2C, FALSE);
        EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hI2C), HANDLE_TYPE_DEVICE_I2C, FALSE);
        LPI2C lpI2C;
        LONG lRet;
        I2C_SMBUS_DATA isdData;
        lpI2C = (LPI2C)GetHandleDerivative(hI2C);
        EXIT_IF_UNLIKELY_NULL(lpI2C, FALSE);
        
        isdData.byByte = byValue;

        WaitForSingleObject(lpI2C->hSemaphore, INFINITE_WAIT_TIME);

        lRet = __I2CSMBusAccess(lpI2C->lFileDesriptor, I2C_SMBUS_WRITE, byRegister, I2C_SMBUS_BYTE_DATA, &isdData);

        ReleaseSingleObject(lpI2C->hSemaphore);        

        return (0 == lRet) ? TRUE : FALSE;
}




_Success_(return != FALSE, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_)))
PODNET_API
BOOL
WriteI2CRegisterShort(
        _In_                    HANDLE          hI2C,
        _In_                    BYTE            byRegister,
        _In_                    USHORT          usValue
){
        EXIT_IF_UNLIKELY_NULL(hI2C, FALSE);
        EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hI2C), HANDLE_TYPE_DEVICE_I2C, FALSE);
        LPI2C lpI2C;
        LONG lRet;
        I2C_SMBUS_DATA isdData;
        lpI2C = (LPI2C)GetHandleDerivative(hI2C);
        EXIT_IF_UNLIKELY_NULL(lpI2C, FALSE);

        isdData.usWord = usValue;
        
        WaitForSingleObject(lpI2C->hSemaphore, INFINITE_WAIT_TIME);

        lRet = __I2CSMBusAccess(lpI2C->lFileDesriptor, I2C_SMBUS_WRITE, byRegister, I2C_SMBUS_WORD_DATA, &isdData);

        ReleaseSingleObject(lpI2C->hSemaphore);        

        return (0 == lRet) ? TRUE : FALSE;
}




_Success_(return != NULL, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_)))
PODNET_API
BYTE
ReadI2C(
        _In_                    HANDLE          hI2C
){
        EXIT_IF_UNLIKELY_NULL(hI2C, FALSE);
        EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hI2C), HANDLE_TYPE_DEVICE_I2C, FALSE);
        LPI2C lpI2C;
        I2C_SMBUS_DATA isdData;
        lpI2C = (LPI2C)GetHandleDerivative(hI2C);
        EXIT_IF_UNLIKELY_NULL(lpI2C, FALSE);

        WaitForSingleObject(lpI2C->hSemaphore, INFINITE_WAIT_TIME);

        __I2CSMBusAccess(lpI2C->lFileDesriptor, I2C_SMBUS_READ, 0, I2C_SMBUS_BYTE, &isdData);

        ReleaseSingleObject(lpI2C->hSemaphore);        

        return (0xFF & isdData.byByte);
}




_Success_(return != NULL, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_)))
PODNET_API
BYTE
ReadI2CRegisterByte(
        _In_                    HANDLE          hI2C,
        _In_                    BYTE            byRegister
){
        EXIT_IF_UNLIKELY_NULL(hI2C, FALSE);
        EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hI2C), HANDLE_TYPE_DEVICE_I2C, FALSE);
        LPI2C lpI2C;
        I2C_SMBUS_DATA isdData;
        lpI2C = (LPI2C)GetHandleDerivative(hI2C);
        EXIT_IF_UNLIKELY_NULL(lpI2C, FALSE);

        WaitForSingleObject(lpI2C->hSemaphore, INFINITE_WAIT_TIME);

        __I2CSMBusAccess(lpI2C->lFileDesriptor, I2C_SMBUS_READ, byRegister, I2C_SMBUS_BYTE_DATA, &isdData);

        ReleaseSingleObject(lpI2C->hSemaphore);        

        return (0xFF & isdData.byByte);
}




_Success_(return != NULL, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_)))
PODNET_API
USHORT
ReadI2CRegisterShort(
        _In_                    HANDLE          hI2C,
        _In_                    BYTE            byRegister
){
        EXIT_IF_UNLIKELY_NULL(hI2C, FALSE);
        EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hI2C), HANDLE_TYPE_DEVICE_I2C, FALSE);
        LPI2C lpI2C;
        I2C_SMBUS_DATA isdData;
        lpI2C = (LPI2C)GetHandleDerivative(hI2C);
        EXIT_IF_UNLIKELY_NULL(lpI2C, FALSE);

        WaitForSingleObject(lpI2C->hSemaphore, INFINITE_WAIT_TIME);

        __I2CSMBusAccess(lpI2C->lFileDesriptor, I2C_SMBUS_READ, byRegister, I2C_SMBUS_WORD_DATA, &isdData);

        ReleaseSingleObject(lpI2C->hSemaphore);        

        return (0xFFFF & isdData.usWord);
}
