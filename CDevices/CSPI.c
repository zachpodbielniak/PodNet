/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CSPI.h"


typedef struct __SPI 
{
        INHERITS_FROM_HANDLE();
        HANDLE          hSemaphore;
        LPSTR           lpszDevice;
        ULONG           ulSpeed;
        LONG            lFileDesriptor;
        BYTE            byChannel;
        BYTE            byBPW;
        USHORT          usDelay;
        UARCHLONG       ualMode;
} SPI, *LPSPI;


GLOBAL_VARIABLE DONT_OPTIMIZE_OUT HANDLE hHandleTable = NULLPTR;




_Success_(return != FALSE, ...)
INTERNAL_OPERATION
BOOL
__CreateHandleTable(
        VOID
){
        if (NULLPTR == hHandleTable)
        { 
                hHandleTable = CreateHandleTable(0x04, NULLPTR, HANDLE_TABLE_HIGH_PERFORMANCE);
                EXIT_IF_NULL(hHandleTable, FALSE);
        }
        return TRUE;
}




_Call_Back_
_Success_(return != FALSE, _Acquires_Shared_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_)))
INTERNAL_OPERATION
BOOL
__DestroySpi(
        _In_                    HDERIVATIVE     hDerivative,
        _In_Opt_                ULONG           ulFlags
){
        UNREFERENCED_PARAMETER(ulFlags);
        EXIT_IF_UNLIKELY_NULL(hDerivative, FALSE);
        LPSPI lpSPI;
        lpSPI = (LPSPI)hDerivative;
        WaitForSingleObject(lpSPI->hSemaphore, INFINITE_WAIT_TIME);
        DestroyHandle(lpSPI->hThis);
        close(lpSPI->lFileDesriptor);
        FreeMemory(lpSPI);
        return TRUE;
}




_Call_Back_
_Success_(return != FALSE, ...)
INTERNAL_OPERATION
BOOL
__ReverseSpiLookup(
        _In_                    HDERIVATIVE     hLookingFor,
        _In_                    HDERIVATIVE     hComparand
){
        EXIT_IF_NULL(hComparand, FALSE);

        BYTE byChannel;
        LPSPI lpSPI;
        
        byChannel = (BYTE)(UARCHLONG)hLookingFor;
        lpSPI = (LPSPI)hComparand;

        return (byChannel == lpSPI->byChannel) ? TRUE : FALSE;
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HANDLE
OpenSpiChannel(
        _In_Range_(0, 1)                BYTE            byChannel,
        _In_Range_(500000, 32000000)    ULONG           ulSpeed,
        _Reserved_Must_Be_Null_         LPVOID          lpReserved
){
        UNREFERENCED_PARAMETER(lpReserved);

        HANDLE hSPI;
        LPSPI lpSPI;
        hSPI = NULLPTR;

        if (UNLIKELY(NULLPTR == hHandleTable))
        { EXIT_IF_UNLIKELY_NOT_VALUE(__CreateHandleTable(), TRUE, NULLPTR); }

        hSPI = GrabHandleByDerivative(hHandleTable, (LPVOID)(UARCHLONG)byChannel, NULL, __ReverseSpiLookup);

        if (NULLPTR != hSPI)
        { return hSPI; }

        hSPI = NULLPTR;

        lpSPI = (LPSPI)LocalAllocAndZero(sizeof(SPI));
	/* cppcheck-suppress memleak */
        EXIT_IF_UNLIKELY_NULL(lpSPI, NULL_OBJECT);

        lpSPI->byBPW = 0x08;
        lpSPI->byChannel = byChannel;
        lpSPI->hSemaphore = CreateSemaphore(0x01U);
        lpSPI->lpszDevice = (0 == byChannel) ? "/dev/spidev0.0" : "/dev/spidev0.1";
        lpSPI->ulSpeed = ulSpeed;
        lpSPI->usDelay = 0x0000;
        lpSPI->ualMode = 0x00;

        lpSPI->lFileDesriptor = open(lpSPI->lpszDevice, O_RDWR);
	if (-1 == lpSPI->lFileDesriptor)
	{	
		DestroyObject(lpSPI->hSemaphore);
		FreeMemory(lpSPI);
		return NULL_OBJECT;
	}

        ioctl(lpSPI->lFileDesriptor, SPI_IOC_WR_MODE, &(lpSPI->ualMode));
        ioctl(lpSPI->lFileDesriptor, SPI_IOC_WR_BITS_PER_WORD, &(lpSPI->byBPW));
        ioctl(lpSPI->lFileDesriptor, SPI_IOC_WR_MAX_SPEED_HZ, &ulSpeed);


        hSPI = CreateHandleWithSingleInheritor(
                        lpSPI,
                        &(lpSPI->hThis),
                        HANDLE_TYPE_DEVICE_SPI,
                        __DestroySpi,
                        NULL,
                        NULLPTR,
                        NULL,
                        NULLPTR,
                        NULL,
                        &(lpSPI->ullId),
                        NULL
        );

        EXIT_IF_UNLIKELY_NULL(hSPI, NULLPTR);

        RegisterHandle(hHandleTable, hSPI);
        return hSPI;
}




_Success_(return != NULL, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_)))
PODNET_API
ULONG 
SpiReadWrite(
        _In_                            HANDLE          hSPI,
        _In_                            LPBYTE          lpbyData,
        _In_                            ULONG           ulSize,
        _Reserved_Must_Be_Null_         LPVOID          lpReserved
){
        UNREFERENCED_PARAMETER(lpReserved);
        EXIT_IF_UNLIKELY_NULL(hSPI, NULL);
        EXIT_IF_UNLIKELY_NULL(lpbyData, NULL);
        EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hSPI), HANDLE_TYPE_DEVICE_SPI, NULL);
        LPSPI lpSPI;
        struct spi_ioc_transfer sitTransfer;
        lpSPI = (LPSPI)GetHandleDerivative(hSPI);
        EXIT_IF_UNLIKELY_NULL(lpSPI, NULL);

        ZeroMemory(&sitTransfer, sizeof(struct spi_ioc_transfer));

        WaitForSingleObject(lpSPI->hSemaphore, INFINITE_WAIT_TIME);

        sitTransfer.tx_buf = (UARCHLONG)lpbyData;
        sitTransfer.rx_buf = (UARCHLONG)lpbyData;
        sitTransfer.len = ulSize;
        sitTransfer.delay_usecs = lpSPI->usDelay;
        sitTransfer.speed_hz = lpSPI->ulSpeed;
        sitTransfer.bits_per_word = lpSPI->byBPW;

        ReleaseSingleObject(lpSPI->hSemaphore);

        return ioctl(lpSPI->lFileDesriptor, SPI_IOC_MESSAGE(1), &sitTransfer);
}
