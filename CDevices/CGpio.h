/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CGPIO_H
#define CGPIO_H


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../CHandle/CHandle.h"
#include "../CThread/CThread.h"
#include "../CLock/CSemaphore.h"
#include "../CError/CError.h"
#include "../CSystem/CSystem.h"


typedef HANDLE                          HPIN;



#define GPIO_PULL_MODE_OFF              0x00
#define GPIO_PULL_MODE_UP               0x01
#define GPIO_PULL_MODE_DOWN             0x02


#define GPIO_MODE_INPUT                 0x01
#define GPIO_MODE_OUTPUT                0x02
#define GPIO_MODE_PWM                   0x03
#define GPIO_MODE_CLOCK                 0x04

#define GPIO_OFF                        0x00
#define GPIO_ON                         0x01


/* GPIO General Purpose Flags */
typedef enum __GPIO_FLAG
{
        GPIO_INTERLOCKED_OPERATION      = 1 << 0
} GPIO_FLAGS;



typedef enum __GPIO_READ_WRITE_FLAGS
{
        GPIO_DIGITAL_OPERATION          = 1 << 8,
        GPIO_ANALOG_OPERATION           = 1 << 9,
        GPIO_PWM_OPERATION              = 1 << 10
} GPIO_READ_WRITE_FLAGS;



_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HANDLE
OpenGpioPinAndSpecifyMode(
        _In_                            ULONG                   ulPin,
        _In_                            ULONG                   ulMode,
        _In_                            ULONG                   ulPullMode
);




_Success_(return != MAX_ULONG, _Non_Locking_)
PODNET_API
ULONG
GetGpioPinMode(
        _In_                            HANDLE                  hPin
);




_Success_(return != MAX_ULONG, _Non_Locking_)
PODNET_API
ULONG
GetGpioPullMode(
        _In_                            HANDLE                  hPin
);




_Success_(return != MAX_ULONG, _Non_Locking_)
PODNET_API
HANDLE
GetGpioPinHandle(
        _In_                            ULONG                   ulPin
);




_Success_(return != FALSE, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_)))
PODNET_API
BOOL
GpioSpecifyPinMode(
        _In_                            HANDLE                  hPin,
        _In_                            ULONG                   ulMode,
        _In_                            ULONG                   ulPullMode
);




_Success_(return != FALSE, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_)))
PODNET_API
BOOL
GpioChangePullMode(
        _In_                            HANDLE                  hPin,
        _In_                            ULONG                   ulPullMode
);




_Success_(return != FALSE, _Interlocked_When_(ulFlags & GPIO_INTERLOCKED_OPERATION, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_))))
PODNET_API
BOOL
WriteGpio(
        _In_                            HANDLE                  hPin,
        _In_                            BOOL                    bValue,
        _Reserved_Must_Be_Null_         LPVOID                  lpReserved,
        _In_Opt_                        ULONG                   ulFlags
);




_Success_(return != FALSE, _Interlocked_When_(ulFlags & GPIO_INTERLOCKED_OPERATION, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_))))
PODNET_API
BOOL
WriteGpioAnalog(
        _In_                            HANDLE                  hPin,
        _In_                            ULONG                   ulValue,
        _Reserved_Must_Be_Null_         LPVOID                  lpReserved,
        _In_Opt_                        ULONG                   ulFlags
);




_Success_(return != FALSE, _Interlocked_When_(ulFlags & GPIO_INTERLOCKED_OPERATION, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_))))
PODNET_API
BOOL
WriteGpioPwm(
        _In_                            HANDLE                  hPin,
        _In_                            BYTE                    ubDutyCycle,
        _Reserved_Must_Be_Null_         LPVOID                  lpReserved,
        _In_Opt_                        ULONG                   ulFlags
);




_Success_(return != MAX_UBYTE, _Interlocked_When_(ulFlags & GPIO_INTERLOCKED_OPERATION, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_))))
PODNET_API
BOOL
ReadGpio(
        _In_                            HANDLE                  hPin,
        _Reserved_Must_Be_Null_         LPVOID                  lpReserved,
        _In_Opt_                        ULONG                   ulFlags
);




_Success_(return != MAX_ULONG, _Interlocked_When_(ulFlags & GPIO_INTERLOCKED_OPERATION, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_))))
PODNET_API
ULONG
ReadGpioAnalog(
        _In_                            HANDLE                  hPin,
        _In_Opt_                        ULONG                   ulFlags
);



#endif
