/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CGpio.h"
#include "../CHandle/CHandleTable.h"
#include <wiringPi.h>


typedef struct __GPIO
{
        INHERITS_FROM_HANDLE();
        BYTE                    bPin;
        BYTE                    bState;
        BYTE                    bMode;
        BYTE                    bPullMode;
        BYTE                    bDutyCycle;
        HANDLE                  hPwmThread;
        HANDLE                  hSemaphore;
} GPIO, *LPGPIO;



GLOBAL_VARIABLE DONT_OPTIMIZE_OUT BOOL bInitWiringPi = FALSE;

GLOBAL_VARIABLE DONT_OPTIMIZE_OUT HANDLE hHandleTable = NULLPTR;


_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__InitWiringPi(
        VOID
){
        wiringPiSetup();
        bInitWiringPi = TRUE;
        return TRUE;
}


_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__CreateHandleTable(
        VOID
){
        if (NULLPTR == hHandleTable)
        { 
                hHandleTable = CreateHandleTable(40, NULLPTR, HANDLE_TABLE_HIGH_PERFORMANCE);
                EXIT_IF_NULL(hHandleTable, FALSE);
        }
        return TRUE;
}


_Thread_Proc_ _Acquires_Spin_Lock(lpGpio->hSemaphore)
INTERNAL_OPERATION
LPVOID
__PwmThreadProc(
        _In_                            LPVOID                  lpParam
){
        KEEP_IN_REGISTER LPGPIO lpGpio;
        KEEP_IN_REGISTER ULONG ulOnTime, ulOffTime;
        KEEP_IN_REGISTER BYTE bPin;
        KEEP_IN_REGISTER LPBYTE lpbyDutyCycle;

        lpGpio = (LPGPIO)lpParam;
        bPin = lpGpio->bPin;
        lpbyDutyCycle = &(lpGpio->bDutyCycle);

        INFINITE_LOOP()
        {
                ulOnTime = (ULONG)InterlockedAcquire(lpbyDutyCycle);
                ulOffTime = (0xFF - ulOnTime) << 0x03;
                ulOnTime <<= 0x03;
                digitalWrite(bPin, HIGH);
                MicroSleep(ulOnTime);
                digitalWrite(bPin, LOW);
                MicroSleep(ulOffTime);
        }

        return 0;
}




_Call_Back_
_Success_(return != FALSE, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_)))
INTERNAL_OPERATION
BOOL
__DestroyGpioPin(
        _In_            HDERIVATIVE             hDerivative,
        _In_Opt_        ULONG                   ulFlags
){
        UNREFERENCED_PARAMETER(ulFlags);
        EXIT_IF_UNLIKELY_NULL(hDerivative, FALSE);
        LPGPIO lpGpio;
        lpGpio = (LPGPIO)hDerivative;
        
        if (lpGpio->bMode == GPIO_MODE_PWM)
        { KillThread(lpGpio->hPwmThread); }
        DeregisterHandle(hHandleTable, lpGpio->hThis);
        DestroyHandle(lpGpio->hPwmThread);
        DestroyHandle(lpGpio->hSemaphore);
        WriteGpio(lpGpio->hThis, FALSE, NULLPTR, NULL);
        FreeMemory(lpGpio);

        return TRUE;
}




_Call_Back_
_Success_(return != NULL, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_)))
INTERNAL_OPERATION
LPVOID
__WriteOverride(
        _In_                    HANDLE                  hFile,
        _In_                    LPVOID                  lpData,
        _In_                    ULONGLONG               ullSize,
        _In_Opt_                LPVOID                  lpParam,
        _In_Opt_                ULONG                   ulFlags
){
        UNREFERENCED_PARAMETER(lpParam);
        UNREFERENCED_PARAMETER(ullSize);

        EXIT_IF_UNLIKELY_NULL(hFile, NULLPTR);
        EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hFile), HANDLE_TYPE_DEVICE_GPIO, NULLPTR);

        if (ulFlags & GPIO_INTERLOCKED_OPERATION)
        {
                if (ulFlags & GPIO_ANALOG_OPERATION)
                { return (LPVOID)(UARCHLONG)WriteGpioAnalog(hFile, (ULONG)(UARCHLONG)lpData, NULLPTR, GPIO_INTERLOCKED_OPERATION); }
                else if (ulFlags & GPIO_PWM_OPERATION)
                { return (LPVOID)(UARCHLONG)WriteGpioPwm(hFile, (BYTE)(UARCHLONG)lpData, NULLPTR, GPIO_INTERLOCKED_OPERATION); }
                /* Fall Through */ 
                return (LPVOID)(UARCHLONG)WriteGpio(hFile, (BOOL)(UARCHLONG)lpData, NULLPTR, GPIO_INTERLOCKED_OPERATION);
        }
        else
        {
                if (ulFlags & GPIO_ANALOG_OPERATION)
                { return (LPVOID)(UARCHLONG)WriteGpioAnalog(hFile, (ULONG)(UARCHLONG)lpData, NULLPTR, NULL); }
                else if (ulFlags & GPIO_PWM_OPERATION)
                { return (LPVOID)(UARCHLONG)WriteGpioPwm(hFile, (BYTE)(UARCHLONG)lpData, NULLPTR, NULL); }
                /* Fall Through */ 
                return (LPVOID)(UARCHLONG)WriteGpio(hFile, (BOOL)(UARCHLONG)lpData, NULLPTR, NULL);
        }

        return NULLPTR;
}




/* Maybe make the function return in lpBuffer like the rest? Not sure. */

_Call_Back_
_Success_(return != NULL, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_)))
INTERNAL_OPERATION
LPVOID
__ReadOverride(
        _In_                    HANDLE                  hFile,
        _In_                    LPVOID                  lpBuffer,
        _In_                    ULONGLONG               ullSize,
        _In_Opt_                LPVOID                  lpParam,
        _In_Opt_                ULONG                   ulFlags
){
        UNREFERENCED_PARAMETER(lpParam);
        UNREFERENCED_PARAMETER(ullSize);
        UNREFERENCED_PARAMETER(lpBuffer);

        EXIT_IF_UNLIKELY_NULL(hFile, NULLPTR);
        EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hFile), HANDLE_TYPE_DEVICE_GPIO, NULLPTR);

        if (ulFlags & GPIO_INTERLOCKED_OPERATION)
        {
                if (ulFlags & GPIO_ANALOG_OPERATION)
                { return (LPVOID)(UARCHLONG)ReadGpioAnalog(hFile, GPIO_INTERLOCKED_OPERATION); }

                return (LPVOID)(UARCHLONG)ReadGpio(hFile, NULLPTR, GPIO_INTERLOCKED_OPERATION);
        }
        else
        {
                if (ulFlags & GPIO_ANALOG_OPERATION)
                { return (LPVOID)(UARCHLONG)ReadGpioAnalog(hFile, NULL); }
 
                return (LPVOID)(UARCHLONG)ReadGpio(hFile, NULLPTR, NULL);
        }

        return NULLPTR;
}




_Call_Back_
_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__GpioPinTableLookup(
        _In_            HDERIVATIVE             hLookingFor,
        _In_            HDERIVATIVE             hComparand
){
        EXIT_IF_NULL(hComparand, FALSE);

        #ifdef __LP64__
        ULONGLONG ulPin;
        ulPin = (ULONGLONG)hLookingFor;
        #else
        ULONG ulPin;
        ulPin = (ULONG)hLookingFor;
        #endif

        LPGPIO lpGpio;
        lpGpio = (LPGPIO)hComparand;        

        if ((BYTE)ulPin == lpGpio->bPin)
        { return TRUE; }
        return FALSE; 
}



_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HANDLE
OpenGpioPinAndSpecifyMode(
        _In_                            ULONG                   ulPin,
        _In_                            ULONG                   ulMode,
        _In_                            ULONG                   ulPullMode
){
        SET_ERROR_AND_EXIT_IF_NULL(ulPin, ERROR_GPIO_REQUIRED_ARGUMENT_WAS_NULL, NULLPTR);
        SET_ERROR_AND_EXIT_IF_NULL(ulMode, ERROR_GPIO_REQUIRED_ARGUMENT_WAS_NULL, NULLPTR);
        HANDLE hGpioPin;
        LPGPIO lpGpio;
        hGpioPin = NULLPTR;

        if (UNLIKELY(FALSE == bInitWiringPi)) __InitWiringPi();
        if (UNLIKELY(NULLPTR == hHandleTable)) 
        { EXIT_IF_UNLIKELY_NOT_VALUE(__CreateHandleTable(), TRUE, NULLPTR); }

        #ifdef __LP64__
        hGpioPin = GrabHandleByDerivative(hHandleTable, (LPVOID)(ULONGLONG)ulPin, NULL, __GpioPinTableLookup);
        #else
        hGpioPin = GrabHandleByDerivative(hHandleTable, (LPVOID)ulPin, NULL, __GpioPinTableLookup);
        #endif

        if (NULLPTR != hGpioPin)
        { return hGpioPin; }
        else 
        { hGpioPin = NULLPTR; }

        lpGpio = LocalAllocAndZero(sizeof(GPIO));
        SET_ERROR_AND_EXIT_IF_NULL(lpGpio, ERROR_GPIO_DATA_COULD_NOT_BE_ALLOC, NULLPTR);

        lpGpio->bPin = (BYTE)ulPin;
        lpGpio->bMode = (BYTE)ulMode;
        lpGpio->bState = (BOOL)digitalRead(lpGpio->bPin);
        lpGpio->bPullMode = (BYTE)ulPullMode;

        lpGpio->hSemaphore = CreateSemaphore(1);
	if (NULL_OBJECT == lpGpio->hSemaphore)
	{
		SetLastGlobalError(ERROR_GPIO_COULD_NOT_CREATE_SPIN_LOCK);
		FreeMemory(lpGpio);
		return NULL_OBJECT;
	}

        WaitForSingleObject(lpGpio->hSemaphore, INFINITE_WAIT_TIME);        


        switch (ulMode)
        {

                case GPIO_MODE_INPUT:
                {
                        pinMode(ulPin, INPUT);
                        break;
                }

                case GPIO_MODE_OUTPUT:
                {
                        pinMode(ulPin, OUTPUT);       
                        break;
                }

                case GPIO_MODE_PWM:
                {       
                        pinMode(ulPin, OUTPUT);
                        lpGpio->hPwmThread = CreateThread(__PwmThreadProc, (LPVOID)lpGpio, NULL);
                        break;
                }

                case GPIO_MODE_CLOCK:
                {
                        pinMode(ulPin, GPIO_CLOCK);
                        break;
                }

        }

        switch (ulPullMode)
        {

                case GPIO_PULL_MODE_OFF:
                {
                        pullUpDnControl((ULONG)lpGpio->bPin, PUD_OFF);
                        break;
                }

                case GPIO_PULL_MODE_DOWN:
                {
                        pullUpDnControl((ULONG)lpGpio->bPin, PUD_DOWN);
                        break;
                }

                case GPIO_PULL_MODE_UP:
                {
                        pullUpDnControl((ULONG)lpGpio->bPin, PUD_UP);
                        break;
                }

        }

        hGpioPin = CreateHandleWithSingleInheritor(
                        lpGpio,
                        &(lpGpio->hThis),
                        HANDLE_TYPE_DEVICE_GPIO,
                        __DestroyGpioPin,
                        NULL,
                        NULLPTR,
                        NULL,
                        NULLPTR,
                        NULL,
                        &(lpGpio->ullId),
                        NULL
        );

        EXIT_IF_UNLIKELY_NULL(hGpioPin, NULLPTR);

        SetHandleReadAndWriteProcs(hGpioPin, __ReadOverride, __WriteOverride);
        RegisterHandle(hHandleTable, hGpioPin);

        ReleaseSingleObject(lpGpio->hSemaphore);
        return hGpioPin;
}




_Success_(return != MAX_ULONG, _Non_Locking_)
PODNET_API
ULONG
GetGpioPinMode(
        _In_                            HANDLE                  hPin
){
        EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hPin), HANDLE_TYPE_DEVICE_GPIO, MAX_ULONG);
        LPGPIO lpGpio;
        lpGpio = (LPGPIO)GetHandleDerivative(hPin);
        SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(lpGpio, ERROR_GPIO_DATA_WAS_NULL, MAX_ULONG);
        return lpGpio->bMode;
}




_Success_(return != MAX_ULONG, _Non_Locking_)
PODNET_API
ULONG
GetGpioPullMode(
        _In_                            HANDLE                  hPin
){
        EXIT_IF_NULL(hPin, MAX_ULONG);
        EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hPin), HANDLE_TYPE_DEVICE_GPIO, MAX_ULONG);
        LPGPIO lpGpio;
        lpGpio = (LPGPIO)GetHandleDerivative(hPin);
        return (ULONG)lpGpio->bPullMode;
}




_Success_(return != MAX_ULONG, _Non_Locking_)
PODNET_API
HANDLE
GetGpioPinHandle(
        _In_                            ULONG                   ulPin
){
        #ifdef __LP64__
        return GrabHandleByDerivative(hHandleTable, (LPVOID)(ULONGLONG)ulPin, NULL, __GpioPinTableLookup);
        #else
        return GrabHandleByDerivative(hHandleTable, (LPVOID)ulPin, NULL, __GpioPinTableLookup);
        #endif 
}




_Success_(return != FALSE, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_)))
PODNET_API
BOOL
GpioSpecifyPinMode(
        _In_                            HANDLE                  hPin,
        _In_                            ULONG                   ulMode,
        _In_                            ULONG                   ulPullMode
){
        EXIT_IF_NULL(hPin, FALSE);
        EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hPin), HANDLE_TYPE_DEVICE_GPIO, FALSE);
        LPGPIO lpGpio;
        lpGpio = (LPGPIO)GetHandleDerivative(hPin);
        EXIT_IF_NULL(lpGpio, FALSE);

        WaitForSingleObject(lpGpio->hSemaphore, INFINITE_WAIT_TIME);

        if (GPIO_MODE_PWM == lpGpio->bMode)
        { KillThread(lpGpio->hPwmThread); }

        lpGpio->bMode = (BYTE)ulMode,
        lpGpio->bPullMode = (BYTE)ulPullMode;

        switch (ulMode)
        {

                case GPIO_MODE_INPUT:
                {
                        pinMode((ULONG)lpGpio->bPin, INPUT);
                        break;
                }

                case GPIO_MODE_OUTPUT:
                {
                        pinMode((ULONG)lpGpio->bPin, OUTPUT);
                        break;
                }

                case GPIO_MODE_PWM:
                {
                        pinMode((ULONG)lpGpio->bPin, OUTPUT);
                        lpGpio->hPwmThread = CreateThread(__PwmThreadProc, (LPVOID)lpGpio, NULL);
                        break;
                }

                case GPIO_MODE_CLOCK:
                {
                        pinMode((ULONG)lpGpio->bPin, GPIO_CLOCK);
                        break;
                }

        }

        ReleaseSingleObject(lpGpio->hSemaphore);

        return TRUE;
}




_Success_(return != FALSE, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_)))
PODNET_API
BOOL
GpioChangePullMode(
        _In_                            HANDLE                  hPin,
        _In_                            ULONG                   ulPullMode
){
        EXIT_IF_NULL(hPin, FALSE);
        EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hPin), HANDLE_TYPE_DEVICE_GPIO, FALSE);
        LPGPIO lpGpio;
        lpGpio = (LPGPIO)GetHandleDerivative(hPin);
        EXIT_IF_NULL(lpGpio, FALSE);

        WaitForSingleObject(lpGpio->hSemaphore, INFINITE_WAIT_TIME);

        lpGpio->bPullMode = ulPullMode;

        switch (ulPullMode)
        {

                case GPIO_PULL_MODE_OFF:
                {
                        pullUpDnControl((ULONG)lpGpio->bPin, PUD_OFF);
                        break;
                }

                case GPIO_PULL_MODE_DOWN:
                {
                        pullUpDnControl((ULONG)lpGpio->bPin, PUD_DOWN);
                        break;
                }

                case GPIO_PULL_MODE_UP:
                {
                        pullUpDnControl((ULONG)lpGpio->bPin, PUD_UP);
                        break;
                }

        }

        ReleaseSingleObject(lpGpio->hSemaphore);
        return TRUE;
}



_Success_(return != FALSE, _Interlocked_When_(ulFlags & GPIO_INTERLOCKED_OPERATION, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_))))
PODNET_API
BOOL
WriteGpio(
        _In_                            HANDLE                  hPin,
        _In_                            BOOL                    bValue,
        _Reserved_Must_Be_Null_         LPVOID                  lpReserved,
        _In_Opt_                        ULONG                   ulFlags
){
        UNREFERENCED_PARAMETER(lpReserved);
        EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hPin), HANDLE_TYPE_DEVICE_GPIO, FALSE);
        LPGPIO lpGpio;
        lpGpio = (LPGPIO)GetHandleDerivative(hPin);
        SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(lpGpio, ERROR_GPIO_DATA_WAS_NULL, FALSE);

        if (ulFlags & GPIO_INTERLOCKED_OPERATION)
        { WaitForSingleObject(lpGpio->hSemaphore, INFINITE_WAIT_TIME); }

        digitalWrite(lpGpio->bPin, (LONG)bValue);

        if (ulFlags & GPIO_INTERLOCKED_OPERATION)
        { ReleaseSingleObject(lpGpio->hSemaphore); }

        return TRUE;
}




_Success_(return != FALSE, _Interlocked_When_(ulFlags & GPIO_INTERLOCKED_OPERATION, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_))))
PODNET_API
BOOL
WriteGpioAnalog(
        _In_                            HANDLE                  hPin,
        _In_                            ULONG                   ulValue,
        _Reserved_Must_Be_Null_         LPVOID                  lpReserved,
        _In_Opt_                        ULONG                   ulFlags
){
        UNREFERENCED_PARAMETER(lpReserved);
        EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hPin), HANDLE_TYPE_DEVICE_GPIO, FALSE);
        LPGPIO lpGpio;
        lpGpio = (LPGPIO)GetHandleDerivative(hPin);
        SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(lpGpio, ERROR_GPIO_DATA_WAS_NULL, FALSE);

        if (ulFlags & GPIO_INTERLOCKED_OPERATION)
        { WaitForSingleObject(lpGpio->hSemaphore, INFINITE_WAIT_TIME); }

        analogWrite(lpGpio->bPin, ulValue);

        if (ulFlags & GPIO_INTERLOCKED_OPERATION)
        { ReleaseSingleObject(lpGpio->hSemaphore); }

        return TRUE;
}




_Success_(return != FALSE, _Interlocked_When_(ulFlags & GPIO_INTERLOCKED_OPERATION, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_))))
PODNET_API
BOOL
WriteGpioPwm(
        _In_                            HANDLE                  hPin,
        _In_                            BYTE                    bDutyCycle,
        _Reserved_Must_Be_Null_         LPVOID                  lpReserved,
        _In_Opt_                        ULONG                   ulFlags
){
        UNREFERENCED_PARAMETER(lpReserved);
        UNREFERENCED_PARAMETER(ulFlags);
        EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hPin), HANDLE_TYPE_DEVICE_GPIO, FALSE);
        LPGPIO lpGpio;
        lpGpio = (LPGPIO)GetHandleDerivative(hPin);
        SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(lpGpio, ERROR_GPIO_DATA_WAS_NULL, FALSE);

        WaitForSingleObject(lpGpio->hSemaphore, INFINITE_WAIT_TIME);
        lpGpio->bDutyCycle = bDutyCycle;
        ReleaseSingleObject(lpGpio->hSemaphore); 

        return TRUE;
}




_Success_(return != MAX_UBYTE, _Interlocked_When_(ulFlags & GPIO_INTERLOCKED_OPERATION, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_))))
PODNET_API
BOOL
ReadGpio(
        _In_                            HANDLE                  hPin,
        _Reserved_Must_Be_Null_         LPVOID                  lpReserved,
        _In_Opt_                        ULONG                   ulFlags
){
        UNREFERENCED_PARAMETER(lpReserved);
        EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hPin), HANDLE_TYPE_DEVICE_GPIO, FALSE);
        LPGPIO lpGpio;
        BOOL bRet;
        lpGpio = (LPGPIO)GetHandleDerivative(hPin);
        SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(lpGpio, ERROR_GPIO_DATA_WAS_NULL, FALSE);

        if (ulFlags & GPIO_INTERLOCKED_OPERATION)
        { WaitForSingleObject(lpGpio->hSemaphore, INFINITE_WAIT_TIME); }

        bRet = (BOOL)digitalRead(lpGpio->bPin);

        if (ulFlags & GPIO_INTERLOCKED_OPERATION)
        { ReleaseSingleObject(lpGpio->hSemaphore); }

        return bRet;
}




_Success_(return != MAX_ULONG, _Interlocked_When_(ulFlags & GPIO_INTERLOCKED_OPERATION, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_))))
PODNET_API
ULONG
ReadGpioAnalog(
        _In_                            HANDLE                  hPin,
        _In_Opt_                        ULONG                   ulFlags
){
        EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hPin), HANDLE_TYPE_DEVICE_GPIO, FALSE);
        LPGPIO lpGpio;
        ULONG ulRet;
        lpGpio = (LPGPIO)GetHandleDerivative(hPin);
        SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(lpGpio, ERROR_GPIO_DATA_WAS_NULL, FALSE);

        if (ulFlags & GPIO_INTERLOCKED_OPERATION)
        { WaitForSingleObject(lpGpio->hSemaphore, INFINITE_WAIT_TIME); }

        ulRet = analogRead(lpGpio->bPin);

        if (ulFlags & GPIO_INTERLOCKED_OPERATION)
        { ReleaseSingleObject(lpGpio->hSemaphore); }

        return ulRet;
}


