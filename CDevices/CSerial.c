/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CSerial.h"
#include "../CHandle/CHandleTable.h"



typedef struct __SERIAL
{
        INHERITS_FROM_HANDLE();
        LONG            lFileDesriptor;
        ULONG           ulBaudRate;
        LPSTR           lpszDevice;
        HANDLE          hSemaphore;
} SERIAL, *LPSERIAL;


GLOBAL_VARIABLE DONT_OPTIMIZE_OUT HANDLE hHandleTable = NULLPTR;


typedef struct __WRITESERIAL_PROMISE
{
        LPSERIAL        lpSerial;
        LPBYTE          lpbyData;
        ULONGLONG       ullSize;
} WRITESERIAL_PROMISE, *LPWRITESERIAL_PROMISE;




typedef struct __READSERIAL_PROMISE
{
        LPSERIAL        lpSerial;
        ULONGLONG       ullSize;
} READSERIAL_PROMISE, *LPREADSERIAL_PROMISE;




_Promise_Proc_
_Success_(return == sizeof(BOOL), _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_)))
INTERNAL_OPERATION
UARCHLONG
__WriteSerialAsyncPromiseProc(
        _In_                    LPVOID                  lpParam,
        _In_Out_                DLPVOID                 dlpOut
){
        SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(lpParam, ERROR_PROMISE_NOT_SUPPLIED_WITH_DATA, 0);
        SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(dlpOut, ERROR_PROMISE_NOT_SUPPLIED_WITH_ALLOCABLE_BUFFER, 0);

        LPWRITESERIAL_PROMISE lpwspProm;
        lpwspProm = (LPWRITESERIAL_PROMISE)lpParam;
        *dlpOut = LocalAlloc(sizeof(BOOL));
        SET_ERROR_AND_EXIT_IF_NULL(*dlpOut, ERROR_PROMISE_COULD_NOT_ALLOC_BUFFER, 0);

        WaitForSingleObject(lpwspProm->lpSerial->hSemaphore, INFINITE_WAIT_TIME);

        **(DLPBOOL)(dlpOut) = 
                        (0 == write(lpwspProm->lpSerial->lFileDesriptor, 
                                        lpwspProm->lpbyData,
                                        lpwspProm->ullSize
                        )) ? TRUE : FALSE;

        ReleaseSingleObject(lpwspProm->lpSerial->hSemaphore);

        FreeMemory(lpwspProm);
        return sizeof(BOOL);
}




_Promise_Proc_
_Success_(return != NULL, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_)))
INTERNAL_OPERATION
UARCHLONG
__ReadSerialAsyncPromiseProc(
        _In_                    LPVOID                  lpParam,
        _In_Out_                DLPVOID                 dlpOut
){
        SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(lpParam, ERROR_PROMISE_NOT_SUPPLIED_WITH_DATA, 0);
        SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(dlpOut, ERROR_PROMISE_NOT_SUPPLIED_WITH_ALLOCABLE_BUFFER, 0);

        LPREADSERIAL_PROMISE lprspProm;
        BOOL bCheck;
        lprspProm = (LPREADSERIAL_PROMISE)lpParam;
        *dlpOut = LocalAlloc(lprspProm->ullSize);
        SET_ERROR_AND_EXIT_IF_NULL(*dlpOut, ERROR_PROMISE_COULD_NOT_ALLOC_BUFFER, 0);

        WaitForSingleObject(lprspProm->lpSerial->hSemaphore, INFINITE_WAIT_TIME);

        bCheck = (0 == read(
                                lprspProm->lpSerial->lFileDesriptor, 
                                *dlpOut,
                                lprspProm->ullSize
                        )) ? TRUE : FALSE;        

        ReleaseSingleObject(lprspProm->lpSerial->hSemaphore);

        FreeMemory(lprspProm);
        return ((TRUE == bCheck) ? lprspProm->ullSize : NULL);
}




_Success_(return != FALSE, ...)
INTERNAL_OPERATION
BOOL
__CreateHandleTable(
        VOID
){
        if (NULLPTR == hHandleTable)
        { 
                hHandleTable = CreateHandleTable(0x04, NULLPTR, HANDLE_TABLE_HIGH_PERFORMANCE);
                EXIT_IF_NULL(hHandleTable, FALSE);
        }
        return TRUE;
}



_Call_Back_
_Success_(return != FALSE, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_)))
INTERNAL_OPERATION
BOOL 
__DestroySerial(
        _In_                    HDERIVATIVE             hDerivative,
        _In_                    ULONG                   ulFlags
){
        UNREFERENCED_PARAMETER(ulFlags);
        EXIT_IF_UNLIKELY_NULL(hDerivative, FALSE);
        LPSERIAL lpSerial;
        lpSerial = (LPSERIAL)hDerivative;

        WaitForSingleObject(lpSerial->hSemaphore, INFINITE_WAIT_TIME);

        close(lpSerial->lFileDesriptor);
        DestroyHandle(lpSerial->hSemaphore);
        DeregisterHandle(hHandleTable, lpSerial->hThis);
        FreeMemory(lpSerial);

        return TRUE;
}




_Call_Back_
_Success_(return != NULL, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_)))
INTERNAL_OPERATION
LPVOID
__WriteOverride(
        _In_                    HANDLE                  hFile,
        _In_                    LPVOID                  lpData,
        _In_                    ULONGLONG               ullSize,
        _In_Opt_                LPVOID                  lpParam,
        _In_Opt_                ULONG                   ulFlags
){
        UNREFERENCED_PARAMETER(lpParam);
        
        EXIT_IF_UNLIKELY_NULL(hFile, FALSE);
        EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hFile), HANDLE_TYPE_DEVICE_SERIAL, FALSE);
        LPSERIAL lpSerial;
        lpSerial = (LPSERIAL)GetHandleDerivative(hFile);
        EXIT_IF_UNLIKELY_NULL(lpSerial, FALSE);

        if (ulFlags & WRITE_ASYNC)
        { return WriteSerialAsync(hFile, (LPBYTE)lpData, ullSize, NULL); }
        else
        { 
                #ifdef __LP64__
                return (LPVOID)(ULONGLONG)WriteSerial(hFile, (LPBYTE)lpData, ullSize, NULL);
                #else
                return (LPVOID)(ULONG)WriteSerial(hFile, (LPBYTE)lpData, ullSize, NULL);
                #endif
        }

        return NULL;
}




_Call_Back_
_Success_(return != NULL, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_)))
INTERNAL_OPERATION
LPVOID
__ReadOverride(
        _In_                    HANDLE                  hFile,
        _In_                    LPVOID                  lpBuffer,
        _In_                    ULONGLONG               ullSize,
        _In_Opt_                LPVOID                  lpParam,
        _In_Opt_                ULONG                   ulFlags
){

        UNREFERENCED_PARAMETER(lpParam);
        
        EXIT_IF_UNLIKELY_NULL(hFile, FALSE);
        EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hFile), HANDLE_TYPE_DEVICE_SERIAL, FALSE);
        LPSERIAL lpSerial;
        lpSerial = (LPSERIAL)GetHandleDerivative(hFile);
        EXIT_IF_UNLIKELY_NULL(lpSerial, FALSE);

        if (ulFlags & READ_ASYNC)
        { return ReadSerialAsync(hFile, ullSize, ulFlags) ;}
        else
        {
                #ifdef __LP64__
                return (LPVOID)(ULONGLONG)ReadSerial(hFile, lpBuffer, ullSize, NULL);
                #else
                return (LPVOID)(ULONG)ReadSerial(hFile, lpBuffer, ullSize, NULL);
                #endif
        }

        return NULL;
}




_Call_Back_
_Success_(return != FALSE, ...)
INTERNAL_OPERATION
BOOL
__SerialTableLookup(
        _In_                    HDERIVATIVE             hLookingFor,
        _In_                    HDERIVATIVE             hComparand
){
        EXIT_IF_NULL(hComparand, FALSE);

        LPSTR lpszDevice;
        LPSERIAL lpSerial;
        lpSerial = (LPSERIAL)hComparand;

        lpszDevice = (LPSTR)hLookingFor;
        return (0 == StringCompare(lpszDevice, lpSerial->lpszDevice)) ? TRUE : FALSE;
        return FALSE;
}



_Success_(return != -1, ...)
INTERNAL_OPERATION
LONG
__SerialOpen(
        _In_                    LPCSTR                  lpcszDevice,
        _In_                    ULONG                   ulBaud
){
        struct termios termOptions;
        speed_t spBaud;
        LONG lStatus, lFileDesriptor;


        switch (ulBaud)
        {

                case 50U:
                {	
                        spBaud = B50; 
                        break;
                }

                case 75U:
                {	
                        spBaud = B75; 
                        break;
                }

                case 110U:	
                { 
                        spBaud = B110;
                        break;
                }

                case 134U:
                {
                        spBaud = B134;
                        break;
                }

                case 150:
                { 
                        spBaud = B150; 
                        break;
                }

                case 200U:
                { 
                        spBaud = B200;
                        break;
                }

                case 300U: 
                { 
                        spBaud = B300; 
                        break;
                }

                case 600U:
                {
                        spBaud = B600; 
                        break;
                }

                case 1200U: 
                {      
                        spBaud = B1200; 
                        break;
                }

                case 1800U:
                {      
                        spBaud = B1800; 
                        break;
                }

                case 2400U:
                {
                        spBaud = B2400; 
                        break;
                }

                case 4800U:
                { 
                        spBaud = B4800; 
                        break;
                }

                case 9600U:
                { 
                        spBaud = B9600; 
                        break;
                }

                case 19200U: 
                {
                        spBaud = B19200; 
                        break;
                }
                
                case 38400U:
                { 
                        spBaud = B38400; 
                        break;
                }

                case 57600U: 
                { 
                        spBaud = B57600;
                        break;
                }

                case 115200:
                {
                        spBaud = B115200; 
                        break;
                }

                case 230400U:
                { 
                        spBaud = B230400; 
                        break;
                }

                case 460800U:
                {
                        spBaud = B460800; 
                        break;
                }

                case 500000U:
                {
                        spBaud = B500000; 
                        break;
                }

                case 576000U:
                {
                        spBaud = B576000; 
                        break;
                }
                
                case 921600U:
                {
                        spBaud = B921600; 
                        break;
                }

                case 1000000U: 
                {
                        spBaud = B1000000; 
                        break;
                }

                case 1152000U:
                { 
                        spBaud = B1152000; 
                        break;
                }

                case 1500000U:
                {
                        spBaud = B1500000; 
                        break;
                }

                case 2000000U:
                {
                        spBaud = B2000000; 
                        break;
                }

                case 2500000U:
                {
                        spBaud = B2500000; 
                        break;
                }

                case 3000000U:
                {
                        spBaud = B3000000; 
                        break;
                }

                case 3500000U:
                {
                        spBaud = B3500000; 
                        break;
                }

                case 4000000U:
                {
                        spBaud = B4000000; 
                        break;
                }

                default: return -1;
        }

        lFileDesriptor = open(lpcszDevice, O_RDWR | O_NOCTTY | O_NDELAY | O_NONBLOCK);
        EXIT_IF_UNLIKELY_VALUE(lFileDesriptor, -1, -1);
        
        fcntl(lFileDesriptor, F_SETFL, O_RDWR);
        tcgetattr(lFileDesriptor, &termOptions);
        cfmakeraw(&termOptions);
        cfsetispeed(&termOptions, spBaud);
        cfsetospeed(&termOptions, spBaud);
        
        termOptions.c_cflag |= (CLOCAL | CREAD);
        termOptions.c_cflag &= ~PARENB;
        termOptions.c_cflag &= ~CSTOPB;
        termOptions.c_cflag &= ~CSIZE;
        termOptions.c_cflag |= CS8;
        termOptions.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);
        termOptions.c_oflag &= ~OPOST;
        termOptions.c_cc[VMIN] = 0;
        termOptions.c_cc[VTIME] = 100;   /* Ten seconds (100 decisecs) */

        tcsetattr(lFileDesriptor, TCSANOW, &termOptions);
        ioctl(lFileDesriptor, TIOCMGET, &lStatus);

        lStatus |= TIOCM_DTR;
        lStatus |= TIOCM_RTS;

        ioctl(lFileDesriptor, TIOCMSET, &lStatus);

        MicroSleep(TEN * TEN_THOUSAND); 

        return lFileDesriptor;
}


_Success_(return != NULLPTR, ...)
PODNET_API
HANDLE
CreateSerialInterface(
        _In_                    LPCSTR                  lpcszDevice,
        _In_                    ULONG                   ulBaud,
        _Reserved_Must_Be_Null_ LPVOID                  lpReserved,
        _In_Opt_                ULONG                   ulFlags
){
        UNREFERENCED_PARAMETER(lpReserved);
        UNREFERENCED_PARAMETER(ulFlags);
        HANDLE hSerial;
        LPSERIAL lpSerial;
        hSerial = NULLPTR;

        if (UNLIKELY(NULLPTR == hHandleTable))
        { EXIT_IF_UNLIKELY_NOT_VALUE(__CreateHandleTable(), TRUE, NULLPTR); }

        hSerial = GrabHandleByDerivative(hHandleTable, (LPVOID)lpcszDevice, NULL, __SerialTableLookup);

        if (NULLPTR != hSerial)
        { return hSerial; }
        else
        { hSerial = NULLPTR; }

        lpSerial = LocalAllocAndZero(sizeof(SERIAL));
	/* cppcheck-suppress memleak */
        EXIT_IF_UNLIKELY_NULL(lpSerial, NULLPTR);

        lpSerial->hSemaphore = CreateSemaphore(1U);
	if (NULL_OBJECT == lpSerial->hSemaphore)
	{
		FreeMemory(lpSerial);
		return NULL_OBJECT;
	}

        lpSerial->ulBaudRate = ulBaud;
        lpSerial->lpszDevice = (LPSTR)lpcszDevice;
        lpSerial->lFileDesriptor = __SerialOpen(lpcszDevice, ulBaud);
	if (-1 == lpSerial->lFileDesriptor)
	{
		DestroyObject(lpSerial->hSemaphore);
		FreeMemory(lpSerial);
		return NULL_OBJECT;
	}

        hSerial = CreateHandleWithSingleInheritor(
                        lpSerial,
                        &(lpSerial->hThis),
                        HANDLE_TYPE_DEVICE_SERIAL,
                        __DestroySerial,
                        NULL,
                        NULLPTR,
                        NULL,
                        NULLPTR,
                        NULL,
                        &(lpSerial->ullId),
                        NULL
        );

	if (NULL_OBJECT == hSerial)
	{
		close(lpSerial->lFileDesriptor);
		DestroyObject(lpSerial->hSemaphore);
		FreeMemory(lpSerial);
		return NULL_OBJECT;
	}

        SetHandleReadAndWriteProcs(hSerial, __ReadOverride, __WriteOverride);

        RegisterHandle(hHandleTable, hSerial);
        return hSerial;
}




_Success_(return != NULLPTR, ...)
PODNET_API
HANDLE
GetSerialInterfaceHandle(
        _In_                    LPCSTR                  lpcszDevice
){
        EXIT_IF_UNLIKELY_NULL(lpcszDevice, NULLPTR);
        return GrabHandleByDerivative(hHandleTable, (LPVOID)lpcszDevice, NULL, __SerialTableLookup);
}




_Success_(return != NULL, ...)
PODNET_API
ULONG 
GetSerialInterfaceBaudRate(
        _In_                    HANDLE                  hSerial
){
        EXIT_IF_UNLIKELY_NULL(hSerial, NULL);
        EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hSerial), HANDLE_TYPE_DEVICE_SERIAL, NULL);
        LPSERIAL lpSerial;
        lpSerial = (LPSERIAL)GetHandleDerivative(hSerial);
        EXIT_IF_UNLIKELY_NULL(lpSerial, NULL);

        return lpSerial->ulBaudRate;
}




_Success_(return != FALSE, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_)))
PODNET_API
BOOL
WriteSerialCharacter(
        _In_                    HANDLE                  hSerial,
        _In_                    CHAR                    cValue
){
        EXIT_IF_UNLIKELY_NULL(hSerial, FALSE);
        EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hSerial), HANDLE_TYPE_DEVICE_SERIAL, FALSE);
        LPSERIAL lpSerial;
        BOOL bRet;
        lpSerial = (LPSERIAL)GetHandleDerivative(hSerial);
        EXIT_IF_UNLIKELY_NULL(lpSerial, FALSE);

        WaitForSingleObject(lpSerial->hSemaphore, INFINITE_WAIT_TIME);

        bRet = (0 == write(lpSerial->lFileDesriptor, &cValue, 1)) ? TRUE : FALSE;

        ReleaseSingleObject(lpSerial->hSemaphore);
        return bRet;
}




_Success_(return != FALSE, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_)))
PODNET_API
BOOL
WriteSerialString(
        _In_                    HANDLE                  hSerial,
        _In_                    LPCSTR                  lpcszString
){
        EXIT_IF_UNLIKELY_NULL(hSerial, FALSE);
        EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hSerial), HANDLE_TYPE_DEVICE_SERIAL, FALSE);
        LPSERIAL lpSerial;
        BOOL bRet;
        lpSerial = (LPSERIAL)GetHandleDerivative(hSerial);
        EXIT_IF_UNLIKELY_NULL(lpSerial, FALSE);

        WaitForSingleObject(lpSerial->hSemaphore, INFINITE_WAIT_TIME);

        bRet = (0 == write(lpSerial->lFileDesriptor, lpcszString, StringLength(lpcszString)-1ULL)) ? TRUE : FALSE;

        ReleaseSingleObject(lpSerial->hSemaphore);
        return bRet;
}




_Success_(return != FALSE, _Acquires_Locks_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_)))
PODNET_API
BOOL
WriteSerial(
        _In_                    HANDLE                  hSerial,
        _In_                    LPBYTE                  lpbyData,
        _In_                    ULONGLONG               ullSize,
        _In_                    ULONG                   ulFlags 
){
        UNREFERENCED_PARAMETER(ulFlags);
        EXIT_IF_UNLIKELY_NULL(hSerial, FALSE);
        EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hSerial), HANDLE_TYPE_DEVICE_SERIAL, FALSE);
        LPSERIAL lpSerial;
        BOOL bRet;
        lpSerial = (LPSERIAL)GetHandleDerivative(hSerial);
        EXIT_IF_UNLIKELY_NULL(lpSerial, FALSE);

        WaitForSingleObject(lpSerial->hSemaphore, INFINITE_WAIT_TIME);

        bRet = (0 == write(lpSerial->lFileDesriptor, lpbyData, ullSize)) ? TRUE : FALSE;

        ReleaseSingleObject(lpSerial->hSemaphore);
        return bRet;
}




/* Returns HANDLE to Promise */

_Spawns_New_Thread_Async_And_Locks_(hSerial, _Lock_Kind_Semaphore_)
_Success_(return != FALSE, _Acquires_Locks_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_)))
PODNET_API
HANDLE 
WriteSerialAsync(
        _In_                    HANDLE                  hSerial,
        _In_                    LPBYTE                  lpbyData,
        _In_                    ULONGLONG               ullSize,
        _In_                    ULONG                   ulFlags 
){
        UNREFERENCED_PARAMETER(ulFlags);
        EXIT_IF_UNLIKELY_NULL(hSerial, NULLPTR);
        EXIT_IF_UNLIKELY_NULL(lpbyData, NULLPTR);
        EXIT_IF_UNLIKELY_NULL(ullSize, NULLPTR);
        HANDLE hPromise;
        LPWRITESERIAL_PROMISE lpwspProm;
        lpwspProm = (LPWRITESERIAL_PROMISE)LocalAllocAndZero(sizeof(WRITESERIAL_PROMISE));
	/* cppcheck-suppress memleak */
        EXIT_IF_UNLIKELY_NULL(lpwspProm, NULLPTR);

        lpwspProm->lpSerial = (LPSERIAL)GetHandleDerivative(hSerial);
        lpwspProm->lpbyData = lpbyData;
        lpwspProm->ullSize = ullSize;

        hPromise = CreatePromise(__WriteSerialAsyncPromiseProc, (LPVOID)lpwspProm, NULLPTR, NULLPTR, NULL);
        return hPromise;
}




_Success_(return != NULL, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_)))
PODNET_API
ULONG
SerialHasNewData(
        _In_                    HANDLE                  hSerial
){
        EXIT_IF_UNLIKELY_NULL(hSerial, FALSE);
        EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hSerial), HANDLE_TYPE_DEVICE_SERIAL, FALSE);
        LPSERIAL lpSerial;
        ULONG ulSize;
        lpSerial = (LPSERIAL)GetHandleDerivative(hSerial);
        EXIT_IF_UNLIKELY_NULL(lpSerial, FALSE);

        WaitForSingleObject(lpSerial->hSemaphore, INFINITE_WAIT_TIME);

        ioctl(lpSerial->lFileDesriptor, FIONREAD, &ulSize);

        ReleaseSingleObject(lpSerial->hSemaphore);

        return ulSize;
}




_Success_(return != MAX_USHORT, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_)))
PODNET_API
USHORT
ReadSerialNextByte(
        _In_                    HANDLE                  hSerial
){
        EXIT_IF_UNLIKELY_NULL(hSerial, FALSE);
        EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hSerial), HANDLE_TYPE_DEVICE_SERIAL, FALSE);
        LPSERIAL lpSerial;
        CHAR cOut;
        lpSerial = (LPSERIAL)GetHandleDerivative(hSerial);
        EXIT_IF_UNLIKELY_NULL(lpSerial, FALSE);

        WaitForSingleObject(lpSerial->hSemaphore, INFINITE_WAIT_TIME);
        
        read(lpSerial->lFileDesriptor, &cOut, 1ULL);
        
        ReleaseSingleObject(lpSerial->hSemaphore);
        return cOut;
}




_Success_(return != FALSE, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_)))
PODNET_API
BOOL
ReadSerial(
        _In_                    HANDLE                  hSerial,
        _Out_                   LPVOID                  lpDataOut,
        _In_                    ULONGLONG               ullDataSize,
        _In_Opt_                ULONG                   ulFlags
){
        UNREFERENCED_PARAMETER(ulFlags);
        EXIT_IF_UNLIKELY_NULL(hSerial, FALSE);
        EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hSerial), HANDLE_TYPE_DEVICE_SERIAL, FALSE);
        LPSERIAL lpSerial;
        BOOL bRet;
        lpSerial = (LPSERIAL)GetHandleDerivative(hSerial);
        EXIT_IF_UNLIKELY_NULL(lpSerial, FALSE);

        WaitForSingleObject(lpSerial->hSemaphore, INFINITE_WAIT_TIME);
        
        bRet = (0 == read(lpSerial->lFileDesriptor, lpDataOut, ullDataSize)) ? TRUE: FALSE;
        
        ReleaseSingleObject(lpSerial->hSemaphore);
        return bRet;
}




/* Return HANDLE to new Promise */

_Spawns_New_Thread_Async_Call_
_Success_(return != NULLPTR, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_)))
PODNET_API
HANDLE 
ReadSerialAsync(
        _In_                    HANDLE                  hSerial,
        _In_                    ULONGLONG               ullDataSize,
        _In_Opt_                ULONG                   ulFlags
){

        UNREFERENCED_PARAMETER(ulFlags);
        EXIT_IF_UNLIKELY_NULL(hSerial, NULLPTR);
        EXIT_IF_UNLIKELY_NULL(ullDataSize, NULLPTR);
        HANDLE hPromise;
        LPREADSERIAL_PROMISE lprspProm;
        lprspProm = (LPREADSERIAL_PROMISE)LocalAllocAndZero(sizeof(READSERIAL_PROMISE));
	/* cppcheck-suppress memleak */
        EXIT_IF_UNLIKELY_NULL(lprspProm, NULLPTR);

        lprspProm->lpSerial = GetHandleDerivative(hSerial);
        lprspProm->ullSize = ullDataSize;

        hPromise = CreatePromise(__ReadSerialAsyncPromiseProc, (LPVOID)lprspProm, NULLPTR, NULLPTR, NULL);
        return hPromise;        
}




_Success_(return != FALSE, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_)))
PODNET_API
BOOL
FlushSerial(
        _In_                    HANDLE                  hSerial
){
        EXIT_IF_UNLIKELY_NULL(hSerial, FALSE);
        EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hSerial), HANDLE_TYPE_DEVICE_SERIAL, FALSE);
        LPSERIAL lpSerial;
        lpSerial = (LPSERIAL)GetHandleDerivative(hSerial);
        EXIT_IF_UNLIKELY_NULL(lpSerial, FALSE);

        WaitForSingleObject(lpSerial->hSemaphore, INFINITE_WAIT_TIME);

        tcflush(lpSerial->lFileDesriptor, TCIOFLUSH);

        ReleaseSingleObject(lpSerial->hSemaphore);
        return TRUE;
}