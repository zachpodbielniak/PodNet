/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CSPI_H
#define CSPI_H


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../CHandle/CHandle.h"
#include "../CHandle/CHandleTable.h"
#include "../CThread/CThread.h"
#include "../CLock/CSemaphore.h"
#include "../CError/CError.h"
#include "../CSystem/CSystem.h"


_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HANDLE
OpenSpiChannel(
        _In_Range_(0, 1)                BYTE            byChannel,
        _In_Range_(500000, 32000000)    ULONG           ulSpeed,
        _Reserved_Must_Be_Null_         LPVOID          lpReserved
);




_Success_(return != NULL, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_)))
PODNET_API
ULONG 
SpiReadWrite(
        _In_                            HANDLE          hSPI,
        _In_                            LPBYTE          lpbyData,
        _In_                            ULONG           ulSize,
        _Reserved_Must_Be_Null_         LPVOID          lpReserved
);



#endif