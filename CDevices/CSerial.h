/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CSERIAL_H
#define CSERIAL_H


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../CHandle/CHandle.h"
#include "../CThread/CThread.h"
#include "../CLock/CSemaphore.h"
#include "../CError/CError.h"
#include "../CSystem/CSystem.h"



typedef HANDLE                  HSERIAL;


_Success_(return != NULLPTR, ...)
PODNET_API
HANDLE
CreateSerialInterface(
        _In_                    LPCSTR                  lpcszDevice,
        _In_                    ULONG                   ulBaud,
        _Reserved_Must_Be_Null_ LPVOID                  lpReserved,
        _In_Opt_                ULONG                   ulFlags
);




_Success_(return != NULLPTR, ...)
PODNET_API
HANDLE
GetSerialInterfaceHandle(
        _In_                    LPCSTR                  lpcszDevice
);




_Success_(return != NULL, ...)
PODNET_API
ULONG 
GetSerialInterfaceBaudRate(
        _In_                    HANDLE                  hSerial
);




_Success_(return != FALSE, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_)))
PODNET_API
BOOL
WriteSerialCharacter(
        _In_                    HANDLE                  hSerial,
        _In_                    CHAR                    cValue
);




_Success_(return != FALSE, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_)))
PODNET_API
BOOL
WriteSerialString(
        _In_                    HANDLE                  hSerial,
        _In_                    LPCSTR                  lpcszString
);




_Success_(return != FALSE, _Acquires_Locks_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_)))
PODNET_API
BOOL
WriteSerial(
        _In_                    HANDLE                  hSerial,
        _In_                    LPBYTE                  lpbyData,
        _In_                    ULONGLONG               ullSize,
        _In_                    ULONG                   ulFlags 
);




/* Returns HANDLE to Promise */

_Spawns_New_Thread_Async_And_Locks_(hSerial, _Lock_Kind_Semaphore_)
_Success_(return != FALSE, _Acquires_Locks_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_)))
PODNET_API
HANDLE 
WriteSerialAsync(
        _In_                    HANDLE                  hSerial,
        _In_                    LPBYTE                  lpbyData,
        _In_                    ULONGLONG               ullSize,
        _In_                    ULONG                   ulFlags 
);




_Success_(return != NULL, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_)))
PODNET_API
ULONG
SerialHasNewData(
        _In_                    HANDLE                  hSerial
);




_Success_(return != MAX_USHORT, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_)))
PODNET_API
USHORT
ReadSerialNextByte(
        _In_                    HANDLE                  hSerial
);




_Success_(return != FALSE, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_)))
PODNET_API
BOOL
ReadSerial(
        _In_                    HANDLE                  hSerial,
        _Out_                   LPVOID                  lpDataOut,
        _In_                    ULONGLONG               ullDataSize,
        _In_Opt_                ULONG                   ulFlags
);




/* Return HANDLE to new Promise */

_Spawns_New_Thread_Async_And_Locks_(hSerial, _Lock_Kind_Semaphore_)
_Success_(return != NULLPTR, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_)))
PODNET_API
HANDLE 
ReadSerialAsync(
        _In_                    HANDLE                  hSerial,
        _In_                    ULONGLONG               ullDataSize,
        _In_Opt_                ULONG                   ulFlags
);




_Success_(return != FALSE, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_)))
PODNET_API
BOOL
FlushSerial(
        _In_                    HANDLE                  hSerial
);








#endif