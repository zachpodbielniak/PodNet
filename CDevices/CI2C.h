/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CI2C_H
#define CI2C_H


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../CHandle/CHandle.h"
#include "../CThread/CThread.h"
#include "../CLock/CSpinLock.h"
#include "../CError/CError.h"
#include "../CSystem/CSystem.h"




_Success_(return != NULLPTR, ...)
PODNET_API
HANDLE
CreateI2CInterface(
        _In_                    ULONG           ulDeviceId,
        _Reserved_Must_Be_Null_ LPVOID          lpReserved,
        _In_                    ULONG           ulFlags
);




_Success_(return != FALSE, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_)))
PODNET_API
BOOL
WriteI2C(
        _In_                    HANDLE          hI2C,
        _In_                    BYTE            byValue
);




_Success_(return != FALSE, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_)))
PODNET_API
BOOL
WriteI2CRegisterByte(
        _In_                    HANDLE          hI2C,
        _In_                    BYTE            byRegister,
        _In_                    BYTE            byValue
);




_Success_(return != FALSE, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_)))
PODNET_API
BOOL
WriteI2CRegisterShort(
        _In_                    HANDLE          hI2C,
        _In_                    BYTE            byRegister,
        _In_                    USHORT          usValue
);




_Success_(return != NULL, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_)))
PODNET_API
BYTE
ReadI2C(
        _In_                    HANDLE          hI2C
);




_Success_(return != NULL, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_)))
PODNET_API
BYTE
ReadI2CRegisterByte(
        _In_                    HANDLE          hI2C,
        _In_                    BYTE            byRegister
);




_Success_(return != NULL, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Semaphore_)))
PODNET_API
USHORT
ReadI2CRegisterShort(
        _In_                    HANDLE          hI2C,
        _In_                    BYTE            byRegister
);


#endif