/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CMemSet.h"

#ifndef __LP64__
#ifndef __x86_64__


_Success_(return != FALSE, ...)
PODNET_API
BOOL
SetMemoryByte(
        _In_Out_                        LPVOID          lpAddress,
        _In_Range_(1, MAX_ULONGLONG)    ULONGLONG       ullSize,
        _In_                            BYTE            bValue
){
        EXIT_IF_UNLIKELY_NULL(lpAddress, FALSE);
        EXIT_IF_UNLIKELY_NULL(ullSize, FALSE);
        for (
                ;
                ullSize > 0;
                ullSize--, lpAddress++
        ){ *(LPBYTE)lpAddress = bValue; }
        return TRUE;
}




_Success_(return != FALSE, ...)
PODNET_API
BOOL
SetMemoryShort(
        _In_Out_                        LPVOID          lpAddress,
        _In_Range_(2, MAX_ULONGLONG)    ULONGLONG       ullSize,
        _In_                            USHORT          usValue
){
        EXIT_IF_UNLIKELY_NULL(lpAddress, FALSE);
        EXIT_IF_UNLIKELY_NULL(ullSize, FALSE);
        KEEP_IN_REGISTER ULONGLONG ullIndex;
        for (
                ullIndex = 0;
                ullIndex < ullSize;
                ullIndex += 2, lpAddress += 2
        ){ *(LPUSHORT)lpAddress = usValue; }
        (ullIndex > ullSize) ? (*(LPBYTE)(LPVOID)((ULONGLONG)(lpAddress) - 1)) = ((usValue >> 8) & 0xFF) : NO_OPERATION();
        return TRUE;
}




_Success_(return != FALSE, ...)
PODNET_API
BOOL
SetMemoryLong(
        _In_Out_                        LPVOID          lpAddress,
        _In_Range_(4, MAX_ULONGLONG)    ULONGLONG       ullSize,
        _In_                            ULONG           ulValue
){
        EXIT_IF_UNLIKELY_NULL(lpAddress, FALSE);
        EXIT_IF_UNLIKELY_NULL(ullSize, FALSE);
        KEEP_IN_REGISTER ULONGLONG ullIndex;
        for (
                ullIndex = 0;
                ullIndex < ullSize;
                ullIndex += 4, lpAddress += 4
        ){ *(LPULONG)lpAddress = ulValue; }
        return TRUE;
}




_Success_(return != FALSE, ...)
PODNET_API
BOOL
SetMemoryLongLong(
        _In_Out_                        LPVOID          lpAddress,
        _In_Range_(8, MAX_ULONGLONG)    ULONGLONG       ullSize,
        _In_                            ULONGLONG       ullValue
){
        EXIT_IF_UNLIKELY_NULL(lpAddress, FALSE);
        EXIT_IF_UNLIKELY_NULL(ullSize, FALSE);
        KEEP_IN_REGISTER ULONGLONG ullIndex;
        for (
                ullIndex = 0;
                ullIndex < ullSize;
                ullIndex += 8, lpAddress += 8
        ){ *(LPULONGLONG)lpAddress = ullValue; }
        return TRUE;
}

#endif
#endif