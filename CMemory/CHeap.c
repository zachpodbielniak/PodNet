/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "../CHandle/CHandleObject.c"
#include "CHeap.h"


#define HEAP_MANAGEMENT_STACK_TRACK 		0x20
#define HEAP_MANAGEMENT_SEED_ADDRESS		0x400


typedef struct __HEAP
{
        INHERITS_FROM_HANDLE();
        HANDLE 		hManagerThread;
        HANDLE 		hLock;
        UARCHLONG 	ualHeapSize;
        UARCHLONG 	ualNumberOfAllocObjects;
	UARCHLONG 	ualPageSize;
	UARCHLONG 	ualPageIndex;
	UARCHLONG 	ualNumberOfPages;
	UARCHLONG 	ualNumberOfObjectsPerPage;
	LPVOID 		lpNext;
	DLPVOID 	dlpPages;
	DLPVOID 	dlpManagementPages;
} HEAP, *LPHEAP;




typedef struct __ADDRESS
{
        UARCHLONG       ualSize;
        LPVOID          lpAllocAddress;
} ADDRESS, *LPADDRESS;




typedef struct __MANAGER
{
	UARCHLONG 	ualIndex;
	UARCHLONG 	lpualNext[HEAP_MANAGEMENT_STACK_TRACK];
} MANAGER, *LPMANAGER;




_Success_(return != NULLPTR, ...)
PODNET_API
LPVOID
RequestPage(
        _In_                    ULONGLONG               ullSize
){
        EXIT_IF_NULL(ullSize, NULLPTR);
        LPVOID lpRet;
        lpRet = mmap(0, ullSize, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, NULL, NULL);
        return ((MAP_FAILED == lpRet) ? NULLPTR : lpRet);
}




_Success_(return != FALSE, ...)
PODNET_API
BOOL
ReturnPage(
        _In_                    LPVOID                  lpPageStartAddress,
        _In_                    ULONGLONG               ullSize
){
        EXIT_IF_NULL(lpPageStartAddress, FALSE);
        EXIT_IF_NULL(ullSize, FALSE);
        return ((0 == munmap(lpPageStartAddress, ullSize)) ? TRUE : FALSE);
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__DestroyHandle(
	_In_ 			HDERIVATIVE 		hDerivative,
	_In_Opt_ 		ULONG 			ulFlags
){
	UNREFERENCED_PARAMETER(ulFlags);
	EXIT_IF_UNLIKELY_NULL(hDerivative, FALSE);
	LPHEAP lpHeap;
	UARCHLONG ualIndex;
	lpHeap = (LPHEAP)hDerivative;
	WaitForSingleObject(lpHeap->hLock, INFINITE_WAIT_TIME);	
	
	for (
		ualIndex = 0;
		ualIndex < lpHeap->ualPageIndex;
		ualIndex++
	){
		ReturnPage(lpHeap->dlpManagementPages[ualIndex], sizeof(MANAGER) + (sizeof(ADDRESS) * HEAP_MANAGEMENT_SEED_ADDRESS));
		ReturnPage(lpHeap->dlpPages[ualIndex], lpHeap->ualPageSize);
	}

	DestroyHandle(lpHeap->hManagerThread);
	DestroyHandle(lpHeap->hLock);

	ReturnPage(lpHeap, sizeof(HEAP) + (sizeof(LPVOID) * lpHeap->ualNumberOfPages));	
	
	return TRUE;
}




_Thread_Proc_
INTERNAL_OPERATION
LPVOID
__HeapManagementThreadProc(
	_In_ 			LPVOID 			lpParam
){
	LPHEAP lpHeap;
	lpHeap = (LPHEAP)lpParam;
	

	INFINITE_LOOP()
	{
		EnterCriticalSection(lpHeap->hLock);
		
		/* Thread loop goes here */

		ExitCriticalSection(lpHeap->hLock);
		MicroSleep(0x64);	
	}	


	return NULLPTR;
}




_Success_(return != NULL, ...)
PODNET_API
HANDLE
CreateHeap(
        _In_                    UARCHLONG 		ualInitialSize,
	_In_ 			UARCHLONG 		ualMaxSize,
	_In_Opt_ 		ULONG 			ulFlags
){
	UNREFERENCED_PARAMETER(ulFlags);
	EXIT_IF_UNLIKELY_NULL(ualInitialSize, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(ualMaxSize, NULLPTR);

	HANDLE hHeap;
	LPHEAP lpHeap;
	lpHeap = RequestPage(sizeof(HEAP) + (sizeof(LPVOID) * ((ualMaxSize / ualInitialSize) + 1)));
	EXIT_IF_NULL(lpHeap, NULLPTR);
	ZeroMemory(lpHeap, sizeof(HEAP));	


	lpHeap->hLock = CreateCriticalSectionAndSpecifySpinCount(0xC80);
	EXIT_IF_UNLIKELY_NULL(lpHeap->hLock, NULLPTR);
	lpHeap->ualNumberOfPages = (ualMaxSize / ualInitialSize) + 1;
	lpHeap->ualNumberOfObjectsPerPage = HEAP_MANAGEMENT_SEED_ADDRESS;
	lpHeap->ualPageSize = ualInitialSize;

	lpHeap->dlpPages = (DLPVOID)((UARCHLONG)lpHeap + sizeof(HEAP));
	lpHeap->dlpPages[0] = RequestPage(ualInitialSize);
	EXIT_IF_UNLIKELY_NULL(lpHeap->dlpPages[0], NULLPTR);

	
	lpHeap->dlpManagementPages = (DLPVOID)((UARCHLONG)lpHeap->dlpPages + (sizeof(LPVOID) * (ualMaxSize / ualInitialSize) + sizeof(LPVOID)));
	lpHeap->dlpManagementPages[0] = RequestPage(sizeof(MANAGER) + (sizeof(ADDRESS) * HEAP_MANAGEMENT_SEED_ADDRESS));
	EXIT_IF_UNLIKELY_NULL(lpHeap->dlpManagementPages[0], NULLPTR);

	((LPMANAGER)lpHeap->dlpManagementPages[0])->ualIndex = 0;
	ZeroMemory(((LPMANAGER)lpHeap->dlpManagementPages[0])->lpualNext, sizeof(UARCHLONG) * HEAP_MANAGEMENT_STACK_TRACK);

	lpHeap->lpNext = lpHeap->dlpPages[0];
	lpHeap->ualPageIndex++;

	hHeap = CreateHandleWithSingleInheritor(
		lpHeap,
		&(lpHeap->hThis),
		HANDLE_TYPE_HEAP,
		__DestroyHandle,
		NULL,
		NULLPTR,
		NULL,
		NULLPTR,
		NULL,
		&(lpHeap->ullId),
		NULL
	);

	EXIT_IF_UNLIKELY_NULL(hHeap, NULLPTR);
	
	
	lpHeap->hManagerThread = CreateThread(__HeapManagementThreadProc, (LPVOID)lpHeap, NULL);
	EXIT_IF_UNLIKELY_NULL(lpHeap->hManagerThread, NULLPTR);
	

	return hHeap;
}




_Success_(return != NULLPTR, _Acquires_Lock_(hHeap, _Lock_Kind_Spin_Lock_))
PODNET_API
LPVOID
HeapAlloc(
        _In_                    HANDLE RESTRICT         hHeap,
        _In_                    UARCHLONG               ualSize,
        _In_Opt_                ULONG                   ulFlags    
){
	UNREFERENCED_PARAMETER(ulFlags);
	EXIT_IF_UNLIKELY_NULL(hHeap, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(ualSize, NULLPTR);
	EXIT_IF_NOT_VALUE(GetHandleType(hHeap), HANDLE_TYPE_HEAP, NULLPTR);
		

	LPHEAP lpHeap;
	LPVOID lpRet;
	LPMANAGER lpManager;
	LPADDRESS lpAddress;
	UARCHLONG ualIndex, ualNext;
	lpHeap = (LPHEAP)*(hHeap->lphDerivatives);

	EXIT_IF_UNLIKELY_NULL(lpHeap, NULLPTR); 
	lpRet = NULLPTR;

	EnterCriticalSection(lpHeap->hLock); 
	
	for (
		ualIndex = 0;
		ualIndex < lpHeap->ualPageIndex;
		ualIndex++
	){
		lpManager = lpHeap->dlpManagementPages[ualIndex];
	
		if (NULL != (ualNext = *(lpManager->lpualNext)))
		{
			lpAddress = (LPVOID)((UARCHLONG)lpManager + sizeof(MANAGER));
			lpAddress += ualNext * sizeof(ADDRESS);
		
			/* TODO: Allow splicing to reuse multiple times */
			if (ualSize <= lpAddress->ualSize)
			{ return lpAddress->lpAllocAddress; }

			/* TODO: Shift the whole stack over, if not zero! */
		}

	}


	lpManager = lpHeap->dlpManagementPages[lpHeap->ualPageIndex - 1];
	lpRet = lpHeap->lpNext;
	*(LPUARCHLONG)lpRet = lpManager->ualIndex;
	lpRet = (LPVOID)((UARCHLONG)lpRet + sizeof(UARCHLONG));
	lpAddress = (LPVOID)((UARCHLONG)lpManager + sizeof(MANAGER) + (sizeof(ADDRESS) * lpManager->ualIndex));
	lpAddress->lpAllocAddress = lpRet;
	lpAddress->ualSize = ualSize;
	lpManager->ualIndex++;
	lpHeap->lpNext = (LPVOID)((UARCHLONG)lpHeap->lpNext + ualSize + sizeof(UARCHLONG));

	ExitCriticalSection(lpHeap->hLock); 
	return lpRet;
}
