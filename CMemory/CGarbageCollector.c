/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CGarbageCollector.h"

#define TRASH_CAN_SIZE 		0x100


GLOBAL_VARIABLE HANDLE 	hCritSec = NULLPTR;
GLOBAL_VARIABLE HVECTOR hvTrashCan = NULLPTR;
GLOBAL_VARIABLE UARCHLONG ualTrashSize = NULL;



_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
InitializeGarbageCollector(
	_In_Opt_ 	UARCHLONG 	ualTrashCanSize
){
	BOOL bRet;
	bRet = FALSE;

	if (UNLIKELY(NULLPTR == hvTrashCan))
	{
		ualTrashSize = (0 == ualTrashCanSize) ? TRASH_CAN_SIZE : ualTrashCanSize;
		hvTrashCan = CreateVector(
			ualTrashSize,
			sizeof(HANDLE), 
			NULL
		);

		EXIT_IF_UNLIKELY_NULL(hvTrashCan, FALSE);
		bRet = TRUE;
	}

	if (UNLIKELY(NULLPTR == hCritSec))
	{
		hCritSec = CreateCriticalSectionAndSpecifySpinCount(0xC00);
		EXIT_IF_UNLIKELY_NULL(hCritSec, FALSE);
		bRet = TRUE;
	}

	return bRet;
}



_Success_(return != FALSE, _Acquires_Shared_Lock_(_Lock_Kind_Critical_Section_))
PODNET_API
BOOL
Dispose(
	_In_ 		HANDLE 		hObject
){
	EXIT_IF_UNLIKELY_NULL(hObject, FALSE);
	BOOL bRet;
	bRet = TRUE;

	WaitForSingleObject(hCritSec, INFINITE_WAIT_TIME);
	VectorPushBack(hvTrashCan, &hObject, NULL);

	if (VectorSize(hvTrashCan) >= ualTrashSize)
	{ 
		UARCHLONG ualIndex;
		LPHANDLE lphObject;

		for (
			ualIndex = 0;
			ualIndex < ualTrashSize;
			ualIndex++
		){ 
			lphObject = (LPHANDLE)VectorAt(hvTrashCan, ualIndex);
			DestroyObject(*lphObject);
		}	

		VectorClear(hvTrashCan);
	}

	ReleaseSingleObject(hCritSec);
	return bRet;
}




_Success_(return != FALSE, _Acquires_Shared_Lock_(_Lock_Kind_Critical_Section_))
PODNET_API
BOOL
TakeOutTheTrash(
	VOID
){
	BOOL bRet;
	bRet = TRUE;

	WaitForSingleObject(hCritSec, INFINITE_WAIT_TIME);

	UARCHLONG ualIndex;
	LPHANDLE lphObject;
	
	for (
		ualIndex = 0;
		ualIndex < ualTrashSize;
		ualIndex++
	){ 
		lphObject = (LPHANDLE)VectorAt(hvTrashCan, ualIndex);
		if (NULLPTR != lphObject)
		{ DestroyObject(*lphObject); }
	}	

	VectorClear(hvTrashCan);
	ReleaseSingleObject(hCritSec);
	return bRet;
}
