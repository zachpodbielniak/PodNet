; 
; 
; 
;  ____           _ _   _      _   
; |  _ \ ___   __| | \ | | ___| |_ 
; | |_) / _ \ / _` |  \| |/ _ \ __|
; |  __/ (_) | (_| | |\  |  __/ |_ 
; |_|   \___/ \__,_|_| \_|\___|\__|
; 
; 
; General Purpose C Library.
; Copyright (C) 2017-2019 Zach Podbielniak
; 
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU Affero General Public License as
; published by the Free Software Foundation, either version 3 of the
; License, or (at your option) any later version.
; 
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU Affero General Public License for more details.
; 
; You should have received a copy of the GNU Affero General Public License
; along with this program.  If not, see <https://www.gnu.org/licenses/>.
; 
;
; Parameters are passed in RDI, RSI, RDX, RCX, R8, R9, Stack : in that order.


[DEFAULT REL]
[BITS 64]


global __FastZeroX64

segment .data

segment .text


; __FastZeroX64(
;       _In_            LPVOID          lpAddress       RDI,
;       _In_            ULONGLONG       ullSize         RSI
;);

__FastZeroX64:
        cmp rdi, 0
        je __FastZeroX64Error
        cmp rsi, 0
        je __FastZeroX64Error

        xorps xmm0, xmm0        ; Nullify xmm0
        xor rdx, rdx            ; Nullify rdx
        mov rax, rsi
        mov rbx, rsi            ; rbx is main counter
        shr rbx, 4              ; Divide by 16 to get XmmWord Loops
        mov rcx, rbx
        shl rcx, 4
        sub rax, rcx            ; Remainder after 16 bit loops.

        cmp rbx, 0
        je __FastZeroX64DoneWithMain

        __FastZeroX64MainLoop:
        movups [rdi], xmm0
        ;movups [rdi+16], xmm0
        add rdi, 16
        dec rbx
        cmp rbx, 0
        jg __FastZeroX64MainLoop

        __FastZeroX64DoneWithMain:
        cmp rax, 1
        jle __FastZeroX64SetLastByte

        __FastZeroX64SetLastBytes:
        mov WORD [rdi], dx
        add rdi, 2
        sub rax, 2
        cmp rax, 2
        jge __FastZeroX64SetLastBytes

        __FastZeroX64SetLastByte:
        cmp rax, 0
        je __FastZeroX64Done
        mov BYTE [rdi], dl

        __FastZeroX64Done:
        mov rax, 1
        ret

        __FastZeroX64Error:
        mov rax, 0
        ret