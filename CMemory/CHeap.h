/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../CError/CError.h"
#include "../CHandle/CHandle.h"
#include "../CHandle/CHandleTable.h"
#include "../CLock/CCritSec.h"
#include "../CThread/CThread.h"



_Success_(return != NULL, ...)
PODNET_API
HANDLE
CreateHeap(
        _In_                    UARCHLONG 		ualInitialSize,
	_In_ 			UARCHLONG 		ualMaxSize,
	_In_Opt_ 		ULONG 			ulFlags
);




_Success_(return != NULLPTR, _Acquires_Lock_(hHeap, _Lock_Kind_Spin_Lock_))
PODNET_API
LPVOID
HeapAlloc(
        _In_                    HANDLE RESTRICT         hHeap,
        _In_                    UARCHLONG               ualSize,
        _In_Opt_                ULONG                   ulFlags    
) CALL_TYPE(HOT);




_Success_(return != NULLPTR, _Acquires_Lock_(hHeap, _Lock_Kind_Spin_Lock_))
PODNET_API
LPVOID
HeapReAlloc(
        _In_                    HANDLE                  hHeap,
        _In_                    LPVOID                  lpData,
        _In_                    ULONGLONG               ullNewSize,
        _In_Opt_                ULONG                   ulFlags   
);




_Success_(return != FALSE, _Acquires_Lock_(hHeap, _Lock_Kind_Spin_Lock_))
PODNET_API
BOOL
HeapFree(
        _In_                    HANDLE                  hHeap,
        _In_                    LPVOID                  lpAddress,
        _In_Opt_                ULONG                   ulFlags
);



/*
_Success_(return != NULLPTR, _Acquires_Lock(hGlobalHeap, _Lock_Kind_Critical_Section_))
PODNET_API
LPVOID
GlobalAlloc(
        _In_                    ULONGLONG               ullSize,
        _In_Opt_                ULONG                   ulFlags
);
*/



_Success_(return != NULLPTR, ...)
PODNET_API
LPVOID
RequestPage(
        _In_                    ULONGLONG               ullSize
);




_Success_(return != FALSE, ...)
PODNET_API
BOOL
ReturnPage(
        _In_                    LPVOID                  lpPageStartAddress,
        _In_                    ULONGLONG               ullSize
);
