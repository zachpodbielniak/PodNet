; 
; 
; 
;  ____           _ _   _      _   
; |  _ \ ___   __| | \ | | ___| |_ 
; | |_) / _ \ / _` |  \| |/ _ \ __|
; |  __/ (_) | (_| | |\  |  __/ |_ 
; |_|   \___/ \__,_|_| \_|\___|\__|
; 
; 
; General Purpose C Library.
; Copyright (C) 2017-2019 Zach Podbielniak
; 
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU Affero General Public License as
; published by the Free Software Foundation, either version 3 of the
; License, or (at your option) any later version.
; 
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU Affero General Public License for more details.
; 
; You should have received a copy of the GNU Affero General Public License
; along with this program.  If not, see <https://www.gnu.org/licenses/>.
; 
;
; Parameters are passed in RDI, RSI, RDX, RCX, R8, R9, Stack : in that order.


[DEFAULT REL]
[BITS 64]


segment .text

; __SetMemoryByteX86(
;       _In_Out_        LPVOID          lpAddress       rdi,      
;       _In_            ULONGLONG       ullSize         rsi         
;       _In_            BYTE            bValue          dl
;);
__SetMemoryByteX86:
        cmp rdi, 0
        je __SMBX86FAIL
        cmp rsi, 0
        je __SMBX86FAIL

        __SMBX86LOOP:
        mov BYTE [rdi], dl
        inc rdi
        dec rsi
        cmp rsi, 0
        jne __SMBX86LOOP

        __SMBX86DONE:
        mov rax, 1
        ret

        __SMBX86FAIL:
        mov rax, 0
        ret



; __SetMemoryWordX86(
;       _In_Out_        LPVOID          lpAddress       rdi,      
;       _In_            ULONGLONG       ullSize         rsi         
;       _In_            WORD            wValue          dx
;);
__SetMemoryWordX86:
        cmp rdi, 0
        je __SMWX86FAIL
        cmp rsi, 1
        jle __SMWX86FAIL        


        __SMWX86LOOP:
        mov WORD [rdi], dx
        add rdi, 2
        sub rsi, 2
        cmp rsi, 1
        jg __SMWX86LOOP

        cmp rsi, 0
        je __SMWX86DONE
        mov BYTE [rdi-1], dh

        __SMWX86DONE:
        mov rax, 1
        ret

        __SMWX86FAIL:
        mov rax, 0
        ret




; __SetMemoryDwordX86(
;       _In_Out_        LPVOID          lpAddress       rdi,      
;       _In_            ULONGLONG       ullSize         rsi         
;       _In_            DWORD           dwValue         edx
;);
__SetMemoryDwordX86:
        cmp rdi, 0
        je __SMDWX86FAIL
        cmp rdi, 3
        jle __SMDWX86FAIL


        __SMDWX86LOOP:
        mov DWORD [rdi], edx
        add rdi, 4
        sub rsi, 4
        cmp rsi, 4
        jge __SMDWX86LOOP

        cmp rsi, 2
        jl __SMDWX86SETLASTBYTE
        mov r9d, edx
        shr r9d, 16
        mov WORD [rdi], r9w

        __SMDWX86SETLASTBYTE:
        cmp rsi, 1
        jne __SMDWX86DONE
        shr edx, 24
        mov BYTE [rdi], dl

        __SMDWX86DONE:
        mov rax, 1
        ret

        __SMDWX86FAIL:
        mov rax, 0
        ret



; __SetMemoryQwordX86(
;       _In_Out_        LPVOID          lpAddress       rdi,      
;       _In_            ULONGLONG       ullSize         rsi         
;       _In_            QWORD           qwValue         rdx
;);
__SetMemoryQwordX86:
        cmp rdi, 0
        je __SMQWX86FAIL
        cmp rsi, 7
        jle __SMQWX86FAIL


        __SMQWX86LOOP:
        mov QWORD [rdi], rdx
        add rdi, 8
        sub rsi, 8
        cmp rsi, 8
        jge __SMQWX86LOOP

        cmp rsi, 3
        jle __SMQWX86SWTLASTWORD
        mov r9, rdx
        shr r9, 32
        mov DWORD [rdi], r9d

        __SMQWX86SWTLASTWORD:        
        cmp rsi, 2
        jl __SMQWX86SETLASTBYTE
        mov r9, rdx
        shr r9, 48
        mov WORD [rdi], r9w

        __SMQWX86SETLASTBYTE:
        cmp rsi, 1
        jl __SMBX86DONE
        shr rdx, 56
        mov BYTE [rdi], dl

        __SMQWX86DONE:
        mov rax, 1
        ret

        __SMQWX86FAIL:
        mov rax, 0
        ret



__SetMemoryXmmwordX86: