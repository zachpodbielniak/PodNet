; 
; 
; 
;  ____           _ _   _      _   
; |  _ \ ___   __| | \ | | ___| |_ 
; | |_) / _ \ / _` |  \| |/ _ \ __|
; |  __/ (_) | (_| | |\  |  __/ |_ 
; |_|   \___/ \__,_|_| \_|\___|\__|
; 
; 
; General Purpose C Library.
; Copyright (C) 2017-2019 Zach Podbielniak
; 
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU Affero General Public License as
; published by the Free Software Foundation, either version 3 of the
; License, or (at your option) any later version.
; 
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU Affero General Public License for more details.
; 
; You should have received a copy of the GNU Affero General Public License
; along with this program.  If not, see <https://www.gnu.org/licenses/>.
; 
;
; Parameters are passed in RDI, RSI, RDX, RCX, R8, R9, Stack : in that order.


[DEFAULT REL]
[BITS 64]


global __FastCompareX64

segment .data

segment .text

; __FastCompareX64(
;       _In_            LPVOID          lpSource        RDI,
;       _In_            LPVOID          lpComparand     RSI,
;       _In_            QWORD           qwSize          RDX
;);

__FastCompareX64:
        cmp rdi, 0
        je __FastCompareX64Error
        cmp rsi, 0
        je __FastCompareX64Error
        cmp rdx, 0
        je __FastCompareX64Error

        xorps xmm0, xmm0        ; Nullify xmm0
        mov rax, rdx
        mov rcx, rdx
        shr rdx, 4              ; Divide by 16 to get XmmWord Loops
        shl rcx, 4
        sub rax, rcx            ; Remainder after 16 bit loops

        cmp rdx, 0
        je __FastCompareX64DoneWithMain

        __FastCompareX64MainLoop:
        movups xmm1, [rdi]
        movups xmm2, [rsi]
        xorps xmm1, xmm2
        cmpeqps xmm1, xmm0      ; When not equal sets xmm1 to 0
        ;cvtss2si r9, xmm1       
        movq r9, xmm1           ; Should be faster(?)
        cmp r9, 0
        je __FastCompareX64Error
        dec rdx
        add rdi, 16
        add rsi, 16
        cmp rdx, 0
        jne __FastCompareX64MainLoop        

        __FastCompareX64DoneWithMain:
        xor rbx, rbx   
        cmp rax, 1
        jle __FastCompareX64CompareLastByte        

        __FastCompareX64CompareLastBytes:
        mov r9w, WORD [rsi]
        mov r10w, WORD [rdi]
        xor r9w, r10w
        cmp r9w, 0
        jne __FastCompareX64Error
        add rdi, 2
        add rsi, 2
        sub rax, 2
        cmp rax, 2
        jge __FastCompareX64CompareLastBytes

        __FastCompareX64CompareLastByte:
        cmp rax, 0
        je __FastCompareX64Success
        mov r9b, BYTE [rsi]
        mov r10b, BYTE [rdi]
        xor r9w, r10w
        cmp r9w, 0
        jne __FastCompareX64Error

        __FastCompareX64Success:
        mov rax, 1
        ret

        __FastCompareX64Error:
        mov rax, 0
        ret