; 
; 
; 
;  ____           _ _   _      _   
; |  _ \ ___   __| | \ | | ___| |_ 
; | |_) / _ \ / _` |  \| |/ _ \ __|
; |  __/ (_) | (_| | |\  |  __/ |_ 
; |_|   \___/ \__,_|_| \_|\___|\__|
; 
; 
; General Purpose C Library.
; Copyright (C) 2017-2019 Zach Podbielniak
; 
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU Affero General Public License as
; published by the Free Software Foundation, either version 3 of the
; License, or (at your option) any later version.
; 
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU Affero General Public License for more details.
; 
; You should have received a copy of the GNU Affero General Public License
; along with this program.  If not, see <https://www.gnu.org/licenses/>.
; 
;
; Parameters are passed in RDI, RSI, RDX, RCX, R8, R9, Stack : in that order.


[DEFAULT REL]
[BITS 64]


global __FastFindByteX64
global __FastFindWordX64
global __FastFindDwordX64
global __FastFindQwordX64


segment .data

segment .text


; _Success_(return != NULLPTR, ...)
; __FastFindByteX64(
;       _In_            LPVOID          lpSource        RDI,      
;       _In_            BYTE            bValue          SIL,
;       _In_            QWORD           qwSize          RDX
;);

__FastFindByteX64:
        cmp rdi, 0
        je __FastFindByteX64Error
        cmp rdx, 0
        je __FastFindByteX64Error
        
        mov rax, rdx
        shr rdx, 4              ; Divide by 16 to get XmmWord Loops
        mov rcx, rdx
        shl rcx, 4
        sub rax, rcx            ; Remainder after 16 bit loops

        cmp rdx, 0
        je __FastFindByteX64DoneWithMain

        __FastFindByteX64MainLoop:
        movups xmm0, [rdi]
        movq r8, xmm0           ; Move one half of xmm0 into r8
        shufps xmm0, xmm0, 0b01001110 
        movq r9, xmm0           ; Move the other half into r9
        mov rbx, 8


        ; I have unrolled the "loops" that these
        ; next two segments represent.

        ; Here we loop through r8, by checking
        ; r8l against sil, then we shift-right
        ; by 8 bytes and check the next r8l.

        __FastFindByteX64MainLoopInnerR8: 
        cmp r8b, sil
        je __FastFindByteX64Success
        shr r8, 8
        inc rdi         ; Iteration 1
        
        cmp r8b, sil
        je __FastFindByteX64Success
        shr r8, 8
        inc rdi         ; Iteration 2
        
        cmp r8b, sil
        je __FastFindByteX64Success
        shr r8, 8
        inc rdi         ; Iteration 3

        cmp r8b, sil
        je __FastFindByteX64Success
        shr r8, 8
        inc rdi         ; Iteration 4

        cmp r8b, sil
        je __FastFindByteX64Success
        shr r8, 8
        inc rdi         ; Iteration 5

        cmp r8b, sil
        je __FastFindByteX64Success
        shr r8, 8
        inc rdi         ; Iteration 6

        cmp r8b, sil
        je __FastFindByteX64Success
        shr r8, 8
        inc rdi         ; Iteration 7

        cmp r8b, sil
        je __FastFindByteX64Success
        shr r8, 8
        inc rdi         ; Iteration 8


        ; Here we loop through r9, by checking
        ; r9l against sil, then we shift-right
        ; by 8 bytes and check the next r9l.

        __FastFindByteX64MainLoopInnerR9:
        cmp r9b, sil
        je __FastFindByteX64Success
        shr r9, 8
        inc rdi         ; Iteration 1

        cmp r9b, sil
        je __FastFindByteX64Success
        shr r9, 8
        inc rdi         ; Iteration 2

        cmp r9b, sil
        je __FastFindByteX64Success
        shr r9, 8
        inc rdi         ; Iteration 3

        cmp r9b, sil
        je __FastFindByteX64Success
        shr r9, 8
        inc rdi         ; Iteration 4

        cmp r9b, sil
        je __FastFindByteX64Success
        shr r9, 8
        inc rdi         ; Iteration 5

        cmp r9b, sil
        je __FastFindByteX64Success
        shr r9, 8
        inc rdi         ; Iteration 6

        cmp r9b, sil
        je __FastFindByteX64Success
        shr r9, 8
        inc rdi         ; Iteration 7

        cmp r9b, sil
        je __FastFindByteX64Success
        shr r9, 8
        inc rdi         ; Iteration 8

        dec rdx
        cmp rdx, 0
        jne __FastFindByteX64MainLoop

        ; Handle finishing up after XmmWord loop
        __FastFindByteX64DoneWithMain:
        cmp rax, 1
        jle __FastFindByteX64CompareLastByte
        
        __FastFindByteX64CompareLastBytes:
        mov r9w, WORD [rdi]
        cmp r9b, sil
        je __FastFindByteX64Success
        shr r9, 8
        cmp r9b, sil
        je __FastFindByteX64Success
        add rdi, 2
        sub rax, 2
        cmp rax, 2
        jge __FastFindByteX64CompareLastBytes

        __FastFindByteX64CompareLastByte:
        cmp rax, 1
        jne __FastFindByteX64Error
        mov r9b, BYTE [rdi]
        cmp r9b, sil
        jne __FastFindByteX64Error

        __FastFindByteX64Success:
        mov rax, rdi
        ret

        __FastFindByteX64Error:
        mov rax, 0
        ret



__FastFindWordX64:
        cmp rdi, 0
        je __FastFindByteX64Error
        cmp rdx, 0
        je __FastFindByteX64Error
        
        mov rax, rdx
        shr rdx, 4              ; Divide by 16 to get XmmWord Loops
        mov rcx, rdx
        shl rcx, 4
        sub rax, rcx            ; Remainder after 16 bit loops
        shr rax, 1              ; Word size is 2

        cmp rdx, 0
        je __FastFindWordX64DoneWithMain

        __FastFindWordX64MainLoop:
        movups xmm0, [rdi]
        movq r8, xmm0           ; Move one half of xmm0 into r8
        shufps xmm0, xmm0, 0b01001110 
        movq r9, xmm0           ; Move the other half into r9
        mov rbx, 8


        ; I have unrolled the "loops" that these
        ; next two segments represent.

        ; Here we loop through r8, by checking
        ; r8l against sil, then we shift-right
        ; by 8 bytes and check the next r8l.

        __FastFindWordX64MainLoopInnerR8: 
        cmp r8w, si
        je __FastFindWordX64Success
        shr r8, 16
        add rdi, 2      ; Iteration 1

        cmp r8w, si
        je __FastFindWordX64Success
        shr r8, 16
        add rdi, 2      ; Iteration 2

        cmp r8w, si
        je __FastFindWordX64Success
        shr r8, 16
        add rdi, 2      ; Iteration 3

        cmp r8w, si
        je __FastFindWordX64Success
        shr r8, 16
        add rdi, 2      ; Iteration 4


        ; Here we loop through r9, by checking
        ; r9l against sil, then we shift-right
        ; by 8 bytes and check the next r9l.

        __FastFindWordX64MainLoopInnerR9:
        cmp r9w, si
        je __FastFindWordX64Success
        shr r9, 16
        add rdi, 2      ; Iteration 1

        cmp r9w, si
        je __FastFindWordX64Success
        shr r9, 16
        add rdi, 2      ; Iteration 2

        cmp r9w, si
        je __FastFindWordX64Success
        shr r9, 16
        add rdi, 2      ; Iteration 3

        cmp r9w, si
        je __FastFindWordX64Success
        shr r9, 16
        add rdi, 2      ; Iteration 4

        dec rdx
        cmp rdx, 0
        jne __FastFindWordX64MainLoop

        ; Handle finishing up after XmmWord loop
        __FastFindWordX64DoneWithMain:
        cmp rax, 1
        jle __FastFindWordX64Error
        
        __FastFindWordX64CompareLastBytes:
        mov r9w, WORD [rdi]
        cmp r9b, sil
        je __FastFindWordX64Success
        add rdi, 2
        dec rax
        cmp rax, 1
        jge __FastFindWordX64CompareLastBytes

        jmp __FastFindWordX64Error

        __FastFindWordX64Success:
        mov rax, rdi
        ret

        __FastFindWordX64Error:
        mov rax, 0
        ret