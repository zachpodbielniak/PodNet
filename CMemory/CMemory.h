/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CMEMORY_H
#define CMEMORY_H


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"


/*
        The following two structs can be used as a 
        way to interpret one type as another while
        successfully getting around the strict 
        aliasing rules. Simply cast the original
        type to one of these two unions, then use
        the "." operator to get the value out 
        you need.

        Example:
        FLOAT fX;
        LONG lX;
        fX = 5.0f;
        lX = ((LONG_UNION)fX).lX;
*/

typedef union __LONG_UNION
{
        FLOAT fX;
        LONG lX;
        ULONG ulX;
        SHORT sX[2];
        USHORT usX[2];
        CHAR cX[4];
        BYTE byX[4];
} LONG_UNION, *LPLONG_UNION;




typedef union __LONGLONG_UNION
{
        DOUBLE dbX;
        FLOAT fX[2];
        LONGLONG llX;
        ULONGLONG ullX;
        LONG lX[2];
        ULONG ulX[2];
        SHORT sX[4];
        USHORT usX[4];
        CHAR cX[8];
        BYTE byX[8];
        LONG_UNION luUnion[2];
} LONGLONG_UNION, *LPLONGLONG_UNION;



typedef union __ULTRALONG_UNION
{
        DOUBLE dbX[2];
        FLOAT fX[4];
        ULTRALONG ultX;
        UULTRALONG uultX;
        LONGLONG llX[2];
        ULONGLONG ullX[2];
        LONG lX[4];
        ULONG ulX[4];
        SHORT sX[8];
        USHORT usX[8];
        CHAR cX[16];
        BYTE byX[16];
        LONG_UNION luUnion[4];
        LONGLONG_UNION lluUnion[2];
} ULTRALONG_UNION, *LPULTRALONG_UNION; 


#endif