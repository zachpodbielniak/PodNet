/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/




#ifndef CMEMSET_H
#define CMEMSET_H

#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"


#ifdef __LP64__
#ifdef __x86_64__




_Success_(return != FALSE, ...)
PODNET_API
BOOL
__SetMemoryByteX86(
        _In_Out_                        LPVOID          lpAddress,
        _In_Range_(1, MAX_ULONGLONG)    ULONGLONG       ullSize,
        _In_                            BYTE            bValue
);




_Success_(return != FALSE, ...)
PODNET_API
BOOL
__SetMemoryWordX86(
        _In_Out_                        LPVOID          lpAddress,
        _In_Range_(2, MAX_ULONGLONG)    ULONGLONG       ullSize,
        _In_                            WORD            wValue
);




_Success_(return != FALSE, ...)
PODNET_API
BOOL
__SetMemoryDwordX86(
        _In_Out_                        LPVOID          lpAddress,
        _In_Range_(4, MAX_ULONGLONG)    ULONGLONG       ullSize,
        _In_                            DWORD           dwValue
);




_Success_(return != FALSE, ...)
PODNET_API
BOOL
__SetMemoryQwordX86(
        _In_Out_                        LPVOID          lpAddress,
        _In_Range_(8, MAX_ULONGLONG)    ULONGLONG       ullSize,
        _In_                            QWORD           qwValue
);


_Success_(return != FALSE, ...)
PODNET_API
BOOL
__FastZeroX64(
        _In_                            LPVOID          lpAddress,
        _In_                            ULONGLONG       ullAddress
) ASM_CALL("__FastZeroX64");


_Success_(return != FALSE, ...)
PODNET_API
BOOL
__FastCopyX64(
        _In_                            LPVOID          lpDestination,
        _In_                            LPVOID          lpSource,
        _In_                            QWORD           qwSize
) ASM_CALL("__FastCopyX64");


_Success_(return != FALSE, ...)
PODNET_API
BOOL
__FastCompareX64(
        _In_                            LPVOID          lpControl,
        _In_                            LPVOID          lpComparand,
        _In_                            QWORD           qwSize
) ASM_CALL("__FastCompareX64");


_Success_(return != NULLPTR, ...)
PODNET_API
LPVOID
__FastFindByteX64(
        _In_                            LPVOID          lpAddress,
        _In_                            BYTE            bValue,
        _In_                            QWORD           qwSize
) ASM_CALL("__FastFindByteX64");




_Success_(return != NULLPTR, ...)
PODNET_API
LPVOID
__FastFindWordX64(
        _In_                            LPVOID          lpAddress,
        _In_                            WORD            wValue,
        _In_                            QWORD           qwSize
) ASM_CALL("__FastFindWordX64");




_Success_(return != FALSE, ...)
PODNET_API
INLINE
BOOL
SetMemoryByte(
        _In_Out_                        LPVOID          lpAddress,
        _In_Range_(1, MAX_ULONGLONG)    ULONGLONG       ullSize,
        _In_                            BYTE            bValue
) { return __SetMemoryByteX86(lpAddress, ullSize, bValue); }




_Success_(return != FALSE, ...)
PODNET_API
INLINE
BOOL
SetMemoryShort(
        _In_Out_                        LPVOID          lpAddress,
        _In_Range_(2, MAX_ULONGLONG)    ULONGLONG       ullSize,
        _In_                            USHORT          usValue
){ return __SetMemoryWordX86(lpAddress, ullSize, (WORD)usValue); }




_Success_(return != FALSE, ...)
PODNET_API
INLINE
BOOL
SetMemoryLong(
        _In_Out_                        LPVOID          lpAddress,
        _In_Range_(4, MAX_ULONGLONG)    ULONGLONG       ullSize,
        _In_                            ULONG           ulValue
){ return __SetMemoryDwordX86(lpAddress, ullSize, (DWORD)ulValue); }




_Success_(return != FALSE, ...)
PODNET_API
INLINE
BOOL
SetMemoryLongLong(
        _In_Out_                        LPVOID          lpAddress,
        _In_Range_(8, MAX_ULONGLONG)    ULONGLONG       ullSize,
        _In_                            ULONGLONG       ullValue
){ return __SetMemoryQwordX86(lpAddress, ullSize, (QWORD)ullValue); }


_Success_(return != FALSE, ...)
INLINE
BOOL
ZeroMemoryFast(
        _In_                            LPVOID          lpAddress,
        _In_                            ULONGLONG       ullSize
){ return __FastZeroX64(lpAddress, ullSize); }




_Success_(return != FALSE, ...)
INLINE
BOOL
CopyMemoryFast(
        _In_                            LPVOID          lpDestination,
        _In_                            LPVOID          lpSource,
        _In_                            ULONGLONG       ullSize
) { return __FastCopyX64(lpDestination, lpSource, (QWORD)ullSize); }


_Success_(return != FALSE, ...)
INLINE
BOOL
CompareMemoryFast(
        _In_                            LPVOID          lpControl,
        _In_                            LPVOID          lpComparand,
        _In_                            ULONGLONG       ullSize
) { return __FastCompareX64(lpControl, lpComparand, (QWORD)ullSize); }


_Success_(return != NULLPTR, ...)
PODNET_API
LPVOID
FindByteFast(
        _In_                            LPVOID          lpAddress,
        _In_                            BYTE            bValue,
        _In_                            ULONGLONG       ullSize
){ return __FastFindByteX64(lpAddress, bValue, (QWORD)ullSize); }




_Success_(return != NULLPTR, ...)
PODNET_API
LPVOID
FindShortFast(
        _In_                            LPVOID          lpAddress,
        _In_                            SHORT           sValue,
        _In_                            ULONGLONG       ullSize
){ return __FastFindWordX64(lpAddress, (WORD)sValue, (QWORD)ullSize); }



#endif /* x86_64 */

#else

/* Default Implementation in C */

_Success_(return != FALSE, ...)
PODNET_API
BOOL
SetMemoryByte(
        _In_Out_                        LPVOID          lpAddress,
        _In_Range_(1, MAX_ULONGLONG)    ULONGLONG       ullSize,
        _In_                            BYTE            bValue
);




_Success_(return != FALSE, ...)
PODNET_API
BOOL
SetMemoryShort(
        _In_Out_                        LPVOID          lpAddress,
        _In_Range_(2, MAX_ULONGLONG)    ULONGLONG       ullSize,
        _In_                            USHORT          usValue
);




_Success_(return != FALSE, ...)
PODNET_API
BOOL
SetMemoryLong(
        _In_Out_                        LPVOID          lpAddress,
        _In_Range_(4, MAX_ULONGLONG)    ULONGLONG       ullSize,
        _In_                            ULONG           ulValue
);




_Success_(return != FALSE, ...)
PODNET_API
BOOL
SetMemoryLongLong(
        _In_Out_                        LPVOID          lpAddress,
        _In_Range_(8, MAX_ULONGLONG)    ULONGLONG       ullSize,
        _In_                            ULONGLONG       ullValue
);




_Success_(return != FALSE, ...)
PODNET_API
INLINE
BOOL
ZeroMemoryFast(
        _In_                            LPVOID          lpAddress,
        _In_                            ULONGLONG       ullSize
) { return ZeroMemory(lpAddress, ullSize); }




_Success_(return != FALSE, ...)
PODNET_API
INLINE
BOOL
CopyMemoryFast(
        _In_                            LPVOID          lpDestination,
        _In_                            LPVOID          lpSource,
        _In_                            ULONGLONG       ullSize
) { return CopyMemory(lpDestination, lpSource, (QWORD)ullSize); }




_Success_(return != FALSE, ...)
INLINE
BOOL
CompareMemoryFast(
        _In_                            LPVOID          lpControl,
        _In_                            LPVOID          lpComparand,
        _In_                            ULONGLONG       ullSize
) { return CompareMemory(lpControl, lpComparand, (QWORD)ullSize); }



_Success_(return != NULLPTR, ...)
PODNET_API
LPVOID
FindByteFast(
        _In_                            LPVOID          lpAddress,
        _In_                            BYTE            bValue,
        _In_                            ULONGLONG       ullSize
){ return memchr(lpAddress, (LONG)bValue, (QWORD)ullSize); }

#endif /* LP64 */
#endif /* CMEMSET_H */