/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CDynamicHeapRegion.h"


typedef struct __DYNAMIC_HEAP_REGION
{
	INHERITS_FROM_HANDLE();
	HANDLE 			hLock;
	HANDLE 			hHeap;
	LPVOID 			lpData;
	UARCHLONG 		ualCommittedSize;
	UARCHLONG 		ualRegionSize;
} DYNAMIC_HEAP_REGION, *LPDYNAMIC_HEAP_REGION;




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__ExpandRegion(
	_In_ 		LPDYNAMIC_HEAP_REGION	lpdhsData,
	_In_ 		UARCHLONG 		ualExpandBy
){
	if (NULL_OBJECT == lpdhsData->hHeap)
	{
		LPVOID lpData;
		lpData = GlobalReAlloc(lpdhsData->lpData, lpdhsData->ualRegionSize + ualExpandBy);
		
		lpdhsData->lpData = lpData;
		lpdhsData->ualRegionSize += ualExpandBy;
	}
	else 
	{ /* TODO: FINISH HEAP IMPLEMENTATION */}

	return TRUE;
}



_Success_(return != FALSE, _Interlocked_Operation_)
INTERNAL_OPERATION
BOOL
__DestroyDynamicHeapRegion(
	_In_ 		HDERIVATIVE 		hRegion,
	_In_Opt_ 	ULONG 			ulFlags
){
	UNREFERENCED_PARAMETER(ulFlags);

	LPDYNAMIC_HEAP_REGION lpdhsData;
	lpdhsData = (LPDYNAMIC_HEAP_REGION)hRegion;

	DestroyObject(lpdhsData->hLock);
	FreeMemory(lpdhsData->lpData);
	FreeMemory(lpdhsData);

	return TRUE;
}




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNET_API
HANDLE
CreateDynamicHeapRegion(
	_In_Opt_	HANDLE 			hHeap,
	_In_		UARCHLONG 		ualSize,
	_In_ 		BOOL 			bMapSupport,
	_In_Opt_ 	ULONG 			ulFlags
){
	UNREFERENCED_PARAMETER(bMapSupport);
	UNREFERENCED_PARAMETER(ulFlags);

	HANDLE hRegion;
	LPDYNAMIC_HEAP_REGION lpdhsData;

	lpdhsData = GlobalAllocAndZero(sizeof(DYNAMIC_HEAP_REGION));
	if (NULLPTR == lpdhsData)
	{ return NULL_OBJECT; }

	lpdhsData->hHeap = hHeap;
	lpdhsData->ualRegionSize = ualSize;
	lpdhsData->ualCommittedSize = 0;
	lpdhsData->hLock = CreateCriticalSectionAndSpecifySpinCount(0xC00);
	lpdhsData->lpData = GlobalAllocAndZero(ualSize);

	hRegion = CreateHandleWithSingleInheritor(
		lpdhsData,
		&(lpdhsData->hThis),
		HANDLE_TYPE_DYNAMIC_HEAP_REGION,
		__DestroyDynamicHeapRegion,
		0,
		NULLPTR,
		0,
		NULLPTR,
		0,
		&(lpdhsData->ullId),
		0
	);

	return hRegion;
}




_Success_(return != NULL_OBJECT, _Interlocked_Operation_)
PODNET_API
BOOL
DynamicHeapRegionAppend(
	_In_ 		HANDLE 			hHeapRegion,
	_In_		LPVOID 			lpData,
	_In_ 		UARCHLONG 		ualSize
){
	EXIT_IF_UNLIKELY_NULL(hHeapRegion, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpData, FALSE);
	EXIT_IF_UNLIKELY_NULL(ualSize, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hHeapRegion), HANDLE_TYPE_DYNAMIC_HEAP_REGION, FALSE);

	LPDYNAMIC_HEAP_REGION lpdhsData;
	lpdhsData = OBJECT_CAST(hHeapRegion, LPDYNAMIC_HEAP_REGION);
	EXIT_IF_UNLIKELY_NULL(lpdhsData, FALSE);

	WaitForSingleObject(lpdhsData->hLock, INFINITE_WAIT_TIME);

	if (lpdhsData->ualCommittedSize + ualSize > lpdhsData->ualRegionSize)
	{ __ExpandRegion(lpdhsData, (lpdhsData->ualCommittedSize + ualSize) - lpdhsData->ualRegionSize); }

	CopyMemory(
		(LPVOID)((UARCHLONG)lpdhsData->lpData + lpdhsData->ualCommittedSize),
		lpData,
		ualSize
	);

	lpdhsData->ualCommittedSize += ualSize;

	ReleaseSingleObject(lpdhsData->hLock);

	return TRUE;
}




_Success_(return != NULLPTR, _Interlocked_Operation_)
PODNET_API
LPVOID
DynamicHeapRegionData(
	_In_ 		HANDLE 			hHeapRegion,
	_In_Opt_ 	LPUARCHLONG 		lpualSize
){
	EXIT_IF_UNLIKELY_NULL(hHeapRegion, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hHeapRegion), HANDLE_TYPE_DYNAMIC_HEAP_REGION, FALSE);

	LPDYNAMIC_HEAP_REGION lpdhsData;
	lpdhsData = OBJECT_CAST(hHeapRegion, LPDYNAMIC_HEAP_REGION);
	EXIT_IF_UNLIKELY_NULL(lpdhsData, FALSE);

	if (NULLPTR != lpualSize)
	{ *lpualSize = lpdhsData->ualCommittedSize; }

	return lpdhsData->lpData;
}