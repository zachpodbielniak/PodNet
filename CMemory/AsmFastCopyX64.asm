; 
; 
; 
;  ____           _ _   _      _   
; |  _ \ ___   __| | \ | | ___| |_ 
; | |_) / _ \ / _` |  \| |/ _ \ __|
; |  __/ (_) | (_| | |\  |  __/ |_ 
; |_|   \___/ \__,_|_| \_|\___|\__|
; 
; 
; General Purpose C Library.
; Copyright (C) 2017-2019 Zach Podbielniak
; 
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU Affero General Public License as
; published by the Free Software Foundation, either version 3 of the
; License, or (at your option) any later version.
; 
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU Affero General Public License for more details.
; 
; You should have received a copy of the GNU Affero General Public License
; along with this program.  If not, see <https://www.gnu.org/licenses/>.
; 
;
; Parameters are passed in RDI, RSI, RDX, RCX, R8, R9, Stack : in that order.


[DEFAULT REL]
[BITS 64]


global __FastCopyX64

segment .data

segment .text


; __FastCopyX64(
;       _In_            LPVOID          lpDestination   RDI,
;       _In_            LPVOID          lpSource        RSI,
;       _In_            QWORD           qwSize         RDX
;);

__FastCopyX64:
        cmp rdi, 0
        je __FastCopyX64Error
        cmp rsi, 0
        je __FastCopyX64Error
        cmp rdx, 0
        je __FastCopyX64Error

        mov rax, rdx
        mov rbx, rdx            ; rbx is main counter
        shr rbx, 4              ; Divide by 16 to get XmmWord Loops
        mov rcx, rbx
        shl rcx, 4
        sub rax, rcx            ; Remainder after 16 bit loops

        cmp rbx, 0
        je __FastCopyX64DoneWithMain

        __FastCopyX64MainLoop:
        movups xmm0, [rsi]
        movups [rdi], xmm0
        add rdi, 16
        add rsi, 16
        dec rbx
        cmp rbx, 0
        jg __FastCopyX64MainLoop

        __FastCopyX64DoneWithMain:
        cmp rax, 1
        jle __FastCopyX64CopyLastByte

        __FastCopyX64CopyLastBytes:
        mov bx, WORD [rsi]
        mov WORD [rdi], bx
        add rdi, 2
        add rsi, 2
        sub rax, 2
        cmp rax, 2
        jge __FastCopyX64CopyLastBytes

        __FastCopyX64CopyLastByte:
        cmp rax, 0
        je __FastCopyX64Success
        mov bl, BYTE [rsi]
        mov BYTE [rdi], bl

        __FastCopyX64Success:
        mov rax, 1
        ret

        __FastCopyX64Error:
        mov rax, 0
        ret