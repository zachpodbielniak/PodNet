/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CDYNAMICHEAPREGION_H
#define CDYNAMICHEAPREGION_H


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../CHandle/CHandle.h"



_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNET_API
HANDLE
CreateDynamicHeapRegion(
	_In_Opt_	HANDLE 			hHeap,
	_In_ 		UARCHLONG 		ualSize,
	_In_ 		BOOL 			bMapSupport,
	_In_Opt_ 	ULONG 			ulFlags
);




_Success_(return != NULL_OBJECT, _Interlocked_Operation_)
PODNET_API
BOOL
DynamicHeapRegionAppend(
	_In_ 		HANDLE 			hHeapRegion,
	_In_		LPVOID 			lpData,
	_In_ 		UARCHLONG 		ualSize
);




_Success_(return != NULLPTR, _Interlocked_Operation_)
PODNET_API
LPVOID
DynamicHeapRegionData(
	_In_ 		HANDLE 			hHeapRegion,
	_In_Opt_ 	LPUARCHLONG 		lpualSize
);



#endif