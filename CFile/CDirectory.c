/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CDirectory.h"
#include "../CString/CString.h"


typedef struct __DIRECTORY
{
	INHERITS_FROM_HANDLE();
	LPSTR 		lpszPath;
	HVECTOR		hvListing;
	DIR 		*dirDescriptor;
} DIRECTORY, *LPDIRECTORY, **DLPDIRECTORY;




_Success_(return != NULLPTR, _Non_Locking_)
INTERNAL_OPERATION
BOOL 
__DestroyDirectoryVectorListing(
	_In_		HVECTOR			hvListing
){
	EXIT_IF_UNLIKELY_NULL(hvListing, FALSE);

	ARCHLONG alIterator;
	LPDIRECTORY_ENTRY lpdeEntry;

	for (
		alIterator = 0;
		alIterator < (ARCHLONG)VectorSize(hvListing);
		alIterator++
	){
		lpdeEntry = VectorAt(hvListing, (ARCHLONG)alIterator);

		if (NULLPTR != lpdeEntry)
		{
			if (NULLPTR != lpdeEntry->lpszFileName)
			{ FreeMemory(lpdeEntry->lpszFileName); }
		}
	}

	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__DestroyDirectory(
	_In_                            HDERIVATIVE             hDerivative,
	_In_Opt_                        ULONG                   ulFlags
){
	UNREFERENCED_PARAMETER(ulFlags);
	EXIT_IF_UNLIKELY_NULL(hDerivative, FALSE);
	LPDIRECTORY lpdObject;

	lpdObject = hDerivative;
	
	if (NULLPTR != lpdObject->hvListing)
	{ __DestroyDirectoryVectorListing(lpdObject->hvListing); }

	if (NULLPTR != lpdObject->lpszPath)
	{ FreeMemory(lpdObject->lpszPath); }

	if (NULLPTR != lpdObject->dirDescriptor)
	{ closedir(lpdObject->dirDescriptor); }

	FreeMemory(lpdObject);
	return TRUE;
}




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNET_API 
HANDLE
OpenDirectory(
	_In_Z_		LPCSTR			lpcszPath
){
	EXIT_IF_UNLIKELY_NULL(lpcszPath, NULL_OBJECT);

	HANDLE hObject;
	LPDIRECTORY lpdObject;

	lpdObject = GlobalAllocAndZero(sizeof(DIRECTORY));
	if (NULLPTR == lpdObject)
	{ return NULL_OBJECT; }

	lpdObject->lpszPath = StringDuplicate(lpcszPath);
	if (NULLPTR == lpdObject->lpszPath)
	{
		FreeMemory(lpdObject);
		return NULL_OBJECT;
	}


	hObject = CreateHandleWithSingleInheritor(
		lpdObject,
		&(lpdObject->hThis),
		HANDLE_TYPE_DIRECTORY,
		__DestroyDirectory,
		0,
		NULLPTR,
		0,
		NULLPTR,
		0,
		&(lpdObject->ullId),
		0
	);

	if (NULL_OBJECT == hObject)
	{
		__DestroyDirectory((HDERIVATIVE)lpdObject, 0);
		return NULL_OBJECT;
	}

	return hObject;
}




_Success_(return == NULLPTR, _Non_Locking_)
INTERNAL_OPERATION
LPVOID 
__DirectoryIterator(	
	_In_		HANDLE			hDirectory,
	_In_ 		LPDIRECTORY_ENTRY	lpdeInfo,
	_In_Opt_	LPVOID			lpParam
){
	UNREFERENCED_PARAMETER(hDirectory);
	EXIT_IF_UNLIKELY_NULL(lpdeInfo, (LPVOID)-1);
	EXIT_IF_UNLIKELY_NULL(lpParam, (LPVOID)-1);

	HVECTOR hvListing;
	hvListing = lpParam;

	if (FALSE == IsValidVectorHandle(hvListing))
	{ return (LPVOID)1; }

	VectorPushBack(hvListing, lpdeInfo, 0);

	return (LPVOID)1;
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API 
BOOL
DirectoryIterate(
	_In_		HANDLE				hDirectory,
	_In_ 		LPFN_DIRECTORY_ITERATOR_PROC	lpfnEntryCallBack,
	_In_Opt_	LPVOID				lpParam
){
	EXIT_IF_UNLIKELY_NULL(hDirectory, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hDirectory), HANDLE_TYPE_DIRECTORY, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpfnEntryCallBack, FALSE);

	LPDIRECTORY lpdObject;
	struct dirent *deIterator;

	lpdObject = OBJECT_CAST(hDirectory, LPDIRECTORY);
	deIterator = NULLPTR;

	if (NULLPTR == lpdObject->lpszPath)
	{ return FALSE; }

	lpdObject->dirDescriptor = opendir((LPCSTR)lpdObject->lpszPath);
	if (NULLPTR == lpdObject->dirDescriptor)
	{ return FALSE; }

	while (NULLPTR != (deIterator = readdir(lpdObject->dirDescriptor)))
	{
		LPVOID lpRet;
		DIRECTORY_ENTRY deData;
		ZeroMemory(&deData, sizeof(DIRECTORY_ENTRY));

		deData.lpszFileName = StringDuplicate(deIterator->d_name);
		deData.bType = deIterator->d_type;

		lpRet = lpfnEntryCallBack(
			hDirectory,
			&deData,
			lpParam
		);

		if (lpRet == 0)
		{ FreeMemory(deData.lpszFileName); }

	}

	closedir(lpdObject->dirDescriptor);
	lpdObject->dirDescriptor = NULLPTR;
	return TRUE;
}



_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HVECTOR
DirectoryToVectorListing(
	_In_		HANDLE			hDirectory
){
	EXIT_IF_UNLIKELY_NULL(hDirectory, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hDirectory), HANDLE_TYPE_DIRECTORY, NULLPTR);

	LPDIRECTORY lpdObject;
	HVECTOR hvListing;
	lpdObject = OBJECT_CAST(hDirectory, LPDIRECTORY);

	if (NULLPTR != lpdObject->hvListing)
	{ __DestroyDirectoryVectorListing(lpdObject->hvListing); }

	hvListing = CreateVector(0x10, sizeof(DIRECTORY), 0);

	if (NULLPTR == hvListing)
	{ return NULLPTR; }


	DirectoryIterate(
		hDirectory,
		__DirectoryIterator,
		hvListing
	);

	lpdObject->hvListing = hvListing;

	return hvListing;
}