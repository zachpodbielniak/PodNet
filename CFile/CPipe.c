/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CPipe.h"
#include "CFileObject.c"




_Success_(return != FALSE, _Acquires_Shared_Lock())
PODNET_API
BOOL 
Pipe(
	_In_		HANDLE RESTRICT		hOut,
	_In_		HANDLE RESTRICT		hIn
){ return PipeEx(hOut, hIn, 0); }




_Success_(return != FALSE, _Acquires_Shared_Lock())
PODNET_API
BOOL 
PipeEx(
	_In_		HANDLE RESTRICT		hOut,
	_In_		HANDLE RESTRICT		hIn,
	_In_Opt_	UARCHLONG		ualFlags
){
	EXIT_IF_UNLIKELY_NULL(hIn, FALSE);
	EXIT_IF_UNLIKELY_NULL(hOut, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hIn), HANDLE_TYPE_FILE, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hOut), HANDLE_TYPE_FILE, FALSE);

	LPFILE_OBJ lpfIn;
	LPFILE_OBJ lpfOut;
	LONG lplFilDes[2];
	LONG lRet;

	lpfIn = OBJECT_CAST(hIn, LPFILE_OBJ);
	lpfOut = OBJECT_CAST(hOut, LPFILE_OBJ);

	if (NULLPTR == lpfIn || NULLPTR == lpfOut)
	{ return FALSE; }

	lplFilDes[0] = lpfOut->lDescriptor;
	lplFilDes[1] = lpfIn->lDescriptor;

	lRet = pipe2(lplFilDes, (LONG)ualFlags);

	if (0 == lRet)
	{
		lpfIn->hRelative = hOut;
		lpfOut->hRelative = hIn;
		lpfIn->bIsPiped = TRUE;
		lpfOut->bIsPiped = TRUE;
		lpfIn->bIsPipeSink = TRUE;
	}

	return (0 == lRet) ? TRUE : FALSE;
}