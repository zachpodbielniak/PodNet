/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/




#include "CFile.h"
#include "CFileObject.c"





GLOBAL_VARIABLE DONT_OPTIMIZE_OUT HANDLE hStdOut = NULLPTR;
GLOBAL_VARIABLE DONT_OPTIMIZE_OUT HANDLE hStdIn = NULLPTR;
GLOBAL_VARIABLE DONT_OPTIMIZE_OUT HANDLE hStdErr = NULLPTR;





_Success_(return != FALSE, _Acquires_Shared_Lock_(hFile->hCritSec, _Has_Lock_Kind_(_Lock_Kind_Critical_Section_)))
INTERNAL_OPERATION
BOOL
__CloseFile(
	_In_                            HDERIVATIVE             hDerivative,
	_In_Opt_                        ULONG                   ulFlags
){
	UNREFERENCED_PARAMETER(ulFlags);
	LPFILE_OBJ lpfoFile;
	lpfoFile = (LPFILE_OBJ)hDerivative;

	EnterCriticalSection(lpfoFile->hCritSec);
	fflush(lpfoFile->fFile);
	if (lpfoFile->bIsLegitFile)
		fclose(lpfoFile->fFile);
	ExitCriticalSection(lpfoFile->hCritSec);
	DestroyHandle(lpfoFile->hCritSec);
	FreeMemory(lpfoFile);
	return TRUE;
}




_Success_(return != FALSE, _Acquires_Shared_Lock_(hFile->hCritSec, _Has_Lock_Kind_(_Lock_Kind_Critical_Section_)))
_Call_Back_
INTERNAL_OPERATION
BOOL
__LockFile(
	_In_                            HANDLE                  hBase
){
	SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(hBase, ERROR_HANDLE_SUPPLIED_WAS_NULL, FALSE);
	SET_ERROR_AND_EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hBase), HANDLE_TYPE_FILE, ERROR_FILE_HANDLE_SUPPLIED_IS_NOT_FILE_HANDLE_TYPE, FALSE);
	LPFILE_OBJ lpfoFile;
	lpfoFile = (LPFILE_OBJ)GetHandleDerivative(hBase);
	SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(lpfoFile, ERROR_HANDLE_DERIVATIVE_WAS_NULL, FALSE);

	return EnterCriticalSection(lpfoFile->hCritSec);
}




_Success_(return != FALSE, _Acquires_Shared_Lock_(hFile->hCritSec, _Has_Lock_Kind_(_Lock_Kind_Critical_Section_)))
_Call_Back_
INTERNAL_OPERATION
BOOL
__TryLockFile(
	_In_                            HANDLE                  hBase
){
	SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(hBase, ERROR_HANDLE_SUPPLIED_WAS_NULL, FALSE);
	SET_ERROR_AND_EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hBase), HANDLE_TYPE_FILE, ERROR_FILE_HANDLE_SUPPLIED_IS_NOT_FILE_HANDLE_TYPE, FALSE);
	LPFILE_OBJ lpfoFile;
	lpfoFile = (LPFILE_OBJ)GetHandleDerivative(hBase);
	SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(lpfoFile, ERROR_HANDLE_DERIVATIVE_WAS_NULL, FALSE);

	return TryToEnterCriticalSection(lpfoFile->hCritSec);
}




_Success_(return != FALSE, _Acquires_Shared_Lock_(hFile->hCritSec, _Has_Lock_Kind_(_Lock_Kind_Critical_Section_)))
_Call_Back_
INTERNAL_OPERATION
BOOL
__UnlockFile(
	_In_                            HANDLE                  hBase
){
	SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(hBase, ERROR_HANDLE_SUPPLIED_WAS_NULL, FALSE);
	SET_ERROR_AND_EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hBase), HANDLE_TYPE_FILE, ERROR_FILE_HANDLE_SUPPLIED_IS_NOT_FILE_HANDLE_TYPE, FALSE);
	LPFILE_OBJ lpfoFile;
	lpfoFile = (LPFILE_OBJ)GetHandleDerivative(hBase);
	SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(lpfoFile, ERROR_HANDLE_DERIVATIVE_WAS_NULL, FALSE);

	return ExitCriticalSection(lpfoFile->hCritSec);
}




_Result_Null_On_Failure_
PODNET_API
HANDLE
CreateFile(
	_In_Z_                          LPCSTR                  lpcszFile,
	_In_                            ULONG                   ulPermissions,
	_In_Opt_                        ULONG                   ulFlags
){
	UNREFERENCED_PARAMETER(ulFlags);
	SET_ERROR_AND_EXIT_IF_NULL(lpcszFile, ERROR_FILE_NAME_NOT_DEFINED, NULLPTR);
	HANDLE hHandle;
	LPFILE_OBJ lpfoFile;
	ULONGLONG ullID;
	lpfoFile = GlobalAllocAndZero(sizeof(FILE_OBJ));
	EXIT_IF_NULL(lpfoFile, 0);
	ulPermissions = FILE_PERMISSION_READ | FILE_PERMISSION_WRITE | FILE_PERMISSION_CREATE;

	lpfoFile->lpszFileName = (LPSTR)lpcszFile;
	lpfoFile->ulOpenFlags = ulPermissions;
	lpfoFile->bIsLegitFile = TRUE;
	lpfoFile->ftType = FILE_TYPE_FILE;

	lpfoFile->fFile = fopen(lpcszFile, "w+");
	if (NULLPTR == lpfoFile->fFile)
	{
		SetLastGlobalError(ERROR_FILE_COULD_NOT_BE_OPENED);
		FreeMemory(lpfoFile);
		return NULL_OBJECT;
	}

	lpfoFile->lDescriptor = fileno(lpfoFile->fFile);
	if (-1 == lpfoFile->lDescriptor)
	{
		SetLastGlobalError(ERROR_FILE_COULD_NOT_BE_OPENED);
		fclose(lpfoFile->fFile);
		FreeMemory(lpfoFile);
		return NULL_OBJECT;
	}
	

	hHandle = CreateHandleWithSingleInheritor(
			lpfoFile,
			&(lpfoFile->hThis),
			HANDLE_TYPE_FILE,
			__CloseFile,
			0,
			NULLPTR,
			0,
			NULLPTR,
			0,
			&ullID,
			0
	);

	if (NULL_OBJECT == hHandle)
	{
		SetLastGlobalError(ERROR_FILE_HANDLE_COULD_NOT_BE_CREATED);
		fclose(lpfoFile->fFile);
		FreeMemory(lpfoFile);
		return NULL_OBJECT;
	}

	SetHandleLockFunctions(hHandle, __LockFile, __UnlockFile, __TryLockFile);

	lpfoFile->hCritSec = CreateCriticalSection();
	return hHandle;
}




_Result_Null_On_Failure_
PODNET_API
HANDLE
CreateTemporaryFile(
	_Reserved_Must_Be_Null_		ULONG			ulFlags
){
	UNREFERENCED_PARAMETER(ulFlags);
	HANDLE hHandle;
	LPFILE_OBJ lpfoFile;
	ULONGLONG ullID;
	lpfoFile = GlobalAllocAndZero(sizeof(FILE_OBJ));
	EXIT_IF_NULL(lpfoFile, 0);

	lpfoFile->lpszFileName = "TemporaryFile";
	lpfoFile->ulOpenFlags = FILE_PERMISSION_READ | FILE_PERMISSION_WRITE | FILE_PERMISSION_CREATE;
	lpfoFile->bIsLegitFile = TRUE;
	lpfoFile->ftType = FILE_TYPE_TEMP;

	lpfoFile->fFile = tmpfile();
	
	if (NULLPTR == lpfoFile->fFile)
	{
		SetLastGlobalError(ERROR_FILE_COULD_NOT_BE_OPENED);
		FreeMemory(lpfoFile);
		return NULL_OBJECT;
	}

	lpfoFile->lDescriptor = fileno(lpfoFile->fFile);
	
	hHandle = CreateHandleWithSingleInheritor(
			lpfoFile,
			&(lpfoFile->hThis),
			HANDLE_TYPE_FILE,
			__CloseFile,
			0,
			NULLPTR,
			0,
			NULLPTR,
			0,
			&ullID,
			0
	);
	SET_ERROR_AND_EXIT_IF_NULL(hHandle, ERROR_FILE_HANDLE_COULD_NOT_BE_CREATED, NULLPTR);

	SetHandleLockFunctions(hHandle, __LockFile, __UnlockFile, __TryLockFile);

	lpfoFile->hCritSec = CreateCriticalSection();
	SET_ERROR_AND_EXIT_IF_NULL(lpfoFile->hCritSec, ERROR_FILE_CRITICAL_SECTION_COULD_NOT_BE_CREATED, NULLPTR);

	return hHandle;
}




_Result_Null_On_Failure_
PODNET_API
HANDLE
OpenFile(
	_In_Z_                          LPCSTR                  lpcszFile,
	_In_                            ULONG                   ulPermissions,
	_In_Opt_                        ULONG                   ulFlags
){
	UNREFERENCED_PARAMETER(ulFlags);
	SET_ERROR_AND_EXIT_IF_NULL(lpcszFile, ERROR_FILE_NAME_NOT_DEFINED, NULLPTR);
	HANDLE hHandle;
	LPFILE_OBJ lpfoFile;
	ULONGLONG ullID;
	lpfoFile = GlobalAllocAndZero(sizeof(FILE_OBJ));
	EXIT_IF_NULL(lpfoFile, 0);

	lpfoFile->lpszFileName = (LPSTR)lpcszFile;
	lpfoFile->ulOpenFlags = ulPermissions;
	lpfoFile->bIsLegitFile = TRUE;
	lpfoFile->ftType = FILE_TYPE_FILE;


	switch (ulPermissions)
	{
		/* Binary */
		case (FILE_PERMISSION_READ | FILE_PERMISSION_WRITE | FILE_PERMISSION_CREATE | FILE_PERMISSION_BINARY):
		{
			lpfoFile->fFile = fopen(lpcszFile, "w+b");
			break;
		}

		case (FILE_PERMISSION_WRITE | FILE_PERMISSION_CREATE | FILE_PERMISSION_BINARY):
		{
			lpfoFile->fFile = fopen(lpcszFile, "w+b");
			break;
		}

		case (FILE_PERMISSION_READ | FILE_PERMISSION_WRITE | FILE_PERMISSION_BINARY):
		{
			lpfoFile->fFile = fopen(lpcszFile, "r+b");
			break;
		}

		case (FILE_PERMISSION_APPEND | FILE_PERMISSION_READ | FILE_PERMISSION_BINARY):
		{
			lpfoFile->fFile = fopen(lpcszFile, "a+b");
			break;
		}

		case (FILE_PERMISSION_APPEND | FILE_PERMISSION_BINARY):
		{
			lpfoFile->fFile = fopen(lpcszFile, "ab");
			break;
		}

		case (FILE_PERMISSION_WRITE | FILE_PERMISSION_BINARY):
		{
			lpfoFile->fFile = fopen(lpcszFile, "wb");
			break;
		}

		case (FILE_PERMISSION_READ | FILE_PERMISSION_BINARY):
		{
			lpfoFile->fFile = fopen(lpcszFile, "rb");
			break;
		}
	
		/* Text */
		case (FILE_PERMISSION_READ | FILE_PERMISSION_WRITE | FILE_PERMISSION_CREATE):
		{
			lpfoFile->fFile = fopen(lpcszFile, "w+");
			break;
		}

		case (FILE_PERMISSION_WRITE | FILE_PERMISSION_CREATE):
		{
			lpfoFile->fFile = fopen(lpcszFile, "w+");
			break;
		}

		case (FILE_PERMISSION_READ | FILE_PERMISSION_WRITE):
		{
			lpfoFile->fFile = fopen(lpcszFile, "r+");
			break;
		}
	
		case (FILE_PERMISSION_APPEND | FILE_PERMISSION_READ):
		{
			lpfoFile->fFile = fopen(lpcszFile, "a+");
			break;
		}

		case (FILE_PERMISSION_APPEND):
		{
			lpfoFile->fFile = fopen(lpcszFile, "a");
			break;
		}

		case (FILE_PERMISSION_WRITE):
		{
			lpfoFile->fFile = fopen(lpcszFile, "w");
			break;
		}

		case (FILE_PERMISSION_READ):
		{
			lpfoFile->fFile = fopen(lpcszFile, "r");
			break;
		}

	}
	
	if (NULLPTR == lpfoFile->fFile)
	{
		SetLastGlobalError(ERROR_FILE_COULD_NOT_BE_OPENED);
		FreeMemory(lpfoFile);
		return NULL_OBJECT;
	}

	lpfoFile->lDescriptor = fileno(lpfoFile->fFile);
	lpfoFile->hCritSec = CreateCriticalSection();

	if (NULL_OBJECT == lpfoFile->hCritSec)
	{
		SetLastGlobalError(ERROR_FILE_CRITICAL_SECTION_COULD_NOT_BE_CREATED);
		fclose(lpfoFile->fFile);
		FreeMemory(lpfoFile);
		return NULLPTR;
	}

	hHandle = CreateHandleWithSingleInheritor(
			lpfoFile,
			&(lpfoFile->hThis),
			HANDLE_TYPE_FILE,
			__CloseFile,
			0,
			NULLPTR,
			0,
			NULLPTR,
			0,
			&ullID,
			0
	);

	if (NULL_OBJECT == lpfoFile->hCritSec)
	{
		SetLastGlobalError(ERROR_FILE_HANDLE_COULD_NOT_BE_CREATED);
		DestroyObject(lpfoFile->hCritSec);
		fclose(lpfoFile->fFile);
		FreeMemory(lpfoFile);
		return NULLPTR;
	}

	SetHandleLockFunctions(hHandle, __LockFile, __UnlockFile, __TryLockFile);
	return hHandle;
}




_Result_Null_On_Failure_
PODNET_API
HANDLE
OpenFileFromMemory(
	_In_				LPVOID			lpBuffer,
	_In_				UARCHLONG		ualSize,
	_In_Opt_                        ULONG                   ulPermissions,
	_In_Opt_                        ULONG                   ulFlags
){
	UNREFERENCED_PARAMETER(ulFlags);
	SET_ERROR_AND_EXIT_IF_NULL(lpBuffer, ERROR_FILE_NAME_NOT_DEFINED, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NULL(ualSize, NULL_OBJECT);
	HANDLE hHandle;
	LPFILE_OBJ lpfoFile;
	ULONGLONG ullID;
	lpfoFile = GlobalAllocAndZero(sizeof(FILE_OBJ));
	EXIT_IF_NULL(lpfoFile, 0);

	lpfoFile->lpszFileName = NULLPTR;
	lpfoFile->ulOpenFlags = ulPermissions;
	lpfoFile->bIsLegitFile = FALSE;
	lpfoFile->ftType = FILE_TYPE_MEM;
	lpfoFile->lDescriptor = -1;


	switch (ulPermissions)
	{
		/* Binary */
		case (FILE_PERMISSION_READ | FILE_PERMISSION_WRITE | FILE_PERMISSION_CREATE | FILE_PERMISSION_BINARY):
		{
			lpfoFile->fFile = fmemopen(lpBuffer, ualSize, "w+b");
			break;
		}

		case (FILE_PERMISSION_WRITE | FILE_PERMISSION_CREATE | FILE_PERMISSION_BINARY):
		{
			lpfoFile->fFile = fmemopen(lpBuffer, ualSize, "w+b");
			break;
		}

		case (FILE_PERMISSION_READ | FILE_PERMISSION_WRITE | FILE_PERMISSION_BINARY):
		{
			lpfoFile->fFile = fmemopen(lpBuffer, ualSize, "r+b");
			break;
		}

		case (FILE_PERMISSION_APPEND | FILE_PERMISSION_READ | FILE_PERMISSION_BINARY):
		{
			lpfoFile->fFile = fmemopen(lpBuffer, ualSize, "a+b");
			break;
		}

		case (FILE_PERMISSION_APPEND | FILE_PERMISSION_BINARY):
		{
			lpfoFile->fFile = fmemopen(lpBuffer, ualSize, "ab");
			break;
		}

		case (FILE_PERMISSION_WRITE | FILE_PERMISSION_BINARY):
		{
			lpfoFile->fFile = fmemopen(lpBuffer, ualSize, "wb");
			break;
		}

		case (FILE_PERMISSION_READ | FILE_PERMISSION_BINARY):
		{
			lpfoFile->fFile = fmemopen(lpBuffer, ualSize, "rb");
			break;
		}
	
		/* Text */
		case (FILE_PERMISSION_READ | FILE_PERMISSION_WRITE | FILE_PERMISSION_CREATE):
		{
			lpfoFile->fFile = fmemopen(lpBuffer, ualSize, "w+");
			break;
		}

		case (FILE_PERMISSION_WRITE | FILE_PERMISSION_CREATE):
		{
			lpfoFile->fFile = fmemopen(lpBuffer, ualSize, "w+");
			break;
		}

		case (FILE_PERMISSION_READ | FILE_PERMISSION_WRITE):
		{
			lpfoFile->fFile = fmemopen(lpBuffer, ualSize, "r+");
			break;
		}
	
		case (FILE_PERMISSION_APPEND | FILE_PERMISSION_READ):
		{
			lpfoFile->fFile = fmemopen(lpBuffer, ualSize, "a+");
			break;
		}

		case (FILE_PERMISSION_APPEND):
		{
			lpfoFile->fFile = fmemopen(lpBuffer, ualSize, "a");
			break;
		}

		case (FILE_PERMISSION_WRITE):
		{
			lpfoFile->fFile = fmemopen(lpBuffer, ualSize, "w");
			break;
		}

		case (FILE_PERMISSION_READ):
		{
			lpfoFile->fFile = fmemopen(lpBuffer, ualSize, "r");
			break;
		}

	}
	
	if (NULLPTR == lpfoFile->fFile)
	{
		SetLastGlobalError(ERROR_FILE_COULD_NOT_BE_OPENED);
		FreeMemory(lpfoFile);
		return NULL_OBJECT;
	}
	
	lpfoFile->hCritSec = CreateCriticalSection();
	if (NULL_OBJECT == lpfoFile->hCritSec)
	{
		fclose(lpfoFile->fFile);
		SetLastGlobalError(ERROR_FILE_COULD_NOT_BE_OPENED);
		FreeMemory(lpfoFile);
		return NULL_OBJECT;
	}

	hHandle = CreateHandleWithSingleInheritor(
			lpfoFile,
			&(lpfoFile->hThis),
			HANDLE_TYPE_FILE,
			__CloseFile,
			0,
			NULLPTR,
			0,
			NULLPTR,
			0,
			&ullID,
			0
	);
	
	if (NULL_OBJECT == lpfoFile->hCritSec)
	{
		SetLastGlobalError(ERROR_FILE_HANDLE_COULD_NOT_BE_CREATED);
		DestroyObject(lpfoFile->hCritSec);
		fclose(lpfoFile->fFile);
		SetLastGlobalError(ERROR_FILE_COULD_NOT_BE_OPENED);
		FreeMemory(lpfoFile);
		return NULL_OBJECT;
	}

	SetHandleLockFunctions(hHandle, __LockFile, __UnlockFile, __TryLockFile);


	return hHandle;
}



_Result_Null_On_Failure_
PODNET_API
HANDLE
OpenFileAndDefineReadAndWriteProc(
	_In_Opt_Z_                      LPCSTR                  lpcszFile,
	_In_Opt_                        LPFN_HANDLE_CUSTOM_PROC lpfnReadProc,
	_In_Opt_                        LPFN_HANDLE_CUSTOM_PROC lpfnWriteProc,
	_In_Opt_                        ULONG                   ulPermissions,
	_In_Opt_                        ULONG                   ulFlags
){
	UNREFERENCED_PARAMETER(ulFlags);
	HANDLE hHandle;
	LPFILE_OBJ lpfoFile;
	ULONGLONG ullID;
	lpfoFile = GlobalAllocAndZero(sizeof(FILE_OBJ));
	EXIT_IF_NULL(lpfoFile, 0);

	if (lpcszFile != NULLPTR)
	{
		lpfoFile->lpszFileName = (LPSTR)lpcszFile;
		lpfoFile->bIsLegitFile = TRUE;
		lpfoFile->ftType = FILE_TYPE_FILE;

	       
		switch (ulPermissions)
		{
			/* Binary */
			case (FILE_PERMISSION_READ | FILE_PERMISSION_WRITE | FILE_PERMISSION_CREATE | FILE_PERMISSION_BINARY):
			{
				lpfoFile->fFile = fopen(lpcszFile, "w+b");
				break;
			}

			case (FILE_PERMISSION_WRITE | FILE_PERMISSION_CREATE | FILE_PERMISSION_BINARY):
			{
				lpfoFile->fFile = fopen(lpcszFile, "w+b");
				break;
			}

			case (FILE_PERMISSION_READ | FILE_PERMISSION_WRITE | FILE_PERMISSION_BINARY):
			{
				lpfoFile->fFile = fopen(lpcszFile, "r+b");
				break;
			}

			case (FILE_PERMISSION_APPEND | FILE_PERMISSION_READ | FILE_PERMISSION_BINARY):
			{
				lpfoFile->fFile = fopen(lpcszFile, "a+b");
				break;
			}

			case (FILE_PERMISSION_APPEND | FILE_PERMISSION_BINARY):
			{
				lpfoFile->fFile = fopen(lpcszFile, "ab");
				break;
			}

			case (FILE_PERMISSION_WRITE | FILE_PERMISSION_BINARY):
			{
				lpfoFile->fFile = fopen(lpcszFile, "wb");
				break;
			}

			case (FILE_PERMISSION_READ | FILE_PERMISSION_BINARY):
			{
				lpfoFile->fFile = fopen(lpcszFile, "rb");
				break;
			}

			/* Text */
			case (FILE_PERMISSION_READ | FILE_PERMISSION_WRITE | FILE_PERMISSION_CREATE):
			{
				lpfoFile->fFile = fopen(lpcszFile, "w+");
				break;
			}

			case (FILE_PERMISSION_WRITE | FILE_PERMISSION_CREATE):
			{
				lpfoFile->fFile = fopen(lpcszFile, "w+");
				break;
			}

			case (FILE_PERMISSION_READ | FILE_PERMISSION_WRITE):
			{
				lpfoFile->fFile = fopen(lpcszFile, "r+");
				break;
			}

			case (FILE_PERMISSION_APPEND | FILE_PERMISSION_READ):
			{
				lpfoFile->fFile = fopen(lpcszFile, "a+");
				break;
			}

			case (FILE_PERMISSION_APPEND):
			{
			lpfoFile->fFile = fopen(lpcszFile, "a");
			break;
			}

			case (FILE_PERMISSION_WRITE):
			{
				lpfoFile->fFile = fopen(lpcszFile, "w");
				break;
			}

			case (FILE_PERMISSION_READ):
			{
				lpfoFile->fFile = fopen(lpcszFile, "r");
				break;
			}

		}

	}

	if (NULLPTR == lpfoFile->fFile)
	{
		SetLastGlobalError(ERROR_FILE_COULD_NOT_BE_OPENED);
		FreeMemory(lpfoFile);
		return NULL_OBJECT;
	}
	
	lpfoFile->hCritSec = CreateCriticalSection();
	if (NULL_OBJECT == lpfoFile->hCritSec)
	{
		fclose(lpfoFile->fFile);
		SetLastGlobalError(ERROR_FILE_COULD_NOT_BE_OPENED);
		FreeMemory(lpfoFile);
		return NULL_OBJECT;
	}

	lpfoFile->lDescriptor = fileno(lpfoFile->fFile);
	lpfoFile->ulOpenFlags = ulPermissions;
	lpfoFile->lpfnReadProc = lpfnReadProc;
	lpfoFile->lpfnWriteProc = lpfnWriteProc;

	hHandle = CreateHandleWithSingleInheritor(
			lpfoFile,
			&(lpfoFile->hThis),
			HANDLE_TYPE_FILE,
			__CloseFile,
			0,
			NULLPTR,
			0,
			NULLPTR,
			0,
			&ullID,
			0
	);

	if (NULL_OBJECT == lpfoFile->hCritSec)
	{
		SetLastGlobalError(ERROR_FILE_HANDLE_COULD_NOT_BE_CREATED);
		DestroyObject(lpfoFile->hCritSec);
		fclose(lpfoFile->fFile);
		SetLastGlobalError(ERROR_FILE_COULD_NOT_BE_OPENED);
		FreeMemory(lpfoFile);
		return NULL_OBJECT;
	}

	SetHandleLockFunctions(hHandle, __LockFile, __UnlockFile, __TryLockFile);
	return hHandle;
}




_Result_Null_On_Failure_
PODNET_API
HANDLE
OpenFileWithFilePointer(
	_In_ 				FILE			*lpfFilePointer,
	_In_ 				ULONG 			ulPermission
){

	HANDLE hHandle;
	LPFILE_OBJ lpfoFile;
	lpfoFile = GlobalAllocAndZero(sizeof(FILE_OBJ));
	EXIT_IF_NULL(lpfoFile, NULL_OBJECT);

	lpfoFile->bIsLegitFile = TRUE;
	lpfoFile->fFile = lpfFilePointer;
	lpfoFile->ulOpenFlags = ulPermission;
	lpfoFile->lDescriptor = fileno(lpfoFile->fFile);
	lpfoFile->ftType = FILE_TYPE_OTHER;
	lpfoFile->hCritSec = CreateCriticalSection();

	if (NULL_OBJECT == lpfoFile->hCritSec)
	{
		fclose(lpfFilePointer);
		FreeMemory(lpfoFile);
		return NULL_OBJECT;
	}

	hHandle = CreateHandleWithSingleInheritor(
			lpfoFile,
			&(lpfoFile->hThis),
			HANDLE_TYPE_FILE,
			__CloseFile,
			0,
			NULLPTR,
			0,
			NULLPTR,
			0,
			&(lpfoFile->ullId),
			0
	);

	if (NULL_OBJECT == hHandle)
	{
		fclose(lpfFilePointer);
		DestroyObject(lpfoFile->hCritSec);
		FreeMemory(lpfoFile);
		return NULL_OBJECT;
	}

	SetHandleLockFunctions(hHandle, __LockFile, __UnlockFile, __TryLockFile);


	return hHandle;
}




_Success_(return != FALSE, _Acquires_Shared_Lock_(hFile->hCritSec, _Has_Lock_Kind_(_Lock_Kind_Critical_Section_)))
PODNET_API
BOOL
WriteFile(
	_In_                            HANDLE                  hFile,
	_In_Z_                          LPCSTR                  lpcszStringToWrite,
	_In_Opt_                        ULONG                   ulFlags
){
	SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(hFile, ERROR_HANDLE_SUPPLIED_WAS_NULL, FALSE);
	
	if (GetHandleDerivativeType(hFile) != HANDLE_TYPE_FILE)
	{
		Write(hFile, (LPVOID)lpcszStringToWrite, StringLength(lpcszStringToWrite), NULL, ulFlags);
		return TRUE;
	}

	LPFILE_OBJ lpfoFile;
	lpfoFile = (LPFILE_OBJ)GetHandleDerivative(hFile);
	SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(lpfoFile, ERROR_HANDLE_DERIVATIVE_WAS_NULL, FALSE);


	if (FALSE == (lpfoFile->ulOpenFlags & FILE_PERMISSION_WRITE) && FALSE == (lpfoFile->ulOpenFlags & FILE_PERMISSION_APPEND))
	{
		SetLastGlobalError(ERROR_FILE_NOT_OPENED_WITH_WRITE_FLAG);
		return FALSE;
	}
 
	if (lpfoFile->lpfnWriteProc != NULLPTR)
		lpfoFile->lpfnWriteProc(hFile, NULLPTR, (LPVOID)lpcszStringToWrite, NULL);

	EnterCriticalSection(lpfoFile->hCritSec);
	
	fwrite((LPCVOID)lpcszStringToWrite, sizeof(CHAR), StringLength(lpcszStringToWrite), lpfoFile->fFile);

	if (ulFlags & FILE_WRITE_FLUSH)
	{ fflush(lpfoFile->fFile); }

	ExitCriticalSection(lpfoFile->hCritSec);
	return TRUE;
}




_Success_(return != FALSE, _Acquires_Shared_Lock_(hFile->hCritSec, _Has_Lock_Kind_(_Lock_Kind_Critical_Section_)))
PODNET_API
BOOL
WriteDataToFile(
	_In_ 				HANDLE 			hFile,
	_In_ 				LPBYTE			lpbData,
	_In_ 				ULONGLONG 		ullSize,
	_Reserved_Must_Be_Null_ 	LPVOID 			lpReserved,
	_In_Opt_ 			ULONG 			ulFlags
){
	UNREFERENCED_PARAMETER(lpReserved);
	SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(hFile, ERROR_HANDLE_SUPPLIED_WAS_NULL, FALSE);
	
	if (GetHandleDerivativeType(hFile) != HANDLE_TYPE_FILE)
	{
		Write(hFile, (LPVOID)lpbData, ullSize, NULL, ulFlags);
		return TRUE;
	}

	LPFILE_OBJ lpfoFile;
	lpfoFile = (LPFILE_OBJ)GetHandleDerivative(hFile);
	SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(lpfoFile, ERROR_HANDLE_DERIVATIVE_WAS_NULL, FALSE);


	if (!(lpfoFile->ulOpenFlags & FILE_PERMISSION_WRITE))
	{
		SetLastGlobalError(ERROR_FILE_NOT_OPENED_WITH_WRITE_FLAG);
		return FALSE;
	}
 
	if (lpfoFile->lpfnWriteProc != NULLPTR)
		lpfoFile->lpfnWriteProc(hFile, NULLPTR, (LPVOID)lpbData, NULL);

	EnterCriticalSection(lpfoFile->hCritSec);
	
	fwrite((LPCVOID)lpbData, sizeof(BYTE), ullSize, lpfoFile->fFile);

	if (ulFlags & FILE_WRITE_FLUSH)
	{ fflush(lpfoFile->fFile); }

	ExitCriticalSection(lpfoFile->hCritSec);
	return TRUE;
}




typedef struct _WRITEFILE_PROMISE
{
	HANDLE          hFile;
	LPSTR           lpszStringToWrite;
} WRITEFILE_PROMISE, *LPWRITEFILE_PROMISE;




_Promise_Proc_
INTERNAL_OPERATION
UARCHLONG
__WriteFilePromiseProc(
	_In_                            LPVOID                  lpParam,
	_In_                            DLPVOID                 dlpOut
){
	SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(lpParam, ERROR_PROMISE_NOT_SUPPLIED_WITH_DATA, 0);
	SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(dlpOut, ERROR_PROMISE_NOT_SUPPLIED_WITH_ALLOCABLE_BUFFER, 0);
	
	LPWRITEFILE_PROMISE lpwfpData;
	lpwfpData = (LPWRITEFILE_PROMISE)lpParam;
	*dlpOut = LocalAlloc(sizeof(BOOL));
	SET_ERROR_AND_EXIT_IF_NULL(*dlpOut, ERROR_PROMISE_COULD_NOT_ALLOC_BUFFER, 0);

	**(DLPBOOL)(dlpOut) = WriteFile(lpwfpData->hFile, lpwfpData->lpszStringToWrite, NULL);

	FreeMemory(lpwfpData);
	return sizeof(BOOL);
}




/* Returns HANDLE to a Promise */
_Result_Null_On_Failure_
_Spawns_New_Thread_Async_And_Locks_(hFile->hCritSec, _Has_Lock_Kind_(_Lock_Kind_Critical_Section_))
PODNET_API
HANDLE
WriteFileAsynchronously(
	_In_                            HANDLE                  hFile,
	_In_Z_                          LPCSTR                  lpcszStringToWrite,
	_In_Opt_                        ULONG                   ulFlags
){
	UNREFERENCED_PARAMETER(ulFlags);
	SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(hFile, ERROR_HANDLE_SUPPLIED_WAS_NULL, FALSE);
	SET_ERROR_AND_EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hFile), HANDLE_TYPE_FILE, ERROR_FILE_HANDLE_SUPPLIED_IS_NOT_FILE_HANDLE_TYPE, FALSE);
	HANDLE hPromise;
	LPWRITEFILE_PROMISE lpwfpData;
	lpwfpData = LocalAllocAndZero(sizeof(WRITEFILE_PROMISE));
	SET_ERROR_AND_EXIT_IF_NULL(lpwfpData, ERROR_FILE_PROMISE_DATA_COULD_NOT_BE_ALLOC, NULLPTR);

	lpwfpData->hFile = hFile;
	lpwfpData->lpszStringToWrite = (LPSTR)lpcszStringToWrite;
	
	hPromise = CreatePromise(__WriteFilePromiseProc, (LPVOID)lpwfpData, NULL, NULLPTR, NULL);
	SET_ERROR_AND_EXIT_IF_NULL(hPromise, ERROR_PROMISE_COULD_NOT_BE_CREATED, NULLPTR);
	return hPromise;
}






_Success_(return != FALSE, _Acquires_Shared_Lock_(hFile->hCritSec, _Has_Lock_Kind_(_Lock_Kind_Critical_Section_)))
PODNET_API
ARCHLONG
ReadFile(
	_In_                            HANDLE                  hFile,
	_Out_Writes_To_Ptr_Z_(dlpstrOut)DLPSTR                  dlpstrOut,
	_In_                            ULONGLONG               ullBytesToRead,
	_In_Opt_                        ULONG                   ulFlags
){
	SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(hFile, ERROR_HANDLE_SUPPLIED_WAS_NULL, FALSE);
	SET_ERROR_AND_EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hFile), HANDLE_TYPE_FILE, ERROR_FILE_HANDLE_SUPPLIED_IS_NOT_FILE_HANDLE_TYPE, FALSE);
	LPFILE_OBJ lpfoFile;
	ARCHLONG alOut;
	lpfoFile = (LPFILE_OBJ)GetHandleDerivative(hFile);
	SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(lpfoFile, ERROR_HANDLE_DERIVATIVE_WAS_NULL, FALSE);

	if (!(lpfoFile->ulOpenFlags & FILE_PERMISSION_READ))
	{
		SetLastGlobalError(ERROR_FILE_NOT_OPENED_WITH_READ_FLAG);
		return FALSE;
	}
	
	if (NULLPTR != lpfoFile->lpfnReadProc)
		lpfoFile->lpfnReadProc(hFile, NULLPTR, (LPVOID)dlpstrOut, NULL);

	EnterCriticalSection(lpfoFile->hCritSec);
	if (ulFlags & FILE_READ_ALLOC_OUT)
		*dlpstrOut = LocalAllocAndZero(ullBytesToRead + 16);
 	/* ullFileSize = fread(*dlpstrOut, sizeof(CHAR), ullBytesToRead, lpfoFile->fFile); */
	alOut = read(fileno(lpfoFile->fFile), *dlpstrOut, ullBytesToRead);
	

	ExitCriticalSection(lpfoFile->hCritSec);
	return alOut;
}




_Success_(return != FALSE, _Acquires_Shared_Lock_(hFile->hCritSec, _Has_Lock_Kind_(_Lock_Kind_Critical_Section_)))
PODNET_API
ARCHLONG
ReadLineFromFile(
	_In_ 				HANDLE 			hFile,
	_Out_Writes_To_Ptr_Z_(dlpstrOut)DLPSTR 			dlpstrOut,
	_In_ 				UARCHLONG 		ualSize,
	_In_Opt_ 			ULONG 			ulFlags
){
	UNREFERENCED_PARAMETER(ulFlags);
	
	SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(hFile, ERROR_HANDLE_SUPPLIED_WAS_NULL, FALSE);
	SET_ERROR_AND_EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hFile), HANDLE_TYPE_FILE, ERROR_FILE_HANDLE_SUPPLIED_IS_NOT_FILE_HANDLE_TYPE, FALSE);
	LPFILE_OBJ lpfoFile;
	LPSTR lpszTemp;
	UARCHLONG alRead;
	lpfoFile = (LPFILE_OBJ)GetHandleDerivative(hFile);
	SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(lpfoFile, ERROR_HANDLE_DERIVATIVE_WAS_NULL, FALSE);
	alRead = -1;

	WaitForSingleObject(lpfoFile->hCritSec, INFINITE_WAIT_TIME);

	if (FILE_READ_SINGLE_POINTER_SAFE & ulFlags)
	{
		lpszTemp = (LPSTR)dlpstrOut;
		alRead = getline(&lpszTemp, &ualSize, lpfoFile->fFile);
	}
	else 
	{ alRead = getline(dlpstrOut, &ualSize, lpfoFile->fFile); }
	
	ReleaseSingleObject(lpfoFile->hCritSec);

	return alRead;
}




typedef struct _READFILE_PROMISE
{
	HANDLE          hFile;
	ULONGLONG       ullBytesToRead;
} READFILE_PROMISE, *LPREADFILE_PROMISE;




_Promise_Proc_
INTERNAL_OPERATION
UARCHLONG
__ReadFilePromiseProc(
	_In_                            LPVOID                  lpParam,
	_In_                            DLPVOID                 dlpOut
){
	SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(lpParam, ERROR_PROMISE_NOT_SUPPLIED_WITH_DATA, 0);
	SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(dlpOut, ERROR_PROMISE_NOT_SUPPLIED_WITH_ALLOCABLE_BUFFER, 0);
	
	LPREADFILE_PROMISE lprfpData;
	UARCHLONG ualSize;
	lprfpData = (LPREADFILE_PROMISE)lpParam;
	*dlpOut = LocalAlloc(sizeof(CHAR) * lprfpData->ullBytesToRead);
	SET_ERROR_AND_EXIT_IF_NULL(*dlpOut, ERROR_PROMISE_COULD_NOT_ALLOC_BUFFER, 0);
	ualSize = lprfpData->ullBytesToRead;

	ReadFile(lprfpData->hFile, (DLPSTR)dlpOut, ualSize, NULL);

	FreeMemory(lprfpData);
	return (sizeof(CHAR) * ualSize);
}



/* Returns HANDLE to a Promise */
_Result_Null_On_Failure_
_Spawns_New_Thread_Async_And_Locks_(hFile->hCritSec, _Has_Lock_Kind_(_Lock_Kind_Critical_Section_))
PODNET_API
HANDLE
ReadFileAsynchronously(
	_In_                            HANDLE                  hFile,
	_In_                            ULONGLONG               ullBytesToRead
){
	SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(hFile, ERROR_HANDLE_SUPPLIED_WAS_NULL, NULLPTR);
	SET_ERROR_AND_EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hFile), HANDLE_TYPE_FILE, ERROR_FILE_HANDLE_SUPPLIED_IS_NOT_FILE_HANDLE_TYPE, NULLPTR);
	SET_ERROR_AND_EXIT_IF_NULL(ullBytesToRead, ERROR_FILE_NO_BUFFER_SIZE_WAS_SUPPLIED, NULLPTR);

	HANDLE hPromise;
	LPREADFILE_PROMISE lprfpData;
	lprfpData = LocalAllocAndZero(sizeof(READFILE_PROMISE));
	SET_ERROR_AND_EXIT_IF_NULL(lprfpData, ERROR_FILE_PROMISE_DATA_COULD_NOT_BE_ALLOC, NULLPTR);

	lprfpData->hFile = hFile;
	lprfpData->ullBytesToRead = ullBytesToRead;

	hPromise = CreatePromise(__ReadFilePromiseProc, (LPVOID)lprfpData, NULL, NULLPTR, NULL);
	SET_ERROR_AND_EXIT_IF_NULL(hPromise, ERROR_PROMISE_COULD_NOT_BE_CREATED, NULLPTR);
	return hPromise;
}






_Success_(return != FALSE, _Acquires_Shared_Lock_(hFile->hCritSec, _Has_Lock_Kind_(_Lock_Kind_Critical_Section_)))
PODNET_API
BOOL
ReadWholeFile(
	_In_                            HANDLE                  hFile,
	_Out_Writes_To_Ptr_Z_(dlpstrOut)DLPSTR                  dlpstrOut,
	_In_                            ULONG                   ulFlags    
){
	SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(hFile, ERROR_HANDLE_SUPPLIED_WAS_NULL, FALSE);
	SET_ERROR_AND_EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hFile), HANDLE_TYPE_FILE, ERROR_FILE_HANDLE_SUPPLIED_IS_NOT_FILE_HANDLE_TYPE, FALSE);
	LPFILE_OBJ lpfoFile;
	ULONGLONG ullFileSize;
	lpfoFile = (LPFILE_OBJ)GetHandleDerivative(hFile);
	SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(lpfoFile, ERROR_HANDLE_DERIVATIVE_WAS_NULL, FALSE);

	if (!(lpfoFile->ulOpenFlags & FILE_PERMISSION_READ))

	{
		SetLastGlobalError(ERROR_FILE_NOT_OPENED_WITH_READ_FLAG);
		return FALSE;
	}

	ullFileSize = GetFileSize(hFile);

	EnterCriticalSection(lpfoFile->hCritSec);

	if (ulFlags & FILE_READ_ALLOC_OUT)
		*dlpstrOut = LocalAlloc(ullFileSize + 16ULL);
	ullFileSize = fread(*dlpstrOut, sizeof(CHAR), ullFileSize, lpfoFile->fFile);

	ExitCriticalSection(lpfoFile->hCritSec);
	return (ullFileSize > 0) ? TRUE : FALSE;
}





_Promise_Proc_
INTERNAL_OPERATION
UARCHLONG
__ReadWholeFilePromiseProc(
	_In_                            LPVOID                  lpParam,
	_In_                            DLPVOID                 dlpOut
){
	SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(lpParam, ERROR_PROMISE_NOT_SUPPLIED_WITH_DATA, 0);
	SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(dlpOut, ERROR_PROMISE_NOT_SUPPLIED_WITH_ALLOCABLE_BUFFER, 0);
	HANDLE hFile;
	UARCHLONG ualSize;
	hFile = (HANDLE)lpParam;
	ualSize = GetFileSize(hFile);

	ReadWholeFile(hFile, (DLPSTR)dlpOut, NULL);
	
	return ualSize;
}






/* Returns HANDLE to a Promise */
_Result_Null_On_Failure_
_Spawns_New_Thread_Async_And_Locks_(hFile->hCritSec, _Has_Lock_Kind_(_Lock_Kind_Critical_Section_))
PODNET_API
HANDLE
ReadWholeFileAsynchronously(
	_In_                            HANDLE                  hFile
){
	SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(hFile, ERROR_HANDLE_SUPPLIED_WAS_NULL, FALSE);
	SET_ERROR_AND_EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hFile), HANDLE_TYPE_FILE, ERROR_FILE_HANDLE_SUPPLIED_IS_NOT_FILE_HANDLE_TYPE, FALSE);
	HANDLE hPromise;

	hPromise = CreatePromise(__ReadWholeFilePromiseProc, (LPVOID)hFile, NULL, NULLPTR, NULL);
	SET_ERROR_AND_EXIT_IF_NULL(hPromise, ERROR_PROMISE_COULD_NOT_BE_CREATED, NULLPTR);
	return hPromise;
}




_Success_(return != 0, _Acquires_Shared_Lock_(hFile->hCritSec, _Has_Lock_Kind_(_Lock_Kind_Critical_Section_)))
PODNET_API
UARCHLONG
GetFileSize(
	_In_                            HANDLE                  hFile
){
	SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(hFile, ERROR_HANDLE_SUPPLIED_WAS_NULL, FALSE);
	SET_ERROR_AND_EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hFile), HANDLE_TYPE_FILE, ERROR_FILE_HANDLE_SUPPLIED_IS_NOT_FILE_HANDLE_TYPE, FALSE);
	LPFILE_OBJ lpfoFile;
	UARCHLONG ualFileSize;
	lpfoFile = (LPFILE_OBJ)GetHandleDerivative(hFile);
	SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(lpfoFile, ERROR_HANDLE_DERIVATIVE_WAS_NULL, FALSE);

	if (!(lpfoFile->ulOpenFlags & FILE_PERMISSION_READ))

	{
		SetLastGlobalError(ERROR_FILE_NOT_OPENED_WITH_READ_FLAG);
		return FALSE;
	}

	EnterCriticalSection(lpfoFile->hCritSec);

	fseek(lpfoFile->fFile, 0, SEEK_END);
	ualFileSize = ftell(lpfoFile->fFile);
	rewind(lpfoFile->fFile);

	ExitCriticalSection(lpfoFile->hCritSec);
	return ualFileSize;
}




_Success_(return != FALSE, _Acquires_Shared_Lock_(hFile->hCritSec, _Has_Lock_Kind_(_Lock_Kind_Critical_Section_)))
PODNET_API
BOOL
FlushFileBuffer(
	_In_                            HANDLE                  hFile
){
	SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(hFile, ERROR_HANDLE_SUPPLIED_WAS_NULL, FALSE);
	SET_ERROR_AND_EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hFile), HANDLE_TYPE_FILE, ERROR_FILE_HANDLE_SUPPLIED_IS_NOT_FILE_HANDLE_TYPE, FALSE);
	LPFILE_OBJ lpfoFile;
	lpfoFile = (LPFILE_OBJ)GetHandleDerivative(hFile);
	SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(lpfoFile, ERROR_HANDLE_DERIVATIVE_WAS_NULL, FALSE);
	SET_ERROR_AND_EXIT_IF_NOT_VALUE(lpfoFile->bIsLegitFile, TRUE, ERROR_FILE_IS_NOT_LEGIT_FILE, FALSE);
	
	EnterCriticalSection(lpfoFile->hCritSec);

	fflush(lpfoFile->fFile);

	ExitCriticalSection(lpfoFile->hCritSec);
	return TRUE;
}




_Success_(return != FALSE, _Acquires_Shared_Lock_(hFile->hCritSec, _Has_Lock_Kind_(_Lock_Kind_Critical_Section_)))
PODNET_API
BOOL
RewindFile(
	_In_                            HANDLE                  hFile
){
	SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(hFile, ERROR_HANDLE_SUPPLIED_WAS_NULL, FALSE);
	SET_ERROR_AND_EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hFile), HANDLE_TYPE_FILE, ERROR_FILE_HANDLE_SUPPLIED_IS_NOT_FILE_HANDLE_TYPE, FALSE);
	LPFILE_OBJ lpfoFile;
	lpfoFile = (LPFILE_OBJ)GetHandleDerivative(hFile);
	SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(lpfoFile, ERROR_HANDLE_DERIVATIVE_WAS_NULL, FALSE);
	SET_ERROR_AND_EXIT_IF_NOT_VALUE(lpfoFile->bIsLegitFile, TRUE, ERROR_FILE_IS_NOT_LEGIT_FILE, FALSE);
	
	if (FALSE != lpfoFile->bIsLegitFile)
	{
		EnterCriticalSection(lpfoFile->hCritSec);

		rewind(lpfoFile->fFile);

		ExitCriticalSection(lpfoFile->hCritSec);

		return TRUE;
	}

	return FALSE;
}




_Success_(return != FALSE, _Acquires_Shared_Lock_(hFile->hCritSec, _Has_Lock_Kind_(_Lock_Kind_Critical_Section_)))
PODNET_API
UARCHLONG
FileTell(
	_In_                            HANDLE                  hFile
){
	SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(hFile, ERROR_HANDLE_SUPPLIED_WAS_NULL, FALSE);
	SET_ERROR_AND_EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hFile), HANDLE_TYPE_FILE, ERROR_FILE_HANDLE_SUPPLIED_IS_NOT_FILE_HANDLE_TYPE, FALSE);
	LPFILE_OBJ lpfoFile;
	UARCHLONG ualRet;
	lpfoFile = (LPFILE_OBJ)GetHandleDerivative(hFile);
	SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(lpfoFile, ERROR_HANDLE_DERIVATIVE_WAS_NULL, FALSE);
	SET_ERROR_AND_EXIT_IF_NOT_VALUE(lpfoFile->bIsLegitFile, TRUE, ERROR_FILE_IS_NOT_LEGIT_FILE, FALSE);
	ualRet = NULL;

	if (FALSE != lpfoFile->bIsLegitFile)
	{
		EnterCriticalSection(lpfoFile->hCritSec);

		ftell(lpfoFile->fFile);

		ExitCriticalSection(lpfoFile->hCritSec);
	}

	return ualRet;
}




_Success_(return != FALSE, _Acquires_Shared_Lock_(hFile->hCritSec, _Has_Lock_Kind_(_Lock_Kind_Critical_Section_)))
PODNET_API
UARCHLONG
FileSeek(
	_In_                            HANDLE                  hFile,
	_In_                            UARCHLONG               ualOffset,
	_In_                            UARCHLONG               ualOrigin
){
	SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(hFile, ERROR_HANDLE_SUPPLIED_WAS_NULL, FALSE);
	SET_ERROR_AND_EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hFile), HANDLE_TYPE_FILE, ERROR_FILE_HANDLE_SUPPLIED_IS_NOT_FILE_HANDLE_TYPE, FALSE);
	LPFILE_OBJ lpfoFile;
	UARCHLONG ualRet;
	lpfoFile = (LPFILE_OBJ)GetHandleDerivative(hFile);
	SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(lpfoFile, ERROR_HANDLE_DERIVATIVE_WAS_NULL, FALSE);
	SET_ERROR_AND_EXIT_IF_NOT_VALUE(lpfoFile->bIsLegitFile, TRUE, ERROR_FILE_IS_NOT_LEGIT_FILE, FALSE);
	ualRet = NULL;

	if (FALSE != lpfoFile->bIsLegitFile)
	{
		EnterCriticalSection(lpfoFile->hCritSec);

		ualRet = fseek(lpfoFile->fFile, ualOffset, (ULONG)ualOrigin);

		ExitCriticalSection(lpfoFile->hCritSec);
	}

	return ualRet;
}




_Success_(return != FALSE, ...)
PODNET_API
BOOL
LockFile(
	_In_                            HANDLE                  hFile,
	_Reserved_Must_Be_Null_         ULONG                   ulFlags
){
	UNREFERENCED_PARAMETER(ulFlags);
	SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(hFile, ERROR_HANDLE_SUPPLIED_WAS_NULL, FALSE);
	SET_ERROR_AND_EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hFile), HANDLE_TYPE_FILE, ERROR_FILE_HANDLE_SUPPLIED_IS_NOT_FILE_HANDLE_TYPE, FALSE);
	LPFILE_OBJ lpfoFile;
	LONG lRes;
	lpfoFile = (LPFILE_OBJ)GetHandleDerivative(hFile);
	SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(lpfoFile, ERROR_HANDLE_DERIVATIVE_WAS_NULL, FALSE);
	SET_ERROR_AND_EXIT_IF_NOT_VALUE(lpfoFile->bIsLegitFile, TRUE, ERROR_FILE_IS_NOT_LEGIT_FILE, FALSE);

	EnterCriticalSection(lpfoFile->hCritSec);
	lRes = flock(fileno(lpfoFile->fFile), LOCK_EX | LOCK_NB);
	ExitCriticalSection(lpfoFile->hCritSec);

	return (lRes == 0) ? TRUE : FALSE;
}




_Success_(return != FALSE, ...)
PODNET_API
BOOL
UnlockFile(
	_In_                            HANDLE                  hFile
){
	SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(hFile, ERROR_HANDLE_SUPPLIED_WAS_NULL, FALSE);
	SET_ERROR_AND_EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hFile), HANDLE_TYPE_FILE, ERROR_FILE_HANDLE_SUPPLIED_IS_NOT_FILE_HANDLE_TYPE, FALSE);
	LPFILE_OBJ lpfoFile;
	LONG lRes;
	lpfoFile = (LPFILE_OBJ)GetHandleDerivative(hFile);
	SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(lpfoFile, ERROR_HANDLE_DERIVATIVE_WAS_NULL, FALSE);
	SET_ERROR_AND_EXIT_IF_NOT_VALUE(lpfoFile->bIsLegitFile, TRUE, ERROR_FILE_IS_NOT_LEGIT_FILE, FALSE);

	EnterCriticalSection(lpfoFile->hCritSec);
	lRes = flock(fileno(lpfoFile->fFile), LOCK_UN);
	ExitCriticalSection(lpfoFile->hCritSec);

	return (lRes == 0) ? TRUE : FALSE;
}




_Success_(return != NULLPTR, ...)
PODNET_API 
LPCSTR
GetFileOpenedPath(
	_In_				HANDLE			hFile
){
	SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(hFile, ERROR_HANDLE_SUPPLIED_WAS_NULL, FALSE);
	SET_ERROR_AND_EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hFile), HANDLE_TYPE_FILE, ERROR_FILE_HANDLE_SUPPLIED_IS_NOT_FILE_HANDLE_TYPE, FALSE);
	LPFILE_OBJ lpfoFile;
	LPCSTR lpcszRes;
	lpfoFile = (LPFILE_OBJ)GetHandleDerivative(hFile);
	SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(lpfoFile, ERROR_HANDLE_DERIVATIVE_WAS_NULL, FALSE);
	SET_ERROR_AND_EXIT_IF_NOT_VALUE(lpfoFile->bIsLegitFile, TRUE, ERROR_FILE_IS_NOT_LEGIT_FILE, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(lpfoFile->ftType, FILE_TYPE_FILE, NULLPTR);

	EnterCriticalSection(lpfoFile->hCritSec);
	lpcszRes = (LPCSTR)lpfoFile->lpszFileName;
	ExitCriticalSection(lpfoFile->hCritSec);

	return lpcszRes;
}




_Result_Null_On_Failure_
PODNET_API
HANDLE
GetStandardOut(
	VOID
){
	if (hStdOut == NULLPTR)
	{
		LPFILE_OBJ lpfoStdOut;
		ULONGLONG ullID;
		lpfoStdOut = LocalAllocAndZero(sizeof(FILE_OBJ));
		SET_ERROR_AND_EXIT_IF_NULL(lpfoStdOut, ERROR_FILE_COULD_NOT_ALLOCATE_OBJECT_FOR_STANDARD_STREAM, NULLPTR);

		lpfoStdOut->bIsLegitFile = FALSE;
		lpfoStdOut->fFile = stdout;
		lpfoStdOut->hCritSec = CreateCriticalSection();
		if (NULL_OBJECT == lpfoStdOut->hCritSec)
		{
			SetLastGlobalError(ERROR_FILE_CRITICAL_SECTION_COULD_NOT_BE_CREATED);
			FreeMemory(lpfoStdOut);
			return NULL_OBJECT;
		}

		lpfoStdOut->lpfnReadProc = NULLPTR;
		lpfoStdOut->lpfnWriteProc = NULLPTR;
		lpfoStdOut->lpszFileName = NULLPTR;
		lpfoStdOut->ulOpenFlags = FILE_PERMISSION_WRITE;
	
		
		hStdOut = CreateHandleWithSingleInheritor(
			       lpfoStdOut,
			       &(lpfoStdOut->hThis),
			       HANDLE_TYPE_FILE,
			       __CloseFile,
			       0,
			       NULLPTR,
			       0,
			       NULLPTR,
			       0,
			       &ullID,
			       0
		);
		
		if (NULL_OBJECT == hStdOut)
		{
			SetLastGlobalError(ERROR_FILE_HANDLE_COULD_NOT_BE_CREATED);
			DestroyObject(lpfoStdOut->hCritSec);
			FreeMemory(lpfoStdOut);
			return NULL_OBJECT;
		}
	}
	return hStdOut;
}



_Result_Null_On_Failure_
PODNET_API
HANDLE
GetStandardIn(
	VOID
){
	if (hStdIn == NULLPTR)
	{
		LPFILE_OBJ lpfoStdIn;
		ULONGLONG ullID;
		lpfoStdIn = LocalAllocAndZero(sizeof(FILE_OBJ));
		SET_ERROR_AND_EXIT_IF_NULL(lpfoStdIn, ERROR_FILE_COULD_NOT_ALLOCATE_OBJECT_FOR_STANDARD_STREAM, NULLPTR);

		lpfoStdIn->bIsLegitFile = FALSE;
		lpfoStdIn->fFile = stdin;
		lpfoStdIn->hCritSec = CreateCriticalSection();
		if (NULL_OBJECT == lpfoStdIn->hCritSec)
		{
			SetLastGlobalError(ERROR_FILE_CRITICAL_SECTION_COULD_NOT_BE_CREATED);
			FreeMemory(lpfoStdIn);
			return NULL_OBJECT;
		}

		lpfoStdIn->lpfnReadProc = NULLPTR;
		lpfoStdIn->lpfnWriteProc = NULLPTR;
		lpfoStdIn->lpszFileName = NULLPTR;
		lpfoStdIn->ulOpenFlags = FILE_PERMISSION_READ;
	
		
		hStdIn = CreateHandleWithSingleInheritor(
			       lpfoStdIn,
			       &(lpfoStdIn->hThis),
			       HANDLE_TYPE_FILE,
			       __CloseFile,
			       0,
			       NULLPTR,
			       0,
			       NULLPTR,
			       0,
			       &ullID,
			       0
		);

		if (NULL_OBJECT == hStdIn)
		{
			SetLastGlobalError(ERROR_FILE_HANDLE_COULD_NOT_BE_CREATED);
			DestroyObject(lpfoStdIn->hCritSec);
			FreeMemory(lpfoStdIn);
			return NULL_OBJECT;
		}
	}
	return hStdIn;
}



_Result_Null_On_Failure_
PODNET_API
HANDLE
GetStandardError(
	VOID
){
	if (hStdErr == NULLPTR)
	{
		LPFILE_OBJ lpfoStdErr;
		ULONGLONG ullID;
		lpfoStdErr = LocalAllocAndZero(sizeof(FILE_OBJ));
		SET_ERROR_AND_EXIT_IF_NULL(lpfoStdErr, ERROR_FILE_COULD_NOT_ALLOCATE_OBJECT_FOR_STANDARD_STREAM, NULLPTR);

		lpfoStdErr->bIsLegitFile = FALSE;
		lpfoStdErr->fFile = stderr;
		lpfoStdErr->hCritSec = CreateCriticalSection();
		if (NULL_OBJECT == lpfoStdErr->hCritSec)
		{
			SetLastGlobalError(ERROR_FILE_CRITICAL_SECTION_COULD_NOT_BE_CREATED);
			FreeMemory(lpfoStdErr);
			return NULL_OBJECT;
		}

		lpfoStdErr->lpfnReadProc = NULLPTR;
		lpfoStdErr->lpfnWriteProc = NULLPTR;
		lpfoStdErr->lpszFileName = NULLPTR;
		lpfoStdErr->ulOpenFlags = FILE_PERMISSION_WRITE;
	
		
		hStdErr = CreateHandleWithSingleInheritor(
			       lpfoStdErr,
			       &(lpfoStdErr->hThis),
			       HANDLE_TYPE_FILE,
			       __CloseFile,
			       0,
			       NULLPTR,
			       0,
			       NULLPTR,
			       0,
			       &ullID,
			       0
		);

		if (NULL_OBJECT == hStdErr)
		{
			SetLastGlobalError(ERROR_FILE_HANDLE_COULD_NOT_BE_CREATED);
			DestroyObject(lpfoStdErr->hCritSec);
			FreeMemory(lpfoStdErr);
			return NULL_OBJECT;
		}
	}
	return hStdErr;
}
