/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CDIRECTORY_H
#define CDIRECTORY_H

#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../CHandle/CHandle.h"
#include "../CString/CString.h"
#include "../CString/CHeapString.h"




typedef struct __DIRECTORY_ENTRY
{
	LPSTR			lpszFileName;
	BYTE			bType;
} DIRECTORY_ENTRY, *LPDIRECTORY_ENTRY, **DLPDIRECTORY_ENTRY;



typedef LPVOID (*LPFN_DIRECTORY_ITERATOR_PROC)(
	_In_		HANDLE			hDirectory,
	_In_ 		LPDIRECTORY_ENTRY	lpdeInfo,
	_In_Opt_	LPVOID			lpParam
);




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNET_API 
HANDLE
OpenDirectory(
	_In_Z_		LPCSTR			lpcszPath
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API 
BOOL
DirectoryIterate(
	_In_		HANDLE				hDirectory,
	_In_ 		LPFN_DIRECTORY_ITERATOR_PROC	lpfnEntryCallBack,
	_In_Opt_	LPVOID				lpParam
);



_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HVECTOR
DirectoryToVectorListing(
	_In_		HANDLE			hDirectory
);



#endif