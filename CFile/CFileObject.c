/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CFILEOBJECT_C
#define CFILEOBJECT_C


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../CHandle/CHandle.h"
#include "../CFile/CFile.h"


typedef enum __FILE_TYPE
{
	FILE_TYPE_NOT_A_FILE	= 0,
	FILE_TYPE_FILE		= 1,
	FILE_TYPE_MEM		= 2,
	FILE_TYPE_TEMP		= 3,
	FILE_TYPE_OTHER		= 4
} FILE_TYPE;




typedef struct __FILE_OBJ
{
	INHERITS_FROM_HANDLE();
	HANDLE				hRelative;
	HANDLE                          hCritSec;
	FILE                            *fFile;
	LONG 				lDescriptor;
	LPSTR                           lpszFileName;
	LPSTR                           lpszFilePath;
	LPFN_HANDLE_CUSTOM_PROC         lpfnReadProc;
	LPFN_HANDLE_CUSTOM_PROC         lpfnWriteProc;
	ULONG                           ulOpenFlags;
	FILE_TYPE			ftType;
	BOOL                            bIsLegitFile;
	BOOL				bIsPiped;
	BOOL				bIsPipeSink;
} FILE_OBJ, *LPFILE_OBJ, **DLPFILE_JOB;


#endif