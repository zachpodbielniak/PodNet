/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CFILE_H
#define CFILE_H

#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../CHandle/CHandle.h"
#include "../CError/CError.h"
#include "../CConcurrent/CPromise.h"
#include "../CConcurrent/CFuture.h"

#define FILE_PERMISSION_READ            (1<<0)
#define FILE_PERMISSION_WRITE           (1<<1)
#define FILE_PERMISSION_APPEND          (1<<2)
#define FILE_PERMISSION_CREATE          (1<<3)
#define FILE_PERMISSION_ASYNC           (1<<4)
#define FILE_PERMISSION_BINARY		(1<<5)


#define FILE_READ_ALLOC_OUT     	(1<<1)
#define FILE_READ_SINGLE_POINTER_SAFE	(1<<2)

#define FILE_WRITE_FLUSH		(1<<1)



#define CloseFile               DestroyHandle

typedef FILE 			*LPFILE;


_Result_Null_On_Failure_
PODNET_API
HANDLE
CreateFile(
	_In_Z_				LPCSTR			lpcszFile,
	_In_Opt_			ULONG			ulPermissions,
	_In_Opt_			ULONG			ulFlags
);




_Result_Null_On_Failure_
PODNET_API
HANDLE
CreateTemporaryFile(
	_Reserved_Must_Be_Null_		ULONG			ulFlags
);




_Result_Null_On_Failure_
PODNET_API
HANDLE
OpenFile(
	_In_Z_				LPCSTR			lpcszFile,
	_In_Opt_			ULONG			ulPermissions,
	_In_Opt_			ULONG			ulFlags
);




_Result_Null_On_Failure_
PODNET_API
HANDLE
OpenFileFromMemory(
	_In_				LPVOID			lpBuffer,
	_In_				UARCHLONG		ualSize,
	_In_Opt_                        ULONG                   ulPermissions,
	_In_Opt_                        ULONG                   ulFlags
);




_Result_Null_On_Failure_
PODNET_API
HANDLE
OpenFileAndDefineReadAndWriteProc(
	_In_Opt_Z_                      LPCSTR                  lpcszFile,
	_In_Opt_                        LPFN_HANDLE_CUSTOM_PROC lpfnReadProc,
	_In_Opt_                        LPFN_HANDLE_CUSTOM_PROC lpfnWriteProc,
	_In_Opt_                        ULONG                   ulPermission,
	_In_Opt_                        ULONG                   ulFlags
);




_Result_Null_On_Failure_
PODNET_API
HANDLE
OpenFileWithFileDescriptor(
	_In_                            LONG                    lFileDescriptor,
	_In_Opt_                        ULONG                   ulFlags
);




_Result_Null_On_Failure_
PODNET_API
HANDLE
OpenFileWithFilePointer(
	_In_ 				FILE			*lpfFilePointer,
	_In_ 				ULONG 			ulPermission
);




_Result_Null_On_Failure_
PODNET_API
HANDLE
OpenFileWithFileDescriptorAndDefineReadAndWriteProcs(
	_In_                            LONG                    lFileDescriptor,
	_In_                            LPFN_HANDLE_CUSTOM_PROC lpfnReadProc,
	_In_                            LPFN_HANDLE_CUSTOM_PROC lpfnWriteProc,
	_In_Opt_                        ULONG                   ulFlags
);




_Success_(return != FALSE, _Acquires_Shared_Lock_(hFile->hCritSec, _Has_Lock_Kind_(_Lock_Kind_Critical_Section_)))
PODNET_API
BOOL
WriteFile(
	_In_                            HANDLE                  hFile,
	_In_Z_                          LPCSTR                  lpcszStringToWrite,
	_In_Opt_                        ULONG                   ulFlags
);




_Success_(return != FALSE, _Acquires_Shared_Lock_(hFile->hCritSec, _Has_Lock_Kind_(_Lock_Kind_Critical_Section_)))
PODNET_API
BOOL
WriteDataToFile(
	_In_ 				HANDLE 			hFile,
	_In_ 				LPBYTE			lpbData,
	_In_ 				ULONGLONG 		ullSize,
	_Reserved_Must_Be_Null_ 	LPVOID 			lpReserved,
	_In_Opt_ 			ULONG 			ulFlags
);




/* Returns HANDLE to a Promise */
_Result_Null_On_Failure_
_Spawns_New_Thread_Async_And_Locks_(hFile->hCritSec, _Has_Lock_Kind_(_Lock_Kind_Critical_Section_))
PODNET_API
HANDLE
WriteFileAsynchronously(
	_In_                            HANDLE                  hFile,
	_In_Z_                          LPCSTR                  lpcszStringToWrite,
	_In_Opt_                        ULONG                   ulFlags
);




_Success_(return != FALSE, _Acquires_Shared_Lock_(hFile->hCritSec, _Has_Lock_Kind_(_Lock_Kind_Critical_Section_)))
PODNET_API
ARCHLONG
ReadFile(
	_In_                            HANDLE                  hFile,
	_Out_Writes_To_Ptr_Z_(dlpstrOut)DLPSTR                  dlpstrOut,
	_In_                            ULONGLONG               ullBytesToRead,
	_In_Opt_                        ULONG                   ulFlags
);




_Success_(return != FALSE, _Acquires_Shared_Lock_(hFile->hCritSec, _Has_Lock_Kind_(_Lock_Kind_Critical_Section_)))
PODNET_API
ARCHLONG
ReadLineFromFile(
	_In_ 				HANDLE 			hFile,
	_Out_Writes_To_Ptr_Z_(dlpstrOut)DLPSTR 			dlpstrOut,
	_In_ 				UARCHLONG 		ualSize,
	_In_Opt_ 			ULONG 			ulFlags
);




/* Returns HANDLE to a Promise */
_Result_Null_On_Failure_
_Spawns_New_Thread_Async_And_Locks_(hFile->hCritSec, _Has_Lock_Kind_(_Lock_Kind_Critical_Section_))
PODNET_API
HANDLE
ReadFileAsynchronously(
	_In_                            HANDLE                  hFile,
	_In_                            ULONGLONG               ullBytesToRead
);




_Success_(return != FALSE, _Acquires_Shared_Lock_(hFile->hCritSec, _Has_Lock_Kind_(_Lock_Kind_Critical_Section_)))
PODNET_API
BOOL
ReadWholeFile(
	_In_                            HANDLE                  hFile,
	_Out_Writes_To_Ptr_Z_(dlpstrOut)DLPSTR                  dlpstrOut,
	_In_                            ULONG                   ulFlags    
);




/* Returns HANDLE to a Promise */
_Result_Null_On_Failure_
_Spawns_New_Thread_Async_And_Locks_(hFile->hCritSec, _Has_Lock_Kind_(_Lock_Kind_Critical_Section_))
PODNET_API
HANDLE
ReadWholeFileAsynchronously(
	_In_                            HANDLE                  hFile
);





_Success_(return != 0, _Acquires_Shared_Lock_(hFile->hCritSec, _Has_Lock_Kind_(_Lock_Kind_Critical_Section_)))
PODNET_API
UARCHLONG
GetFileSize(
	_In_                            HANDLE                  hFile
);




_Success_(return != FALSE, _Acquires_Shared_Lock_(hFile->hCritSec, _Has_Lock_Kind_(_Lock_Kind_Critical_Section_)))
PODNET_API
BOOL
FlushFileBuffer(
	_In_                            HANDLE                  hFile
);




_Success_(return != FALSE, _Acquires_Shared_Lock_(hFile->hCritSec, _Has_Lock_Kind_(_Lock_Kind_Critical_Section_)))
PODNET_API
BOOL
RewindFile(
	_In_                            HANDLE                  hFile
);




_Success_(return != FALSE, _Acquires_Shared_Lock_(hFile->hCritSec, _Has_Lock_Kind_(_Lock_Kind_Critical_Section_)))
PODNET_API
UARCHLONG
FileTell(
	_In_                            HANDLE                  hFile
);




_Success_(return != FALSE, _Acquires_Shared_Lock_(hFile->hCritSec, _Has_Lock_Kind_(_Lock_Kind_Critical_Section_)))
PODNET_API
UARCHLONG
FileSeek(
	_In_                            HANDLE                  hFile,
	_In_                            UARCHLONG               ualOffset,
	_In_                            UARCHLONG               ualOrigin
);




_Success_(return != FALSE, ...)
PODNET_API
BOOL
LockFile(
	_In_                            HANDLE                  hFile,
	_Reserved_Must_Be_Null_         ULONG                   ulFlags
);




_Success_(return != FALSE, ...)
PODNET_API
BOOL
UnlockFile(
	_In_                            HANDLE                  hFile
);




_Success_(return != NULLPTR, ...)
PODNET_API 
LPCSTR
GetFileOpenedPath(
	_In_				HANDLE			hFile
);




_Result_Null_On_Failure_
PODNET_API
HANDLE
GetStandardOut(
	VOID
);




_Result_Null_On_Failure_
PODNET_API
HANDLE
GetStandardIn(
	VOID
);




_Result_Null_On_Failure_
PODNET_API
HANDLE
GetStandardError(
	VOID
);




#endif
