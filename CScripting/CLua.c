/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CLua.h"


#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>

typedef lua_State               *HLUA;
typedef struct _LUA
{
        INHERITS_FROM_HANDLE();
        HLUA            hlState;
        HANDLE          hCriticalSection;
} LUA, *LPLUA;



_Success_(return != FALSE, ...)
INTERNAL_OPERATION
BOOL
__DestroyLuaState(
        _In_                    HDERIVATIVE             hDerivative,
        _In_                    ULONG                   ulFlags
){
        UNREFERENCED_PARAMETER(ulFlags);
        LPLUA lpLua;
        lpLua = (LPLUA)hDerivative;
        lua_close(lpLua->hlState);
        DestroyHandle(lpLua->hCriticalSection);
        ZeroMemory(lpLua, sizeof(LUA));
        FreeMemory(lpLua);
        return TRUE;
}


_Success_(return != NULLPTR, ...)
PODNET_API
HANDLE
CreateLuaState(
        VOID
){
        HANDLE hLua;
        LPLUA lpLua;
        ULONGLONG ullID;

        lpLua = LocalAllocAndZero(sizeof(LUA));
	/* cppcheck-suppress memleak */
	EXIT_IF_UNLIKELY_NULL(lpLua, NULL_OBJECT);
	
        lpLua->hCriticalSection = CreateCriticalSection();
        lpLua->hlState = luaL_newstate();
        luaL_openlibs(lpLua->hlState);

        hLua = CreateHandleWithSingleInheritor(
                        lpLua,
                        &(lpLua->hThis),
                        HANDLE_TYPE_LUA,
                        __DestroyLuaState,
                        0,
                        NULLPTR,
                        0,
                        NULLPTR,
                        0,
                        &ullID,
                        0
        );

        return hLua;
}



_Success_(return != FALSE, ...)
PODNET_API
BOOL
RunLuaScript(
        _In_                    HANDLE                  hLua,
        _In_                    HANDLE                  hFile
){
        LPLUA lpLua;
        lpLua = (LPLUA)GetHandleDerivative(hLua);
	EXIT_IF_UNLIKELY_NULL(lpLua, FALSE);
        EnterCriticalSection(lpLua->hCriticalSection);
        UNREFERENCED_PARAMETER(hFile);
        ExitCriticalSection(lpLua->hCriticalSection);
        return TRUE;
}



_Success_(return != FALSE, ...)
PODNET_API
BOOL
RunLuaScriptDirect(
        _In_                    HANDLE                  hLua,
        _In_                    LPCSTR                  lpcszFileName
){
        LPLUA lpLua;
        lpLua = (LPLUA)GetHandleDerivative(hLua);
	EXIT_IF_UNLIKELY_NULL(lpLua, FALSE);
        EnterCriticalSection(lpLua->hCriticalSection);
        luaL_dofile(lpLua->hlState, lpcszFileName);      
        ExitCriticalSection(lpLua->hCriticalSection);
        return TRUE;
}



_Success_(return != FALSE, ...)
PODNET_API
BOOL
RunLuaString(
        _In_                    HANDLE                  hLua,
        _In_                    LPCSTR                  lpcszLuaCode
){
        LPLUA lpLua;
        lpLua = (LPLUA)GetHandleDerivative(hLua);
	EXIT_IF_UNLIKELY_NULL(lpLua, FALSE);
        EnterCriticalSection(lpLua->hCriticalSection);
        luaL_dostring(lpLua->hlState, lpcszLuaCode);      
        ExitCriticalSection(lpLua->hCriticalSection);
        return TRUE;
}


_Success_(return != FALSE, ...)
PODNET_API
BOOL
CallLuaFunction(
        _In_                    HANDLE                  hLua,
        _In_                    LPCSTR                  lpcszFunction
){
        UNREFERENCED_PARAMETER(hLua);
        UNREFERENCED_PARAMETER(lpcszFunction);
        return TRUE;
}
