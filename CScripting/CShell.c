/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CShell.h"


_Success_(return == 0, _Spawns_Process_And_Waits_)
PODNET_API
LONG
RunShell(
	_In_Z_		LPCSTR RESTRICT		lpcszShellBinary,
	_In_Z_		LPCSTR RESTRICT		lpcszCommand,
	_Out_Opt_	DLPSTR RESTRICT		dlpszOutput
){
	EXIT_IF_UNLIKELY_NULL(lpcszShellBinary, 255);
	EXIT_IF_UNLIKELY_NULL(lpcszCommand, 255);

	HANDLE hProcess;
	HANDLE hOutput;
	LONG lRetCode;
	CSTRING csCommand[0x400];
	CSTRING csBuffer[0x400];
	LPSTR lpszBuffer;
	HSTRING hsOutput;
	BOOL bFirstIter;

	ZeroMemory(csCommand, sizeof(csCommand));
	lpszBuffer = (LPSTR)csBuffer;
	bFirstIter = TRUE;
	hsOutput = NULLPTR;

	StringPrintFormatSafe(
		csCommand,
		sizeof(csCommand) - 1,
		"-c \"%s\"",
		lpcszCommand
	);


	hProcess = CreateProcess(
		lpcszShellBinary,
		csCommand,
		FALSE
	);

	WaitForSingleObject(hProcess, INFINITE_WAIT_TIME);

	ZeroMemory(csBuffer, sizeof(csBuffer));

	hOutput = GetProcessOutput(hProcess);
	if (NULL_OBJECT == hOutput)
	{
		DestroyObject(hProcess);
		return 255;
	}

	while (0 < ReadLineFromFile(hOutput, &lpszBuffer, sizeof(csBuffer) - 1, 0))
	{ 
		if (FALSE == bFirstIter)
		{ HeapStringAppend(hsOutput, (LPCSTR)lpszBuffer); }
		else 
		{ 
			bFirstIter = FALSE;
			hsOutput = CreateHeapString((LPCSTR)lpszBuffer); 
		}
	}

	if (NULLPTR != dlpszOutput)
	{ 
		if (NULLPTR == hsOutput)
		{ *dlpszOutput = StringDuplicate(""); }
		else
		{ *dlpszOutput = StringDuplicate(HeapStringValue(hsOutput)); }
	}

	if (NULLPTR != hsOutput && NULLPTR == dlpszOutput )
	{ PrintFormat("%s", HeapStringValue(hsOutput)); } 

	DestroyHeapString(hsOutput);
	lRetCode = GetProcessReturnStatus(hProcess);

	DestroyObject(hProcess);
	return (LONG)lRetCode;
}




_Success_(return == 0, _Spawns_Process_And_Waits_)
PODNET_API
LONG 
RunShellFile(
	_In_Z_		LPCSTR RESTRICT		lpcszShellBinary,
	_In_Z_		LPCSTR RESTRICT		lpcszFile,
	_In_Opt_Z_	LPCSTR RESTRICT		lpcszArgs,
	_Out_Opt_Z_	DLPSTR RESTRICT		dlpszOutput
){
	EXIT_IF_UNLIKELY_NULL(lpcszShellBinary, 255);
	EXIT_IF_UNLIKELY_NULL(lpcszFile, 255);
	EXIT_IF_UNLIKELY_NULL(lpcszArgs, 255);

	HANDLE hProcess;
	HANDLE hOutput;
	LONG lRetCode;
	CSTRING csCommand[0x400];
	CSTRING csBuffer[0x400];
	LPSTR lpszBuffer;
	HSTRING hsOutput;
	BOOL bFirstIter;

	ZeroMemory(csCommand, sizeof(csCommand));
	lpszBuffer = (LPSTR)csBuffer;
	bFirstIter = TRUE;
	hsOutput = NULLPTR;



	StringPrintFormatSafe(
		csCommand,
		sizeof(csCommand) - 1,
		"%s %s",
		lpcszFile,
		lpcszArgs
	);


	hProcess = CreateProcess(
		lpcszShellBinary,
		csCommand,
		FALSE
	);

	WaitForSingleObject(hProcess, INFINITE_WAIT_TIME);

	ZeroMemory(csBuffer, sizeof(csBuffer));

	hOutput = GetProcessOutput(hProcess);
	if (NULL_OBJECT == hOutput)
	{
		DestroyObject(hProcess);
		return 255;
	}

	while (0 < ReadLineFromFile(hOutput, &lpszBuffer, sizeof(csBuffer) - 1, 0))
	{ 
		if (FALSE == bFirstIter)
		{ HeapStringAppend(hsOutput, (LPCSTR)lpszBuffer); }
		else 
		{ 
			bFirstIter = FALSE;
			hsOutput = CreateHeapString((LPCSTR)lpszBuffer); 
		}
	}

	if (NULLPTR != dlpszOutput)
	{ 
		if (NULLPTR == hsOutput)
		{ *dlpszOutput = StringDuplicate(""); }
		else
		{ *dlpszOutput = StringDuplicate(HeapStringValue(hsOutput)); }
	}

	if (NULLPTR != hsOutput && NULL == dlpszOutput )
	{ PrintFormat("%s", HeapStringValue(hsOutput)); } 

	DestroyHeapString(hsOutput);
	lRetCode = GetProcessReturnStatus(hProcess);

	DestroyObject(hProcess);
	return (LONG)lRetCode;
}




_Success_(return == 0, _Spawns_Process_And_Waits_)
PODNET_API
LONG
RunShellFormatted(
	_In_Z_		LPCSTR RESTRICT		lpcszShellBinary,
	_In_Z_		LPCSTR RESTRICT		lpcszCommandFormat,
	_Out_Opt_	DLPSTR RESTRICT		dlpszOutput,
	...
){
	EXIT_IF_UNLIKELY_NULL(lpcszShellBinary, 255);
	EXIT_IF_UNLIKELY_NULL(lpcszCommandFormat, 255);

	HANDLE hProcess;
	HANDLE hOutput;
	LONG lRetCode;
	CSTRING csCommand[0x400];
	CSTRING csPreCommand[0x400];
	CSTRING csBuffer[0x400];
	LPSTR lpszBuffer;
	HSTRING hsOutput;
	BOOL bFirstIter;
	va_list vaArgs;

	ZeroMemory(csCommand, sizeof(csCommand));
	ZeroMemory(csPreCommand, sizeof(csPreCommand));
	lpszBuffer = (LPSTR)csBuffer;
	bFirstIter = TRUE;
	hsOutput = NULLPTR;

	va_start(vaArgs, dlpszOutput);
	vsnprintf(
		csPreCommand,
		sizeof(csPreCommand) - 1,
		lpcszCommandFormat,
		vaArgs
	);
	va_end(vaArgs);

	StringPrintFormatSafe(
		csCommand,
		sizeof(csCommand) - 1,
		"-c \"%s\"",
		csPreCommand
	);


	hProcess = CreateProcess(
		lpcszShellBinary,
		csCommand,
		FALSE
	);

	WaitForSingleObject(hProcess, INFINITE_WAIT_TIME);

	ZeroMemory(csBuffer, sizeof(csBuffer));

	hOutput = GetProcessOutput(hProcess);
	if (NULL_OBJECT == hOutput)
	{
		DestroyObject(hProcess);
		return 255;
	}

	while (0 < ReadLineFromFile(hOutput, &lpszBuffer, sizeof(csBuffer) - 1, 0))
	{ 
		if (FALSE == bFirstIter)
		{ HeapStringAppend(hsOutput, (LPCSTR)lpszBuffer); }
		else 
		{ 
			bFirstIter = FALSE;
			hsOutput = CreateHeapString((LPCSTR)lpszBuffer); 
		}
	}

	if (NULLPTR != dlpszOutput)
	{ 
		if (NULLPTR == hsOutput)
		{ *dlpszOutput = StringDuplicate(""); }
		else
		{ *dlpszOutput = StringDuplicate(HeapStringValue(hsOutput)); }
	}

	if (NULLPTR != hsOutput && NULL == dlpszOutput)
	{ PrintFormat("%s", HeapStringValue(hsOutput)); } 

	DestroyHeapString(hsOutput);
	lRetCode = GetProcessReturnStatus(hProcess);

	DestroyObject(hProcess);
	return (LONG)lRetCode;
}




_Success_(return == 0, _Spawns_Process_And_Waits_)
PODNET_API 
LONG 
Sh(
	_In_Z_		LPCSTR RESTRICT		lpcszCommandFormat,
	_Out_Opt_	DLPSTR RESTRICT		dlpszOutput,
	...
){
	LONG lRetCode;
	CSTRING csCommand[0x400];
	va_list vaArgs;

	ZeroMemory(csCommand, sizeof(csCommand));

	va_start(vaArgs, dlpszOutput);
	vsnprintf(csCommand, sizeof(csCommand) - 1, lpcszCommandFormat, vaArgs);
	va_end(vaArgs);
	
	lRetCode = RunShell(
		"/bin/sh",
		csCommand,
		dlpszOutput
	);

	return lRetCode;
}




_Success_(return == 0, _Spawns_Process_And_Waits_)
PODNET_API 
LONG 
Bash(
	_In_Z_		LPCSTR RESTRICT		lpcszCommandFormat,
	_Out_Opt_	DLPSTR RESTRICT		dlpszOutput,
	...
){
	LONG lRetCode;
	CSTRING csCommand[0x400];
	va_list vaArgs;

	ZeroMemory(csCommand, sizeof(csCommand));

	va_start(vaArgs, dlpszOutput);
	vsnprintf(csCommand, sizeof(csCommand) - 1, lpcszCommandFormat, vaArgs);
	va_end(vaArgs);
	
	lRetCode = RunShell(
		"/bin/bash",
		csCommand,
		dlpszOutput
	);

	return lRetCode;
}
