/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CSHELL_H
#define CSHELL_H



#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../CProcess/CProcess.h"
#include "../CString/CHeapString.h"
#include "../CString/CString.h"




_Success_(return == 0, _Spawns_Process_And_Waits_)
PODNET_API
LONG
RunShell(
	_In_Z_		LPCSTR RESTRICT		lpcszShellBinary,
	_In_Z_		LPCSTR RESTRICT		lpcszCommand,
	_Out_Opt_	DLPSTR RESTRICT		dlpszOutput
);




_Success_(return == 0, _Spawns_Process_And_Waits_)
PODNET_API
LONG 
RunShellFile(
	_In_Z_		LPCSTR RESTRICT		lpcszShellBinary,
	_In_Z_		LPCSTR RESTRICT		lpcszFile,
	_In_Opt_Z_	LPCSTR RESTRICT		lpcszArgs,
	_Out_Opt_Z_	DLPSTR RESTRICT		dlpszOutput
);




_Success_(return == 0, _Spawns_Process_And_Waits_)
PODNET_API
LONG
RunShellFormatted(
	_In_Z_		LPCSTR RESTRICT		lpcszShellBinary,
	_In_Z_		LPCSTR RESTRICT		lpcszCommandFormat,
	_Out_Opt_	DLPSTR RESTRICT		dlpszOutput,
	...
);




_Success_(return == 0, _Spawns_Process_And_Waits_)
PODNET_API 
LONG 
Sh(
	_In_Z_		LPCSTR RESTRICT		lpcszCommandFormat,
	_Out_Opt_	DLPSTR RESTRICT		dlpszOutput,
	...
);




_Success_(return == 0, _Spawns_Process_And_Waits_)
PODNET_API 
LONG 
Bash(
	_In_Z_		LPCSTR RESTRICT		lpcszCommandFormat,
	_Out_Opt_	DLPSTR RESTRICT		dlpszOutput,
	...
);




#endif