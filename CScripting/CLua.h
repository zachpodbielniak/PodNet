/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CLUA_H
#define CLUA_H




#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../CHandle/CHandle.h"



#define CloseLuaState           DestroyHandle




_Success_(return != NULLPTR, ...)
PODNET_API
HANDLE
CreateLuaState(
        VOID
);



_Success_(return != FALSE, ...)
PODNET_API
BOOL
RunLuaScript(
        _In_                    HANDLE                  hLua,
        _In_                    HANDLE                  hFile
);



_Success_(return != FALSE, ...)
PODNET_API
BOOL
RunLuaScriptDirect(
        _In_                    HANDLE                  hLua,
        _In_                    LPCSTR                  lpcszFileName
);



_Success_(return != FALSE, ...)
PODNET_API
BOOL
RunLuaString(
        _In_                    HANDLE                  hLua,
        _In_                    LPCSTR                  lpcszLuaCode
);


_Success_(return != FALSE, ...)
PODNET_API
BOOL
CallLuaFunction(
        _In_                    HANDLE                  hLua,
        _In_                    LPCSTR                  lpcszFunction
);


#endif