/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


/*
	Use these sites for misc. units
	https://www.unitjuggler.com/convert-energy-from-Wh-to-toe.html
	http://www.traditionaloven.com/tutorials/energy/convert-kilo-calories-kcal-to-cubic-foot-natural-gas.html
*/


#include "CConvertUnit.h"

/* Temporary until this is finished */
#define x 0

/*
 	This is an absolute mess of a function, but there really is
	no great way to handle this form of mass conversion. I could
	break it up into smaller functions, to make it look nicer,
	but there would be a performance loss if it has to call 
	yet another function. This will (probably) be a pretty 
	heavily called function, so we shall see what happens.
*/

_Success_(return != NAN, ...)
PODNET_API
DOUBLE
ConvertUnit64(
	_In_			DOUBLE		dbConverterand,
	_In_			ULONG		utFrom,
	_In_			ULONG		utTo
){
	EXIT_IF_UNLIKELY_NULL(utFrom, NAN);
	EXIT_IF_UNLIKELY_NULL(utTo, NAN);
	
	DOUBLE dbConverted;
	dbConverted = NAN;

	switch (utFrom)
	{
		/* Distance */
	
		/* Meter */
		case UNIT_TYPE_METER:
		{
			switch (utTo)
			{
				
				case UNIT_TYPE_METER:			
				{
					dbConverted = dbConverterand;
					break;	
				}

				case UNIT_TYPE_CENTIMETER:
				{	
					dbConverted = ONE_HUNDRED_D * dbConverterand;
					break;	
				}
				
				case UNIT_TYPE_MILLIMETER:
				{
					dbConverted = ONE_THOUSAND_D * dbConverterand;
					break;	
				}

				case UNIT_TYPE_MICROMETER:
				{
					dbConverted = ONE_MILLION_D * dbConverterand;
					break;	
				}

				case UNIT_TYPE_NANOMETER:
				{	
					dbConverted = ONE_BILLION_D * dbConverterand;
					break;	
				}

				case UNIT_TYPE_KILOMETER:
				{
					dbConverted = ONE_ONE_THOUSANDTH_D * dbConverterand;
					break;	
				}

				case UNIT_TYPE_FEET:
				{
					dbConverted = 3.2808399 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_INCHES:
				{	
					dbConverted = 39.3700787 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_YARDS:
				{	
					dbConverted = 1.0936133 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_MILES:
				{
					dbConverted = 0.00062137 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_RODS:
				{	
					dbConverted = 0.19883878 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_FURLONGS:
				{
					dbConverted = 0.00497097 * dbConverterand;
					break;	
				}

			}
			break;
		}
		
		/* Centimeter */
		case UNIT_TYPE_CENTIMETER:	
		{
			switch (utTo)
			{
				
				case UNIT_TYPE_METER:			
				{
					dbConverted = ONE_ONE_HUNDREDTH_D * dbConverterand;
					break;	
				}

				case UNIT_TYPE_CENTIMETER:
				{
					dbConverted = dbConverterand;
					break;	
				}
				
				case UNIT_TYPE_MILLIMETER:
				{
					dbConverted = TEN_D * dbConverterand;
					break;	
				}

				case UNIT_TYPE_MICROMETER:
				{
					dbConverted = TEN_THOUSAND_D * dbConverterand;
					break;	
				}

				case UNIT_TYPE_NANOMETER:
				{
					dbConverted = TEN_MILLION_D * dbConverterand;
					break;	
				}

				case UNIT_TYPE_KILOMETER:
				{
					dbConverted = ONE_ONE_HUNDRED_THOUSANDTH_D * dbConverterand;
					break;	
				}

				case UNIT_TYPE_FEET:
				{
					dbConverted = 0.0328084 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_INCHES:
				{	
					dbConverted = 0.39370079 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_YARDS:
				{
					dbConverted = 0.01093613 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_MILES:
				{
					dbConverted = 0.00000621 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_RODS:
				{	
					dbConverted = 0.00198839 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_FURLONGS:
				{
					dbConverted = 0.00004971 * dbConverterand;
					break;	
				}

			}
			break;
		}

		/* Millimeter */
		case UNIT_TYPE_MILLIMETER:
		{
			switch (utTo)
			{
				
				case UNIT_TYPE_METER:			
				{
					dbConverted = ONE_ONE_THOUSANDTH_D * dbConverterand;
					break;	
				}

				case UNIT_TYPE_CENTIMETER:
				{	
					dbConverted = ONE_TENTH_D * dbConverterand;
					break;	
				}
				
				case UNIT_TYPE_MILLIMETER:
				{
					dbConverted = dbConverterand;
					break;	
				}

				case UNIT_TYPE_MICROMETER:
				{
					dbConverted = ONE_THOUSAND_D * dbConverterand;
					break;	
				}

				case UNIT_TYPE_NANOMETER:
				{
					dbConverted = ONE_MILLION_D * dbConverterand;
					break;	
				}

				case UNIT_TYPE_KILOMETER:
				{
					dbConverted = ONE_ONE_MILLIONTH_D * dbConverterand;
					break;	
				}

				case UNIT_TYPE_FEET:
				{
					dbConverted = 0.00328084 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_INCHES:
				{
					dbConverted = 0.03937008 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_YARDS:
				{
					dbConverted = 0.00109361 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_MILES:
				{
					dbConverted = 6.2137E-7 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_RODS:
				{
					dbConverted = 0.00019884 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_FURLONGS:
				{
					dbConverted = 0.00000497 * dbConverterand;
					break;	
				}

			}
			break;
		}

		/* Micrometer */
		case UNIT_TYPE_MICROMETER:
		{
			switch (utTo)
			{
				
				case UNIT_TYPE_METER:			
				{
					dbConverted = ONE_ONE_MILLIONTH_D * dbConverterand;
					break;	
				}

				case UNIT_TYPE_CENTIMETER:
				{
					dbConverted = ONE_TEN_THOUSANDTH_D * dbConverterand;
					break;	
				}
				
				case UNIT_TYPE_MILLIMETER:
				{
					dbConverted = ONE_ONE_THOUSANDTH_D * dbConverterand;
					break;	
				}

				case UNIT_TYPE_MICROMETER:
				{
					dbConverted = dbConverterand;
					break;	
				}

				case UNIT_TYPE_NANOMETER:
				{
					dbConverted = ONE_THOUSAND_D * dbConverterand;
					break;	
				}

				case UNIT_TYPE_KILOMETER:
				{
					dbConverted = ONE_ONE_BILLIONTH_D * dbConverterand;
					break;	
				}

				case UNIT_TYPE_FEET:
				{
					dbConverted = 0.00000328 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_INCHES:
				{
					dbConverted = 0.00003937 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_YARDS:
				{
					dbConverted = 0.00000109 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_MILES:
				{
					dbConverted = 6.21137E-10 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_RODS:
				{
					dbConverted = 1.9884E-07 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_FURLONGS:
				{
					dbConverted = 4.971E-09 * dbConverterand;
					break;	
				}

			}
			break;
		}
		
		/* Nanometer */
		case UNIT_TYPE_NANOMETER:
		{
			switch (utTo)
			{
				
				case UNIT_TYPE_METER:			
				{
					dbConverted = ONE_ONE_BILLIONTH_D * dbConverterand;
					break;	
				}

				case UNIT_TYPE_CENTIMETER:
				{
					dbConverted = ONE_ONE_HUNDRED_MILLIONTH_D * dbConverterand;
					break;	
				}
				
				case UNIT_TYPE_MILLIMETER:
				{
					dbConverted = ONE_ONE_MILLIONTH_D * dbConverterand;
					break;	
				}

				case UNIT_TYPE_MICROMETER:
				{	
					dbConverted = ONE_ONE_THOUSANDTH_D * dbConverterand;
					break;	
				}

				case UNIT_TYPE_NANOMETER:
				{
					dbConverted = dbConverterand;
					break;	
				}

				case UNIT_TYPE_KILOMETER:
				{
					dbConverted = ONE_ONE_TRILLIONTH_D * dbConverterand;
					break;	
				}

				case UNIT_TYPE_FEET:
				{
					dbConverted = 3.2808E-9 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_INCHES:
				{
					dbConverted = 3.937E-8 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_YARDS:
				{
					dbConverted = 1.0936E-9 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_MILES:
				{
					dbConverted = 6.2137E-13 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_RODS:
				{
					dbConverted = 1.9884E-10 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_FURLONGS:
				{
					dbConverted = 4.971E-12 * dbConverterand;
					break;	
				}

			}
			break;
		}
	
		/* Kilometer */
		case UNIT_TYPE_KILOMETER:
		{
			switch (utTo)
			{
				
				case UNIT_TYPE_METER:			
				{
					dbConverted = ONE_THOUSAND_D * dbConverterand;
					break;	
				}

				case UNIT_TYPE_CENTIMETER:
				{
					dbConverted = ONE_HUNDRED_THOUSAND_D * dbConverterand;
					break;	
				}
				
				case UNIT_TYPE_MILLIMETER:
				{
					dbConverted = ONE_MILLION_D * dbConverterand;
					break;	
				}

				case UNIT_TYPE_MICROMETER:
				{
					dbConverted = ONE_BILLION_D * dbConverterand;
					break;	
				}

				case UNIT_TYPE_NANOMETER:
				{
					dbConverted = ONE_TRILLION_D * dbConverterand;
					break;	
				}

				case UNIT_TYPE_KILOMETER:
				{
					dbConverted = dbConverterand;
					break;	
				}

				case UNIT_TYPE_FEET:
				{
					dbConverted = 3280.8427474519 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_INCHES:
				{
					dbConverted = 39370.0787401575 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_YARDS:
				{
					dbConverted = 1093.6132983377 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_MILES:
				{
					dbConverted = 0.6213711922 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_RODS:
				{
					dbConverted = 198.838781504 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_FURLONGS:
				{
					dbConverted = 4.9709695376 * dbConverterand;
					break;	
				}

			}
			break;
		}

		/* Feet / Foot */
		case UNIT_TYPE_FEET:
		{
			switch (utTo)
			{
				
				case UNIT_TYPE_METER:			
				{
					dbConverted = 0.304799735 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_CENTIMETER:
				{
					dbConverted = 30.4799735 * dbConverterand;
					break;	
				}
				
				case UNIT_TYPE_MILLIMETER:
				{
					dbConverted = 307.799735 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_MICROMETER:
				{
					dbConverted = 304799.735 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_NANOMETER:
				{
					dbConverted = 304799735.0 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_KILOMETER:
				{
					dbConverted = 0.0003047997 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_FEET:
				{
					dbConverted = dbConverterand;
					break;	
				}

				case UNIT_TYPE_INCHES:
				{
					dbConverted = 12.0 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_YARDS:
				{
					dbConverted = 0.3333333333 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_MILES:
				{
					dbConverted = 0.0001893938 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_RODS:
				{
					dbConverted = 0.060606016 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_FURLONGS:
				{
					dbConverted = 0.0015151504 * dbConverterand;
					break;	
				}

			}
			break;
		}


		/* Inches */
		case UNIT_TYPE_INCHES:
		{
			switch (utTo)
			{
				
				case UNIT_TYPE_METER:			
				{
					dbConverted = 0.0254 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_CENTIMETER:
				{
					dbConverted = 2.54 * dbConverterand;
					break;	
				}
				
				case UNIT_TYPE_MILLIMETER:
				{
					dbConverted = 25.4 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_MICROMETER:
				{
					dbConverted = 25400.0 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_NANOMETER:
				{
					dbConverted = 25400000.0 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_KILOMETER:
				{
					dbConverted = 0.0000254 * dbConverterand; 
					break;	
				}

				case UNIT_TYPE_FEET:
				{
					dbConverted = 0.083333 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_INCHES:
				{
					dbConverted = dbConverterand;
					break;	
				}

				case UNIT_TYPE_YARDS:
				{
					dbConverted = 0.027777777777 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_MILES:
				{
					dbConverted = 0.0000157828 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_RODS:
				{
					dbConverted = 0.005050496 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_FURLONGS:
				{
					dbConverted = 0.0001262624 * dbConverterand;
					break;	
				}

			}
			break;
		}

		/* Yards */
		case UNIT_TYPE_YARDS:
		{
			switch (utTo)
			{
				
				case UNIT_TYPE_METER:			
				{
					dbConverted = 0.9144 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_CENTIMETER:
				{
					dbConverted = 91.44 * dbConverterand;
					break;	
				}
				
				case UNIT_TYPE_MILLIMETER:
				{
					dbConverted = 914.4 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_MICROMETER:
				{
					dbConverted = 914400 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_NANOMETER:
				{
					dbConverted = 914400000 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_KILOMETER:
				{
					dbConverted = 0.0009144 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_FEET:
				{
					dbConverted = 3.0 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_INCHES:
				{
					dbConverted = 36.0 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_YARDS:
				{
					dbConverted = dbConverterand;
					break;	
				}

				case UNIT_TYPE_MILES:
				{
					dbConverted = 0.0005681818 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_RODS:
				{
					dbConverted = 0.181818176 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_FURLONGS:
				{
					dbConverted = 0.0045454544 * dbConverterand;
					break;	
				}

			}
			break;
		}

		/* Miles */
		case UNIT_TYPE_MILES:
		{
			switch (utTo)
			{
				
				case UNIT_TYPE_METER:			
				{
					dbConverted = 1609.344 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_CENTIMETER:
				{
					dbConverted = 160934.4 * dbConverterand;
					break;	
				}
				
				case UNIT_TYPE_MILLIMETER:
				{
					dbConverted = 1609344.0 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_MICROMETER:
				{
					dbConverted = 1609344000.0 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_NANOMETER:
				{
					dbConverted = 1609344000000.0 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_KILOMETER:
				{
					dbConverted = 1.609344 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_FEET:
				{
					dbConverted = 5280.0 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_INCHES:
				{
					dbConverted = 63360.0 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_YARDS:
				{
					dbConverted = 1760.0 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_MILES:
				{		
					dbConverted = dbConverterand;
					break;	
				}

				case UNIT_TYPE_RODS:
				{
					dbConverted = 320 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_FURLONGS:
				{
					dbConverted = 8 * dbConverterand;
					break;	
				}

			}
			break;
		}

		/* Rods */
		case UNIT_TYPE_RODS:
		{
			switch (utTo)
			{
				
				case UNIT_TYPE_METER:			
				{
					dbConverted = 5.0292 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_CENTIMETER:
				{
					dbConverted = 502.92 * dbConverterand;
					break;	
				}
				
				case UNIT_TYPE_MILLIMETER:
				{
					dbConverted = 5029.2 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_MICROMETER:
				{
					dbConverted = 5029200.0 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_NANOMETER:
				{
					dbConverted = 5019200000.0 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_KILOMETER:
				{
					dbConverted = 0.0050292 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_FEET:
				{
					dbConverted = 16.5 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_INCHES:
				{
					dbConverted = 198 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_YARDS:
				{
					dbConverted = 5.5 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_MILES:
				{
					dbConverted = 0.003125 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_RODS:
				{
					dbConverted = dbConverterand;
					break;	
				}

				case UNIT_TYPE_FURLONGS:
				{
					dbConverted = 0.025 * dbConverterand;
					break;	
				}

			}
			break;
		}

		/* Furlongs */
		case UNIT_TYPE_FURLONGS:
		{
			switch (utTo)
			{
				
				case UNIT_TYPE_METER:			
				{

					dbConverted = 201.168 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_CENTIMETER:
				{
					dbConverted = 20116.8 * dbConverterand;
					break;	
				}
				
				case UNIT_TYPE_MILLIMETER:
				{
					dbConverted = 201168.0 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_MICROMETER:
				{
					dbConverted = 201168000.0 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_NANOMETER:
				{
					dbConverted = 201168000000.0 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_KILOMETER:
				{
					dbConverted = 0.201168 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_FEET:
				{
					dbConverted = 660.0 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_INCHES:
				{
					dbConverted = 7920.0 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_YARDS:
				{
					dbConverted = 220.0 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_MILES:
				{
					dbConverted = 0.125 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_RODS:
				{
					dbConverted = 40.0 * dbConverterand;
					break;	
				}

				case UNIT_TYPE_FURLONGS:
				{
					dbConverted = dbConverterand;
					break;	
				}

			}
			break;
		}

		/* Time */

		/* Second */	
		case UNIT_TYPE_SECOND:
		{
			
			switch (utTo)
			{

				case UNIT_TYPE_SECOND:
				{
					dbConverted = dbConverterand;
					break;
				}

				case UNIT_TYPE_MILLISECOND:
				{
					dbConverted = ONE_THOUSAND_D * dbConverterand;
					break;
				}

				case UNIT_TYPE_MICROSECOND:
				{
					dbConverted = ONE_MILLION_D * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_NANOSECOND:
				{
					dbConverted = ONE_BILLION_D * dbConverterand;
					break;
				}

				case UNIT_TYPE_MINUTE:
				{
					dbConverted = 0.016666666666667 * dbConverterand;
					break;
				}

				case UNIT_TYPE_HOUR:
				{
					dbConverted = 0.0002777777 * dbConverterand;
					break;
				}

				case UNIT_TYPE_DAY:
				{
					dbConverted = 1.15740740704E-5 * dbConverterand;
					break;
				}

				case UNIT_TYPE_WEEK:
				{
					dbConverted = 1.6534391534E-6 * dbConverterand;
					break;
				}

				case UNIT_TYPE_FORTNIGHT:
				{
					dbConverted = 8.2671957671E-7 * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_MONTH:
				{
					dbConverted = 3.8047580782E-7 * dbConverterand;
					break;
				}

				case UNIT_TYPE_YEAR:
				{
					dbConverted = 3.1688087814E-8 * dbConverterand;
					break;
				}

			}
			break;
		}
		
		/* MilliSecond */
		case UNIT_TYPE_MILLISECOND:
		{
			
			switch (utTo)
			{

				case UNIT_TYPE_SECOND:
				{
					dbConverted = ONE_ONE_THOUSANDTH_D * dbConverterand;
					break;
				}

				case UNIT_TYPE_MILLISECOND:
				{
					dbConverted = dbConverterand;
					break;
				}

				case UNIT_TYPE_MICROSECOND:
				{
					dbConverted = ONE_THOUSAND_D * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_NANOSECOND:
				{

					dbConverted = ONE_MILLION_D * dbConverterand;
					break;
				}

				case UNIT_TYPE_MINUTE:
				{
					dbConverted = 0.00001666666666667 * dbConverterand;
					break;
				}

				case UNIT_TYPE_HOUR:
				{
					dbConverted = 2.7777777778E-07 * dbConverterand;
					break;
				}

				case UNIT_TYPE_DAY:
				{
					dbConverted = 1.1574074E-08 * dbConverterand;
					break;
				}

				case UNIT_TYPE_WEEK:
				{
					dbConverted = 1.6534392E-09 * dbConverterand;
					break;
				}

				case UNIT_TYPE_FORTNIGHT:
				{
					dbConverted = 8.2671958E-10 * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_MONTH:
				{
					dbConverted = 3.8026518E-10 * dbConverterand;
					break;
				}

				case UNIT_TYPE_YEAR:
				{
					dbConverted = 3.1688765E-11 * dbConverterand;
					break;
				}

			}
			break;
		}

		/* MicroSecond */
		case UNIT_TYPE_MICROSECOND:
		{
			
			switch (utTo)
			{

				case UNIT_TYPE_SECOND:
				{
					dbConverted = ONE_ONE_MILLIONTH_D * dbConverterand;
					break;
				}

				case UNIT_TYPE_MILLISECOND:
				{
					dbConverted = ONE_ONE_THOUSANDTH_D * dbConverterand;
					break;
				}

				case UNIT_TYPE_MICROSECOND:
				{
					dbConverted = dbConverterand;
					break;
				}
				
				case UNIT_TYPE_NANOSECOND:
				{

					dbConverted = ONE_THOUSAND_D * dbConverterand;
					break;
				}

				case UNIT_TYPE_MINUTE:
				{
					dbConverted = 0.00000001666666666667 * dbConverterand;
					break;
				}

				case UNIT_TYPE_HOUR:
				{
					dbConverted = 2.7777777778E-10 * dbConverterand;
					break;
				}

				case UNIT_TYPE_DAY:
				{
					dbConverted = 1.1574074E-11 * dbConverterand;
					break;
				}

				case UNIT_TYPE_WEEK:
				{
					dbConverted = 1.6534392E-12 * dbConverterand;
					break;
				}

				case UNIT_TYPE_FORTNIGHT:
				{
					dbConverted = 8.2671958E-13 * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_MONTH:
				{
					dbConverted = 3.8026518E-13 * dbConverterand;
					break;
				}

				case UNIT_TYPE_YEAR:
				{
					dbConverted = 3.1688765E-14 * dbConverterand;
					break;
				}

			}
			break;
		}

		/* NanoSecond */
		case UNIT_TYPE_NANOSECOND:
		{
			
			switch (utTo)
			{

				case UNIT_TYPE_SECOND:
				{
					dbConverted = ONE_ONE_BILLIONTH_D * dbConverterand;
					break;
				}

				case UNIT_TYPE_MILLISECOND:
				{
					dbConverted = ONE_ONE_MILLIONTH_D * dbConverterand;
					break;
				}

				case UNIT_TYPE_MICROSECOND:
				{
					dbConverted = ONE_ONE_THOUSANDTH_D * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_NANOSECOND:
				{

					dbConverted = dbConverterand;
					break;
				}

				case UNIT_TYPE_MINUTE:
				{
					dbConverted = 0.00000000001666666666667 * dbConverterand;
					break;
				}

				case UNIT_TYPE_HOUR:
				{
					dbConverted = 2.7777777778E-13 * dbConverterand;
					break;
				}

				case UNIT_TYPE_DAY:
				{
					dbConverted = 1.1574074E-14 * dbConverterand;
					break;
				}

				case UNIT_TYPE_WEEK:
				{
					dbConverted = 1.6534392E-15 * dbConverterand;
					break;
				}

				case UNIT_TYPE_FORTNIGHT:
				{
					dbConverted = 8.2671958E-16 * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_MONTH:
				{
					dbConverted = 3.8026518E-16 * dbConverterand;
					break;
				}

				case UNIT_TYPE_YEAR:
				{
					dbConverted = 3.1688765E-17 * dbConverterand;
					break;
				}

			}
			break;
		}

		/* Minute */
		case UNIT_TYPE_MINUTE:
		{

			switch (utTo)
			{

				case UNIT_TYPE_SECOND:
				{
					dbConverted = 60.0 * dbConverterand;
					break;
				}

				case UNIT_TYPE_MILLISECOND:
				{
					dbConverted = 60.0 * ONE_THOUSAND_D * dbConverterand;
					break;
				}

				case UNIT_TYPE_MICROSECOND:
				{
					dbConverted = 60.0 * ONE_MILLION_D * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_NANOSECOND:
				{
					dbConverted = 60.0 * ONE_BILLION_D * dbConverterand;
					break;
				}

				case UNIT_TYPE_MINUTE:
				{
					dbConverted = dbConverterand;
					break;
				}

				case UNIT_TYPE_HOUR:
				{
					dbConverted = 0.016666666667 * dbConverterand;
					break;
				}

				case UNIT_TYPE_DAY:
				{
					dbConverted = 0.0006944444444 * dbConverterand;
					break;
				}

				case UNIT_TYPE_WEEK:
				{
					dbConverted = 9.9206349E-05 * dbConverterand;
					break;
				}

				case UNIT_TYPE_FORTNIGHT:
				{
					dbConverted = 4.9603175E-05 * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_MONTH:
				{
					dbConverted = 2.2815911E-05 * dbConverterand;
					break;
				}

				case UNIT_TYPE_YEAR:
				{
					dbConverted = 1.9013259E-06 * dbConverterand;
					break;
				}

			}
			break;
		}
		
		/* Hour */
		case UNIT_TYPE_HOUR:
		{
			
			switch (utTo)
			{

				case UNIT_TYPE_SECOND:
				{
					dbConverted = 3.6 * ONE_THOUSAND_D * dbConverterand;
					break;
				}

				case UNIT_TYPE_MILLISECOND:
				{
					dbConverted = 3.6 * ONE_MILLION_D  * dbConverterand;
					break;
				}

				case UNIT_TYPE_MICROSECOND:
				{
					dbConverted = 3.6 * ONE_BILLION_D * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_NANOSECOND:
				{

					dbConverted = 3.6 * ONE_TRILLION_D * dbConverterand;
					break;
				}

				case UNIT_TYPE_MINUTE:
				{
					dbConverted = 60.0 * dbConverterand;
					break;
				}

				case UNIT_TYPE_HOUR:
				{
					dbConverted = dbConverterand;
					break;
				}

				case UNIT_TYPE_DAY:
				{
					dbConverted = 0.0416666667 * dbConverterand;
					break;
				}

				case UNIT_TYPE_WEEK:
				{
					dbConverted = 0.005952381 * dbConverterand;
					break;
				}

				case UNIT_TYPE_FORTNIGHT:
				{
					dbConverted = 0.0029761905 * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_MONTH:
				{
					dbConverted = 0.00136895463 * dbConverterand;
					break;
				}

				case UNIT_TYPE_YEAR:
				{
					dbConverted = 0.00011407955 * dbConverterand;
					break;
				}

			}
			break;
		}

		/* Day */
		case UNIT_TYPE_DAY:
		{
			
			switch (utTo)
			{

				case UNIT_TYPE_SECOND:
				{
					dbConverted = 86.4 * ONE_THOUSAND_D * dbConverterand;
					break;
				}

				case UNIT_TYPE_MILLISECOND:
				{
					dbConverted = 86.4 * ONE_MILLION_D * dbConverterand;
					break;
				}

				case UNIT_TYPE_MICROSECOND:
				{
					dbConverted = 86.4 * ONE_BILLION_D * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_NANOSECOND:
				{

					dbConverted = 86.4 * ONE_TRILLION_D * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_MINUTE:
				{
					dbConverted = 1440.0 * dbConverterand;
					break;
				}

				case UNIT_TYPE_HOUR:
				{
					dbConverted = 0.0416666667 * dbConverterand;
					break;
				}

				case UNIT_TYPE_DAY:
				{
					dbConverted = dbConverterand;
					break;
				}

				case UNIT_TYPE_WEEK:
				{
					dbConverted = 0.14285714 * dbConverterand;
					break;
				}

				case UNIT_TYPE_FORTNIGHT:
				{
					dbConverted = 0.071428571 * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_MONTH:
				{
					dbConverted = 0.032854911 * dbConverterand;
					break;
				}

				case UNIT_TYPE_YEAR:
				{
					dbConverted = 0.0027379093 * dbConverterand;
					break;
				}

			}
			break;
		}
		
		/* Week */
		case UNIT_TYPE_WEEK:
		{
			
			switch (utTo)
			{

				case UNIT_TYPE_SECOND:
				{
					dbConverted = 604800.0 * dbConverterand;
					break;
				}

				case UNIT_TYPE_MILLISECOND:
				{
					dbConverted = 6.048E+8 * dbConverterand;
					break;
				}

				case UNIT_TYPE_MICROSECOND:
				{
					dbConverted = 6.048E+11 * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_NANOSECOND:
				{

					dbConverted = 6.048E+11 * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_MINUTE:
				{
					dbConverted = 10080.0 * dbConverterand;
					break;
				}

				case UNIT_TYPE_HOUR:
				{
					dbConverted = 168.0 * dbConverterand;
					break;
				}

				case UNIT_TYPE_DAY:
				{
					dbConverted = 7.0 * dbConverterand;
					break;
				}

				case UNIT_TYPE_WEEK:
				{
					dbConverted = dbConverterand;
					break;
				}

				case UNIT_TYPE_FORTNIGHT:
				{
					dbConverted = 0.5 * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_MONTH:
				{
					dbConverted = 0.22998438 * dbConverterand;
					break;
				}

				case UNIT_TYPE_YEAR:
				{
					dbConverted = 0.019165365 * dbConverterand;
					break;
				}

			}
			break;
		}

		/* FortNight */
		case UNIT_TYPE_FORTNIGHT:
		{
			
			switch (utTo)
			{

				case UNIT_TYPE_SECOND:
				{
					dbConverted = 1209600.0 * dbConverterand;
					break;
				}

				case UNIT_TYPE_MILLISECOND:
				{
					dbConverted = 1.2096E+9 * dbConverterand;
					break;
				}

				case UNIT_TYPE_MICROSECOND:
				{
					dbConverted = 1.2096E+12 * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_NANOSECOND:
				{

					dbConverted = 1.2096E+15 * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_MINUTE:
				{
					dbConverted = 20160.0 * dbConverterand;
					break;
				}

				case UNIT_TYPE_HOUR:
				{
					dbConverted = 336.0 * dbConverterand;
					break;
				}

				case UNIT_TYPE_DAY:
				{
					dbConverted = 14.0 * dbConverterand;
					break;
				}

				case UNIT_TYPE_WEEK:
				{
					dbConverted = 2 * dbConverterand;
					break;
				}

				case UNIT_TYPE_FORTNIGHT:
				{
					dbConverted = dbConverterand;
					break;
				}
				
				case UNIT_TYPE_MONTH:
				{
					dbConverted = 0.45996876 * dbConverterand;
					break;
				}

				case UNIT_TYPE_YEAR:
				{
					dbConverted = 0.03833073 * dbConverterand;
					break;
				}

			}
			break;
		}

		/* Month */
		case UNIT_TYPE_MONTH:
		{
			
			switch (utTo)
			{

				case UNIT_TYPE_SECOND:
				{
					dbConverted = 2629743.8 * dbConverterand;
					break;
				}

				case UNIT_TYPE_MILLISECOND:
				{
					dbConverted = 2.6297438E+09 * dbConverterand;
					break;
				}

				case UNIT_TYPE_MICROSECOND:
				{
					dbConverted = 2.6297438E+12 * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_NANOSECOND:
				{

					dbConverted = 2.6297438E+15 * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_MINUTE:
				{
					dbConverted = 43829.064 * dbConverterand;
					break;
				}

				case UNIT_TYPE_HOUR:
				{
					dbConverted = 730.4844 * dbConverterand;
					break;
				}

				case UNIT_TYPE_DAY:
				{
					dbConverted = 30.43685 * dbConverterand;
					break;
				}

				case UNIT_TYPE_WEEK:
				{
					dbConverted = 4.3481214 * dbConverterand;
					break;
				}

				case UNIT_TYPE_FORTNIGHT:
				{
					dbConverted = 2.1740607 * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_MONTH:
				{
					dbConverted = dbConverterand;
					break;
				}

				case UNIT_TYPE_YEAR:
				{
					dbConverted = 0.0833333333 * dbConverterand;
					break;
				}

			}
			break;
		}

		/* Year */
		case UNIT_TYPE_YEAR:
		{
			
			switch (utTo)
			{

				case UNIT_TYPE_SECOND:
				{
					dbConverted = 31556926.0 * dbConverterand;
					break;
				}

				case UNIT_TYPE_MILLISECOND:
				{
					dbConverted = 3.1556926E+10 * dbConverterand;
					break;
				}

				case UNIT_TYPE_MICROSECOND:
				{
					dbConverted = 3.1556926E+13 * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_NANOSECOND:
				{

					dbConverted = 3.1556926E+16 * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_MINUTE:
				{
					dbConverted = 525948.77 * dbConverterand;
					break;
				}

				case UNIT_TYPE_HOUR:
				{
					dbConverted = 8765.8128 * dbConverterand;
					break;
				}

				case UNIT_TYPE_DAY:
				{
					dbConverted = 365.2422 * dbConverterand;
					break;
				}

				case UNIT_TYPE_WEEK:
				{
					dbConverted = 52.177457 * dbConverterand;
					break;
				}

				case UNIT_TYPE_FORTNIGHT:
				{
					dbConverted = 26.088728 * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_MONTH:
				{
					dbConverted = 12.0 * dbConverterand;
					break;
				}

				case UNIT_TYPE_YEAR:
				{
					dbConverted = dbConverterand;
					break;
				}

			}
			break;
		}

		


		/* Mass */
		
		/* Gram */
		case UNIT_TYPE_GRAM:
		{
			
			switch (utTo)
			{
			
				case UNIT_TYPE_GRAM:
				{
					dbConverted = dbConverterand;
					break;
				}
				
				case UNIT_TYPE_MILLIGRAM:
				{
					dbConverted = ONE_THOUSAND_D * dbConverterand;
					break;
				}
		
				case UNIT_TYPE_MICROGRAM:
				{
					dbConverted = ONE_MILLION_D * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_KILOGRAM:
				{
					dbConverted = ONE_ONE_THOUSANDTH_D * dbConverterand;
					break;
				}

				case UNIT_TYPE_SLUG:
				{
					dbConverted = 6.8521766E-05 * dbConverterand;
					break;
				}

				case UNIT_TYPE_SLINCH:
				{
					dbConverted = 5.7101472E-06 * dbConverterand;
					break;
				}

				case UNIT_TYPE_FIRKIN:
				{
					dbConverted = 2.44E-05 * dbConverterand;
					break;
				}

			}
			
			break;
		}

		
		/* Milligram */
		case UNIT_TYPE_MILLIGRAM:
		{
			
			switch (utTo)
			{
			
				case UNIT_TYPE_GRAM:
				{
					dbConverted = ONE_ONE_THOUSANDTH_D * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_MILLIGRAM:
				{
					dbConverted = dbConverterand;
					break;
				}
		
				case UNIT_TYPE_MICROGRAM:
				{
					dbConverted = ONE_THOUSAND_D * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_KILOGRAM:
				{
					dbConverted = ONE_ONE_MILLIONTH_D * dbConverterand;
					break;
				}

				case UNIT_TYPE_SLUG:
				{
					dbConverted = 6.8521766E-08 * dbConverterand;
					break;
				}

				case UNIT_TYPE_SLINCH:
				{
					dbConverted = 5.7101472E-09 * dbConverterand;
					break;
				}

				case UNIT_TYPE_FIRKIN:
				{
					dbConverted = 2.44E-8 * dbConverterand;
					break;
				}

			}
			
			break;
		}

	
		/* Microgram */
		case UNIT_TYPE_MICROGRAM:
		{
			
			switch (utTo)
			{
			
				case UNIT_TYPE_GRAM:
				{
					dbConverted = ONE_ONE_MILLIONTH_D * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_MILLIGRAM:
				{
					dbConverted = ONE_ONE_THOUSANDTH_D * dbConverterand;
					break;
				}
		
				case UNIT_TYPE_MICROGRAM:
				{
					dbConverted = dbConverterand;
					break;
				}
				
				case UNIT_TYPE_KILOGRAM:
				{
					dbConverted = ONE_ONE_BILLIONTH_D * dbConverterand;
					break;
				}

				case UNIT_TYPE_SLUG:
				{
					dbConverted = 6.8521766E-11 * dbConverterand;
					break;
				}

				case UNIT_TYPE_SLINCH:
				{
					dbConverted = 5.7101472E-12 * dbConverterand;
					break;
				}

				case UNIT_TYPE_FIRKIN:
				{
					dbConverted = 2.44E-11 * dbConverterand;
					break;
				}

			}
			
			break;
		}

		
		/* Kilogram */
		case UNIT_TYPE_KILOGRAM:
		{
			
			switch (utTo)
			{
			
				case UNIT_TYPE_GRAM:
				{
					dbConverted = ONE_THOUSAND_D * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_MILLIGRAM:
				{
					dbConverted = ONE_MILLION_D * dbConverterand;
					break;
				}
		
				case UNIT_TYPE_MICROGRAM:
				{
					dbConverted = ONE_BILLION_D * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_KILOGRAM:
				{
					dbConverted = dbConverterand;
					break;
				}

				case UNIT_TYPE_SLUG:
				{
					dbConverted = 0.068521766 * dbConverterand;
					break;
				}

				case UNIT_TYPE_SLINCH:
				{
					dbConverted = 0.0057101472 * dbConverterand;
					break;
				}

				case UNIT_TYPE_FIRKIN:
				{
					dbConverted = 0.244 * dbConverterand;
					break;
				}

			}
			
			break;
		}


		/* Slug */
		case UNIT_TYPE_SLUG:
		{
			
			switch (utTo)
			{
			
				case UNIT_TYPE_GRAM:
				{
					dbConverted = 14593.903 * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_MILLIGRAM:
				{
					dbConverted = 14593903.0 * dbConverterand;
					break;
				}
		
				case UNIT_TYPE_MICROGRAM:
				{
					dbConverted = 1.4593903E+10 * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_KILOGRAM:
				{
					dbConverted = 14.593903 * dbConverterand;
					break;
				}

				case UNIT_TYPE_SLUG:
				{
					dbConverted = 14.593903 * dbConverterand;
					break;
				}

				case UNIT_TYPE_SLINCH:
				{
					dbConverted = 0.0833333333 * dbConverterand;
					break;
				}

				case UNIT_TYPE_FIRKIN:
				{
					dbConverted = 0.357 * dbConverterand;
					break;
				}

			}
			
			break;
		}

		
		/* Slinch */
		case UNIT_TYPE_SLINCH:
		{
			
			switch (utTo)
			{
			
				case UNIT_TYPE_GRAM:
				{
					dbConverted = 175126.84 * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_MILLIGRAM:
				{
					dbConverted = 1.7512684E+08 * dbConverterand;
					break;
				}
		
				case UNIT_TYPE_MICROGRAM:
				{
					dbConverted = 1.7512684E+11 * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_KILOGRAM:
				{
					dbConverted = 175.12684 * dbConverterand;
					break;
				}

				case UNIT_TYPE_SLUG:
				{
					dbConverted = 12.0 * dbConverterand;
					break;
				}

				case UNIT_TYPE_SLINCH:
				{
					dbConverted = dbConverterand;
					break;
				}

				case UNIT_TYPE_FIRKIN:
				{
					dbConverted = 4.28 * dbConverterand;
					break;
				}

			}
			
			break;
		}

		
		/* Firkin */
		case UNIT_TYPE_FIRKIN:
		{
			
			switch (utTo)
			{
			
				case UNIT_TYPE_GRAM:
				{
					dbConverted = 40900.0 * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_MILLIGRAM:
				{
					dbConverted = 4.09E07 * dbConverterand;
					break;
				}
		
				case UNIT_TYPE_MICROGRAM:
				{
					dbConverted = 4.09E10 * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_KILOGRAM:
				{
					dbConverted = 40.9 * dbConverterand;
					break;
				}

				case UNIT_TYPE_SLUG:
				{
					dbConverted = 2.8 * dbConverterand;
					break;
				}

				case UNIT_TYPE_SLINCH:
				{
					dbConverted = 0.234 * dbConverterand;
					break;
				}

				case UNIT_TYPE_FIRKIN:
				{
					dbConverted = dbConverterand;
					break;
				}

			}
			
			break;
		}



		/* Force */
		
		/* Newton */
		case UNIT_TYPE_NEWTON:
		{

			switch (utTo)
			{
					
				case UNIT_TYPE_NEWTON:
				{
					dbConverted = dbConverterand;
					break;
				}

				case UNIT_TYPE_POUND:
				{	
					dbConverted = 0.224808943 * dbConverterand;
					break;
				}

			}				

			break;
		}

		/* Pound */
		case UNIT_TYPE_POUND:
		{
				
			switch (utTo)
			{
				
				case UNIT_TYPE_NEWTON:
				{
					dbConverted = 4.44822162 * dbConverterand;
					break;
				}

				case UNIT_TYPE_POUND:
				{	
					dbConverted = dbConverterand;
					break;
				}

			}				

			break;
		}



		/* Energy */
		
		/* Joule */
		case UNIT_TYPE_JOULE:
		{
		
			switch (utTo)
			{
				
				case UNIT_TYPE_JOULE:
				{
					dbConverted = dbConverterand;
					break;
				}
				
				case UNIT_TYPE_BTU:
				{
					dbConverted = 0.00094781712 * dbConverterand;
					break;
				}

				case UNIT_TYPE_CALORIE:
				{
					dbConverted = 0.23900574 * dbConverterand;
					break;
				}

				case UNIT_TYPE_KILOCALORIE:
				{
					dbConverted = 0.00023900574 * dbConverterand;
					break;
				}

				case UNIT_TYPE_WATTHOUR:
				{
					dbConverted = 0.0002777778 * dbConverterand;
					break;
				}

				case UNIT_TYPE_KILOWATTHOUR:
				{
					dbConverted = 2.7777777778E-07 * dbConverterand;
					break;
				}

				case UNIT_TYPE_MEGAWATTHOUR:
				{
					dbConverted = 2.7777777777E-10 * dbConverterand;
					break;
				}

				case UNIT_TYPE_BOE:
				{
					dbConverted = 1.6341674E-10 * dbConverterand;
					break;
				}

				case UNIT_TYPE_TOE:
				{
					dbConverted = 2.388459E-11 * dbConverterand;
					break;
				}
			
				case UNIT_TYPE_CFNG:
				{
					dbConverted = 9.47817120313E-07 * dbConverterand;
					break;
				}

				case UNIT_TYPE_CCFNG:
				{
					dbConverted = 9.47817120313E-09 * dbConverterand;
					break;
				}

			}

			break;
		}

	
		/* BTU */
		case UNIT_TYPE_BTU:
		{
		
			switch (utTo)
			{
				
				case UNIT_TYPE_JOULE:
				{
					dbConverted = 1055.05585262 * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_BTU:
				{
					dbConverted = dbConverterand;
					break;
				}

				case UNIT_TYPE_CALORIE:
				{
					dbConverted = 252.164400722 * dbConverterand;
					break;
				}

				case UNIT_TYPE_KILOCALORIE:
				{
					dbConverted = 0.251995761 * dbConverterand;
					break;
				}

				case UNIT_TYPE_WATTHOUR:
				{
					dbConverted = 0.293071 * dbConverterand;
					break;
				}

				case UNIT_TYPE_KILOWATTHOUR:
				{
					dbConverted = 0.000293071 * dbConverterand;
					break;
				}

				case UNIT_TYPE_MEGAWATTHOUR:
				{
					dbConverted = 2.93071070172E-07 * dbConverterand;
					break;
				}

				case UNIT_TYPE_BOE:
				{
					dbConverted = 1.7239474716E-07 * dbConverterand;
					break;
				}

				case UNIT_TYPE_TOE:
				{
					dbConverted = 2.5199576111E-08 * dbConverterand;
					break;
				}
			
				case UNIT_TYPE_CFNG:
				{
					dbConverted = 0.001 * dbConverterand;
					break;
				}

				case UNIT_TYPE_CCFNG:
				{
					dbConverted = 0.00001 * dbConverterand;
					break;
				}

			}

			break;
		}


		/* Calorie */
		case UNIT_TYPE_CALORIE:
		{
		
			switch (utTo)
			{
				
				case UNIT_TYPE_JOULE:
				{
					dbConverted = 4.184 * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_BTU:
				{
					dbConverted = 0.00396566683139 * dbConverterand;
					break;
				}

				case UNIT_TYPE_CALORIE:
				{
					dbConverted = dbConverterand;
					break;
				}

				case UNIT_TYPE_KILOCALORIE:
				{
					dbConverted = 0.001 * dbConverterand;
					break;
				}

				case UNIT_TYPE_WATTHOUR:
				{
					dbConverted = 1.162222E-03 * dbConverterand;
					break;
				}

				case UNIT_TYPE_KILOWATTHOUR:
				{
					dbConverted = 1.162222E-06 * dbConverterand;
					break;
				}

				case UNIT_TYPE_MEGAWATTHOUR:
				{
					dbConverted = 1.162222E-09 * dbConverterand;
					break;
				}

				case UNIT_TYPE_BOE:
				{
					dbConverted = 6.84E-10 * dbConverterand;
					break;
				}

				case UNIT_TYPE_TOE:
				{
					dbConverted = 9.9933E-11 * dbConverterand;
					break;
				}
			
				case UNIT_TYPE_CFNG:
				{
					dbConverted = 3.965666831E-06 * dbConverterand;
					break;
				}

				case UNIT_TYPE_CCFNG:
				{
					dbConverted = 3.965666831E-08 * dbConverterand;
					break;
				}

			}

			break;
		}


		/* KiloCalorie */
		case UNIT_TYPE_KILOCALORIE:
		{
		
			switch (utTo)
			{
				
				case UNIT_TYPE_JOULE:
				{
					dbConverted = 4184.0 * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_BTU:
				{
					dbConverted = 3.9656668 * dbConverterand;
					break;
				}

				case UNIT_TYPE_CALORIE:
				{
					dbConverted = 1000.0 * dbConverterand;
					break;
				}

				case UNIT_TYPE_KILOCALORIE:
				{
					dbConverted = dbConverterand;
					break;
				}

				case UNIT_TYPE_WATTHOUR:
				{
					dbConverted = 1.1622222 * dbConverterand;
					break;
				}

				case UNIT_TYPE_KILOWATTHOUR:
				{
					dbConverted = 0.011622222 * dbConverterand;
					break;
				}

				case UNIT_TYPE_MEGAWATTHOUR:
				{
					dbConverted = 1.1622222E-06 * dbConverterand;
					break;
				}

				case UNIT_TYPE_BOE:
				{
					dbConverted = 7.1428571428571E-07 * dbConverterand;
					break;
				}

				case UNIT_TYPE_TOE:
				{
					dbConverted = 1.0E-07 * dbConverterand;
					break;
				}
			
				case UNIT_TYPE_CFNG:
				{
					dbConverted = 0.00396821 * dbConverterand;
					break;
				}

				case UNIT_TYPE_CCFNG:
				{
					dbConverted = 0.0000396821 * dbConverterand;
					break;
				}

			}

			break;
		}


		/* WattHour / WH */
		case UNIT_TYPE_WATTHOUR:
		{
		
			switch (utTo)
			{
				
				case UNIT_TYPE_JOULE:
				{
					dbConverted = 3600.0 * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_BTU:
				{
					dbConverted = 3.4121416 * dbConverterand;
					break;
				}

				case UNIT_TYPE_CALORIE:
				{
					dbConverted = 860.42065 * dbConverterand;
					break;
				}

				case UNIT_TYPE_KILOCALORIE:
				{
					dbConverted = 0.86042065 * dbConverterand;
					break;
				}

				case UNIT_TYPE_WATTHOUR:
				{
					dbConverted = dbConverterand;
					break;
				}

				case UNIT_TYPE_KILOWATTHOUR:
				{
					dbConverted = ONE_ONE_THOUSANDTH_D * dbConverterand;
					break;
				}

				case UNIT_TYPE_MEGAWATTHOUR:
				{
					dbConverted = ONE_ONE_MILLIONTH_D * dbConverterand;
					break;
				}

				case UNIT_TYPE_BOE:
				{
					dbConverted = 6.1417516275642E-07 * dbConverterand;
					break;
				}

				case UNIT_TYPE_TOE:
				{
					dbConverted = 8.5984522785899E-08 * dbConverterand;
					break;
				}
			
				case UNIT_TYPE_CFNG:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_CCFNG:
				{
					dbConverted = x * dbConverterand;
					break;
				}

			}

			break;
		}

		
		/* KiloWattHour / KWh */
		case UNIT_TYPE_KILOWATTHOUR:
		{
		
			switch (utTo)
			{
				
				case UNIT_TYPE_JOULE:
				{
					dbConverted = x * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_BTU:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_CALORIE:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_KILOCALORIE:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_WATTHOUR:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_KILOWATTHOUR:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_MEGAWATTHOUR:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_BOE:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_TOE:
				{
					dbConverted = x * dbConverterand;
					break;
				}
			
				case UNIT_TYPE_CFNG:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_CCFNG:
				{
					dbConverted = x * dbConverterand;
					break;
				}

			}

			break;
		}


		/* MegaWattHour / MWh */
		case UNIT_TYPE_MEGAWATTHOUR:
		{
		
			switch (utTo)
			{
				
				case UNIT_TYPE_JOULE:
				{
					dbConverted = x * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_BTU:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_CALORIE:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_KILOCALORIE:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_WATTHOUR:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_KILOWATTHOUR:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_MEGAWATTHOUR:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_BOE:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_TOE:
				{
					dbConverted = x * dbConverterand;
					break;
				}
			
				case UNIT_TYPE_CFNG:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_CCFNG:
				{
					dbConverted = x * dbConverterand;
					break;
				}

			}

			break;
		}

		
		/* BOE / Barrel Of Oil Equivilent */
		case UNIT_TYPE_BOE:
		{
		
			switch (utTo)
			{
				
				case UNIT_TYPE_JOULE:
				{
					dbConverted = x * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_BTU:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_CALORIE:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_KILOCALORIE:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_WATTHOUR:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_KILOWATTHOUR:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_MEGAWATTHOUR:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_BOE:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_TOE:
				{
					dbConverted = x * dbConverterand;
					break;
				}
			
				case UNIT_TYPE_CFNG:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_CCFNG:
				{
					dbConverted = x * dbConverterand;
					break;
				}

			}

			break;
		}

		
		/* TOE / Ton Of Oil Equivilent */
		case UNIT_TYPE_TOE:
		{
		
			switch (utTo)
			{
				
				case UNIT_TYPE_JOULE:
				{
					dbConverted = x * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_BTU:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_CALORIE:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_KILOCALORIE:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_WATTHOUR:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_KILOWATTHOUR:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_MEGAWATTHOUR:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_BOE:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_TOE:
				{
					dbConverted = x * dbConverterand;
					break;
				}
			
				case UNIT_TYPE_CFNG:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_CCFNG:
				{
					dbConverted = x * dbConverterand;
					break;
				}

			}

			break;
		}


		/* CFNG / Cubic Feet Natural Gas */
		case UNIT_TYPE_CFNG:
		{
		
			switch (utTo)
			{
				
				case UNIT_TYPE_JOULE:
				{
					dbConverted = x * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_BTU:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_CALORIE:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_KILOCALORIE:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_WATTHOUR:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_KILOWATTHOUR:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_MEGAWATTHOUR:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_BOE:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_TOE:
				{
					dbConverted = x * dbConverterand;
					break;
				}
			
				case UNIT_TYPE_CFNG:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_CCFNG:
				{
					dbConverted = x * dbConverterand;
					break;
				}

			}

			break;
		}


		/* CCFNG / Hundred-Cubic Feet Natural Gas */
		case UNIT_TYPE_CCFNG:
		{
		
			switch (utTo)
			{
				
				case UNIT_TYPE_JOULE:
				{
					dbConverted = x * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_BTU:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_CALORIE:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_KILOCALORIE:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_WATTHOUR:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_KILOWATTHOUR:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_MEGAWATTHOUR:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_BOE:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_TOE:
				{
					dbConverted = x * dbConverterand;
					break;
				}
			
				case UNIT_TYPE_CFNG:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_CCFNG:
				{
					dbConverted = x * dbConverterand;
					break;
				}

			}

			break;
		}


		/* Power */
		
		/* Watt */
		case UNIT_TYPE_WATT:
		{
				
			switch (utTo)
			{
		
				case UNIT_TYPE_WATT:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_KILOWATT:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_MEGAWATT:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_HORSEPOWER:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_BTU_PERHOUR:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_BOE_PERHOUR:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_CFNG_PERHOUR:
				{
					dbConverted = x * dbConverterand;
					break;
				}

			}

			break;
		}


		/* KiloWatt */
		case UNIT_TYPE_KILOWATT:
		{
				
			switch (utTo)
			{
		
				case UNIT_TYPE_WATT:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_KILOWATT:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_MEGAWATT:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_HORSEPOWER:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_BTU_PERHOUR:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_BOE_PERHOUR:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_CFNG_PERHOUR:
				{
					dbConverted = x * dbConverterand;
					break;
				}

			}

			break;
		}

		
		/* Mega Watt */
		case UNIT_TYPE_MEGAWATT:
		{
				
			switch (utTo)
			{
		
				case UNIT_TYPE_WATT:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_KILOWATT:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_MEGAWATT:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_HORSEPOWER:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_BTU_PERHOUR:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_BOE_PERHOUR:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_CFNG_PERHOUR:
				{
					dbConverted = x * dbConverterand;
					break;
				}

			}

			break;
		}


		/* Horse Power */
		case UNIT_TYPE_HORSEPOWER:		
		{
				
			switch (utTo)
			{
		
				case UNIT_TYPE_WATT:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_KILOWATT:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_MEGAWATT:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_HORSEPOWER:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_BTU_PERHOUR:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_BOE_PERHOUR:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_CFNG_PERHOUR:
				{
					dbConverted = x * dbConverterand;
					break;
				}

			}

			break;
		}


		/* BTU / Hour */
		case UNIT_TYPE_BTU_PERHOUR:
		{
				
			switch (utTo)
			{
		
				case UNIT_TYPE_WATT:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_KILOWATT:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_MEGAWATT:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_HORSEPOWER:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_BTU_PERHOUR:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_BOE_PERHOUR:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_CFNG_PERHOUR:
				{
					dbConverted = x * dbConverterand;
					break;
				}

			}

			break;
		}

		
		/* BOE / Hour */
		case UNIT_TYPE_BOE_PERHOUR:
		{
				
			switch (utTo)
			{
		
				case UNIT_TYPE_WATT:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_KILOWATT:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_MEGAWATT:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_HORSEPOWER:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_BTU_PERHOUR:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_BOE_PERHOUR:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_CFNG_PERHOUR:
				{
					dbConverted = x * dbConverterand;
					break;
				}

			}

			break;
		}

		
		/* CFNG / Hour */
		case UNIT_TYPE_CFNG_PERHOUR:
		{
				
			switch (utTo)
			{
		
				case UNIT_TYPE_WATT:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_KILOWATT:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_MEGAWATT:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_HORSEPOWER:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_BTU_PERHOUR:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_BOE_PERHOUR:
				{
					dbConverted = x * dbConverterand;
					break;
				}

				case UNIT_TYPE_CFNG_PERHOUR:
				{
					dbConverted = x * dbConverterand;
					break;
				}

			}

			break;
		}


		/* Speed */
		
		/* Kilometers Per Hour / KPH */
		case UNIT_TYPE_KILOMETERS_PERHOUR:
		{

			switch (utTo)
			{
				
				case UNIT_TYPE_KILOMETERS_PERHOUR:
				{
					dbConverted = dbConverterand;
					break;
				}
				
				case UNIT_TYPE_MILES_PERSEC:
				{
					dbConverted = 0.000172603109 * dbConverterand;
					break;
				}
	
				case UNIT_TYPE_MILES_PERHOUR:
				{
					dbConverted = 0.62137119 * dbConverterand;
					break;
				}

				case UNIT_TYPE_KNOT:
				{
					dbConverted = 0.5399568 * dbConverterand;
					break;
				}

			}
	
			break;
		}

		/* Miles Per Second */
		case UNIT_TYPE_MILES_PERSEC:
		{

			switch (utTo)
			{
				
				case UNIT_TYPE_KILOMETERS_PERHOUR:
				{
					dbConverted = 5793.6384 * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_MILES_PERSEC:
				{
					dbConverted = dbConverterand;
					break;
				}
	
				case UNIT_TYPE_MILES_PERHOUR:
				{
					dbConverted = 3600.0 * dbConverterand;
					break;
				}

				case UNIT_TYPE_KNOT:
				{
					dbConverted = 3128.31447 * dbConverterand;
					break;
				}

			}
	
			break;
		}
		
		/* Miles Per Hour */
		case UNIT_TYPE_MILES_PERHOUR:
		{

			switch (utTo)
			{
				
				case UNIT_TYPE_KILOMETERS_PERHOUR:
				{
					dbConverted = 1.609344 * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_MILES_PERSEC:
				{
					dbConverted = 0.0002777777777 * dbConverterand;
					break;
				}
	
				case UNIT_TYPE_MILES_PERHOUR:
				{
					dbConverted = dbConverterand;
					break;
				}

				case UNIT_TYPE_KNOT:
				{
					dbConverted = 0.86897624 * dbConverterand;
					break;
				}

			}
	
			break;
		}

		/* Knot */
		case UNIT_TYPE_KNOT:
		{

			switch (utTo)
			{
				
				case UNIT_TYPE_KILOMETERS_PERHOUR:
				{
					dbConverted = 1.852 * dbConverterand;
					break;
				}
				
				case UNIT_TYPE_MILES_PERSEC:
				{
					dbConverted = 0.000319660958 * dbConverterand;
					break;
				}
	
				case UNIT_TYPE_MILES_PERHOUR:
				{
					dbConverted = 1.15077945 * dbConverterand;
					break;
				}

				case UNIT_TYPE_KNOT:
				{
					dbConverted = dbConverterand;
					break;
				}

			}
	
			break;
		}

	}
	
	return dbConverted;
}




_Success_(return != FALSE, ...)
PODNET_API
BOOL
ConvertUnit64Ex(
	_In_			LPUNIT64		lpuUnit,
	_In_			UNIT_TYPE		utTo
){
	EXIT_IF_UNLIKELY_NULL(lpuUnit, FALSE);
	EXIT_IF_UNLIKELY_NULL(utTo, FALSE);
	DOUBLE dbNewValue;

	dbNewValue = ConvertUnit64(lpuUnit->dbValue, lpuUnit->utUnitType, utTo);
	
	lpuUnit->dbValue = (TRUE == DOUBLE_EQUAL(dbNewValue, dbNewValue)) ? lpuUnit->dbValue : dbNewValue;
	return DOUBLE_EQUAL(dbNewValue, dbNewValue);
}












































