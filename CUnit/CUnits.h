/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CUNITS_H
#define CUNITS_H


#include "../Defs.h"
#include "../Macros.h"


typedef enum __UNIT_TYPE
{
	/* Distance */
	UNIT_TYPE_METER			= 1,
	UNIT_TYPE_CENTIMETER,
	UNIT_TYPE_MILLIMETER,
	UNIT_TYPE_MICROMETER,
	UNIT_TYPE_NANOMETER,
	UNIT_TYPE_KILOMETER,
	UNIT_TYPE_FEET,
	UNIT_TYPE_INCHES,
	UNIT_TYPE_YARDS,
	UNIT_TYPE_MILES,
	UNIT_TYPE_RODS,
	UNIT_TYPE_FURLONGS,

	/* Time */
	UNIT_TYPE_SECOND,
	UNIT_TYPE_MILLISECOND,
	UNIT_TYPE_MICROSECOND,
	UNIT_TYPE_NANOSECOND,
	UNIT_TYPE_MINUTE,
	UNIT_TYPE_HOUR,
	UNIT_TYPE_DAY,
	UNIT_TYPE_WEEK,
	UNIT_TYPE_FORTNIGHT,	/* Two weeks */
	UNIT_TYPE_MONTH, 	/* Assuming 30.42 days in one month */
	UNIT_TYPE_YEAR,		/* Assuming 365.25 days in a year */

	/* Mass */
	UNIT_TYPE_GRAM,
	UNIT_TYPE_MILLIGRAM,
	UNIT_TYPE_MICROGRAM,
	UNIT_TYPE_KILOGRAM,
	UNIT_TYPE_SLUG,
	UNIT_TYPE_SLINCH,
	UNIT_TYPE_FIRKIN,	/* One Firkin is the weight of 9 gallons of water */

	/* Force */
	UNIT_TYPE_NEWTON,
	UNIT_TYPE_POUND,

	/* Energy */
	UNIT_TYPE_JOULE,
	UNIT_TYPE_BTU,
	UNIT_TYPE_CALORIE,
	UNIT_TYPE_KILOCALORIE,
	UNIT_TYPE_WATTHOUR,
	UNIT_TYPE_KILOWATTHOUR,
	UNIT_TYPE_MEGAWATTHOUR,
	UNIT_TYPE_BOE,		/* Barrel of Oil Eqivalent */
	UNIT_TYPE_TOE,		/* Ton of Oil Equivalent */
	UNIT_TYPE_CFNG,		/* Cubic Feet of Natural Gas */
	UNIT_TYPE_CCFNG,	/* Centum (100) Cubic Feet of Natural Gas */

	/* Power */
	UNIT_TYPE_WATT,
	UNIT_TYPE_KILOWATT,
	UNIT_TYPE_MEGAWATT,
	UNIT_TYPE_HORSEPOWER,
	UNIT_TYPE_BTU_PERHOUR,
	UNIT_TYPE_BOE_PERHOUR,
	UNIT_TYPE_CFNG_PERHOUR,	

	/* Speed */
	UNIT_TYPE_KILOMETERS_PERHOUR,
	UNIT_TYPE_MILES_PERSEC,
	UNIT_TYPE_MILES_PERHOUR,
	UNIT_TYPE_KNOT,

	/* Area */
	UNIT_TYPE_SQUAREINCHES,
	UNIT_TYPE_SQUAREFEET,
	UNIT_TYPE_ACRE,
	UNIT_TYPE_HECTARE,
	UNIT_TYPE_SQUAREMILE,
	UNIT_TYPE_SQUAREKILOMETER,
	UNIT_TYPE_SQUAREMETER,
	UNIT_TYPE_SQUAREMILLIMETER,
	UNIT_TYPE_SQUARECENTIMETER,

	/* Volume */
	UNIT_TYPE_GALLON,
	UNIT_TYPE_QUART,
	UNIT_TYPE_BARREL,
	UNIT_TYPE_CORD,
	UNIT_TYPE_LITER,
	UNIT_TYPE_CUBICMETER,
	UNIT_TYPE_CUBICCENTIMETER,
	UNIT_TYPE_CUBICMILLIMETER,
	UNIT_TYPE_CUBICINCH,
	UNIT_TYPE_CUBICFOOT,

} UNIT_TYPE;



/* Use this to use a DOUBLE */
typedef struct __UNIT64
{
        DOUBLE          dbValue;
        UNIT_TYPE       utUnitType;
} UNIT64, *LPUNIT64, **DLPUNIT64, ***TLPUNIT64;



/* Use this to use a FLOAT */
typedef struct __UNIT
{
	FLOAT 		fValue;
	UNIT_TYPE 	utUnitType;
} UNIT, *LPUNIT, **DLPUNIT, ***TLPUNIT;




_Success_(return != NULLPTR, ...)
PODNET_API
LPUNIT
CreateUnit(
	_In_Opt_		LPUNIT		lpuUnit,
	_In_			FLOAT		fValue,
	_In_			UNIT_TYPE	utType
);




_Success_(return != NULLPTR, ...)
PODNET_API
LPUNIT64
CreateUnit64(
	_In_Opt_		LPUNIT		lpuUnit64,
	_In_			DOUBLE		dbValue,
	_In_			UNIT_TYPE	utType
);




_Success_(return != FALSE, ...)
PODNET_API
BOOL
DestroyUnit(
	_In_			LPUNIT		lpuUnit
);

























































#endif
