/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/



/* https://github.com/B-Con/crypto-algorithms */


#ifndef CSHA256_H
#define CSHA256_H


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../CError/CError.h"


#define SHA256_BUFFER_SIZE              0x20

typedef struct _SHA_256_OBJECT           *LPSHA256;




_Result_Null_On_Failure_
PODNET_API
LPSHA256
CreateSha256Object(
        VOID
);



_Success_(return != FALSE, ...)
PODNET_API
BOOL
DestroySha256Object(
        _In_                                                    LPSHA256                                lpsSha
);




_Success_(return != FALSE, ...)
PODNET_API
BOOL
SetSha256State(
        _In_                                                    LPSHA256                                lpsSha,
        _In_                                                    LPBYTE                                  lpbData,
        _In_                                                    ULONGLONG                               ullDataSize
);




_Success_(return != FALSE, ...)
PODNET_API
BOOL
CalculateSha256Hash(
        _In_                                                    LPSHA256                                lpsSha,
        _Out_Writes_Bytes_(sizeof(BYTE) * SHA256_BUFFER_SIZE)   LPBYTE                                  lpbOut
);




_Success_(return != FALSE, ...)
PODNET_API
BOOL
Sha256(
        _In_                                                    LPBYTE                                  lpbData,
        _In_                                                    ULONGLONG                               ullDataSize,
        _Out_Writes_Bytes_(sizeof(BYTE) * SHA256_BUFFER_SIZE)   LPBYTE                                  lpbOut
);




_Success_(return != FALSE, ...)
PODNET_API
BOOL
Sha256ToHexString(
        _In_                                                    LPBYTE                                  lpbSha256Value,
        _Out_Writes_Bytes_(sizeof(CHAR) * SHA256_BUFFER_SIZE)   LPCSTRING                               lpcsOut
);







#endif