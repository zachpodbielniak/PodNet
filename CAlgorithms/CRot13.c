/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CRot13.h"



_Success_(return != FALSE, ...)
PODNET_API
BOOL
Rot13(
        _In_Out_Updates_Z_(StringLength(lpcsString))                    LPCSTRING               lpcsString
){
        ULONG ulCase, ulIndex, ulLength;

        for (
                ulIndex = 0, ulLength = StringLength(lpcsString);
                ulIndex < ulLength; 
                ulIndex++
        ){
                if (lpcsString[ulIndex] < ASCII_A || (lpcsString[ulIndex] > ASCII_Z && lpcsString[ulIndex] < ASCII_A_LOWER) || lpcsString[ulIndex] > ASCII_Z_LOWER)
                        continue;
                if (lpcsString[ulIndex] >= ASCII_A_LOWER)
                        ulCase = (ULONG)ASCII_A_LOWER;
                else ulCase = (ULONG)ASCII_A;

                lpcsString[ulIndex] = (lpcsString[ulIndex] + 13) % (ulCase + 26);
                if (lpcsString[ulIndex] < 26)
                        lpcsString[ulIndex] += ulCase;
        }

        return TRUE;
}