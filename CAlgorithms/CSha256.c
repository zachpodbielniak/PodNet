/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CSha256.h"


#define SHA256_ROTLEFT(a,b) 	(((a) << (b)) | ((a) >> (32-(b))))
#define SHA256_ROTRIGHT(a,b) 	(((a) >> (b)) | ((a) << (32-(b))))
#define SHA256_CH(x,y,z) 	(((x) & (y)) ^ (~(x) & (z)))
#define SHA256_MAJ(x,y,z) 	(((x) & (y)) ^ ((x) & (z)) ^ ((y) & (z)))
#define SHA256_EP0(x) 		(SHA256_ROTRIGHT(x,2) ^ SHA256_ROTRIGHT(x,13) ^ SHA256_ROTRIGHT(x,22))
#define SHA256_EP1(x) 		(SHA256_ROTRIGHT(x,6) ^ SHA256_ROTRIGHT(x,11) ^ SHA256_ROTRIGHT(x,25))
#define SHA256_SIG0(x) 		(SHA256_ROTRIGHT(x,7) ^ SHA256_ROTRIGHT(x,18) ^ ((x) >> 3))
#define SHA256_SIG1(x) 		(SHA256_ROTRIGHT(x,17) ^ SHA256_ROTRIGHT(x,19) ^ ((x) >> 10))



typedef struct _SHA_256_OBJECT
{
	BYTE 		bData[64];
	ULONG		ulDataLength;
	ULONGLONG	ullBitLength;
	ULONG		ulState[8];
} SHA_256_OBJECT, *LPSHA_256_OBJECT;




GLOBAL_VARIABLE CONSTANT ULONG ulK[64] = 
{
	0x428a2f98,0x71374491,0xb5c0fbcf,0xe9b5dba5,0x3956c25b,0x59f111f1,0x923f82a4,0xab1c5ed5,
	0xd807aa98,0x12835b01,0x243185be,0x550c7dc3,0x72be5d74,0x80deb1fe,0x9bdc06a7,0xc19bf174,
	0xe49b69c1,0xefbe4786,0x0fc19dc6,0x240ca1cc,0x2de92c6f,0x4a7484aa,0x5cb0a9dc,0x76f988da,
	0x983e5152,0xa831c66d,0xb00327c8,0xbf597fc7,0xc6e00bf3,0xd5a79147,0x06ca6351,0x14292967,
	0x27b70a85,0x2e1b2138,0x4d2c6dfc,0x53380d13,0x650a7354,0x766a0abb,0x81c2c92e,0x92722c85,
	0xa2bfe8a1,0xa81a664b,0xc24b8b70,0xc76c51a3,0xd192e819,0xd6990624,0xf40e3585,0x106aa070,
	0x19a4c116,0x1e376c08,0x2748774c,0x34b0bcb5,0x391c0cb3,0x4ed8aa4a,0x5b9cca4f,0x682e6ff3,
	0x748f82ee,0x78a5636f,0x84c87814,0x8cc70208,0x90befffa,0xa4506ceb,0xbef9a3f7,0xc67178f2
};




_Success_(return != FALSE, ...)
INTERNAL_OPERATION
VOID
__Transform(
	_In_ 							LPSHA256				lpsSha,
	_In_							LPBYTE					lpbData
){
	ULONG ulA, ulB, ulC, ulD, ulE, ulF, ulG, ulH, ulT1, ulT2, ulM[64];
	ULONG ulIndex;
	ULONG ulJndex;
	for (
		ulIndex = 0, ulJndex = 0; 
		ulIndex < 16; 
		ulIndex++, ulJndex += 4
	){ ulM[ulIndex] = (lpbData[ulJndex] << 24) | (lpbData[ulJndex + 1] << 16) | (lpbData[ulJndex + 2] << 8) | (lpbData[ulJndex + 3]); }

	for (
		;
		ulIndex < 64;
		ulIndex++
	){ ulM[ulIndex] = SHA256_SIG1(ulM[ulIndex - 2]) + ulM[ulIndex - 7] + SHA256_SIG0(ulM[ulIndex - 15]) + ulM[ulIndex - 16]; }


	ulA = lpsSha->ulState[0];
	ulB = lpsSha->ulState[1];
	ulC = lpsSha->ulState[2];
	ulD = lpsSha->ulState[3];
	ulE = lpsSha->ulState[4];
	ulF = lpsSha->ulState[5];
	ulG = lpsSha->ulState[6];
	ulH = lpsSha->ulState[7];

	for (
		ulIndex = 0; 
		ulIndex < 64; 
		ulIndex++
	){
		ulT1 = ulH + SHA256_EP1(ulE) + SHA256_CH(ulE, ulF, ulG) + ulK[ulIndex] + ulM[ulIndex];
		ulT2 = SHA256_EP0(ulA) + SHA256_MAJ(ulA, ulB, ulC);
		ulH = ulG;
		ulG = ulF;
		ulF = ulE;
		ulE = ulD + ulT1;
		ulD = ulC;
		ulC = ulB;
		ulB = ulA;
		ulA = ulT1 + ulT2; 
	}

	lpsSha->ulState[0] += ulA;
	lpsSha->ulState[1] += ulB;
	lpsSha->ulState[2] += ulC;
	lpsSha->ulState[3] += ulD;
	lpsSha->ulState[4] += ulE;
	lpsSha->ulState[5] += ulF;
	lpsSha->ulState[6] += ulG;
	lpsSha->ulState[7] += ulH;
}




_Result_Null_On_Failure_
PODNET_API
LPSHA256
CreateSha256Object(
        VOID
){
	LPSHA256 lpsSha;

	lpsSha = LocalAllocAndZero(sizeof(SHA_256_OBJECT));
	/* cppcheck-suppress memleak */
	EXIT_IF_UNLIKELY_NULL(lpsSha, NULLPTR);


	lpsSha->ulDataLength = 0;
	lpsSha->ullBitLength = 0ULL;

	lpsSha->ulState[0] = 0x6A09E667;
	lpsSha->ulState[1] = 0xBB67AE85;
	lpsSha->ulState[2] = 0x3C6EF372;
	lpsSha->ulState[3] = 0xA54FF53A;
	lpsSha->ulState[4] = 0x510E527F;
	lpsSha->ulState[5] = 0x9B05688C;
	lpsSha->ulState[6] = 0x1F83D9AB;
	lpsSha->ulState[7] = 0x5BE0CD19;

	return lpsSha;
}




_Success_(return != FALSE, ...)
PODNET_API
BOOL
DestroySha256Object(
        _In_                                                    LPSHA256                                lpsSha
){
	ZeroMemory(lpsSha, sizeof(SHA_256_OBJECT));
	FreeMemory(lpsSha);
	return TRUE;
}




_Success_(return != FALSE, ...)
PODNET_API
BOOL
SetSha256State(
        _In_                                                    LPSHA256                                lpsSha,
        _In_                                                    LPBYTE                                  lpbData,
        _In_                                                    ULONGLONG                               ullDataSize
){
	ULONGLONG ullIndex;
	for (
		ullIndex = 0;
		ullIndex < ullDataSize;
		ullIndex++
	){
		lpsSha->bData[lpsSha->ulDataLength] = lpbData[ullIndex];
		lpsSha->ulDataLength++;
		if (lpsSha->ulDataLength == 64)
		{
			__Transform(lpsSha, lpsSha->bData);
			lpsSha->ullBitLength += 512;
			lpsSha->ulDataLength = 0;
		}
	}
	return TRUE;
}




_Success_(return != FALSE, ...)
PODNET_API
BOOL
CalculateSha256Hash(
        _In_                                                    LPSHA256                                lpsSha,
        _Out_Writes_Bytes_(sizeof(BYTE) * SHA256_BUFFER_SIZE)   LPBYTE                                  lpbOut
){
	ULONG ulIndex;
	ulIndex = lpsSha->ulDataLength;

	if (lpsSha->ulDataLength < 56) {
		lpsSha->bData[ulIndex++] = 0x80;
		while (ulIndex < 56)
		{ lpsSha->bData[ulIndex++] = 0x00; }
	}
	else
	{
		lpsSha->bData[ulIndex++] = 0x80;
		while (ulIndex < 64)
		{ lpsSha->bData[ulIndex++] = 0x00; }
		__Transform(lpsSha, lpsSha->bData);
		SetMemory(lpsSha->bData, 0, 56);
	}

	lpsSha->ullBitLength += lpsSha->ulDataLength * 8;
	lpsSha->bData[63] = lpsSha->ullBitLength;
	lpsSha->bData[62] = lpsSha->ullBitLength >> 8;
	lpsSha->bData[61] = lpsSha->ullBitLength >> 16;
	lpsSha->bData[60] = lpsSha->ullBitLength >> 24;
	lpsSha->bData[59] = lpsSha->ullBitLength >> 32;
	lpsSha->bData[58] = lpsSha->ullBitLength >> 40;
	lpsSha->bData[57] = lpsSha->ullBitLength >> 48;
	lpsSha->bData[56] = lpsSha->ullBitLength >> 56;
	__Transform(lpsSha, lpsSha->bData);

	/* 
		TODO: Add flags to see if we are little endian or big endian. 
		We are converting to bid endian here.
	*/
	
	for (
		ulIndex = 0; 
		ulIndex < 4; 
		ulIndex++
	){
		lpbOut[ulIndex]		= (lpsSha->ulState[0]) >> (24 - ulIndex * 8) & 0x000000FF;
		lpbOut[ulIndex + 4]	= (lpsSha->ulState[1]) >> (24 - ulIndex * 8) & 0x000000FF;
		lpbOut[ulIndex + 8]	= (lpsSha->ulState[2]) >> (24 - ulIndex * 8) & 0x000000FF;
		lpbOut[ulIndex + 12]	= (lpsSha->ulState[3]) >> (24 - ulIndex * 8) & 0x000000FF;
		lpbOut[ulIndex + 16]	= (lpsSha->ulState[4]) >> (24 - ulIndex * 8) & 0x000000FF;
		lpbOut[ulIndex + 20]	= (lpsSha->ulState[5]) >> (24 - ulIndex * 8) & 0x000000FF;
		lpbOut[ulIndex + 24]	= (lpsSha->ulState[6]) >> (24 - ulIndex * 8) & 0x000000FF;
		lpbOut[ulIndex + 28]	= (lpsSha->ulState[7]) >> (24 - ulIndex * 8) & 0x000000FF;
	}
	return TRUE;
}




_Success_(return != FALSE, ...)
PODNET_API
BOOL
Sha256(
        _In_                                                    LPBYTE                                  lpbData,
        _In_                                                    ULONGLONG                               ullDataSize,
        _Out_Writes_Bytes_(sizeof(BYTE) * SHA256_BUFFER_SIZE)   LPBYTE                                  lpbOut
){
	LPSHA256 lpsSha;
	lpsSha = CreateSha256Object();
	EXIT_IF_UNLIKELY_NULL(lpsSha, FALSE);
	SetSha256State(lpsSha, lpbData, ullDataSize);
	CalculateSha256Hash(lpsSha, lpbOut);
	DestroySha256Object(lpsSha);
	return TRUE;
}





_Success_(return != FALSE, ...)
PODNET_API
BOOL
Sha256ToHexString(
        _In_                                                    LPBYTE                                  lpbSha256Value,
        _Out_Writes_Bytes_(sizeof(CHAR) * SHA256_BUFFER_SIZE)   LPCSTRING                               lpcsOut
){
        BYTE bIndex;
        LPULONGLONG lpullIndex;
	CSTRING csBuffer[16];
        lpullIndex = (LPULONGLONG)lpbSha256Value;
	for (
		bIndex = 0;
		bIndex < 4;
		bIndex++
	){ 
		#ifdef __LP64__
		StringPrintFormat(csBuffer, "%lx", LITTLE_TO_BIG_ENDIAN_64(lpullIndex[bIndex])); 
		#else
		StringPrintFormat(csBuffer, "%llx", LITTLE_TO_BIG_ENDIAN_64(lpullIndex[bIndex])); 
		#endif 
		StringConcatenate(lpcsOut, csBuffer);
	}
	return TRUE;
}
