/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CBASE64_H
#define CBASE64_H


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"




_Success_(return != MAX_ULONGLONG, _Non_Locking_)
PODNET_API
ULONGLONG 
Base64EncodedLength(
	_In_		ULONGLONG		ullLength
);




_Success_(return != MAX_ULONGLONG, _Non_Locking_)
PODNET_API
ULONGLONG 
Base64DecodedLength(
	_In_		LPCSTR			lpcszEncodedData
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPSTR
Base64Encode(
	_In_Z_		LPVOID			lpData,
	_In_		ULONGLONG		ullInputSize,
	_Out_Opt_	LPULONGLONG		lpullOutSize
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API 
BOOL 
Base64Decode(
	_In_Z_		LPCSTR			lpcszEncodedData, 
	_Out_		LPBYTE			lpbyOut, 
	_In_		ULONGLONG		ullOutSize
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPBYTE 
Base64DecodeAllocOut(
	_In_Z_		LPCSTR			lpcszEncodedData,
	_In_		BOOL			bNullTerm
);




#endif