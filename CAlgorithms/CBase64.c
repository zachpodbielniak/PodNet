/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CBase64.h"




GLOBAL_VARIABLE CONSTANT CSTRING __csBase64Characters[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
GLOBAL_VARIABLE CONSTANT LONG __lplBase64Inverse[] = { 	
							62, -1, -1, -1, 63, 52, 53, 54, 55, 56, 57, 58,
							59, 60, 61, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5,
							6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
							21, 22, 23, 24, 25, -1, -1, -1, -1, -1, -1, 26, 27, 28,
							29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42,
							43, 44, 45, 46, 47, 48, 49, 50, 51 
						};



_Success_(return != MAX_ULONGLONG, _Non_Locking_)
PODNET_API
ULONGLONG 
Base64EncodedLength(
	_In_		ULONGLONG		ullLength
){
	ULONGLONG ullSize;

	ullSize = ullLength;
	if (ullLength % 3 != 0)
	{ ullSize += 3 - (ullLength % 3); }

	ullSize /= 3;
	ullSize *= 4;

	return ullSize;
}




_Success_(return != MAX_ULONGLONG, _Non_Locking_)
PODNET_API
ULONGLONG 
Base64DecodedLength(
	_In_		LPCSTR			lpcszEncodedData
){
	EXIT_IF_UNLIKELY_NULL(lpcszEncodedData, (ULONGLONG)-1);

	ULONGLONG ullSize;
	ULONGLONG ullLength;
	ULONGLONG ullIterator;

	ullLength = StringLength(lpcszEncodedData);

	if (0 == ullLength)
	{ return (ULONGLONG)-1; }

	ullSize = (ullLength / 4) * 3;

	for (
		ullIterator = ullLength;
		ullIterator-- > 0;
		NO_OPERATION()
	){
		if (lpcszEncodedData[ullIterator] == '=')
		{ ullSize--; }
		else
		{ break; }
	}

	return ullSize;
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPSTR
Base64Encode(
	_In_Z_		LPVOID			lpData,
	_In_		ULONGLONG		ullInputSize,
	_Out_Opt_	LPULONGLONG		lpullOutSize
){
	EXIT_IF_UNLIKELY_NULL(lpData, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(ullInputSize, NULLPTR); 

	LPBYTE lpbyData;
	LPSTR lpszOut;
	ULONGLONG ullOutputSize;
	ULONGLONG ullIterator;
	ULONGLONG ullJterator;
	ULONGLONG ullTemp;

	lpbyData = lpData;

	ullOutputSize = Base64EncodedLength(ullInputSize);
	lpszOut  = GlobalAllocAndZero(ullOutputSize+1);

	if (NULLPTR != lpullOutSize)
	{ *lpullOutSize = ullOutputSize; }

	for (
		ullIterator=0, ullJterator=0; 
		ullIterator < ullInputSize; 
		ullIterator+=3, ullJterator+=4
	){
		ullTemp = lpbyData[ullIterator];
		ullTemp = ullIterator + 1 < ullInputSize ? ullTemp << 8 | lpbyData[ullIterator + 1] : ullTemp << 8;
		ullTemp = ullIterator + 2 < ullInputSize ? ullTemp << 8 | lpbyData[ullIterator + 2] : ullTemp << 8;

		lpszOut[ullJterator] = __csBase64Characters[(ullTemp >> 18) & 0x3F];
		lpszOut[ullJterator + 1] = __csBase64Characters[(ullTemp >> 12) & 0x3F];

		if (ullIterator + 1 < ullInputSize) 
		{ lpszOut[ullJterator + 2] = __csBase64Characters[(ullTemp >> 6) & 0x3F]; } 
		else 
		{ lpszOut[ullJterator + 2] = '='; }

		if (ullIterator + 2 < ullInputSize) 
		{ lpszOut[ullJterator + 3] = __csBase64Characters[ullTemp & 0x3F]; } 
		else 
		{ lpszOut[ullJterator + 3] = '='; }
	}

	return lpszOut;
}



_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL 
__Base64IsValidChar(
	_In_		CHAR			cData
){
	if (
		(cData >= '0' && cData <= '9') ||
		(cData >= 'A' && cData <= 'Z') ||
		(cData >= 'a' && cData <= 'z') ||
		(cData == '+' || cData == '/' || cData == '=')
	){ return TRUE; }

	return FALSE;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API 
BOOL 
Base64Decode(
	_In_Z_		LPCSTR			lpcszEncodedData, 
	_Out_		LPBYTE			lpbyOut, 
	_In_		ULONGLONG		ullOutSize
){
	EXIT_IF_UNLIKELY_NULL(lpcszEncodedData, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpbyOut, FALSE); 
	ULONGLONG ullLength;
	ULONGLONG ullIterator;
	ULONGLONG ullJterator;
	LONG lTemp;

	ullLength = StringLength(lpcszEncodedData);
	if (ullOutSize < Base64DecodedLength(lpcszEncodedData) || 0 != ullLength % 4)
	{ return FALSE; }

	for (
		ullIterator = 0; 
		ullIterator < ullLength;
		ullIterator++
	){
		if (FALSE == __Base64IsValidChar(lpcszEncodedData[ullIterator]))
		{ return FALSE; }
	}

	for (
		ullIterator = 0, ullJterator = 0; 
		ullIterator < ullLength; 
		ullIterator += 4, ullJterator += 3
	){
		lTemp = __lplBase64Inverse[lpcszEncodedData[ullIterator] - 43];
		lTemp = (lTemp << 6) | __lplBase64Inverse[lpcszEncodedData[ullIterator + 1] - 43];
		lTemp = lpcszEncodedData[ullIterator + 2]=='=' ? lTemp << 6 : (lTemp << 6) | __lplBase64Inverse[lpcszEncodedData[ullIterator + 2] - 43];
		lTemp = lpcszEncodedData[ullIterator + 3]=='=' ? lTemp << 6 : (lTemp << 6) | __lplBase64Inverse[lpcszEncodedData[ullIterator + 3] - 43];

		lpbyOut[ullJterator] = (lTemp >> 16) & 0xFF;

		if (lpcszEncodedData[ullIterator + 2] != '=')
		{ lpbyOut[ullJterator + 1] = (lTemp >> 8) & 0xFF; }

		if (lpcszEncodedData[ullIterator + 3] != '=')
		{ lpbyOut[ullJterator + 2] = lTemp & 0xFF; } 
	}

	return 1;
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPBYTE 
Base64DecodeAllocOut(
	_In_Z_		LPCSTR			lpcszEncodedData,
	_In_		BOOL			bNullTerm
){
	EXIT_IF_UNLIKELY_NULL(lpcszEncodedData, NULLPTR);

	LPBYTE lpbyOut;
	ULONGLONG ullOutSize;

	ullOutSize = Base64DecodedLength(lpcszEncodedData);
	lpbyOut = (TRUE == bNullTerm) ? GlobalAllocAndZero(ullOutSize + 1) : GlobalAllocAndZero(ullOutSize);

	Base64Decode(lpcszEncodedData, lpbyOut, ullOutSize);
	return lpbyOut;
}