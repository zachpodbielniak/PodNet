/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CThread.h"
#include <time.h>


typedef struct _THREAD_OBJ
{
	INHERITS_FROM_HANDLE();
	pthread_t               pThread;
	pthread_cond_t          ptcCondition;
	HANDLE                  hMutex;
	LPFN_THREAD_PROC        lpfnProc;
	LPVOID                  lpArgument;
	BOOL                    bIsSuspended;
} THREAD_OBJ, *LPTHREAD_OBJ, **DLPTHREAD_OBJ;


GLOBAL_VARIABLE DONT_OPTIMIZE_OUT HANDLE hHandleTable = NULLPTR;
GLOBAL_VARIABLE DONT_OPTIMIZE_OUT HANDLE hMainThread = NULLPTR;



_Success_(return != FALSE, {})
INTERNAL_OPERATION
BOOL
__DestroyThread(
    _In_        HDERIVATIVE         hDerivative,
    _In_Opt_    ULONG               ulFlags
){
	UNREFERENCED_PARAMETER(ulFlags);
	EXIT_IF_UNLIKELY_NULL(hDerivative, FALSE);
	LPTHREAD_OBJ lpThread;
	lpThread = (LPTHREAD_OBJ)hDerivative;
	
	KillThread(lpThread->hThis);
	DestroyObject(lpThread->hMutex);
	FreeMemory(lpThread);
	return TRUE;
}


_Success_(return != FALSE, {})
INTERNAL_OPERATION
BOOL
__WaitForThreadToFinish(
	_In_            HANDLE                  hBase,
	_In_Opt_        ULONGLONG               ullMilliseconds
){
	EXIT_IF_UNLIKELY_NULL(hBase, FALSE);
	EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hBase), HANDLE_TYPE_THREAD, FALSE);
	UNREFERENCED_PARAMETER(ullMilliseconds);

	LPTHREAD_OBJ lpThread;
	LONG lRet;
	lpThread = (LPTHREAD_OBJ)GetHandleDerivative(hBase);
	EXIT_IF_NULL(lpThread, FALSE);

	if (INFINITE_WAIT_TIME == ullMilliseconds)
	{ lRet = pthread_join(lpThread->pThread, NULLPTR); }
	else if (0 == ullMilliseconds)
	{ 
		#ifndef __linux__
		TIMESPEC tmSpec;
		CreateTimeSpec(&tmSpec, 0);
		lRet = pthread_timedjoin_np(lpThread->pThread, NULLPTR, &tmSpec);
		#else
		lRet = pthread_tryjoin_np(lpThread->pThread, NULLPTR); 	
		#endif
	}
	else
	{
		TIMESPEC tmSpec, tmTime;
		CreateTimeSpec(&tmSpec, ullMilliseconds);
		GetTime(&tmTime);
		tmTime.tv_nsec += tmSpec.tv_nsec;
		tmTime.tv_sec += tmSpec.tv_sec;
		lRet = pthread_timedjoin_np(lpThread->pThread, NULLPTR, &tmTime);   
	}

	return (lRet == 0) ? TRUE : FALSE;
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__CreateHandleTable(
	VOID
){
	if (NULLPTR == hHandleTable)
	{ 
		hHandleTable = CreateHandleTable(40, NULLPTR, HANDLE_TABLE_HIGH_PERFORMANCE);
		EXIT_IF_NULL(hHandleTable, FALSE);
	}
	return TRUE;
}




_Call_Back_
_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__ReverseThreadLookup(
	_In_            HDERIVATIVE             hLookingFor,
	_In_            HDERIVATIVE             hComparand
){
	EXIT_IF_NULL(hLookingFor, FALSE);
	EXIT_IF_NULL(hComparand, FALSE);

	pthread_t ptLookUp;
	LPTHREAD_OBJ lpThread;
	ptLookUp = *(pthread_t *)hLookingFor;
	lpThread = (LPTHREAD_OBJ)hComparand;
	return (ptLookUp == lpThread->pThread) ? TRUE : FALSE;
}




_Result_Null_On_Failure_
PODNET_API 
HANDLE
CreateThread(
	_In_            LPFN_THREAD_PROC        lpfnProc, 
	_In_Opt_        LPVOID                  lpArgument, 
	_Out_Opt_       LPULONG                 ulID
){
	HANDLE hThread;
	LPTHREAD_OBJ lpThread;
	EXIT_IF_UNLIKELY_NULL(lpfnProc, NULLPTR);


	if (UNLIKELY(NULLPTR == hHandleTable))
	{ EXIT_IF_UNLIKELY_NOT_VALUE(__CreateHandleTable(), TRUE, NULLPTR); }


	if (UNLIKELY(NULLPTR == hMainThread))
	{
		lpThread = LocalAllocAndZero(sizeof(THREAD_OBJ));
		/* cppcheck-suppress memleak */
		EXIT_IF_UNLIKELY_NULL(lpThread, NULLPTR);

		lpThread->bIsSuspended = FALSE;
		lpThread->hMutex = NULLPTR;
		lpThread->lpArgument = NULLPTR;
		lpThread->lpfnProc = NULLPTR;
		lpThread->pThread = pthread_self();
		
		hThread = CreateHandleWithSingleInheritor(
			lpThread,
			&(lpThread->hThis),
			HANDLE_TYPE_THREAD,
			__DestroyThread,
			0,
			NULLPTR,
			0,
			NULLPTR,
			0,
			&(lpThread->ullId),
			0
		);  
		EXIT_IF_NULL(hThread, NULLPTR);

		SetHandleEventWaitFunction(hThread, __WaitForThreadToFinish, 0);
		RegisterHandle(hHandleTable, hThread);
		lpThread = NULLPTR;
	}

	lpThread = LocalAllocAndZero(sizeof(THREAD_OBJ));
	EXIT_IF_NULL(lpThread, NULLPTR);

	lpThread->lpfnProc = lpfnProc;
	lpThread->lpArgument = lpArgument;
	lpThread->bIsSuspended = FALSE;
	
	lpThread->hMutex = CreateMutex();
	if (NULL_OBJECT == lpThread->hMutex)
	{ 
		FreeMemory(lpThread);
		return NULL_OBJECT;
	}


	if (ulID != NULLPTR)
	{ *ulID = (ULONG)pthread_create(&(lpThread->pThread), NULL, lpfnProc, lpArgument); }
	else
	{ pthread_create(&(lpThread->pThread), NULL, lpfnProc, lpArgument); }

	hThread = CreateHandleWithSingleInheritor(
		lpThread,
		&(lpThread->hThis),
		HANDLE_TYPE_THREAD,
		__DestroyThread,
		0,
		NULLPTR,
		0,
		NULLPTR,
		0,
		&(lpThread->ullId),
		0
	);  
	EXIT_IF_NULL(hThread, NULLPTR);

	SetHandleEventWaitFunction(hThread, __WaitForThreadToFinish, 0);
	RegisterHandle(hHandleTable, hThread);

	return hThread;
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HANDLE
GetCurrentThread(
	VOID
){
	HANDLE hRet;

	if (UNLIKELY(NULLPTR == hHandleTable))
	{ EXIT_IF_UNLIKELY_NOT_VALUE(__CreateHandleTable(), TRUE, NULLPTR); }
	

	if (UNLIKELY(NULLPTR == hMainThread))
	{
		HANDLE hThread;
		LPTHREAD_OBJ lpThread;
		lpThread = LocalAllocAndZero(sizeof(THREAD_OBJ));
		/* cppcheck-suppress memleak */
		EXIT_IF_UNLIKELY_NULL(lpThread, NULLPTR);

		lpThread->bIsSuspended = FALSE;
		lpThread->hMutex = NULLPTR;
		lpThread->lpArgument = NULLPTR;
		lpThread->lpfnProc = NULLPTR;
		lpThread->pThread = pthread_self();
		
		hThread = CreateHandleWithSingleInheritor(
			lpThread,
			&(lpThread->hThis),
			HANDLE_TYPE_THREAD,
			__DestroyThread,
			0,
			NULLPTR,
			0,
			NULLPTR,
			0,
			&(lpThread->ullId),
			0
		);  
		EXIT_IF_NULL(hThread, NULLPTR);

		hMainThread = hThread;

		SetHandleEventWaitFunction(hThread, __WaitForThreadToFinish, 0);
		RegisterHandle(hHandleTable, hThread);
	}
	
	pthread_t pThread;
	pThread = pthread_self();
	
	hRet = GrabHandleByDerivative(
		hHandleTable, 
		(HDERIVATIVE)&pThread, 
		NULL, 
		__ReverseThreadLookup
	);  

	if (NULL_OBJECT != hRet)
	{ IncrementObjectReferenceCount(hRet); }

	return hRet;      
}




_Success_(return != FALSE, {})
PODNET_API
BOOL
KillThread(
	HANDLE              hThread
){
	EXIT_IF_UNLIKELY_NULL(hThread, FALSE);
	EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hThread), HANDLE_TYPE_THREAD, FALSE);
	LPTHREAD_OBJ lpThread;
	lpThread = GetHandleDerivative(hThread);
	EXIT_IF_NULL(lpThread, FALSE);
	if ((LPVOID)hThread != NULL)
		    pthread_cancel(lpThread->pThread);           
	return TRUE;
}




PODNET_API
BOOL
WaitForSingleThread(
	HANDLE              hThread
){
	EXIT_IF_UNLIKELY_NULL(hThread, FALSE);
	EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hThread), HANDLE_TYPE_THREAD, FALSE);
	LPTHREAD_OBJ lpThread;
	lpThread = GetHandleDerivative(hThread);
	EXIT_IF_NULL(lpThread, FALSE);
	
	if ((LPVOID)hThread != (LPVOID)NULL)
		pthread_join(lpThread->pThread, NULL);
	return TRUE;
}




_Success_(return != FALSE, ...)
PODNET_API
BOOL
SuspendThread(
	_In_                    HANDLE          hThread
){
	EXIT_IF_UNLIKELY_NULL(hThread, FALSE);
	EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hThread), HANDLE_TYPE_THREAD, FALSE);
	LPTHREAD_OBJ lpThread;
	lpThread = GetHandleDerivative(hThread);
	EXIT_IF_UNLIKELY_NULL(lpThread, FALSE);

	WaitForSingleObject(lpThread->hMutex, INFINITE_WAIT_TIME);
	
	ReleaseSingleObject(lpThread->hMutex);

	return TRUE;
}




_Success_(return != FALSE, ...)
PODNET_API
BOOL
ResumeThread(
	_In_                    HANDLE          hThread
);




_Success_(return != FALSE, ...)
PODNET_API 
BOOL 
SetThreadName(
	_In_ 			HANDLE 		hThread,
	_In_Z_ 			LPCSTR 		lpcszName
){
	EXIT_IF_UNLIKELY_NULL(hThread, FALSE);
	EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hThread), HANDLE_TYPE_THREAD, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpcszName, FALSE);

	LPTHREAD_OBJ lpThread;
	lpThread = GetHandleDerivative(hThread);
	EXIT_IF_UNLIKELY_NULL(lpThread, FALSE);

	if (16 <= StringLength(lpcszName))
	{ return FALSE; }

	WaitForSingleObject(lpThread->hMutex, INFINITE_WAIT_TIME);
	pthread_setname_np(lpThread->pThread, lpcszName);
	ReleaseSingleObject(lpThread->hMutex);

	return TRUE;
}




_Success_(return != NULLPTR, ...)
PODNET_API 
LPCSTR
GetThreadName(
	_In_ 			HANDLE 		hThread
){
	EXIT_IF_UNLIKELY_NULL(hThread, NULLPTR);
	EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hThread), HANDLE_TYPE_THREAD, NULLPTR);

	LPTHREAD_OBJ lpThread;
	CSTRING csBuffer[16];
	LPCSTR lpcszRet;

	lpThread = GetHandleDerivative(hThread);
	EXIT_IF_UNLIKELY_NULL(lpThread, FALSE);

	WaitForSingleObject(lpThread->hMutex, INFINITE_WAIT_TIME);

	pthread_getname_np(lpThread->pThread, csBuffer, sizeof(csBuffer));
	lpcszRet = GlobalAllocAndZero(sizeof(csBuffer));
	StringCopySafe((LPSTR)lpcszRet, csBuffer, sizeof(csBuffer) - 0x01U);

	ReleaseSingleObject(lpThread->hMutex);

	return lpcszRet;
}