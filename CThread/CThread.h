/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CTHREADS_H
#define CTHREADS_H

#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../CHandle/CHandle.h"
#include "../CHandle/CHandleTable.h"
#include "../CClock/CClock.h"
#include "../CLock/CMutex.h"


typedef LPVOID(* LPFN_THREAD_PROC)(
    _In_Opt_    LPVOID      lpParam
);




_Result_Null_On_Failure_
PODNET_API
HANDLE
CreateThread(
        LPFN_THREAD_PROC    lpFnProc,
        LPVOID lpArgument,
        LPULONG ulID
);


/*
        Use CreateThreadEx if you want a suspendable thread.
        The way this works is, there will be a shared 
        lock between the thread routine, and the HANDLE.
        If you want an infinite loop, do not create an 
        infinite loop inside the thread proc routine.
        Rather, pass in INFINITE to the ullNumberOfIterations
        parameter. The proc will be called an infinite number
        of times. You are sacrificing a small amount of 
        performance by creating a thread this way, as there
        is slight overhead in continously calling the proc.
*/

_Result_Null_On_Failure_
PODNET_API
HANDLE
CreateThreadEx(
        _In_                    LPFN_THREAD_PROC        lpfnThreadProc,
        _In_                    LPVOID                  lpArgument,
        _In_Opt_                LPULONG                 lpulID,
        _In_                    ULONGLONG               ullNumberOfIterations
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HANDLE
GetCurrentThread(
        VOID
);




_Success_(return != FALSE, {})
PODNET_API
BOOL 
KillThread(
        HANDLE              hThread
);




_Success_(return != FALSE, {})
PODNET_API
BOOL 
WaitForSingleThread(
        HANDLE              hThread
);




_Success_(return != FALSE, ...)
PODNET_API
BOOL
SuspendThread(
        _In_                    HANDLE          hThread
);




_Success_(return != FALSE, ...)
PODNET_API
BOOL
ResumeThread(
        _In_                    HANDLE          hThread
);




_Success_(return != FALSE, ...)
PODNET_API 
BOOL 
SetThreadName(
	_In_ 			HANDLE 		hThread,
	_In_Z_ 			LPCSTR 		lpcszName
);




_Success_(return != NULLPTR, ...)
PODNET_API 
LPCSTR
GetThreadName(
	_In_ 			HANDLE 		hThread
);


#endif
