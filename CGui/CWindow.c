/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CWindow.h"


typedef enum __WINDOW_LIB
{
	WINDOW_LIB_NCURSES 	= 0x100,
	WINDOW_LIB_GTK3		= 0x101
} WINDOW_LIB;


typedef LPVOID (* LPFN_WINDOW_CALLBACK)(
	_In_ 		HANDLE 		hWindow,
	_In_ 		LPVOID 		lpDescriptor,
	_In_ 		LPVOID 		lpParam
);



typedef struct __WINDOW
{
	INHERITS_FROM_HANDLE();
	INHERITS_FROM_WINDOW_SYSTEM();
	WINDOW_LIB 		wlLib;
	LPWINDOW_CREATE_PARAM 	lpwcpParams;
} WINDOW_OBJECT, *LPWINDOW_OBJECT;


typedef enum __WINDOW_UPDATE_OPERATION
{
	WINDOW_UPDATE_RESIZE		= 1,
	WINDOW_UPDATE_MAXIMIZE		= 2,
	WINDOW_UPDATE_MINIMIZE		= 3
} WINDOW_UPDATE_OPERATION;



typedef struct __WINDOW_UPDATE_PARAM
{
	HANDLE 				hWindow;
	LPVOID 				lpParam;
	WINDOW_UPDATE_OPERATION 	wuoOperation;
} WINDOW_UPDATE_PARAM, *LPWINDOW_UPDATE_PARAM;



typedef struct __WINDOW_RESIZE_PARAM
{
	ULONG 		ulWidth;
	ULONG 		ulHeight;
	ULONG 		ulPosX;
	ULONG 		ulPosY;
} WINDOW_RESIZE_PARAM, *LPWINDOW_RESIZE_PARAM;



_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HANDLE 
CreateWindow(
	_In_ 		HANDLE RESTRICT		hWindowSystem,
	_In_Opt_ 	UARCHLONG 		ualWidth,
	_In_Opt_ 	UARCHLONG		ualHeight,
	_In_Opt_Z_	LPCSTR RESTRICT		lpcszPath, 	/* For UI files, like GTK */
	_In_Opt_ 	LPVOID RESTRICT		lpParam
){
	EXIT_IF_UNLIKELY_NULL(hWindowSystem, NULLPTR);
	HANDLE hWindow;
	LPWINDOW_CREATE_PARAM lpwcpParams;
	lpwcpParams = (LPWINDOW_CREATE_PARAM)GlobalAllocAndZero(sizeof(WINDOW_CREATE_PARAM));
	/* cppcheck-suppress memleak */
	EXIT_IF_UNLIKELY_NULL(lpwcpParams, NULLPTR);

	lpwcpParams->hWindowSystem = hWindowSystem;
	lpwcpParams->lpParam = lpParam;
	lpwcpParams->lpszPath = (LPSTR)lpcszPath;
	lpwcpParams->ualHeight = ualHeight;
	lpwcpParams->ualWidth = ualWidth;
	
	LockHandle(hWindowSystem);

	if (NULLPTR != GetParentWindow(hWindowSystem))
	{ hWindow = CreateParentWindow(hWindowSystem, lpwcpParams, NULLPTR); }
	else
	{ hWindow = CreateChildWindow(hWindowSystem, lpwcpParams, NULLPTR); }

	UnlockHandle(hWindowSystem);

	return hWindow;
}




_Success_(return != FALSE, _Acquires_Shared_Lock_(_Lock_Kind_Mutex_))
PODNET_API
BOOL
ShowWindow(
	_In_ 		HANDLE RESTRICT		hWindow
){
	EXIT_IF_UNLIKELY_NULL(hWindow, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hWindow), HANDLE_TYPE_WINDOW, FALSE);

	LPWINDOW_OBJECT lpwoWindow; 
	lpwoWindow = (LPWINDOW_OBJECT)GetHandleDerivative(hWindow);
	EXIT_IF_UNLIKELY_NULL(lpwoWindow, FALSE);

	return CallModuleProcByName(
		GetWindowSystemModule(lpwoWindow->hWindowSystem),
		"OnShowWindow",
		(LPVOID)hWindow
	);
}




_Success_(return != FALSE, _Acquires_Shared_Lock_(_Lock_Kind_Mutex_))
PODNET_API
BOOL
HideWindow(
	_In_ 		HANDLE RESTRICT 	hWindow
){
	EXIT_IF_UNLIKELY_NULL(hWindow, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hWindow), HANDLE_TYPE_WINDOW, FALSE);

	LPWINDOW_OBJECT lpwoWindow; 
	lpwoWindow = (LPWINDOW_OBJECT)GetHandleDerivative(hWindow);
	EXIT_IF_UNLIKELY_NULL(lpwoWindow, FALSE);

	return CallModuleProcByName(
		GetWindowSystemModule(lpwoWindow->hWindowSystem),
		"OnHideWindow",
		(LPVOID)hWindow
	);
}




_Success_(return != FALSE, _Acquires_Shared_Lock_(_Lock_Kind_Mutex_))
PODNET_API
BOOL
ResizeWindow(
	_In_ 		HANDLE RESTRICT 	hWindow,
	_In_ 		ULONG 			ulWidth,
	_In_ 		ULONG 			ulHeight
){
	EXIT_IF_UNLIKELY_NULL(hWindow, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hWindow), HANDLE_TYPE_WINDOW, FALSE);

	LPWINDOW_OBJECT lpwoWindow; 
	WINDOW_UPDATE_PARAM wupParam;
	WINDOW_RESIZE_PARAM wrpParam;
	lpwoWindow = (LPWINDOW_OBJECT)GetHandleDerivative(hWindow);
	EXIT_IF_UNLIKELY_NULL(lpwoWindow, FALSE);

	wupParam.hWindow = hWindow;

	if (MAX_ULONG == ulWidth && MAX_ULONG == ulHeight)
	{ 
		wupParam.wuoOperation = WINDOW_UPDATE_MAXIMIZE; 
		wupParam.lpParam = NULLPTR;
	}
	else if (0 == ulWidth && 0 == ulHeight)
	{ 
		wupParam.wuoOperation = WINDOW_UPDATE_MINIMIZE; 
		wupParam.lpParam = NULLPTR;
	}
	else
	{ 
		wupParam.wuoOperation = WINDOW_UPDATE_RESIZE; 
		wupParam.lpParam = (LPVOID)&wrpParam;
		wrpParam.ulHeight = ulHeight;
		wrpParam.ulWidth = ulWidth;
		wrpParam.ulPosX = 0x00U;
		wrpParam.ulPosY = 0x00U;
	}



	return CallModuleProcByName(
		GetWindowSystemModule(lpwoWindow->hWindowSystem),
		"OnUpdateWindow",
		(LPVOID)&wrpParam
	);
}