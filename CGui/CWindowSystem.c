/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CWindowSystem.h"
#include "../CContainers/CContainers.h"


#define INHERITS_FROM_WINDOW_SYSTEM()		HANDLE 		hWindowSystem;



typedef struct __WINDOW_SYSTEM
{
	INHERITS_FROM_HANDLE();
	INHERITS_FROM_MODULE();
	HANDLE 			hMutex;
	HANDLE 			hParentWindow;
	HVECTOR 		hvChildWindows;
	ULONG 			ulOnCreateWindowIndex;
} WINDOW_SYSTEM, *LPWINDOW_SYSTEM;




_Success_(return != FALSE, _Acquires_Shared_Lock_(_Lock_Kind_Mutex_))
INTERNAL_OPERATION
BOOL
__LockWindowSystem(
	_In_ 		HANDLE 		hWindowSystem
){
	EXIT_IF_UNLIKELY_NULL(hWindowSystem, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hWindowSystem), HANDLE_TYPE_WINDOW_SYSTEM, FALSE);
	LPWINDOW_SYSTEM lpwsBackend;
	lpwsBackend = (LPWINDOW_SYSTEM)GetHandleDerivative(hWindowSystem);
	EXIT_IF_UNLIKELY_NULL(lpwsBackend, FALSE);
	return WaitForSingleObject(lpwsBackend->hMutex, INFINITE_WAIT_TIME);
}




_Success_(return != FALSE, _Acquires_Shared_Lock_(_Lock_Kind_Mutex_))
INTERNAL_OPERATION
BOOL
__TryLockWindowSystem(
	_In_ 		HANDLE 		hWindowSystem
){
	EXIT_IF_UNLIKELY_NULL(hWindowSystem, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hWindowSystem), HANDLE_TYPE_WINDOW_SYSTEM, FALSE);
	LPWINDOW_SYSTEM lpwsBackend;
	lpwsBackend = (LPWINDOW_SYSTEM)GetHandleDerivative(hWindowSystem);
	EXIT_IF_UNLIKELY_NULL(lpwsBackend, FALSE);
	return WaitForSingleObject(lpwsBackend->hMutex, 0);
}



_Success_(return != FALSE, _Acquires_Shared_Lock_(_Lock_Kind_Mutex_))
INTERNAL_OPERATION
BOOL
__UnlockWindowSystem(
	_In_ 		HANDLE 		hWindowSystem
){
	EXIT_IF_UNLIKELY_NULL(hWindowSystem, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hWindowSystem), HANDLE_TYPE_WINDOW_SYSTEM, FALSE);
	LPWINDOW_SYSTEM lpwsBackend;
	lpwsBackend = (LPWINDOW_SYSTEM)GetHandleDerivative(hWindowSystem);
	EXIT_IF_UNLIKELY_NULL(lpwsBackend, FALSE);
	return ReleaseSingleObject(lpwsBackend->hMutex);
}



_Success_(return != FALSE, _Acquires_Shared_Lock_(_Lock_Kind_Mutex_))
INTERNAL_OPERATION
BOOL
__DestroyWindowSystem(
	_In_ 		HDERIVATIVE 	hDerivative,
	_In_Opt_ 	ULONG 		ulFlags
){
	EXIT_IF_UNLIKELY_NULL(hDerivative, FALSE);
	UNREFERENCED_PARAMETER(ulFlags);
	LPWINDOW_SYSTEM lpwsBackend;
	UARCHLONG ualIndex;
	lpwsBackend = (LPWINDOW_SYSTEM)hDerivative;
	
	WaitForSingleObject(lpwsBackend->hMutex, INFINITE_WAIT_TIME);

	for (
		ualIndex = 0;
		ualIndex < VectorSize(lpwsBackend->hvChildWindows);
		ualIndex++
	){ DestroyObject(*(LPHANDLE)VectorAt(lpwsBackend->hvChildWindows, ualIndex)); }

	DestroyVector(lpwsBackend->hvChildWindows);
	DestroyObject(lpwsBackend->hParentWindow);
	DestroyObject(lpwsBackend->hModule);
	DestroyObject(lpwsBackend->hMutex);
	FreeMemory(lpwsBackend);

	return TRUE;
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HANDLE
CreateWindowSystem(
	_In_ 		LPCSTR 		lpcszWindowModule,
	_In_ 		ULONG 		ulOnCreateWindowIndex,
	_In_Opt_ 	LPVOID 		lpOnCreateParam,
	_In_Opt_ 	LPVOID 		lpOnDestroyParam,
	_Reserved_	LPVOID 		lpReserved
){
	EXIT_IF_UNLIKELY_NULL(lpcszWindowModule, NULLPTR);
	EXIT_IF_CONDITION(7U >= ulOnCreateWindowIndex, NULLPTR); /* BASE_MODULE_SIZE */
	UNREFERENCED_PARAMETER(lpReserved);

	HANDLE hWindowSystem;
	LPWINDOW_SYSTEM lpwsBackend;
	LPFN_MODULE_DISPATCH_PROC lpfnOnError, lpfnOnAlarm;
	BOOL bRet;
	lpwsBackend = GlobalAllocAndZero(sizeof(WINDOW_SYSTEM));
	/* cppcheck-suppress memleak */
	EXIT_IF_UNLIKELY_NULL(lpwsBackend, NULLPTR);

	lpwsBackend->hMutex = CreateMutex();
	
	lpwsBackend->hModule = LoadModule(
		lpcszWindowModule,
		lpOnCreateParam,
		lpOnDestroyParam,
		LOCK_TYPE_MUTEX,
		NULL,
		lpwsBackend
	);

	lpfnOnError = GetModuleProcByName(lpwsBackend->hModule, "OnError");
	lpfnOnAlarm = GetModuleProcByName(lpwsBackend->hModule, "OnAlarm");

	lpwsBackend->ulOnCreateWindowIndex = ulOnCreateWindowIndex;
	lpwsBackend->hvChildWindows = CreateVector(0x08, sizeof(HANDLE), NULL);

	
	bRet = RegisterModuleThunk(lpwsBackend->hModule, ulOnCreateWindowIndex + 0U, "OnCreateWindow", NULLPTR);		
	bRet &= RegisterModuleThunk(lpwsBackend->hModule, ulOnCreateWindowIndex + 1U, "OnDestroyWindow", NULLPTR);
	bRet &= RegisterModuleThunk(lpwsBackend->hModule, ulOnCreateWindowIndex + 2U, "OnShowWindow", NULLPTR); 			
	bRet &= RegisterModuleThunk(lpwsBackend->hModule, ulOnCreateWindowIndex + 3U, "OnHideWindow", NULLPTR); 			
	bRet &= RegisterModuleThunk(lpwsBackend->hModule, ulOnCreateWindowIndex + 4U, "OnUpdateWindow", NULLPTR);		
	bRet &= RegisterModuleThunk(lpwsBackend->hModule, ulOnCreateWindowIndex + 5U, "OnEditWindow", NULLPTR);			
	bRet &= RegisterModuleThunk(lpwsBackend->hModule, ulOnCreateWindowIndex + 6U, "OnWindowEvent", NULLPTR);	
	bRet &= RegisterModuleThunk(lpwsBackend->hModule, ulOnCreateWindowIndex + 7U, "GetWindowInformation", NULLPTR);	

	if (FALSE == bRet)
	{
		DestroyVector(lpwsBackend->hvChildWindows);
		DestroyObject(lpwsBackend->hMutex);
		DestroyObject(lpwsBackend->hModule);
		FreeMemory(lpwsBackend);
	}

	/*
		TODO: Change callback functions for
		OnError and OnAlarm to internal 
		functions that pass on the HMODULE.
	*/
	hWindowSystem = CreateHandleWithSingleInheritor(
		lpwsBackend,
		&(lpwsBackend->hThis),
		HANDLE_TYPE_WINDOW_SYSTEM,
		__DestroyWindowSystem,
		NULL,
		(LPFN_HANDLE_ERROR)lpfnOnError,
		NULL,
		(LPFN_HANDLE_ALARM)lpfnOnAlarm,
		NULL,
		&(lpwsBackend->ullId),
		NULL
	);

	EXIT_IF_UNLIKELY_NULL(hWindowSystem, NULLPTR);

	SetHandleLockFunctions(
		hWindowSystem, 
		__LockWindowSystem, 
		__UnlockWindowSystem, 
		__TryLockWindowSystem
	);

	return hWindowSystem;
}



_Success_(return != FALSE, _Acquires_Shared_Lock_(_Lock_Kind_Mutex_))
PODNET_API
BOOL
SetParentWindow(
	_In_ 		HANDLE 		hWindowSystem,
	_In_ 		HANDLE 		hWindow
){
	EXIT_IF_UNLIKELY_NULL(hWindowSystem, FALSE);
	EXIT_IF_UNLIKELY_NULL(hWindow, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hWindowSystem), HANDLE_TYPE_WINDOW_SYSTEM, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hWindow), HANDLE_TYPE_WINDOW, FALSE);

	LPHANDLE lphTemp;
	LPWINDOW_SYSTEM lpwsBackend;
	UARCHLONG ualIndex;
	BOOL bSet;
	lpwsBackend = (LPWINDOW_SYSTEM)GetHandleDerivative(hWindowSystem);
	EXIT_IF_UNLIKELY_NULL(lpwsBackend, FALSE);
	bSet = FALSE;

	/* No need to lock, we call LockHandle() before calling this function */

	for (
		ualIndex = 0;
		ualIndex < VectorSize(lpwsBackend->hvChildWindows);
		ualIndex++
	){
		lphTemp = VectorAt(lpwsBackend->hvChildWindows, ualIndex);
		if (NULLPTR == lphTemp)
		{ continue; }
		if (*lphTemp == hWindow)
		{ 
			*lphTemp = lpwsBackend->hParentWindow;
			bSet = TRUE;
			JUMP(Found);
		}		
	}
	
	Found:
	
	/* If found, exchange, if not, push back and change. */
	if (TRUE == bSet)
	{ lpwsBackend->hParentWindow = hWindow; }
	else
	{
		VectorPushBack(lpwsBackend->hvChildWindows, &(lpwsBackend->hParentWindow), NULL);
		lpwsBackend->hParentWindow = hWindow;
	}	


	return TRUE;
}




_Success_(return != FALSE, _Acquires_Shared_Lock_(_Lock_Kind_Mutex_))
PODNET_API
HANDLE
CreateParentWindow(
	_In_ 		HANDLE 		hWindowSystem,
	_In_ 		LPVOID 		lpCreateParam,
	_In_Opt_ 	LPVOID 		lpDestroyParam
){
	UNREFERENCED_PARAMETER(lpDestroyParam);

	EXIT_IF_UNLIKELY_NULL(hWindowSystem, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hWindowSystem), HANDLE_TYPE_WINDOW_SYSTEM, NULLPTR);

	HANDLE hWindow;
	LPWINDOW_SYSTEM lpwsBackend;
	lpwsBackend = GetHandleDerivative(hWindowSystem);
	EXIT_IF_UNLIKELY_NULL(lpwsBackend, NULLPTR);

	/* No need to lock, we call LockHandle() before calling this function */

	hWindow = (HANDLE)CallModuleProc(
		lpwsBackend->hModule,
		lpwsBackend->ulOnCreateWindowIndex,
		lpCreateParam
	);

	if (NULLPTR != lpwsBackend->hParentWindow)
	{ SetParentWindow(hWindowSystem, hWindow); }
	else
	{ lpwsBackend->hParentWindow = hWindow; }

	return hWindow;	
}




_Success_(return != FALSE, _Acquires_Shared_Lock_(_Lock_Kind_Mutex_))
PODNET_API
HANDLE
CreateChildWindow(
	_In_ 		HANDLE 		hWindowSystem,
	_In_ 		LPVOID 		lpCreateParam,
	_In_Opt_ 	LPVOID 		lpDestroyParam
){
	UNREFERENCED_PARAMETER(lpDestroyParam);

	EXIT_IF_UNLIKELY_NULL(hWindowSystem, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hWindowSystem), HANDLE_TYPE_WINDOW_SYSTEM, NULLPTR);

	HANDLE hWindow;
	LPWINDOW_SYSTEM lpwsBackend;
	lpwsBackend = GetHandleDerivative(hWindowSystem);
	EXIT_IF_UNLIKELY_NULL(lpwsBackend, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpwsBackend->hParentWindow, NULLPTR);

	/* No need to lock, we call LockHandle() before calling this function */

	hWindow = (HANDLE)CallModuleProc(
		lpwsBackend->hModule,
		lpwsBackend->ulOnCreateWindowIndex,
		lpCreateParam
	);

	VectorPushBack(lpwsBackend->hvChildWindows, &hWindow, NULL);

	return hWindow;
}



_Success_(return != FALSE, _Non_Locking_)
PODNET_API
HANDLE
GetWindowSystemModule(
	_In_ 		HANDLE 		hWindowSystem
){
	EXIT_IF_UNLIKELY_NULL(hWindowSystem, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hWindowSystem), HANDLE_TYPE_WINDOW_SYSTEM, NULLPTR);

	LPWINDOW_SYSTEM lpwsBackend;
	lpwsBackend = GetHandleDerivative(hWindowSystem);
	EXIT_IF_UNLIKELY_NULL(lpwsBackend, NULLPTR);
	return lpwsBackend->hModule;
}




_Success_(return != FALSE, _Interlocked_Operation_)
PODNET_API
BOOL
WindowSystemHasParentWindow(
	_In_		HANDLE 		hWindowSystem
){
	EXIT_IF_UNLIKELY_NULL(hWindowSystem, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hWindowSystem), HANDLE_TYPE_WINDOW_SYSTEM, FALSE);

	LPWINDOW_SYSTEM lpwsBackend;
	HANDLE hWindow;
	lpwsBackend = GetHandleDerivative(hWindowSystem);
	EXIT_IF_UNLIKELY_NULL(lpwsBackend, FALSE);

	hWindow = (HANDLE)InterlockedAcquire(&(lpwsBackend->hParentWindow));
	return ((NULLPTR != hWindow) ? TRUE : FALSE);
}




_Success_(return != FALSE, _Interlocked_Operation_)
PODNET_API
HANDLE
GetParentWindow(
	_In_ 		HANDLE 		hWindowSystem
){
	EXIT_IF_UNLIKELY_NULL(hWindowSystem, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hWindowSystem), HANDLE_TYPE_WINDOW_SYSTEM, NULLPTR);

	LPWINDOW_SYSTEM lpwsBackend;
	HANDLE hWindow;
	lpwsBackend = GetHandleDerivative(hWindowSystem);
	EXIT_IF_UNLIKELY_NULL(lpwsBackend, NULLPTR);

	hWindow = (HANDLE)InterlockedAcquire(&(lpwsBackend->hParentWindow));
	return hWindow;
}
