/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CWINDOWSYSTEM_H
#define CWINDOWSYSTEM_H


#include "../CModule/CModule.h"


#define INHERITS_FROM_WINDOW_SYSTEM()		HANDLE 		hWindowSystem;



typedef struct __WINDOW_CREATE_PARAM
{
	HANDLE 		hWindowSystem;
	HANDLE 		hParentWindow;
	UARCHLONG 	ualWidth;
	UARCHLONG 	ualHeight;
	LPSTR 		lpszPath;	/* For UI files, like in GTK */
	LPVOID 		lpParam;
} WINDOW_CREATE_PARAM, *LPWINDOW_CREATE_PARAM;




typedef struct __WINDOW_PARAM 
{
	HANDLE 		hWindow;
	LPVOID 		lpParam;
} WINDOW_PARAM, *LPWINDOW_PARAM;




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HANDLE
CreateWindowSystem(
	_In_ 		LPCSTR 		lpcszWindowModule,
	_In_ 		ULONG 		ulOnCreateWindowIndex,
	_In_Opt_ 	LPVOID 		lpOnCreateParam,
	_In_Opt_ 	LPVOID 		lpOnDestroyParam,
	_Reserved_	LPVOID 		lpReserved
);



_Success_(return != FALSE, _Acquires_Shared_Lock_(_Lock_Kind_Mutex_))
PODNET_API
BOOL
SetParentWindow(
	_In_ 		HANDLE 		hWindowSystem,
	_In_ 		HANDLE 		hWindow
);




_Success_(return != FALSE, _Acquires_Shared_Lock_(_Lock_Kind_Mutex_))
PODNET_API
HANDLE
CreateParentWindow(
	_In_ 		HANDLE 		hWindowSystem,
	_In_ 		LPVOID 		lpCreateParam,
	_In_Opt_ 	LPVOID 		lpDestroyParam
);




_Success_(return != FALSE, _Acquires_Shared_Lock_(_Lock_Kind_Mutex_))
PODNET_API
HANDLE
CreateChildWindow(
	_In_ 		HANDLE 		hWindowSystem,
	_In_ 		LPVOID 		lpCreateParam,
	_In_Opt_ 	LPVOID 		lpDestroyParam
);



_Success_(return != FALSE, _Non_Locking_)
PODNET_API
HANDLE
GetWindowSystemModule(
	_In_ 		HANDLE 		hWindowSystem
);




_Success_(return != FALSE, _Interlocked_Operation_)
PODNET_API
BOOL
WindowSystemHasParentWindow(
	_In_		HANDLE 		hWindowSystem
);




_Success_(return != FALSE, _Interlocked_Operation_)
PODNET_API
HANDLE
GetParentWindow(
	_In_ 		HANDLE 		hWindowSystem
);


#endif