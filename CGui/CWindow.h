/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CWINDOW_H
#define CWINDOW_H


#include "CWindowSystem.h"






_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HANDLE 
CreateWindow(
	_In_ 		HANDLE RESTRICT		hWindowSystem,
	_In_Opt_ 	ULONG 			ulWidth,
	_In_Opt_ 	ULONG 			ulHeight,
	_In_Opt_Z_	LPCSTR RESTRICT		lpcszPath, 	/* For UI files, like GTK */
	_In_Opt_ 	LPVOID RESTRICT		lpParam
);




_Success_(return != FALSE, _Acquires_Shared_Lock_(_Lock_Kind_Mutex_))
PODNET_API
BOOL
ShowWindow(
	_In_ 		HANDLE RESTRICT		hWindow
);




_Success_(return != FALSE, _Acquires_Shared_Lock_(_Lock_Kind_Mutex_))
PODNET_API
BOOL
HideWindow(
	_In_ 		HANDLE RESTRICT 	hWindow
);




_Success_(return != FALSE, _Acquires_Shared_Lock_(_Lock_Kind_Mutex_))
PODNET_API
BOOL
ResizeWindow(
	_In_ 		HANDLE RESTRICT 	hWindow,
	_In_ 		ULONG 			ulWidth,
	_In_ 		ULONG 			ulHeight
);


#define MinimizeWindow(HWINDOW)			ResizeWindow(HWINDOW, 0x00U, 0x00U)

#define MaximizeWindow(HWINDOW)			ResizeWindow(HWINDOW, MAX_ULONG, MAX_ULONG)







#endif