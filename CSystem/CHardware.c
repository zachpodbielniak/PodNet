/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CHardware.h"
#include "../CFile/CFile.h"




_Success_(return != NULL, _Non_Locking_)
PODNET_API
UARCHLONG
GetNumberOfCpuCores(
        VOID
){ return sysconf(_SC_NPROCESSORS_ONLN); }




_Success_(return != NULL, _Non_Locking_)
PODNET_API
UARCHLONG
GetSystemVirtualMemorySize(
        VOID
){
        HANDLE hFile;
        UARCHLONG ualTotal;
        CSTRING csBuffer[0x80];
        hFile = OpenFile("/proc/self/statm", FILE_PERMISSION_READ, NULL);
        EXIT_IF_UNLIKELY_NULL(hFile, NULL);

        ZeroMemory(&csBuffer, 0x80);
        ReadFile(hFile, (DLPSTR)&csBuffer, 0x7E, NULL);
        #ifdef __LP64__
        StringScanFormat(csBuffer, "%ld %*[^\n]", &ualTotal);
        #else
        StringScanFormat(csBuffer, "%d %*[^\n]", &ualTotal);
        #endif


        DestroyObject(hFile);

        return ualTotal;
}
