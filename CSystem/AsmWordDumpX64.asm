; 
; 
; 
;  ____           _ _   _      _   
; |  _ \ ___   __| | \ | | ___| |_ 
; | |_) / _ \ / _` |  \| |/ _ \ __|
; |  __/ (_) | (_| | |\  |  __/ |_ 
; |_|   \___/ \__,_|_| \_|\___|\__|
; 
; 
; General Purpose C Library.
; Copyright (C) 2017-2019 Zach Podbielniak
; 
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU Affero General Public License as
; published by the Free Software Foundation, either version 3 of the
; License, or (at your option) any later version.
; 
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU Affero General Public License for more details.
; 
; You should have received a copy of the GNU Affero General Public License
; along with this program.  If not, see <https://www.gnu.org/licenses/>.
; 
;
; Parameters are passed in RDI, RSI, RDX, RCX, R8, R9, Stack : in that order.


[DEFAULT REL]
[BITS 64]


global __WordDumpX64
global __WordDumpToFileX64

extern printf
extern sprintf
extern WriteFile
extern WaitForSingleObject
extern ReleaseSingleObject
extern CreateSpinLock

segment .data

lpszWordDump:	db	"Word Dump of size 0x%016X bytes",0x0A,"at address %p",0x0A,0x0A,0x00
lpszFormat:	db	"0x%04hX",0x09,"0x%04hX",0x09,"0x%04hX",0x09,"0x%04hX",0x0A,0x00

lpszBuffer:	times 128 db 0
hSpinLock:	dq	0x0000000000000000



segment .text


; _Success_(return != FALSE, ...)
; __WordDumpX64(
;	_In_		LPVOID		lpAddress	RDI,
;	_In_		QWORD		qwSize		RSI
;);

__WordDumpX64:
	cmp rdi, 0
	je __WordDumpX64Error
	cmp rsi, 0
	je __WordDumpX64Error

	push r15
	push r14
	push r13
	push r12
	push r11
	push rbx

	mov r15, rsi
	mov r13, rsi
	mov r11, rsi
	shr r15, 4
	mov r14, r15
	shl r14, 4
	sub r13, r14
	shr r13, 1
	mov r12, rdi

	; qwSize -> r11
	; lpAddress -> r12
	; word loops -> r13
	; xmmword loops -> r15

	xor rdi, rdi
	xor rsi, rsi
	xor rdx, rdx
	xor rcx, rcx

	__WordDumpX64PrintFirstLine:
	lea rdi, [lpszWordDump]
	mov rsi, r11
	mov rdx, r12
	xor rax, rax
	sub rsp, 0x20
	call printf wrt ..plt
	add rsp, 0x20
	xor rdi, rdi
	xor rsi, rsi

	cmp r15, 0
	je __WordDumpX64DoneWithMain


	__WordDumpX64MainLoop:
	movups xmm0, [r12]
	lea rdi, [lpszFormat]
	movq r14, xmm0
	mov si, r14w
	shr r14, 0x10
	mov dx, r14w
	shr r14, 0x10
	mov cx, r14w
	shr r14, 0x10
	mov r8w, r14w
	xor rax, rax
	sub rsp, 0x30
	call printf wrt ..plt
	add rsp, 0x30
	lea rdi, [lpszFormat]	
	shufps xmm0, xmm0, 0b01001110
	movq r14, xmm0
	mov si, r14w
	shr r14, 0x10
	mov dx, r14w
	shr r14, 0x10
	mov cx, 0x10
	shr r14, 0x10
	mov r8w, r14w
	xor rax, rax
	sub rsp, 0x30
	call printf wrt ..plt
	add rsp, 0x30
	add r12, 16
	dec r15
	cmp r15, 0
	jne __WordDumpX64MainLoop


	__WordDumpX64DoneWithMain:
	cmp r13, 0
	je __WordDumpX64Success
	cmp r13, 2
	je __WordDumpX64TwoWords 
	cmp r13, 3
	je __WordDumpX64ThreeWords 
	cmp r13, 4
	je __WordDumpX64FourWords 
	cmp r13, 5
	je __WordDumpX64FiveWords 
	cmp r13, 6
	je __WordDumpX64SixWords 
	cmp r13, 7
	je __WordDumpX64SevenWords 



	__WordDumpX64OneWord:
	mov r15, QWORD [r12]
	lea rdi, [lpszFormat]
	mov si, r15w
	xor rdx, rdx
	xor rcx, rcx
	xor r8, r8	
	xor rax, rax
	sub rsp, 0x30
	call printf wrt ..plt
	add rsp, 0x30
	jmp __WordDumpX64Success


	__WordDumpX64TwoWords:
	mov r15, QWORD [r12]
	lea rdi, [lpszFormat]
	mov si, r15w
	shr r15, 0x10
	mov dx, r15w
	xor rcx, rcx
	xor r8, r8	
	xor rax, rax
	sub rsp, 0x30
	call printf wrt ..plt
	add rsp, 0x30
	jmp __WordDumpX64Success
	

	__WordDumpX64ThreeWords:
	mov r15, QWORD [r12]
	lea rdi, [lpszFormat]
	mov si, r15w
	shr r15, 0x10
	mov dx, r15w
	shr r15, 0x10
	mov cx, r15w
	xor r8, r8	
	xor rax, rax
	sub rsp, 0x30
	call printf wrt ..plt
	add rsp, 0x30
	jmp __WordDumpX64Success
	

	__WordDumpX64FourWords:
	mov r15, QWORD [r12]
	lea rdi, [lpszFormat]
	mov si, r15w
	shr r15, 0x10
	mov dx, r15w
	shr r15, 0x10
	mov cx, r15w
	shr r15, 0x10
	mov r8w, r8w
	xor rax, rax
	sub rsp, 0x30
	call printf wrt ..plt
	add rsp, 0x30
	jmp __WordDumpX64Success


	__WordDumpX64FiveWords:
	mov r15, QWORD [r12]
	lea rdi, [lpszFormat]
	mov si, r15w
	shr r15, 0x10
	mov dx, r15w
	shr r15, 0x10
	mov cx, r15w
	shr r15, 0x10
	mov r8w, r8w
	xor rax, rax
	sub rsp, 0x30
	call printf wrt ..plt
	add rsp, 0x30
	mov r15, QWORD [r12+8]
	lea rdi, [lpszFormat]
	mov si, r15w
	xor rdx, rdx
	xor rcx, rcx	
	xor r8, r8
	xor rax, rax
	sub rsp, 0x30
	call printf wrt ..plt
	add rsp, 0x30
	jmp __WordDumpX64Success
	

	__WordDumpX64SixWords:
	mov r15, QWORD [r12]
	lea rdi, [lpszFormat]
	mov si, r15w
	shr r15, 0x10
	mov dx, r15w
	shr r15, 0x10
	mov cx, r15w
	shr r15, 0x10
	mov r8w, r8w
	xor rax, rax
	sub rsp, 0x30
	call printf wrt ..plt
	add rsp, 0x30
	mov r15, QWORD [r12+8]
	lea rdi, [lpszFormat]
	mov si, r15w
	shr r15, 0x10
	mov dx, r15w
	xor rcx, rcx	
	xor r8, r8
	xor rax, rax
	sub rsp, 0x30
	call printf wrt ..plt
	add rsp, 0x30
	jmp __WordDumpX64Success
	

	__WordDumpX64SevenWords:
	mov r15, QWORD [r12]
	lea rdi, [lpszFormat]
	mov si, r15w
	shr r15, 0x10
	mov dx, r15w
	shr r15, 0x10
	mov cx, r15w
	shr r15, 0x10
	mov r8w, r8w
	xor rax, rax
	sub rsp, 0x30
	call printf wrt ..plt
	add rsp, 0x30
	mov r15, QWORD [r12+8]
	lea rdi, [lpszFormat]
	mov si, r15w
	shr r15, 0x10
	mov dx, r15w
	shr r15, 0x10
	mov cx, r15w
	xor r8, r8
	xor rax, rax
	sub rsp, 0x30
	call printf wrt ..plt
	add rsp, 0x30
	jmp __WordDumpX64Success



	__WordDumpX64Success:
	pop rbx
	pop r11
	pop r12
	pop r13
	pop r14
	pop r15
	mov rax, 1
	ret	

	__WordDumpX64Error:
	pop rbx
	pop r11
	pop r12
	pop r13
	pop r14
	pop r15
	mov rax, 0
	ret	





; _Success_(return != FALSE, ...)
; PODNET_API
; BOOL
; __WordDumpToFileX64(
;	_In_		LPVOID		lpAddress 	RDI,
;	_In_		QWORD		qwSize		RSI,
;	_In_		HANDLE		hFile		RDX
;);


__WordDumpToFileX64:
	cmp rdi, 0
	je __WordDumpToFileX64Error
	cmp rsi, 0
	je __WordDumpToFileX64Error
	cmp rdx, 0
	je __WordDumpToFileX64Error

	push r15
	push r14
	push r13
	push r12
	push r11
	push rbx
	
	mov r15, rsi
	mov r13, rsi
	mov r11, rsi
	shr r15, 4
	mov r14, r15
	shl r14, 4
	sub r13, r14
	shr r13, 1
	mov r12, rdi
	mov rbx, rdx

	; hFile -> rbx
	; qwSize -> r11
	; lpAddress -> r12
	; qword loop -> r13
	; xmmword loops -> r15
	
	xor rdi, rdi
	xor rsi, rsi
	xor rcx, rcx
	xor rdx, rdx

	mov rdi, QWORD [hSpinLock]
	cmp rdi, 0
	jne __WordDumpToFileX64AcquireLock

	__WordDumpToFileX64CreateSpinLock:
	call CreateSpinLock wrt ..plt
	cmp rax, 0
	je __WordDumpToFileX64Error
	mov rdi, rax
	mov QWORD [hSpinLock], rax

	__WordDumpToFileX64AcquireLock:
	mov rsi, 0xFFFFFFFFFFFFFFFF
	sub rsp, 0x10
	call WaitForSingleObject wrt ..plt
	add rsp, 0x10


	__WordDumpToFileX64PrintFirstLine:
	lea rdi, [lpszBuffer]
	lea rsi, [lpszWordDump]
	mov rdx, r11
	mov rcx, r12
	xor rax, rax
	sub rsp, 0x20
	call sprintf wrt ..plt
	add rsp, 0x20
	mov rdi, rbx
	lea rsi, [lpszBuffer]
	xor rdx, rdx
	sub rsp, 0x20
	call WriteFile wrt ..plt
	add rsp, 0x20

	cmp r15, 0
	je __WordDumpToFileX64DoneWithMain

	__WordDumpToFileX64MainLoop:
	movups xmm0, [r12]
	lea rdi, [lpszBuffer]
	lea rsi, [lpszFormat]
	movq r9, xmm0
	mov dx, r9w
	shr r9, 0x10
	mov cx, r9w
	shr r9, 0x10
	mov r8w, r9w
	shr r9, 0x10
	xor rax, rax
	sub rsp, 0x30
	call sprintf wrt ..plt
	add rsp, 0x30
	mov rdi, rbx
	lea rsi, [lpszBuffer]
	xor rdx, rdx
	sub rsp, 0x20
	call WriteFile wrt ..plt
	add rsp, 0x20
	shufps xmm0, xmm0, 0b01001110	
	lea rdi, [lpszBuffer]
	lea rsi, [lpszFormat]
	movq r9, xmm0
	mov dx, r9w
	shr r9, 0x10
	mov cx, r9w
	shr r9, 0x10
	mov r8w, r9w
	shr r9, 0x10
	xor rax, rax
	sub rsp, 0x30
	call sprintf wrt ..plt
	add rsp, 0x30
	mov rdi, rbx
	lea rsi, [lpszBuffer]
	xor rdx, rdx
	sub rsp, 0x20
	call WriteFile wrt ..plt
	add rsp, 0x20
	add r12, 16
	dec r15
	cmp r15, 0
	jne __WordDumpToFileX64MainLoop
	

	__WordDumpToFileX64DoneWithMain:
	cmp r13, 0
	je __WordDumpToFileX64Success


	__WordDumpX64HandleLastWords:
	cmp r13, 0
	je __WordDumpToFileX64Success
	cmp r13, 2
	je __WordDumpToFileX64TwoWords 
	cmp r13, 3
	je __WordDumpToFileX64ThreeWords 
	cmp r13, 4
	je __WordDumpToFileX64FourWords 
	cmp r13, 5
	je __WordDumpToFileX64FiveWords 
	cmp r13, 6
	je __WordDumpToFileX64SixWords 
	cmp r13, 7
	je __WordDumpToFileX64SevenWords 



	__WordDumpToFileX64OneWord:
	mov r9, QWORD [r12]
	lea rdi, [lpszBuffer]
	lea rsi, [lpszFormat]
	mov dx, r9w
	xor rcx, rcx
	xor r8, r8
	xor r9, r9	
	xor rax, rax
	sub rsp, 0x30
	call sprintf wrt ..plt
	add rsp, 0x30
	mov rdi, rbx
	lea rsi, [lpszBuffer]
	xor rdx, rdx
	sub rsp, 0x20
	call WriteFile wrt ..plt
	add rsp, 0x20
	jmp __WordDumpX64Success


	__WordDumpToFileX64TwoWords:
	mov r9, QWORD [r12]
	lea rdi, [lpszBuffer]
	lea rsi, [lpszFormat]
	mov dx, r9w
	shr r9, 0x10
	mov cx, r9w
	xor r8, r8
	xor r9, r9	
	xor rax, rax
	sub rsp, 0x30
	call sprintf wrt ..plt
	add rsp, 0x30
	mov rdi, rbx
	lea rsi, [lpszBuffer]
	xor rdx, rdx
	sub rsp, 0x20
	call WriteFile wrt ..plt
	add rsp, 0x20
	jmp __WordDumpX64Success
	

	__WordDumpToFileX64ThreeWords:
	mov r9, QWORD [r12]
	lea rdi, [lpszBuffer]
	lea rsi, [lpszFormat]
	mov dx, r9w
	shr r9, 0x10
	mov cx, r9w
	shr r9, 0x10
	mov r8w, r9w
	xor r9, r9	
	xor rax, rax
	sub rsp, 0x30
	call sprintf wrt ..plt
	add rsp, 0x30
	mov rdi, rbx
	lea rsi, [lpszBuffer]
	xor rdx, rdx
	sub rsp, 0x20
	call WriteFile wrt ..plt
	add rsp, 0x20
	jmp __WordDumpX64Success
	

	__WordDumpToFileX64FourWords:
	mov r9, QWORD [r12]
	lea rdi, [lpszBuffer]
	lea rsi, [lpszFormat]
	mov dx, r9w
	shr r9, 0x10
	mov cx, r9w
	shr r9, 0x10
	mov r8w, r9w
	shr r9, 0x10
	xor rax, rax
	sub rsp, 0x30
	call sprintf wrt ..plt
	add rsp, 0x30
	mov rdi, rbx
	lea rsi, [lpszBuffer]
	xor rdx, rdx
	sub rsp, 0x20
	call WriteFile wrt ..plt
	add rsp, 0x20
	jmp __WordDumpX64Success


	__WordDumpToFileX64FiveWords:
	mov r9, QWORD [r12]
	lea rdi, [lpszBuffer]
	lea rsi, [lpszFormat]
	mov dx, r9w
	shr r9, 0x10
	mov cx, r9w
	shr r9, 0x10
	mov r8w, r9w
	shr r9, 0x10
	xor rax, rax
	sub rsp, 0x30
	call sprintf wrt ..plt
	add rsp, 0x30
	mov rdi, rbx
	lea rsi, [lpszBuffer]
	xor rdx, rdx
	sub rsp, 0x20
	call WriteFile wrt ..plt
	add rsp, 0x20
	mov dx, WORD [r12+8]
	lea rdi, [lpszBuffer]
	lea rsi, [lpszFormat]
	xor cx, cx
	xor r8, r8
	xor r9, r9
	xor rax, rax
	sub rsp, 0x30
	call sprintf wrt ..plt
	add rsp, 0x30
	mov rdi, rbx
	lea rsi, [lpszBuffer]
	xor rdx, rdx
	sub rsp, 0x20
	call WriteFile wrt ..plt
	add rsp, 0x20
	jmp __WordDumpX64Success
	

	__WordDumpToFileX64SixWords:
	mov r9, QWORD [r12]
	lea rdi, [lpszBuffer]
	lea rsi, [lpszFormat]
	mov dx, r9w
	shr r9, 0x10
	mov cx, r9w
	shr r9, 0x10
	mov r8w, r9w
	shr r9, 0x10
	xor rax, rax
	sub rsp, 0x30
	call sprintf wrt ..plt
	add rsp, 0x30
	mov rdi, rbx
	lea rsi, [lpszBuffer]
	xor rdx, rdx
	sub rsp, 0x20
	call WriteFile wrt ..plt
	add rsp, 0x20
	mov r9d, DWORD [r12+8]
	lea rdi, [lpszBuffer]
	lea rsi, [lpszFormat]
	mov dx, r9w
	shr r9, 0x10
	mov cx, r9w
	xor r8, r8
	xor r9, r9
	xor rax, rax
	sub rsp, 0x30
	call sprintf wrt ..plt
	add rsp, 0x30
	mov rdi, rbx
	lea rsi, [lpszBuffer]
	xor rdx, rdx
	sub rsp, 0x20
	call WriteFile wrt ..plt
	add rsp, 0x20
	jmp __WordDumpX64Success
	

	__WordDumpToFileX64SevenWords:
	mov r9, QWORD [r12]
	lea rdi, [lpszBuffer]
	lea rsi, [lpszFormat]
	mov dx, r9w
	shr r9, 0x10
	mov cx, r9w
	shr r9, 0x10
	mov r8w, r9w
	shr r9, 0x10
	xor rax, rax
	sub rsp, 0x30
	call sprintf wrt ..plt
	add rsp, 0x30
	mov rdi, rbx
	lea rsi, [lpszBuffer]
	xor rdx, rdx
	sub rsp, 0x20
	call WriteFile wrt ..plt
	add rsp, 0x20
	mov r9d, DWORD [r12+8]
	lea rdi, [lpszBuffer]
	lea rsi, [lpszFormat]
	mov dx, r9w
	shr r9, 0x10
	mov cx, r9w
	mov r8w, WORD [r12+12]
	xor r9, r9
	xor rax, rax
	sub rsp, 0x30
	call sprintf wrt ..plt
	add rsp, 0x30
	mov rdi, rbx
	lea rsi, [lpszBuffer]
	xor rdx, rdx
	sub rsp, 0x20
	call WriteFile wrt ..plt
	add rsp, 0x20
	jmp __WordDumpX64Success



	__WordDumpToFileX64Success:
	pop rbx
	pop r11
	pop r12
	pop r13
	pop r14
	pop r15
	mov rax, 1
	ret

	__WordDumpToFileX64Error:
	pop rbx
	pop r11
	pop r12
	pop r13
	pop r14
	pop r15
	mov rax, 0
	ret

