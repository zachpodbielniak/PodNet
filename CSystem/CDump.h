/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/



#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"

#ifndef CDWORDDUMP_H
#define CDWORDDUMP_H

#ifdef __LP64__
#ifdef __x86_64__


_Success_(return != FALSE, ...)
PODNET_API
BOOL
__WordDumpX64(
        _In_            LPVOID          lpAddress,
        _In_            QWORD           qwSize
) ASM_CALL("__WordDumpX64");




_Success_(return != FALSE, ...)
PODNET_API
BOOL
__WordDumpToFileX64(
        _In_            LPVOID          lpAddress,
        _In_            QWORD           qwSize,
	_In_		HANDLE		hFile
) ASM_CALL("__WordDumpToFileX64");




_Success_(return != FALSE, ...)
PODNET_API
BOOL
__DwordDumpX64(
        _In_            LPVOID          lpAddress,
        _In_            QWORD           qwSize
) ASM_CALL("__DwordDumpX64");




_Success_(return != FALSE, ...)
PODNET_API
BOOL
__DwordDumpToFileX64(
        _In_            LPVOID          lpAddress,
        _In_            QWORD           qwSize,
	_In_		HANDLE		hFile
) ASM_CALL("__DwordDumpToFileX64");




_Success_(return != FALSE, ...)
PODNET_API
BOOL
__QwordDumpX64(
        _In_            LPVOID          lpAddress,
        _In_            QWORD           qwSize
) ASM_CALL("__QwordDumpX64");




_Success_(return != FALSE, ...)
PODNET_API
BOOL
__QwordDumpToFileX64(
        _In_            LPVOID          lpAddress,
        _In_            QWORD           qwSize,
	_In_		HANDLE		hFile
) ASM_CALL("__QwordDumpToFileX64");




_Success_(return != FALSE, ...)
INLINE
BOOL
WordDump(
        _In_            LPVOID          lpAddress,
        _In_            ULONGLONG       ullSize
) { return __WordDumpX64(lpAddress, (QWORD)ullSize); }




_Success_(return != FALSE, ...)
INLINE
BOOL
WordDumpToFile(
	_In_		HANDLE		hFile,
        _In_            LPVOID          lpAddress,
        _In_            ULONGLONG       ullSize
) { return __WordDumpToFileX64(lpAddress, (QWORD)ullSize, hFile); }




_Success_(return != FALSE, ...)
INLINE
BOOL
DwordDump(
        _In_            LPVOID          lpAddress,
        _In_            ULONGLONG       ullSize
) { return __DwordDumpX64(lpAddress, (QWORD)ullSize); }




_Success_(return != FALSE, ...)
INLINE
BOOL
DwordDumpToFile(
	_In_		HANDLE		hFile,
        _In_            LPVOID          lpAddress,
        _In_            ULONGLONG       ullSize
) { return __DwordDumpToFileX64(lpAddress, (QWORD)ullSize, hFile); }




_Success_(return != FALSE, ...)
INLINE
BOOL
QwordDump(
        _In_            LPVOID          lpAddress,
        _In_            ULONGLONG       ullSize
) { return __QwordDumpX64(lpAddress, (QWORD)ullSize); }




_Success_(return != FALSE, ...)
INLINE
BOOL
QwordDumpToFile(
	_In_		HANDLE		hFile,
        _In_            LPVOID          lpAddress,
        _In_            ULONGLONG       ullSize
) { return __QwordDumpToFileX64(lpAddress, (QWORD)ullSize, hFile); }



#endif /* __x86_64__ */
#endif /* __LP64 */

#endif
