/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CSystem.h"



GLOBAL_VARIABLE DONT_OPTIMIZE_OUT LPFN_DIE_PROC         lpfnOnDie = NULLPTR;




_Success_(return != 0, ...)
PODNET_API
CLOCK
LongSleep(
        _In_                                    ULONGLONG                       ullSeconds
){
        TIMESPEC tsWait;
        HighResolutionClock(&tsWait);
        sleep(ullSeconds);
        return HighResolutionClockAndGetSeconds(&tsWait);
}




_Success_(return != 0, ...)
PODNET_API
CLOCK
Sleep(
        _In_                                    ULONGLONG                       ullMilliSeconds
){
        TIMESPEC tsWait;
        HighResolutionClock(&tsWait);
        usleep(MILLISECONDS_TO_MICROSECONDS(ullMilliSeconds));
        return HighResolutionClockAndGetMilliSeconds(&tsWait);
}



_Success_(return != 0, ...)
PODNET_API
CLOCK
MicroSleep(
        _In_                                    ULONGLONG                       ullMicroSeconds
){
        TIMESPEC tsWait;
        HighResolutionClock(&tsWait);
        usleep(ullMicroSeconds);
        return HighResolutionClockAndGetMicroSeconds(&tsWait);
}




_Success_(return != 0, ...)
PODNET_API
CLOCK
NanoSleep(
        _In_                                    ULONGLONG                       ullNanoSeconds
){
        TIMESPEC tmSpec, tsWait;
        CreateTimeSpecNano(&tmSpec, ullNanoSeconds);
	HighResolutionClock(&tsWait); 
        nanosleep(&tmSpec, NULLPTR);
	return HighResolutionClockAndGetNanoSeconds(&tsWait);
}




_Success_(return != 0, ...)
PODNET_API
BOOL
PrintLine(
        _In_                                    LPCSTR                          lpcszPrint
){
        PrintFormat("%s\n", lpcszPrint);
        return TRUE;
}




_Kills_Process_
PODNET_API
KILL
PostQuitMessage(
        _In_                                    LONG                            lQuitMessage
){ _exit(lQuitMessage); }




_Kills_Process_
PODNET_API
KILL
DieViolently(
        _In_                                    LPCSTR                          lpcszDieMessage
){
        CSTRING csDie[512];
        ZeroMemory(&csDie, sizeof(CHAR) * 512);
        StringCopy(csDie,        "==========================================\n\n");
        StringConcatenate(csDie, "DieViolently() has been invoked.\n");
        StringConcatenate(csDie, "NO CLEAN UP HAS BEEN DONE!\n\n");
        StringConcatenate(csDie, "NOTHING WAS CLOSED PROPERLY!\n\n");
        StringConcatenate(csDie, "CALLING THIS CAN HAVE IRREVERSIBLE DAMAGE!\n\n");
        StringConcatenate(csDie, "Closing invoke recieved string: ");
        StringConcatenate(csDie, lpcszDieMessage);
        StringConcatenate(csDie, "\n\n\n");
        WriteFile(GetStandardError(), csDie, NULL);
        FlushFileBuffer(GetStandardError());
        PostQuitMessage(1);
}




_Success_(return != FALSE, ...)
PODNET_API
BOOL
RegisterOnDieCallBack(
        _In_                                    LPFN_DIE_PROC                   lpfnDieCallBack
){
        InterlockedExchangePointer(lpfnOnDie, lpfnDieCallBack);
        return TRUE;
}




_Kills_Process_
PODNET_API
KILL
DiePeacefully(
        _In_                                    LPCSTR                          lpcszDieMessage,
        _In_                                    LPVOID                          lpParam
){
        if (lpfnOnDie != NULLPTR)
                lpfnOnDie(lpParam);
        WriteFile(GetStandardError(), lpcszDieMessage, NULL);
        PostQuitMessage(0);
}




_Kills_Process_
PODNET_API
KILL
DieByTheAtom(
        _In_                                    HANDLE                          hAtomTable,
        _In_                                    ATOM                            atAtom,
        _In_Opt_                                LPVOID                          lpParam
){
        CSTRING csDie[512];
        CSTRING csAtom[256];
        ZeroMemory(csDie, sizeof(CHAR) * 512);
        ZeroMemory(csAtom, sizeof(CHAR) * 256);
        
        (hAtomTable == NULLPTR) ? 
                GetGlobalAtomValue(atAtom, csAtom) :
                GetAtomValue(hAtomTable, atAtom, csAtom);
        
        (lpfnOnDie == NULLPTR) ? 
                NO_OPERATION() :
                lpfnOnDie(lpParam);
        
        StringCopy(csDie,        "==========================================\n\n");
        StringConcatenate(csDie, "DieByTheAtom() has been invoked!!\n\n");
        StringConcatenate(csDie, "If an LPFN_DIE_PROC was specified, it has\n");
        StringConcatenate(csDie, "been called, and has ran successfully.\n\n");
        StringConcatenate(csDie, "The ATOM provided has this value: \n");
        StringConcatenate(csDie, csAtom);
        StringConcatenate(csDie, "\n\n");
        WriteFile(GetStandardError(), csDie, 0);
        PostQuitMessage(0);
}




_Success_(return != NULL, _Non_Locking_)
PODNET_API
UARCHLONG
TypeToSize(
        _In_                                    TYPE                            tyType
){
        UARCHLONG ualSize;
        ualSize = NULL;

        switch(tyType)
        {

                case TYPE_CHAR:
                {
                        ualSize = sizeof(CHAR);
                        break;
                }

                case TYPE_BYTE:
                {
                        ualSize = sizeof(BYTE);
                        break;
                }

                case TYPE_SHORT:
                {
                        ualSize = sizeof(SHORT);
                        break;
                }

                case TYPE_USHORT:
                {
                        ualSize = sizeof(USHORT);
                        break;
                }

                case TYPE_WORD:
                {
                        ualSize = sizeof(WORD);
                        break;
                }

                case TYPE_LONG:
                {
                        ualSize = sizeof(LONG);
                        break;
                }

                case TYPE_ULONG:
                {
                        ualSize = sizeof(ULONG);
                        break;
                }

                case TYPE_DWORD:
                {
                        ualSize = sizeof(DWORD);
                        break;
                }

                case TYPE_LONGLONG:
                {
                        ualSize = sizeof(LONGLONG);
                        break;
                }

                case TYPE_ULONGLONG:
                {
                        ualSize = sizeof(ULONGLONG);
                        break;
                }

                case TYPE_QWORD:
                {
                        ualSize = sizeof(QWORD);
                        break;
                }

                case TYPE_ULTRALONG:
                {
                        ualSize = sizeof(ULTRALONG);
                        break;
                }

                case TYPE_UULTRALONG:
                {
                        ualSize = sizeof(UULTRALONG);
                        break;
                }

                case TYPE_ARCHLONG:
                {
                        ualSize = sizeof(ARCHLONG);
                        break;
                }

                case TYPE_UARCHLONG:
                {
                        ualSize = sizeof(UARCHLONG);
                        break;
                }

                case TYPE_LPVOID:
                {
                        ualSize = sizeof(LPVOID);
                        break;
                }

                case TYPE_DLPVOID:
                {
                        ualSize = sizeof(DLPVOID);
                        break;
                }

                case TYPE_HANDLE:
                {
                        ualSize = sizeof(HANDLE);
                        break;
                }

		case TYPE_LPSTR:
		{
			ualSize = sizeof(LPSTR);
			break;
		}

		case TYPE_DLPSTR:
		{
			ualSize = sizeof(DLPSTR);
			break;
		}

        }

        return ualSize;
}