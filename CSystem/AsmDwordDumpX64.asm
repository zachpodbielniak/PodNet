; 
; 
; 
;  ____           _ _   _      _   
; |  _ \ ___   __| | \ | | ___| |_ 
; | |_) / _ \ / _` |  \| |/ _ \ __|
; |  __/ (_) | (_| | |\  |  __/ |_ 
; |_|   \___/ \__,_|_| \_|\___|\__|
; 
; 
; General Purpose C Library.
; Copyright (C) 2017-2019 Zach Podbielniak
; 
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU Affero General Public License as
; published by the Free Software Foundation, either version 3 of the
; License, or (at your option) any later version.
; 
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU Affero General Public License for more details.
; 
; You should have received a copy of the GNU Affero General Public License
; along with this program.  If not, see <https://www.gnu.org/licenses/>.
; 
;
; Parameters are passed in RDI, RSI, RDX, RCX, R8, R9, Stack : in that order.


[DEFAULT REL]
[BITS 64]


global __DwordDumpX64
global __DwordDumpToFileX64

extern printf
extern sprintf
extern WriteFile
extern WaitForSingleObject
extern ReleaseSingleObject
extern CreateSpinLock


segment .data

lpszDwordDump:  db      "Dword Dump of size 0x%016X bytes",0x0A,"at address %p",0x0A,0x0A,0x00
lpszFormat:     db      "0x%08X",0x09,"0x%08X",0x09,"0x%08X",0x09,"0x%08X",0x0A,0x00

lpszBuffer: 	times 128 db 0
hSpinLock:	dq	0x0000000000000000




segment .text

; _Success_(return != FALSE, ...)
; __DwordDumpX64(
;       _In_            LPVOID          lpAddress       RDI,
;       _In_            QWORD           qwSize          RSI
;);

__DwordDumpX64:
        cmp rdi, 0
        je __DwordDumpX64Error
        cmp rsi, 0
        je __DwordDumpX64Error        

        ; Save extended register state
        push r15
        push r14
        push r13
        push r12
        push rbx

        mov r15, rsi
        mov r13, rsi
        mov r11, r13            ; Preserve qwSize
        shr r15, 4              ; Divide r15 by 16 for XmmWord Loops
        mov r14, r15         
        shl r14, 4              ; Find what is left over
        sub r13, r14            ; Get remainder
        shr r13, 2              ; Dword is 4 bytes wide, ignore anything after
        mov r12, rdi

        ; qwSize -> r11
        ; lpAddress -> r12
        ; qword loops -> r13
        ; xmmword loops -> r15

        xor rdi, rdi
        xor rsi, rsi
        xor rcx, rcx
        xor rdx, rdx


        __DwordDumpX64PrintFirstLine:
        lea rdi, [lpszDwordDump]
        mov rsi, r11
        mov rdx, r12        
        sub rsp, 32
        call printf wrt ..plt
        add rsp, 32
        xor rdi, rdi
        xor rsi, rsi

        cmp r15, 0
        je __DwordDumpX64DoneWithMain

        __DwordDumpX64MainLoop:
        movups xmm0, [r12]
        movq r8, xmm0
        lea rdi, [lpszFormat]
        mov esi, r8d
        shr r8, 32
        mov edx, r8d
        shufps xmm0, xmm0, 0b01001110
        movq r8, xmm0
        mov ecx, r8d
        shr r8, 32
        sub rsp, 48
        call printf wrt ..plt
        add rsp, 48
        add r12, 16
        dec r15                 ; XmmWord Loop Counter
        cmp r15, 0
        jne __DwordDumpX64MainLoop

        __DwordDumpX64DoneWithMain:
        cmp r13, 0
        je __DwordDumpX64Success

        ; Handle remainder
        __DwordDumpX64LastDwords:
        cmp r13, 1
        je __DwordDumpX641Dwords
        je __DwordDumpX642Dwords

        __DwordDumpX643Dwords:
        lea rdi, [lpszFormat]
        mov esi, DWORD [r12]
        mov edx, DWORD [r12+4]
        xor ecx, DWORD [r12+8]
        xor r8, r8
        sub rsp, 48
        call printf wrt ..plt
        add rsp, 48
        jmp __DwordDumpX64Success

        __DwordDumpX642Dwords:
        lea rdi, [lpszFormat]
        mov esi, DWORD [r12]
        mov edx, DWORD [r12+4]
        xor rcx, rcx
        xor r8, r8
        sub rsp, 48
        call printf wrt ..plt
        add rsp, 48
        jmp __DwordDumpX64Success

        __DwordDumpX641Dwords:
        lea rdi, [lpszFormat]
        mov esi, DWORD [r12]
        xor rdx, rdx
        xor rcx, rcx
        xor rdx, rdx
        xor r8, r8
        sub rsp, 48
        call printf wrt ..plt
        add rsp, 48

        __DwordDumpX64Success:
        pop rbx
        pop r12
        pop r13
        pop r14
        pop r15
        mov rax, 1
        ret

        __DwordDumpX64Error:
        pop rbx
        pop r12
        pop r13
        pop r14
        pop r15
        mov rax, 0
        ret




; _Success_(return != FALSE, ...)
; __DwordDumpToFileX64(
;       _In_            LPVOID          lpAddress       RDI,
;       _In_            QWORD           qwSize          RSI,
;	_In_		HANDLE		hFile		RDX
;);

__DwordDumpToFileX64:
	cmp rdi, 0
	je __DwordDumpToFileX64Error
	cmp rsi, 0
	je __DwordDumpToFileX64Error
	cmp rdx, 0
	je __DwordDumpToFileX64Error

        ; Save extended register state
        push r15
        push r14
        push r13
        push r12
        push rbx

        mov r15, rsi
        mov r13, rsi
        mov r11, r13            ; Preserve qwSize
        shr r15, 4              ; Divide r15 by 16 for XmmWord Loops
        mov r14, r15         
        shl r14, 4              ; Find what is left over
        sub r13, r14            ; Get remainder
        shr r13, 2              ; Dword is 4 bytes wide, ignore anything after
        mov r12, rdi
	mov rbx, rdx		; Handle in rbx

        ; hFile -> rbx
        ; qwSize -> r11
        ; lpAddress -> r12
        ; qword loops -> r13
        ; xmmword loops -> r15
        
        xor rdi, rdi
        xor rsi, rsi
        xor rcx, rcx
        xor rdx, rdx

	mov rdi, QWORD [hSpinLock]
	cmp rdi, 0
	jne __DwordDumpToFileX64AcquireSpinLock	

	__DwordDumpToFileX64CreateSpinLock:
	sub rsp, 16
	call CreateSpinLock wrt ..plt
	add rsp, 16
	mov QWORD [hSpinLock], rax
	mov rdi, rax
	xor rax, rax

	__DwordDumpToFileX64AcquireSpinLock:
	mov rsi, 0xFFFFFFFFFFFFFFFF
	sub rsp, 16
	call WaitForSingleObject wrt ..plt
	add rsp, 16
	cmp rax, 0
	je __DwordDumpToFileX64Error

	; Now we own the lock on lpszBuffer
	
	; Print out dword dump header
	
	__DwordDumpToFileX64PrintFirstLine:
	lea rdi, [lpszBuffer]
	lea rsi, [lpszDwordDump]
	mov rdx, r11
	mov rcx, r12
	sub rsp, 32
	call sprintf wrt ..plt
	add rsp, 32

	; Call WriteFile
	mov rdi, rbx
	lea rsi, [lpszBuffer]
	xor rdx, rdx
	xor rcx, rcx
	sub rsp, 32
	call WriteFile wrt ..plt
	add rsp, 32

        cmp r15, 0
        je __DwordDumpToFileX64DoneWithMain

	__DwordDumpToFileX64MainLoop:
        movups xmm0, [r12]
        movq r8, xmm0
        lea rdi, [lpszBuffer]
	lea rsi, [lpszFormat]
        mov edx, r8d
        shr r8, 32
        mov ecx, r8d
        shufps xmm0, xmm0, 0b01001110
        movq r9, xmm0
        mov r8d, r9d
        shr r9, 32
        sub rsp, 64
        call sprintf wrt ..plt
        add rsp, 64

	lea rsi, [lpszBuffer]
	mov rdi, rbx
	xor rdx, rdx
	xor rcx, rcx
	sub rsp, 32
	call WriteFile wrt ..plt
	add rsp, 32

        add r12, 16
        dec r15                 ; XmmWord Loop Counter
        cmp r15, 0
        jne __DwordDumpToFileX64MainLoop

	__DwordDumpToFileX64DoneWithMain:
	cmp r13, 0
	je __DwordDumpToFileX64Success

	cmp r13, 2
	je __DwordDumpToFileX64TwoDwords
	cmp r13, 1
	je __DwordDumpToFileX64OneDword

	__DwordDumpToFileX64ThreeDwords:
	lea rdi, [lpszBuffer]
	lea rsi, [lpszFormat]
	mov edx, [r12]
	mov rcx, [r12+4]
	mov r8d, [r12+8]
	xor r9, r9
	sub rsp, 64
	call sprintf wrt ..plt
	add rsp, 64
	mov rdi, rbx
	lea rsi, [lpszBuffer]
	xor rdx, rdx
	xor rcx, rcx
	sub rsp, 32
	call WriteFile wrt ..plt
	add rsp, 32
	mov rdi, QWORD [hSpinLock]
	sub rsp, 16
	call ReleaseSingleObject wrt ..plt
	add rsp, 16
	jmp __DwordDumpToFileX64Success	


	__DwordDumpToFileX64TwoDwords:
	lea rdi, [lpszBuffer]
	lea rsi, [lpszFormat]
	mov edx, [r12]
	mov ecx, [r12+4]
	xor r8, r8
	xor r9, r9
	sub rsp, 64
	call sprintf wrt ..plt
	add rsp, 64
	mov rdi, rbx
	lea rsi, [lpszBuffer]
	xor rdx, rdx
	xor rcx, rcx
	sub rsp, 32
	call WriteFile wrt ..plt
	add rsp, 32
	mov rdi, QWORD [hSpinLock]
	sub rsp, 16
	call ReleaseSingleObject wrt ..plt
	add rsp, 16
	jmp __DwordDumpToFileX64Success
	

	__DwordDumpToFileX64OneDword:
	lea rdi, [lpszBuffer]
	lea rsi, [lpszFormat]
	mov edx, DWORD [r12]
	xor rcx, rcx
	xor r8, r8
	xor r9, r9
	sub rsp, 64
	call sprintf wrt ..plt
	add rsp, 64
	mov rdi, rbx
	lea rsi, [lpszBuffer]
	xor rdx, rdx
	xor rcx, rcx
	sub rsp, 32
	call WriteFile wrt ..plt
	add rsp, 32
	mov rdi, QWORD [hSpinLock]
	sub rsp, 16
	call ReleaseSingleObject wrt ..plt
	add rsp, 16


	__DwordDumpToFileX64Success:
        pop rbx
        pop r12
        pop r13
        pop r14
        pop r15
	mov rax, 1
	ret

	__DwordDumpToFileX64Error:
        pop rbx
        pop r12
        pop r13
        pop r14
        pop r15
	mov rax, 0
	ret
