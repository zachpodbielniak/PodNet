/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CSYSTEM_H
#define CSYSTEM_H


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../CError/CError.h"
#include "../CClock/CClock.h"
#include "../CAtom/CAtomTable.h"
#include "../CFile/CFile.h"
/* #include "CDwordDump.h" */


typedef VOID                    KILL;

typedef _Call_Back_ BOOL (* LPFN_DIE_PROC)(
        _In_Opt_        LPVOID          lpParam
);




_Success_(return != 0, ...)
PODNET_API
CLOCK
LongSleep(
        _In_                                    ULONGLONG                       ullSeconds
);




_Success_(return != 0, ...)
PODNET_API
CLOCK
Sleep(
        _In_                                    ULONGLONG                       ullMilliSeconds
);



_Success_(return != 0, ...)
PODNET_API
CLOCK
MicroSleep(
        _In_                                    ULONGLONG                       ullMicroSeconds
);




_Success_(return != 0, ...)
PODNET_API
CLOCK
NanoSleep(
        _In_                                    ULONGLONG                       ullNanoSeconds
);


_Success_(return != 0, ...)
PODNET_API
BOOL
PrintLine(
        _In_                                    LPCSTR                          lpcszPrint
);



_Kills_Process_
PODNET_API
KILL
PostQuitMessage(
        _In_                                    LONG                            lQuitMessage
);




_Success_(return != FALSE, ...)
PODNET_API
BOOL
RegisterOnDieCallBack(
        _In_                                    LPFN_DIE_PROC                   lpfnDieCallBack
);




_Kills_Process_
PODNET_API
KILL
DieViolently(
        _In_                                    LPCSTR                          lpcszDieMessage
);




_Kills_Process_
PODNET_API
KILL
DiePeacefully(
        _In_                                    LPCSTR                          lpcszDieMessage,
        _In_                                    LPVOID                          lpParam
);




/*
        He who lives by the ATOM,
        Must DieByTheAtom();
                        - The Pod
*/

_Kills_Process_
PODNET_API
KILL
DieByTheAtom(
        _In_                                    HANDLE                          hAtomTable,
        _In_                                    ATOM                            atAtom,
        _In_Opt_                                LPVOID                          lpParam
);




_Success_(return != NULL, _Non_Locking_)
PODNET_API
UARCHLONG
TypeToSize(
        _In_                                    TYPE                            tyType
) CALL_TYPE(HOT);

#endif