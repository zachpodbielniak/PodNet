/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CCLOCK_H
#define CCLOCK_H


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../CError/CError.h"

typedef clock_t                 CLOCK, *LPCLOCK;
typedef struct timespec         TIMESPEC, *LPTIMESPEC;


/* These macros convert with truncation */

#define SECONDS_TO_MILLISECONDS(X)              ((X) * ONE_THOUSAND)
#define SECONDS_TO_MICROSECONDS(X)              ((X) * ONE_MILLION)
#define SECONDS_TO_NANOSECONDS(X)               ((X) * ONE_BILLION)

#define MILLISECONDS_TO_SECONDS(X)              ((X) / ONE_THOUSAND)
#define MILLISECONDS_TO_MICROSECONDS(X)         ((X) * ONE_THOUSAND)
#define MILLISECONDS_TO_NANOSECONDS(X)          ((X) * ONE_MILLION)

#define MICROSECONDS_TO_SECONDS(X)              ((X) / ONE_MILLION)
#define MICROSECONDS_TO_MILLISECONDS(X)         ((X) / ONE_THOUSAND)
#define MICROSECONDS_TO_NANOSECONDS(X)          ((X) * ONE_THOUSAND)

#define NANOSECONDS_TO_SECONDS(X)               ((X) / ONE_BILLION)
#define NANOSECONDS_TO_MILLISECONDS(X)          ((X) / ONE_MILLION)
#define NANOSECONDS_TO_MICROSECONDS(X)          ((X) / ONE_THOUSAND)




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
Clock(
        _Out_                  LPCLOCK                  lpcClock
);




_Success_(return != 0, _Non_Locking_)
PODNET_API
ULONGLONG
ClockAndGetNanoSeconds(
        _In_                    LPCLOCK                 lpcClock
);




_Success_(return != 0, _Non_Locking_)
PODNET_API
ULONGLONG
ClockAndGetMicroSeconds(
        _In_                    LPCLOCK                 lpcClock
);




_Success_(return != 0, _Non_Locking_)
PODNET_API
ULONGLONG
ClockAndGetMilliSeconds(
        _In_                    LPCLOCK                 lpcClock
);




_Success_(return != 0, _Non_Locking_)
PODNET_API
ULONGLONG
ClockAndGetSeconds(
        _In_                    LPCLOCK                 lpcClock
);




_Success_(return != NAN, _Non_Locking_)
PODNET_API
FLOAT
ClockAndGetSecondsFloat(
        _In_                    LPCLOCK                 lpcClock
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
CreateTimeSpec(
        _In_                    LPTIMESPEC              lptsTime,
        _In_                    ULONGLONG               ullMilliSeconds
);


_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
CreateTimeSpecNano(
        _In_                    LPTIMESPEC              lptsTime,
        _In_                    ULONGLONG               ullNanoSeconds
);




_Success_(return != NULL, _Non_Locking_)
PODNET_API
ULONGLONG
TimeSpecToSeconds(
        _In_                    LPTIMESPEC              lptsTime
);




_Success_(return != NULL, _Non_Locking_)
PODNET_API
ULONGLONG
TimeSpecToMilliSeconds(
        _In_                    LPTIMESPEC              lptsTime
);




_Success_(return != NULL, _Non_Locking_)
PODNET_API
ULONGLONG
TimeSpecToMicroSeconds(
        _In_                    LPTIMESPEC              lptsTime
);




_Success_(return != NULL, _Non_Locking_)
PODNET_API
ULONGLONG
TimeSpecToNanoSeconds(
        _In_                    LPTIMESPEC              lptsTime
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
GetTime(
	_In_ 			LPTIMESPEC 		lptsTime
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
HighResolutionClock(
        _In_                    LPTIMESPEC              lptsTime
);




_Success_(return != NULL, _Non_Locking_)
PODNET_API
ULONGLONG
HighResolutionClockAndGetSeconds(
        _In_                    LPTIMESPEC              lptsTime
);




_Success_(return != NULL, _Non_Locking_)
PODNET_API
ULONGLONG
HighResolutionClockAndGetMilliSeconds(
        _In_                    LPTIMESPEC              lptsTime
);




_Success_(return != NULL, _Non_Locking_)
PODNET_API
ULONGLONG
HighResolutionClockAndGetMicroSeconds(
        _In_                    LPTIMESPEC              lptsTime
);




_Success_(return != NULL, _Non_Locking_)
PODNET_API
ULONGLONG
HighResolutionClockAndGetNanoSeconds(
        _In_                    LPTIMESPEC              lptsTime
);

#endif