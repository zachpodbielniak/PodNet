/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CTIMER_H
#define CTIMER_H



#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../CHandle/CHandle.h"
#include "../CThread/CThread.h"
#include "../CUnit/CConvertUnit.h"



typedef _Call_Back_ LPVOID (* LPFN_TIMER_PROC)(
	_In_ 		HANDLE 		hTimer,
	_In_ 		LPVOID 		lpParam 
);




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNET_API
HANDLE
CreateTimer(
	_In_ 		UNIT_TYPE 		utTime,
	_In_ 		ULONGLONG 		ullValue
);




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNET_API
HANDLE
CreateTimerEx(
	_In_ 		UNIT_TYPE 		utTime,
	_In_ 		ULONGLONG 		ullValue,
	_In_ 		LPFN_TIMER_PROC		lpfnCallBack,
	_In_Opt_ 	LPVOID			lpParam
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
SetTimerValue(
	_In_ 		HANDLE 			hTimer,
	_In_ 		UNIT_TYPE 		utTime,
	_In_ 		ULONGLONG 		ullValue
);




/* Returns the time in MilliSeconds */
_Success_(return != FALSE, _Non_Locking_)
PODNET_API
ULONGLONG
GetTimerValue(
	_In_ 		HANDLE 			hTimer
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
ResetTimer(
	_In_ 		HANDLE 			hTimer
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
ResetTimerEx(
	_In_ 		HANDLE 			hTimer,
	_In_ 		UNIT_TYPE 		utTime,
	_In_ 		ULONGLONG 		ullValue 
);




_Success_(return != MAX_ULONGLONG, _Non_Locking_)
PODNET_API
ULONGLONG
GetTimerRemaining(
	_In_ 		HANDLE 			hTimer
);


#endif