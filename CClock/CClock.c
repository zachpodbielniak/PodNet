/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CClock.h"




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
Clock(
        _Out_                  LPCLOCK                  lpcClock
){
        SET_ERROR_AND_EXIT_IF_NULL(lpcClock, ERROR_CLOCK_NOT_DEFINED, FALSE);
        *lpcClock = clock();
        return TRUE;
}




_Success_(return != 0, _Non_Locking_)
PODNET_API
ULONGLONG
ClockAndGetNanoSeconds(
        _In_                    LPCLOCK                 lpcClock
){
        SET_ERROR_AND_EXIT_IF_NULL(lpcClock, ERROR_CLOCK_NOT_DEFINED, 0ULL);
        FLOAT fSecs;
        CLOCK cClock;
        cClock = clock() - (*lpcClock);
        fSecs = ((FLOAT)cClock) / CLOCKS_PER_SEC;
        fSecs *= (FLOAT)((ULONG)ONE_BILLION);
        return (ULONGLONG)fSecs;
}




_Success_(return != 0, _Non_Locking_)
PODNET_API
ULONGLONG
ClockAndGetMicroSeconds(
        _In_                    LPCLOCK                 lpcClock
){
        SET_ERROR_AND_EXIT_IF_NULL(lpcClock, ERROR_CLOCK_NOT_DEFINED, 0ULL);
        FLOAT fSecs;
        CLOCK cClock;
        cClock = clock() - (*lpcClock);
        fSecs = ((FLOAT)cClock) / CLOCKS_PER_SEC;
        fSecs *= (FLOAT)((ULONG)ONE_MILLION);
        return (ULONGLONG)fSecs;
}




_Success_(return != 0, _Non_Locking_)
PODNET_API
ULONGLONG
ClockAndGetMilliSeconds(
        _In_                    LPCLOCK                 lpcClock
){
        SET_ERROR_AND_EXIT_IF_NULL(lpcClock, ERROR_CLOCK_NOT_DEFINED, 0ULL);
        FLOAT fSecs;
        CLOCK cClock;
        cClock = clock() - (*lpcClock);
        fSecs = ((FLOAT)cClock) / CLOCKS_PER_SEC;
        fSecs *= (FLOAT)((ULONG)ONE_THOUSAND);
        return (ULONGLONG)fSecs;
}




_Success_(return != 0, _Non_Locking_)
PODNET_API
ULONGLONG
ClockAndGetSeconds(
        _In_                    LPCLOCK                 lpcClock
){
        SET_ERROR_AND_EXIT_IF_NULL(lpcClock, ERROR_CLOCK_NOT_DEFINED, 0ULL);
        FLOAT fSecs;
        CLOCK cClock;
        cClock = clock() - (*lpcClock);
        fSecs = ((FLOAT)cClock) / CLOCKS_PER_SEC;
        return (ULONGLONG)fSecs;
}




_Success_(return != NAN, _Non_Locking_)
PODNET_API
FLOAT
ClockAndGetSecondsFloat(
        _In_                    LPCLOCK                 lpcClock
){
        SET_ERROR_AND_EXIT_IF_NULL(lpcClock, ERROR_CLOCK_NOT_DEFINED, NAN);
        CLOCK cClock;
        cClock = clock() - (*lpcClock);
        return  ((FLOAT)cClock) / CLOCKS_PER_SEC;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
CreateTimeSpec(
        _In_                    LPTIMESPEC              lptsTime,
        _In_                    ULONGLONG               ullMilliSeconds
){
        EXIT_IF_NULL(lptsTime, FALSE);
	lptsTime->tv_sec = (ULONGLONG)(((DOUBLE)ullMilliSeconds) / 1000.0);
	lptsTime->tv_nsec = (ULONGLONG)MILLISECONDS_TO_NANOSECONDS((ULONGLONG)(((DOUBLE)ullMilliSeconds) - (SECONDS_TO_MILLISECONDS(lptsTime->tv_sec))));
        return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
CreateTimeSpecNano(
        _In_                    LPTIMESPEC              lptsTime,
        _In_                    ULONGLONG               ullNanoSeconds
){
        EXIT_IF_NULL(lptsTime, FALSE);
        lptsTime->tv_sec = (ULONGLONG)(((DOUBLE)ullNanoSeconds) / 1000000000.0);
        lptsTime->tv_nsec = (ULONGLONG)(((DOUBLE)ullNanoSeconds - (SECONDS_TO_NANOSECONDS(lptsTime->tv_sec))));
        return TRUE;
}




_Success_(return != NULL, _Non_Locking_)
PODNET_API
ULONGLONG
TimeSpecToSeconds(
        _In_                    LPTIMESPEC              lptsTime
){
        EXIT_IF_NULL(lptsTime, NULL);
        ULONGLONG ullTime;
        ullTime = lptsTime->tv_sec;
        ullTime += NANOSECONDS_TO_SECONDS(lptsTime->tv_nsec);
        return ullTime;
}




_Success_(return != NULL, _Non_Locking_)
PODNET_API
ULONGLONG
TimeSpecToMilliSeconds(
        _In_                    LPTIMESPEC              lptsTime
){
        EXIT_IF_NULL(lptsTime, NULL);
        ULONGLONG ullTime;
        ullTime = SECONDS_TO_MILLISECONDS(lptsTime->tv_sec);
        ullTime += NANOSECONDS_TO_MILLISECONDS(lptsTime->tv_nsec);
        return ullTime;
}




_Success_(return != NULL, _Non_Locking_)
PODNET_API
ULONGLONG
TimeSpecToMicroSeconds(
        _In_                    LPTIMESPEC              lptsTime
){
        EXIT_IF_NULL(lptsTime, NULL);
        ULONGLONG ullTime;
        ullTime = SECONDS_TO_MICROSECONDS(lptsTime->tv_sec);
        ullTime += NANOSECONDS_TO_MICROSECONDS(lptsTime->tv_nsec);
        return ullTime;
}




_Success_(return != NULL, _Non_Locking_)
PODNET_API
ULONGLONG
TimeSpecToNanoSeconds(
        _In_                    LPTIMESPEC              lptsTime
){
        EXIT_IF_NULL(lptsTime, NULL);
        ULONGLONG ullTime;
        ullTime = SECONDS_TO_NANOSECONDS(lptsTime->tv_sec);
        ullTime += lptsTime->tv_nsec;
        return ullTime;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
GetTime(
	_In_ 			LPTIMESPEC 		lptsTime
){
	EXIT_IF_NULL(lptsTime, FALSE);
	return (0 == clock_gettime(CLOCK_REALTIME, lptsTime)) ? TRUE : FALSE;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
HighResolutionClock(
        _In_                    LPTIMESPEC              lptsTime
){
        EXIT_IF_NULL(lptsTime, FALSE);
        return (0 == clock_gettime(CLOCK_REALTIME, lptsTime)) ? TRUE : FALSE;
}




_Success_(return != NULL, _Non_Locking_)
PODNET_API
ULONGLONG
HighResolutionClockAndGetSeconds(
        _In_                    LPTIMESPEC              lptsTime
){
        EXIT_IF_NULL(lptsTime, FALSE);
        TIMESPEC tsNewClock;
        clock_gettime(CLOCK_REALTIME, &tsNewClock);
        return (TimeSpecToSeconds(&tsNewClock) - TimeSpecToSeconds(lptsTime));
}




_Success_(return != NULL, _Non_Locking_)
PODNET_API
ULONGLONG
HighResolutionClockAndGetMilliSeconds(
        _In_                    LPTIMESPEC              lptsTime
){
        EXIT_IF_NULL(lptsTime, FALSE);
        TIMESPEC tsNewClock;
        clock_gettime(CLOCK_REALTIME, &tsNewClock);
        return (TimeSpecToMilliSeconds(&tsNewClock) - TimeSpecToMilliSeconds(lptsTime));
}




_Success_(return != NULL, _Non_Locking_)
PODNET_API
ULONGLONG
HighResolutionClockAndGetMicroSeconds(
        _In_                    LPTIMESPEC              lptsTime
){
        EXIT_IF_NULL(lptsTime, FALSE);
        TIMESPEC tsNewClock;
        clock_gettime(CLOCK_REALTIME, &tsNewClock);
        return (TimeSpecToMicroSeconds(&tsNewClock) - TimeSpecToMicroSeconds(lptsTime));
}




_Success_(return != NULL, _Non_Locking_)
PODNET_API
ULONGLONG
HighResolutionClockAndGetNanoSeconds(
        _In_                    LPTIMESPEC              lptsTime
){
        EXIT_IF_NULL(lptsTime, FALSE);
        TIMESPEC tsNewClock;
        clock_gettime(CLOCK_REALTIME, &tsNewClock);
        return (TimeSpecToNanoSeconds(&tsNewClock) - TimeSpecToNanoSeconds(lptsTime));
}