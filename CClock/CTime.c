/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CTime.h"




_Success_(return != MAX_ULONGLONG, _Non_Locking_)
PODNET_API
TIME
GetCurrentTime(
	VOID
){
	TIME tX;
	time(&tX);
	return tX;
}




_Success_(return != MAX_ULONGLONG, _Non_Locking_)
PODNET_API 
DOUBLE
GetTimeDifference(
	_In_ 		TIME 			tTime1,
	_In_ 		TIME 			tTime2
){ return difftime(tTime1, tTime2); }




_Success_(return != FALSE, _Non_Locking_)
PODNET_API 
BOOL
DestroyTimeInfo(
	_In_ 		LPTIME_INFO		lptiTime
){
	EXIT_IF_UNLIKELY_NULL(lptiTime, FALSE);
	FreeMemory(lptiTime);
	return TRUE;
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API 
LPTIME_INFO
GetLocalTime(
	_In_		TIME 			tTime
){ 
	LPTIME_INFO lptiTime;
	
	lptiTime = GlobalAllocAndZero(sizeof(TIME_INFO));
	if (NULLPTR == lptiTime)
	{ return NULLPTR; }

	localtime_r(&tTime, lptiTime);
	return lptiTime;
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API 
LPTIME_INFO 
GetUtcTime(
	_In_ 		TIME 			tTime
){
	LPTIME_INFO lptiTime;
	
	lptiTime = GlobalAllocAndZero(sizeof(TIME_INFO));
	if (NULLPTR == lptiTime)
	{ return NULLPTR; }

	gmtime_r(&tTime, lptiTime);
	return lptiTime;
}




_Success_(return != MAX_ULONGLONG, _Non_Locking_)
PODNET_API 
TIME 
ConvertTimeInfoToTime(
	_In_ 		LPTIME_INFO		lptiTime
){
	EXIT_IF_UNLIKELY_NULL(lptiTime, FALSE);
	return mktime(lptiTime);
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPCSTR
ConvertTimeInfoToString(
	_In_ 		LPTIME_INFO		lptiTime
){
	EXIT_IF_UNLIKELY_NULL(lptiTime, NULLPTR);
	return asctime(lptiTime);
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
ConvertTimeInfoToStringByCopy(
	_In_ 		LPTIME_INFO		lptiTime,
	_Out_Z_ 	LPSTR 			lpszOutput,
	_In_ 		UARCHLONG 		ualBufferSize
){
	EXIT_IF_UNLIKELY_NULL(lptiTime, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpszOutput, FALSE);
	EXIT_IF_UNLIKELY_NULL(ualBufferSize, FALSE);

	CSTRING csBuffer[16384];
	
	ZeroMemory(csBuffer, sizeof(csBuffer));
	asctime_r(lptiTime, csBuffer);
	StringCopySafe(lpszOutput, csBuffer, ualBufferSize);
	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
ConvertTimeInfoToFormattedStringByCopy(
	_In_ 		LPTIME_INFO		lptiTime,
	_In_ 		LPCSTR RESTRICT 	lpcszFormat,
	_Out_Z_ 	LPSTR RESTRICT		lpszOutput,
	_In_ 		UARCHLONG 		ualBufferSize
){
	EXIT_IF_UNLIKELY_NULL(lptiTime, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpcszFormat, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpszOutput, FALSE);
	EXIT_IF_UNLIKELY_NULL(ualBufferSize, FALSE);

	strftime(
		lpszOutput,
		ualBufferSize,
		lpcszFormat,
		lptiTime
	);
	
	return TRUE;
}