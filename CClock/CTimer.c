/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CTimer.h"


typedef struct __TIMER
{
	INHERITS_FROM_HANDLE();
	HANDLE 		hThread;
	LPFN_TIMER_PROC	lpfnCallBack;
	LPVOID 		lpParam;
	ULONGLONG 	ullStartStamp;
	ULONGLONG 	ullFinishStamp;
	ULONGLONG 	ullValue;
	UNIT_TYPE 	utTimeType;
} TIMER, *LPTIMER;



INTERNAL_OPERATION
BOOL
__DestroyTimer(
	_In_ 		HDERIVATIVE 		hDerivative,
	_In_Opt_ 	ULONG 			ulFlags
){
	UNREFERENCED_PARAMETER(ulFlags);
	EXIT_IF_UNLIKELY_NULL(hDerivative, FALSE);

	LPTIMER lptmrTimer;
	lptmrTimer = (LPTIMER)hDerivative;

	DestroyObject(lptmrTimer->hThread);
	FreeMemory(lptmrTimer);

	return TRUE;
}




INTERNAL_OPERATION
BOOL
__WaitForTimer(
	_In_ 		HANDLE			hTimer,
	_In_		ULONGLONG 		ullMilliSeconds
){
	EXIT_IF_UNLIKELY_NULL(hTimer, FALSE);	

	LPTIMER lptmrTimer;
	lptmrTimer = (LPTIMER)GetHandleDerivative(hTimer);

	return WaitForSingleObject(lptmrTimer->hThread, ullMilliSeconds);
}




INTERNAL_OPERATION
LPVOID
__TimerThreadProc(
	_In_Opt_ 	LPVOID 			lpParam
){
	EXIT_IF_UNLIKELY_NULL(lpParam, NULLPTR);

	HANDLE hTimer;
	LPTIMER lptmrTimer;
	hTimer = (HANDLE)lpParam;
	lptmrTimer = (LPTIMER)GetHandleDerivative(hTimer);

	lptmrTimer->ullStartStamp = time(NULLPTR);
	lptmrTimer->ullFinishStamp = (
		lptmrTimer->ullStartStamp + 
		(ULONGLONG)ConvertUnit64(
			lptmrTimer->ullValue, 
			UNIT_TYPE_MILLISECOND, 
			UNIT_TYPE_SECOND
		)
	);

	Sleep(lptmrTimer->ullValue);
	
	if (NULLPTR != lptmrTimer->lpfnCallBack)
	{ return lptmrTimer->lpfnCallBack(hTimer, lptmrTimer->lpParam); }

	return NULLPTR;
}




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNET_API
HANDLE
CreateTimer(
	_In_ 		UNIT_TYPE 		utTime,
	_In_ 		ULONGLONG 		ullValue
){
	EXIT_IF_NULL(ullValue, NULL_OBJECT);

	HANDLE hTimer;
	LPTIMER lptmrTimer;

	lptmrTimer = (LPTIMER)GlobalAllocAndZero(sizeof(TIMER));
	/* cppcheck-suppress memleak */
	EXIT_IF_UNLIKELY_NULL(lptmrTimer, NULL_OBJECT);

	switch ((ULONG)utTime)
	{

		case UNIT_TYPE_YEAR: 
		{
			lptmrTimer->utTimeType = UNIT_TYPE_YEAR;
			lptmrTimer->ullValue = (ULONGLONG)ConvertUnit64(
				(DOUBLE)ullValue, 
				UNIT_TYPE_YEAR, 
				UNIT_TYPE_MILLISECOND
			);
			break;
		}

		case UNIT_TYPE_MONTH: 
		{
			lptmrTimer->utTimeType = UNIT_TYPE_MONTH;
			lptmrTimer->ullValue = (ULONGLONG)ConvertUnit64(
				(DOUBLE)ullValue, 
				UNIT_TYPE_MONTH, 
				UNIT_TYPE_MILLISECOND
			);
			break;
		}

		case UNIT_TYPE_FORTNIGHT: 
		{
			lptmrTimer->utTimeType = UNIT_TYPE_FORTNIGHT;
			lptmrTimer->ullValue = (ULONGLONG)ConvertUnit64(
				(DOUBLE)ullValue, 
				UNIT_TYPE_FORTNIGHT, 
				UNIT_TYPE_MILLISECOND
			);
			break;
		}

		case UNIT_TYPE_WEEK: 
		{
			lptmrTimer->utTimeType = UNIT_TYPE_WEEK;
			lptmrTimer->ullValue = (ULONGLONG)ConvertUnit64(
				(DOUBLE)ullValue, 
				UNIT_TYPE_WEEK, 
				UNIT_TYPE_MILLISECOND
			);
			break;
		}

		case UNIT_TYPE_DAY: 
		{
			lptmrTimer->utTimeType = UNIT_TYPE_DAY;
			lptmrTimer->ullValue = (ULONGLONG)ConvertUnit64(
				(DOUBLE)ullValue, 
				UNIT_TYPE_DAY, 
				UNIT_TYPE_MILLISECOND
			);
			break;
		}

		case UNIT_TYPE_HOUR:
		{
			lptmrTimer->utTimeType = UNIT_TYPE_HOUR;
			lptmrTimer->ullValue = (ULONGLONG)ConvertUnit64(
				(DOUBLE)ullValue, 
				UNIT_TYPE_HOUR, 
				UNIT_TYPE_MILLISECOND
			);
			break;
		}

		case UNIT_TYPE_MINUTE: 
		{
			lptmrTimer->utTimeType = UNIT_TYPE_MINUTE;
			lptmrTimer->ullValue = (ULONGLONG)ConvertUnit64(
				(DOUBLE)ullValue, 
				UNIT_TYPE_MINUTE, 
				UNIT_TYPE_MILLISECOND
			);
			break;
		}

		case UNIT_TYPE_SECOND:
		{
			lptmrTimer->utTimeType = UNIT_TYPE_SECOND;
			lptmrTimer->ullValue = (ULONGLONG)ConvertUnit64(
				(DOUBLE)ullValue, 
				UNIT_TYPE_SECOND, 
				UNIT_TYPE_MILLISECOND
			);
			break;
		}

		case UNIT_TYPE_MILLISECOND:
		{
			lptmrTimer->utTimeType = UNIT_TYPE_MILLISECOND;
			lptmrTimer->ullValue = ullValue;
			break;
		}

		case UNIT_TYPE_MICROSECOND:
		{
			lptmrTimer->utTimeType = UNIT_TYPE_MICROSECOND;
			lptmrTimer->ullValue = (ULONGLONG)ConvertUnit64(
				(DOUBLE)ullValue, 
				UNIT_TYPE_MICROSECOND, 
				UNIT_TYPE_MILLISECOND
			);
			break;
		}

		case UNIT_TYPE_NANOSECOND:
		{
			lptmrTimer->utTimeType = UNIT_TYPE_NANOSECOND;
			lptmrTimer->ullValue = (ULONGLONG)ConvertUnit64(
				(DOUBLE)ullValue, 
				UNIT_TYPE_NANOSECOND, 
				UNIT_TYPE_MILLISECOND
			);
			break;
		}

		default:
		{	
			FreeMemory(lptmrTimer);
			return NULL_OBJECT;
			break;
		}

	}


	hTimer = CreateHandleWithSingleInheritor(
		lptmrTimer,
		&(lptmrTimer->hThis),
		HANDLE_TYPE_TIMER,
		__DestroyTimer,
		NULL,
		NULLPTR,
		NULL,
		NULLPTR,
		NULL,
		&(lptmrTimer->ullId),
		NULL
	);

	SetHandleEventWaitFunction(hTimer, __WaitForTimer, NULL);
	lptmrTimer->hThread = CreateThread(__TimerThreadProc, (LPVOID)hTimer, 0);

	EXIT_IF_UNLIKELY_NULL(lptmrTimer->hThread, NULL_OBJECT);

	return hTimer;
}




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNET_API
HANDLE
CreateTimerEx(
	_In_ 		UNIT_TYPE 		utTime,
	_In_ 		ULONGLONG 		ullValue,
	_In_ 		LPFN_TIMER_PROC		lpfnCallBack,
	_In_Opt_ 	LPVOID 			lpParam
){
	EXIT_IF_NULL(ullValue, NULL_OBJECT);

	HANDLE hTimer;
	LPTIMER lptmrTimer;

	lptmrTimer = (LPTIMER)GlobalAllocAndZero(sizeof(TIMER));
	/* cppcheck-suppress memleak */
	EXIT_IF_UNLIKELY_NULL(lptmrTimer, NULL_OBJECT);

	switch ((ULONG)utTime)
	{

		case UNIT_TYPE_YEAR: 
		{
			lptmrTimer->utTimeType = UNIT_TYPE_YEAR;
			lptmrTimer->ullValue = (ULONGLONG)ConvertUnit64(
				(DOUBLE)ullValue, 
				UNIT_TYPE_YEAR, 
				UNIT_TYPE_MILLISECOND
			);
			break;
		}

		case UNIT_TYPE_MONTH: 
		{
			lptmrTimer->utTimeType = UNIT_TYPE_MONTH;
			lptmrTimer->ullValue = (ULONGLONG)ConvertUnit64(
				(DOUBLE)ullValue, 
				UNIT_TYPE_MONTH, 
				UNIT_TYPE_MILLISECOND
			);
			break;
		}

		case UNIT_TYPE_FORTNIGHT: 
		{
			lptmrTimer->utTimeType = UNIT_TYPE_FORTNIGHT;
			lptmrTimer->ullValue = (ULONGLONG)ConvertUnit64(
				(DOUBLE)ullValue, 
				UNIT_TYPE_FORTNIGHT, 
				UNIT_TYPE_MILLISECOND
			);
			break;
		}

		case UNIT_TYPE_WEEK: 
		{
			lptmrTimer->utTimeType = UNIT_TYPE_WEEK;
			lptmrTimer->ullValue = (ULONGLONG)ConvertUnit64(
				(DOUBLE)ullValue, 
				UNIT_TYPE_WEEK, 
				UNIT_TYPE_MILLISECOND
			);
			break;
		}

		case UNIT_TYPE_DAY: 
		{
			lptmrTimer->utTimeType = UNIT_TYPE_DAY;
			lptmrTimer->ullValue = (ULONGLONG)ConvertUnit64(
				(DOUBLE)ullValue, 
				UNIT_TYPE_DAY, 
				UNIT_TYPE_MILLISECOND
			);
			break;
		}

		case UNIT_TYPE_HOUR:
		{
			lptmrTimer->utTimeType = UNIT_TYPE_HOUR;
			lptmrTimer->ullValue = (ULONGLONG)ConvertUnit64(
				(DOUBLE)ullValue, 
				UNIT_TYPE_HOUR, 
				UNIT_TYPE_MILLISECOND
			);
			break;
		}

		case UNIT_TYPE_MINUTE: 
		{
			lptmrTimer->utTimeType = UNIT_TYPE_MINUTE;
			lptmrTimer->ullValue = (ULONGLONG)ConvertUnit64(
				(DOUBLE)ullValue, 
				UNIT_TYPE_MINUTE, 
				UNIT_TYPE_MILLISECOND
			);
			break;
		}

		case UNIT_TYPE_SECOND:
		{
			lptmrTimer->utTimeType = UNIT_TYPE_SECOND;
			lptmrTimer->ullValue = (ULONGLONG)ConvertUnit64(
				(DOUBLE)ullValue, 
				UNIT_TYPE_SECOND, 
				UNIT_TYPE_MILLISECOND
			);
			break;
		}

		case UNIT_TYPE_MILLISECOND:
		{
			lptmrTimer->utTimeType = UNIT_TYPE_MILLISECOND;
			lptmrTimer->ullValue = ullValue;
			break;
		}

		case UNIT_TYPE_MICROSECOND:
		{
			lptmrTimer->utTimeType = UNIT_TYPE_MICROSECOND;
			lptmrTimer->ullValue = (ULONGLONG)ConvertUnit64(
				(DOUBLE)ullValue, 
				UNIT_TYPE_MICROSECOND, 
				UNIT_TYPE_MILLISECOND
			);
			break;
		}

		case UNIT_TYPE_NANOSECOND:
		{
			lptmrTimer->utTimeType = UNIT_TYPE_NANOSECOND;
			lptmrTimer->ullValue = (ULONGLONG)ConvertUnit64(
				(DOUBLE)ullValue, 
				UNIT_TYPE_NANOSECOND, 
				UNIT_TYPE_MILLISECOND
			);
			break;
		}

		default:
		{
			FreeMemory(lptmrTimer);
			return NULL_OBJECT;
			break;
		}

	}


	hTimer = CreateHandleWithSingleInheritor(
		lptmrTimer,
		&(lptmrTimer->hThis),
		HANDLE_TYPE_TIMER,
		__DestroyTimer,
		NULL,
		NULLPTR,
		NULL,
		NULLPTR,
		NULL,
		&(lptmrTimer->ullId),
		NULL
	);

	if (NULL_OBJECT == hTimer)
	{
		FreeMemory(lptmrTimer);
		return NULL_OBJECT;
	}

	lptmrTimer->lpfnCallBack = lpfnCallBack;
	lptmrTimer->lpParam = lpParam;

	SetHandleEventWaitFunction(hTimer, __WaitForTimer, NULL);
	lptmrTimer->hThread = CreateThread(__TimerThreadProc, (LPVOID)hTimer, 0);

	if (NULL_OBJECT == lptmrTimer->hThread)
	{
		DestroyObject(hTimer);
		return NULL_OBJECT;
	}

	return hTimer;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
SetTimerValue(
	_In_ 		HANDLE 			hTimer,
	_In_ 		UNIT_TYPE 		utTime,
	_In_ 		ULONGLONG 		ullValue
){
	EXIT_IF_UNLIKELY_NULL(hTimer, FALSE);
	EXIT_IF_UNLIKELY_NULL(ullValue, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hTimer), HANDLE_TYPE_TIMER, FALSE);

	LPTIMER lptmrTimer;
	lptmrTimer = (LPTIMER)GetHandleDerivative(hTimer);


	switch ((ULONG)utTime)
	{

		case UNIT_TYPE_YEAR: 
		{
			lptmrTimer->utTimeType = UNIT_TYPE_YEAR;
			lptmrTimer->ullValue = (ULONGLONG)ConvertUnit64(
				(DOUBLE)ullValue, 
				UNIT_TYPE_YEAR, 
				UNIT_TYPE_MILLISECOND
			);
			break;
		}

		case UNIT_TYPE_MONTH: 
		{
			lptmrTimer->utTimeType = UNIT_TYPE_MONTH;
			lptmrTimer->ullValue = (ULONGLONG)ConvertUnit64(
				(DOUBLE)ullValue, 
				UNIT_TYPE_MONTH, 
				UNIT_TYPE_MILLISECOND
			);
			break;
		}

		case UNIT_TYPE_FORTNIGHT: 
		{
			lptmrTimer->utTimeType = UNIT_TYPE_FORTNIGHT;
			lptmrTimer->ullValue = (ULONGLONG)ConvertUnit64(
				(DOUBLE)ullValue, 
				UNIT_TYPE_FORTNIGHT, 
				UNIT_TYPE_MILLISECOND
			);
			break;
		}

		case UNIT_TYPE_WEEK: 
		{
			lptmrTimer->utTimeType = UNIT_TYPE_WEEK;
			lptmrTimer->ullValue = (ULONGLONG)ConvertUnit64(
				(DOUBLE)ullValue, 
				UNIT_TYPE_WEEK, 
				UNIT_TYPE_MILLISECOND
			);
			break;
		}

		case UNIT_TYPE_DAY: 
		{
			lptmrTimer->utTimeType = UNIT_TYPE_DAY;
			lptmrTimer->ullValue = (ULONGLONG)ConvertUnit64(
				(DOUBLE)ullValue, 
				UNIT_TYPE_DAY, 
				UNIT_TYPE_MILLISECOND
			);
			break;
		}

		case UNIT_TYPE_HOUR:
		{
			lptmrTimer->utTimeType = UNIT_TYPE_HOUR;
			lptmrTimer->ullValue = (ULONGLONG)ConvertUnit64(
				(DOUBLE)ullValue, 
				UNIT_TYPE_HOUR, 
				UNIT_TYPE_MILLISECOND
			);
			break;
		}

		case UNIT_TYPE_MINUTE: 
		{
			lptmrTimer->utTimeType = UNIT_TYPE_MINUTE;
			lptmrTimer->ullValue = (ULONGLONG)ConvertUnit64(
				(DOUBLE)ullValue, 
				UNIT_TYPE_MINUTE, 
				UNIT_TYPE_MILLISECOND
			);
			break;
		}

		case UNIT_TYPE_SECOND:
		{
			lptmrTimer->utTimeType = UNIT_TYPE_SECOND;
			lptmrTimer->ullValue = (ULONGLONG)ConvertUnit64(
				(DOUBLE)ullValue, 
				UNIT_TYPE_SECOND, 
				UNIT_TYPE_MILLISECOND
			);
			break;
		}

		case UNIT_TYPE_MILLISECOND:
		{
			lptmrTimer->utTimeType = UNIT_TYPE_MILLISECOND;
			lptmrTimer->ullValue = ullValue;
			break;
		}

		case UNIT_TYPE_MICROSECOND:
		{
			lptmrTimer->utTimeType = UNIT_TYPE_MICROSECOND;
			lptmrTimer->ullValue = (ULONGLONG)ConvertUnit64(
				(DOUBLE)ullValue, 
				UNIT_TYPE_MICROSECOND, 
				UNIT_TYPE_MILLISECOND
			);
			break;
		}

		case UNIT_TYPE_NANOSECOND:
		{
			lptmrTimer->utTimeType = UNIT_TYPE_NANOSECOND;
			lptmrTimer->ullValue = (ULONGLONG)ConvertUnit64(
				(DOUBLE)ullValue, 
				UNIT_TYPE_NANOSECOND, 
				UNIT_TYPE_MILLISECOND
			);
			break;
		}

		default:
		{
			return FALSE;
			break;
		}

	}

	if (NULL_OBJECT != lptmrTimer->hThread)
	{
		/* Handle created thread here */
	}

	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
ULONGLONG
GetTimerValue(
	_In_ 		HANDLE 			hTimer
){
	EXIT_IF_UNLIKELY_NULL(hTimer, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hTimer), HANDLE_TYPE_TIMER, FALSE);

	LPTIMER lptmrTimer;
	lptmrTimer = (LPTIMER)GetHandleDerivative(hTimer);
	return lptmrTimer->ullValue;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
ResetTimer(
	_In_ 		HANDLE 			hTimer
){
	EXIT_IF_UNLIKELY_NULL(hTimer, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hTimer), HANDLE_TYPE_TIMER, FALSE);

	LPTIMER lptmrTimer;
	lptmrTimer = (LPTIMER)GetHandleDerivative(hTimer);
	
	DestroyObject(lptmrTimer->hThread);
	lptmrTimer->hThread = CreateThread(__TimerThreadProc, (LPVOID)hTimer, 0);

	return (lptmrTimer->hThread != NULL_OBJECT);
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
ResetTimerEx(
	_In_ 		HANDLE 			hTimer,
	_In_ 		UNIT_TYPE 		utTime,
	_In_ 		ULONGLONG 		ullValue 
){
	EXIT_IF_UNLIKELY_NULL(hTimer, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hTimer), HANDLE_TYPE_TIMER, FALSE);

	LPTIMER lptmrTimer;
	lptmrTimer = (LPTIMER)GetHandleDerivative(hTimer);
	
	if (NULL_OBJECT != lptmrTimer->hThread)
	{ DestroyObject(lptmrTimer->hThread); }

	switch ((ULONG)utTime)
	{

		case UNIT_TYPE_YEAR: 
		{
			lptmrTimer->utTimeType = UNIT_TYPE_YEAR;
			lptmrTimer->ullValue = (ULONGLONG)ConvertUnit64(
				(DOUBLE)ullValue, 
				UNIT_TYPE_YEAR, 
				UNIT_TYPE_MILLISECOND
			);
			break;
		}

		case UNIT_TYPE_MONTH: 
		{
			lptmrTimer->utTimeType = UNIT_TYPE_MONTH;
			lptmrTimer->ullValue = (ULONGLONG)ConvertUnit64(
				(DOUBLE)ullValue, 
				UNIT_TYPE_MONTH, 
				UNIT_TYPE_MILLISECOND
			);
			break;
		}

		case UNIT_TYPE_FORTNIGHT: 
		{
			lptmrTimer->utTimeType = UNIT_TYPE_FORTNIGHT;
			lptmrTimer->ullValue = (ULONGLONG)ConvertUnit64(
				(DOUBLE)ullValue, 
				UNIT_TYPE_FORTNIGHT, 
				UNIT_TYPE_MILLISECOND
			);
			break;
		}

		case UNIT_TYPE_WEEK: 
		{
			lptmrTimer->utTimeType = UNIT_TYPE_WEEK;
			lptmrTimer->ullValue = (ULONGLONG)ConvertUnit64(
				(DOUBLE)ullValue, 
				UNIT_TYPE_WEEK, 
				UNIT_TYPE_MILLISECOND
			);
			break;
		}

		case UNIT_TYPE_DAY: 
		{
			lptmrTimer->utTimeType = UNIT_TYPE_DAY;
			lptmrTimer->ullValue = (ULONGLONG)ConvertUnit64(
				(DOUBLE)ullValue, 
				UNIT_TYPE_DAY, 
				UNIT_TYPE_MILLISECOND
			);
			break;
		}

		case UNIT_TYPE_HOUR:
		{
			lptmrTimer->utTimeType = UNIT_TYPE_HOUR;
			lptmrTimer->ullValue = (ULONGLONG)ConvertUnit64(
				(DOUBLE)ullValue, 
				UNIT_TYPE_HOUR, 
				UNIT_TYPE_MILLISECOND
			);
			break;
		}

		case UNIT_TYPE_MINUTE: 
		{
			lptmrTimer->utTimeType = UNIT_TYPE_MINUTE;
			lptmrTimer->ullValue = (ULONGLONG)ConvertUnit64(
				(DOUBLE)ullValue, 
				UNIT_TYPE_MINUTE, 
				UNIT_TYPE_MILLISECOND
			);
			break;
		}

		case UNIT_TYPE_SECOND:
		{
			lptmrTimer->utTimeType = UNIT_TYPE_SECOND;
			lptmrTimer->ullValue = (ULONGLONG)ConvertUnit64(
				(DOUBLE)ullValue, 
				UNIT_TYPE_SECOND, 
				UNIT_TYPE_MILLISECOND
			);
			break;
		}

		case UNIT_TYPE_MILLISECOND:
		{
			lptmrTimer->utTimeType = UNIT_TYPE_MILLISECOND;
			lptmrTimer->ullValue = ullValue;
			break;
		}

		case UNIT_TYPE_MICROSECOND:
		{
			lptmrTimer->utTimeType = UNIT_TYPE_MICROSECOND;
			lptmrTimer->ullValue = (ULONGLONG)ConvertUnit64(
				(DOUBLE)ullValue, 
				UNIT_TYPE_MICROSECOND, 
				UNIT_TYPE_MILLISECOND
			);
			break;
		}

		case UNIT_TYPE_NANOSECOND:
		{
			lptmrTimer->utTimeType = UNIT_TYPE_NANOSECOND;
			lptmrTimer->ullValue = (ULONGLONG)ConvertUnit64(
				(DOUBLE)ullValue, 
				UNIT_TYPE_NANOSECOND, 
				UNIT_TYPE_MILLISECOND
			);
			break;
		}

		default:
		{
			return FALSE;
			break;
		}

	}

	lptmrTimer->hThread = CreateThread(__TimerThreadProc, (LPVOID)hTimer, 0);

	return TRUE;
}




_Success_(return != MAX_ULONGLONG, _Non_Locking_)
PODNET_API
ULONGLONG
GetTimerRemaining(
	_In_ 		HANDLE 			hTimer
){
	EXIT_IF_UNLIKELY_NULL(hTimer, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hTimer), HANDLE_TYPE_TIMER, FALSE);

	LPTIMER lptmrTimer;
	lptmrTimer = (LPTIMER)GetHandleDerivative(hTimer);

	return lptmrTimer->ullFinishStamp - time(NULLPTR);
}