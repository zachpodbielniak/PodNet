/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CLINKEDLIST_H
#define CLINKEDLIST_H


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../CHandle/CHandle.h"



typedef LPVOID                  HLINKEDLIST, *LPHLINKEDLIST;



typedef _Call_Back_ BOOL (* LPFN_LINKED_LIST_CLOSE_PROC)(
        _In_                    HLINKEDLIST     hThisList,
        _In_                    LPVOID          lpData    
);




_Success_(return != NULLPTR, ...)
PODNET_API
HLINKEDLIST
CreateLinkedList(
        _In_                    ULONGLONG       ullObjectSize,
        _In_                    BOOL            bIsDoubleLinkedList,
        _In_Opt_                HANDLE          hLock
);




_Success_(return != FALSE, ...)
PODNET_API
BOOL
DestroyLinkedList(
        _In_                    HLINKEDLIST     hllList
);




_Calls_Call_Back_
_Success_(return != FALSE, ...)
PODNET_API
BOOL
DestroyLinkedListEx(
        _In_                    HLINKEDLIST                     hllList,
        _In_ _Call_Back_        LPFN_LINKED_LIST_CLOSE_PROC     lpfnllClose
);




_Return_May_Be_Null_
PODNET_API
LPVOID
LinkedListFront(
        _In_                    HLINKEDLIST     hllList
);




_Return_May_Be_Null_
PODNET_API
LPVOID
LinkedListBack(
        _In_                    HLINKEDLIST     hllList
);




_Return_May_Be_Null_
PODNET_API
LPVOID
LinkedListTraverse(
        _In_                    HLINKEDLIST     hllList,
        _In_                    LPVOID          lpPrevTraversalPoint
);




_Return_May_Be_Null_
PODNET_API
LPVOID
LinkedListAtNode(
        _In_                    HLINKEDLIST     hllList,
        _In_                    LPVOID          lpNode
);




_Success_(return != FALSE, ...)
PODNET_API
BOOL
LinkedListAttachFront(
        _In_                    HLINKEDLIST     hllList,
        _In_                    LPVOID          lpData
);




_Return_May_Be_Null_
PODNET_API
LPVOID
LinkedListDetachFront(
        _In_                    HLINKEDLIST     hllList
);




_Success_(return != FALSE, ...)
PODNET_API
BOOL
LinkedListAttachBack(
        _In_                    HLINKEDLIST     hllList,
        _In_                    LPVOID          lpData
);




_Return_May_Be_Null_
PODNET_API
LPVOID
LinkedListDetachBack(
        _In_                    HLINKEDLIST     hllList
);




_Success_(return != FALSE, ...)
PODNET_API
BOOL
LinkedListInsert(
        _In_                    HLINKEDLIST     hllList,
        _In_                    LPVOID          lpData,
        _In_                    ULONGLONG       ullPosition      
);




_Return_May_Be_Null_
PODNET_API
LPVOID
LinkedListRemove(
        _In_                    HLINKEDLIST     hllList,
        _In_                    ULONGLONG       ullPosition
);




_Return_May_Be_Null_
PODNET_API
LPVOID
LinkedListRemoveAtNode(
        _In_                    HLINKEDLIST     hllList,
        _In_                    LPVOID          lpNode,
        _In_                    BOOL            bFreeData
);




_Success_(return != NULLPTR, ...)
PODNET_API
LPVOID
LinkedListRemoveItemWithSpecificData(
        _In_                    HLINKEDLIST     hllList,
        _In_                    LPVOID          lpDataToCompare
);




_Success_(return != FALSE, ...)
PODNET_API
BOOL
LinkedListErase(
        _In_                    HLINKEDLIST     hllList
);




_Success_(return != FALSE, ...)
PODNET_API
BOOL
LinkedListSwap(
        _In_                    HLINKEDLIST     hllListSwapee,
        _In_                    HLINKEDLIST     hllListSwaperand
);




_Return_May_Be_Null_
PODNET_API
HLINKEDLIST
LinkedListCopy(
        _In_                    HLINKEDLIST     hllList
);




_Success_(return != FALSE, ...)
PODNET_API
BOOL
LinkedListIsEmpty(
        _In_                    HLINKEDLIST     hllList
);




_Success_(return != MAX_ULONGLONG, ...)
PODNET_API
ULONGLONG
LinkedListSize(
        _In_                    HLINKEDLIST     hllList
);




_Success_(return != FALSE, ...)
PODNET_API
BOOL
LinkedListSplice(
        _In_                    HLINKEDLIST     hllListMain,
        _In_                    HLINKEDLIST     hllListSplicerand,
        _In_                    ULONGLONG       ullPosition
);




_Success_(return != FALSE, ...)
PODNET_API
BOOL
LinkedListIsUnique(
        _In_                    HLINKEDLIST     hllList
);




_Success_(return != FALSE, ...)
PODNET_API
BOOL
LinkedListMerge(
        _In_                    LPHLINKEDLIST   lphllList,
        _In_                    ULONG           ulNumberOfLists
);




_Success_(return != FALSE, ...)
PODNET_API
BOOL
LinkedListReverse(
        _In_                    HLINKEDLIST     hllList
);





#endif