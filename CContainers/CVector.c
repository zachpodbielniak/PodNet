/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CVector.h"

#define VALID_VECTOR_HANDLE 0xAA
#define CHECK_FOR_VALID_VECTOR_HANDLE(hVector) \
	if (hVector == NULL || \
	  	((((LPVECTOR)hVector)->bValidHandle ^ VALID_VECTOR_HANDLE) != 0)) \
	  	return FALSE;




typedef struct _VECTOR
{
	BYTE 		bValidHandle;
  	BOOL 		bInUse;
  	UARCHLONG 	ualObjectSize;
  	UARCHLONG 	ualIndex;
  	UARCHLONG 	ualMax;
  	LPVOID 		lpAddress;
} VECTOR, *LPVECTOR;




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL 
__ExpandVectorSize(
	_In_		HVECTOR 	hVector, 
	_In_		UARCHLONG 	ualSize
){
  	CHECK_FOR_VALID_VECTOR_HANDLE(hVector);
  	if (ualSize == 0)
    		return FALSE;
  	LPVECTOR lpVector;
  	lpVector = (LPVECTOR)hVector;
  	LPVOID lpReAlloc;
  	lpReAlloc = (LPVOID)LocalReAlloc(lpVector->lpAddress,
                (lpVector->ualMax * lpVector->ualObjectSize) + (ualSize * lpVector->ualObjectSize));
  	if (!lpReAlloc)
    		return FALSE;

  	lpVector->lpAddress = (LPVOID)lpReAlloc;
  	lpVector->ualMax += ualSize;
  	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL 
__ShrinkVectorSize(
	_In_		HVECTOR 	hVector, 
	_In_		ULONG 		ualSize
){
  	CHECK_FOR_VALID_VECTOR_HANDLE(hVector);
  	if (ualSize == 0)
    		return FALSE;
  	LPVECTOR lpVector; /*lpReAlloc;*/
	LPVOID lpReAlloc;
  	lpVector = (LPVECTOR) hVector;
  	if (lpVector->ualMax - ualSize < lpVector->ualIndex)
    		return FALSE;
  	lpReAlloc = (LPVOID)LocalReAlloc(lpVector->lpAddress,
                (lpVector->ualMax * lpVector->ualObjectSize) - (ualSize * lpVector->ualObjectSize));
  	if(!lpReAlloc)
    		return FALSE;
  	FreeMemory(lpVector->lpAddress);
  	lpVector->lpAddress = (LPVOID)lpReAlloc;
  	return TRUE;
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HVECTOR 
CreateVector(
	_In_		UARCHLONG	ualArrayAmount, 
	_In_		UARCHLONG	ualSizeOfObject, 
	_Reserved_	ULONG		ulFlags
){
	UNREFERENCED_PARAMETER(ulFlags);
	if (ualArrayAmount == 0 || ualSizeOfObject == 0)
		return NULL;
	LPVECTOR lpVector;
	lpVector = (LPVECTOR)LocalAllocAndZero(sizeof(VECTOR));
	if (!lpVector)
		return NULL;
	lpVector->bValidHandle = VALID_VECTOR_HANDLE;
	lpVector->bInUse = TRUE;
	lpVector->ualIndex = 0;
	lpVector->ualMax = ualArrayAmount;
	lpVector->ualObjectSize = ualSizeOfObject;
	lpVector->lpAddress = (LPVOID)LocalAllocAndZero(ualSizeOfObject * ualArrayAmount);
	if (NULLPTR == lpVector->lpAddress)
	{
		FreeMemory(lpVector);
		return NULLPTR;
	}
	/* Add Registration Flag code handling here. */
	return (HVECTOR)lpVector;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL 
CloseVector(
	_In_		HVECTOR 	hVector
){
	CHECK_FOR_VALID_VECTOR_HANDLE(hVector);
	BOOL bRet;
	bRet = TRUE;
	FreeMemory(((LPVECTOR)hVector)->lpAddress);
	FreeMemory(((LPVECTOR)hVector));
	return bRet;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL 
DestroyVector(
	_In_		HVECTOR 	hVector
){
	EXIT_IF_UNLIKELY_NULL(hVector, FALSE);
	CHECK_FOR_VALID_VECTOR_HANDLE(hVector);
	BOOL bRet;
	bRet = TRUE;
	FreeMemory(((LPVECTOR)hVector)->lpAddress);
	FreeMemory(((LPVECTOR)hVector));
	return bRet;
}




_Success_(return != MAX_ULONG, _Non_Locking_)
PODNET_API
ULONG 
VectorPushBack(
	_In_		HVECTOR RESTRICT 	hVector, 
	_In_		LPVOID RESTRICT		lpData, 
	_In_Opt_	ULONG 			ulFlags
){
	UNREFERENCED_PARAMETER(ulFlags);
	CHECK_FOR_VALID_VECTOR_HANDLE(hVector);
	if (!lpData)
		return 0;
	LPVECTOR lpVector;
	UARCHLONG ualOffset;
	lpVector = (LPVECTOR)hVector;
	if (lpVector->ualIndex >= lpVector->ualMax)
		__ExpandVectorSize(hVector, lpVector->ualMax * 2);
	ualOffset = (UARCHLONG)lpVector->lpAddress;
	ualOffset += ((lpVector->ualIndex) * ((lpVector)->ualObjectSize));
	
	/* TODO: Add flag code here! */
	CopyMemory((LPVOID)ualOffset, lpData, lpVector->ualObjectSize);
	lpVector->ualIndex++;
    	return lpVector->ualIndex;
}




_Return_May_Be_Null_
PODNET_API
LPVOID 
VectorAt(
	_In_		HVECTOR 	hVector,
	_In_ 		UARCHLONG 	ualIndex
){
	CHECK_FOR_VALID_VECTOR_HANDLE(hVector);
	LPVECTOR lpVector;
	UARCHLONG ualAccess;
	lpVector = (LPVECTOR)hVector;
	ualAccess = (UARCHLONG)lpVector->lpAddress;
	ualAccess += (UARCHLONG)(lpVector->ualObjectSize * ualIndex);
	return (LPVOID)ualAccess;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL 
VectorPopBack(
	_In_		HVECTOR 	hVector, 
	_In_		LPVOID 		lpOut
){
	CHECK_FOR_VALID_VECTOR_HANDLE(hVector);
	if (!lpOut)
		return 0;
	LPVECTOR lpVector;
	UARCHLONG ualOffset;
	lpVector = hVector;
	if (lpVector->ualIndex == 0)
		return 0;

	
	ualOffset = (UARCHLONG)lpVector->lpAddress;
	ualOffset += (UARCHLONG)(lpVector->ualIndex * lpVector->ualObjectSize - lpVector->ualObjectSize);
	
	
	CopyMemory(lpOut, (LPVOID)ualOffset, lpVector->ualObjectSize);
	if (lpVector->ualIndex != 0)
	{ lpVector->ualIndex--; }
	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL 
VectorShrinkToFit(
	_In_		HVECTOR 	hVector
){ return __ShrinkVectorSize(hVector, ((LPVECTOR)hVector)->ualMax - ((LPVECTOR)hVector)->ualIndex); }




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
VectorClear(
    _In_ 		HVECTOR 	hVector
){
	CHECK_FOR_VALID_VECTOR_HANDLE(hVector);
	LPVECTOR lpVector;
	lpVector = (LPVECTOR)hVector;
	ZeroMemory(lpVector->lpAddress, lpVector->ualObjectSize * lpVector->ualMax);
	lpVector->ualIndex = 0;
	return TRUE;
}




_Success_(return != MAX_ULONG, _Non_Locking_)
PODNET_API
UARCHLONG 
VectorSize(
	_In_		HVECTOR 	hVector
){
        CHECK_FOR_VALID_VECTOR_HANDLE(hVector);
        return ((LPVECTOR)hVector)->ualIndex;
}




_Success_(return != 0, _Non_Locking_)
PODNET_API
UARCHLONG 
VectorCapacity(
	_In_		HVECTOR 	hVector
){
    	CHECK_FOR_VALID_VECTOR_HANDLE(hVector);
    	return ((LPVECTOR)hVector)->ualMax;
}




_Success_(return != 0, _Non_Locking_)
PODNET_API
UARCHLONG 
VectorObjectSize(
	_In_		HVECTOR 	hVector
){
    	CHECK_FOR_VALID_VECTOR_HANDLE(hVector);
    	return ((LPVECTOR)hVector)->ualObjectSize;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL 
VectorResize(
	_In_		HVECTOR 	hVector, 
	_In_		UARCHLONG	ualSize
){
    	CHECK_FOR_VALID_VECTOR_HANDLE(hVector);
    	if (ualSize == 0)
    	    	return FALSE;
    	LPVECTOR lpVector;
    	BOOLEAN bRet;
    	lpVector = (LPVECTOR)hVector;
    	bRet = TRUE;
    	if (lpVector->ualMax < ualSize)
    		bRet &= __ExpandVectorSize(hVector, lpVector->ualMax + ualSize);
    	else if (lpVector->ualIndex <= ualSize)
    		bRet &= __ShrinkVectorSize(hVector, lpVector->ualMax - ualSize);
    	return bRet;
}




_Return_May_Be_Null_
PODNET_API
LPVOID 
VectorFront(
	_In_ 		HVECTOR 	hVector
){
    	CHECK_FOR_VALID_VECTOR_HANDLE(hVector);
    	return ((LPVECTOR)hVector)->lpAddress;
}




_Return_May_Be_Null_
PODNET_API
LPVOID 
VectorBack(
	_In_		HVECTOR 	hVector
){
    	CHECK_FOR_VALID_VECTOR_HANDLE(hVector);
    	LPVECTOR lpVector;
    	UARCHLONG ualOffset;
    	lpVector = (LPVECTOR)hVector;
    	ualOffset = (UARCHLONG)lpVector->lpAddress;
    	ualOffset += (UARCHLONG)(lpVector->ualObjectSize * lpVector->ualIndex - (lpVector->ualObjectSize));
    	return (LPVOID)ualOffset;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
IsValidVectorHandle(
    _In_        HVECTOR     hVector
){
	CHECK_FOR_VALID_VECTOR_HANDLE(hVector);
	return TRUE;
}




























