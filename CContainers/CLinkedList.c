/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CLinkedList.h"


#define VALID_LIST_HANDLE               0xBA


typedef struct __SINGLE_NODE    SINGLE_NODE, *LPSINGLE_NODE;
struct __SINGLE_NODE
{
        LPVOID                  lpData;
        LPSINGLE_NODE           lpsnNext;
};


typedef struct __DOUBLE_NODE    DOUBLE_NODE, *LPDOUBLE_NODE;
struct __DOUBLE_NODE
{
        LPVOID                  lpData;
        LPDOUBLE_NODE           lpdnPrevious;
        LPDOUBLE_NODE           lpdnNext;
};



typedef struct __LINKED_LIST    LINKED_LIST, *LPLINKED_LIST, **DLPLINKED_LIST;
struct __LINKED_LIST
{
        BYTE                    bValidListHandle;
        BOOL                    bIsDoubleLinkedList;
        HANDLE                  hLock;
        ULONGLONG               ullObjectSize;
        ULONGLONG               ullNumberOfElements;      
        LPVOID                  lpFirst;
        LPVOID                  lpLast;
};



#define __TRAVERSE_SINGLE(LIST, NODE)    \
        for (NODE = (LPSINGLE_NODE)LIST->lpFirst; NODE != NULLPTR; NODE = NODE->lpsnNext)


#define __TRAVERSE_DOUBLE_FORWARD(LIST, NODE)     \
        for (NODE = (LPDOUBLE_NODE)LIST->lpFirst; NODE != NULLPTR; NODE = NODE->lpdnNext)


#define __TRAVERSE_DOUBLE_BACKWARD(LIST, NODE)    \
        for (NODE = (LPDOUBLE_NODE)LIST->lpLast; NODE != NULLPTR; NODE = NODE->lpdnPrevious)


_Success_(return != NULLPTR, ...)
PODNET_API
HLINKEDLIST
CreateLinkedList(
        _In_                    ULONGLONG       ullObjectSize,
        _In_                    BOOL            bIsDoubleLinkedList,
        _In_Opt_                HANDLE          hLock
){
        LPLINKED_LIST lpllList;

        lpllList = LocalAllocAndZero(sizeof(LINKED_LIST));
	/* cppcheck-suppress memleak */
	EXIT_IF_UNLIKELY_NULL(lpllList, NULLPTR);
	
        lpllList->bValidListHandle = VALID_LIST_HANDLE;
        lpllList->bIsDoubleLinkedList = bIsDoubleLinkedList;
        lpllList->hLock = hLock;
        lpllList->ullObjectSize = ullObjectSize;
        return (HLINKEDLIST)lpllList;
}




_Success_(return != FALSE, ...)
PODNET_API
BOOL
DestroyLinkedList(
        _In_                    HLINKEDLIST     hllList
){
        LPLINKED_LIST lpllList;
        lpllList = (LPLINKED_LIST)hllList;

        if (NULLPTR != lpllList->hLock) 
        { WaitForSingleObject(lpllList->hLock, INFINITE_WAIT_TIME); }

        switch (lpllList->bIsDoubleLinkedList)
        {

                case TRUE:
                {
                        LPDOUBLE_NODE lpdnNode;
                        LPVOID lpPrev;
                        lpPrev = NULLPTR;

                        __TRAVERSE_DOUBLE_FORWARD(lpllList, lpdnNode)
                        {       
                                FreeMemory(lpdnNode->lpdnPrevious);
                                FreeMemory(lpdnNode->lpData);
                        }
                        
                        FreeMemory(lpPrev);
                        break;
                }

                case FALSE:
                {
                        LPSINGLE_NODE lpsnNode;
                        LPVOID lpPrev;
                        lpPrev = NULLPTR;

                        __TRAVERSE_SINGLE(lpllList, lpsnNode)
                        {       
                                if (lpPrev != NULLPTR)
                                        FreeMemory(lpPrev);       
                                FreeMemory(lpsnNode->lpData);
                                lpPrev = lpsnNode;
                        }

                        FreeMemory(lpPrev);
                        break;
                }

        }

        FreeMemory(lpllList);

        if (NULLPTR != lpllList->hLock) 
        { ReleaseSingleObject(lpllList->hLock); }

        return TRUE;
}




_Calls_Call_Back_
_Success_(return != FALSE, ...)
PODNET_API
BOOL
DestroyLinkedListEx(
        _In_                    HLINKEDLIST                     hllList,
        _In_ _Call_Back_        LPFN_LINKED_LIST_CLOSE_PROC     lpfnllClose
){
        LPLINKED_LIST lpllList;
        HANDLE hLock;
        lpllList = (LPLINKED_LIST)hllList;
        hLock = lpllList->hLock;

        if (NULLPTR != hLock)
        { WaitForSingleObject(hLock, INFINITE_WAIT_TIME); }

        switch (lpllList->bIsDoubleLinkedList)
        {
                case TRUE:
                {
                        LPDOUBLE_NODE lpdnTraverse;
                        
                        for (
                                lpdnTraverse = (LPDOUBLE_NODE)lpllList->lpFirst;
                                lpdnTraverse != NULLPTR;
                                lpdnTraverse = lpdnTraverse->lpdnNext
                        ){ 
                                ZeroMemory(lpdnTraverse->lpdnPrevious, sizeof(DOUBLE_NODE));
                                FreeMemory(lpdnTraverse->lpdnPrevious);
                                lpfnllClose(hllList, lpdnTraverse->lpData); 
                        }

                        ZeroMemory(lpllList->lpLast, sizeof(DOUBLE_NODE));
                        FreeMemory(lpllList->lpLast);

                        break;
                }
                case FALSE:
                {
                        LPSINGLE_NODE lpsnTraverse, lpsnPrevious;	
	
                        for (
                                lpsnTraverse = (LPSINGLE_NODE)lpllList->lpFirst, lpsnPrevious = NULLPTR;
                                lpsnTraverse != NULLPTR;
                                lpsnPrevious = lpsnTraverse, lpsnTraverse = lpsnTraverse->lpsnNext
                        ){ 
                                ZeroMemory(lpsnPrevious, sizeof(SINGLE_NODE));
                                if (NULLPTR != lpsnPrevious)
				{ FreeMemory(lpsnPrevious); }
                                lpfnllClose(hllList, lpsnTraverse->lpData); 
                        }

                        ZeroMemory(lpllList->lpLast, sizeof(SINGLE_NODE));
                        FreeMemory(lpllList->lpLast);

                        break;
                }
        }

        ZeroMemory(lpllList, sizeof(LINKED_LIST));
        FreeMemory(lpllList);

        if (NULLPTR != hLock)
        { ReleaseSingleObject(hLock); }

        return TRUE;
}




_Return_May_Be_Null_
PODNET_API
LPVOID
LinkedListFront(
        _In_                    HLINKEDLIST     hllList
){
        LPLINKED_LIST lpllList;
        LPVOID lpRet;
        lpllList = (LPLINKED_LIST)hllList;
        lpRet = NULLPTR;

        switch (lpllList->bIsDoubleLinkedList)
        {

                case TRUE:
                {
                        LPDOUBLE_NODE lpdnNode;
                        lpdnNode = (LPDOUBLE_NODE)lpllList->lpFirst;
                        lpRet = lpdnNode->lpData;
                        break;
                }

                case FALSE:
                {
                        LPSINGLE_NODE lpsnNode;
                        lpsnNode = (LPSINGLE_NODE)lpllList->lpFirst;
                        lpRet = lpsnNode->lpData;
                        break;
                }

        }

        return lpRet;
}




_Return_May_Be_Null_
PODNET_API
LPVOID
LinkedListBack(
        _In_                    HLINKEDLIST     hllList
){
        LPLINKED_LIST lpllList;
        LPVOID lpRet;
        lpllList = (LPLINKED_LIST)hllList;
        lpRet = NULLPTR;

        switch (lpllList->bIsDoubleLinkedList)
        {

                case TRUE:
                {
                        LPDOUBLE_NODE lpdnNode;
                        lpdnNode = (LPDOUBLE_NODE)lpllList->lpLast;
                        lpRet = lpdnNode->lpData;
                        break;
                }

                case FALSE:
                {
                        LPSINGLE_NODE lpsnNode;
                        lpsnNode = (LPSINGLE_NODE)lpllList->lpLast;
                        lpRet = lpsnNode->lpData;
                        break;
                }

        }

        return lpRet;
}




_Return_May_Be_Null_
PODNET_API
LPVOID
LinkedListTraverse(
        _In_                    HLINKEDLIST     hllList,
        _In_                    LPVOID          lpPrevTraversalPoint
){
        LPLINKED_LIST lpllList;
        LPVOID lpRet;
        lpllList = (LPLINKED_LIST)hllList;
        lpRet = NULLPTR;

        switch (lpllList->bIsDoubleLinkedList)
        {

                case TRUE:
                {
                        LPDOUBLE_NODE lpdnNode;
                        if (NULLPTR == lpPrevTraversalPoint)
                        { lpdnNode = (LPDOUBLE_NODE)lpllList->lpFirst; }
                        else
                        { 
				lpdnNode = (LPDOUBLE_NODE)lpPrevTraversalPoint;
				lpdnNode = lpdnNode->lpdnNext;
			}
 
			lpRet = (LPVOID)lpdnNode;
                        break;
                }

                case FALSE:
                {
                        LPSINGLE_NODE lpsnNode;
                        if (NULLPTR == lpPrevTraversalPoint)
                        { lpsnNode = (LPSINGLE_NODE)lpllList->lpFirst; }
                        else
                        { 
				lpsnNode = (LPSINGLE_NODE)lpPrevTraversalPoint; 
				lpsnNode = lpsnNode->lpsnNext;
			}

			lpRet = (LPVOID)lpsnNode;
                        break;
                }

        }

        return lpRet;
}




_Return_May_Be_Null_
PODNET_API
LPVOID
LinkedListAtNode(
        _In_                    HLINKEDLIST     hllList,
        _In_                    LPVOID          lpNode
){
        LPLINKED_LIST lpllList;
        LPVOID lpRet;
        lpllList = (LPLINKED_LIST)hllList;
        lpRet = NULLPTR;

        switch (lpllList->bIsDoubleLinkedList)
        {

                case TRUE:
                {
                        LPDOUBLE_NODE lpdnNode;
                        if (NULLPTR == lpNode)
                        { lpdnNode = (LPDOUBLE_NODE)lpllList->lpFirst; }
                        else
                        { lpdnNode = (LPDOUBLE_NODE)lpNode; }
                        lpRet = lpdnNode->lpData;
                        break;
                }

                case FALSE:
                {
                        LPSINGLE_NODE lpsnNode;
                        if (NULLPTR == lpNode)
                        { lpsnNode = (LPSINGLE_NODE)lpllList->lpFirst; }
                        else
                        { lpsnNode = (LPSINGLE_NODE)lpNode; }
                        lpRet = lpsnNode->lpData;
                        break;
                }

        }

        return lpRet;
}




_Success_(return != FALSE, ...)
PODNET_API
BOOL
LinkedListAttachFront(
        _In_                    HLINKEDLIST     hllList,
        _In_                    LPVOID          lpData
){
        LPLINKED_LIST lpllList;
        lpllList = (LPLINKED_LIST)hllList;

        switch (lpllList->bIsDoubleLinkedList)
        {

                case TRUE:
                {
                        LPDOUBLE_NODE lpdnNew;
                        lpdnNew = LocalAlloc(sizeof(DOUBLE_NODE));
			EXIT_IF_UNLIKELY_NULL(lpdnNew, FALSE);
                        lpdnNew->lpdnPrevious = NULLPTR;
                        lpdnNew->lpdnNext = (LPDOUBLE_NODE)lpllList->lpFirst;
                        lpdnNew->lpdnNext->lpdnPrevious = lpdnNew;
                        lpdnNew->lpData = LocalAlloc(lpllList->ullObjectSize);
			if (NULLPTR == lpdnNew->lpData)
			{
				FreeMemory(lpdnNew);
				return FALSE;
			}
                        CopyMemory(lpdnNew->lpData, lpData, lpllList->ullObjectSize);
			lpllList->ullNumberOfElements++;
                        break;
                }
                
                case FALSE:
                {
                        LPSINGLE_NODE lpsnNew;
                        lpsnNew = LocalAlloc(sizeof(SINGLE_NODE));
			EXIT_IF_UNLIKELY_NULL(lpsnNew, FALSE);
                        lpsnNew->lpsnNext = (LPSINGLE_NODE)lpllList->lpFirst;
                        lpllList->lpFirst = lpsnNew;
                        lpsnNew->lpData = LocalAlloc(lpllList->ullObjectSize);
			if (NULLPTR == lpsnNew->lpData)
			{
				FreeMemory(lpsnNew);
				return FALSE;
			}
                        CopyMemory(lpsnNew->lpData, lpData, lpllList->ullObjectSize);
			lpllList->ullNumberOfElements++;
                        break;
                }

        }

        return TRUE;
}




_Return_May_Be_Null_
PODNET_API
LPVOID
LinkedListDetachFront(
        _In_                    HLINKEDLIST     hllList
){
        LPLINKED_LIST lpllList;
        LPVOID lpRet;
        lpllList = (LPLINKED_LIST)hllList;
	lpRet = NULLPTR;
	
	if (NULLPTR == lpllList->lpFirst)
	{ return NULLPTR; }

        switch (lpllList->bIsDoubleLinkedList)
        {

                case TRUE:
                {
                        LPDOUBLE_NODE lpdnMain;
                        lpdnMain = (LPDOUBLE_NODE)lpllList->lpFirst;
                        lpRet = lpdnMain->lpData;
                        lpdnMain->lpdnNext->lpdnPrevious = NULLPTR;
                        lpllList->lpFirst = lpdnMain->lpdnNext;
			lpllList->ullNumberOfElements--;
                        FreeMemory(lpdnMain);
                        break;
                }

                case FALSE:
                {
                        LPSINGLE_NODE lpsnMain;
                        lpsnMain = (LPSINGLE_NODE)lpllList->lpFirst;
                        lpRet = lpsnMain->lpData;
                        lpllList->lpFirst = lpsnMain->lpsnNext;
			lpllList->ullNumberOfElements--;
                        FreeMemory(lpsnMain);
                        break;
                }
        
        }

        return lpRet;
}




_Success_(return != FALSE, ...)
PODNET_API
BOOL
LinkedListAttachBack(
        _In_                    HLINKEDLIST     hllList,
        _In_                    LPVOID          lpData
){
        LPLINKED_LIST lpllList;
        lpllList = (LPLINKED_LIST)hllList;

        if (NULLPTR != lpllList->hLock) WaitForSingleObject(lpllList->hLock, INFINITE_WAIT_TIME);

        switch (lpllList->bIsDoubleLinkedList)
        {
                case TRUE:
                {
                        LPDOUBLE_NODE lpdnNew;
                        lpdnNew = LocalAlloc(sizeof(DOUBLE_NODE));
			if (NULLPTR == lpdnNew)
			{
        			if (NULLPTR != lpllList->hLock) 
				{ ReleaseSingleObject(lpllList->hLock); }
				return FALSE;
			}

                        lpdnNew->lpdnNext = NULLPTR;
                        lpllList->lpLast = lpdnNew;

                        if (0 == lpllList->ullNumberOfElements)
                        {
                                lpllList->lpFirst = lpdnNew;
                                lpdnNew->lpdnPrevious = NULLPTR;
                        }
                        else
                        { lpdnNew->lpdnPrevious = (LPDOUBLE_NODE)lpllList->lpLast; }

                        lpdnNew->lpData = LocalAlloc(lpllList->ullObjectSize);
			if (NULLPTR == lpdnNew->lpData)
			{
        			if (NULLPTR != lpllList->hLock) 
				{ ReleaseSingleObject(lpllList->hLock); }
				FreeMemory(lpdnNew);
				return FALSE;
			}

                        CopyMemory(lpdnNew->lpData, lpData, lpllList->ullObjectSize);
                        lpllList->ullNumberOfElements++;
                        break;
                }
                case FALSE:
                {
                        LPSINGLE_NODE lpsnNew;
                        lpsnNew = LocalAlloc(sizeof(SINGLE_NODE));
			if (NULLPTR == lpsnNew)
			{
        			if (NULLPTR != lpllList->hLock) 
				{ ReleaseSingleObject(lpllList->hLock); }
				return FALSE;
			}

                        lpsnNew->lpsnNext = NULLPTR;

                        if (0 == lpllList->ullNumberOfElements)
                        {
                                lpllList->lpFirst = lpsnNew;
                                lpllList->lpLast = lpsnNew;
                        }
                        else
                        { ((LPSINGLE_NODE)lpllList->lpLast)->lpsnNext = lpsnNew; }

			lpllList->lpLast = lpsnNew;
                        lpsnNew->lpData = LocalAlloc(lpllList->ullObjectSize);
			if (NULLPTR == lpsnNew->lpData)
			{
        			if (NULLPTR != lpllList->hLock) 
				{ ReleaseSingleObject(lpllList->hLock); }
				FreeMemory(lpsnNew);
				return FALSE;
			}

                        CopyMemory(lpsnNew->lpData, lpData, lpllList->ullObjectSize);
                        lpllList->ullNumberOfElements++;
                        break;
                }
        }

        if (NULLPTR != lpllList->hLock) ReleaseSingleObject(lpllList->hLock);
        return TRUE;
}




_Return_May_Be_Null_
PODNET_API
LPVOID
LinkedListDetachBack(
        _In_                    HLINKEDLIST     hllList
){
        LPLINKED_LIST lpllList;
        LPVOID lpRet;
        lpllList = (LPLINKED_LIST)hllList;
	lpRet = NULLPTR;

        if (NULLPTR != lpllList->hLock) 
        { WaitForSingleObject(lpllList->hLock, INFINITE_WAIT_TIME); }

        if (lpllList->ullNumberOfElements == 0) return NULLPTR;

        switch (lpllList->bIsDoubleLinkedList)
        {
                case TRUE:
                {
                        LPDOUBLE_NODE lpdnLast;
                        lpdnLast = (LPDOUBLE_NODE)lpllList->lpLast;
                        lpRet = lpdnLast->lpData;
                        
                        if (lpllList->ullNumberOfElements == 1)
                        {
                                lpllList->lpFirst = NULLPTR;
                                lpllList->lpLast = NULLPTR;
                        }
                        else
                        { lpdnLast->lpdnPrevious->lpdnNext = NULLPTR; }

			lpllList->ullNumberOfElements--;
                        FreeMemory(lpdnLast);
                        break;
                }
                case FALSE:
                {
                        LPSINGLE_NODE lpsnTraverse;
                        lpRet = ((LPSINGLE_NODE)lpllList->lpLast)->lpData;

                        if (1 == lpllList->ullNumberOfElements)
                        {
                                FreeMemory(lpllList->lpLast);
                                lpllList->lpFirst = NULLPTR;
                                lpllList->lpLast = NULLPTR;
                        }
                        else
                        {
                                for (
                                     lpsnTraverse = (LPSINGLE_NODE)lpllList->lpFirst;
                                     lpsnTraverse->lpsnNext != (LPSINGLE_NODE)lpllList->lpLast;
                                     lpsnTraverse = lpsnTraverse->lpsnNext   
                                ){ NO_OPERATION(); }

                                FreeMemory(lpllList->lpLast);
                                lpllList->lpLast = (LPVOID)lpsnTraverse;
                        }
			lpllList->ullNumberOfElements--;
                        break;
                }
        }
        lpllList->ullNumberOfElements--;

        if (NULLPTR != lpllList->hLock) 
        { ReleaseSingleObject(lpllList->hLock); }

        return lpRet;
}




_Success_(return != FALSE, ...)
PODNET_API
BOOL
LinkedListInsert(
        _In_                    HLINKEDLIST     hllList,
        _In_                    LPVOID          lpData,
        _In_                    ULONGLONG       ullPosition      
){
        LPLINKED_LIST lpllList;
        lpllList = (LPLINKED_LIST)hllList;
        if (lpllList->ullNumberOfElements < ullPosition || 0 == lpllList->ullNumberOfElements) return FALSE;

        
        if (ullPosition == lpllList->ullNumberOfElements)
        { return LinkedListAttachBack(hllList, lpData); }

        if (0 == ullPosition)
        { return LinkedListAttachFront(hllList, lpData); }
        

        if (NULLPTR != lpllList->hLock) 
        { WaitForSingleObject(lpllList->hLock, INFINITE_WAIT_TIME); }
        

        switch (lpllList->bIsDoubleLinkedList)
        {
                case TRUE:
                {
                        LPDOUBLE_NODE lpdnTraversal, lpdnTemp;
                        ULONGLONG ullIndex;

                        for (
                                ullIndex = 0, lpdnTraversal = (LPDOUBLE_NODE)lpllList->lpFirst;
                                ullIndex != ullPosition;
                                ullIndex++, lpdnTraversal = lpdnTraversal->lpdnNext
                        ){ NO_OPERATION(); }

                        lpdnTemp = lpdnTraversal->lpdnNext;
                        lpdnTraversal->lpdnNext = LocalAlloc(sizeof(LPDOUBLE_NODE));
			if (NULLPTR == lpdnTraversal->lpdnNext)
			{
				if (NULLPTR != lpllList->hLock)
				{ ReleaseSingleObject(lpllList->hLock); }
				return FALSE;
			}

                        lpdnTraversal->lpdnNext->lpdnPrevious = lpdnTraversal;
                        lpdnTraversal = lpdnTraversal->lpdnNext;
                        lpdnTraversal->lpdnNext = lpdnTemp;
                        lpdnTemp->lpdnPrevious = lpdnTraversal;
                        lpdnTraversal->lpData = LocalAlloc(sizeof(lpllList->ullObjectSize));
			if (NULLPTR == lpdnTraversal->lpData)
			{
				if (NULLPTR != lpllList->hLock)
				{ ReleaseSingleObject(lpllList->hLock); }
				return FALSE;
			}

                        CopyMemory(lpdnTraversal->lpData, lpData, lpllList->ullObjectSize);
                        break;
                }        
                case FALSE:
                {
                        LPSINGLE_NODE lpsnTraversal, lpsnTemp;
                        ULONGLONG ullIndex;

                        for (
                                ullIndex = 0, lpsnTraversal = (LPSINGLE_NODE)lpllList->lpFirst;
                                ullIndex != ullPosition;
                                ullIndex++, lpsnTraversal = lpsnTraversal->lpsnNext
                        ){ NO_OPERATION(); }

                        lpsnTemp = lpsnTraversal->lpsnNext;
                        lpsnTraversal->lpsnNext = LocalAlloc(sizeof(LPSINGLE_NODE));
			if (NULLPTR == lpsnTraversal->lpsnNext)
			{
				if (NULLPTR != lpllList->hLock)
				{ ReleaseSingleObject(lpllList->hLock); }
				return FALSE;
			}

                        lpsnTraversal = lpsnTraversal->lpsnNext;
                        lpsnTraversal->lpsnNext = lpsnTemp;
                        lpsnTraversal->lpData = LocalAlloc(lpllList->ullObjectSize);
			if (NULLPTR == lpsnTraversal->lpData)
			{
				if (NULLPTR != lpllList->hLock)
				{ ReleaseSingleObject(lpllList->hLock); }
				return FALSE;
			}

                        CopyMemory(lpsnTraversal->lpData, lpData, lpllList->ullObjectSize);
                        break;
                }
        }

        lpllList->ullNumberOfElements++;

        if (NULLPTR != lpllList->hLock) 
        { ReleaseSingleObject(lpllList->hLock); }

        return TRUE;
}




_Return_May_Be_Null_
PODNET_API
LPVOID
LinkedListRemove(
        _In_                    HLINKEDLIST     hllList,
        _In_                    ULONGLONG       ullPosition
){
	EXIT_IF_UNLIKELY_NULL(hllList, NULLPTR);
        LPLINKED_LIST lpllList;
        LPVOID lpRet;
        ULONGLONG ullIndex;
        lpllList = (LPLINKED_LIST)hllList;
        lpRet = NULLPTR;
	if (ullPosition < lpllList->ullNumberOfElements || 0 == ullPosition)
        { return FALSE; }

        if (ullPosition == lpllList->ullNumberOfElements)
        { return LinkedListDetachBack(hllList); }
        if (0 == ullPosition)
        { return LinkedListDetachFront(hllList); }

        if (NULLPTR != lpllList->hLock)
        { WaitForSingleObject(lpllList->hLock, INFINITE_WAIT_TIME); }

        switch (lpllList->bIsDoubleLinkedList)
        {
                case TRUE:
                {
                        LPDOUBLE_NODE lpdnTraversal, lpdnTemp;

                        if (ullPosition <= (ULONGLONG)(lpllList->ullNumberOfElements / 2))
                        {
                                for (
                                        ullIndex = 0, lpdnTraversal = (LPDOUBLE_NODE)lpllList->lpFirst;
                                        ullIndex != ullPosition;
                                        ullIndex++, lpdnTraversal = lpdnTraversal->lpdnNext
                                ){ NO_OPERATION(); }

                                lpdnTemp = lpdnTraversal->lpdnNext;
                                lpRet = lpdnTemp->lpData;
                                lpdnTraversal->lpdnNext = lpdnTemp->lpdnNext;
                                lpdnTraversal->lpdnNext->lpdnPrevious = lpdnTraversal;
                                FreeMemory(lpdnTraversal);
                        }
                        else
                        {
                                for (
                                        ullIndex = lpllList->ullNumberOfElements -1, lpdnTraversal = (LPDOUBLE_NODE)lpllList->lpLast;
                                        ullIndex != ullPosition;
                                        ullIndex--, lpdnTraversal = lpdnTraversal->lpdnPrevious
                                ){ NO_OPERATION(); }

                                lpdnTemp = lpdnTraversal->lpdnNext;
                                lpRet = lpdnTemp->lpData;
                                lpdnTraversal->lpdnNext = lpdnTemp->lpdnNext;
                                lpdnTraversal->lpdnNext->lpdnPrevious = lpdnTraversal;
                                FreeMemory(lpdnTraversal);
                        }
                        break;
                }      
                case FALSE:
                {
                        LPSINGLE_NODE lpsnTraversal, lpsnPrevious;

                        for (
                                ullIndex = 0, lpsnTraversal = (LPSINGLE_NODE)lpllList->lpFirst, lpsnPrevious = NULLPTR;
                                ullIndex != ullPosition;
                                ullIndex++, lpsnPrevious = lpsnTraversal, lpsnTraversal = lpsnTraversal->lpsnNext
                        ){ NO_OPERATION(); }

                        lpsnPrevious->lpsnNext = lpsnTraversal->lpsnNext;
                        lpRet = lpsnTraversal;
                        FreeMemory(lpsnTraversal);
                        break;
                }
        }

        lpllList->ullNumberOfElements--;
        
        if (NULLPTR != lpllList->hLock)      
        { ReleaseSingleObject(lpllList->hLock); }  

        return lpRet;
}




_Return_May_Be_Null_
PODNET_API
LPVOID
LinkedListRemoveAtNode(
        _In_                    HLINKEDLIST     hllList,
        _In_                    LPVOID          lpNode,
        _In_                    BOOL            bFreeData
){
        LPLINKED_LIST lpllList;
        LPVOID lpRet;
        lpllList = (LPLINKED_LIST)hllList;
        lpRet = NULLPTR;

        if (NULLPTR == lpNode)
        { return LinkedListDetachFront(hllList); }
        
        switch (lpllList->bIsDoubleLinkedList)
        {

                case TRUE:
                {
                        LPDOUBLE_NODE lpdnNode;
                        lpdnNode = (LPDOUBLE_NODE)lpNode;
                        
                        if (NULLPTR != lpdnNode->lpdnPrevious)
                        { lpdnNode->lpdnPrevious->lpdnNext = lpdnNode->lpdnNext; }
                        
                        if (NULLPTR != lpdnNode->lpdnNext)
                        { lpdnNode->lpdnNext->lpdnPrevious = lpdnNode->lpdnPrevious; }

                        if (TRUE == bFreeData)
                        { FreeMemory(lpdnNode->lpData); }
                        else
                        { lpRet = lpdnNode->lpData; }
                        FreeMemory(lpdnNode);
                        lpllList->ullNumberOfElements--;

                        break;
                }

                case FALSE:
                {
                        LPSINGLE_NODE lpsnNode, lpsnPrevious;
                        for (
                                lpsnNode = (LPSINGLE_NODE)lpllList->lpFirst, lpsnPrevious = NULLPTR;
                                lpsnNode != (LPSINGLE_NODE)lpNode;
                                lpsnNode = lpsnNode->lpsnNext
                        ){ lpsnPrevious = lpsnNode; }

			if (NULLPTR != lpsnPrevious)
                        { lpsnPrevious->lpsnNext = lpsnNode->lpsnNext; }

                        if (TRUE == bFreeData)
                        { FreeMemory(lpsnNode->lpData); }
                        else
                        { lpRet = lpsnNode->lpData; }
                        FreeMemory(lpsnNode);
                        lpllList->ullNumberOfElements--;

                        break;
                }

        }

        return lpRet;
}




_Success_(return != NULLPTR, ...)
PODNET_API
LPVOID
LinkedListRemoveItemWithSpecificData(
        _In_                    HLINKEDLIST     hllList,
        _In_                    LPVOID          lpDataToCompare
){
        LPLINKED_LIST lpllList;
        BOOL bRet;
        LPVOID lpRet;
        lpllList = (LPLINKED_LIST)hllList;
        bRet = FALSE;
        lpRet = NULLPTR;

        if (0 == lpllList->ullNumberOfElements)
        { return FALSE; }

        if (NULLPTR != lpllList->hLock)
        { WaitForSingleObject(lpllList->hLock, INFINITE_WAIT_TIME); }


        switch (lpllList->bIsDoubleLinkedList)
        {
                case TRUE:
                {
                        LPDOUBLE_NODE lpdnTraverse;

                        for (
                                lpdnTraverse = (LPDOUBLE_NODE)lpllList->lpFirst;
                                lpdnTraverse->lpdnNext != NULLPTR;
                                lpdnTraverse = lpdnTraverse->lpdnNext
                        ){
                                bRet = CompareMemory(lpdnTraverse->lpData, lpDataToCompare, lpllList->ullObjectSize);
                                if (TRUE == bRet)
                                {
                                        lpdnTraverse->lpdnPrevious->lpdnNext = lpdnTraverse->lpdnNext;
                                        lpdnTraverse->lpdnNext->lpdnPrevious= lpdnTraverse->lpdnNext;
                                        lpRet = lpdnTraverse->lpData;
                                        FreeMemory(lpdnTraverse);
                                        lpllList->ullNumberOfElements--;
                                        JUMP(Found);
                                }
                        }

                        break;
                }
                case FALSE:
                {
                        LPSINGLE_NODE lpsnTraverse, lpsnPrevious;

                        for (
                                lpsnTraverse = (LPSINGLE_NODE)lpllList->lpFirst, lpsnPrevious = NULLPTR;
                                lpsnTraverse->lpsnNext != NULLPTR;
                                lpsnPrevious = lpsnTraverse, lpsnTraverse = lpsnTraverse->lpsnNext
                        ){
                                bRet = CompareMemory(lpsnTraverse->lpData, lpDataToCompare, lpllList->ullObjectSize);
                                if (TRUE == bRet)
                                {
                                        if (lpsnPrevious == NULLPTR)
                                        { lpllList->lpFirst = (LPVOID)lpsnTraverse->lpsnNext; }               
                                        else
                                        { lpsnPrevious->lpsnNext = lpsnTraverse->lpsnNext; }
                                        lpRet = lpsnTraverse->lpData;
                                        FreeMemory(lpsnTraverse);
                                        lpllList->ullNumberOfElements--;
                                        JUMP(Found); 
                                }
                        }

                        break;
                }
        }

Found:

        if (NULLPTR != lpllList->hLock)
        { ReleaseSingleObject(lpllList->hLock); }

        return lpRet;
}




_Success_(return != FALSE, ...)
PODNET_API
BOOL
LinkedListErase(
        _In_                    HLINKEDLIST     hllList
){
        LPLINKED_LIST lpllList;
        lpllList = (LPLINKED_LIST)hllList;

        if (NULLPTR == lpllList->hLock)
        { WaitForSingleObject(lpllList->hLock, INFINITE_WAIT_TIME); }

        if (lpllList->ullNumberOfElements == 0)
        {
                FreeMemory(lpllList);
                JUMP(Finished);
        }

        switch (lpllList->bIsDoubleLinkedList)
        {
                case TRUE:
                {
                        LPDOUBLE_NODE lpdnTraverse;

                        for (
                                lpdnTraverse = (LPDOUBLE_NODE)lpllList->lpFirst;
                                lpdnTraverse != NULLPTR;
                                lpdnTraverse = lpdnTraverse->lpdnNext
                        ){
                                if (NULLPTR != lpdnTraverse->lpdnPrevious)
                                { 
                                        FreeMemory(lpdnTraverse->lpdnPrevious->lpData);
                                        FreeMemory(lpdnTraverse->lpdnPrevious); 
                                }
                        }

                        FreeMemory(((LPDOUBLE_NODE)lpllList->lpLast)->lpData);
                        FreeMemory(lpllList->lpLast);
                        break;
                }
                case FALSE:
                {
                        LPSINGLE_NODE lpsnTraverse, lpsnPrevious;

                        for (
                                lpsnTraverse = (LPSINGLE_NODE)lpllList->lpFirst, lpsnPrevious = NULLPTR;
                                lpsnTraverse != NULLPTR;
                                lpsnPrevious = lpsnTraverse, lpsnTraverse = lpsnTraverse->lpsnNext
                        ){
                                if (NULLPTR != lpsnPrevious)
                                {
                                        FreeMemory(lpsnPrevious->lpData);
                                        FreeMemory(lpsnPrevious);
                                }
                                FreeMemory(((LPSINGLE_NODE)lpllList->lpLast)->lpData);
                                FreeMemory(lpllList->lpLast);
                        }

                        break;
                }
        }

        lpllList->ullNumberOfElements = 0;
        if (NULLPTR == lpllList->hLock)
        { ReleaseSingleObject(lpllList->hLock); }

Finished:

        return TRUE;
}




_Success_(return != FALSE, ...)
PODNET_API
BOOL
LinkedListSwap(
        _In_                    HLINKEDLIST     hllListSwapee,
        _In_                    HLINKEDLIST     hllListSwaperand
){
	EXIT_IF_UNLIKELY_NULL(hllListSwapee, FALSE);
	EXIT_IF_UNLIKELY_NULL(hllListSwaperand, FALSE);
        LPLINKED_LIST lpllSwapee, lpllSwaperand;
        LPVOID lpTemp;
        lpllSwapee = (LPLINKED_LIST)hllListSwapee;
        lpllSwaperand = (LPLINKED_LIST)hllListSwaperand;
 
        lpTemp = LocalAlloc(sizeof(LINKED_LIST));
	/* cppcheck-suppress memleak */
	EXIT_IF_UNLIKELY_NULL(lpTemp, FALSE);

        CopyMemory(lpTemp, lpllSwapee, sizeof(LINKED_LIST));
        CopyMemory(lpllSwapee, lpllSwaperand, sizeof(LINKED_LIST));
        CopyMemory(lpllSwaperand, lpTemp, sizeof(LINKED_LIST));
        FreeMemory(lpTemp);
        return TRUE;
}




_Return_May_Be_Null_
PODNET_API
HLINKEDLIST
LinkedListCopy(
        _In_                    HLINKEDLIST     hllList
){
	EXIT_IF_UNLIKELY_NULL(hllList, NULLPTR);
        LPLINKED_LIST lpllList, lpllNew;
        lpllList = (LPLINKED_LIST)hllList;

        lpllNew = LocalAllocAndZero(sizeof(LINKED_LIST));
	/* cppcheck-suppress memleak */
	EXIT_IF_UNLIKELY_NULL(lpllNew, NULLPTR);
        
        if (NULLPTR != lpllList->hLock)
        { WaitForSingleObject(lpllList->hLock, INFINITE_WAIT_TIME); }
        
        CopyMemory(lpllNew, lpllList, sizeof(LINKED_LIST));
        
        switch (lpllList->bIsDoubleLinkedList)
        {
                case TRUE:
                {
                        LPDOUBLE_NODE lpdnTraverse, lpdnNew, lpdnPrevious;

                        for (
                                lpdnTraverse = (LPDOUBLE_NODE)lpllList->lpFirst, lpdnNew = NULLPTR, lpdnPrevious = NULLPTR;
                                lpdnTraverse != NULLPTR;
                                lpdnTraverse = lpdnTraverse->lpdnNext, lpdnPrevious = lpdnNew
                        ){
                                lpdnNew = LocalAlloc(sizeof(DOUBLE_NODE));
				if (NULLPTR == lpdnNew)
				{
					FreeMemory(lpllNew);
					return NULLPTR;
				}

                                lpdnNew->lpData = LocalAlloc(lpllNew->ullObjectSize);
				if (NULLPTR == lpdnNew->lpData)
				{
					FreeMemory(lpdnNew);
					FreeMemory(lpllNew);
					return NULLPTR;
				}

                                CopyMemory(lpdnNew->lpData, lpdnTraverse->lpData, sizeof(lpllNew->ullObjectSize));
                                if (NULLPTR == lpllNew->lpFirst)
                                {
                                        lpllNew->lpFirst = (LPVOID)lpdnNew; 
                                        lpdnNew->lpdnPrevious = lpdnPrevious;
                                }
                                else
                                { lpdnPrevious->lpdnNext = lpdnNew; }
                        }

                        lpllNew->lpLast = (LPVOID)lpdnPrevious;
                        lpdnPrevious->lpdnNext = NULLPTR;
                        break;
                }
                case FALSE:
                {
                        LPSINGLE_NODE lpsnTraverse, lpsnNew, lpsnPrevious;

                        for (
                                lpsnTraverse = (LPSINGLE_NODE)lpllList->lpFirst, lpsnNew = NULLPTR, lpsnPrevious = NULLPTR;
                                lpsnTraverse != NULLPTR;
                                lpsnTraverse = lpsnTraverse->lpsnNext, lpsnPrevious = lpsnNew
                        ){
                                lpsnNew = LocalAlloc(sizeof(SINGLE_NODE));
				if (NULLPTR == lpsnNew)
				{
					FreeMemory(lpllNew);
					return NULLPTR;
				}

                                lpsnNew->lpData = LocalAlloc(lpllNew->ullObjectSize);
				if (NULLPTR == lpsnNew->lpData)
				{
					FreeMemory(lpsnNew);
					FreeMemory(lpllList);
					return NULLPTR;
				}
				
                                CopyMemory(lpsnNew->lpData, lpsnTraverse->lpData, lpllNew->ullObjectSize);
                                if (NULLPTR == lpllNew->lpFirst)
                                { lpllNew->lpFirst = (LPVOID)lpsnNew; }
                                else
                                { lpsnPrevious->lpsnNext = lpsnNew; }
                        }

                        lpllNew->lpLast = (LPVOID)lpsnPrevious;
                        lpsnPrevious->lpsnNext = NULLPTR;
                        break;
                }
        }

        if (NULLPTR != lpllList->hLock)
        { ReleaseSingleObject(lpllList->hLock); }

        return (HLINKEDLIST)lpllNew;
}




_Success_(return != FALSE, ...)
PODNET_API
BOOL
LinkedListIsEmpty(
        _In_                    HLINKEDLIST     hllList
){
        LPLINKED_LIST lpllList;
        lpllList = (LPLINKED_LIST)hllList;
        return (NULLPTR == lpllList->lpFirst) ? TRUE : FALSE;
}




_Success_(return != MAX_ULONGLONG, ...)
PODNET_API
ULONGLONG
LinkedListSize(
        _In_                    HLINKEDLIST     hllList
){
        LPLINKED_LIST lpllList;
        lpllList = (LPLINKED_LIST)hllList;
        return lpllList->ullNumberOfElements;
}




_Success_(return != FALSE, ...)
PODNET_API
BOOL
LinkedListSplice(
        _In_                    HLINKEDLIST     hllListMain,
        _In_                    HLINKEDLIST     hllListSplicerand,
        _In_                    ULONGLONG       ullPosition
){
        LPLINKED_LIST lpllListMain, lpllListSplicerand;
        ULONGLONG ullIndex;
        lpllListMain = (LPLINKED_LIST)hllListMain;
        lpllListSplicerand = (LPLINKED_LIST)hllListSplicerand;

        if (lpllListMain->bIsDoubleLinkedList != lpllListSplicerand->bIsDoubleLinkedList)
        { return FALSE; }

        if (lpllListMain->ullObjectSize != lpllListSplicerand->ullObjectSize)
        { return FALSE; }

        if (NULLPTR != lpllListMain->hLock)
        { WaitForSingleObject(lpllListMain->hLock, INFINITE_WAIT_TIME); }

        if (NULLPTR != lpllListSplicerand->hLock)
        { WaitForSingleObject(lpllListSplicerand->hLock, INFINITE_WAIT_TIME); }

        switch (lpllListMain->bIsDoubleLinkedList)
        {
                case TRUE:
                {
                        LPDOUBLE_NODE lpdnTraverse, lpdnNext;

                        for (
                                ullIndex = 0, lpdnTraverse = (LPDOUBLE_NODE)lpllListMain->lpFirst;
                                ullIndex != ullPosition;
                                ullIndex++
                        ){ NO_OPERATION(); }

                        lpdnNext = lpdnTraverse->lpdnNext;
                        lpdnTraverse->lpdnNext = (LPDOUBLE_NODE)lpllListSplicerand->lpFirst;
                        lpdnTraverse->lpdnNext->lpdnPrevious = lpdnTraverse;
                        lpdnNext->lpdnPrevious = (LPDOUBLE_NODE)lpllListSplicerand->lpLast;
                        lpdnNext->lpdnPrevious->lpdnNext = lpdnNext;
                        lpllListSplicerand->lpLast = lpllListMain->lpLast;
                        ullIndex = lpllListSplicerand->ullNumberOfElements;
                        lpllListSplicerand->ullNumberOfElements = ullIndex + lpllListMain->ullNumberOfElements - ullPosition;
                        lpllListMain->ullNumberOfElements += ullIndex;
                        break;
                }
                case FALSE:
                {
                        LPSINGLE_NODE lpsnTraverse, lpsnNext;

                        for (
                                ullIndex = 0, lpsnTraverse = (LPSINGLE_NODE)lpllListMain->lpFirst;
                                ullIndex != ullPosition;
                                ullIndex++
                        ){ NO_OPERATION(); }

                        lpsnNext = lpsnTraverse->lpsnNext;
                        lpsnTraverse ->lpsnNext = (LPSINGLE_NODE)lpllListSplicerand->lpFirst;
                        ((LPSINGLE_NODE)lpllListSplicerand->lpLast)->lpsnNext = lpsnNext;
                        ullIndex = lpllListSplicerand->ullNumberOfElements;
                        lpllListSplicerand->ullNumberOfElements = ullIndex + lpllListMain->ullNumberOfElements - ullPosition;
                        lpllListMain->ullNumberOfElements += ullIndex;
                        break;
                }
        }

        
        if (NULLPTR != lpllListMain->hLock)
        { ReleaseSingleObject(lpllListMain->hLock); }
        if (NULLPTR != lpllListSplicerand->hLock)
        { ReleaseSingleObject(lpllListSplicerand->hLock); }
        return TRUE;        
}




_Success_(return != FALSE, ...)
PODNET_API
BOOL
LinkedListIsUnique(
        _In_                    HLINKEDLIST     hllList
){
        LPLINKED_LIST lpllList;
        BOOL bRet;
        lpllList = (LPLINKED_LIST)hllList;
        bRet = TRUE;

        if (0 == lpllList->ullNumberOfElements)
        { return TRUE; }

        if (NULLPTR != lpllList->hLock)
        { WaitForSingleObject(lpllList->hLock, INFINITE_WAIT_TIME); }

        switch (lpllList->bIsDoubleLinkedList)
        {
                case TRUE:
                {
                        LPDOUBLE_NODE lpdnTraverse, lpdnComparand;

                        for (
                                lpdnTraverse = (LPDOUBLE_NODE)lpllList->lpFirst;
                                lpdnTraverse != NULLPTR;
                                lpdnTraverse = lpdnTraverse->lpdnNext
                        ){
                                for (
                                        lpdnComparand = lpdnTraverse->lpdnNext;
                                        lpdnComparand != NULLPTR;
                                        lpdnComparand = lpdnComparand->lpdnNext
                                ){
                                        bRet = (0 == CompareMemory(lpdnComparand, lpdnTraverse, lpllList->ullObjectSize)) ? TRUE : FALSE;
                                        if (FALSE == bRet)
                                        { JUMP(Finished); }
                                }
                        }

                        break;
                }
                case FALSE:
                {
                        LPSINGLE_NODE lpsnTraverse, lpsnComparand;

                        for (
                                lpsnTraverse = (LPSINGLE_NODE)lpllList->lpFirst;
                                lpsnTraverse != NULLPTR;
                                lpsnTraverse = lpsnTraverse->lpsnNext
                        ){
                                for (
                                        lpsnComparand = lpsnTraverse->lpsnNext;
                                        lpsnComparand != NULLPTR;
                                        lpsnComparand = lpsnComparand->lpsnNext
                                ){
                                        bRet = (0 == CompareMemory(lpsnComparand, lpsnTraverse, lpllList->ullObjectSize)) ? TRUE : FALSE;
                                        if (FALSE == bRet)
                                        { JUMP(Finished); }
                                }
                        }

                        break;
                }
        }

Finished:
        if (NULLPTR != lpllList->hLock)
        { ReleaseSingleObject(lpllList->hLock); }

        return bRet;
}




_Success_(return != FALSE, ...)
PODNET_API
BOOL
LinkedListMerge(
        _In_                    LPHLINKEDLIST   lphllList,
        _In_                    ULONG           ulNumberOfLists
){
        LPLINKED_LIST lpllList;
        DLPLINKED_LIST dlpllLists;
        ULONG ulIndex;
        lpllList = (LPLINKED_LIST)lphllList[0];
        dlpllLists = LocalAlloc(sizeof(LPLINKED_LIST) * ulNumberOfLists);

        for(
                ulIndex = 1;
                ulIndex < ulNumberOfLists;
                ulIndex++
        ){ 
                dlpllLists[ulIndex] = (LPLINKED_LIST)lphllList[ulIndex]; 
                if (NULLPTR != dlpllLists[ulIndex]->hLock)
                { WaitForSingleObject(dlpllLists[ulIndex]->hLock, INFINITE_WAIT_TIME); }
        }

        switch (lpllList->bIsDoubleLinkedList)
        {
                case TRUE:
                {
                        LPDOUBLE_NODE lpdnMerger, lpdnMergerand;

                        for (   
                                ulIndex = 1U, lpdnMerger = (LPDOUBLE_NODE)lpllList->lpLast, lpdnMergerand = NULLPTR;
                                ulIndex < ulNumberOfLists;
                                ulIndex++, lpdnMerger = lpdnMergerand, lpdnMergerand = (LPDOUBLE_NODE)dlpllLists[ulIndex]->lpLast
                        ){
                                lpdnMerger->lpdnNext = (LPDOUBLE_NODE)dlpllLists[ulIndex]->lpFirst;
                                lpdnMerger->lpdnNext->lpdnPrevious = lpdnMerger;
                        }

                        for (
                                ulIndex = ulNumberOfLists - 2U;
                                ulIndex < ulNumberOfLists;
                                ulIndex--
                        ){ 
                                dlpllLists[ulIndex]->ullNumberOfElements += dlpllLists[ulIndex + 1U]->ullNumberOfElements; 
                                dlpllLists[ulIndex]->lpLast = dlpllLists[ulNumberOfLists - 1U]->lpLast;
                        }

                        break;
                }
                case FALSE:
                {
                        LPSINGLE_NODE lpsnMerger, lpsnMergerand;

                        for (
                                ulIndex = 1U, lpsnMerger = (LPSINGLE_NODE)lpllList->lpLast, lpsnMergerand = NULLPTR;
                                ulIndex < ulNumberOfLists;
                                ulIndex++
                        ){ 
                                lpsnMerger = lpsnMergerand;
                                lpsnMergerand = (LPSINGLE_NODE)dlpllLists[ulIndex]->lpLast;
                                lpsnMerger->lpsnNext = (LPSINGLE_NODE)dlpllLists[ulIndex]->lpFirst; 
                        }

			if (NULLPTR != dlpllLists && NULLPTR != dlpllLists[ulIndex])
                        { lpllList->lpLast = dlpllLists[ulIndex]->lpLast; }

                        for (
                                ulIndex = ulNumberOfLists - 2U;
                                ulIndex < ulNumberOfLists;
                                ulIndex--
                        ){ 
                                dlpllLists[ulIndex]->ullNumberOfElements += dlpllLists[ulIndex + 1U]->ullNumberOfElements; 
                                dlpllLists[ulIndex]->lpLast = dlpllLists[ulNumberOfLists - 1U]->lpLast;
                        }

                        break;
                }
        }
                
        for (
                ulIndex = 0;
                ulIndex < ulNumberOfLists;
                ulIndex++
        ){ 
                if (NULLPTR != dlpllLists[ulIndex]->hLock)
                { ReleaseSingleObject(dlpllLists[ulIndex]->hLock); }
        }

        
        return TRUE;
}




_Success_(return != FALSE, ...)
PODNET_API
BOOL
LinkedListReverse(
        _In_                    HLINKEDLIST     hllList
){
        LPLINKED_LIST lpllList;
        lpllList = (LPLINKED_LIST)hllList;

        if (NULLPTR != lpllList->hLock)
        { WaitForSingleObject(lpllList->hLock, INFINITE_WAIT_TIME); }

        switch (lpllList->bIsDoubleLinkedList)
        {
                case TRUE:
                {
                        LPDOUBLE_NODE lpdnTraverse;

                        for (
                                lpdnTraverse = (LPDOUBLE_NODE)lpllList->lpFirst;
                                lpdnTraverse != NULLPTR;
                                lpdnTraverse = lpdnTraverse->lpdnPrevious      /* Wtf? */
                        ){ XOR_SWAP_PTR(lpdnTraverse->lpdnNext, lpdnTraverse->lpdnPrevious); }

                        XOR_SWAP_PTR(lpllList->lpFirst, lpllList->lpLast);
                        break;
                }
                case FALSE:
                {       /* This is an operation that may need to be optimized, or written better */ 
                        LPSINGLE_NODE lpsnTraverse, lpsnReverserand, lpsnNext;

                        for (
                                lpsnTraverse = (LPSINGLE_NODE)lpllList->lpFirst, lpsnReverserand = NULLPTR, lpsnNext = NULLPTR;
                                lpsnTraverse != NULLPTR;
                                lpsnTraverse = lpsnNext
                        ){ 
                                lpsnReverserand = lpsnTraverse;
                                lpsnNext = lpsnTraverse->lpsnNext;
                                lpsnTraverse->lpsnNext = lpsnReverserand; 
                        }

                        XOR_SWAP_PTR(lpllList->lpFirst, lpllList->lpLast);
                        break;
                }
        }

        if (NULLPTR != lpllList->hLock)
        { ReleaseSingleObject(lpllList->hLock); }
        
        return TRUE;      
}
