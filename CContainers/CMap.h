/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CMAP_H
#define CMAP_H
#include "CContainers.h"
#include "CVector.h"

typedef struct _MAP	 	*HMAP;

typedef BOOL(* LPFN_CLOSE_MAP_ROUTINE)(
	_In_        HMAP    hMap,
	_In_Opt_    ULONG   ulFlags
);

/*
PODNET_API
HISTANCE
InitializeMapLibrary(
	ULONG   ulFlags
);


PODNET_API
BOOL
CloseMapLibrary(
	HINSTANCE hInstance
);
*/

PODNET_API
HMAP
CreateMap(
	_In_                                    ULONG                   ulSizeKey,
	_In_                                    ULONG                   ulSizeMapped,
	_In_                                    ULONG                   ulNumberOfObjects,
	_In_Opt_                                ULONG                   ulFlags
);

PODNET_API
BOOL
CloseMap(
	_In_                                    HMAP                    hMap,
	_In_Opt_                                ULONG                   ulFlags
);


PODNET_API
BOOL
CloseMapEx(
	_In_                                    HMAP                    hMap,
	_In_                                    LPFN_CLOSE_MAP_ROUTINE  lpfnKeyCloser,
	_In_Opt_ _When_(lpfnKeyCloser != NULL)  ULONG                   ulFlagsKeyCloser,
	_In_                                    LPFN_CLOSE_MAP_ROUTINE  lpfnMappedCloser,
	_In_Opt_ _When_(lpfnKeyCloser != NULL)  ULONG                   ulFlagsMappedCloser,
	_In_Opt_                                ULONG                   ulFlags
);


PODNET_API
BOOL
MapInsert(
	_In_                                    HMAP                    hMap,
	_In_                                    LPVOID                  lpKeyData,
	_In_                                    LPVOID                  lpMappedData,
	_In_Opt_                                ULONG                   ulFlags
);

PODNET_API
LPVOID
MapValueAtKey(
	_In_                                    HMAP                    hMap,
	_In_                                    LPVOID                  lpKey,
	_In_Opt_                                ULONG                   ulFlags
);

#endif
