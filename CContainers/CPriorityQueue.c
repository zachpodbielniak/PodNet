/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CPriorityQueue.h"


typedef struct _PRIORITYQUEUE
{
        LPHLINKEDLIST   lphllQueues;
        ULONG           ulNumberOfQueues;
        HANDLE          hLock;
} PRIORITYQUEUE, *LPPRIORITYQUEUE;




_Success_(return != NULLPTR, ...)
PODNET_API
HPRIORITYQUEUE
CreatePriorityQueue(
        _In_                    ULONGLONG               ullObjectSize,
        _In_                    ULONG                   ulNumberOfDifferentPriorities,
        _In_Opt_                HANDLE                  hLock
){
        LPPRIORITYQUEUE lpPQ;
        ULONG ulIndex;
 
        lpPQ = LocalAlloc(sizeof(PRIORITYQUEUE));
	/* cppcheck-suppress memleak */
	EXIT_IF_UNLIKELY_NULL(lpPQ, NULLPTR);

        lpPQ->hLock = hLock;
        lpPQ->ulNumberOfQueues = ulNumberOfDifferentPriorities;
        lpPQ->lphllQueues = LocalAlloc(sizeof(HLINKEDLIST) * ulNumberOfDifferentPriorities);
	if (NULLPTR == lpPQ->lphllQueues)
	{
		DestroyObject(lpPQ->hLock);
		FreeMemory(lpPQ);
		return NULLPTR;
	}

	for (
                ulIndex = 0;
                ulIndex < ulNumberOfDifferentPriorities;
                ulIndex++
        ){ lpPQ->lphllQueues[ulIndex] = CreateLinkedList(ullObjectSize, FALSE, NULL); }

        return (HPRIORITYQUEUE)lpPQ;
}




_Success_(return != FALSE, ...)
PODNET_API
BOOL
DestroyPriorityQueue(
        _In_                    HPRIORITYQUEUE          hqQueue
){
        LPPRIORITYQUEUE lpPQ;
        HANDLE hLock;
        ULONG ulIndex;
        lpPQ = (LPPRIORITYQUEUE)hqQueue;
        hLock = lpPQ->hLock;

        if (NULLPTR != hLock)
        { WaitForSingleObject(hLock, INFINITE_WAIT_TIME); }

        for (
                ulIndex = 0;
                ulIndex < lpPQ->ulNumberOfQueues;
                ulIndex++
        ){ DestroyLinkedList(lpPQ->lphllQueues[ulIndex]); }

        ZeroMemory(lpPQ->lphllQueues, sizeof(HLINKEDLIST) * lpPQ->ulNumberOfQueues);
        FreeMemory(lpPQ->lphllQueues);
        ZeroMemory(lpPQ, sizeof(PRIORITYQUEUE));
        FreeMemory(lpPQ);

        if (NULLPTR != hLock)
        { ReleaseSingleObject(hLock); }

        return TRUE;
}




_Success_(return != FALSE, ...)
PODNET_API
BOOL
PriorityQueuePush(
        _In_                    HPRIORITYQUEUE          hqQueue,
        _In_                    ULONG                   ulPriority,
        _In_                    LPVOID                  lpData
){
        LPPRIORITYQUEUE lpPQ;
        lpPQ = (LPPRIORITYQUEUE)hqQueue;

        if (ulPriority >= lpPQ->ulNumberOfQueues)
        { return FALSE; }

        if (NULLPTR != lpPQ->hLock)
        { WaitForSingleObject(lpPQ->hLock, INFINITE_WAIT_TIME); }

        LinkedListAttachBack(lpPQ->lphllQueues[ulPriority], lpData);

        if (NULLPTR != lpPQ->hLock)
        { ReleaseSingleObject(lpPQ->hLock); }

        return TRUE;
}




_Success_(return != NULLPTR, ...)
PODNET_API
LPVOID
PriorityQueueNextInLine(
        _In_                    HPRIORITYQUEUE          hqQueue
){
        LPPRIORITYQUEUE lpPQ;
        LPVOID lpRet;
        ULONG ulIndex;
        lpPQ = (LPPRIORITYQUEUE)hqQueue;
        lpRet = NULLPTR;

        if (NULLPTR != lpPQ->hLock)
        { WaitForSingleObject(lpPQ->hLock, INFINITE_WAIT_TIME); }

        for (
                ulIndex = 0;
                ulIndex < lpPQ->ulNumberOfQueues;
                ulIndex++
        ){
                if (0 != LinkedListSize(lpPQ->lphllQueues[ulIndex]))
                {
                        lpRet = LinkedListDetachFront(lpPQ->lphllQueues[ulIndex]);
                        JUMP(GotNextItem); 
                }
        }

GotNextItem:

        if (NULLPTR != lpPQ->hLock)
        { ReleaseSingleObject(lpPQ->hLock); }

        return lpRet;
}




_Success_(return != MAX_ULONGLONG, ...)
PODNET_API
ULONGLONG
PriorityQueueNumberOfObjectsWaiting(
        _In_                    HPRIORITYQUEUE          hqQueue
){
        LPPRIORITYQUEUE lpPQ;
        ULONGLONG ullTotal;
        ULONG ulIndex;
        lpPQ = (LPPRIORITYQUEUE)hqQueue;
        ullTotal = 0x00ULL;

        if (NULLPTR != lpPQ->hLock)
        { WaitForSingleObject(lpPQ->hLock, INFINITE_WAIT_TIME); }

        for (
                ulIndex = 0;
                ulIndex < lpPQ->ulNumberOfQueues;
                ulIndex++
        ){ ullTotal += LinkedListSize(lpPQ->lphllQueues[ulIndex]); }

        if (NULLPTR != lpPQ->hLock)
        { ReleaseSingleObject(lpPQ->hLock); }

        return ullTotal;
}



_Success_(return != FALSE, ...)
PODNET_API
BOOL
PriorityQueueSwap(
        _In_                    HPRIORITYQUEUE          hqMain,
        _In_                    HPRIORITYQUEUE          hqSwaperand
){
        LPPRIORITYQUEUE lpMain, lpSwaperand, lpTemp;
        lpMain = (LPPRIORITYQUEUE)hqMain;
        lpSwaperand = (LPPRIORITYQUEUE)hqSwaperand;
        lpTemp = LocalAlloc(sizeof(PRIORITYQUEUE));
	
	if (NULLPTR == lpTemp)
	{ return FALSE; }
        
        if (NULLPTR != lpMain->hLock)
        { WaitForSingleObject(lpMain->hLock, INFINITE_WAIT_TIME); }
        if (NULLPTR != lpSwaperand->hLock)
        { WaitForSingleObject(lpSwaperand->hLock, INFINITE_WAIT_TIME); }


        CopyMemory(lpTemp, lpMain, sizeof(PRIORITYQUEUE));
        CopyMemory(lpMain, lpSwaperand, sizeof(PRIORITYQUEUE));
        CopyMemory(lpSwaperand, lpTemp, sizeof(PRIORITYQUEUE));
        FreeMemory(lpTemp);

        if (NULLPTR != lpMain->hLock)
        { ReleaseSingleObject(lpMain->hLock); }
        if (NULLPTR != lpSwaperand->hLock)
        { ReleaseSingleObject(lpSwaperand->hLock); }

        return TRUE;
}