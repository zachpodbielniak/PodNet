/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../CHandle/CHandle.h"
#include "CLinkedList.h"


typedef LPVOID                  HSTACK, *LPHSTACK;




_Success_(return != NULLPTR, ...)
PODNET_API
HSTACK
CreateStack(
        _In_                    ULONGLONG               ullObjectSize,
        _In_Opt_                HANDLE                  hLock
);




_Success_(return != FALSE, ...)
PODNET_API
BOOL
DestroyStack(
        _In_                    HSTACK                  hsStack
);




_Success_(return != FALSE, ...)
PODNET_API
BOOL
StackPush(
        _In_                    HSTACK                  hsStack,
        _In_                    LPVOID                  lpData
);




_Success_(return != NULLPTR, ...)
PODNET_API
LPVOID
StackPop(
        _In_                    HSTACK                  hsStack
);




_Success_(return != NULLPTR, ...)
PODNET_API
LPVOID
StackTop(
        _In_                    HSTACK                  hsStack
);




_Success_(return != NULLPTR, ...)
PODNET_API
BOOL
StackSwap(
        _In_                    HSTACK                  hsStackMain,
        _In_                    HSTACK                  hsStackSwaperand
);