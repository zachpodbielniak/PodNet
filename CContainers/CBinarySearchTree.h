/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CBINARYSEARCHTREE_H
#define CBINARYSEARCHTREE_h


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../CHandle/CHandle.h"


typedef _Call_Back_ LONG (*LPFN_BS_TREE_COMPARE_PROC)(
	_In_ 		LPVOID 		lpLeafValue,
	_In_ 		LPVOID 		lpComparandValue,
	_In_ 		UARCHLONG	ualSize
);




typedef LPVOID 		HBINARYSEARCHTREE, HBST;




typedef _Call_Back_ LPVOID (*LPFN_BS_TREE_TRAVERSE_PROC)(
	_In_ 		HBINARYSEARCHTREE	hbstData,
	_In_ 		LPVOID 			lpNode, 
	_In_ 		LPVOID 			lpData, 
	_In_Opt_ 	LPVOID 			lpUserData
);



typedef _Call_Back_ BOOL (*LPFN_BS_TREE_DATA_FREE_PROC)(
	_In_ 		LPVOID 			lpData
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HBINARYSEARCHTREE
CreateBinarySearchTree(
	_In_ 		TYPE 				tyDataType,
	_In_		UARCHLONG			ualDataTypeSize,
	_In_Opt_ 	LPFN_BS_TREE_COMPARE_PROC	lpfnCompare,
	_In_Opt_ 	LPFN_BS_TREE_DATA_FREE_PROC	lpfnDestroy
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API 
BOOL 
DestroyBinarySearchTree(
	_In_ 		HBINARYSEARCHTREE		hbstTree
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL 
BinarySearchTreeInsert(
	_In_		HBINARYSEARCHTREE		hbstTree,
	_In_ 		LPVOID 				lpData,
	_In_ 		UARCHLONG 			ualDataSize
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API 
LPVOID 
BinarySearchTreeSearch(
	_In_		HBINARYSEARCHTREE		hbstTree,
	_In_ 		LPVOID 				lpData,
	_In_ 		UARCHLONG 			ualDataSize
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API 
BOOL 
BinarySearchTreeTraverse(
	_In_ 		HBINARYSEARCHTREE		hbstTree,
	_In_ 		LPFN_BS_TREE_TRAVERSE_PROC	lpfnCallBack,
	_In_Opt_	LPVOID 				lpUserData
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API 
LPVOID 
BinarySearchTreeAt(
	_In_ 		HBINARYSEARCHTREE		hbstTree,
	_In_ 		LPVOID 				lpNode
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API 
BOOL 
BinarySearchTreeDestroyAt(
	_In_		HBINARYSEARCHTREE		hbstTree,
	_In_ 		LPVOID 				lpNode
);




#endif