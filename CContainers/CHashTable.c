/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CHashTable.h"


#define HASH_TABLE_MAX_OFFSET		0x08




typedef struct __HASHTABLENODE
{
	ULONGLONG 		ullData;
	UARCHLONG 		ualSize;
	ULONGLONG 		ullExData;
	BOOL 			bIsData;
} HASHTABLENODE, *LPHASHTABLENODE, **DLPHASHTABLENODE;




typedef struct __HASHTABLE
{
	LPHASHTABLENODE		lphtData;
	UARCHLONG 		ualTableSize;
} HASHTABLE, *LPHASHTABLE;



_Success_(return != (UARCHLONG)-1, _Non_Locking_)
INTERNAL_OPERATION
UARCHLONG
__ComputeHash(
	_In_ 			ULONGLONG 		ullData,
	_In_ 			UARCHLONG 		ualDataSize,
	_In_ 			UARCHLONG 		ualTableSize,
	_In_ 			BOOL 			bIsData
){
	UARCHLONG ualHash;
	
	/* BYTE lpbyOut[SHA256_BUFFER_SIZE]; */
	/* ZeroMemory(lpbyOut, sizeof(lpbyOut)); */

	if (FALSE == bIsData)
	{	
		/*
		Sha256((LPBYTE)ullData, ualDataSize, lpbyOut);
		ualHash = *(LPUARCHLONG)lpbyOut;
		*/
		
		ualHash = Crc64((LPBYTE)ullData, ualDataSize, 0);
	}
	else 
	{
		BYTE lpbyIn[8];
		CopyMemory(lpbyIn, &ullData, ualDataSize);
		
		/*
		Sha256(lpbyIn, ualDataSize, lpbyOut);
		ualHash = *(LPUARCHLONG)lpbyOut;
		*/

		ualHash = Crc64(lpbyIn, ualDataSize, 0);
	}

	return ualHash % ualTableSize;
}



_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HHASHTABLE
CreateHashTable(
	_In_ 			UARCHLONG 		ualHashTableSize 			
){
	EXIT_IF_UNLIKELY_NULL(ualHashTableSize, NULLPTR);

	LPHASHTABLE lphtTable;
	
	lphtTable = GlobalAllocAndZero(sizeof(HASHTABLE));
	/* cppcheck-suppress memleak */
	EXIT_IF_UNLIKELY_NULL(lphtTable, NULLPTR);
	
	lphtTable->lphtData = GlobalAllocAndZero(sizeof(HASHTABLENODE) * ualHashTableSize);
	lphtTable->ualTableSize = ualHashTableSize;

	return lphtTable;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
DestroyHashTable(
	_In_ 			HHASHTABLE		hhTable
){
	EXIT_IF_UNLIKELY_NULL(hhTable, FALSE);

	LPHASHTABLE lphtTable;
	lphtTable = (LPHASHTABLE)hhTable;

	FreeMemory(lphtTable->lphtData);
	FreeMemory(lphtTable);
	
	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
HashTableInsert(
	_In_ 			HHASHTABLE		hhTable,
	_In_ 			ULONGLONG 		ullData,
	_In_ 			UARCHLONG 		ualDataSize,
	_In_ 			BOOL 			bIsData
){
	EXIT_IF_UNLIKELY_NULL(hhTable, FALSE);
	EXIT_IF_UNLIKELY_NULL(ualDataSize, FALSE);

	LPHASHTABLE lphtTable;
	UARCHLONG ualHash;

	lphtTable = (LPHASHTABLE)hhTable;
	ualHash = __ComputeHash(ullData, ualDataSize, lphtTable->ualTableSize, bIsData);

	if (ualHash >= lphtTable->ualTableSize)
	{ return FALSE; }

	lphtTable->lphtData[ualHash].ullData = ullData;
	lphtTable->lphtData[ualHash].bIsData = bIsData;
	lphtTable->lphtData[ualHash].ualSize = ualDataSize;

	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
HashTableInsertEx(
	_In_ 			HHASHTABLE		hhTable,
	_In_ 			ULONGLONG 		ullData,
	_In_ 			UARCHLONG 		ualDataSize,
	_In_ 			BOOL			bIsData,
	_In_ 			ULONGLONG 		ullExData
){
	EXIT_IF_UNLIKELY_NULL(hhTable, FALSE);
	EXIT_IF_UNLIKELY_NULL(ualDataSize, FALSE);

	LPHASHTABLE lphtTable;
	UARCHLONG ualHash;

	lphtTable = (LPHASHTABLE)hhTable;
	ualHash = __ComputeHash(ullData, ualDataSize, lphtTable->ualTableSize, bIsData);

	if (ualHash >= lphtTable->ualTableSize)
	{ return FALSE; }

	lphtTable->lphtData[ualHash].ullData = ullData;
	lphtTable->lphtData[ualHash].bIsData = bIsData;
	lphtTable->lphtData[ualHash].ualSize = ualDataSize;
	lphtTable->lphtData[ualHash].ullExData = ullExData;

	return TRUE;
}



_Success_(return != (UARCHLONG)-1, _Non_Locking_)
PODNET_API
UARCHLONG
HashTableGetKey(
	_In_ 			HHASHTABLE 		hhTable,
	_In_ 			ULONGLONG 		ullData,
	_In_ 			UARCHLONG 		ualDataSize,
	_In_ 			BOOL			bIsData
){
	EXIT_IF_UNLIKELY_NULL(hhTable, (UARCHLONG)-1);
	EXIT_IF_UNLIKELY_NULL(ualDataSize, (UARCHLONG)-1);

	LPHASHTABLE lphtTable;
	UARCHLONG ualHash;

	lphtTable = (LPHASHTABLE)hhTable;
	ualHash = __ComputeHash(ullData, ualDataSize, lphtTable->ualTableSize, bIsData);

	if (ualHash >= lphtTable->ualTableSize)
	{ return (UARCHLONG)-1; }

	return ualHash;
}



_Success_(return != MAX_ULONGLONG, _Non_Locking_)
PODNET_API
ULONGLONG
HashTableGetValue(
	_In_ 			HHASHTABLE		hhTable,
	_In_ 			UARCHLONG 		ualKey,
	_Out_Opt_ 		LPUARCHLONG 		lpualDataSize,
	_Out_Opt_		LPBOOL 			lpbIsData
){
	EXIT_IF_UNLIKELY_NULL(hhTable, (ULONGLONG)-1);

	LPHASHTABLE lphtTable;

	lphtTable = (LPHASHTABLE)hhTable;

	if (ualKey >= lphtTable->ualTableSize)
	{ return (ULONGLONG)-1; }

	if (NULLPTR != lpualDataSize)
	{ *lpualDataSize = lphtTable->lphtData[ualKey].ualSize; }

	if (NULLPTR != lpbIsData)
	{ *lpbIsData = lphtTable->lphtData[ualKey].bIsData; }

	return lphtTable->lphtData[ualKey].ullData;
}




_Success_(return != MAX_ULONGLONG, _Non_Locking_)
PODNET_API
ULONGLONG
HashTableGetValueEx(
	_In_ 			HHASHTABLE		hhTable,
	_In_ 			UARCHLONG 		ualKey,
	_Out_Opt_ 		LPUARCHLONG 		lpualDataSize,
	_Out_Opt_		LPBOOL 			lpbIsData,
	_Out_Opt_ 		LPULONGLONG		lpullExData
){
	EXIT_IF_UNLIKELY_NULL(hhTable, (ULONGLONG)-1);

	LPHASHTABLE lphtTable;

	lphtTable = (LPHASHTABLE)hhTable;

	if (ualKey >= lphtTable->ualTableSize)
	{ return (ULONGLONG)-1; }

	if (NULLPTR != lpualDataSize)
	{ *lpualDataSize = lphtTable->lphtData[ualKey].ualSize; }

	if (NULLPTR != lpbIsData)
	{ *lpbIsData = lphtTable->lphtData[ualKey].bIsData; }

	if (NULLPTR != lpullExData)
	{ *lpullExData = lphtTable->lphtData[ualKey].ullExData; }

	return lphtTable->lphtData[ualKey].ullData;
}