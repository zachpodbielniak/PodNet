/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CVECTOR_H
#define CVECTOR_H


#include "../TypeDefs.h"
#include "../Macros.h"
#include "../Prereqs.h"
#include "../Defs.h"


typedef VOID 		*HVECTOR, **LPHVECTOR, ***DLPHVECTOR;
typedef VOID 		*HINSTANCE;
typedef enum __VECTOR_FLAGS
{
	VECTOR_USE_REGISTRATION_VECTOR = 	1 << 0,
	VECTOR_ZERO_NEW_VECTOR = 		1 << 1,
 	VECTOR_COPY_BY_VALUE = 			1 << 2
} VECTOR_FLAGS;


/*
    An iterator macro, for the vector.  The core is a
    for loop, so use braces to surround logical code.
    This macro will take in a HANDLE to the Vector
    you wish to iterate through, and a pointer to the
    value of each index is returned in lpIterator. 
*/
#define VECTOR_ITERATOR(hVector, lpIterator) \
    LPVOID lpIterator;                      \
	for (lpIterator = VectorFront(hVector); \
	  lpIterator <= VectorBack(hVector); \
	  ((ULONGLONG)lpIterator) += ((ULONGLONG)VectorObjectSize(hVector)))





_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HVECTOR 
CreateVector(
	_In_		UARCHLONG 	ualArrayAmount, 
	_In_		UARCHLONG 	ualSizeOfObject, 
	_Reserved_	ULONG 		ulFlags
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL 
CloseVector(
	_In_		HVECTOR 	hVector
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL 
DestroyVector(
	_In_		HVECTOR 	hVector
);




_Success_(return != MAX_ULONG, _Non_Locking_)
PODNET_API
ULONG 
VectorPushBack(
	_In_		HVECTOR RESTRICT 	hVector, 
	_In_		LPVOID RESTRICT		lpData, 
	_In_Opt_	ULONG 			ulFlags
);




_Return_May_Be_Null_
PODNET_API
LPVOID 
VectorAt(
	_In_		HVECTOR 	hVector,
	_In_ 		UARCHLONG 	ualIndex
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL 
VectorPopBack(
	_In_		HVECTOR 	hVector, 
	_In_		LPVOID 		lpOut
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL 
VectorShrinkToFit(
	_In_		HVECTOR 	hVector
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
VectorClear(
    _In_ 		HVECTOR 	hVector
);




_Success_(return != MAX_ULONG, _Non_Locking_)
PODNET_API
UARCHLONG 
VectorSize(
	_In_		HVECTOR 	hVector
);




_Success_(return != 0, _Non_Locking_)
PODNET_API
UARCHLONG 
VectorCapacity(
	_In_		HVECTOR 	hVector
);




_Success_(return != 0, _Non_Locking_)
PODNET_API
UARCHLONG 
VectorObjectSize(
	_In_		HVECTOR 	hVector
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL 
VectorResize(
	_In_		HVECTOR 	hVector, 
	_In_		UARCHLONG	ualSize
);




_Return_May_Be_Null_
PODNET_API
LPVOID 
VectorFront(
	_In_ 		HVECTOR 	hVector
);




_Return_May_Be_Null_
PODNET_API
LPVOID 
VectorBack(
	_In_		HVECTOR 	hVector
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
IsValidVectorHandle(
    	_In_        	HVECTOR     	hVector
);




#endif
