/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/



#ifndef CHASHTABLE_H
#define CHASHTABLE_H


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../Ascii.h"
#include "../CAlgorithms/CCrc64.h"

typedef LPVOID				HHASHTABLE, *LPHHASHTABLE, **DLPHHASHTABLE;




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HHASHTABLE
CreateHashTable(
	_In_ 			UARCHLONG 		ualHashTableSize 			
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
DestroyHashTable(
	_In_ 			HHASHTABLE		hhTable
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
HashTableInsert(
	_In_ 			HHASHTABLE		hhTable,
	_In_ 			ULONGLONG 		ullData,
	_In_ 			UARCHLONG 		ualDataSize,
	_In_ 			BOOL			bIsData
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
HashTableInsertEx(
	_In_ 			HHASHTABLE		hhTable,
	_In_ 			ULONGLONG 		ullData,
	_In_ 			UARCHLONG 		ualDataSize,
	_In_ 			BOOL			bIsData,
	_In_ 			ULONGLONG 		ullExData
);




_Success_(return != (UARCHLONG)-1, _Non_Locking_)
PODNET_API
UARCHLONG
HashTableGetKey(
	_In_ 			HHASHTABLE 		hhTable,
	_In_ 			ULONGLONG 		ullData,
	_In_ 			UARCHLONG 		ualDataSize,
	_In_ 			BOOL 			bIsData		/* Is this direct data, or a pointer? */
);



_Success_(return != MAX_ULONGLONG, _Non_Locking_)
PODNET_API
ULONGLONG
HashTableGetValue(
	_In_ 			HHASHTABLE		hhTable,
	_In_ 			UARCHLONG 		ualKey,
	_Out_Opt_ 		LPUARCHLONG 		lpualDataSize,
	_Out_Opt_		LPBOOL 			lpbIsData
);




_Success_(return != MAX_ULONGLONG, _Non_Locking_)
PODNET_API
ULONGLONG
HashTableGetValueEx(
	_In_ 			HHASHTABLE		hhTable,
	_In_ 			UARCHLONG 		ualKey,
	_Out_Opt_ 		LPUARCHLONG 		lpualDataSize,
	_Out_Opt_		LPBOOL 			lpbIsData,
	_Out_Opt_ 		LPULONGLONG		lpullExData
);


#endif