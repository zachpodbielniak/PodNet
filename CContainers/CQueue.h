/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../CHandle/CHandle.h"
#include "CLinkedList.h"



typedef LPVOID                  HQUEUE, *LPHQUEUE;
typedef HQUEUE                  HQ, *LPHQ;



_Success_(return != NULLPTR, ...)
PODNET_API
HQUEUE
CreateQueue(
        _In_                    ULONGLONG               ullObjectSize,
        _In_Opt_                HANDLE                  hLock
);




_Success_(return != FALSE, ...)
PODNET_API
BOOL
DestroyQueue(
        _In_                    HQUEUE                  hqQueue
);




_Success_(return != FALSE, ...)
PODNET_API
BOOL
QueuePush(
        _In_                    HQUEUE                  hqQueue,
        _In_                    LPVOID                  lpData
);




_Success_(return != NULLPTR, ...)
PODNET_API
LPVOID
QueueNextInLine(
        _In_                    HQUEUE                  hqQueue
);




_Success_(return != MAX_ULONGLONG, ...)
PODNET_API
ULONGLONG
QueueNumberOfObjectsWaiting(
        _In_                    HQUEUE                  hqQueue
);



_Success_(return != FALSE, ...)
PODNET_API
BOOL
QueueSwap(
        _In_                    HQUEUE                  hqMain,
        _In_                    HQUEUE                  hqSwaperand
);