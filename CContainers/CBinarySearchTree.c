/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CBinarySearchTree.h"
#include "CValueComparison.c"


typedef struct __BS_TREE_NODE	BS_TREE_NODE, *LPBS_TREE_NODE;


struct __BS_TREE_NODE
{
	LPVOID 			lpData;
	LPBS_TREE_NODE		lpbstnLeft;
	LPBS_TREE_NODE		lpbstnRight;
};



typedef struct __BS_TREE 
{
	LPBS_TREE_NODE			lpbstnRoot;
	UARCHLONG 			ualDataSize;
	UARCHLONG 			ualLeaves;
	LPFN_BS_TREE_COMPARE_PROC	lpfnCompare;
	LPFN_BS_TREE_DATA_FREE_PROC	lpfnFree;
	TYPE 				tyData;
} BS_TREE, *LPBS_TREE;




_Success_(return != NULLPTR, _Non_Locking_)
INTERNAL_OPERATION
LPBS_TREE_NODE
__CreateNode(
	_In_		LPVOID 		lpData,
	_In_ 		UARCHLONG 	ualSize
){
	LPBS_TREE_NODE lpbstnLeaf;
	
	lpbstnLeaf = GlobalAllocAndZero(sizeof(BS_TREE_NODE));
	if (NULLPTR == lpbstnLeaf)
	{ return NULLPTR; }

	lpbstnLeaf->lpData = GlobalAllocAndZero(ualSize);
	if (NULLPTR == lpbstnLeaf)
	{
		FreeMemory(lpbstnLeaf);
		return NULLPTR;
	}

	CopyMemory(lpbstnLeaf->lpData, lpData, ualSize);
	return lpbstnLeaf; 
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__InsertNode(
	_In_Opt_ 	LPBS_TREE 			lpbstData,
	_In_ 		LPVOID 				lpData
){
	LPBS_TREE_NODE lpNode;
	LPBS_TREE_NODE lpCursor;
	LPBS_TREE_NODE lpPrevious;
	LONG lRet;

	lpNode = NULLPTR;
	lpCursor = lpbstData->lpbstnRoot;
	lpPrevious = NULLPTR;
	
	if (NULLPTR == lpbstData->lpbstnRoot)
	{ 
		lpNode = __CreateNode(lpData, lpbstData->ualDataSize);
		EXIT_IF_UNLIKELY_NULL(lpNode, FALSE);

		lpbstData->lpbstnRoot = lpNode;
		lpbstData->ualLeaves++;
		return TRUE;
	}

	while (NULLPTR != lpCursor)
	{
		lRet = lpbstData->lpfnCompare(lpCursor->lpData, lpData, lpbstData->ualDataSize);
		lpPrevious = lpCursor;

		/* Already Inserted */
		if (0 == lRet)
		{ return TRUE; }
		else if (-1 == lRet)
		{ lpCursor = lpCursor->lpbstnLeft; }
		else if (1 == lRet) 
		{ lpCursor = lpCursor->lpbstnRight; }
		else 
		{ return FALSE; }
	}

	lpNode = __CreateNode(lpData, lpbstData->ualDataSize);
	EXIT_IF_UNLIKELY_NULL(lpNode, FALSE);

	if (lRet == -1)
	{ lpPrevious->lpbstnLeft = lpNode;}
	else 
	{ lpPrevious->lpbstnRight = lpNode; }

	lpbstData->ualLeaves++;

	return TRUE;
}




_Success_(return != NULLPTR, _Non_Locking_)
INTERNAL_OPERATION
LPBS_TREE_NODE
__DestroySingleNode(
	_In_ 		LPBS_TREE			lpbstData,
	_In_ 		LPBS_TREE_NODE			lpbstnCurrent,
	_In_ 		LPVOID 				lpComparand
){
	LPBS_TREE_NODE lpNewRoot;
	LPBS_TREE_NODE lpParent;
	LPBS_TREE_NODE lpCursor;
	LONG lRet;

	lpNewRoot = lpbstnCurrent;
	lpParent = NULLPTR;
	lpCursor = NULLPTR;

	if (NULLPTR == lpbstnCurrent)
	{ return NULLPTR; }

	lRet = lpbstData->lpfnCompare(lpbstnCurrent->lpData, lpComparand, lpbstData->ualDataSize);
	if (-1 == lRet)
	{ lpbstnCurrent->lpbstnLeft = __DestroySingleNode(lpbstData, lpbstnCurrent->lpbstnLeft, lpComparand); }
	else if (1 == lRet)
	{ lpbstnCurrent->lpbstnRight = __DestroySingleNode(lpbstData, lpbstnCurrent->lpbstnRight, lpComparand); }
	else 
	{
		if (lpbstnCurrent->lpbstnLeft == NULLPTR)
		{
			lpCursor = lpbstnCurrent->lpbstnRight;

			if (NULLPTR != lpbstData->lpfnFree)
			{ lpbstData->lpfnFree(lpbstnCurrent->lpData); }
			else 
			{ FreeMemory(lpbstnCurrent->lpData); }

			FreeMemory(lpbstnCurrent);
			lpNewRoot = lpCursor;
		}
		else if (lpbstnCurrent->lpbstnRight == NULLPTR)
		{
			lpCursor = lpbstnCurrent->lpbstnLeft;
			
			if (NULLPTR != lpbstData->lpfnFree)
			{ lpbstData->lpfnFree(lpbstnCurrent->lpData); }
			else 
			{ FreeMemory(lpbstnCurrent->lpData); }

			FreeMemory(lpbstnCurrent);
			lpNewRoot = lpCursor;
		}
		else 	/* Two Children */
		{
			lpCursor = lpbstnCurrent->lpbstnRight;

			while (lpCursor->lpbstnLeft != NULLPTR)
			{
				lpParent = lpCursor;
				lpCursor = lpCursor->lpbstnLeft;
			}
			lpbstnCurrent->lpData = lpCursor->lpData;
			if (NULLPTR != lpParent)
			{ lpParent->lpbstnLeft = __DestroySingleNode(lpbstData, lpParent->lpbstnLeft, lpParent->lpbstnLeft->lpData); }
			else 
			{ lpParent->lpbstnRight = __DestroySingleNode(lpbstData, lpParent->lpbstnRight, lpParent->lpbstnRight->lpData); }
		}
	}

	return lpNewRoot;
}




_Success_(return != NULLPTR, _Non_Locking_)
INTERNAL_OPERATION
LPBS_TREE_NODE
__FindNode(
	_In_		LPBS_TREE			lpbstData,
	_In_		LPVOID 				lpData
){
	EXIT_IF_UNLIKELY_NULL(lpbstData, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpbstData->lpbstnRoot, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpData, NULLPTR);

	LPBS_TREE_NODE lpCursor;
	LONG lRet; 

	lpCursor = lpbstData->lpbstnRoot;
	while (NULLPTR != lpCursor)
	{
		lRet = lpbstData->lpfnCompare(lpCursor->lpData, lpData, lpbstData->ualDataSize);
		if (0 == lRet)
		{ return lpCursor; }
		else if (-1 == lRet)
		{ lpCursor = lpCursor->lpbstnLeft; }
		else if (1 == lRet) 
		{ lpCursor = lpCursor->lpbstnRight; }
		else 
		{ return NULLPTR; }
	}

	return lpCursor;
}



_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL 
__TraverseTree(
	_In_ 		LPBS_TREE 			lpbstData,
	_In_ 		LPBS_TREE_NODE 			lpbstnNode,
	_In_		LPFN_BS_TREE_TRAVERSE_PROC	lpfnCallBack,
	_In_Opt_ 	LPVOID 				lpUserData
){
	EXIT_IF_UNLIKELY_NULL(lpbstData, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpfnCallBack, FALSE);

	if (NULLPTR == lpbstnNode)
	{ lpbstnNode = lpbstData->lpbstnRoot; }

	if (NULLPTR != lpbstnNode->lpbstnLeft)
	{ __TraverseTree(lpbstData, lpbstnNode->lpbstnLeft, lpfnCallBack, lpUserData); }
	
	if (NULLPTR != lpbstnNode->lpData)
	{ lpfnCallBack(lpbstData, lpbstnNode, lpbstnNode->lpData, lpUserData); }

	if (NULLPTR != lpbstnNode->lpbstnRight)
	{ __TraverseTree(lpbstData, lpbstnNode->lpbstnRight, lpfnCallBack, lpUserData); }

	return TRUE;

}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL 
__DestroyNodeAndBelow(
	_In_ 		LPBS_TREE			lpbstData,
	_In_ 		LPBS_TREE_NODE			lpbstnCurrent
){
	BOOL bRet;
	bRet = TRUE;

	if (NULLPTR != lpbstData && NULLPTR != lpbstnCurrent)
	{
		bRet &= __DestroyNodeAndBelow(lpbstData, lpbstnCurrent->lpbstnLeft);
		bRet &= __DestroyNodeAndBelow(lpbstData, lpbstnCurrent->lpbstnRight);
		
		if (NULLPTR != lpbstData->lpfnFree)
		{ bRet &= lpbstData->lpfnFree(lpbstnCurrent->lpData); }
		else 
		{ FreeMemory(lpbstnCurrent->lpData); }
		FreeMemory(lpbstnCurrent);
	}

	return bRet; 
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HBINARYSEARCHTREE
CreateBinarySearchTree(
	_In_ 		TYPE 				tyDataType,
	_In_		UARCHLONG			ualDataTypeSize,
	_In_Opt_ 	LPFN_BS_TREE_COMPARE_PROC	lpfnCompare,
	_In_Opt_ 	LPFN_BS_TREE_DATA_FREE_PROC	lpfnDestroy
){
	LPBS_TREE lpbstData;
	
	lpbstData = GlobalAllocAndZero(sizeof(BS_TREE));
	EXIT_IF_UNLIKELY_NULL(lpbstData, FALSE);

	lpbstData->tyData = tyDataType;
	lpbstData->ualDataSize = ualDataTypeSize;
	lpbstData->lpfnCompare = lpfnCompare;
	lpbstData->lpfnFree = lpfnDestroy;

	if (NULLPTR == lpfnCompare)
	{
		switch (lpbstData->tyData)
		{
			case TYPE_CHAR:
			{
				lpbstData->lpfnCompare = __CompareChar;
				lpbstData->ualDataSize = sizeof(CHAR);
				break;
			}

			case TYPE_BYTE:
			{
				lpbstData->lpfnCompare = __CompareByte;
				lpbstData->ualDataSize = sizeof(BYTE);
				break;
			}

			case TYPE_SHORT:
			{
				lpbstData->lpfnCompare = __CompareShort;
				lpbstData->ualDataSize = sizeof(SHORT);
				break;
			}

			case TYPE_USHORT:
			{
				lpbstData->lpfnCompare = __CompareUnsignedShort;
				lpbstData->ualDataSize = sizeof(USHORT);
				break;
			}

			case TYPE_LONG:
			{
				lpbstData->lpfnCompare = __CompareLong;
				lpbstData->ualDataSize = sizeof(LONG);
				break;
			}

			case TYPE_ULONG:
			{
				lpbstData->lpfnCompare = __CompareUnsignedLong;
				lpbstData->ualDataSize = sizeof(ULONG);
				break;
			}

			case TYPE_LONGLONG:
			{
				lpbstData->lpfnCompare = __CompareLongLong;
				lpbstData->ualDataSize = sizeof(LONGLONG);
				break;
			}

			case TYPE_ULONGLONG:
			{
				lpbstData->lpfnCompare = __CompareUnsignedLongLong;
				lpbstData->ualDataSize = sizeof(ULONGLONG);
				break;
			}

			case TYPE_ULTRALONG:
			{
				lpbstData->lpfnCompare = __CompareUltraLong;
				lpbstData->ualDataSize = sizeof(ULTRALONG);
				break;
			}

			case TYPE_UULTRALONG:
			{
				lpbstData->lpfnCompare = __CompareUnsignedUltraLong;
				lpbstData->ualDataSize = sizeof(UULTRALONG);
				break;
			}

			case TYPE_ARCHLONG:
			{
				lpbstData->lpfnCompare = __CompareArchitectureLong;
				lpbstData->ualDataSize = sizeof(ARCHLONG);
				break;
			}

			case TYPE_UARCHLONG:
			{
				lpbstData->lpfnCompare = __CompareUnsignedArchitectureLong;
				lpbstData->ualDataSize = sizeof(UARCHLONG);
				break;
			}

			case TYPE_LPVOID:
			{
				lpbstData->lpfnCompare = __CompareLocalPointerVoid;
				lpbstData->ualDataSize = sizeof(LPVOID);
				break;
			}

			case TYPE_DLPVOID:
			{
				lpbstData->lpfnCompare = __CompareDoubleLocalPointerVoid;
				lpbstData->ualDataSize = sizeof(DLPVOID);
				break;
			}

			case TYPE_HANDLE:
			{
			lpbstData->lpfnCompare = __CompareChar;
				lpbstData->ualDataSize = sizeof(HANDLE);
				break;
			}

			case TYPE_WORD:
			{
				lpbstData->lpfnCompare = __CompareUnsignedShort;
				lpbstData->ualDataSize = sizeof(WORD);
				break;
			}

			case TYPE_DWORD:
			{
				lpbstData->lpfnCompare = __CompareUnsignedLong;
				lpbstData->ualDataSize = sizeof(DWORD);
				break;
			}

			case TYPE_QWORD:
			{
				lpbstData->lpfnCompare = __CompareUnsignedLongLong;
				lpbstData->ualDataSize = sizeof(QWORD);
				break;
			}

			case TYPE_LPSTR:
			{
				lpbstData->lpfnCompare = __CompareLocalPointerNullTerminatedString;
				lpbstData->ualDataSize = sizeof(LPSTR);
				break;
			}

			case TYPE_DLPSTR:
			{
				lpbstData->lpfnCompare = __CompareDoubleLocalPointerVoid;
				lpbstData->ualDataSize = sizeof(DLPSTR);
				break;
			}

			default:
			{ 
				return NULLPTR;
				break;
			}
		}
	}

	return (HBINARYSEARCHTREE)lpbstData;
}



_Success_(return != FALSE, _Non_Locking_)
PODNET_API 
BOOL 
DestroyBinarySearchTree(
	_In_ 		HBINARYSEARCHTREE		hbstTree
){
	EXIT_IF_UNLIKELY_NULL(hbstTree, FALSE);
	LPBS_TREE lpbstData;
	lpbstData = (LPBS_TREE)hbstTree;

	__DestroyNodeAndBelow(lpbstData, lpbstData->lpbstnRoot);
	FreeMemory(lpbstData);

	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL 
BinarySearchTreeInsert(
	_In_		HBINARYSEARCHTREE		hbstTree,
	_In_ 		LPVOID 				lpData,
	_In_ 		UARCHLONG 			ualDataSize
){
	LPBS_TREE lpbstData;
	BOOL bRet;

	lpbstData = (LPBS_TREE)hbstTree;

	if (ualDataSize != lpbstData->ualDataSize)
	{ return FALSE; }

	bRet = __InsertNode(lpbstData, lpData);

	return bRet;
}



_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API 
LPVOID 
BinarySearchTreeSearch(
	_In_		HBINARYSEARCHTREE		hbstTree,
	_In_ 		LPVOID 				lpData,
	_In_ 		UARCHLONG 			ualDataSize
){
	LPBS_TREE lpbstData;
	lpbstData = (LPBS_TREE)hbstTree;

	if (ualDataSize != lpbstData->ualDataSize)
	{ return FALSE; }
	
	return __FindNode(lpbstData, lpData);
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API 
BOOL 
BinarySearchTreeTraverse(
	_In_ 		HBINARYSEARCHTREE		hbstTree,
	_In_ 		LPFN_BS_TREE_TRAVERSE_PROC	lpfnCallBack,
	_In_Opt_	LPVOID 				lpUserData
){
	EXIT_IF_LIKELY_NULL(hbstTree, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpfnCallBack, FALSE);

	return __TraverseTree(hbstTree, NULLPTR, lpfnCallBack, lpUserData);
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API 
LPVOID 
BinarySearchTreeAt(
	_In_ 		HBINARYSEARCHTREE		hbstTree,
	_In_ 		LPVOID 				lpNode
){
	UNREFERENCED_PARAMETER(hbstTree);
	EXIT_IF_UNLIKELY_NULL(hbstTree, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpNode, NULLPTR);

	LPBS_TREE_NODE lpbstnNode;

	lpbstnNode = (LPBS_TREE_NODE)lpNode;
	return lpbstnNode->lpData;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API 
BOOL 
BinarySearchTreeDestroyAt(
	_In_		HBINARYSEARCHTREE		hbstTree,
	_In_ 		LPVOID 				lpNode
){
	EXIT_IF_UNLIKELY_NULL(hbstTree, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpNode, FALSE);

	LPBS_TREE lpbstData;
	LPBS_TREE_NODE lpbstnNode;

	lpbstData = (LPBS_TREE)hbstTree;
	lpbstnNode = (LPBS_TREE_NODE)lpNode;

	return (0 != __DestroySingleNode(lpbstData, lpbstnNode, BinarySearchTreeAt(hbstTree, lpNode))) ? TRUE : FALSE;
}
