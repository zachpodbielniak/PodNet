/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CMap.h"


#define VALID_MAP_HANDLE                        0xAC

#define CHECK_FOR_VALID_HMAP(hMap)              if (hMap == NULLPTR || \ 
                                                ((hMap->bValidHandle) ^ VALID_MAP_HANDLE) != 0) \
                                                        return NULLPTR

typedef struct _MAP
{
        BYTE bValidHandle;
        BYTE bInUse;
        HVECTOR hvKey;
        HVECTOR hvMapped;
        ULONG ulIndex;
        ULONG ulKeyObjectSize;
        ULONG ulMapObjectSize;
} MAP, *LPMAP;




PODNET_API
HMAP
CreateMap(
	_In_                                    ULONG                   ulSizeKey,
	_In_                                    ULONG                   ulSizeMapped,
	_In_                                    ULONG                   ulNumberOfObjects,
	_In_Opt_                                ULONG                   ulFlags
){
        UNREFERENCE_PARAMETER(ulFlags);
        if (0 == ulSizeKey || 0 == ulSizeMapped)
                return NULLPTR;
        HMAP hMap;
        hMap = LocalAllocAndZero(sizeof(MAP));
        EXIT_IF_NULL(hMap, NULLPTR);

        hMap->bValidHandle = VALID_MAP_HANDLE;
        hMap->ulIndex = 0;
        hMap->ulKeyObjectSize = ulSizeKey;
        hMap->ulMapObjectSize = ulSizeMapped;
        hMap->hvKey = CreateVector(ulNumberOfObjects, ulSizeKey, NULL);
        hMap->hvMapped = CreateVector(ulNumberOfObjects, ulSizeMapped, NULL);

        return hMap;
}




PODNET_API
BOOL
CloseMap(
	_In_                                    HMAP                    hMap,
	_In_Opt_                                ULONG                   ulFlags
){
        UNREFERENCED_PARAMETER(ulFlags);
        CHECK_FOR_VALID_HMAP(hMap);
        BOOL bRet;
        bRet = TRUE;
        bRet &= CloseVector(hMap->hvMapped);
        bRet &= CloseVector(hMap->hvKey);
        ZeroMemory(hMap, sizeof(MAP));
        FreeMemory(hMap);
        return bRet;
}


PODNET_API
BOOL
CloseMapEx(
	_In_                                    HMAP                    hMap,
	_In_                                    LPFN_CLOSE_MAP_ROUTINE  lpfnKeyCloser,
	_In_Opt_ _When_(lpfnKeyCloser != NULL)  ULONG                   ulFlagsKeyCloser,
	_In_                                    LPFN_CLOSE_MAP_ROUTINE  lpfnMappedCloser,
	_In_Opt_ _When_(lpfnKeyCloser != NULL)  ULONG                   ulFlagsMappedCloser,
	_In_Opt_                                ULONG                   ulFlags
);


PODNET_API
BOOL
MapInsert(
	_In_                                    HMAP                    hMap,
	_In_                                    LPVOID                  lpKeyData,
	_In_                                    LPVOID                  lpMappedData,
	_In_Opt_                                ULONG                   ulFlags
){
        UNREFERENCED_PARAMETER(ulFlags);
        CHECK_FOR_VALID_HMAP(hMap);
        VectorPushBack(hMap->hvKey, lpKeyData, NULL);
        VectorPushBack(hMap->hvMapped, lpMappedData, NULL);
        hMap->ulIndex++;
        return TRUE;
}

PODNET_API
LPVOID
MapValueAtKey(
	_In_                                    HMAP                    hMap,
	_In_                                    LPVOID                  lpKey,
	_In_Opt_                                ULONG                   ulFlags
){
        UNREFERENCED_PARAMETER(ulFlags);
        CHECK_FOR_VALID_HMAP(hMap);
        ULONG ulAccess;
        VECTOR_ITERATOR(hMap->hvKey, lpIter)
        {
                if (CompareMemory(lpIter, lpKey, hMap->ulMapObjectSize) == 0)
                        ulAccess = (ULONG)(LPVOID)(((ULONGLONG)lpIter - (ULONGLONG)VectorFront(hMap->hvKey)) / hMap->ulKeyObjectSize);
        }
        return VectorAt(hMap->hvMapped, ulAccess);
}