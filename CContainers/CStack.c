/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CStack.h"


typedef struct _STACK
{
        HANDLE                  hLock;
        HLINKEDLIST             hllStack;
} STACK, *LPSTACK;




_Success_(return != NULLPTR, ...)
PODNET_API
HSTACK
CreateStack(
        _In_                    ULONGLONG               ullObjectSize,
        _In_Opt_                HANDLE                  hLock
){
        LPSTACK lpStack;

        lpStack = LocalAlloc(sizeof(STACK));
	/* cppcheck-suppress memleak */
	EXIT_IF_UNLIKELY_NULL(lpStack, NULLPTR);

        lpStack->hLock = hLock;
        lpStack->hllStack = CreateLinkedList(ullObjectSize, TRUE, hLock);
        return (HSTACK)lpStack;
}




_Success_(return != FALSE, ...)
PODNET_API
BOOL
DestroyStack(
        _In_                    HSTACK                  hsStack
){
	EXIT_IF_UNLIKELY_NULL(hsStack, FALSE);
        LPSTACK lpStack;
        lpStack = (LPSTACK)hsStack;
        DestroyLinkedList(lpStack->hllStack);
        FreeMemory(lpStack);
        return TRUE;
}




_Success_(return != FALSE, ...)
PODNET_API
BOOL
StackPush(
        _In_                    HSTACK                  hsStack,
        _In_                    LPVOID                  lpData
){ 
	EXIT_IF_UNLIKELY_NULL(hsStack, FALSE);
	return LinkedListAttachBack(((LPSTACK)hsStack)->hllStack, lpData); 
}




_Success_(return != NULLPTR, ...)
PODNET_API
LPVOID
StackPop(
        _In_                    HSTACK                  hsStack
){
	EXIT_IF_UNLIKELY_NULL(hsStack, NULLPTR);
	return LinkedListDetachBack(((LPSTACK)hsStack)->hllStack); 
}




_Success_(return != NULLPTR, ...)
PODNET_API
LPVOID
StackTop(
        _In_                    HSTACK                  hsStack
){ 
	EXIT_IF_UNLIKELY_NULL(hsStack, NULLPTR);
	return LinkedListBack(((LPSTACK)hsStack)->hllStack); 
}




_Success_(return != NULLPTR, ...)
PODNET_API
BOOL
StackSwap(
        _In_                    HSTACK                  hsStackMain,
        _In_                    HSTACK                  hsStackSwaperand
){
	EXIT_IF_UNLIKELY_NULL(hsStackMain, FALSE);
	EXIT_IF_UNLIKELY_NULL(hsStackSwaperand, FALSE);
        LPSTACK lpStackMain, lpStackSwaperand;
        lpStackMain = (LPSTACK)hsStackMain;
        lpStackSwaperand = (LPSTACK)hsStackSwaperand;
        XOR_SWAP_PTR(lpStackMain->hllStack, lpStackSwaperand->hllStack);
        return TRUE;
}