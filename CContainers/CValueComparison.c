/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CVALUECOMPARISON_C
#define CVALUECOMPARISON_C


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"


#define COMPARE_MACRO(T)					\
	UNREFERENCED_PARAMETER(ualSize);			\
	T __L;							\
	T __C;							\
	__L = *(T*)lpLeafValue;					\
	__C = *(T*)lpComparandValue;				\
	if (__L == __C)						\
	{ return 0; }						\
	else if (__L > __C) 					\
	{ return -1; }						\
	else 							\
	{ return 1; }						\
	return MAX_LONG;			




_Call_Back_
_Success_(return != FALSE, _Non_Locking_)
LONG 
__CompareChar(
	_In_ 		LPVOID 		lpLeafValue,
	_In_ 		LPVOID 		lpComparandValue,
	_In_ 		UARCHLONG	ualSize
){ COMPARE_MACRO(CHAR); }




_Call_Back_
_Success_(return != FALSE, _Non_Locking_)
LONG 
__CompareByte(
	_In_ 		LPVOID 		lpLeafValue,
	_In_ 		LPVOID 		lpComparandValue,
	_In_ 		UARCHLONG	ualSize
){ COMPARE_MACRO(BYTE); }




_Call_Back_
_Success_(return != FALSE, _Non_Locking_)
LONG 
__CompareShort(
	_In_ 		LPVOID 		lpLeafValue,
	_In_ 		LPVOID 		lpComparandValue,
	_In_ 		UARCHLONG	ualSize
){ COMPARE_MACRO(SHORT); }




_Call_Back_
_Success_(return != FALSE, _Non_Locking_)
LONG 
__CompareUnsignedShort(
	_In_ 		LPVOID 		lpLeafValue,
	_In_ 		LPVOID 		lpComparandValue,
	_In_ 		UARCHLONG	ualSize
){ COMPARE_MACRO(USHORT); }




_Call_Back_
_Success_(return != FALSE, _Non_Locking_)
LONG 
__CompareLong(
	_In_ 		LPVOID 		lpLeafValue,
	_In_ 		LPVOID 		lpComparandValue,
	_In_ 		UARCHLONG	ualSize
){ COMPARE_MACRO(LONG); }




_Call_Back_
_Success_(return != FALSE, _Non_Locking_)
LONG 
__CompareUnsignedLong(
	_In_ 		LPVOID 		lpLeafValue,
	_In_ 		LPVOID 		lpComparandValue,
	_In_ 		UARCHLONG	ualSize
){ COMPARE_MACRO(ULONG); }




_Call_Back_
_Success_(return != FALSE, _Non_Locking_)
LONG 
__CompareLongLong(
	_In_ 		LPVOID 		lpLeafValue,
	_In_ 		LPVOID 		lpComparandValue,
	_In_ 		UARCHLONG	ualSize
){ COMPARE_MACRO(LONGLONG); }




_Call_Back_
_Success_(return != FALSE, _Non_Locking_)
LONG 
__CompareUnsignedLongLong(
	_In_ 		LPVOID 		lpLeafValue,
	_In_ 		LPVOID 		lpComparandValue,
	_In_ 		UARCHLONG	ualSize
){ COMPARE_MACRO(ULONGLONG); }




_Call_Back_
_Success_(return != FALSE, _Non_Locking_)
LONG 
__CompareUltraLong(
	_In_ 		LPVOID 		lpLeafValue,
	_In_ 		LPVOID 		lpComparandValue,
	_In_ 		UARCHLONG	ualSize
){ COMPARE_MACRO(ULTRALONG); }




_Call_Back_
_Success_(return != FALSE, _Non_Locking_)
LONG 
__CompareUnsignedUltraLong(
	_In_ 		LPVOID 		lpLeafValue,
	_In_ 		LPVOID 		lpComparandValue,
	_In_ 		UARCHLONG	ualSize
){ COMPARE_MACRO(UULTRALONG); }




_Call_Back_
_Success_(return != FALSE, _Non_Locking_)
LONG 
__CompareArchitectureLong(
	_In_ 		LPVOID 		lpLeafValue,
	_In_ 		LPVOID 		lpComparandValue,
	_In_ 		UARCHLONG	ualSize
){ COMPARE_MACRO(ARCHLONG); }




_Call_Back_
_Success_(return != FALSE, _Non_Locking_)
LONG 
__CompareUnsignedArchitectureLong(
	_In_ 		LPVOID 		lpLeafValue,
	_In_ 		LPVOID 		lpComparandValue,
	_In_ 		UARCHLONG	ualSize
){ COMPARE_MACRO(UARCHLONG); }




_Call_Back_
_Success_(return != FALSE, _Non_Locking_)
LONG 
__CompareLocalPointerVoid(
	_In_ 		LPVOID 		lpLeafValue,
	_In_ 		LPVOID 		lpComparandValue,
	_In_ 		UARCHLONG	ualSize
){ return CompareMemory(lpLeafValue, lpComparandValue, ualSize); }




_Call_Back_
_Success_(return != FALSE, _Non_Locking_)
LONG 
__CompareDoubleLocalPointerVoid(
	_In_ 		LPVOID 		lpLeafValue,
	_In_ 		LPVOID 		lpComparandValue,
	_In_ 		UARCHLONG	ualSize
){ COMPARE_MACRO(DLPVOID); }




_Call_Back_
_Success_(return != FALSE, _Non_Locking_)
LONG 
__CompareLocalPointerNullTerminatedString(
	_In_ 		LPVOID 		lpLeafValue,
	_In_ 		LPVOID 		lpComparandValue,
	_In_ 		UARCHLONG 	ualSize
){
	UNREFERENCED_PARAMETER(ualSize);
	LPSTR __L;
	LPSTR __C;
	__L = *(DLPSTR)lpLeafValue;
	__C = *(DLPSTR)lpComparandValue;
	if (NULLPTR == lpLeafValue)
	{ return MAX_ULONG; }
	else if (NULLPTR == lpComparandValue)
	{ return MAX_ULONG; }
	else 
	{ return StringCompare(__L, __C); }

	return MAX_ULONG;
}





#endif