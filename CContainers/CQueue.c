/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CQueue.h"


typedef struct _QUEUE
{
        HANDLE          hLock;
        HLINKEDLIST     hllQueue;
} QUEUE, *LPQUEUE;



_Success_(return != NULLPTR, ...)
PODNET_API
HQUEUE
CreateQueue(
        _In_                    ULONGLONG               ullObjectSize,
        _In_Opt_                HANDLE                  hLock
){
        LPQUEUE lpqQueue;

        lpqQueue = LocalAlloc(sizeof(QUEUE));
	/* cppcheck-suppress memleak */
	EXIT_IF_UNLIKELY_NULL(lpqQueue, NULLPTR);

        lpqQueue->hLock = hLock;
        lpqQueue->hllQueue = CreateLinkedList(ullObjectSize, FALSE, hLock);

        return (HQUEUE)lpqQueue;
}




_Success_(return != FALSE, ...)
PODNET_API
BOOL
DestroyQueue(
        _In_                    HQUEUE                  hqQueue
){
	EXIT_IF_UNLIKELY_NULL(hqQueue, FALSE);

        LPQUEUE lpqQueue;
        lpqQueue = (LPQUEUE)hqQueue;
        DestroyLinkedList(lpqQueue->hllQueue);
        FreeMemory(lpqQueue);
        return TRUE;
}




_Success_(return != FALSE, ...)
PODNET_API
BOOL
QueuePush(
        _In_                    HQUEUE                  hqQueue,
        _In_                    LPVOID                  lpData
){ 
	EXIT_IF_UNLIKELY_NULL(hqQueue, FALSE);
	return LinkedListAttachBack(((LPQUEUE)hqQueue)->hllQueue, lpData); 
}




_Success_(return != NULLPTR, ...)
PODNET_API
LPVOID
QueueNextInLine(
        _In_                    HQUEUE                  hqQueue
){ 
	EXIT_IF_UNLIKELY_NULL(hqQueue, NULLPTR);
	return LinkedListDetachFront(((LPQUEUE)hqQueue)->hllQueue);
}




_Success_(return != MAX_ULONGLONG, ...)
PODNET_API
ULONGLONG
QueueNumberOfObjectsWaiting(
        _In_                    HQUEUE                  hqQueue
){ 
	EXIT_IF_UNLIKELY_NULL(hqQueue, MAX_ULONGLONG);
	return LinkedListSize(((LPQUEUE)hqQueue)->hllQueue); 
}




_Success_(return != FALSE, ...)
PODNET_API
BOOL
QueueSwap(
        _In_                    HQUEUE                  hqMain,
        _In_                    HQUEUE                  hqSwaperand
){
	EXIT_IF_UNLIKELY_NULL(hqMain, FALSE);
	EXIT_IF_UNLIKELY_NULL(hqSwaperand, FALSE);
	
        LPQUEUE lpqMain, lpqSwaperand;
        lpqMain = (LPQUEUE)hqMain;
        lpqSwaperand = (LPQUEUE)hqSwaperand;
        XOR_SWAP_PTR(lpqMain->hllQueue, lpqSwaperand->hllQueue);
        return TRUE;
}