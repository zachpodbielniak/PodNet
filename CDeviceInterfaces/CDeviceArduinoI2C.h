/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CDEVICEARDUINOI2C_H
#define CDEVICEARDUINOI2C_H


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../CHandle/CHandle.h"



_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HANDLE
CreateArduinoConnectionOverI2C(
        _In_            ULONG           ulPin
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
WriteArduinoI2CRegisterByte(
        _In_            HANDLE          hArduino,
        _In_            ULONG           ulPin,
        _In_            BYTE            byValue
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
WriteArduinoI2CRegisterShort(
        _In_            HANDLE          hArduino,
        _In_            ULONG           ulPin,
        _In_            USHORT          usValue
);




_Success_(_Inexpressible_, _Non_Locking_)
PODNET_API
BYTE
ReadArduinoI2CRegisterByte(
        _In_            HANDLE          hArduino,
        _In_            ULONG           ulRegister
);




_Success_(_Inexpressible_, _Non_Locking_)
PODNET_API
USHORT
ReadArduinoI2CRegisterShort(
        _In_            HANDLE          hArduino,
        _In_            ULONG           ulRegister
);



_Success_(_Inexpressible_, _Non_Locking_)
PODNET_API
BOOL
IssueArduinoI2CCommand(
        _In_            HANDLE          hArduino,
        _In_            USHORT          usCommand,
        _In_            USHORT          usParam
);




_Success_(_Inexpressible_, _Non_Locking_)
PODNET_API
USHORT
RequestArduinoI2CResponse(
        _In_            HANDLE          hArduino
);




_Success_(_Inexpressible_, _Non_Locking_)
PODNET_API
USHORT
IssueArduinoI2CCommandAndWaitForResponse(
        _In_            HANDLE          hArduino,
        _In_            USHORT          usCommand,
        _In_            USHORT          usParam
);




_Success_(_Inexpressible_, _Non_Locking_)
PODNET_API
USHORT
ExchangeArduinoI2CCommandAndResponse(
        _In_            HANDLE          hArduino,
        _In_            USHORT          usCommand,
        _In_            USHORT          usParam
);


#endif