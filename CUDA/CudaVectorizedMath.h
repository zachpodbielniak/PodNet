/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


/*

  ____          _     __     __        _             _             _ __  __       _   _       _
 / ___|   _  __| | __ \ \   / /__  ___| |_ ___  _ __(_)_______  __| |  \/  | __ _| |_| |__   | |__
| |  | | | |/ _` |/ _` \ \ / / _ \/ __| __/ _ \| '__| |_  / _ \/ _` | |\/| |/ _` | __| '_ \  | '_ \
| |__| |_| | (_| | (_| |\ V /  __/ (__| || (_) | |  | |/ /  __/ (_| | |  | | (_| | |_| | | |_| | | |
 \____\__,_|\__,_|\__,_| \_/ \___|\___|\__\___/|_|  |_/___\___|\__,_|_|  |_|\__,_|\__|_| |_(_)_| |_|

*/

#ifndef CUDAVECTORIZEDMATH_H
#define CUDAVECTORIZEDMATH_H

#include "CudaDefs.h"

enum CUDA_VECTORIZED_MATH_FLAGS
{
	CUDA_VECTORIZED_MATH_UNSIGNED_OPERATION = 1 << 0,
	CUDA_VECTORIZED_MATH_FLOAT_OPERATION = 1 << 1,
	CUDA_VECTORIZED_MATH_DOUBLE_OPERATION =	1 << 2
};

/*
//This function will take in two vectors
//and use CUDA to accelerate the speed
//of addition.
//Param: lpVectorOne
//	Description: Local Pointer to
//	  first vector.
//
//Param: lpVectorTwo
//	Description: Local Pointer to
//	  second vector.
//
//Param: lpVectorOut
//	Description: Local Pointer to
//	  vector that will store the
//	  results of the calculation.
//
//Param: ulDataSize
//	Description: Size of the data
//	  type you wish to operate on,
//	  for example: sizeof(ULONG)
//
//Param: ulNumberOfElements
//	Description: The number of
//	  elements that are in the
//	  vector.
//
//Param: ulFlags
//	Description: This is reserved
//	  for future use, this must
//	  be NULL.
*/
EXTERN
BOOL
CudaVectorizedAddition(
	LPVOID 		lpVectorOne,
	LPVOID 		lpVectorTwo,
	LPVOID 		lpVectorOut,
	ULONG 		ulDataSize,
	ULONG 		ulNumberOfElements,
	ULONG 		ulFlags
);


/*
//This function will take in two vectors
//and use CUDA to accelerate the speed
//of subtraction.
//Param: lpVectorOne
//	Description: Local Pointer to
//	  first vector.
//
//Param: lpVectorTwo
//	Description: Local Pointer to
//	  second vector.
//
//Param: lpVectorOut
//	Description: Local Pointer to
//	  vector that will store the
//	  results of the calculation.
//
//Param: ulDataSize
//	Description: Size of the data
//	  type you wish to operate on,
//	  for example: sizeof(ULONG)
//
//Param: ulNumberOfElements
//	Description: The number of
//	  elements that are in the
//	  vector.
//
//Param: ulFlags
//	Description: This is reserved
//	  for future use, this must
//	  be NULL.
*/
EXTERN
BOOL
CudaVectorizedSubtraction(
	LPVOID 		lpVectorOne,
	LPVOID		lpVectorTwo,
	LPVOID 		lpVectorOut,
	ULONG 		ulDataSize,
	ULONG 		ulNumberOfElements,
	ULONG 		ulFlags
);



/*
//This function will take in two vectors
//and use CUDA to accelerate the speed
//of multiplication.
//Param: lpVectorOne
//	Description: Local Pointer to
//	  first vector.
//
//Param: lpVectorTwo
//	Description: Local Pointer to
//	  second vector.
//
//Param: lpVectorOut
//	Description: Local Pointer to
//	  vector that will store the
//	  results of the calculation.
//
//Param: ulDataSize
//	Description: Size of the data
//	  type you wish to operate on,
//	  for example: sizeof(ULONG)
//
//Param: ulNumberOfElements
//	Description: The number of
//	  elements that are in the
//	  vector.
//
//Param: ulFlags
//	Description: This is reserved
//	  for future use, this must
//	  be NULL.
*/
EXTERN
BOOL
CudaVectorizedMultiplication(
	LPVOID		lpVectorOne,
	LPVOID		lpVectorTwo,
	LPVOID		lpVectorOut,
	ULONG		ulDataSize,
	ULONG		ulNumberOfElements,
	ULONG		ulFlags
);


/*
//This function will take in two vectors
//and use CUDA to accelerate the speed
//of division.
//Param: lpVectorOne
//	Description: Local Pointer to
//	  first vector (number to be
//	  divided)
//
//Param: lpVectorTwo
//	Description: Local Pointer to
//	  second vector.
//
//Param: lpVectorOut
//	Description: Local Pointer to
//	  vector that will store the
//	  results of the calculation.
//
//Param: lpVectorRemainder
//	Description: Local Pointer to
//	  vector that will store the
//	  remainder.  This can be
//	  NULL.
//
//Param: ulDataSize
//	Description: Size of the data
//	  type you wish to operate on,
//	  for example: sizeof(ULONG)
//
//Param: ulNumberOfElements
//	Description: The number of
//	  elements that are in the
//	  vector.
//
//Param: ulFlags
//	Description: This is reserved
//	  for future use, this must
//	  be NULL.
*/
EXTERN
BOOL
CudaVectorizedDivision(
	LPVOID		lpVectorOne,
	LPVOID		lpVectorTwo,
	LPVOID		lpVectorOut,
	LPVOID		lpVectorRemainder,
	ULONG		ulDataSize,
	ULONG		ulNumberOfElements,
	ULONG		ulFlags
);



/*
//This function will take in two vectors
//and use CUDA to accelerate the speed
//of exponents.
//Param: lpVectorOne
//	Description: Local Pointer to
//	  first vector (the base
//	  number).
//
//Param: lpVectorTwo
//	Description: Local Pointer to
//	  second vector. (The exponent)
//
//Param: lpVectorOut
//	Description: Local Pointer to
//	  vector that will store the
//	  results of the calculation.
//
//Param: lpVectorRemainder
//	Description: Local Pointer to
//	  vector that will store the
//	  remainder.  This can be
//	  NULL.
//
//Param: ulDataSize
//	Description: Size of the data
//	  type you wish to operate on,
//	  for example: sizeof(ULONG)
//
//Param: ulNumberOfElements
//	Description: The number of
//	  elements that are in the
//	  vector.
//
//Param: ulFlags
//	Description: This is reserved
//	  for future use, this must
//	  be NULL.
*/
EXTERN
BOOL
CudaVectorizedPower(
	LPVOID		lpVectorOne,
	LPVOID		lpVectorTwo,
	LPVOID		lpVectorOut,
	ULONG		ulDataSize,
	ULONG		ulNumberOfElements,
	ULONG		ulFlags
);




/*
//This function will take in two vectors
//and use CUDA to accelerate the speed
//of exponents.
//Param: lpVector
//	Description: Local Pointer to
//	  the vector (the base
//	  number).
//
//Param: bPower
//	Description: Value that
//	  represents the scalar exponent.
//
//Param: lpVectorOut
//	Description: Local Pointer to
//	  vector that will store the
//	  results of the calculation.
//
//Param: ulDataSize
//	Description: Size of the data
//	  type you wish to operate on,
//	  for example: sizeof(ULONG)
//
//Param: ulNumberOfElements
//	Description: The number of
//	  elements that are in the
//	  vector.
//
//Param: ulFlags
//	Description: Pass in any
//	  relevant flags.
*/
EXTERN
BOOL
CudaVectorizedScalarPower(
	LPVOID		lpVector,
	BYTE		bPower,
	LPVOID 		lpVectorOut,
	ULONG		ulDataSize,
	ULONG		ulNumberOfElements,
	ULONG		ulFlags
);





/* Scalar Functions */
EXTERN
BOOL
CudaVectorizedScalarAddition(
	LPVOID		lpVector,
	LPVOID		lpScalarand,
	LPVOID		lpVectorOut,
	ULONG		ulDataSize,
	ULONG 		ulNumberOfElements,
	ULONG		ulFlags
);





EXTERN
BOOL
CudaVectorizedScalarSubtraction(
	LPVOID		lpVector,
	LPVOID		lpScalarand,
	LPVOID		lpVectorOut,
	ULONG		ulDataSize,
	ULONG		ulNumberOfElements,
	ULONG		ulFlags
);





EXTERN
BOOL
CudaVectorizedScalarMultiplication(
	LPVOID		lpVector,
	LPVOID		lpScalarand,
	LPVOID		lpVectorOut,
	ULONG		ulDataSize,
	ULONG		ulNumberOfElements,
	ULONG		ulFlags
);





EXTERN
BOOL
CudaVectorizedScalarDivision(
	LPVOID		lpVector,
	LPVOID		lpScalarand,
	LPVOID		lpVectorOut,
	LPVOID		lpVectorRemainder,
	ULONG		ulDataSize,
	ULONG		ulNumberOfElements,
	ULONG		ulFlags
);




//Random Number Functions
EXTERN
BOOL
CudaVectorizedRandomNumbers(
	LPVOID 		lpVector,
	ULONG		ulDataSize,
	ULONG		ulNumberOfElements,
	ULONG		ulSeed,
	ULONG		ulFlags
);



























#endif
