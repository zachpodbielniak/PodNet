/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CUDADEFS_H
#define CUDADEFS_H
#include "Prerequisites.h"


#ifdef __cplusplus
#define EXTERN extern "C"
#else
#define EXTERN extern
#endif

#ifndef NUMBER_TYPES
#define NUMBER_TYPES
typedef char 			CHAR;
typedef unsigned char 		UCHAR;
typedef UCHAR 			BYTE;
typedef BYTE 			BOOL;
typedef short int 		SHORT;
typedef unsigned short 		USHORT;
typedef long 			LONG;
typedef unsigned long 		ULONG;
typedef long long 		LONGLONG;
typedef unsigned long long 	ULONGLONG;
typedef float                   FLOAT;
typedef double                  DOUBLE;
typedef void 			VOID;
#endif

#ifndef HOST_POINTER_TYPES
#define HOST_POINTER_TYPES
/* Host Pointer Typedefs */
typedef CHAR 			*LPCHAR;
typedef BYTE 			*LPBYTE;
typedef SHORT 			*LPSHORT;
typedef USHORT 			*LPUSHORT;
typedef LONG 			*LPLONG;
typedef ULONG 			*LPULONG;
typedef LONGLONG 		*LPLONGLONG;
typedef ULONGLONG 		*LPULONGLONG;
typedef FLOAT                   *LPFLOAT;
typedef DOUBLE                  *LPDOUBLE;
typedef VOID 			*LPVOID;
#endif


#ifndef GPU_POINTER_TYPES
#define GPU_POINTER_TYPES
/* GPU Pointer Typedefs */
typedef CHAR 			*GPCHAR;
typedef BYTE 			*GPBYTE;
typedef SHORT 			*GPSHORT;
typedef USHORT 			*GPUSHORT;
typedef LONG 			*GPLONG;
typedef ULONG 			*GPULONG;
typedef LONGLONG 		*GPLONGLONG;
typedef ULONGLONG 		*GPULONGLONG;
typedef FLOAT                   *GPFLOAT;
typedef DOUBLE                  *GPDOUBLE;
typedef VOID 			*GPVOID;
#endif


#ifndef CUDA_ALIASES
#define CUDA_ALIASES
/* Define 'over-rides' */
#define CudaCopyMemory		cudaMemcpy
#define CudaAlloc		cudaMalloc
#define CudaFree		cudaFree

#define CPU_TO_GPU		cudaMemcpyHostToDevice
#define GPU_TO_CPU		cudaMemcpyDeviceToHost

#define CUDA_FUNCTION		__global__
#define CUDA_SHARED		__shared__
#define CUDA_STATIC		__device__
#define CPU_FUNCTION		__host__
#endif
#endif
