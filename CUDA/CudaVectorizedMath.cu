/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


/*

  ____          _     __     __        _             _             _ __  __       _   _
 / ___|   _  __| | __ \ \   / /__  ___| |_ ___  _ __(_)_______  __| |  \/  | __ _| |_| |__    ___ _   _
| |  | | | |/ _` |/ _` \ \ / / _ \/ __| __/ _ \| '__| |_  / _ \/ _` | |\/| |/ _` | __| '_ \  / __| | | |
| |__| |_| | (_| | (_| |\ V /  __/ (__| || (_) | |  | |/ /  __/ (_| | |  | | (_| | |_| | | || (__| |_| |
 \____\__,_|\__,_|\__,_| \_/ \___|\___|\__\___/|_|  |_/___\___|\__,_|_|  |_|\__,_|\__|_| |_(_)___|\__,_|


*/

#include "CudaVectorizedMath.h"
#include "CudaMacros.h"

CUDA_FUNCTION VOID CudaVectorizedAdditionByteKernel(GPBYTE gpVectorOne, GPBYTE gpVectorTwo, ULONG ulNumberOfElements)
{
	ULONG ulID;
	CUDA_GET_THREAD_ID(ulID);
	if (ulID < ulNumberOfElements)
		gpVectorOne[ulID] += gpVectorTwo[ulID];
}

CUDA_FUNCTION VOID CudaVectorizedAdditionShortKernel(GPSHORT gpVectorOne, GPSHORT gpVectorTwo, ULONG ulNumberOfElements)
{
	ULONG ulID;
	CUDA_GET_THREAD_ID(ulID);
	if (ulID < ulNumberOfElements)
		gpVectorOne[ulID] += gpVectorTwo[ulID];
}

CUDA_FUNCTION VOID CudaVectorizedAdditionLongKernel(GPLONG gpVectorOne, GPLONG gpVectorTwo, ULONG ulNumberOfElements)
{
	ULONG ulID;
	CUDA_GET_THREAD_ID(ulID);
	if (ulID < ulNumberOfElements)
		gpVectorOne[ulID] += gpVectorTwo[ulID];
}

CUDA_FUNCTION VOID CudaVectorizedAdditionLongLongKernel(GPLONGLONG gpVectorOne, GPLONGLONG gpVectorTwo, ULONG ulNumberOfElements)
{
	ULONG ulID;
	CUDA_GET_THREAD_ID(ulID);
	if (ulID < ulNumberOfElements)
		gpVectorOne[ulID] += gpVectorTwo[ulID];
}

CUDA_FUNCTION VOID CudaVectorizedAdditionFloatKernel(GPFLOAT gpVectorOne, GPFLOAT gpVectorTwo, ULONG ulNumberOfElements)
{
	ULONG ulID;
	CUDA_GET_THREAD_ID(ulID);
	if (ulID < ulNumberOfElements)
		gpVectorOne[ulID] += gpVectorTwo[ulID];
}

CUDA_FUNCTION VOID CudaVectorizedAdditionDoubleKernel(GPDOUBLE gpVectorOne, GPDOUBLE gpVectorTwo, ULONG ulNumberOfElements)
{
	ULONG ulID;
	CUDA_GET_THREAD_ID(ulID);
	if (ulID < ulNumberOfElements)
		gpVectorOne[ulID] += gpVectorTwo[ulID];
}

extern "C"
BOOL
CudaVectorizedAddition(LPVOID lpVectorOne, LPVOID lpVectorTwo, LPVOID lpVectorOut, ULONG ulDataSize, ULONG ulNumberOfElements, ULONG ulFlags)
{
	if (lpVectorOne == NULL ||
      	   lpVectorTwo == NULL)
     		return 0;
  	ULONG ulTotalSize, ulNumberOfBlocks;
  	GPVOID gpVectorOne, gpVectorTwo;
  	ulTotalSize = ulDataSize * ulNumberOfElements;
  	CudaAlloc(&gpVectorOne, ulTotalSize);
  	CudaAlloc(&gpVectorTwo, ulTotalSize);
  	CudaCopyMemory(gpVectorOne, lpVectorOne, ulTotalSize, CPU_TO_GPU);
  	CudaCopyMemory(gpVectorTwo, lpVectorTwo, ulTotalSize, CPU_TO_GPU);

  	ulNumberOfBlocks = (ulNumberOfElements / 1024) + 1;

	switch (ulDataSize)
	{
		case 1:
      			CudaVectorizedAdditionByteKernel<<<ulNumberOfBlocks, 1024>>>((GPBYTE)gpVectorOne, (GPBYTE)gpVectorTwo, ulNumberOfElements);
      			break;
   		case 2:
      			CudaVectorizedAdditionShortKernel<<<ulNumberOfBlocks, 1024>>>((GPSHORT)gpVectorOne, (GPSHORT)gpVectorTwo, ulNumberOfElements);
      			break;
    		case 4:
			if (ulFlags & CUDA_VECTORIZED_MATH_FLOAT_OPERATION)
				CudaVectorizedAdditionFloatKernel<<<ulNumberOfBlocks, 1024>>>((GPFLOAT)gpVectorOne, (GPFLOAT)gpVectorTwo, ulNumberOfElements);
			else
				CudaVectorizedAdditionLongKernel<<<ulNumberOfBlocks, 1024>>>((GPLONG)gpVectorOne, (GPLONG)gpVectorTwo, ulNumberOfElements);
      			break;
    		case 8:
			if (ulFlags & CUDA_VECTORIZED_MATH_DOUBLE_OPERATION)
				CudaVectorizedAdditionDoubleKernel<<<ulNumberOfBlocks, 1024>>>((GPDOUBLE)gpVectorOne, (GPDOUBLE)gpVectorTwo, ulNumberOfElements);
			else
      				CudaVectorizedAdditionLongLongKernel<<<ulNumberOfBlocks, 1024>>>((GPLONGLONG)gpVectorOne, (GPLONGLONG)gpVectorTwo, ulNumberOfElements);
      			break;
  	}
  	CudaCopyMemory(lpVectorOut, gpVectorOne, ulTotalSize, GPU_TO_CPU);
  	CudaFree(gpVectorOne);
  	CudaFree(gpVectorTwo);
  	return 1;
}









//Subtraction Functions

CUDA_FUNCTION VOID CudaVectorizedSubtractByteKernel(GPBYTE gpVectorOne, GPBYTE gpVectorTwo, ULONG ulNumberOfElements)
{
  	ULONG ulID;
  	CUDA_GET_THREAD_ID(ulID);
	if (ulID < ulNumberOfElements)
  		gpVectorOne[ulID] -= gpVectorTwo[ulID];
}

CUDA_FUNCTION VOID CudaVectorizedSubtractShortKernel(GPSHORT gpVectorOne, GPSHORT gpVectorTwo, ULONG ulNumberOfElements)
{
  	ULONG ulID;
  	CUDA_GET_THREAD_ID(ulID);
	if (ulID < ulNumberOfElements)
  		gpVectorOne[ulID] -= gpVectorTwo[ulID];
}

CUDA_FUNCTION VOID CudaVectorizedSubtractLongKernel(GPLONG gpVectorOne, GPLONG gpVectorTwo, ULONG ulNumberOfElements)
{
  	ULONG ulID;
  	CUDA_GET_THREAD_ID(ulID);
	if (ulID < ulNumberOfElements)
  		gpVectorOne[ulID] -= gpVectorTwo[ulID];
}

CUDA_FUNCTION VOID CudaVectorizedSubtractLongLongKernel(GPLONGLONG gpVectorOne, GPLONGLONG gpVectorTwo, ULONG ulNumberOfElements)
{
  	ULONG ulID;
  	CUDA_GET_THREAD_ID(ulID);
	if (ulID < ulNumberOfElements)
  		gpVectorOne[ulID] -= gpVectorTwo[ulID];
}

extern "C"
BOOL
CudaVectorizedSubtraction(LPVOID lpVectorOne, LPVOID lpVectorTwo, LPVOID lpVectorOut, ULONG ulDataSize, ULONG ulNumberOfElements, ULONG ulFlags)
{
  	if (lpVectorOne == NULL ||
      	   lpVectorTwo == NULL)
     		return 0;
  	ULONG ulTotalSize, ulNumberOfBlocks;
  	GPVOID gpVectorOne, gpVectorTwo;
  	ulTotalSize = ulDataSize * ulNumberOfElements;
  	CudaAlloc(&gpVectorOne, ulTotalSize);
  	CudaAlloc(&gpVectorTwo, ulTotalSize);
  	CudaCopyMemory(gpVectorOne, lpVectorOne, ulTotalSize, CPU_TO_GPU);
  	CudaCopyMemory(gpVectorTwo, lpVectorTwo, ulTotalSize, CPU_TO_GPU);

  	ulNumberOfBlocks = (ulNumberOfElements / 1024) + 1;

  	switch (ulDataSize)
  	{
    		case 1:
      			CudaVectorizedSubtractByteKernel<<<ulNumberOfBlocks, 1024>>>((GPBYTE)gpVectorOne, (GPBYTE)gpVectorTwo, ulNumberOfElements);
     			break;
    		case 2:
      			CudaVectorizedSubtractShortKernel<<<ulNumberOfBlocks, 1024>>>((GPSHORT)gpVectorOne, (GPSHORT)gpVectorTwo, ulNumberOfElements);
      			break;
    		case 4:
      			CudaVectorizedSubtractLongKernel<<<ulNumberOfBlocks, 1024>>>((GPLONG)gpVectorOne, (GPLONG)gpVectorTwo, ulNumberOfElements);
      			break;
    		case 8:
      			CudaVectorizedSubtractLongLongKernel<<<ulNumberOfBlocks, 1024>>>((GPLONGLONG)gpVectorOne, (GPLONGLONG)gpVectorTwo, ulNumberOfElements);
     			break;
  	}
  	CudaCopyMemory(lpVectorOut, gpVectorOne, ulTotalSize, GPU_TO_CPU);
  	CudaFree(gpVectorOne);
  	CudaFree(gpVectorTwo);
  	return 1;
}


//Multiplication Functions

CUDA_FUNCTION VOID CudaVectorizedMultiplicationByteKernel(GPBYTE gpVectorOne, GPBYTE gpVectorTwo, ULONG ulNumberOfElements)
{
  	ULONG ulID;
  	CUDA_GET_THREAD_ID(ulID);
	if (ulID < ulNumberOfElements)
  		gpVectorOne[ulID] *= gpVectorTwo[ulID];
}

CUDA_FUNCTION VOID CudaVectorizedMultiplicationShortKernel(GPSHORT gpVectorOne, GPSHORT gpVectorTwo, ULONG ulNumberOfElements)
{
  	ULONG ulID;
  	CUDA_GET_THREAD_ID(ulID);
	if (ulID < ulNumberOfElements)
  		gpVectorOne[ulID] *= gpVectorTwo[ulID];
}

CUDA_FUNCTION VOID CudaVectorizedMultiplicationLongKernel(GPLONG gpVectorOne, GPLONG gpVectorTwo, ULONG ulNumberOfElements)
{
  	ULONG ulID;
  	CUDA_GET_THREAD_ID(ulID);
	if (ulID < ulNumberOfElements)
  		gpVectorOne[ulID] *= gpVectorTwo[ulID];
}

CUDA_FUNCTION VOID CudaVectorizedMultiplicationLongLongKernel(GPLONGLONG gpVectorOne, GPLONGLONG gpVectorTwo, ULONG ulNumberOfElements)
{
  	ULONG ulID;
  	CUDA_GET_THREAD_ID(ulID);
	if (ulID < ulNumberOfElements)
  		gpVectorOne[ulID] *= gpVectorTwo[ulID];
}

extern "C"
BOOL
CudaVectorizedMultiplication(LPVOID lpVectorOne, LPVOID lpVectorTwo, LPVOID lpVectorOut, ULONG ulDataSize, ULONG ulNumberOfElements, ULONG ulFlags)
{
  	if (lpVectorOne == NULL ||
      	   lpVectorTwo == NULL)
     		return 0;
  	ULONG ulTotalSize, ulNumberOfBlocks;
  	GPVOID gpVectorOne, gpVectorTwo;
  	ulTotalSize = ulDataSize * ulNumberOfElements;
  	CudaAlloc(&gpVectorOne, ulTotalSize);
  	CudaAlloc(&gpVectorTwo, ulTotalSize);
  	CudaCopyMemory(gpVectorOne, lpVectorOne, ulTotalSize, CPU_TO_GPU);
  	CudaCopyMemory(gpVectorTwo, lpVectorTwo, ulTotalSize, CPU_TO_GPU);

  	ulNumberOfBlocks = (ulNumberOfElements / 1024) + 1;

  	switch (ulDataSize)
  	{
    		case 1:
      			CudaVectorizedMultiplicationByteKernel<<<ulNumberOfBlocks, 1024>>>((GPBYTE)gpVectorOne, (GPBYTE)gpVectorTwo, ulNumberOfElements);
     			break;
    		case 2:
      			CudaVectorizedMultiplicationShortKernel<<<ulNumberOfBlocks, 1024>>>((GPSHORT)gpVectorOne, (GPSHORT)gpVectorTwo, ulNumberOfElements);
      			break;
    		case 4:
      			CudaVectorizedMultiplicationLongKernel<<<ulNumberOfBlocks, 1024>>>((GPLONG)gpVectorOne, (GPLONG)gpVectorTwo, ulNumberOfElements);
      			break;
    		case 8:
      			CudaVectorizedMultiplicationLongLongKernel<<<ulNumberOfBlocks, 1024>>>((GPLONGLONG)gpVectorOne, (GPLONGLONG)gpVectorTwo, ulNumberOfElements);
     			break;
  	}
  	CudaCopyMemory(lpVectorOut, gpVectorOne, ulTotalSize, GPU_TO_CPU);
  	CudaFree(gpVectorOne);
  	CudaFree(gpVectorTwo);
  	return 1;
}


//Division Functions

CUDA_FUNCTION VOID CudaVectorizedDivisionByteKernel(GPBYTE gpVectorOne, GPBYTE gpVectorTwo, GPBYTE gpVectorRemainder, ULONG ulNumberOfElements)
{
  	ULONG ulID;
  	CUDA_GET_THREAD_ID(ulID);
	if (ulID < ulNumberOfElements)
	{
		gpVectorRemainder[ulID] = gpVectorOne[ulID] % gpVectorTwo[ulID];
  		gpVectorOne[ulID] /= gpVectorTwo[ulID];
	}
}

CUDA_FUNCTION VOID CudaVectorizedDivisionShortKernel(GPSHORT gpVectorOne, GPSHORT gpVectorTwo, GPSHORT gpVectorRemainder, ULONG ulNumberOfElements)
{
  	ULONG ulID;
  	CUDA_GET_THREAD_ID(ulID);
	if (ulID < ulNumberOfElements)
	{
		gpVectorRemainder[ulID] = gpVectorOne[ulID] % gpVectorTwo[ulID];
  		gpVectorOne[ulID] /= gpVectorTwo[ulID];
	}
}

CUDA_FUNCTION VOID CudaVectorizedDivisionLongKernel(GPLONG gpVectorOne, GPLONG gpVectorTwo, GPLONG gpVectorRemainder, ULONG ulNumberOfElements)
{
  	ULONG ulID;
  	CUDA_GET_THREAD_ID(ulID);
	if (ulID < ulNumberOfElements)
	{
		gpVectorRemainder[ulID] = gpVectorOne[ulID] % gpVectorTwo[ulID];
		gpVectorOne[ulID] /= gpVectorTwo[ulID];
	}
}

CUDA_FUNCTION VOID CudaVectorizedDivisionLongLongKernel(GPLONGLONG gpVectorOne, GPLONGLONG gpVectorTwo, GPLONGLONG gpVectorRemainder, ULONG ulNumberOfElements)
{
  	ULONG ulID;
  	CUDA_GET_THREAD_ID(ulID);
	if (ulID < ulNumberOfElements)
	{
		gpVectorRemainder[ulID] = gpVectorOne[ulID] % gpVectorTwo[ulID];
  		gpVectorOne[ulID] /= gpVectorTwo[ulID];
	}
}

extern "C"
BOOL
CudaVectorizedDivision(LPVOID lpVectorOne, LPVOID lpVectorTwo, LPVOID lpVectorOut, LPVOID lpVectorRemainder, ULONG ulDataSize, ULONG ulNumberOfElements, ULONG ulFlags)
{
  	if (lpVectorOne == NULL ||
      	   lpVectorTwo == NULL)
     		return 0;
  	ULONG ulTotalSize, ulNumberOfBlocks;
  	GPVOID gpVectorOne, gpVectorTwo, gpVectorRemainder;
  	ulTotalSize = ulDataSize * ulNumberOfElements;
  	CudaAlloc(&gpVectorOne, ulTotalSize);
  	CudaAlloc(&gpVectorTwo, ulTotalSize);
	CudaAlloc(&gpVectorRemainder, ulTotalSize);
  	CudaCopyMemory(gpVectorOne, lpVectorOne, ulTotalSize, CPU_TO_GPU);
  	CudaCopyMemory(gpVectorTwo, lpVectorTwo, ulTotalSize, CPU_TO_GPU);

  	ulNumberOfBlocks = (ulNumberOfElements / 1024) + 1;

  	switch (ulDataSize)
  	{
    		case 1:
      			CudaVectorizedDivisionByteKernel<<<ulNumberOfBlocks, 1024>>>((GPBYTE)gpVectorOne, (GPBYTE)gpVectorTwo, (GPBYTE)gpVectorRemainder, ulNumberOfElements);
     			break;
    		case 2:
      			CudaVectorizedDivisionShortKernel<<<ulNumberOfBlocks, 1024>>>((GPSHORT)gpVectorOne, (GPSHORT)gpVectorTwo, (GPSHORT)gpVectorRemainder, ulNumberOfElements);
      			break;
    		case 4:
      			CudaVectorizedDivisionLongKernel<<<ulNumberOfBlocks, 1024>>>((GPLONG)gpVectorOne, (GPLONG)gpVectorTwo, (GPLONG)gpVectorRemainder, ulNumberOfElements);
      			break;
    		case 8:
      			CudaVectorizedDivisionLongLongKernel<<<ulNumberOfBlocks, 1024>>>((GPLONGLONG)gpVectorOne, (GPLONGLONG)gpVectorTwo, (GPLONGLONG)gpVectorRemainder, ulNumberOfElements);
     			break;
  	}
  	CudaCopyMemory(lpVectorOut, gpVectorOne, ulTotalSize, GPU_TO_CPU);
  	if (lpVectorRemainder) CudaCopyMemory(lpVectorRemainder, gpVectorRemainder, ulTotalSize, GPU_TO_CPU);
	CudaFree(gpVectorOne);
  	CudaFree(gpVectorTwo);
	CudaFree(gpVectorRemainder);
  	return 1;
}

//End Division Functions

//Power Functions

#define CUDA_POWER_KERNAL_CORE(DataType, ThreadID, VectorOne, VectorTwo)	\
		DataType dtValue;						\
		dtValue = VectorOne[ThreadID];					\
		if (ThreadID < ulNumberOfElements) 				\
		{								\
			for (DataType dtI = 1; dtI < VectorTwo[ThreadID]; dtI++)\
				VectorOne[ThreadID] *= dtValue;			\
		}




CUDA_FUNCTION VOID CudaVectorizedPowerByteKernel(GPBYTE gpVectorOne, GPBYTE gpVectorTwo, ULONG ulNumberOfElements)
{
  	ULONG ulID;
  	CUDA_GET_THREAD_ID(ulID);
	CUDA_POWER_KERNAL_CORE(BYTE, ulID, gpVectorOne, gpVectorTwo)
}
CUDA_FUNCTION VOID CudaVectorizedPowerShortKernel(GPSHORT gpVectorOne, GPSHORT gpVectorTwo, ULONG ulNumberOfElements)
{
  	ULONG ulID;
  	CUDA_GET_THREAD_ID(ulID);
	CUDA_POWER_KERNAL_CORE(SHORT, ulID, gpVectorOne, gpVectorTwo)
}

CUDA_FUNCTION VOID CudaVectorizedPowerLongKernel(GPLONG gpVectorOne, GPLONG gpVectorTwo, ULONG ulNumberOfElements)
{
  	ULONG ulID;
  	CUDA_GET_THREAD_ID(ulID);
	CUDA_POWER_KERNAL_CORE(LONG, ulID, gpVectorOne, gpVectorTwo)
}

CUDA_FUNCTION VOID CudaVectorizedPowerLongLongKernel(GPLONGLONG gpVectorOne, GPLONGLONG gpVectorTwo, ULONG ulNumberOfElements)
{
  	ULONG ulID;
	CUDA_GET_THREAD_ID(ulID);
	CUDA_POWER_KERNAL_CORE(LONGLONG, ulID, gpVectorOne, gpVectorTwo)
}


EXTERN
BOOL
CudaVectorizedPower(LPVOID lpVectorOne, LPVOID lpVectorTwo, LPVOID lpVectorOut, ULONG ulDataSize, ULONG ulNumberOfElements, ULONG ulFlags)
{
	if (!lpVectorOne || !lpVectorTwo)
		return 0;
	ULONG ulTotalSize, ulNumberOfBlocks;
	GPVOID gpVectorOne, gpVectorTwo;
	ulTotalSize = ulDataSize * ulNumberOfElements;
	CudaAlloc(&gpVectorOne, ulTotalSize);
	CudaAlloc(&gpVectorTwo, ulTotalSize);
	CudaCopyMemory(gpVectorOne, lpVectorOne, ulTotalSize, CPU_TO_GPU);
	CudaCopyMemory(gpVectorTwo, lpVectorTwo, ulTotalSize, CPU_TO_GPU);

	ulNumberOfBlocks = (ulNumberOfElements / 1024) + 1;

  	switch (ulDataSize)
  	{
    		case 1:
      			CudaVectorizedPowerByteKernel<<<ulNumberOfBlocks, 1024>>>((GPBYTE)gpVectorOne, (GPBYTE)gpVectorTwo, ulNumberOfElements);
     			break;
    		case 2:
      			CudaVectorizedPowerShortKernel<<<ulNumberOfBlocks, 1024>>>((GPSHORT)gpVectorOne, (GPSHORT)gpVectorTwo, ulNumberOfElements);
      			break;
    		case 4:
      			CudaVectorizedPowerLongKernel<<<ulNumberOfBlocks, 1024>>>((GPLONG)gpVectorOne, (GPLONG)gpVectorTwo, ulNumberOfElements);
      			break;
    		case 8:
      			CudaVectorizedPowerLongLongKernel<<<ulNumberOfBlocks, 1024>>>((GPLONGLONG)gpVectorOne, (GPLONGLONG)gpVectorTwo, ulNumberOfElements);
     			break;
  	}

	CudaCopyMemory(lpVectorOut, gpVectorOne, ulTotalSize, GPU_TO_CPU);
	CudaFree(gpVectorOne);
  	CudaFree(gpVectorTwo);
  	return 1;
}



//Scalar Power Functions.

CUDA_FUNCTION VOID CudaVectorizedScalarPowerByteKernel(GPBYTE gpVector, BYTE bPower, ULONG ulNumberOfElements)
{
	ULONGLONG ullID;
	CUDA_GET_THREAD_ID(ullID);
	if (ullID < ulNumberOfElements)
	{
		BYTE bIndex, bScalarValue;
		bScalarValue = gpVector[ullID];
		for (bIndex = 1; bIndex < bPower; bIndex++)
			gpVector[ullID] *= bScalarValue;
	}
}

CUDA_FUNCTION VOID CudaVectorizedScalarPowerShortKernel(GPSHORT gpVector, BYTE bPower, ULONG ulNumberOfElements)
{
	ULONGLONG ullID;
	CUDA_GET_THREAD_ID(ullID);
	if (ullID < ulNumberOfElements)
	{
		BYTE bIndex;
	 	SHORT sScalarValue;
		sScalarValue = gpVector[ullID];
		for (bIndex = 1; bIndex < bPower; bIndex++)
			gpVector[ullID] *= sScalarValue;
	}
}

CUDA_FUNCTION VOID CudaVectorizedScalarPowerLongKernel(GPLONG gpVector, BYTE bPower, ULONG ulNumberOfElements)
{
	ULONGLONG ullID;
	CUDA_GET_THREAD_ID(ullID);
	if (ullID < ulNumberOfElements)
	{
		BYTE bIndex;
	 	LONG lScalarValue;
		lScalarValue = gpVector[ullID];
		for (bIndex = 1; bIndex < bPower; bIndex++)
			gpVector[ullID] *= lScalarValue;
	}
}

CUDA_FUNCTION VOID CudaVectorizedScalarPowerLongLongKernel(GPLONGLONG gpVector, BYTE bPower, ULONG ulNumberOfElements)
{
	ULONGLONG ullID;
	CUDA_GET_THREAD_ID(ullID);
	if (ullID < ulNumberOfElements)
	{
		BYTE bIndex;
	 	LONGLONG llScalarValue;
		llScalarValue = gpVector[ullID];
		for (bIndex = 1; bIndex < bPower; bIndex++)
			gpVector[ullID] *= llScalarValue;
	}
}

CUDA_FUNCTION VOID CudaVectorizedScalarPowerFloatKernel(GPFLOAT gpVector, BYTE bPower, ULONG ulNumberOfElements)
{
	ULONGLONG ullID;
	CUDA_GET_THREAD_ID(ullID);
	if (ullID < ulNumberOfElements)
	{
		BYTE bIndex;
		FLOAT fScalarValue;
		fScalarValue = gpVector[ullID];
		for (bIndex = 1; bIndex < bPower; bIndex++)
			gpVector[ullID] *= fScalarValue;
	}
}

CUDA_FUNCTION VOID CudaVectorizedScalarPowerDoubleKernel(GPDOUBLE gpVector, BYTE bPower, ULONG ulNumberOfElements)
{
	ULONGLONG ullID;
	CUDA_GET_THREAD_ID(ullID);
	if (ullID < ulNumberOfElements)
	{
		BYTE bIndex;
		DOUBLE dScalarValue;
		dScalarValue = gpVector[ullID];
		for (bIndex = 1; bIndex < bPower; bIndex++)
			gpVector[ullID] *= dScalarValue;
	}
}

EXTERN
BOOL
CudaVectorizedScalarPower(LPVOID lpVector, BYTE bPower, LPVOID lpVectorOut, ULONG ulDataSize, ULONG ulNumberOfElements, ULONG ulFlags)
{
	if (!lpVector || !lpVectorOut)
		return 0;
	ULONG ulTotalSize, ulNumberOfBlocks;
	GPVOID gpVector;
	ulTotalSize = ulDataSize * ulNumberOfElements;
	if (!CudaAlloc(&gpVector, ulTotalSize)) { return 0; }
	CudaCopyMemory(gpVector, lpVector, ulTotalSize, CPU_TO_GPU);

	ulNumberOfBlocks = (ulNumberOfElements / 1024)  + 1;

	switch (ulDataSize)
	{
		case 1:
			CudaVectorizedScalarPowerByteKernel<<<ulNumberOfBlocks, 1024>>>((GPBYTE)gpVector, bPower, ulNumberOfElements);
			break;
		case 2:
			CudaVectorizedScalarPowerShortKernel<<<ulNumberOfBlocks, 1024>>>((GPSHORT)gpVector, bPower, ulNumberOfElements);
			break;
		case 4:
			if (ulFlags & CUDA_VECTORIZED_MATH_FLOAT_OPERATION)
				CudaVectorizedScalarPowerFloatKernel<<<ulNumberOfBlocks, 1024>>>((GPFLOAT)gpVector, bPower, ulNumberOfElements);
			else
				CudaVectorizedScalarPowerLongKernel<<<ulNumberOfBlocks, 1024>>>((GPLONG)gpVector, bPower, ulNumberOfElements);
			break;
		case 8:
			if (ulFlags & CUDA_VECTORIZED_MATH_DOUBLE_OPERATION)
				CudaVectorizedScalarPowerDoubleKernel<<<ulNumberOfBlocks, 1024>>>((GPDOUBLE)gpVector, bPower, ulNumberOfElements);
			else
				CudaVectorizedScalarPowerLongLongKernel<<<ulNumberOfBlocks, 1024>>>((GPLONGLONG)gpVector, bPower, ulNumberOfElements);
			break;
	}

	CudaCopyMemory(lpVectorOut, gpVector, ulTotalSize, GPU_TO_CPU);
	CudaFree(gpVector);
  	return 1;

}


/*
SCALAR FUNCTIONS
*/


#define CUDA_SCALAR_ADDITION_KERNAL_CORE(DataType, ThreadID, Vector, VectorScalarand, NumberOfElements)	\
		if (ThreadID < NumberOfElements) 							\
		{ Vector[ThreadID] += *(DataType *)VectorScalarand; }						\

CUDA_FUNCTION VOID CudaVectorizedScalarAdditionByteKernel(GPBYTE gpVector, GPBYTE gpScalarand, ULONG ulNumberOfElements)
{
	ULONG ulID;
	CUDA_GET_THREAD_ID(ulID);
	CUDA_SCALAR_ADDITION_KERNAL_CORE(BYTE, ulID, gpVector, gpScalarand, ulNumberOfElements);
} 

CUDA_FUNCTION VOID CudaVectorizedScalarAdditionShortKernel(GPSHORT gpVector, GPSHORT gpScalarand, ULONG ulNumberOfElements)
{
	ULONG ulID;
	CUDA_GET_THREAD_ID(ulID);
	CUDA_SCALAR_ADDITION_KERNAL_CORE(SHORT, ulID, gpVector, gpScalarand, ulNumberOfElements);
}

CUDA_FUNCTION VOID CudaVectorizedScalarAdditionLongKernel(GPLONG gpVector, GPLONG gpScalarand, ULONG ulNumberOfElements)
{
	ULONG ulID;
	CUDA_GET_THREAD_ID(ulID);
	CUDA_SCALAR_ADDITION_KERNAL_CORE(LONG, ulID, gpVector, gpScalarand, ulNumberOfElements);
}

CUDA_FUNCTION VOID CudaVectorizedScalarAdditionLongLongKernel(GPLONGLONG gpVector, GPLONGLONG gpScalarand, ULONG ulNumberOfElements)
{
	ULONG ulID;
	CUDA_GET_THREAD_ID(ulID);
	CUDA_SCALAR_ADDITION_KERNAL_CORE(LONGLONG, ulID, gpVector, gpScalarand, ulNumberOfElements);
}

CUDA_FUNCTION VOID CudaVectorizedScalarAdditionFloatKernel(GPFLOAT gpVector, GPFLOAT gpScalarand, ULONG ulNumberOfElements)
{
	ULONG ulID;
	CUDA_GET_THREAD_ID(ulID);
	//CUDA_SCALAR_ADDITION_KERNAL_CORE(FLOAT, ulID, gpVector, gpScalarand, ulNumberOfElements);
	if (ulID < ulNumberOfElements)
		gpVector[ulID] += gpScalarand[0];
}

CUDA_FUNCTION VOID CudaVectorizedScalarAdditionDoubleKernel(GPDOUBLE gpVector, GPDOUBLE gpScalarand, ULONG ulNumberOfElements)
{
	ULONG ulID;
	CUDA_GET_THREAD_ID(ulID);
	CUDA_SCALAR_ADDITION_KERNAL_CORE(DOUBLE, ulID, gpVector, gpScalarand, ulNumberOfElements);
}


EXTERN
BOOL
CudaVectorizedScalarAddition(LPVOID lpVector, LPVOID lpScalarand, LPVOID lpVectorOut, ULONG ulDataSize, ULONG ulNumberOfElements, ULONG ulFlags)
{
    EXIT_IF_NULL(lpVector, 0);
    EXIT_IF_NULL(lpVectorOut, 0);
	ULONG ulTotalSize, ulNumberOfBlocks;
	GPVOID gpVector;
	GPVOID gpScalarand;
	ulTotalSize = (ulDataSize * ulNumberOfElements);
	CudaAlloc(&gpVector, ulTotalSize);
	CudaAlloc(&gpScalarand, ulDataSize);
	CudaCopyMemory(gpVector, lpVector, ulTotalSize, CPU_TO_GPU);
	CudaCopyMemory(gpScalarand, lpScalarand, ulDataSize, CPU_TO_GPU);

	ulNumberOfBlocks = (ulNumberOfElements / 1024) + 1;
	
	switch (ulDataSize)
	{
		case 1:
			CudaVectorizedScalarAdditionByteKernel<<<ulNumberOfBlocks, 1024>>>((GPBYTE)gpVector, (GPBYTE)gpScalarand, ulNumberOfElements);
			break;
		case 2:
			CudaVectorizedScalarAdditionShortKernel<<<ulNumberOfBlocks, 1024>>>((GPSHORT)gpVector, (GPSHORT)gpScalarand, ulNumberOfElements);
			break;
		case 4:
			if (ulFlags & CUDA_VECTORIZED_MATH_FLOAT_OPERATION)
				CudaVectorizedScalarAdditionFloatKernel<<<ulNumberOfBlocks, 1024>>>((GPFLOAT)gpVector, (GPFLOAT)gpScalarand, ulNumberOfElements);
			else
				CudaVectorizedScalarAdditionLongKernel<<<ulNumberOfBlocks, 1024>>>((GPLONG)gpVector, (GPLONG)gpScalarand, ulNumberOfElements);
			break;
		case 8:
			if (ulFlags & CUDA_VECTORIZED_MATH_DOUBLE_OPERATION)
				CudaVectorizedAdditionDoubleKernel<<<ulNumberOfBlocks, 1024>>>((GPDOUBLE)gpVector, (GPDOUBLE)gpScalarand, ulNumberOfElements);
			else
				CudaVectorizedAdditionLongLongKernel<<<ulNumberOfBlocks, 1024>>>((GPLONGLONG)gpVector, (GPLONGLONG)gpScalarand, ulNumberOfElements);
			break;
	}
	
	CudaCopyMemory(lpVectorOut, gpVector, ulTotalSize, GPU_TO_CPU);
	CudaFree(gpVector);
  	CudaFree(gpScalarand);
	return 1;
}








//Random Number function
// http://cs.umw.edu/~finlayson/class/fall14/cpsc425/notes/23-cuda-random.html
//TODO: Implement this.
CUDA_FUNCTION VOID CudaVectorizedRandomNumbersKernel(GPLONGLONG gplVector, ULONG ulNumberOfElements, ULONGLONG ullSeed)
{
	ULONGLONG ullID;
	CUDA_GET_THREAD_ID(ullID);
	if (ullID < ulNumberOfElements)
	{
		//curandState_t crsState;
		//curand_init(0, 0, 0, 0, &crsState);
		//gplVector[ullID] = curand(&crsState);
	}
}

EXTERN
BOOL
CudaVectorizedRandomNumbers(LPVOID lpVector, ULONG ulDataSize, ULONG ulNumberOfElements, ULONG ulSeed, ULONG ulFlags)
{

	return 1;
}
