/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "CudaVectorizedMath.h"
#define AMOUNT 5

VOID GenerateNewRandoms(LPLONG lpLong, ULONG ulSize)
{
	ULONG ulIndex;
	for (ulIndex = 0; ulIndex < ulSize; ulIndex++)
		lpLong[ulIndex] = rand() % 100;
}

int main()
{
	srand(time(NULL));
	LONG lRandomNumbers[AMOUNT];
	//LONG lRandomNumbers2[AMOUNT];
	LONG lResults[AMOUNT];
	LONG lScalar;
	lScalar = 1;
	GenerateNewRandoms(lRandomNumbers, AMOUNT);
	//GenerateNewRandoms(lRandomNumbers2, AMOUNT);
	//FLOAT fNum1[5] = {0.0f, 1.0f, 2.0f, 3.0f, 4.0f};
	//FLOAT fNum2[5] = {5.0f, 6.0f, 7.0f, 8.0f, 9.0f};
	//FLOAT fRes[5] = {0.0f, 0.0f, 0.0f, 0.0f, 0.0f};
	//FLOAT fScalar = 1.0f;
	//LONG lNum1[5] = {0, 1, 2, 3, 4};
	//LONG lNum2[5] = {5, 6, 7, 8 ,9};
	//LONG lRes[5] = {0, 0, 0, 0, 0};

	//CudaVectorizedAddition(lNum1, lNum2, lRes, 8, (ULONG)5, (ULONG)NULL);

	CudaVectorizedScalarAddition(lRandomNumbers, &lScalar, lResults, sizeof(LONG), 5, NULL);

	ULONG ulIndex;
	for (ulIndex = 0; ulIndex < 5; ulIndex++)
		printf("%d\t", lRandomNumbers[ulIndex]);
	printf("\n");
//	for (ulIndex = 0; ulIndex < 5; ulIndex++)
//		printf("%f\t", fNum2[ulIndex]);
	printf("\n");
	for (ulIndex = 0; ulIndex <5; ulIndex++)
		printf("%d\t", lResults[ulIndex]);
	printf("\n");

	return 0;
}
