/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CUDAMACROS_H
#define CUDAMACROS_H

#include "../Macros.h"

#define CUDA_GET_THREAD_ID(ulID) ulID = blockIdx.x * blockDim.x + threadIdx.x

#define CUDA_COPY_TO_GPU(GPU_POINTER, CPU_POINTER, TOTAL_SIZE)	\
	CudaCopyMemory(GPU_POINTER, CPU_POINTER, TOTAL_SIZE, CPU_TO_GPU);

#define CUDA_COPY_TO_CPU(GPU_POINTER, CPU_POINTER, TOTAL_SIZE)	\
	CudaCopyMemory(GPU_POINTER, CPU_POINTER, TOTAL_SIZE, GPU_TO_CPU);

#endif
