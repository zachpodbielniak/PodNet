/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CUDACLASS_H
#define CUDACLASS_H
#include "CudaVectorizedMath.h"

typedef struct _CUDAVECTORIZEDMATHCLASS
{
	BOOL (*VectorizedAddition)(
		LPVOID	lpVectorOne,
		LPVOID	lpVectorTwo,	
		LPVOID	lpVectorOut,
		ULONG	ulDataSize,
		ULONG	ulNumberOfElements,
		ULONG	ulFlags
	);
	
	BOOL (*VectorizedSubtraction)(
		LPVOID	lpVectorOne,
		LPVOID	lpVectorTwo,	
		LPVOID	lpVectorOut,
		ULONG	ulDataSize,
		ULONG	ulNumberOfElements,
		ULONG	ulFlags
	);

	BOOL (*VectorizedMultiplication)(
		LPVOID	lpVectorOne,
		LPVOID	lpVectorTwo,	
		LPVOID	lpVectorOut,
		ULONG	ulDataSize,
		ULONG	ulNumberOfElements,
		ULONG	ulFlags
	);

	BOOL (*VectorizedDivision)(
		LPVOID	lpVectorOne,
		LPVOID	lpVectorTwo,	
		LPVOID	lpVectorOut,
		LPVOID 	lpVectorRemainder,
		ULONG	ulDataSize,
		ULONG	ulNumberOfElements,
		ULONG	ulFlags
	);
	
	BOOL (*VectorizedPower)(
		LPVOID	lpVectorOne,
		LPVOID	lpVectorTwo,	
		LPVOID	lpVectorOut,
		ULONG	ulDataSize,
		ULONG	ulNumberOfElements,
		ULONG	ulFlags
	);

	BOOL (*VectorizedScalarPower)(
		LPVOID	lpVector,
		BYTE	bPower,
		LPVOID	lpVectorOut,
		ULONG	ulDataSize,
		ULONG	ulNumberOfElements,
		ULONG	ulFlags
	);
} CUDAVECTORIZEDMATHCLASS, *LPCUDAVECTORIZEDMATHCLASS;

typedef struct _CUDACLASS
{
	CUDAVECTORIZEDMATHCLASS VectorizedMath;
} CUDACLASS, *LPCUDACLASS;

#endif
