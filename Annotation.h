/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef ANNOTATION_H
#define ANNOTATION_H

#define _In_
#define _Out_
#define _In_Z_
#define _Out_Z_
#define _In_Opt_
#define _Out_Opt_
#define _In_Opt_Z_
#define _Out_Opt_Z_
#define _In_Out_
#define _In_Out_Opt_
#define _In_Out_Z_
#define _In_Out_Opt_Z_


#define _In_Writes_(X)
#define _In_Writes_Bytes_(X)
#define _In_Writes_Z_(X)
#define _Out_Writes_(X)
#define _Out_Writes_Bytes_(X)
#define _Out_Writes_Z_(X)
#define _In_Reads_Opt_(X)
#define _In_Reads_Bytes_Opt_(X)
#define _In_Reads_Z_(X)
#define _Out_Reads_Opt_(X)
#define _Out_Reads_Bytes_Opt_(X)
#define _In_Out_Updates_(X)
#define _In_Out_Updates_Opt_(X)
#define _In_Out_Updates_Bytes_Opt_(X)
#define _In_Out_Updates_Z_(X)
#define _In_Out_Writes_(X)
#define _In_Out_Writes_To_(X,Y)
#define _Out_Writes_To_(X,Y)
#define _Out_Writes_Bytes_To_(X,Y)
#define _Out_Writes_All_(X,Y)
#define _Out_Writes_Bytes_All_(X)
#define _In_Out_Updates_To_(X,Y)
#define _In_Out_Updates_Bytes_To_(X,Y)
#define _In_Reads_To_Ptr_(X)
#define _In_Reads_To_Ptr_Z_(X)
#define _In_Reads_To_Ptr_Opt_(X)
#define _In_Reads_To_Ptr_Opt_Z_(X)
#define _Out_Writes_To_Ptr_(X)
#define _Out_Writes_To_Ptr_Z_(X)
#define _Out_Writes_To_Ptr_Opt_(X)
#define _Out_Writes_To_Ptr_Opt_Z_(X)
#define _In_Required_When_(X)
#define _In_Ptr_Required_When_(X)
#define _In_Z_Required_When_(X)
#define _Out_Required_When_(X)
#define _Out_Ptr_Required_When_(X)
#define _Out_Z_Required_When_(X)



/* Function Return Conditions */
#define _Out_Ptr_
#define _Out_Ptr_Z_
#define _Out_Ptr_Opt_
#define _Out_Ptr_Opt_Z_
#define _Out_Ptr_Result_May_Be_Null_
#define _Out_Ptr_Result_May_Be_Null_Z_
#define _Out_Ptr_Opt_Result_May_Be_Null_
#define _Out_Ptr_Opt_Result_May_Be_Null_Z_

/* The returned pointer points to a valid buffer of size S. */
#define _Out_Ptr_Result_Buffer_(S)
#define _Out_Ptr_Result_Byte_Buffer_(S)
#define _Out_Ptr_Opt_Result_Buffer_(S)
#define _Out_Ptr_Opt_Result_Byte_Buffer_(S)

#define _Result_Null_On_Failure_
#define _Result_Zero_On_Failure_
#define _Out_Ptr_Result_Null_On_Failure_

#define _Return_Z_
#define _Return_Writes_(S)
#define _Return_Writes_Bytes_(S)
#define _Return_Writes_Z_(S)
#define _Return_Writes_To_(S,P)
#define _Return_Writes_May_Be_Null(S)
#define _Return_Writes_To_May_Be_Null_(S)
#define _Return_Writes_May_Be_Null_Z_(S)
#define _Return_May_Be_Null_
#define _Return_May_Be_Null_Z_
#define _Return_Null_
#define _Return_Not_Null_
#define _Return_Writes_Bytes_To_(S)
#define _Return_Writes_Bytes_May_Be_Null_(S)
#define _Return_Writes_Bytes_To_May_Be_Null_(S)


#define _In_Range_(L,H)
#define _In_Opt_Range_(L,H)
#define _Out_Range_(L,H)
#define _Out_Opt_Range_(L,H)
#define _Return_Range_(L,H)
#define _In_Out_Range_(L,H)
#define _In_Out_Opt_Range_(L,H)

#define _Reserved_
#define _Reserved_Must_Be_Null_


#define _Call_Back_
#define _Call_Back_Default_Implementation_
#define _Calls_Call_Back_


#define _Kills_Process_
#define _Can_Kill_Process_


/* Threading */
#define _Spawns_New_Thread_
#define _Spawns_New_Thread_Async_Call_
#define _Spawns_New_Thread_And_Locks_(X,T)
#define _Spawns_New_Thread_Async_And_Locks_(X,T)

#define _Thread_Proc_
#define _Promise_Proc_


/* Processes */
#define _Spawns_Process_
#define _Spawns_Process_And_Waits_


/* Locking */
#define _Non_Locking_
#define _Acquires_Exclusive_Lock_(X,T)
#define _Acquires_Lock_(X,T)
#define _Acquires_Non_Reentrant_Lock_(X,T)
#define _Acquires_Shared_Lock_(X,T)
#define _Acquires_Spin_Lock(X)
#define _Releases_Exclusive_Lock_(X,T)
#define _Releases_Lock_(X,T)
#define _Releases_Non_Reentrant_Lock_(X,T)
#define _Releases_Shared_Lock_(X,T)
#define _Releases_Spin_Lock(X)
#define _Requires_Lock_Held_(X,T)
#define _Requires_Lock_Not_Held_(X,T)
#define _Requires_No_Locks_Held_(X,T)
#define _Requires_Shared_Lock_Held_(X,T)
#define _Requires_Exclusive_Lock_Held_(X,T)

#define _Create_Lock_Level_(X)
#define _Has_Lock_Kind_(X)
#define _Lock_Kind_Mutex_
#define _Lock_Kind_Event_
#define _Lock_Kind_Semaphore_
#define _Lock_Kind_Spin_Lock_
#define _Lock_Kind_Critical_Section_

#define _Global_Cancel_Spin_Lock_
#define _Global_Critical_Region_
#define _Global_Interlock_
#define _Global_Priority_Region_

#define _Guarded_By_(X)
#define _Interlocked_
#define _Interlocked_Operation_
#define _Interlocked_When_(X,T)
#define _Interlocked_Operand_
#define _Write_Guarded_By_(X)
#define _Read_Guarded_By_(X)



#define _At_(X,S)
#define _At_Buffer_(X,C,S)
#define _Group_(S)
#define _When_(X)
#define _Maybe_(X)
#define _Inexpressible_(X)

#define _Success_(S,C)
#define _Failure_(S,C)

#endif