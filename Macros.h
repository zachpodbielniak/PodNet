/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef MACROS_H
#define MACROS_H
#include "TypeDefs.h"


#define NO_OPERATION()						(VOID)0
#define SPIN_LOCK(X)						do { (X)--; } while ((X) > 0)
#define SPIN_LOCK_WITH_OPERATION(X,OP)				do { (X)--; (OP); } while ((X) > 0)

#define UNREFERENCED_PARAMETER(X)				((X) = (X))
#define UNREFERENCED_VARIABLE(X)				((X) = (X))

#ifndef MIN
#define MIN(X,Y)						(((X) > (Y)) ? (Y) : (X))
#endif 

#ifndef MAX
#define MAX(X,Y)						(((X) > (Y)) ? (X) : (Y))
#endif

#define MAKEWORD(X,Y)						((WORD)((((X) << 8) | (Y))))
/* #define GET_LOWER_WORD(X)					((WORD)(((X) << 8) >> 8)) */
#define GET_LOWER_WORD(X)					((WORD)((X) & 0x00FFU))
#define GET_UPPER_WORD(X)					((X) >> 8)

#define MAKEDWORD(W,X,Y,Z)					((DWORD)((MAKEWORD((W),(X)) << 16) | (MAKEWORD((Y),(Z))))


#define FLOAT_AS_LONG(X)					(*(LPLONG)&(X))
#define FLOAT_AS_ULONG(X)					(*(LPULONG)&(X))
#define LONG_AS_FLOAT(X)					(*(LPFLOAT)&(X))
#define ULONG_AS_FLOAT(X)					LONG_AS_FLOAT((X))

#define DOUBLE_AS_LONGLONG(X)					(*(LPLONGLONG)&(X))
#define DOUBLE_AS_ULONGLONG(X)					(*(LPULONGLONG)&(X))
#define LONGLONG_AS_DOUBLE(X)					(*(LPDOUBLE)&(X))
#define ULONGLONG_AS_DOUBLE(X)					LONGLONG_AS_DOUBLE((X))

#define ARRAY_LENGTH(X)						do { ((sizeof(X) / sizeof(0[X])) / ((size_t)(!(sizeof(X) % sizeof(0[X]))))); } while (0)
#define ARRAY_SIZE(X, TYPE)					do { (sizeof(X) / sizeof(TYPE)); } while (0)
#define SET_ARRAY(TYPE,SZ,VAL)					do{ size_t i_, n_;					\
									for (n_ = (SZ), i_ = 0; n_ > 0; --n_; ++i_)	\
										(TYPE)[i_] = (VAL); } while (0)
#define ZERO_ARRAY(TYPE,SZ)					SET_ARRAY((TYPE),(SZ),0)
#define IS_ARRAY(X)						((LPVOID)&a == (LPVOID)a)



#define ABSOLUTE_VALUE(X)					((X < 0) ? (-X) : (X))
#define DIFFERENCE(X,Y)						ABSOLUTE_VALUE((X)-(Y))

#define PERCENTAGE_AS_FLOAT(N,D)				((FLOAT)((N * 100.0f) / D))
#define PERCENTAGE_AS_DOUBLE(N,D)				((DOUBLE)((N * 100.0) / D))

#define SET_BIT(VAL, PLACE)					((VAL) | (1 << (PLACE)))
#define CLEAR_BIT(VAL, PLACE)					((VAL) & (~(1 << (PLACE))))
#define TOGGLE_BIT(VAL, PLACE)					((VAL) ^ (1 << (PLACE)))
#define GET_BIT(VAL, PLACE)					(((VAL) >> (PLACE)) & 1)
#define GET_MOST_SIGNIFICANT_BIT(VAL, TYPE)			GET_BIT((VAL), (sizeof((TYPE)) * 4 - 1))
#define GET_LEAST_SIGNIFICANT_BIT(VAL)				GET_BIT((VAL), 0)

/* Sigh */					
#define LITTLE_TO_BIG_ENDIAN_128(X)				((((X)&0xFF)<<120)|(((X)&0xFF00)<<104)|(((X)&0xFF0000)<<88)|(((X)&0xFF000000)<<72)|\
								(((X)&0xFF00000000)<<56)|(((X)&0xFF0000000000)<<40)|(((X)<<24)&0xFF000000000000)|\
								(((X)&0xFF00000000000000)<<8)|(((X)&(UULTRALONG)0xFF0000000000000000)>>8)|\
								(((X)&(UULTRALONG)0xFF000000000000000000)>>24)|(((X)&(UULTRALONG)0xFF00000000000000000000)>>40)|\
								(((X)&(UULTRALONG)0xFF0000000000000000000000)>>56)|(((X)&(UULTRALONG)0xFF000000000000000000000000)>>72)|\
								(((X)&(UULTRALONG)0xFF00000000000000000000000000)>>88)|(((X)&(UULTRALONG)0xFF0000000000000000000000000000)>>104)|\
								(((X)&(UULTRALONG)0xFF000000000000000000000000000000)>>120))
#define LITTLE_TO_BIG_ENDIAN_64(X)				(ULONGLONG)((((X)&0xFF)<<56)|(((X)&0xFF00)<< 40)|(((X)& 0xFF0000)<<24)|(((X)&0xFF000000)<< 8)|\
								(((X)&0xFF00000000)>>8)|(((X)&0xFF0000000000)>>24)|(((X)&0xFF000000000000)>>40)|\
								(((X)&0xFF00000000000000)>>56))
#define BIG_TO_LITTLE_ENDIAN_64(X)				(ULONGLONG)((((X) >> 56)&0xFF)|(((X)>>40)&0xFF00)|(((X)>>24)&0xFF0000)|(((X)>>8)&0xFF000000))|\
								(((X)<<8)&0xFF00000000)|(((X)<<24)& 0xFF0000000000)|(((X)<<40)&0xFF000000000000)|\
								(((X)<<56)&0xFF00000000000000))
#define LITTLE_TO_BIG_ENDIAN_32(X)				((((X)&0xFF) << 24) | (((X)&0xFF00) << 8) | (((X)&0xFF0000) >> 8) | (((X)>>24)&0xFF))
#define BIG_TO_LITTLE_ENDIAN_32(X)				((((X) >> 24)&0xFF) | (((X) << 8 )&0xFF0000) | (((X) >> 8)&0xFF00) | (((X) << 24)&0xFF000000))
#define LITTLE_TO_BIG_ENDIAN_16(X)				(((X) >> 8) | (((X) << 8)&0xFF00))
#define BIG_TO_LITTLE_ENDIAN_16(X)				(((X) >> 8) | (((X) << 8)&0xFF00))

#define XOR_SWAP(D1,D2)						((D1) = (D1) ^ (D2))	\
								((D2) = (D2) ^ (D1))	\
								((D1) = (D1) ^ (D2))

#ifdef __LP64__
#define XOR_SWAP_PTR(P1,P2)					(P1) = (LPVOID)((ULONGLONG)(P1) ^ (ULONGLONG)(P2));	\
								(P2) = (LPVOID)((ULONGLONG)(P2) ^ (ULONGLONG)(P1));	\
								(P1) = (LPVOID)((ULONGLONG)(P1) ^ (ULONGLONG)(P2));
#else
#define XOR_SWAP_PTR(P1,P2)					(P1) = (LPVOID)((ULONG)(P1) ^ (ULONG)(P2));	\
								(P2) = (LPVOID)((ULONG)(P2) ^ (ULONG)(P1));	\
								(P1) = (LPVOID)((ULONG)(P1) ^ (ULONG)(P2));
#endif


#define SWAP_CONTENTS(X,Y)												\
								if (sizeof(*(X)) != sizeof(*(Y))) 		\
								{ JUMP(__SWAP__OUT__LEAVE__); }			\
								LPVOID lpSwapTmp;				\
								lpSwapTmp = LocalAllocAndZero(sizeof(*(X)));	\
								if (NULLPTR == lpSwapTmp)			\
								{ JUMP(__SWAP__OUT__LEAVE__); }			\
								CopyMemory(lpSwapTmp, (X), sizeof(*(X)));	\
								CopyMemory((X), (Y), sizeof(*(X)));		\
								CopyMemory((Y), lpSwapTmp, sizeof(*(X)));	\
								FreeMemory(lpSwapTmp);				\
								__SWAP__OUT__LEAVE__:



#define IS_NAN(X)						((X) != (X))

#define FLOAT_EQUAL(X,Y)					((EPSILON >= ABSOLUTE_VALUE((((LONG_UNION)X).lX - ((LONG_UNION)Y).lX))) ? TRUE : FALSE)


#define DOUBLE_EQUAL(X,Y)					((EPSILON_D >= ABSOLUTE_VALUE((((LONGLONG_UNION)X).llX - ((LONGLONG_UNION)Y).llX))) ? TRUE : FALSE)

#define IMPLIES(X,Y)						(!(X) || (Y))

#define SWAP(X,Y)						(X) ^= (Y); (Y) ^= (X); (X) ^= (Y);
#define SORT(X,Y)						do { (((X) > (Y)) ? SWAP((X),(Y)) : NO_OPERATION()); } while (0)
#define COMPARE(X,Y)						(((X) > (Y)) - ((X) < (Y)))
#define GET_SIGN(X)						COMPARE((X),0)
#define IS_ODD(X)						((X) & 1)
#define IS_EVEN(X)						(!IS_ODD(X))

#define VALIDATE(TEST,FAIL_ACTION)				if (!(TEST)) { assert(0); FAIL_ACTION; }
#define RANGE_CHECK(X,M,MX,F_RET)				VALIDATE((M) <= (X) && (V) <= (MX), return F_RET)
#define IS_BETWEEN(X,M,MX)					((UBYTE)((X) >= (M) && (X) <= (MX)))

#define INFINITE_LOOP()						for (;;)
#define INFINITE_LOOP_WITH_COUNTER(X)				for(;;(X)++)

#define RADIANS_TO_DEGREES(X)					((X) * (180/PI))
#define DEGREES_TO_RADIANS(X)					((X) * (PI/180))

/* Clamps X to between L and H */
/*
#define CLAMP(X,L,H)											\
								((X) = ((X) < (L)) ? (L) : (X));	\
								((X) = ((X) > (H)) ? (H) : (X))
*/
#define CLAMP(X,L,H) 						(((X) < (L)) ? (L) : (((X) > (H)) ? (H) : (X)))

#define UNLIKELY(X)						__builtin_expect(!!(X), 0)
#define LIKELY(X)						__builtin_expect(!!(X), 1)
                                    
#define EXIT_IF_NULL(X, RET_CODE)       			if (!(X)) { return (RET_CODE); }                         
#define EXIT_IF_NOT_ZERO(X, RET_CODE)				if ((X) != 0) { return (RET_CODE); }
#define EXIT_IF_NOT_VALUE(X, VAL, RET_CODE)			if ((X) != (VAL)) { return (RET_CODE); }
#define EXIT_IF_VALUE(X, VAL, RET_CODE)				if ((X) == (VAL)) { return (RET_CODE); }
#define EXIT_IF_CONDITION(CON, RET_CODE)			if ((CON)) { return (RET_CODE); }
#define EXIT_IF_UNLIKELY_NULL(X, RET_CODE)			if (UNLIKELY(!(X))) { return (RET_CODE); }
#define EXIT_IF_LIKELY_NULL(X, RET_CODE)			if (LIKELY(!(X))) { return (RET_CODE); }
#define EXIT_IF_UNLIKELY_NOT_ZERO(X, RET_CODE)			if (UNLIKELY((X) != 0)) { return (RET_CODE); }
#define EXIT_IF_LIKELY_NOT_ZERO(X, RET_CODE)			if (LIKELY((X) != 0)) { return (RET_CODE); }
#define EXIT_IF_UNLIKELY_NOT_VALUE(X, VAL, RET_CODE)		if (UNLIKELY((X) != (VAL))) { return (RET_CODE); }
#define EXIT_IF_LIKELY_NOT_VALUE(X, VAL, RET_CODE)		if (LIKELY((X) != (VAL))) { return (RET_CODE); }
#define EXIT_IF_UNLIKELY_VALUE(X, VAL, RET_CODE)		if (UNLIKELY(X) == (VAL)) { return (RET_CODE); }
#define EXIT_IF_LIKELY_VALUE(X, VAL, RET_CODE)			if (LIKELY(X) == (VAL)) { return (RET_CODE); }
#define EXIT_IF_UNLIKELY_CONDITION(X, CON, RET_CODE)		if (UNLIKELY((X) (CON))) { return (RET_CODE); }
#define EXIT_IF_LIKELY_CONDITION(X, CON, RET_CODE)		if (LIKELY((X) (CON))) { return (RET_CODE); }


#define SET_ERROR_AND_EXIT(E,RET)				do { SetLastGlobalError((E)); return (RET); } while (0)
#define SET_ERROR_AND_EXIT_IF_NULL(X,E,RET)			if (!(X)) { SetLastGlobalError((E)); return (RET); }
#define SET_ERROR_AND_EXIT_IF_NOT_VALUE(X,V,E,RET)		if ((X) != V) { SetLastGlobalError((E)); return (RET); }
#define SET_ERROR_AND_EXIT_IF_VALUE(X,V,E,RET)			if ((X) == V) { SetLastGlobalError((E)); return (RET); }
#define SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(X,E,RET)		if (UNLIKELY(!(X))) { SetLastGlobalError((E)); return (RET); }
#define SET_ERROR_AND_EXIT_IF_LIKELY_NULL(X,E,RET)		if (LIKELY(!(X))) { SetLastGlobalError((E)); return (RET); }
#define SET_ERROR_AND_EXIT_IF_UNLLIKELY_NOT_VALUE(X,V,E,RET)	if ((UNLIKELY((X) != (V)))) { SetLastGlobalError((E)); return (RET); }
#define SET_ERROR_AND_EXIT_IF_LIKELY_NOT_VALUE(X,V,E,RET)	if ((LIKELY((X) != (V)))) { SetLastGlobalError((E)); return (RET); }
#define SET_ERROR_AND_EXIT_IF_UNLLIKELY_VALUE(X,V,E,RET)	if ((UNLIKELY((X) == (V)))) { SetLastGlobalError((E)); return (RET); }
#define SET_ERROR_AND_EXIT_IF_LIKELY_VALUE(X,V,E,RET)		if ((LIKELY((X) == (V)))) { SetLastGlobalError((E)); return (RET); }


#define EXIT_AND_RELEASE_IF_NULL(X, LOCK, RET_CODE)		if (!(X)) { ReleaseSingleObject((LOCK)); return (RET_CODE); }
#define EXIT_AND_RELEASE_IF_NOT_ZERO(X, LOCK, RET_CODE)		if (0 != (X)) { ReleaseSingleObject((LOCK)); return (RET_CODE); }
#define EXIT_AND_RELEASE_IF_NOT_VALUE(X, LOCK, VAL, RET_CODE)	if ((VAL) != (X)) { ReleaseSingleObject((LOCK)); return (RET_CODE); }
#define EXIT_AND_RELEASE_IF_VALUE(X, LOCK, VAL, RET_CODE)	if ((VAL) == (X)) { ReleaseSingleObject((LOCK)); return (RET_CODE); }



#define POW(X,Y)					do { BYTE Y_;						\
								for (Y_ = 0; Y_ < Y; Y_++) 			\
									X *= Y; } while (0)	


#endif

