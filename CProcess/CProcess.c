/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CProcess.h"
#include "../CFile/CFileObject.c"
#include "../CNetworking/CSocketObject.c"


#define READ_FD		0
#define WRITE_FD	1


typedef struct __PROCESS 
{
	INHERITS_FROM_HANDLE();
	HANDLE 		hFile;
	LPFILE		lpfFile;
	LPSTR 		lpszProgram;
	LPSTR 		lpszArgs;
	PID 		pidId;
	LONG 		lDescriptors[2];
	ULONGLONG 	ullSleepTime;
	LONG 		lStatus;
} PROCESS, *LPPROCESS;




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__DestroyProcess(
	_In_ 		HDERIVATIVE 		hDerivative,
	_In_Opt_ 	ULONG 			ulFlags
){
	UNREFERENCED_PARAMETER(ulFlags);

	LPPROCESS lppProc;
	lppProc = (LPPROCESS)hDerivative;

	DestroyObject(lppProc->hFile);
	KillProcess(lppProc->hThis, SIGINT);

	FreeMemory(lppProc->lpszArgs);
	FreeMemory(lppProc->lpszProgram);
	FreeMemory(lppProc);

	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__WaitForProcess(
	_In_ 		HANDLE 			hProcess,
	_In_		ULONGLONG 		ullMilliSeconds
){
	UNREFERENCED_PARAMETER(ullMilliSeconds);

	LPPROCESS lppProc;
	TIMESPEC tmSpec;
	lppProc = (LPPROCESS)GetHandleDerivative(hProcess);
	
	HighResolutionClock(&tmSpec);

	if (0 == ullMilliSeconds)
	{ return (0 < waitpid(lppProc->pidId, &(lppProc->lStatus), WNOHANG)) ? TRUE : FALSE; }

	else if (INFINITE_WAIT_TIME == ullMilliSeconds)
	{ return (0 < waitpid(lppProc->pidId, &(lppProc->lStatus), 0)) ? TRUE : FALSE;}

	else
	{
		while (HighResolutionClockAndGetMilliSeconds(&tmSpec) <= ullMilliSeconds)
		{
			if (0 < waitpid(lppProc->pidId, &(lppProc->lStatus), WNOHANG))
			{ JUMP(__Finished); }
			Sleep(lppProc->ullSleepTime);
		}
	}
	
	return FALSE;
	
	__Finished:
	return TRUE;
}




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNET_API
HANDLE
CreateProcess(
	_In_Z_		LPCSTR RESTRICT		lpcszProgram,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszArgs,
	_In_ 		BOOL 			bWritable
){
	EXIT_IF_UNLIKELY_NULL(lpcszProgram, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NULL(lpcszArgs, NULL_OBJECT);

	HANDLE hProcess;
	LPPROCESS lppProc;
	UARCHLONG ualProgLength, ualArgLength;
	LPSTR lpszCommand;

	lppProc = GlobalAllocAndZero(sizeof(PROCESS));
	/* cppcheck-suppress memleak */
	EXIT_IF_UNLIKELY_NULL(lppProc, NULL_OBJECT);

	ualProgLength = StringLength(lpcszProgram);
	ualArgLength = StringLength(lpcszArgs);

	lppProc->lpszProgram = GlobalAllocAndZero((sizeof(CHAR) * ualProgLength) + 1);
	if (NULLPTR == lppProc->lpszProgram)
	{
		FreeMemory(lppProc);
		return NULL_OBJECT;
	}

	lppProc->lpszArgs = GlobalAllocAndZero((sizeof(CHAR) * ualArgLength) + 1);
	if (NULLPTR == lppProc->lpszArgs)
	{
		FreeMemory(lppProc->lpszProgram);
		FreeMemory(lppProc);
		return NULL_OBJECT;
	}

	StringCopy(lppProc->lpszProgram, lpcszProgram);
	StringCopy(lppProc->lpszArgs, lpcszArgs);

	lpszCommand = LocalAllocAndZero((sizeof(CHAR) * (ualArgLength + ualProgLength)) + 2);
	if (NULLPTR == lpszCommand)
	{
		FreeMemory(lppProc->lpszArgs);
		FreeMemory(lppProc->lpszProgram);
		FreeMemory(lppProc);
		return NULL_OBJECT;
	}

	StringPrintFormatSafe(
		lpszCommand,
		(sizeof(CHAR) * (ualArgLength + ualProgLength)) + 2,
		"%s %s",
		lpcszProgram,
		lpcszArgs
	);

	lppProc->ullSleepTime = 0x01U;
	lppProc->lStatus = (LONG)MAX_ULONG;

	pipe(lppProc->lDescriptors);

	lppProc->pidId = fork();

	/* Child Process */
	if (0 == lppProc->pidId)
	{
		LONG lOut;

		if (FALSE == bWritable)
		{
			close(lppProc->lDescriptors[READ_FD]);
			dup2(lppProc->lDescriptors[WRITE_FD], 1);
		}
		else
		{
			close(lppProc->lDescriptors[WRITE_FD]);
			dup2(lppProc->lDescriptors[READ_FD], 0);
		}

		setpgid(lppProc->pidId, lppProc->pidId);
		execl("/bin/sh", "/bin/sh", "-c", lpszCommand, NULLPTR);
		waitpid(getpid(), &lOut, 0);
		PostQuitMessage(lOut);
	}
	/* Parent Process */
	else
	{
		if (FALSE == bWritable)
		{ close(lppProc->lDescriptors[WRITE_FD]); }
		else
		{ close(lppProc->lDescriptors[READ_FD]); }
	}

	if (FALSE == bWritable)
	{ lppProc->lpfFile = fdopen(lppProc->lDescriptors[READ_FD], "r"); }
	else
	{ lppProc->lpfFile = fdopen(lppProc->lDescriptors[WRITE_FD], "w"); }



	hProcess = CreateHandleWithSingleInheritor(
		lppProc,
		&(lppProc->hThis),
		HANDLE_TYPE_PROCESS,
		__DestroyProcess,
		NULL,
		NULLPTR,
		NULL,
		NULLPTR,
		NULL,
		&(lppProc->ullId),
		NULL
	);

	SetHandleEventWaitFunction(hProcess, __WaitForProcess, NULL);

	lppProc->hFile = OpenFileWithFilePointer(lppProc->lpfFile, FILE_PERMISSION_CREATE | FILE_PERMISSION_READ);

	if (NULLPTR != lpszCommand)
	{ FreeMemory(lpszCommand); }

	return hProcess;
}




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNET_API
HANDLE
CreateProcessEx(
	_In_Z_ 		LPCSTR RESTRICT		lpcszProgram,
	_In_Z_ 		LPCSTR RESTRICT		lpcszArgs,
	_In_ 		BOOL 			bWritable,
	_In_Opt_ 	HANDLE RESTRICT 	hStdOut
){
	EXIT_IF_UNLIKELY_NULL(lpcszProgram, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NULL(lpcszArgs, NULL_OBJECT);

	HANDLE hProcess;
	LPPROCESS lppProc;
	UARCHLONG ualProgLength, ualArgLength;
	LPSTR lpszCommand;

	if (NULL_OBJECT == hStdOut)
	{ hStdOut = GetStandardOut(); }

	lppProc = GlobalAllocAndZero(sizeof(PROCESS));
	/* cppcheck-suppress memleak */
	EXIT_IF_UNLIKELY_NULL(lppProc, NULL_OBJECT);

	ualProgLength = StringLength(lpcszProgram);
	ualArgLength = StringLength(lpcszArgs);

	lppProc->lpszProgram = GlobalAllocAndZero((sizeof(CHAR) * ualProgLength) + 1);
	if (NULLPTR == lppProc->lpszProgram)
	{
		FreeMemory(lppProc);
		return NULL_OBJECT;
	}

	lppProc->lpszArgs = GlobalAllocAndZero((sizeof(CHAR) * ualArgLength) + 1);
	if (NULLPTR == lppProc->lpszArgs)
	{
		FreeMemory(lppProc->lpszProgram);
		FreeMemory(lppProc);
		return NULL_OBJECT;
	}

	StringCopy(lppProc->lpszProgram, lpcszProgram);
	StringCopy(lppProc->lpszArgs, lpcszArgs);

	lpszCommand = LocalAllocAndZero((sizeof(CHAR) * (ualArgLength + ualProgLength)) + 2);
	if (NULLPTR == lpszCommand)
	{
		FreeMemory(lppProc->lpszArgs);
		FreeMemory(lppProc->lpszProgram);
		FreeMemory(lppProc);
		return NULL_OBJECT;
	}


	StringPrintFormatSafe(
		lpszCommand,
		(sizeof(CHAR) * (ualArgLength + ualProgLength)) + 2,
		"%s %s",
		lpcszProgram,
		lpcszArgs
	);

	lppProc->ullSleepTime = 0x01U;
	lppProc->lStatus = (LONG)MAX_ULONG;

	pipe(lppProc->lDescriptors);

	lppProc->pidId = fork();

	/* Child Process */
	if (0 == lppProc->pidId)
	{
		LONG lOut;

		if (FALSE == bWritable)
		{
			close(lppProc->lDescriptors[READ_FD]);
			dup2(lppProc->lDescriptors[WRITE_FD], 1);
		}
		else
		{
			close(lppProc->lDescriptors[WRITE_FD]);
			switch (GetHandleDerivativeType(hStdOut))
			{
				case HANDLE_TYPE_FILE:
				{
					LPFILE_OBJ lpFile;
					lpFile = GetHandleDerivative(hStdOut);

					dup2(0, fileno(lpFile->fFile));
					break;
				}

				case HANDLE_TYPE_SOCKET:
				{
					LPSOCKET lpSocket;
					lpSocket = GetHandleDerivative(hStdOut);

					dup2(lpSocket->lDescriptor, 0);					
					break;
				}

				case HANDLE_TYPE_SOCKET6:
				{
					LPSOCKET6 lpSocket6;
					lpSocket6 = GetHandleDerivative(hStdOut);

					dup2(lpSocket6->lDescriptor, 0);
					break;
				}

				default: PostQuitMessage(0);
			}
		}

		setpgid(lppProc->pidId, lppProc->pidId);
		execl("/bin/sh", "/bin/sh", "-c", lpszCommand, NULLPTR);
		waitpid(getpid(), &lOut, 0);
		PostQuitMessage(lOut);
	}
	/* Parent Process */
	else
	{
		if (FALSE == bWritable)
		{ close(lppProc->lDescriptors[WRITE_FD]); }
		else
		{ close(lppProc->lDescriptors[READ_FD]); }
	}

	if (FALSE == bWritable)
	{ lppProc->lpfFile = fdopen(lppProc->lDescriptors[READ_FD], "r"); }
	else
	{ lppProc->lpfFile = fdopen(lppProc->lDescriptors[WRITE_FD], "w"); }



	hProcess = CreateHandleWithSingleInheritor(
		lppProc,
		&(lppProc->hThis),
		HANDLE_TYPE_PROCESS,
		__DestroyProcess,
		NULL,
		NULLPTR,
		NULL,
		NULLPTR,
		NULL,
		&(lppProc->ullId),
		NULL
	);

	SetHandleEventWaitFunction(hProcess, __WaitForProcess, NULL);

	lppProc->hFile = OpenFileWithFilePointer(lppProc->lpfFile, FILE_PERMISSION_CREATE | FILE_PERMISSION_READ);

	if (NULLPTR != lpszCommand)
	{ FreeMemory(lpszCommand); }
	
	return hProcess;
}




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNET_API
HANDLE
GetProcessOutput(
	_In_		HANDLE 			hProcess
){
	EXIT_IF_UNLIKELY_NULL(hProcess, NULL_OBJECT);
	EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hProcess), HANDLE_TYPE_PROCESS, NULL_OBJECT);

	LPPROCESS lppProc;
	lppProc = OBJECT_CAST(hProcess, LPPROCESS);
	EXIT_IF_UNLIKELY_NULL(lppProc, NULL_OBJECT);

	return lppProc->hFile;
}




_Success_(return != 0, _Non_Locking_)
PODNET_API
PID
GetProcessIdentifer(
	_In_ 		HANDLE 			hProcess
){
	EXIT_IF_UNLIKELY_NULL(hProcess, 0);
	EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hProcess), HANDLE_TYPE_PROCESS, 0);

	LPPROCESS lppProc;
	lppProc = OBJECT_CAST(hProcess, LPPROCESS);
	EXIT_IF_UNLIKELY_NULL(lppProc, 0);

	return lppProc->pidId;
}




_Success_(return != (LONG)MAX_ULONG, _Non_Locking_)
PODNET_API
LONG
GetProcessReturnStatus(
	_In_ 		HANDLE 			hProcess
){
	EXIT_IF_UNLIKELY_NULL(hProcess, 0);
	EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hProcess), HANDLE_TYPE_PROCESS, 0);

	LPPROCESS lppProc;
	lppProc = OBJECT_CAST(hProcess, LPPROCESS);
	EXIT_IF_UNLIKELY_NULL(lppProc, 0);

	return lppProc->lStatus;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
KillProcess(
	_In_ 		HANDLE 			hProcess,
	_In_ 		ULONG 			ulSignal
){
	EXIT_IF_UNLIKELY_NULL(hProcess, FALSE);
	EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hProcess), HANDLE_TYPE_PROCESS, FALSE);

	LPPROCESS lppProc;
	lppProc = (LPPROCESS)GetHandleDerivative(hProcess);
	EXIT_IF_UNLIKELY_NULL(lppProc, FALSE);

	kill(lppProc->pidId, ulSignal);

	return TRUE;
}
