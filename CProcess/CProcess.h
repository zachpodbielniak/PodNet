/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CPROCESS_H
#define CPROCESS_H


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../Annotation.h"
#include "../CHandle/CHandle.h"
#include "../CFile/CFile.h"


#ifndef DEFINED_PID
#define DEFINED_PID
typedef pid_t		PID;
#endif



_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNET_API
HANDLE
CreateProcess(
	_In_Z_		LPCSTR RESTRICT		lpcszProgram,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszArgs,
	_In_ 		BOOL 			bWritable
);




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNET_API
HANDLE
CreateProcessEx(
	_In_Z_ 		LPCSTR RESTRICT		lpcszProgram,
	_In_Z_ 		LPCSTR RESTRICT		lpcszArgs,
	_In_ 		BOOL 			bWritable,
	_In_Opt_ 	HANDLE RESTRICT 	hStdOut
);




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNET_API
HANDLE
GetProcessOutput(
	_In_		HANDLE 			hProcess
);




_Success_(return != 0, _Non_Locking_)
PODNET_API
PID
GetProcessIdentifer(
	_In_ 		HANDLE 			hProcess
);



_Success_(return != (LONG)MAX_ULONG, _Non_Locking_)
PODNET_API
LONG
GetProcessReturnStatus(
	_In_ 		HANDLE 			hProcess
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
KillProcess(
	_In_ 		HANDLE 			hProcess,
	_In_ 		ULONG 			ulSignal
);






#endif