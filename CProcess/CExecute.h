/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CEXECUTE_H
#define CEXECUTE_H


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../CFile/CFile.h"




typedef enum __EXECUTE_FLAGS
{
	EXECUTE_SEARCH_PATH		= 1 << 0,
	EXECUTE_INHERIT_ENVIRONMENT	= 1 << 1
} EXECUTE_FLAGS;




_Success_(return != -1, _Non_Locking_)
PODNET_API
LONG
Execute(
	_In_Opt_ 		HANDLE 			hFile,
	_In_Opt_Z_ 		LPCSTR 			lpcszFile,
	_In_Opt_Z_ 		DLPCSTR			dlpszArgValues,
	_In_Opt_Z_ 		DLPCSTR 		dlpszEnvArgValues,
	_In_Opt_ 		ULONG 			ulFlags
);


#endif