/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CExecute.h"
#include "../CFile/CFileObject.c"




_Success_(return != -1, _Non_Locking_)
PODNET_API
LONG
Execute(
	_In_Opt_ 		HANDLE 			hFile,
	_In_Opt_Z_ 		LPCSTR 			lpcszFile,
	_In_Opt_Z_ 		DLPCSTR			dlpszArgValues,
	_In_Opt_Z_ 		DLPCSTR 		dlpszEnvArgValues,
	_In_Opt_ 		ULONG 			ulFlags
){
	if (NULL_OBJECT == hFile && NULLPTR == lpcszFile)
	{ return -1 ;}
	else if (NULL_OBJECT != hFile)
	{
		if (HANDLE_TYPE_FILE != GetHandleDerivativeType(hFile))
		{ return -1; }

		return fexecve(
			OBJECT_CAST(hFile, LPFILE_OBJ)->lDescriptor,
			(DCLPSTR)dlpszArgValues,
			(DCLPSTR)dlpszEnvArgValues
		);
	}
	else 
	{
		if (ulFlags & EXECUTE_INHERIT_ENVIRONMENT)
		{
			if (ulFlags & EXECUTE_SEARCH_PATH)
			{
				return execvp(
					lpcszFile,
					(DCLPSTR)dlpszArgValues
				);
			}
			else 
			{
				return execv(
					lpcszFile,
					(DCLPSTR)dlpszArgValues
				);
			}
		}
		else 
		{
			if (ulFlags & EXECUTE_SEARCH_PATH)
			{
				return execvpe(
					lpcszFile,
					(DCLPSTR)dlpszArgValues,
					(DCLPSTR)dlpszEnvArgValues
				);
			}
			else 
			{
				return execve(
					lpcszFile,
					(DCLPSTR)dlpszArgValues,
					(DCLPSTR)dlpszEnvArgValues
				);
			}
		}
	}
	
	return -1;
}
