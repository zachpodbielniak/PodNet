/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CPARSER_H
#define CPARSER_H


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../Ascii.h"
#include "../CThread/CThread.h"
#include "../CString/CString.h"
#include "CLexer.h"
#include "CTokenTable.h"



typedef UARCHLONG 			MACHINESTATE;
#define MACHINE_STATE_NULL		((MACHINESTATE)-1)



typedef _Call_Back_ MACHINESTATE (* LPFN_PARSER_CALLBACK)(
	_In_ 		HANDLE 			hParser,
	_In_ 		MACHINESTATE		msCurrent,
	_In_ 		LEXEME			lxLeft,
	_In_ 		LEXEME 			lxRight,
	_In_ 		TOKENFLAG		tfType,
	_In_Opt_ 	LPVOID 			lpData
);




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNET_API
HANDLE
CreateParser(
	_In_ 		HANDLE 			hLexer,
	_In_ 		HANDLE 			hTokenTable
);




_Success_(return != FALSE, _Interlocked_Operation_)
PODNET_API
BOOL
ParserRegisterCallback(
	_In_ 		HANDLE 			hParser,
	_In_ 		TOKENFLAG		tfType,
	_In_ 		LPFN_PARSER_CALLBACK	lpfnCallback,
	_In_Opt_ 	LPVOID 			lpData
);




_Success_(return != FALSE, _Interlocked_Operation_)
PODNET_API
BOOL
ParserSetText(
	_In_ 		HANDLE 			hParser,
	_In_ 		LPCSTR 			lpcszText
);




_Success_(return != FALSE, _Interlocked_Operand_)
PODNET_API
BOOL
Parse(
	_In_ 		HANDLE 			hParser
);



#endif