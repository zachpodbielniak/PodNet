/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CTOKENTABLE_H
#define CTOKENTABLE_H


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../Ascii.h"
#include "../CHandle/CHandle.h"
#include "../CContainers/CVector.h"
#include "../CContainers/CHashTable.h"
#include "CLexer.h"



typedef ULONGLONG 			TOKENFLAG, *LPTOKENFLAG, **DLPTOKENFLAG;


typedef struct __TOKEN
{	
	LEXEME			lxValue;
	TOKENFLAG		tfValue;
} TOKEN, *LPTOKEN, **DLPTOKEN;




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNET_API
HANDLE
CreateTokenTable(
	_In_ 		LPTOKEN		lptknDescriptions,
	_In_ 		UARCHLONG 	ualNumberOfTokens
);




_Success_(return != FALSE, _Interlocked_Operation_)
PODNET_API
BOOL
TokenTableAppendToken(
	_In_ 		HANDLE 		hTokenTable,
	_In_ 		LPTOKEN		lptknDescriptions,
	_In_ 		UARCHLONG 	ualNumberOfTokens
);




_Success_(return != (UARCHLONG)-1, _Non_Locking_)
PODNET_API
TOKENFLAG
TokenTableGetType(
	_In_ 		HANDLE 		hTokenTable,
	_In_ 		LEXEME 		lxValue
);




_Success_(return != (UARCHLONG)-1, _Non_Locking_)
PODNET_API
UARCHLONG
TokenTableGetSize(
	_In_ 		HANDLE 		hTokenTable
);



#endif