#include "CIniFileParser.h"


typedef struct __INI_PARSER
{
	INHERITS_FROM_HANDLE();
	HANDLE 			hParserLock;
	HANDLE 			hFile;

	HVECTOR 		hvResultVector;
	HLINKEDLIST		hllResultLinkedList;
	HHASHTABLE		hhtResultHashTable;
	ULONG 			ulCreationFlags;
} INI_PARSER, *LPINI_PARSER, **DLPINI_PARSER;




_Call_Back_
_Success_(return != FALSE, _Interlocked_Operation_)
INTERNAL_OPERATION
BOOL
__DestroyIniFile(
	_In_		HDERIVATIVE 	hDerivative,
	_In_Opt_ 	ULONG 		ulFlags
){
	UNREFERENCED_PARAMETER(ulFlags);
	EXIT_IF_UNLIKELY_NULL(hDerivative, FALSE);

	LPINI_PARSER lpipState;
	lpipState = hDerivative;

	DestroyObject(lpipState->hParserLock);
	DestroyObject(lpipState->hFile);

	if (NULLPTR != lpipState->hvResultVector)
	{ DestroyVector(lpipState->hvResultVector); }

	return TRUE;
}




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNET_API 
HANDLE 
OpenIniFile(
	_In_Z_		LPCSTR		lpcszFile,
	_In_Opt_ 	ULONG 		ulFlags
){
	EXIT_IF_UNLIKELY_NULL(lpcszFile, NULL_OBJECT);

	HANDLE hIniParser;
	LPINI_PARSER lpipState;
	lpipState = GlobalAllocAndZero(sizeof(INI_PARSER));

	if (NULLPTR == lpipState)
	{ JUMP(__FAILED); }

	lpipState->hParserLock = CreateCriticalSectionAndSpecifySpinCount(0x0C00);
	if (NULL_OBJECT == lpipState->hParserLock)
	{ JUMP(__FAILED_CRITICAL_SECTION); }

	lpipState->ulCreationFlags = ulFlags;

	/* Create The Ini Parser Object */
	hIniParser = CreateHandleWithSingleInheritor(
		lpipState,
		&(lpipState->hThis),
		HANDLE_TYPE_INI_PARSER,
		__DestroyIniFile,
		0,
		NULLPTR,
		0,
		NULLPTR,
		0,
		&(lpipState->ullId),
		0
	);

	if (NULL_OBJECT == hIniParser)
	{ JUMP(__FAILED_OBJECT_CREATION); }

	/* Open Config File */
	lpipState->hFile = OpenFile(lpcszFile, FILE_PERMISSION_READ, 0);

	if (NULL_OBJECT == lpipState->hFile)
	{ JUMP(__FAILED_OPEN_FILE); }

	return hIniParser;

	__FAILED_OPEN_FILE:
	DestroyObject(hIniParser);
	__FAILED_OBJECT_CREATION:
	DestroyObject(lpipState->hParserLock);
	__FAILED_CRITICAL_SECTION:
	FreeMemory(lpipState);
	__FAILED:
	return NULL_OBJECT;
}




_Success_(return != FALSE, _Acquires_Shared_Lock_())
PODNET_API 
BOOL 
ParseIniFile(
	_In_		HANDLE 		hIniFile,
	_In_Opt_ 	ULONG		ulFlags
){
	EXIT_IF_UNLIKELY_NULL(hIniFile, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hIniFile), HANDLE_TYPE_INI_PARSER, FALSE);

	LPINI_PARSER lpipState;
	INI_ENTRY ieData;
	ARCHLONG alRead;
	CSTRING csBuffer[8192];
	LPSTR lpszBuffer;
	LPSTR lpszSplit;
	UARCHLONG ualSize;
	INI_SECTION_VECTOR isvCurrent;
	

	lpipState = OBJECT_CAST(hIniFile, LPINI_PARSER);
	EXIT_IF_UNLIKELY_NULL(lpipState, FALSE);

	lpszBuffer = (LPSTR)csBuffer;
	ZeroMemory(csBuffer, sizeof(csBuffer));

	/* Read The File */
	while (-1 != (alRead = ReadLineFromFile(
		lpipState->hFile,
		&lpszBuffer,
		sizeof(csBuffer) - 1,
		0
	))){
		/* Check For Comment */
		if (
			'\n' == csBuffer[0] ||
			'#' == csBuffer[0] ||
			';' == csBuffer[0]
		){ JUMP(__CONTINUE); }
		
		StringReplaceCharacter(lpszBuffer, '\n', '\0');
		if ('\0' == lpszBuffer[0] && '\0' != lpszBuffer[1])
		{ lpszBuffer = (LPSTR)((UARCHLONG)lpszBuffer + 0x01U); }

		/* Check For Section */
		if ('[' == lpszBuffer[0])
		{
			switch (ulFlags)
			{
				case PARSE_INI_USE_DEFAULT:
				{ 
					JUMP(__USE_VECTORS_SECTION);
					break; 
				}
				case PARSE_INI_USE_HASHTABLE:
				{

					break;
				}

				case PARSE_INI_USE_LINKED_LIST:
				{

					break;
				}

				case PARSE_INI_USE_VECTORS:
				{
					__USE_VECTORS_SECTION:
					/* If Previous State, Push */
					if (NULLPTR != lpipState->hvResultVector)
					{ VectorPushBack(lpipState->hvResultVector, &isvCurrent, 0); }
					else 
					{ lpipState->hvResultVector = CreateVector(0x10, sizeof(INI_SECTION_VECTOR), 0); }
					ZeroMemory(&isvCurrent, sizeof(INI_SECTION_VECTOR));

					/* Don't need closing ']' */
					StringReplaceCharacter(lpszBuffer, ']', '\0');

					/* Offset for '[' */
					lpszBuffer = (LPSTR)((UARCHLONG)lpszBuffer + 0x01U);
					ualSize = StringLength(lpszBuffer);

					isvCurrent.lpszSection = LocalAllocAndZero(ualSize + 0x01U);
					CopyMemory(isvCurrent.lpszSection, lpszBuffer, ualSize);

					isvCurrent.hvEntries = CreateVector(16, sizeof(INI_ENTRY), 0);
					break;
				}
			}
		}
		else 
		{
			/* Do The Split */
			lpszSplit = StringInString(lpszBuffer, "=");
			if (NULLPTR == lpszSplit)
			{ JUMP(__CONTINUE); }

			/* Copy Key */
			ualSize = (UARCHLONG)(lpszSplit) - (UARCHLONG)lpszBuffer;
			ieData.lpszKey = LocalAllocAndZero(ualSize + 0x01U);
			CopyMemory(ieData.lpszKey, lpszBuffer, ualSize);

			/* Copy Value */
			lpszSplit = (LPSTR)((UARCHLONG)lpszSplit + 0x01U);
			ualSize = StringLength(lpszSplit);
			ieData.lpszValue = LocalAllocAndZero(ualSize + 0x01U);
			CopyMemory(ieData.lpszValue, lpszSplit, ualSize);

			switch (ulFlags)
			{
				case PARSE_INI_USE_DEFAULT:
				{ 
					JUMP(__USE_VECTORS_ENTRY);
					break; 
				}
				case PARSE_INI_USE_HASHTABLE:
				{

					break;
				}

				case PARSE_INI_USE_LINKED_LIST:
				{

					break;
				}

				case PARSE_INI_USE_VECTORS:
				{
					__USE_VECTORS_ENTRY:
					VectorPushBack(isvCurrent.hvEntries, &ieData, 0);
					break;
				}
			}
		}

		__CONTINUE:
		ZeroMemory(csBuffer, sizeof(csBuffer));
	}
		
	/* Final Push */
	switch (ulFlags)
	{
		case PARSE_INI_USE_DEFAULT:
		{ 
			JUMP(__USE_VECTORS_ENDING);
			break; 
		}
		case PARSE_INI_USE_HASHTABLE:
		{

			break;
		}
		case PARSE_INI_USE_LINKED_LIST:
		{

			break;
		}
		case PARSE_INI_USE_VECTORS:
		{
			__USE_VECTORS_ENDING:

			/* If Previous State, Push */
			if (NULLPTR != lpipState->hvResultVector)
			{ VectorPushBack(lpipState->hvResultVector, &isvCurrent, 0); }

			break;
		}
	}

	return TRUE;
}




_Success_(return != NULLPTR, _Acquires_Shared_Lock_())
PODNET_API
HVECTOR 
GetIniParseResultVector(
	_In_ 		HANDLE 		hIniFile
){
	EXIT_IF_UNLIKELY_NULL(hIniFile, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hIniFile), HANDLE_TYPE_INI_PARSER, NULLPTR);

	LPINI_PARSER lpipState;
	HVECTOR hvRet;

	lpipState = OBJECT_CAST(hIniFile, LPINI_PARSER);
	EXIT_IF_UNLIKELY_NULL(lpipState, NULLPTR);

	WaitForSingleObject(lpipState->hParserLock, INFINITE_WAIT_TIME);
	hvRet = lpipState->hvResultVector;
	ReleaseSingleObject(lpipState->hParserLock);

	return hvRet;
}




_Success_(return != NULLPTR, _Acquires_Shared_Lock_())
PODNET_API 
HLINKEDLIST
GetIniParseResultLinkedList(
	_In_ 		HANDLE 		hIniFile
){
	EXIT_IF_UNLIKELY_NULL(hIniFile, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hIniFile), HANDLE_TYPE_INI_PARSER, NULLPTR);

	LPINI_PARSER lpipState;
	HLINKEDLIST hllRet;

	lpipState = OBJECT_CAST(hIniFile, LPINI_PARSER);
	EXIT_IF_UNLIKELY_NULL(lpipState, NULLPTR);

	WaitForSingleObject(lpipState->hParserLock, INFINITE_WAIT_TIME);
	hllRet = lpipState->hllResultLinkedList;
	ReleaseSingleObject(lpipState->hParserLock);

	return hllRet;
}




_Success_(return != NULLPTR, _Acquires_Shared_Lock_())
PODNET_API 
HHASHTABLE
GetIniParseResultHashTable(
	_In_ 		HANDLE 		hIniFile
){
	EXIT_IF_UNLIKELY_NULL(hIniFile, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hIniFile), HANDLE_TYPE_INI_PARSER, NULLPTR);

	LPINI_PARSER lpipState;
	HHASHTABLE hhtRet;

	lpipState = OBJECT_CAST(hIniFile, LPINI_PARSER);
	EXIT_IF_UNLIKELY_NULL(lpipState, NULLPTR);

	WaitForSingleObject(lpipState->hParserLock, INFINITE_WAIT_TIME);
	hhtRet = lpipState->hhtResultHashTable;
	ReleaseSingleObject(lpipState->hParserLock);

	return hhtRet;
}




_Success_(return != NULLPTR, _Acquires_Shared_Lock_())
PODNET_API
LPSTR
GetIniParseValueFromSectionAndSetting(
	_In_ 		HANDLE 		hIniFile,
	_In_Z_ 		LPCSTR RESTRICT	lpcszSection,
	_In_Z_		LPCSTR RESTRICT	lpcszSetting
){
	EXIT_IF_UNLIKELY_NULL(hIniFile, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpcszSection, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpcszSetting, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hIniFile), HANDLE_TYPE_INI_PARSER, NULLPTR);

	LPINI_PARSER lpipState;
	LPINI_SECTION_VECTOR lpisvIterator;
	HVECTOR hvResults;
	LPSTR lpszOut;
	UARCHLONG ualIndex;
	UARCHLONG ualJndex;

	lpipState = OBJECT_CAST(hIniFile, LPINI_PARSER);
	EXIT_IF_UNLIKELY_NULL(lpipState, NULLPTR);
	lpszOut = NULLPTR;

	WaitForSingleObject(lpipState->hParserLock, INFINITE_WAIT_TIME);
	hvResults = lpipState->hvResultVector;

	for (
		ualIndex = 0;
		ualIndex < VectorSize(hvResults);
		ualIndex++
	){
		lpisvIterator = VectorAt(hvResults, ualIndex);
		if (NULLPTR == lpisvIterator)
		{ continue; }

		if (0 == StringCompare(lpcszSection, lpisvIterator->lpszSection))
		{
			for (
				ualJndex = 0;
				ualJndex < VectorSize(lpisvIterator->hvEntries);
				ualJndex++
			){
				LPINI_ENTRY lpieData;
				lpieData = VectorAt(lpisvIterator->hvEntries, ualJndex);

				if (NULLPTR == lpieData)
				{ continue; }

				if (0 == StringCompare(lpcszSetting, lpieData->lpszKey))
				{ 
					lpszOut = StringDuplicate((LPCSTR)lpieData->lpszValue);
					JUMP(__FOUND);
				}
			}
		}
	}
	__FOUND:
	ReleaseSingleObject(lpipState->hParserLock);

	return lpszOut;
}




_Success_(return != NULLPTR, _Acquires_Shared_Lock_())
PODNET_API
LPSTR
GetIniParseValueFromSectionAndSettingByCopy(
	_In_ 		HANDLE 		hIniFile,
	_In_Z_ 		LPCSTR RESTRICT	lpcszSection,
	_In_Z_		LPCSTR RESTRICT	lpcszSetting,
	_Out_Z_ 	LPSTR RESTRICT	lpszOutBuffer,
	_In_ 		UARCHLONG 	ualBufferSize
){
	EXIT_IF_UNLIKELY_NULL(hIniFile, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpcszSection, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpcszSetting, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpszOutBuffer, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(ualBufferSize, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hIniFile), HANDLE_TYPE_INI_PARSER, NULLPTR);

	LPINI_PARSER lpipState;
	LPINI_SECTION_VECTOR lpisvIterator;
	HVECTOR hvResults;
	BOOL bRet;
	UARCHLONG ualIndex;
	UARCHLONG ualJndex;

	lpipState = OBJECT_CAST(hIniFile, LPINI_PARSER);
	EXIT_IF_UNLIKELY_NULL(lpipState, NULLPTR);
	bRet = FALSE;

	WaitForSingleObject(lpipState->hParserLock, INFINITE_WAIT_TIME);
	hvResults = lpipState->hvResultVector;

	for (
		ualIndex = 0;
		ualIndex < VectorSize(hvResults);
		ualIndex++
	){
		lpisvIterator = VectorAt(hvResults, ualIndex);
		if (NULLPTR == lpisvIterator)
		{ continue; }

		if (0 == StringCompare(lpcszSection, lpisvIterator->lpszSection))
		{
			for (
				ualJndex = 0;
				ualJndex < VectorSize(lpisvIterator->hvEntries);
				ualJndex++
			){
				LPINI_ENTRY lpieData;
				lpieData = VectorAt(lpisvIterator->hvEntries, ualJndex);

				if (NULLPTR == lpieData)
				{ continue; }

				if (0 == StringCompare(lpcszSetting, lpieData->lpszKey))
				{ 
					if (ualBufferSize > StringLength(lpieData->lpszValue))
					{ 
						StringCopySafe(lpszOutBuffer, lpieData->lpszValue, ualBufferSize); 
						bRet = TRUE;
					}
					
					JUMP(__FOUND);
				}
			}
		}
	}
	__FOUND:
	ReleaseSingleObject(lpipState->hParserLock);

	return bRet;
}