/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CINIFILEPARSER_H
#define CINIFILEPARSER_H


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../Ascii.h"
#include "../CHandle/CHandle.h"
#include "../CContainers/CVector.h"
#include "../CContainers/CHashTable.h"
#include "../CString/CString.h"




typedef enum __PARSE_INI_FLAGS
{
	PARSE_INI_USE_DEFAULT		= 0,
	PARSE_INI_USE_VECTORS		= 1,
	PARSE_INI_USE_LINKED_LIST	= 2,
	PARSE_INI_USE_HASHTABLE		= 3
} PARSE_INI_FLAGS;




typedef struct __INI_ENTRY
{
	LPSTR 		lpszKey;
	LPSTR 		lpszValue;
} INI_ENTRY, *LPINI_ENTRY, **DLPINI_ENTRY, ***TLPINI_ENTRY;




typedef struct __INI_SECTION_VECTOR
{
	LPSTR 		lpszSection;
	HVECTOR 	hvEntries;
} INI_SECTION_VECTOR, *LPINI_SECTION_VECTOR, **DLPINI_SECTION_VECTOR;




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNET_API 
HANDLE 
OpenIniFile(
	_In_Z_		LPCSTR		lpcszFile,
	_In_Opt_ 	ULONG 		ulFlags
);




_Success_(return != FALSE, _Acquires_Shared_Lock_())
PODNET_API 
BOOL 
ParseIniFile(
	_In_		HANDLE 		hIniFile,
	_In_Opt_ 	ULONG		ulFlags
);




_Success_(return != NULLPTR, _Acquires_Shared_Lock_())
PODNET_API
HVECTOR 
GetIniParseResultVector(
	_In_ 		HANDLE 		hIniFile
);




_Success_(return != NULLPTR, _Acquires_Shared_Lock_())
PODNET_API 
HLINKEDLIST
GetIniParseResultLinkedList(
	_In_ 		HANDLE 		hIniFile
);




_Success_(return != NULLPTR, _Acquires_Shared_Lock_())
PODNET_API 
HHASHTABLE
GetIniParseResultHashTable(
	_In_ 		HANDLE 		hIniFile
);




/* Needs to be freed */
_Success_(return != NULLPTR, _Acquires_Shared_Lock_())
PODNET_API
LPSTR
GetIniParseValueFromSectionAndSetting(
	_In_ 		HANDLE 		hIniFile,
	_In_Z_ 		LPCSTR RESTRICT	lpcszSection,
	_In_Z_		LPCSTR RESTRICT	lpcszSetting
);




_Success_(return != NULLPTR, _Acquires_Shared_Lock_())
PODNET_API
LPSTR
GetIniParseValueFromSectionAndSettingByCopy(
	_In_ 		HANDLE 		hIniFile,
	_In_Z_ 		LPCSTR RESTRICT	lpcszSection,
	_In_Z_		LPCSTR RESTRICT	lpcszSetting,
	_Out_Z_ 	LPSTR RESTRICT	lpszOutBuffer,
	_In_ 		UARCHLONG 	ualBufferSize
);






#endif