/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CTokenTable.h"


typedef struct __TOKENTABLE
{
	INHERITS_FROM_HANDLE();
	HANDLE 			hLock;
	HVECTOR 		hvTokens;
	HHASHTABLE		hhtTokens;
	LPTOKENFLAG		lptfFlags;
} TOKENTABLE, *LPTOKENTABLE;




_Call_Back_
_Success_(return != FALSE, _Interlocked_Operation_)
INTERNAL_OPERATION
BOOL
__DestroyTokenTable(
	_In_		HDERIVATIVE 	hDerivative,
	_In_Opt_ 	ULONG 		ulFlags
){
	UNREFERENCED_PARAMETER(ulFlags);
	EXIT_IF_UNLIKELY_NULL(hDerivative, FALSE);

	LPTOKENTABLE lptkntData;
	lptkntData = (LPTOKENTABLE)hDerivative;

	WaitForSingleObject(lptkntData->hLock, INFINITE_WAIT_TIME);
	DestroyObject(lptkntData->hLock);
	DestroyHashTable(lptkntData->hhtTokens);
	DestroyVector(lptkntData->hvTokens);
	FreeMemory(lptkntData->lptfFlags);
	FreeMemory(lptkntData);

	return TRUE;
}




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNET_API
HANDLE
CreateTokenTable(
	_In_ 		LPTOKEN		lptknDescriptions,
	_In_ 		UARCHLONG 	ualNumberOfTokens
){
	HANDLE hTokenTable;
	LPTOKENTABLE lptkntData;
	UARCHLONG ualIterator;

	lptkntData = GlobalAllocAndZero(sizeof(TOKENTABLE));
	/* cppcheck-suppress memleak */
	EXIT_IF_UNLIKELY_NULL(lptkntData, NULL_OBJECT);

	if (0 == ualNumberOfTokens)
	{ ualNumberOfTokens = 0x0A; }

	lptkntData->hLock = CreateCriticalSectionAndSpecifySpinCount(0x0C00);
	lptkntData->hvTokens = CreateVector(ualNumberOfTokens, sizeof(TOKEN), 0);
	lptkntData->hhtTokens = CreateHashTable(ualNumberOfTokens * 0x04);
	lptkntData->lptfFlags = GlobalAllocAndZero(sizeof(TOKENFLAG) * ualNumberOfTokens * 4);

	if (NULLPTR != lptknDescriptions)
	{
		for (
			ualIterator = 0;
			ualIterator < ualNumberOfTokens;
			ualIterator++
		){ VectorPushBack(lptkntData->hvTokens, &(lptknDescriptions[ualIterator]), 0); }
	}

	for (
		ualIterator = 0;
		ualIterator < VectorSize(lptkntData->hvTokens);
		ualIterator++
	){
		LPTOKEN lptknIterator;
		UARCHLONG ualKey;

		lptknIterator = (LPTOKEN)VectorAt(lptkntData->hvTokens, ualIterator);
		
		if (NULLPTR == lptknIterator->lxValue)
		{ continue; }

		HashTableInsert(
			lptkntData->hhtTokens,
			(ULONGLONG)(lptknIterator->lxValue),
			StringLength(lptknIterator->lxValue),
			FALSE
		);

		ualKey = HashTableGetKey(
			lptkntData->hhtTokens,
			(ULONGLONG)(lptknIterator->lxValue),
			StringLength(lptknIterator->lxValue),
			FALSE
		);

		lptkntData->lptfFlags[ualKey] = lptknIterator->tfValue;
	}

	hTokenTable = CreateHandleWithSingleInheritor(
		lptkntData,
		&(lptkntData->hThis),
		HANDLE_TYPE_TOKEN_TABLE,
		__DestroyTokenTable,
		0,
		NULLPTR,
		0,
		NULLPTR,
		0,
		&(lptkntData->ullId),
		0
	);

	return hTokenTable;
}




_Success_(return != FALSE, _Interlocked_Operation_)
PODNET_API
BOOL
TokenTableAppendToken(
	_In_ 		HANDLE 		hTokenTable,
	_In_ 		LPTOKEN		lptknDescriptions,
	_In_ 		UARCHLONG 	ualNumberOfTokens
){
	EXIT_IF_UNLIKELY_NULL(hTokenTable, FALSE);
	EXIT_IF_UNLIKELY_NULL(lptknDescriptions, FALSE);
	EXIT_IF_UNLIKELY_NULL(ualNumberOfTokens, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hTokenTable), HANDLE_TYPE_TOKEN_TABLE, FALSE);

	LPTOKENTABLE lptkntData;
	UARCHLONG ualIterator;

	lptkntData = OBJECT_CAST(hTokenTable, LPTOKENTABLE);
	EXIT_IF_UNLIKELY_NULL(lptkntData, FALSE);

	WaitForSingleObject(lptkntData->hLock, INFINITE_WAIT_TIME);
	DestroyHashTable(lptkntData->hhtTokens);
	lptkntData->hhtTokens = CreateHashTable((VectorSize(lptkntData->hvTokens) + ualNumberOfTokens) * 4);	

	for (
		ualIterator = 0;
		ualIterator < ualNumberOfTokens;
		ualIterator++
	){ VectorPushBack(lptkntData->hvTokens, &(lptknDescriptions[ualIterator]), 0); }

	for (
		ualIterator = 0;
		ualIterator < VectorSize(lptkntData->hvTokens);
		ualIterator++
	){
		LPTOKEN lptknIterator;
		ULONGLONG ullKey;
		lptknIterator = (LPTOKEN)VectorAt(lptkntData->hvTokens, ualIterator);

		HashTableInsert(
			lptkntData->hhtTokens,
			(ULONGLONG)(lptknIterator->lxValue),
			StringLength(lptknIterator->lxValue),
			FALSE
		);
		
		ullKey = HashTableGetKey(
			lptkntData->hhtTokens,
			(ULONGLONG)(lptknIterator->lxValue),
			StringLength(lptknIterator->lxValue),
			FALSE
		);

		lptkntData->lptfFlags[ullKey] = lptknIterator->tfValue;
	}

	ReleaseSingleObject(lptkntData->hLock);
	return TRUE;
}




_Success_(return != (UARCHLONG)-1, _Non_Locking_)
PODNET_API
TOKENFLAG
TokenTableGetType(
	_In_ 		HANDLE 		hTokenTable,
	_In_ 		LEXEME 		lxValue
){
	EXIT_IF_UNLIKELY_NULL(hTokenTable, (UARCHLONG)-1);
	EXIT_IF_UNLIKELY_NULL(lxValue, (UARCHLONG)-1);
	
	LPTOKENTABLE lptkntData;
	UARCHLONG ualKey;
	TOKENFLAG tfValue;
	LEXEME lxCompare;

	lptkntData = OBJECT_CAST(hTokenTable, LPTOKENTABLE);
	EXIT_IF_UNLIKELY_NULL(lptkntData, FALSE);

	WaitForSingleObject(lptkntData->hLock, INFINITE_WAIT_TIME);
	
	ualKey = HashTableGetKey(
		lptkntData->hhtTokens,
		(ULONGLONG)lxValue,
		StringLength(lxValue),
		FALSE
	);
	
	lxCompare = (LEXEME)HashTableGetValue(
		lptkntData->hhtTokens,
		ualKey,
		NULLPTR,
		NULLPTR
	);
	
	if (
		NULLPTR  == lxCompare ||
		0 != StringCompare(lxCompare, lxValue)
	){ tfValue = 0; }
	else if ((UARCHLONG)-1 == ualKey)
	{ tfValue = 0; }
	else 
	{ tfValue = lptkntData->lptfFlags[ualKey]; }

	ReleaseSingleObject(lptkntData->hLock);
	return tfValue;
}




_Success_(return != (UARCHLONG)-1, _Non_Locking_)
PODNET_API
UARCHLONG
TokenTableGetSize(
	_In_ 		HANDLE 		hTokenTable
){
	EXIT_IF_UNLIKELY_NULL(hTokenTable, (UARCHLONG)-1);
	
	LPTOKENTABLE lptkntData;
	UARCHLONG ualSize;

	lptkntData = OBJECT_CAST(hTokenTable, LPTOKENTABLE);
	EXIT_IF_UNLIKELY_NULL(lptkntData, FALSE);

	WaitForSingleObject(lptkntData->hLock, INFINITE_WAIT_TIME);
	ualSize = VectorSize(lptkntData->hvTokens);
	ReleaseSingleObject(lptkntData->hLock);

	return ualSize;
}
