/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CLEXER_H
#define CLEXER_H


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../Ascii.h"
#include "../CHandle/CHandle.h"
#include "../CContainers/CVector.h"


/* A MORPHEME Is The Most Basic Element Of A Lexeme */
typedef CHAR		MORPHEME;

/* A LEXEME Might Be A Token */
typedef MORPHEME	*LEXEME;



_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNET_API
HANDLE
CreateLexer(
	_In_Z_		UARCHLONG		ualLexemeBufferSize,
	_In_		DLPSTR			dlpszLexemeSplitters,
	_In_		UARCHLONG		ualNumberOfSplitters
);




_Success_(return != (UARCHLONG)-1, _Interlocked_Operation_)
PODNET_API
UARCHLONG
LexerAnalyzeNextLexeme(
	_In_		HANDLE			hLexer
);




_Success_(return != FALSE, _Interlocked_Operation_)
PODNET_API
BOOL
LexerGetLastLexeme(
	_In_ 		HANDLE 			hLexer,
	_Out_Z_		LEXEME 			lxLexemeBuffer,
	_In_ 		UARCHLONG		ualLexemeBufferSize,
	_Out_Z_		LEXEME			lxSplitterBuffer,
	_In_		UARCHLONG		ualSplitterBufferSize
);




_Success_(return != (UARCHLONG)-1, _Interlocked_Operation_)
PODNET_API
UARCHLONG
LexerAnalyzeAndGetNextLexeme(
	_In_ 		HANDLE			hLexer,
	_Out_Z_ 	LEXEME			lxLexemeBuffer,
	_In_		UARCHLONG		ualLexemeBufferSize,
	_Out_Z_		LEXEME			lxSplitterBuffer,
	_In_		UARCHLONG		ualSplitterBufferSize
);




_Success_(return != FALSE, _Interlocked_Operation_)
PODNET_API
BOOL
LexerSetText(
	_In_		HANDLE 			hLexer,
	_In_Z_ 		LPSTR			lpszText
);



#endif