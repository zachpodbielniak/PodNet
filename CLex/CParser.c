/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CParser.h"


typedef struct __PARSER_CALLBACK
{
	LPFN_PARSER_CALLBACK	lpfnCallback;
	LPVOID 			lpData;
} PARSER_CALLBACK, *LPPARSER_CALLBACK;




typedef struct __PARSER
{
	INHERITS_FROM_HANDLE();
	HANDLE 			hLock;
	HANDLE 			hThread;
	HANDLE 			hLexer;
	HANDLE 			hTokenTable;
	MACHINESTATE		msCurrent;
	MACHINESTATE		msPrevious;
	LPPARSER_CALLBACK	lppcCallbacks;
	UARCHLONG 		ualCallbackSize;
	CSTRING 		csBufferLeft[8192];
	CSTRING 		csBufferRight[8192];
} PARSER, *LPPARSER;




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__DestroyParser(
	_In_ 		HDERIVATIVE		hDerivative,
	_In_Opt_	ULONG 			ulFlags
){
	UNREFERENCED_PARAMETER(ulFlags);
	EXIT_IF_UNLIKELY_NULL(hDerivative, FALSE);

	LPPARSER lpParser;
	lpParser = (LPPARSER)hDerivative;

	WaitForSingleObject(lpParser->hLock, INFINITE_WAIT_TIME);

	DestroyObject(lpParser->hLexer);
	DestroyObject(lpParser->hTokenTable);
	DestroyObject(lpParser->hLock);
	FreeMemory(lpParser);

	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__WaitForParse(
	_In_ 		HANDLE 			hParser,
	_In_Opt_ 	ULONGLONG 		ullMilliSeconds
){
	EXIT_IF_UNLIKELY_NULL(hParser, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hParser), HANDLE_TYPE_PARSER, FALSE);

	LPPARSER lpParser;
	BOOL bRet;

	lpParser = OBJECT_CAST(hParser, LPPARSER);
	EXIT_IF_UNLIKELY_NULL(lpParser, FALSE);

	bRet = WaitForSingleObject(lpParser->hThread, ullMilliSeconds);

	if (TRUE == bRet)
	{ 
		DestroyObject(lpParser->hLock);
		lpParser->hLock = NULL_OBJECT;
	}

	return bRet;
}




_Thread_Proc_
_Success_(return == NULLPTR, _Interlocked_Operation_)
INTERNAL_OPERATION
LPVOID
__ParseProc(
	_In_ 		LPVOID 			lpParam
){
	LPPARSER lpParser;
	TOKENFLAG tfType;
	BOOL bJump;

	lpParser = (LPPARSER)lpParam;
	bJump = FALSE;

	WaitForSingleObject(lpParser->hLock, INFINITE_WAIT_TIME);

	/*
		Get each lexeme. Remove the white space.
		Look it up in the token table.
		Then call the proper call back for that token.
		Each callback may or may not update the state.
	*/
	while ((UARCHLONG)-1 != LexerAnalyzeNextLexeme(lpParser->hLexer))
	{
		__ReLoop:
		bJump = FALSE; 

		if ('\0' != lpParser->csBufferRight[0])
		{
			tfType = TokenTableGetType(
				lpParser->hTokenTable,
				lpParser->csBufferRight
			);

			if (0 != tfType)
			{
				ZeroMemory(lpParser->csBufferLeft, sizeof(lpParser->csBufferLeft));
				CopyMemory(lpParser->csBufferLeft, lpParser->csBufferRight, sizeof(lpParser->csBufferRight));
				ZeroMemory(lpParser->csBufferRight, sizeof(lpParser->csBufferRight));
				bJump = TRUE;
			}
			else
			{
				ZeroMemory(lpParser->csBufferLeft, sizeof(lpParser->csBufferLeft));
				ZeroMemory(lpParser->csBufferRight, sizeof(lpParser->csBufferRight));
				
				LexerGetLastLexeme(
					lpParser->hLexer,
					lpParser->csBufferLeft,
					sizeof(lpParser->csBufferLeft),
					lpParser->csBufferRight,
					sizeof(lpParser->csBufferRight)
				);

				/* If Left Is NULL, Swap */
				if ('\0' == lpParser->csBufferLeft[0] && '\0' != lpParser->csBufferRight[0])
				{ 
					CopyMemory(lpParser->csBufferLeft, lpParser->csBufferRight, sizeof(lpParser->csBufferRight));	
					ZeroMemory(lpParser->csBufferRight, sizeof(lpParser->csBufferRight));
				}

				StringRemoveSpaces(lpParser->csBufferLeft);
			}
			
		}
		else 
		{
			ZeroMemory(lpParser->csBufferLeft, sizeof(lpParser->csBufferLeft));		
			ZeroMemory(lpParser->csBufferRight, sizeof(lpParser->csBufferRight));

			LexerGetLastLexeme(
				lpParser->hLexer,
				lpParser->csBufferLeft,
				sizeof(lpParser->csBufferLeft),
				lpParser->csBufferRight,
				sizeof(lpParser->csBufferRight)
			);

			/* If Left Is NULL, Swap */
			if ('\0' == lpParser->csBufferLeft[0] && '\0' != lpParser->csBufferRight[0])
			{ 
				CopyMemory(lpParser->csBufferLeft, lpParser->csBufferRight, sizeof(lpParser->csBufferRight));	
				ZeroMemory(lpParser->csBufferRight, sizeof(lpParser->csBufferRight));
			}

			StringRemoveSpaces(lpParser->csBufferLeft);
		}

		
		if (ASCII_NULL_TERMINATOR == lpParser->csBufferLeft[0])
		{ tfType = 0; }
		else
		{
			tfType = TokenTableGetType(
				lpParser->hTokenTable,
				lpParser->csBufferLeft
			);
		}
		

		if ((UARCHLONG)-1 == tfType)
		{
			MACHINESTATE msData;
			
			msData = lpParser->lppcCallbacks[0].lpfnCallback(
				lpParser->hThis,
				lpParser->msCurrent,
				lpParser->csBufferLeft,
				lpParser->csBufferRight,
				tfType,
				lpParser->lppcCallbacks[0].lpData
			);

			lpParser->msPrevious = lpParser->msCurrent;
			lpParser->msCurrent = msData;
		}
		else
		{
			MACHINESTATE msData;
			
			if (NULLPTR == lpParser->lppcCallbacks[tfType].lpfnCallback)
			{ continue; }

			msData = lpParser->lppcCallbacks[tfType].lpfnCallback(
				lpParser->hThis,
				lpParser->msCurrent,
				lpParser->csBufferLeft,
				lpParser->csBufferRight,
				tfType,
				lpParser->lppcCallbacks[tfType].lpData
			);
			
			lpParser->msPrevious = lpParser->msCurrent;
			lpParser->msCurrent = msData;
		}

		if (FALSE != bJump)
		{ JUMP(__ReLoop); }
		
	}

	ReleaseSingleObject(lpParser->hLock);
	return NULLPTR;
}




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNET_API
HANDLE
CreateParser(
	_In_ 		HANDLE 			hLexer,
	_In_ 		HANDLE 			hTokenTable
){
	EXIT_IF_UNLIKELY_NULL(hLexer, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NULL(hTokenTable, NULL_OBJECT);

	HANDLE hParser;
	LPPARSER lpParser;

	lpParser = GlobalAllocAndZero(sizeof(PARSER));
	/* cppcheck-suppress memleak */
	EXIT_IF_UNLIKELY_NULL(lpParser, NULL_OBJECT);

	lpParser->hLexer = hLexer;
	lpParser->hTokenTable = hTokenTable;
	lpParser->hThread = NULL_OBJECT;
	lpParser->msCurrent = MACHINE_STATE_NULL;
	lpParser->msPrevious = MACHINE_STATE_NULL;
	lpParser->ualCallbackSize = TokenTableGetSize(hTokenTable);
	lpParser->lppcCallbacks = GlobalAllocAndZero(sizeof(PARSER_CALLBACK) * lpParser->ualCallbackSize);
	if (NULLPTR == lpParser->lppcCallbacks)
	{
		FreeMemory(lpParser);
		return NULL_OBJECT;
	}

	hParser = CreateHandleWithSingleInheritor(
		lpParser,
		&(lpParser->hThis),
		HANDLE_TYPE_PARSER,
		__DestroyParser,
		0,
		NULLPTR,
		0,
		NULLPTR,
		0,
		&(lpParser->ullId),
		0
	);

	SetHandleEventWaitFunction(hParser, __WaitForParse, 0);
	return hParser;
}




_Success_(return != FALSE, _Interlocked_Operation_)
PODNET_API
BOOL
ParserRegisterCallback(
	_In_ 		HANDLE 			hParser,
	_In_ 		TOKENFLAG		tfType,
	_In_ 		LPFN_PARSER_CALLBACK	lpfnCallback,
	_In_Opt_ 	LPVOID 			lpData
){
	EXIT_IF_UNLIKELY_NULL(hParser, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hParser), HANDLE_TYPE_PARSER, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpfnCallback, FALSE);

	LPPARSER lpParser;
	lpParser = OBJECT_CAST(hParser, LPPARSER);
	EXIT_IF_UNLIKELY_NULL(lpParser, FALSE);

	WaitForSingleObject(lpParser->hLock, INFINITE_WAIT_TIME);

	lpParser->lppcCallbacks[tfType].lpData = lpData;
	lpParser->lppcCallbacks[tfType].lpfnCallback = lpfnCallback;

	ReleaseSingleObject(lpParser->hLock);
	return TRUE;
}




_Success_(return != FALSE, _Interlocked_Operation_)
PODNET_API
BOOL
ParserSetText(
	_In_ 		HANDLE 			hParser,
	_In_ 		LPCSTR 			lpcszText
){
	EXIT_IF_UNLIKELY_NULL(hParser, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hParser), HANDLE_TYPE_PARSER, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpcszText, FALSE);

	LPPARSER lpParser;
	lpParser = OBJECT_CAST(hParser, LPPARSER);
	EXIT_IF_UNLIKELY_NULL(lpParser, FALSE);

	WaitForSingleObject(lpParser->hLock, INFINITE_WAIT_TIME);
	
	LexerSetText(
		lpParser->hLexer,
		(LPSTR)lpcszText
	);
	
	ReleaseSingleObject(lpParser->hLock);
	return TRUE;
}




_Success_(return != FALSE, _Interlocked_Operand_)
PODNET_API
BOOL
Parse(
	_In_ 		HANDLE 			hParser
){
	EXIT_IF_UNLIKELY_NULL(hParser, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hParser), HANDLE_TYPE_PARSER, FALSE);

	LPPARSER lpParser;
	lpParser = OBJECT_CAST(hParser, LPPARSER);
	EXIT_IF_UNLIKELY_NULL(lpParser, FALSE);

	WaitForSingleObject(lpParser->hLock, INFINITE_WAIT_TIME);
	
	lpParser->hThread = CreateThread(
		__ParseProc,
		lpParser,
		0
	);
	
	ReleaseSingleObject(lpParser->hLock);
	return TRUE;
}