/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CLexer.h"


typedef struct __LEXER 
{
	INHERITS_FROM_HANDLE();
	HANDLE			hLock;
	LPSTR			lpszText;
	HVECTOR			hvSplitters;
	LEXEME			lxLexeme;
	LEXEME			lxBuffer;
	LEXEME 			lxLexemeSplitter;
	UARCHLONG		ualLexemeBufferSize;
	UARCHLONG		ualLexemeIterator;
	LPSTR			lpszBeginning;
	LPSTR			lpszIterator;
	UARCHLONG		ualLastLexemeSize;
	BOOL			bDone;
} LEXER, *LPLEXER, **DLPLEXER;




_Success_(return != FALSE, _Interlocked_Operation_)
INTERNAL_OPERATION
BOOL
__DestroyLexer(
	_In_ 		HDERIVATIVE		hDerivative,
	_In_Opt_ 	ULONG			ulFlags
){
	UNREFERENCED_PARAMETER(ulFlags);
	EXIT_IF_UNLIKELY_NULL(hDerivative, FALSE);

	LPLEXER lpLex = (LPLEXER)hDerivative;
	UARCHLONG ualIndex;
	WaitForSingleObject(lpLex->hLock, INFINITE_WAIT_TIME);

	for (
		ualIndex = 0;
		ualIndex < VectorSize(lpLex->hvSplitters);
		ualIndex++
	){ FreeMemory(*(DLPSTR)VectorAt(lpLex->hvSplitters, ualIndex)); }

	DestroyVector(lpLex->hvSplitters);
	DestroyObject(lpLex->hLock);
	FreeMemory(lpLex->lxLexeme);
	FreeMemory(lpLex->lxBuffer);
	FreeMemory(lpLex->lxLexemeSplitter);

	if (NULLPTR != lpLex->lpszText)
	{ FreeMemory(lpLex->lpszText); }

	FreeMemory(lpLex);

	return TRUE;
}




_Success_(return != 0xFF, _Non_Locking_)
INTERNAL_OPERATION
CHAR
__GetNextMorpheme(
	_In_ 		DLPSTR			dlpszIterator
){
	MORPHEME mRet;

	mRet = **dlpszIterator;
	*dlpszIterator = (LPSTR)(LPVOID)((UARCHLONG)(*dlpszIterator) + 0x01U);

	return mRet;
}




/* 
	This function will compare the current lexeme buffer 
	against all of the lexeme splitters. If one of the
	lexeme splitters is inside of the lexeme buffer, 
	we are done getting new morphemes, and we have
	the next lexeme.
	lpLex->lxLexeme is the next lexeme.
	lpLex->lxLexemeSplitter is the splitter.
*/
_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__ValidateLexemeBuffer(
	_In_ 		LPLEXER			lpLex
){
	UARCHLONG ualIndex;

	if (ASCII_NULL_TERMINATOR == *(lpLex->lpszIterator))
	{
		/* Copy Out Our Lexeme */
		UARCHLONG ualCopySize;
		
		ualCopySize = ((UARCHLONG)(lpLex->lpszIterator) - (UARCHLONG)(lpLex->lpszBeginning));
		ZeroMemory(lpLex->lxLexeme, lpLex->ualLexemeBufferSize);
		
		CopyMemory(
			lpLex->lxLexeme,
			lpLex->lpszBeginning,
			ualCopySize
		);

		/* Set LexemeSplitter To NULL */
		ZeroMemory(lpLex->lxLexemeSplitter, lpLex->ualLexemeBufferSize);

		lpLex->bDone = TRUE;
		return FALSE; 
	}

	for (
		ualIndex = 0;
		ualIndex < VectorSize(lpLex->hvSplitters);
		ualIndex++
	){
		LPSTR lpszData;
		lpszData = *(DLPSTR)VectorAt(lpLex->hvSplitters, ualIndex);

		if (0 != StringInString(lpLex->lxBuffer, lpszData))
		{ 
			/* Copy Out Our Lexeme */
			UARCHLONG ualCopySize;
			ualCopySize = ((UARCHLONG)(lpLex->lpszIterator) - (UARCHLONG)(lpLex->lpszBeginning) - 0x01U) - (StringLength(lpszData) - 1);

			ZeroMemory(lpLex->lxLexeme, lpLex->ualLexemeBufferSize);
			CopyMemory(
				lpLex->lxLexeme,
				lpLex->lpszBeginning,
				ualCopySize
			);


			/* Copy The LexemeSplitter Out */
			StringCopySafe(
				lpLex->lxLexemeSplitter,
				lpszData,
				lpLex->ualLexemeBufferSize - 1
			);

			return FALSE; 
		}
	}
	
	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__GetNextLexeme(
	_In_		LPLEXER			lpLex
){
	ZeroMemory(lpLex->lxBuffer, lpLex->ualLexemeBufferSize);
	ZeroMemory(lpLex->lxLexeme, lpLex->ualLexemeBufferSize);
	lpLex->ualLastLexemeSize = 0;
	lpLex->ualLexemeIterator = 0;

	while(
		FALSE != __ValidateLexemeBuffer(lpLex) && 
		lpLex->ualLexemeBufferSize > lpLex->ualLexemeIterator
	){
		CHAR cNext;
		
		cNext = __GetNextMorpheme(&(lpLex->lpszIterator));
		lpLex->lxBuffer[lpLex->ualLexemeIterator++] = cNext;
	}

	lpLex->ualLastLexemeSize = StringLength(lpLex->lxLexeme);
	lpLex->lpszBeginning = lpLex->lpszIterator;
	
	return TRUE;
}




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNET_API
HANDLE
CreateLexer(
	_In_Z_		UARCHLONG		ualLexemeBufferSize,
	_In_		DLPSTR			dlxLexemeSplitters,
	_In_		UARCHLONG		ualNumberOfSplitters
){
	EXIT_IF_UNLIKELY_NULL(ualLexemeBufferSize, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NULL(dlxLexemeSplitters, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NULL(ualNumberOfSplitters, NULL_OBJECT);

	HANDLE hLex;
	LPLEXER lpLex;
	UARCHLONG ualIndex;

	lpLex = GlobalAllocAndZero(sizeof(LEXER));
	/* cppcheck-suppress memleak */
	EXIT_IF_UNLIKELY_NULL(lpLex, NULL_OBJECT);

	lpLex->ualLexemeBufferSize = ualLexemeBufferSize;
	lpLex->lxBuffer = GlobalAllocAndZero(ualLexemeBufferSize);
	if (NULLPTR == lpLex->lxBuffer)
	{
		FreeMemory(lpLex);
		return NULL_OBJECT;
	}

	lpLex->lxLexeme = GlobalAllocAndZero(ualLexemeBufferSize);
	if (NULLPTR == lpLex->lxLexeme)
	{
		FreeMemory(lpLex->lxBuffer);
		FreeMemory(lpLex);
		return NULL_OBJECT;
	}

	lpLex->lxLexemeSplitter = GlobalAllocAndZero(ualLexemeBufferSize);
	if (NULLPTR == lpLex->lxLexemeSplitter)
	{
		FreeMemory(lpLex->lxLexeme);
		FreeMemory(lpLex->lxBuffer);
		FreeMemory(lpLex);
		return NULL_OBJECT;
	}

	lpLex->hLock = CreateCriticalSectionAndSpecifySpinCount(0x0C00);
	if (NULL_OBJECT == lpLex->hLock)
	{
		FreeMemory(lpLex->lxLexemeSplitter);
		FreeMemory(lpLex->lxLexeme);
		FreeMemory(lpLex->lxBuffer);
		FreeMemory(lpLex);
		return NULL_OBJECT;
	}

	lpLex->hvSplitters = CreateVector(ualNumberOfSplitters, sizeof(LPSTR), 0);
	if (NULLPTR == lpLex->hvSplitters)
	{
		DestroyObject(lpLex->hLock);
		FreeMemory(lpLex->lxLexemeSplitter);
		FreeMemory(lpLex->lxLexeme);
		FreeMemory(lpLex->lxBuffer);
		FreeMemory(lpLex);
		return NULL_OBJECT;
	}

	for (
		ualIndex = 0;
		ualIndex < ualNumberOfSplitters;
		ualIndex++
	){
		UARCHLONG ualLength;
		LPSTR lpszIterator, lpszData;
		
		lpszIterator = dlxLexemeSplitters[ualIndex];
		ualLength = StringLength(lpszIterator);
		lpszData = LocalAllocAndZero(ualLength + 0x01U);

		StringCopySafe(lpszData, lpszIterator, ualLength);
		VectorPushBack(lpLex->hvSplitters, &lpszData, 0);
	}

	hLex = CreateHandleWithSingleInheritor(
		lpLex,
		&(lpLex->hThis),
		HANDLE_TYPE_LEXER,
		__DestroyLexer,
		0,
		NULLPTR,
		0,
		NULLPTR,
		0,
		&(lpLex->ullId),
		0
	);
	
	if (NULL_OBJECT == hLex)
	{ __DestroyLexer(lpLex, 0); }

	return hLex;
}




_Success_(return != (UARCHLONG)-1, _Interlocked_Operation_)
PODNET_API
UARCHLONG
LexerAnalyzeNextLexeme(
	_In_		HANDLE			hLexer
){
	EXIT_IF_UNLIKELY_NULL(hLexer, (UARCHLONG)-1);
	EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hLexer), HANDLE_TYPE_LEXER, (UARCHLONG)-1);

	LPLEXER lpLex;
	UARCHLONG ualSize;
	lpLex = OBJECT_CAST(hLexer, LPLEXER);
	EXIT_IF_UNLIKELY_NULL(lpLex, (UARCHLONG)-1);

	WaitForSingleObject(lpLex->hLock, INFINITE_WAIT_TIME);

	if (FALSE != lpLex->bDone)
	{
		ReleaseSingleObject(lpLex->hLock);
		return (UARCHLONG)-1;
	}

	__GetNextLexeme(lpLex);
	ualSize = lpLex->ualLastLexemeSize;

	ReleaseSingleObject(lpLex->hLock);
	return ualSize;	
}




_Success_(return != FALSE, _Interlocked_Operation_)
PODNET_API
BOOL
LexerGetLastLexeme(
	_In_ 		HANDLE 			hLexer,
	_Out_Z_		LEXEME 			lxLexemeBuffer,
	_In_ 		UARCHLONG		ualLexemeBufferSize,
	_Out_Z_		LEXEME			lxSplitterBuffer,
	_In_		UARCHLONG		ualSplitterBufferSize
){
	EXIT_IF_UNLIKELY_NULL(hLexer, FALSE);
	EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hLexer), HANDLE_TYPE_LEXER, FALSE);
	EXIT_IF_UNLIKELY_NULL(lxLexemeBuffer, FALSE);
	EXIT_IF_UNLIKELY_NULL(ualLexemeBufferSize, FALSE);
	EXIT_IF_UNLIKELY_NULL(lxSplitterBuffer, FALSE);
	EXIT_IF_UNLIKELY_NULL(ualSplitterBufferSize, FALSE);

	LPLEXER lpLex;
	lpLex = OBJECT_CAST(hLexer, LPLEXER);
	EXIT_IF_UNLIKELY_NULL(lpLex, FALSE);

	WaitForSingleObject(lpLex->hLock, INFINITE_WAIT_TIME);

	StringCopySafe(
		lxLexemeBuffer,
		lpLex->lxLexeme,
		ualLexemeBufferSize
	);

	StringCopySafe(
		lxSplitterBuffer,
		lpLex->lxLexemeSplitter,
		ualSplitterBufferSize
	);

	ReleaseSingleObject(lpLex->hLock);
	return TRUE;
}




_Success_(return != (UARCHLONG)-1, _Interlocked_Operation_)
PODNET_API
UARCHLONG
LexerAnalyzeAndGetNextLexeme(
	_In_ 		HANDLE			hLexer,
	_Out_Z_		LEXEME 			lxLexemeBuffer,
	_In_ 		UARCHLONG		ualLexemeBufferSize,
	_Out_Z_		LEXEME			lxSplitterBuffer,
	_In_		UARCHLONG		ualSplitterBufferSize
){
	EXIT_IF_UNLIKELY_NULL(hLexer, (UARCHLONG)-1);
	EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hLexer), HANDLE_TYPE_LEXER, (UARCHLONG)-1);
	EXIT_IF_UNLIKELY_NULL(lxLexemeBuffer, (UARCHLONG)-1);
	EXIT_IF_UNLIKELY_NULL(ualLexemeBufferSize, (UARCHLONG)-1);
	EXIT_IF_UNLIKELY_NULL(lxSplitterBuffer, (UARCHLONG)-1);
	EXIT_IF_UNLIKELY_NULL(ualSplitterBufferSize, (UARCHLONG)-1);

	LPLEXER lpLex;
	UARCHLONG ualSize;

	lpLex = OBJECT_CAST(hLexer, LPLEXER);
	EXIT_IF_UNLIKELY_NULL(lpLex, (UARCHLONG)-1);
	
	WaitForSingleObject(lpLex->hLock, INFINITE_WAIT_TIME);

	if (FALSE != lpLex->bDone)
	{
		ReleaseSingleObject(lpLex->hLock);
		return (UARCHLONG)-1;
	}

	__GetNextLexeme(lpLex);

	StringCopySafe(
		lxLexemeBuffer,
		lpLex->lxLexeme,
		ualLexemeBufferSize
	);

	StringCopySafe(
		lxSplitterBuffer,
		lpLex->lxLexemeSplitter,
		ualSplitterBufferSize
	);

	ualSize = lpLex->ualLastLexemeSize;

	ReleaseSingleObject(lpLex->hLock);
	return ualSize;
}




_Success_(return != FALSE, _Interlocked_Operation_)
PODNET_API
BOOL
LexerSetText(
	_In_		HANDLE 			hLexer,
	_In_Z_ 		LPSTR			lpszText
){
	EXIT_IF_UNLIKELY_NULL(hLexer, FALSE);
	EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hLexer), HANDLE_TYPE_LEXER, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpszText, FALSE);

	LPLEXER lpLex;
	UARCHLONG ualLength;

	lpLex = OBJECT_CAST(hLexer, LPLEXER);
	EXIT_IF_UNLIKELY_NULL(lpLex, FALSE);

	WaitForSingleObject(lpLex->hLock, INFINITE_WAIT_TIME);

	ualLength = StringLength(lpszText);
	lpLex->lpszText = GlobalAllocAndZero(ualLength + 0x01U);

	if (NULLPTR == lpLex->lpszText)
	{
		ReleaseSingleObject(lpLex->hLock);
		return FALSE;
	}

	StringCopySafe(
		lpLex->lpszText,
		lpszText,
		ualLength
	);

	lpLex->lpszBeginning = lpszText;
	lpLex->lpszIterator = lpszText;
	lpLex->bDone = FALSE;

	ReleaseSingleObject(lpLex->hLock);
	return TRUE;
}