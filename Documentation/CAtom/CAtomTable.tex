\documentclass{article}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{underscore}
\usepackage{systeme}
\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{url}
\usepackage{graphicx}
\usepackage[T1]{fontenc}
\usepackage{titlesec}
\usepackage{titling}
\usepackage[margin=1in]{geometry}
\usepackage{listings}
\usepackage{color}


\newcommand{\imagi}{\mathnormal{i}}
\newcommand{\imagj}{\mathnormal{j}}
\newcommand{\euler}{\mathnormal{e}}

\graphicspath{ {Images/} }

\lstset{language=C,
captionpos=b,
tabsize=3,
frame=lines,
keywordstyle=\color{blue},
commentstyle=\color{darkgreen},
stringstyle=\color{red},
numbers=left,
numberstyle=\tiny,
numbersep=5pt,
breaklines=true,
showstringspaces=false,
basicstyle=\footnotesize,
emph={label}}



\begin{document}

\title{PodNet $\Rightarrow$ CAtom $\rightarrow$ CAtomTable}
\author{Zach Podbielniak}

\maketitle

\section{What Is An ATOM TABLE?}

An ATOM TABLE is essentially a Dictionary<USHORT, LPSTR>. Granted that is not valid C syntax, but it is a great representation of what it is. 

\vspace{2mm}
\noindent
An ATOM TABLE will take in an LPSTR (String), and on success will give back what is called an ATOM, which is essentially the index into the ATOM TABLE. This ATOM can then be used later on to retrieve the value of the LPSTR (String).

\subsection{Types of ATOM TABLEs}

There exist two types of ATOM TABLEs. There is the Global ATOM TABLE, and there are just other "User-Space" defined ATOM TABLEs. There is exactly \textbf{ONE} Global ATOM TABLE per instance of the PodNet Library.

\subsubsection{Global ATOM TABLE}

The Global ATOM TABLE is used for, you guessed it, Global storing of LPSTRs (Strings). Feel free to push anything you want to this ATOM TABLE, as it is a general purpose ATOM TABLE.

\vspace{2mm}
\noindent
While the Global ATOM TABLE is set to be a general-purpose ATOM TABLE, in efforts to keep low RAM usage, the max length LPSTR that can be stored in it is 255 characters long (plus NULL-Terminator, 256 all together). And it can hold \textbf{EXACTLY} 2048 LPSTRs. If for whatever reason you need more, feel free to edit these values in CAtom/CAtomTable.c.

\vspace{2mm}
\noindent
The Global ATOM TABLE is not magically created when the process is opened. This is due to the fact, that you may not even be using an ATOM TABLE in anything you link with the PodNet Library. So, the first call to \textit{PushGlobalAtom()} will create the table. This can take a little bit of time on slower hardware, so if you would like to create it at the beginning simply just call \textit{PushGlobalAtom()} as early as possible; you can simply push some garbage value for the first LPSTR.


\subsubsection{User Defined ATOM TABLEs}

You can also create your own defined ATOM TABLEs. This is very useful for things such as ERROR CODEs, as the ERROR CODE could be the ATOM, and then if you want to look up the ERROR VALUE, just use the ERROR CODE. 

\vspace{2mm}
\noindent
When you create your own ATOM TABLE, you must specify what the max size of the LPSTR can be, and the max number of ATOMs able to be stored in the table. The \textbf{ABSOLUTE} maximum is $2^{16}-1$ $\rightarrow$ USHORT_MAX, because the table is indexed on a USHORT. Really, if you need more than that, wrap some sort of class around it.

\vspace{2mm}
\noindent
On successful creation of a new ATOM TABLE, you will in return recieve a HANDLE. This HANDLE represents the ATOM TABLE Object. This HANDLE must be used to correctly add new ATOMs to the TABLE, as well as to retrieve them. When you are done with the ATOM TABLE, simply call \textit{DestroyObject()}.


\section{ATOM TABLE Functions}

Now that you know a bit about ATOM TABLEs, lets go over the relevant functions


\subsection{CreateAtomTable}

\begin{figure*}[h]
\includegraphics[scale=0.6]{CreateAtomTable.png}
\centering
\end{figure*}

\noindent
\textit{CreateAtomTable()} takes in two parameters, \textit{usAtomBufferSize} and \textit{usMaxAtoms}, and spits out a HANDLE.

\vspace{2mm}
\noindent
\textbf{usAtomBufferSize}:

This is a USHORT value that sets the max buffer size for any LPSTR (String) that is pushed to the ATOM TABLE. If you set this to 256, the max amount of characters any LPSTR can be is 255 + 1 NULL-Terminator. The max value is $2^{16}-1$ $\rightarrow$ USHORT_MAX.

\vspace{2mm}
\noindent
\textbf{usMaxAtoms}:

This is a USHORT value that sets the max number of LPSTRs (Strings) that can be stored in the ATOM TABLE. If you set to this 32, you can only store 32 LPSTRs. The max value is $2^{16}-1$ $\rightarrow$ USHORT_MAX.

\vspace{2mm}
\noindent
\textbf{Return $\rightarrow$ HANDLE}:

On success, the return value is a valid HANDLE (\textbf{NOT} \textit{NULLPTR}). This HANDLE is then used for subsequent functions related to the ATOM TABLE.

\break

\subsection{PushAtom}

\begin{figure*}[h]
\includegraphics[scale=0.6]{PushAtom.png}
\centering
\end{figure*}

\noindent
\textit{PushAtom()} takes 2 parameters. The first being the HANDLE to the ATOM TABLE in question, that you would like to Push to, and the second parameter being the LPSTR you are going to store. In return, you will recieve an ATOM.

\vspace{2mm}
\noindent 
Please note this function will wait for up to an \textbf{INFINITE} amount of time, then lock the corresponding ATOM TABLE. On function exit, the lock is released. No additional locking is required, this is all handled internally.

\vspace{2mm}
\noindent
\textbf{hAtomTable}:

This is a HANDLE to a valid ATOM TABLE. If you are not sure if the HANDLE in question is an ATOM TABLE HANDLE, call \textit{GetHandleDerivativeType()}, if it returns \textit{HANDLE_TYPE_ATOM_TABLE}, you're golden.

\vspace{2mm}
\noindent
\textbf{lpszString}:

This is a CONSTANT NULL-Terminated string that you are wishing to store inside the ATOM TABLE. If this is larger than the value of the ATOM TABLEs max ATOM size, this simply \textbf{WILL NOT} be stored.

\vspace{2mm}
\noindent
\textbf{Return $\rightarrow$ ATOM}:

On success you will be given a valid ATOM (\textbf{NOT} \textit{INVALID_ATOM}), and this is used to retrieve the LPSTR you just pushed at a later time. On every call to \textit{PushAtom()} the ATOM returned is an increment of the previous ATOM returned. Knowing this now, can be a powerful thing.

\break

\subsection{GetAtomValue}

\begin{figure*}[h]
\includegraphics[scale=0.6]{GetAtomValue.png}
\centering
\end{figure*}

\noindent
This function takes in 3 parameters. A HANDLE to a valid ATOM TABLE, an ATOM, and a buffer that is of appropriate size to be written to on output. The return type is of type BOOL. 

\vspace{2mm}
\noindent 
Please note this function will wait for up to an \textbf{INFINITE} amount of time, then lock the corresponding ATOM TABLE. On function exit, the lock is released. No additional locking is required, this is all handled internally.

\vspace{2mm}
\noindent
\textbf{hAtomTable}:

This is a HANDLE to a valid ATOM TABLE. If you are not sure if the HANDLE in question is an ATOM TABLE HANDLE, call \textit{GetHandleDerivativeType()}, if it returns \textit{HANDLE_TYPE_ATOM_TABLE}, you're golden.

\vspace{2mm}
\noindent
\textbf{atAtom}:

This is a valid ATOM that is representing the index of the LPSTR you want to get out.

\vspace{2mm}
\noindent
\textbf{lpszOut}:

This is an address of a pre-allocated buffer of the proper size (\textit{usAtomBufferSize $\times$ sizeof(CHAR)}). For the Global ATOM TABLE, simply use a 256 byte sized buffer. This is written to, so any data stored here \textbf{WILL BE PURGED}. With this copy-out philosophy, this truly means you the ATOM values in the ATOM TABLE can never be changed once pushed. It is recommended to zero this memory beforehand. 

\vspace{2mm}
\noindent
\textbf{Return $\rightarrow$ BOOL}:

On success, the value return will be a NON-ZERO number (\textit{!FALSE}). If an error occured, \textit{lpszOut} will be empty, and the value returned will be \textit{FALSE}. 


\break

\subsection{PushGlobalAtom}

\begin{figure*}[h]
\includegraphics[scale=0.6]{PushGlobalAtom.png}
\centering
\end{figure*}

\noindent
This function takes in one parameter, and that is simply a CONSTANT String that you wish to store in the ATOM TABLE. The return type is of type BOOL.

\vspace{2mm}
\noindent 
Please note this function will wait for up to an \textbf{INFINITE} amount of time, then lock the corresponding ATOM TABLE. On function exit, the lock is released. No additional locking is required, this is all handled internally.

\vspace{2mm}
\noindent
\textbf{lpszString}:

This is a NULL-Terminated String that you wish to store in the Global ATOM TABLE. This must be 255 characters or shorter. Accounting for the NULL-Terminator, the max length is 256 characters.

\vspace{2mm}
\noindent
\textbf{Return $\rightarrow$ ATOM}:

On success, you will recieve a valid ATOM (\textbf{NOT} \textit{INVALID_ATOM}).


\break

\subsection{GetGlobalAtomValue}

\begin{figure*}[h]
\includegraphics[scale=0.6]{GetGlobalAtomValue.png}
\centering
\end{figure*}

\noindent
This function takes in two paramters. The first being the ATOM of the value you wish to retrieve, and a buffer out that will be used to store the corresponding ATOM value. This buffer must be atleast 256 bytes wide, and preferably set to 0. The return is of type BOOL

\vspace{2mm}
\noindent 
Please note this function will wait for up to an \textbf{INFINITE} amount of time, then lock the corresponding ATOM TABLE. On function exit, the lock is released. No additional locking is required, this is all handled internally.

\vspace{2mm}
\noindent
\textbf{atAtom}:

This is a valid ATOM that is representing the index of the LPSTR you want to get out.

\vspace{2mm}
\noindent
\textbf{lpszOut}:

This is an address of a pre-allocated buffer of the proper size (\textit{usAtomBufferSize $\times$ sizeof(CHAR)} $\rightarrow$ 256 bytes for Global ATOM TABLE). This is written to, so any data stored here \textbf{WILL BE PURGED}. With this copy-out philosophy, this truly means you the ATOM values in the ATOM TABLE can never be changed once pushed. It is recommended to zero this memory beforehand. 





\end{document}
