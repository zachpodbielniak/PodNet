\documentclass{article}


\usepackage{underscore}
\usepackage{mathtools}
\usepackage{graphicx}
%\usepackage[T1]{fontenc}
\usepackage{url}

\graphicspath{ {Images/} }

\begin{document}

\title{PodNet $\Rightarrow$  CMath $\rightarrow$ CFastApprox}
\author{Zach Podbielniak}

\maketitle

This is an overview of all the fast approximations used in the PodNet library.

\section{Trigonometric Functions}

All values of \large{\textbf{x}} are in radians. A graph is included comparing the normal function to the fast approximation equivalent. The normal is in \textbf{blue}, and the fast approximation is in \textbf{orange}.

\vspace{4mm}
\noindent
For each equation, the function prototype will be shown. It should also be noted that these functions do no form of range checking. If you pass in a value that is outside the range, you are going to get garbage back as the answer, as these fast approximations only work for a given range. The range is denoted by the following Annotation Macro:

\vspace{2mm}
\textbf{\url{_In_Range_(LOWER_BOUND, UPPER_BOUND)}}

\vspace{4mm}
\noindent 
For the following trig functions, when utilizing C or C++, preprocessor macros are defined to do the operation. As such, this reduces the calling overhead, and does not require a jump with register preservation, it is simply performed inline, and the compiler figures it out. The speedup from using the macro over the normal function is approximately 2 times. The function just "calls" the macro anyways.

\vspace{4mm}
\noindent 
Avoid calling the C functions when possible. The C functions exist as a way to export these functions to other languages that can't take advantage of the preprocessor macros, such as Assembly and Python. The macros are defined in the same way the function is, and take the exact same parameter, and have the same expected input and output range. Here are the macro definitions:

\break

\begin{figure*}[h]
\includegraphics[scale=0.75]{FastApproxTrigDefs.png}
\centering
\end{figure*}

The above macros require a \textbf{FLOAT} to be passed in, and they will "return" a \textbf{FLOAT}. If you require macros that perform the operation but take in a \textbf{DOUBLE} and return a \textbf{DOUBLE} use the following:


\begin{figure*}[h]
\includegraphics[scale=0.75]{FastApproxTrigDefsDouble.png}
\centering
\end{figure*}

\break


\subsection{Normal}

\subsubsection{Sin}

\begin{equation*}
\sin(x) \approx x - \frac{11x^3}{75}
\end{equation*}



\begin{figure*}[h]
\includegraphics[scale=0.75]{SinVsFastSin.png}
\centering
\end{figure*}

\begin{figure*}[h]
\includegraphics[scale=0.75]{FastApproxSinFunction.png}
\centering
\end{figure*}

\break

\subsubsection{Cos}


\begin{equation*}
\cos(x) \approx 1 - \frac{x^2}{2} + \frac{11x^4}{300}
\end{equation*}

\begin{figure*}[h]
\includegraphics[scale=0.75]{CosVsFastCos.png}
\centering
\end{figure*}

\begin{figure*}[h]
\includegraphics[scale=0.75]{FastApproxCosFunction.png}
\centering
\end{figure*}


\break 

\subsubsection{Tan}

From sin and cos approximations we can state that:


 
\begin{equation*}
\tan(x) \approx \frac{300x - 44x^3}{300-150x^2 + 11x^4}
\end{equation*}

\begin{figure*}[h]
\includegraphics[scale=0.75]{TanVsFastTan.png}
\centering
\end{figure*}

\begin{figure*}[h]
\includegraphics[scale=0.75]{FastApproxTanFunction.png}
\centering
\end{figure*}

\break


\subsubsection{Sec}


\begin{equation*}
\sec(x) \approx \frac{1}{x - \frac{11x^3}{75}}
\end{equation*}

\begin{figure*}[h]
\includegraphics[scale=0.75]{SecVsFastSec.png}
\centering
\end{figure*}

\begin{figure*}[h]
\includegraphics[scale=0.5]{FastApproxSecFunction.png}
\centering
\end{figure*}


\break 

\subsubsection{Csc}


\begin{equation*}
\csc(x) \approx \frac{1}{1 - \frac{x^2}{2} + \frac{11x^4}{300}}
\end{equation*}

\begin{figure*}[h]
\includegraphics[scale=0.75]{CscVsFastCsc.png}
\centering
\end{figure*}

\begin{figure*}[h]
\includegraphics[scale=0.5]{FastApproxCscFunction.png}
\centering
\end{figure*}

\break 

\subsubsection{Cot}


\begin{equation*}
\cot(x) \approx \frac{300 - 150x^2 + 11x^4}{300x - 44x^3}
\end{equation*}

\begin{figure*}[h]
\includegraphics[scale=0.75]{CotVsFastCot.png}
\centering
\end{figure*}

\begin{figure*}[h]
\includegraphics[scale=0.5]{FastApproxCotFunction.png}
\centering
\end{figure*}

\break

\subsection{Inverse}

\subsubsection{ArcSin}

\begin{equation*}
\arcsin(x) \approx x + \frac{11x^3}{75}
\end{equation*}

\begin{figure*}[h]
\includegraphics[scale=0.75]{ArcSinVsFastArcSin.png}
\centering
\end{figure*}

\begin{figure*}[h]
\includegraphics[scale=0.5]{FastApproxArcSinFunction.png}
\centering
\end{figure*}


\break 

\subsubsection{ArcCos}

\begin{equation*}
\arccos(x) \approx \frac{\pi}{2} - x - \frac{11x^3}{75}
\end{equation*}

\begin{figure*}[h]
\includegraphics[scale=0.75]{ArcCosVsFastArcCos.png}
\centering
\end{figure*}

\begin{figure*}[h]
\includegraphics[scale=0.5]{FastApproxArcCosFunction.png}
\centering
\end{figure*}



\break

\subsubsection{ArcTan}

\begin{equation*}
\arctan(x) \approx x - \frac{x^3}{3} + \frac{x^5}{5}
\end{equation*}

\begin{figure*}[h]
\includegraphics[scale=0.75]{ArcTanVsFastArcTan.png}
\centering
\end{figure*}

\begin{figure*}[h]
\includegraphics[scale=0.5]{FastApproxArcTanFunction.png}
\centering
\end{figure*}







\end{document}
