\documentclass{article}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{underscore}
\usepackage{systeme}
\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{url}
\usepackage{graphicx}
%\usepackage[T1]{fontenc}
\usepackage{titlesec}
\usepackage{titling}
\usepackage[margin=1in]{geometry}
\usepackage{listings}
\usepackage{color}
\usepackage{courier}
\usepackage{standalone}

\newcommand{\imagi}{\mathnormal{i}}
\newcommand{\imagj}{\mathnormal{j}}
\newcommand{\euler}{\mathnormal{e}}

\graphicspath{ {Images/} }

\lstset{language=C,
captionpos=b,
tabsize=4,
frame=lines,
keywordstyle=\color{blue},
commentstyle=\color{darkgreen},
stringstyle=\color{red},
numbers=left,
numberstyle=\normalsize,
numbersep=5pt,
breaklines=true,
showstringspaces=false,
basicstyle=\large\ttfamily,
emph={label}}

\begin{document}

\title{PodNet $\Rightarrow$ CLock $\rightarrow$ CCriticalSection}
\author{Zach Podbielniak}

\maketitle

\section{Critical Section}

A CRITICAL SECTION is a hybrid-style advisory lock. It takes parts from a SPIN LOCK and parts from a SEMAPHORE and creates a different style of lock out if. 

\subsection{How It Works}

How a CRITICAL SECTION works is, on an attempt to acquire it, the callee will do a `Try Lock' operation on the underlying SEMAPHORE, $x$ amount of times, and if it still was not able to lock it, the thread will go to sleep, and enter the queue for the SEMAPHORE. 

\subsection{Pros}

A CRITICAL SECTION is great for a high-use lock, that is, something that is acquired and released very frequently, and the amount of time the lock is head is a short to moderate amount of time. Using a CRITICAL SECTION over a plain old MUTEX or SEMAPHORE should, overall, have a performance boost, because the caller thread does not always go to sleep if the lock could not be acquired. 

\subsection{Cons}

Keep in mind, since a minor SPIN LOCK is used, you are trading time for CPU power. Entering a while loop to perform a `Try Lock' on the SEMAPHORE will peg the core to 100\%. Also, if the continued `Try Lock' on the SEMAPHORE failed, and the resulting thread enters the SEMAPHORE queue, the thread will go to sleep until it is signalled again by the kernel to wake up. This can be an indefinate amount of time, if the current lock-holder does not release the lock. 

\subsection{Waiting}

When a CRITICAL SECTION is being waited for, there are two possible operations.

\vspace{2mm}
\noindent
\textit{Try Lock} operation on a CRITICAL SECTION will result in attempting to enter the CRITICAL SECTION once, and exactly once. If it fails, it will return \textit{FALSE}.

\vspace{2mm}
\noindent
\textit{Normal Lock} is first performed as an `Try Lock' on the underlying SEMAPHORE \textit{ulSpinCount} (defualt is 0x400) times, and if that results in a \textit{FALSE} lock, it will simply enter the queue for the SEMAPHORE. This can be an indefinite wait time, so a \textbf{POSSIBLE DEADLOCK CONDITON CAN OCCUR}.

\subsubsection{Special Wait Times}

There exist two special wait times, which is the value passed into \textit{ullMilliSeconds} on a call to \textit{WaitForSingleObject()}.

\vspace{2mm}
\noindent
\textbf{0 / NULL}:

\vspace{2mm}
\noindent
When \textbf{0} or \textbf{NULL} are passed into \textit{ullMilliSeconds}, this will perform the \textit{Try Lock} operation, as stated above.

\vspace{2mm}
\noindent
\textbf{INFINITE\_WAIT\_TIME}:

\vspace{2mm}
\noindent
When \textbf{INFINITE\_WAIT\_TIME} or \textbf{MAX\_ULONGLONG} are passed into \textit{ullMilliSeconds} the result is an absolute infinite wait time. That is, it will wait until the end of time. As a result \textbf{A POSSIBLE DEADLOCK CONDITION CAN OCCUR}.

\subsection{CCriticalSection Functions}

Lets take a look at the various functions that are based around a CRITICAL SECTION.

\subsubsection{CreateCriticalSection}

\begin{lstlisting}
_Result_Null_On_Failure_
_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HANDLE
CreateCriticalSection(
	VOID
);
\end{lstlisting}

\vspace{2mm}
\noindent
\textit{CreateCriticalSection} takes no arguments and returns a valid HANDLE to the CRITICAL SECTION.

\vspace{2mm}
\noindent
\textbf{Return $\rightarrow$ HANDLE}: On success, the return is a valid HANDLE to the newly created CRITICAL SECTION.

\subsubsection{CreateCriticalSectionAndSpecifySpinCount}

\begin{lstlisting}
_Result_Null_On_Failure_
_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HANDLE
CreateCriticalSectionAndSpecifySpinCount(
	_In_ 		ULONG 		ulSpinCount
);
\end{lstlisting}

\vspace{2mm}
\noindent 
\textit{CreateCriticalSectionAndSpecifySpinCount} takes in exactly one parameter, and will return a HANDLE to the newly created CRITICAL SECTION.

\vspace{2mm}
\noindent
\textbf{ulSpinCount}: This is a ULONG that represents the desired amount of times the `Try Lock' operation on the SEMAPHORE will be called. By default this is 0x400 (1024) times. A longer time may be used to help increase performance if you have a lot of threads entering the SEMAPHORE queue, and the wait times are consistent. Increasing this value can actually increase overall performance, depending on your use-case.

\vspace{2mm}
\noindent
\textbf{Return $\rightarrow$ HANDLE}: On success, a valid HANDLE is returned that represents the newly created CRITICAL SECTION Object.

\break
\subsubsection{EnterCriticalSection}

\begin{lstlisting}
_Success_(return != FALSE, _Acquires_Lock_(hCritSec, _Lock_Kind_Semaphore))
PODNET_API
BOOL
EnterCriticalSection(
	_In_ 		HANDLE 		hCritSec
);
\end{lstlisting}

\vspace{2mm}
\noindent
\textit{EnterCriticalSection} takes exactly one parameter, that being the HANDLE to the CRITICAL SECTION in question, and returns a BOOL. Please note, by calling this function over \textit{WaitForSingleObject()}, it has an \textbf{INFINITE} wait time, and a \textbf{DEADLOCK CONDITION CAN OCCUR}, as such, it is recommended to call \textit{WaitForSingleObject()} with a specified ullMilliSeconds. This function is exactly equivalent to \textit{WaitForSingleObject(hCritSec, INFINITE\_WAIT\_TIME)}.

\vspace{2mm}
\noindent
\textbf{hCritSec}: This is a HANDLE to the CRITICAL SECTION you would like to lock.

\vspace{2mm}
\noindent
\textbf{Return $\rightarrow$ BOOL} On success, the result will be a non-zero number (\textit{!FALSE}).

\subsubsection{TryToEnterCriticalSection}

\begin{lstlisting}
_Success_(return != FALSE, _Acquires_Lock_(hCritSec, _Lock_Kind_Semaphore))
PODNET_API
BOOL
TryToEnterCriticalSection(
	_In_ 		HANDLE 		hCritSec
);
\end{lstlisting}

\vspace{2mm}
\noindent
\textit{TryToEnterCriticalSection} takes exactly one parameter, that being a HANDLE to the CRITICAL SECTION in question, and returns a BOOL. This function, unlike \textit{EnterCriticalSection()} does not wait for an INFINITE amount of time, rather it goes through just the `Try Lock' operation on the SEMAPHORE; that being, it tries to acquire the SEMAPHORE \textit{ulSpinCount} amount of times (default is 0x400). This function is exactly equivalent to \textit{WaitForSingleObject(hCritSec, 0x00)}.

\vspace{2mm}
\noindent
\textbf{hCritSec}: This is a HANDLE to the CRITICAL SECTION that is going to be acquired.

\vspace{2mm}
\noindent
\textbf{Return $\rightarrow$ BOOL}: On success, the return value will be a BOOL.


\subsubsection{ExitCriticalSection}

\begin{lstlisting}
_Success_(return != FALSE, _Release_Lock_(hCritSec, _Lock_Kind_Semaphore_))
PODNET_API
BOOL
ExitCriticalSection(
	_In_		HANDLE		hCritSec
);
\end{lstlisting}

\vspace{2mm}
\noindent
\textit{ExitCriticalSection} takes exactly one parameter, that being the HANDLE that is going to be unlocked, and returns a BOOL. This function will release the lock, and make it available to be acquired by another caller. This function is exactly equivalent to \textit{ReleaseSingleObject(hCritSec)}. 

\vspace{2mm}
\noindent
\textbf{hCritSect}: This is a HANDLE to the CRITICAL SECTION that is going to be unlocked.

\vspace{2mm}
\noindent
\textbf{Return $\rightarrow$ BOOL}: On success this function will return a non-zero number (\textit{!FALSE)}.



\end{document}
