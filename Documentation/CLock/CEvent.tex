\documentclass{article}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{underscore}
\usepackage{systeme}
\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{url}
\usepackage{graphicx}
%\usepackage[T1]{fontenc}
\usepackage{titlesec}
\usepackage{titling}
\usepackage[margin=1in]{geometry}
\usepackage{listings}
\usepackage{color}
\usepackage{courier}
\usepackage{standalone}

\newcommand{\imagi}{\mathnormal{i}}
\newcommand{\imagj}{\mathnormal{j}}
\newcommand{\euler}{\mathnormal{e}}

\graphicspath{ {Images/} }

\lstset{language=C,
captionpos=b,
tabsize=8,
frame=lines,
keywordstyle=\color{blue},
commentstyle=\color{darkgreen},
stringstyle=\color{red},
numbers=left,
numberstyle=\normalsize,
numbersep=5pt,
breaklines=true,
showstringspaces=false,
basicstyle=\normalsize\ttfamily,
emph={label}}

\begin{document}

\title{PodNet $\Rightarrow$ CLock $\rightarrow$ CEvent}
\author{Zach Podbielniak}

\maketitle

\section{Event}

What is an EVENT? An EVENT is, well, and event that either has or has not happened. It has exactly two states. Other threads can wait for the status of the EVENT to change. 

\subsection{Types of Events}

There are exactly two types of EVENTs. There is a normal EVENT, which can be set, and reset. There is also a BINARY EVENT, which can only be triggered once. Once it triggers, it \textbf{can not} be reset.

\subsection{What Are They Useful For?}

EVENTs can be used for, well, just about anything. Most for, you guessed it, signaling that an EVENT has happened. They are quite useful in Event-Driven Programming. A good use case would be for keyboard handling. When, lets say, 'k' is pressed, it might trigger an EVENT, which another thread may be waiting for, and it sees that 'k' was pressed and can do whatever, when its done, it may reset it again, so 'k' can be pressed again.

\vspace{2mm}
\noindent
In the PodNet Library, for example, an EVENT is used for a FUTURE and PROMISE set. When the PROMISE is done computing, it triggers the EVENT, which then the FUTURE knows that it can retrieve the value.

\vspace{2mm}
\noindent
EVENTs can also be set up to call a call-back function when it is triggered. This can be very useful. The function prototype for this call-back is;

\begin{lstlisting}
typedef _Call_Back_ LPVOID(* LPFN_EVENT_CALLBACK_PROC)(
	_In_Opt_ 	LPVOID 		lpParam
);
\end{lstlisting}

\subsection{Special Wait Times}

Only one special wait time exists for an EVENT.

\vspace{2mm}
\noindent
\textbf{INFINITE\_WAIT\_TIME}:

\vspace{2mm}
\noindent
If \textbf{INFINITE\_WAIT\_TIME} or \textbf{MAX\_ULONGLONG} was passed into the parameter \textit{ullMilliSeconds} for the call to \textit{WaitForSingleObject()}, the calling thread will wait at least until the end of time. \textbf{THIS CAN CAUSE A DEADLOCK CONDITION}. Use with caution.

\subsection{Event Functions}

Lets take a look at the functions related to EVENTs.

\subsubsection{CreateEvent}

\begin{lstlisting}
_Result_Null_On_Failure_
_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HANDLE
CreateEvent(
	_In_ 		BOOL 		bInitialState
);
\end{lstlisting}

\vspace{2mm}
\noindent
\textit{CreateEvent} takes exactly one parameter, and will return a HANDLE to the newly created EVENT.

\vspace{2mm}
\noindent
\textbf{bInitialState}: This is a BOOL that represents whether the EVENT should be started as HIGH or LOW. Passing in \textit{TRUE} will result in a signaled state, and passing in \textit{FALSE} will result in a non-signaled state.

\vspace{2mm}
\noindent
It should be noted that if you pass in \textit{TRUE}, any other thread that waits on this EVENT, such as with \textit{WaitForSingleObject()}, will automatically succeed.

\vspace{2mm}
\noindent
\textbf{Return $\rightarrow$ HANDLE}: On success, a valid HANDLE to the newly created EVENT will be returned.


\subsubsection{CreateBinaryEvent}

\begin{lstlisting}
_Result_Null_On_Failure_ 
_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HANDLE 
CreateBinaryEvent(
	VOID
);
\end{lstlisting}

\vspace{2mm}
\noindent
\textit{CreateBinaryEvent} takes no parameters and will return a HANDLE to the newly created BINARY EVENT. A BINARY EVENT is an EVENT that can only be signaled (triggered) once. That is, once it is signaled, any call to \textit{ResetEvent()} will result in a fail.

\vspace{2mm}
\noindent
\textbf{Return $\rightarrow$ HANDLE}: On success, a valid HANDLE to the newly created EVENT will be returned.

\subsubsection{CreateEventEx}

\begin{lstlisting}
_Result_Null_On_Failure_
_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HANDLE
CreateEventEx(
	_In_ 		BOOL 			 	bInitialState,
	_In_ 		LPFN_EVENT_CALLBACK_PROC 	lpfnCallBack,
	_In_Opt_ 	LPVOID 			 	lpParam 
);
\end{lstlisting}

\vspace{2mm}
\noindent
\textit{CreateEventEx} takes in 3 parameters, and will return a HANDLE to the newly created EVENT. It should be noted, that when an event created by \textit{CreateEventEx()}, any call to \textit{SignalEvent()}, will result in the appropriate CALL BACK being called.

\vspace{2mm}
\noindent
\textbf{bInitialState}: This is a BOOL that represents whether the EVENT should be started as HIGH or LOW. Passing in \textit{TRUE} will result in a signaled state, and passing in \textit{FALSE} will result in a non-signaled state.

\vspace{2mm}
\noindent
It should be noted that if you pass in \textit{TRUE}, any other thread that waits on this EVENT, such as with \textit{WaitForSingleObject()}, will automatically succeed.

\vspace{2mm}
\noindent
\textbf{lpfnCallBack}: This is a function pointer to a function prototyped as the type \textit{LPFN_EVENT_CALLBACK_PROC}. When \textit{SignalEvent()} is called, this CALL BACK will be called.

\vspace{2mm}
\noindent
\textbf{lpParam}: This is an optional parameter, and is the parameter that is passed to \textit{lpfnCallBack} on \textit{SignalEvent()} call.

\vspace{2mm}
\noindent
\textbf{Return $\rightarrow$ HANDLE}: On success, a valid HANDLE to the newly created EVENT will be returned.

\subsubsection{SignalEvent}

\begin{lstlisting}
_Success_(return != FALSE, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Event_)))
PODNET_API
BOOL
SignalEvent(
	_In_ 		HANDLE 		hEvent
);
\end{lstlisting}

\vspace{2mm}
\noindent
\textit{SignalEvent} takes exactly one parameter, and will return a BOOL indicating the result of the operation. If the EVENT has already been signaled before this call, it will just result in \textit{TRUE} being returned.

\vspace{2mm}
\noindent
\textbf{hEvent}: This is a HANDLE to the EVENT that is going to be signaled.

\vspace{2mm}
\noindent
\textbf{Return $\rightarrow$ BOOL}: On success, a BOOL will be returned indicating the success of the operation. A non-zero value will be returned signifying it was a success.



\subsubsection{SignalEventEx}

\begin{lstlisting}
_Success_(return != FALSE, _Calls_Call_Back_ && _Acquires_Lock_(_Has_Lock_Kind(_Lock_Kind_Event_)))
PODNET_API
BOOL
SignalEventEx(
	_In_ 		HANDLE 		hEvent,
	_In_ 		LPVOID 		lpParam
);
\end{lstlisting}

\textit{SignalEventEx} takes 2 parameters, and will return a BOOL signifying whether the operation was a success.

\vspace{2mm}
\noindent
\textbf{hEvent}: This is a HANDLE to the EVENT that is going to be signaled.

\vspace{2mm}
\noindent
\textbf{lpParam}: This is an optional parameter of type \textit{LPVOID} that can be passed to the CALL BACK (\textit{LPFN_EVENT_CALLBACK_PROC}), that is specified with this EVENT. Please note, that when creating an EVENT with \textit{CreateEventEx()}, there was an optional parameter that could be passed to the \textit{LPFN_EVENT_CALLBACK_PROC}. Passing in a NON-NULL value to this function, will result in this value being passed, and not the one specified by calling \textit{CreateEventEx()}. To use the one that was passed in on calling \textit{CreateEventEx()}, simply pass in \textit{NULL}.

\vspace{2mm}
\noindent
\textbf{Return $\rightarrow$ BOOL}: On success, a BOOL will be returned indicating the success of the operation. A non-zero value will be returned signifying it was a success.

\subsubsection{ResetEvent}

\begin{lstlisting}
_Success_(return != FALSE, _Releases_Lock_(_Has_Lock_Kind(_Lock_Kind_Event_)))
PODNET_API
BOOl
ResetEvent(
	_In_ 		HANDLE 		hEvent
);
\end{lstlisting}

\vspace{2mm}
\noindent
\textit{ResetEvent} takes in exactly one parameter, and will return a BOOL indicating the status of the operation. If the EVENT is already in the non-signaled state, the result will be \textit{TRUE}. If you are attempting to reset a BINARY EVENT, you will get \textit{FALSE} 100\% of the time. So just don't do it.

\vspace{2mm}
\noindent
\textbf{hEvent}: This is a HANDLE to the EVENT that is going to be reset.

\vspace{2mm}
\noindent
\textbf{Return $\rightarrow$ BOOL}: On success, the result will be non-zero (\textit{!FALSE}). If \textbf{hEvent} was a HANDLE to a BINARY EVENT, this result will always be 0 (\textit{FALSE}).



\subsubsection{GetEvent}

\begin{lstlisting}
_Success_(return == (FALSE || !FALSE), _Interlocked_Operation_)
PODNET_API
BOOL
GetEvent(
	_In_ 		HANDLE 		hEvent
);
\end{lstlisting}

\vspace{2mm}
\noindent
\textit{GetEvent} takes in exactly one parameter, and returns the status of the EVENT in question. This function is used as the basis for the override of \textit{WaitForSingleObject)} for EVENTs. 

\vspace{2mm}
\noindent
\textbf{hEvent}: This is a HANDLE to the EVENT that is going to be reset.

\vspace{2mm}
\noindent
\textbf{Return $\rightarrow$ BOOL}: If the EVENT is signaled, the result will be a non-zero value (\textit{!FALSE}), and if the EVENT is NOT signaled, the result will be \textit{FALSE}, or simply just 0.



\end{document}
