\documentclass{article}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{underscore}
\usepackage{systeme}
\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{url}
\usepackage{graphicx}
%\usepackage[T1]{fontenc}
\usepackage{titlesec}
\usepackage{titling}
\usepackage[margin=1in]{geometry}
\usepackage{listings}
\usepackage{color}
\usepackage{courier}
\usepackage{standalone}

\newcommand{\imagi}{\mathnormal{i}}
\newcommand{\imagj}{\mathnormal{j}}
\newcommand{\euler}{\mathnormal{e}}

\graphicspath{ {Images/} }

\lstset{language=C,
captionpos=b,
tabsize=8,
frame=lines,
keywordstyle=\color{blue},
commentstyle=\color{darkgreen},
stringstyle=\color{red},
numbers=left,
numberstyle=\normalsize,
numbersep=5pt,
breaklines=true,
showstringspaces=false,
basicstyle=\large\ttfamily,
emph={label}}

\begin{document}

\title{PodNet $\Rightarrow$  CHandle $\rightarrow$ HANDLEs}
\author{Zach Podbielniak}

\maketitle

\part{CHandle}

This document will outline what a HANDLE is, and what they mean to you.

\section{What is a HANDLE?}

A HANDLE layman's terms is a ``blind'' pointer to a system-managed resource. This resource could be anything from a thread to a gpio pin. A HANDLE is like an instance to an ``object'' that represents it's current state. To you, the library user, it mean's absolutely nothing; but pass around the HANDLE to a few functions, and you got yourself a very powerful tool. 

\section{Why HANDLEs?}

If you have ever used the WIN32 API, you are very familiar with HANDLEs and what they do, and I'm also sure you realize how powerful they can be. If not, that's okay.

\vspace{2mm}
\noindent
As mentioned above, a HANDLE represents some state to a system-managed resource. But a HANDLE can also be used to keep track of function overrides, as well as data, and what lies behind that data. In short, a HANDLE is kind of like a god object, it represents all things about a particular state.

\subsection{Encapsulation}

\textbf{Encapsulation} is when data is hidden from the `outside' world, typically by hiding inside a compiled .c file. A HANDLE provides a great interface for this.

\vspace{2mm}
\noindent 
Not everything needs to have its state visisble, as well as editable from the outside world. That beind said, keeping things behind closed-doors for system-managed resources is the best practice. Think to yourself for a second, does a Thread, or Future's internals need to be exposed to the user of the library? No, not at all. If you're worried about tweaking things, then modify the library itself, not through some interface to it.

\subsection{Virtual Function / Interfaces}

Virtual Functions and Interfaces!? In C!? Yup, thats right, through the power of mystical black voodoo magic, we bring you virtual functions and interfaces straight to a C near you.  

\vspace{2mm}
\noindent 
Seriously though, throughtout the library you will be calling functions such as \textit{Write}, or \textit{WaitForSingleObject}, all of these have default `versions' of the function that can be overridden. If you weren't reading this, you probably thought those functions worked on magical pixie dust or something.

\vspace{2mm}
\noindent
This is all done through the power of HANDLEs, and the CHandle subsection of the \textit{PodNet Library}.

\subsection{Inheritance}

Thats right, inheritance in C, what a wonderful, and powerful thing. Many people said it can't be dont, but we did it. All joking aside, it is important to know that a HANDLE can be viewed as a ``Super'' Class to anything that ``Inherits'' from it. While it may not be true OO-style inheritance, very similar concepts apply.

\vspace{2mm}
\noindent
Any object that inherits from a HANDLE, such as a FILE, a THREAD, or SERIAL, is called a derivative. Why? Because the HANDLE's value is derived from the inheritee, so, we call them derivatives. More specifically, you will see them often denoted as HDERIVATIVE, which can be any piece of data that belongs to the underlying HANDLE; whether it be the whole `object' or just a piece of it. Just incase you're not paying attention, an HDERIVATIVE is Hungarian Notation for HANDLE-DERIVATIVE.

\vspace{2mm}
\noindent
A HANDLE can have a single inheritor, or multiple inheritors. Single is most common, but if you want to create something like an event handler, you might want to create aHANDLE that has two or more EVENTs as the underlying inheritor, and still be able to use nicities like \textit{WaitForSingleObject()}. Also, the underlying derivatives do \textbf{NOT} need to be of the same type. You can create a huge underlying class structure that is represented by a single HANDLE. This is \textbf{EXTREMELY} powerful.

\subsection{HANDLE Object}

Below is the struct for a HANDLE, it's not exactly small...

\begin{lstlisting}
typedef struct __OBJECT
{
	LPHDERIVATIVE		lphDerivatives;
	LPULONG			lpulDerivativeTypes;
	ULONG			ulNumberOfDerivatives;
	LPULONGLONG		lpullIndexes;
	DLPFN_HANDLE_DESTROY	dlpfnDestroyers;
	LPULONG			lpulDestroyFlags;
	DLPFN_HANDLE_ALARM	dlpfnAlarmHandlers;
	LPULONG			lpulAlarmFlags;
	DLPFN_HANDLE_ERROR	dlpfnErrorHandlers;
	LPULONG			lpulErrorFlags;
	DLPFN_HANDLE_EVENT_SET	dlpfnEventSetHandlers;
	LPULONG			lpulEventSetFlags;
	DLPFN_HANDLE_EVENT_WAIT	dlpfnEventWaitHandlers;
	LPULONG			lpulEventWaitFlags;
	DLPFN_HANDLE_RELEASE	dlpfnReleaseHandlers;
	LPFN_LOCK_PROC		lpfnLockProc;
	LPFN_UNLOCK_PROC	lpfnUnlockProc;
	LPFN_TRY_LOCK_PROC	lpfnTryLockProc;
	HANDLE			hCritSec;
	LPHANDLE		lphEvents;
	ULONG			ulNumberOfEvents;
	LPFN_HANDLE_WRITE_PROC 	lpfnWriteProc;
	LPFN_HANDLE_READ_PROC	lpfnReadProc;
} OBJECT, *HANDLE, **LPHANDLE, ***DLPHANDLE;
\end{lstlisting}

\vspace{2mm}
\noindent
This struct is hidden away in the CHandle/CHandleObject.c file, and only CHandle sub libraries will normally see this. To the external user of the library, you will just see...

\begin{lstlisting}
typedef __OBJECT 	*HANDLE, **LPHANDLE, ***DLPHANDLE;
\end{lstlisting}

\vspace{2mm}
\noindent 
If you wish to modify, or directly access the underlying OBJECT, just include CHandle/CHandleObject.c . Please note, this is stored in a `.c' file, mostly because it is recommended to not include it, as there is some minor amount of black-voodoo magic behind how HANDLEs work. 

\subsection{HANDLE Types}

There are different HANDLE types. Each HANDLE shares a sense of similarity, each type is different. For example, a HANDLE for a THREAD is not the same as a HANDLE for a FILE. Here is a list of all the different kinds.

\begin{lstlisting}
typedef enum __HANDLE_TYPE
{
HANDLE_TYPE_EVENT			= 0x0100,
HANDLE_TYPE_CRIT_SEC			= 0x0101,
HANDLE_TYPE_SEMAPHORE			= 0x0102,
HANDLE_TYPE_MUTEX			= 0x0103,
HANDLE_TYPE_SPINLOCK			= 0x0104,
HANDLE_TYPE_THREAD			= 0x0105,
HANDLE_TYPE_PROMISE			= 0x0106,
HANDLE_TYPE_FUTURE			= 0x0107,
HANDLE_TYPE_FILE			= 0x0108,
HANDLE_TYPE_LOG				= 0x0109,
HANDLE_TYPE_ATOM_TABLE			= 0x010A,
HANDLE_TYPE_HANDLE_TABLE		= 0x010B,
HANDLE_TYPE_ONCE_CALLABLE		= 0x010C,
HANDLE_TYPE_SOCKET			= 0x010D,
HANDLE_TYPE_WEBSERVER			= 0x010E,
HANDLE_TYPE_LUA				= 0x0110,
HANDLE_TYPE_DEVICE_GPIO			= 0x0111,
HANDLE_TYPE_DEVICE_SERIAL		= 0x0112,
HANDLE_TYPE_DEVICE_I2C			= 0x0113,
HANDLE_TYPE_DEVICE_SPI			= 0x0114
} HANDLE_TYPE;
\end{lstlisting}

\subsection{Other HANDLES?}

HANDLE is not the only type of HANDLE used through the \textit{PodNet Library}. While a HANDLE can be considered to be a `heavy' resource, that is, requires some time to create one, has a decent memory foot print (around half a K on 64 bit systems), and there is some black-wizardry going on in the background with, they are not the only kind. HANDLES are system-resource related, think: threads, serial devices, files, logs, promise and futures, etc. There also exist other HANDLES, such as HVECTORs, HLINKEDLISTS, and HSTACKS to name just a few. These are what I like to denote as `Light HANDLES'. These HANDLES are not registered in any HANDLE Lookup Table, or anything, they are simply a basic pointer, that points to data you can't see.

\vspace{2mm}
\noindent
We define a HANDLE as any abstract-datatype that makes full use of encapsulation. All HANDLES denoted by the type `HANDLE' are system-managed resources that fully utilize the above technologies, while H<WhateverHere> is a `Light HANDLE'.


\section{Overidable Functions}

In the previous secition, we mentioned how some functions can be overrided with HANDLES. Let's take a closer look to see which functions can be overrided. 

\subsection{Destroyer Function}

This function is a call back function that is called when \textit{DestroyHandle()} is called. This function is considered to be COMPLETELY virtual, and MUST be overidden. There exists two ways to overide this function.

\begin{enumerate}
\item On HANDLE Creation. When calling \textit{CreateHandleWithSingleInheritor()} or any of the other equivalent HANDLE creation functions, it is a parameter. Simply pass in the function pointer. Please note, this is \textbf{NOT} and optional parameter, if you fail to pass in a function pointer that defines the destroyer function, the HANDLE creation will fail, and result in a return of NULLPTR.
\item \textit{SetHandleDestroyFunction()} $\rightarrow$ Can be called to re-update the destroyer function.
\end{enumerate}


\subsection{Event Wait Function}

This function is a call back that is called when \textit{WaitForSingleObject()} or \textit{WaitForMultipleObjects()} are called. It denotes how to handle waiting for a specific time. To comply with these over rides, the resulting call back function must account of the time lapsed, and how long to wait, failing to do so, breaks requirements for this library.

\vspace{2mm}
\noindent 
This overide, gives the power to have this general idea of a lock, and being able to wait for it. A function can accept any HANDLE as a a lock parameter, and wait for that specific lock. This is an INSANELY powerful tool. 

\vspace{2mm}
\noindent
There exists one way to override this function. Also, if you created the HANDLE with a function that creates an underlying EVENT(s), this function by default, will wait for the first EVENT to be set.

\begin{enumerate}
\item \textit{SetHandleEventWaitFunction()} $\rightarrow$ This will set the corresponding call back for \textit{WaitForSingleObject()} and \textit{WaitForMultipleObjects()}.
\end{enumerate}


\subsection{Event Release Function}

This function is a call back that is called when \textit{ReleaseSingleObject()} or \textit{ReleaseMultipleObjects()} are called. It denotes how to handle releasing a lock, or other lock-like object. To comply with standards, this function should not rely on any other input other than the HANDLE that is passed in. 

\vspace{2mm}
\noindent
There exists noe way to override this function. Also, if you created the HANDLE with a function that creates an underlying EVENT(s), this function by default, will reset the first EVENT.

\begin{enumerate}
\item \textit{SetHandleReleaseFunction()} $\rightarrow$ This will set the corresponding call back for \textit{ReleaseSingleObject} and \textit{ReleaseMultipleObjects}.
\end{enumerate}


\subsection{Lock Functions}

These functions come in a group. You have to define the Lock and Unlock, but you do NOT have to define the TryLock. These functions are call backs to functions that handle \textit{LockHandle()}, \textit{UnlockHandle()}, and \textit{TryLockHandle()}. This is usefule for functions that have some form of derived lock, but are not a lock themselves, or want to provide an alternative to \textit{WaitForSingleObject} and a-like. 

\vspace{2mm}
\noindent
There is exactly one way to overide these, and that is the following:

\begin{enumerate}
\item \textit{SetHandleLockFunctions()} $\rightarrow$ Sets the corresponding call backs for \textit{LockHandle()}, \textit{UnlockHandle()}, and \textit{TryLockHandle()}, respectively.
\end{enumerate}


\subsection{Write, WriteFile, WriteDataToFile}

\textit{Write()} is a very useful function. It is fairly universal, any HANDLE that has this overriden can be written to. That could be a socket, a log, a generic, file, or even a serial device. It can be pretty powerful.

\vspace{2mm}
\noindent
To override this there exists one function:

\begin{enumerate}
\item \textit{SetHandleReadAndWriteProcs()} $\rightarrow$ This function is used to overide the default write proc which is used in the corresponding functions: \textit{Write()}, \textit{WriteFile()}, and \textit{WriteDataToFile()}.
\end{enumerate}


\subsection{Read, ReadFile, ReadWholeFile, ReadFileAsynchronously}

\textit{Read()} is another very useful function. It is also universal and can be used in conjunction with any socket, log, file, or serial device.

\vspace{2mm}
\noindent
To overide this there exists one function:

\begin{enumerate}
\item \textit{SetHandleReadAndWriteProcs()} $\rightarrow$ This function is used to overide the default read proc which is used in the corresponding functions: \textit{Read()}, \textit{ReadFile()}, \textit{ReadWholeFile()}, and \textit{ReadFileAsynchronously()}.
\end{enumerate}





\end{document}
