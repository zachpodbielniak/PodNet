/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CHandleTable.h"

typedef struct __HANDLE_TABLE
{
        INHERITS_FROM_HANDLE();
        HANDLE          hSpinLock;
        LPVOID          lpHandleRegistrar;
        ULONGLONG       ullNumberOfHandles;        
        ULONGLONG       ullNextId;
        ULONG           ulFlags;
} HANDLE_TABLE, *LPHANDLE_TABLE;


typedef struct __HANDLE_TABLE_NODE
{
        ULONGLONG       ullId;
        HANDLE          hHandle;
} HANDLE_TABLE_NODE, *LPHANDLE_TABLE_NODE;


GLOBAL_VARIABLE DONT_OPTIMIZE_OUT HANDLE hGlobalHandleTable = NULLPTR;


_Call_Back_
INTERNAL_OPERATION
LONG 
__BinarySearchTreeCompareTableNode(
	_In_ 		LPVOID 		lpLeafValue,
	_In_ 		LPVOID 		lpComparandValue,
	_In_ 		UARCHLONG	ualSize
){
	UNREFERENCED_PARAMETER(ualSize);
	LPHANDLE_TABLE_NODE lphtnLeaf;
	LPHANDLE_TABLE_NODE lphtnComparand;

	lphtnLeaf = lpLeafValue;
	lphtnComparand = lpComparandValue;

	/* 
		Leaf is greater, return -1.
		Leaf is same, 0.
		Leaf is less, 1.
	*/
	if (lphtnLeaf->ullId == lphtnComparand->ullId)
	{ return 0; }
	else if (lphtnLeaf->ullId > lphtnComparand->ullId)
	{ return -1; }
	else 
	{ return 1; }

	return MAX_ULONG;
}




_Success_(return != FALSE, ...)
INTERNAL_OPERATION
BOOL
__DestroyHandleTable(
        _In_                            HDERIVATIVE             hDerivative,
        _In_Opt_                        ULONG                   ulFlags
){
        UNREFERENCED_PARAMETER(ulFlags);
        EXIT_IF_UNLIKELY_NULL(hDerivative, FALSE);
        LPHANDLE_TABLE lphtTable;
        lphtTable = (LPHANDLE_TABLE)hDerivative;
        WaitForSingleObject(lphtTable->hSpinLock, INFINITE_WAIT_TIME);

        if (lphtTable->ulFlags & HANDLE_TABLE_HIGH_PERFORMANCE)
        { DestroyVector((HVECTOR)lphtTable->lpHandleRegistrar); }
        else if (lphtTable->ulFlags & HANDLE_TABLE_MEMORY_CONCIOUS)
        { DestroyLinkedList((HLINKEDLIST)lphtTable->lpHandleRegistrar); }
	else if (lphtTable->ulFlags & HANDLE_TABLE_ULTRA_PERFORMANCE || lphtTable->ulFlags & HANDLE_TABLE_ULTRA_PERFORMANCE_EX)
	{ DestroyHashTable(lphtTable->lpHandleRegistrar); }
	else if (lphtTable->ulFlags & HANDLE_TABLE_PERFORMANT_AND_CONCIOUS || lphtTable->ulFlags & HANDLE_TABLE_PERFORMANT_AND_CONCIOUS_EX)
	{ DestroyBinarySearchTree(lphtTable->lpHandleRegistrar); }
 
        DestroyHandle(lphtTable->hSpinLock);
        ZeroMemory(lphtTable, sizeof(HANDLE_TABLE));       
        FreeMemory(lphtTable);
        return TRUE;
}




_Success_(return != NULLPTR, ...)
PODNET_API
HANDLE
CreateHandleTable(
        _In_                            ULONGLONG               ullSize,
        _Reserved_Must_Be_Null_         LPVOID                  lpReserved,
        _In_Opt_                        ULONG                   ulFlags
){
        UNREFERENCED_PARAMETER(lpReserved);
        HANDLE hTable;
        LPHANDLE_TABLE lphtTable;
        ULONGLONG ullID;
        lphtTable = LocalAllocAndZero(sizeof(HANDLE_TABLE));
        EXIT_IF_NULL(lphtTable, NULLPTR);

        if (NULL == ulFlags)
        { ulFlags = HANDLE_TABLE_HIGH_PERFORMANCE; }

        if (UNLIKELY(hGlobalHandleTable == NULLPTR))
        {
                LPHANDLE_TABLE lphtGlobal;
                lphtGlobal = LocalAllocAndZero(sizeof(HANDLE_TABLE));
		if (NULLPTR == lphtGlobal)
		{
			FreeMemory(lphtTable);
			return NULL_OBJECT;
		}

                hGlobalHandleTable = CreateHandleWithSingleInheritor(
                                lphtGlobal,
                                &(lphtGlobal->hThis),
                                HANDLE_TYPE_HANDLE_TABLE,
                                __DestroyHandleTable,
                                NULL,
                                NULLPTR,
                                NULL,
                                NULLPTR,
                                NULL,
                                &ullID,
                                NULL
                );
                
		if (NULL_OBJECT == hGlobalHandleTable)
		{
			FreeMemory(lphtTable);
			FreeMemory(lphtGlobal);
			return NULL_OBJECT;
		}
		
                lphtGlobal->hSpinLock = CreateSpinLock();
                lphtGlobal->lpHandleRegistrar = (LPVOID)CreateVector(20U, sizeof(HANDLE), NULL);
                lphtGlobal->ullNumberOfHandles = 0ULL;
                lphtGlobal->ulFlags = HANDLE_TABLE_HIGH_PERFORMANCE;
                lphtGlobal->ullNextId = 1ULL;
        
	}

	if (0 == ullSize && (ulFlags & HANDLE_TABLE_ULTRA_PERFORMANCE || ulFlags & HANDLE_TABLE_ULTRA_PERFORMANCE_EX))
	{ ullSize = HANDLE_TABLE_HASHTABLE_SIZE; }
	else if (ullSize == 0) 
	{ ullSize = 0x20; }

        /* TODO: Add Error Code For Resource Initialization */
        lphtTable->hSpinLock = CreateSpinLock();

        if (ulFlags & HANDLE_TABLE_HIGH_PERFORMANCE)
        { lphtTable->lpHandleRegistrar = (LPVOID)CreateVector((ULONG)ullSize, sizeof(HANDLE), NULL); }
        else if (ulFlags & HANDLE_TABLE_MEMORY_CONCIOUS)
        { lphtTable->lpHandleRegistrar = (LPVOID)CreateLinkedList(sizeof(HANDLE_TABLE_NODE), TRUE, NULL); }
	else if (ulFlags & HANDLE_TABLE_ULTRA_PERFORMANCE)
	{ lphtTable->lpHandleRegistrar = CreateHashTable((UARCHLONG)ullSize);}
	else if (ulFlags & HANDLE_TABLE_ULTRA_PERFORMANCE_EX)
	{ lphtTable->lpHandleRegistrar = CreateHashTable((UARCHLONG)ullSize);}
	else if (ulFlags & HANDLE_TABLE_PERFORMANT_AND_CONCIOUS || ulFlags & HANDLE_TABLE_PERFORMANT_AND_CONCIOUS_EX)
	{ 
		lphtTable->lpHandleRegistrar = CreateBinarySearchTree(
			TYPE_UULTRALONG,	/* The ID */
			sizeof(HANDLE_TABLE_NODE),	
			__BinarySearchTreeCompareTableNode,
			NULLPTR
		);
	}
	
        lphtTable->ullNumberOfHandles = 0;
        lphtTable->ulFlags = ulFlags;
        lphtTable->ullNextId = 1ULL;

        hTable = CreateHandleWithSingleInheritor(
                        lphtTable,
                        &(lphtTable->hThis),
                        HANDLE_TYPE_HANDLE_TABLE,
                        __DestroyHandleTable,
                        NULL,
                        NULLPTR,
                        NULL,
                        NULLPTR,
                        NULL,
                        &ullID,
                        NULL
        );
        EXIT_IF_NULL(hTable, NULLPTR);

        RegisterHandle(hGlobalHandleTable, hTable);

        return hTable;        
}




_Success_(return != NULL, ...)
PODNET_API
ULONGLONG
RegisterHandle(
        _In_                            HANDLE                  hHandleTable,
        _In_                            HANDLE                  hHandle
){
        LPHANDLE_TABLE lphtTable;
        ULONGLONG ullId;

        lphtTable = (LPHANDLE_TABLE)GetHandleDerivative(hHandleTable);
	EXIT_IF_UNLIKELY_NULL(lphtTable, 0);

        ullId = 0;
        if (NULLPTR != lphtTable->hSpinLock)
	{ WaitForSingleObject(lphtTable->hSpinLock, INFINITE_WAIT_TIME);}
	

        if (lphtTable->ulFlags & HANDLE_TABLE_HIGH_PERFORMANCE)
        { ullId = (ULONGLONG)VectorPushBack((HVECTOR)lphtTable->lpHandleRegistrar, &hHandle, NULL); }
        else if (lphtTable->ulFlags & HANDLE_TABLE_MEMORY_CONCIOUS)
        {  
                HANDLE_TABLE_NODE htnNode;
                htnNode.hHandle = hHandle;
                htnNode.ullId = lphtTable->ullNextId;
                lphtTable->ullNextId++;
                LinkedListAttachBack((HLINKEDLIST)lphtTable->lpHandleRegistrar, &htnNode);
        }
	else if (lphtTable->ulFlags & HANDLE_TABLE_ULTRA_PERFORMANCE)
	{
		LPVOID lpDer;
		lpDer = OBJECT_CAST(hHandle, LPVOID);

		HashTableInsertEx(
			lphtTable->lpHandleRegistrar,
			(ULONGLONG)lpDer,
			sizeof(ULONGLONG),
			FALSE,
			(ULONGLONG)hHandle
		);
		
		ullId = (ULONGLONG)HashTableGetKey(
			lphtTable->lpHandleRegistrar,
			(ULONGLONG)lpDer,
			sizeof(ULONGLONG),
			FALSE
		);
	}
	else if (lphtTable->ulFlags & HANDLE_TABLE_ULTRA_PERFORMANCE_EX)
	{
		if (NULLPTR != lphtTable->hSpinLock)
		{ ReleaseSingleObject(lphtTable->hSpinLock); }
		return 0;
	}
	else if (lphtTable->ulFlags & HANDLE_TABLE_PERFORMANT_AND_CONCIOUS)
	{
		HANDLE_TABLE_NODE htnData;
		ZeroMemory(&htnData, sizeof(HANDLE_TABLE_NODE));

		htnData.hHandle = hHandle;
		htnData.ullId = (ULONGLONG)OBJECT_CAST(hHandle, LPVOID);
		BinarySearchTreeInsert(
			lphtTable->lpHandleRegistrar,
			&htnData,
			sizeof(HANDLE_TABLE_NODE)
		);
	}
	else if (lphtTable->ulFlags & HANDLE_TABLE_PERFORMANT_AND_CONCIOUS_EX)
	{
		if (NULLPTR != lphtTable->hSpinLock)
		{ ReleaseSingleObject(lphtTable->hSpinLock); }
		return 0;
	}

        lphtTable->ullNumberOfHandles++;
        if (NULLPTR != lphtTable->hSpinLock)
        { ReleaseSingleObject(lphtTable->hSpinLock); }
        return (ullId + 1ULL);
}




_Success_(return != NULL, ...)
PODNET_API
ULONGLONG
RegisterHandleEx(
        _In_                            HANDLE                  hHandleTable,
        _In_                            HANDLE                  hHandle,
	_In_ 				ULONGLONG 		ullKey
){
        LPHANDLE_TABLE lphtTable;
        ULONGLONG ullId;

        lphtTable = (LPHANDLE_TABLE)GetHandleDerivative(hHandleTable);
	EXIT_IF_UNLIKELY_NULL(lphtTable, 0);

        ullId = 0;
        if (NULLPTR != lphtTable->hSpinLock)
	{ WaitForSingleObject(lphtTable->hSpinLock, INFINITE_WAIT_TIME);}
	

        if (lphtTable->ulFlags & HANDLE_TABLE_HIGH_PERFORMANCE)
        { ullId = (ULONGLONG)VectorPushBack((HVECTOR)lphtTable->lpHandleRegistrar, &hHandle, NULL); }
        else if (lphtTable->ulFlags & HANDLE_TABLE_MEMORY_CONCIOUS)
        {  
                HANDLE_TABLE_NODE htnNode;
                htnNode.hHandle = hHandle;
                htnNode.ullId = lphtTable->ullNextId;
                lphtTable->ullNextId++;
                LinkedListAttachBack((HLINKEDLIST)lphtTable->lpHandleRegistrar, &htnNode);
        }
	else if (lphtTable->ulFlags & HANDLE_TABLE_ULTRA_PERFORMANCE)
	{
		LPVOID lpDer;
		lpDer = OBJECT_CAST(hHandle, LPVOID);

		HashTableInsertEx(
			lphtTable->lpHandleRegistrar,
			(ULONGLONG)lpDer,
			sizeof(ULONGLONG),
			FALSE,
			(ULONGLONG)hHandle
		);
		
		ullId = (ULONGLONG)HashTableGetKey(
			lphtTable->lpHandleRegistrar,
			(ULONGLONG)lpDer,
			sizeof(ULONGLONG),
			FALSE
		);
	}
	else if (lphtTable->ulFlags & HANDLE_TABLE_ULTRA_PERFORMANCE_EX)
	{
		HashTableInsertEx(
			lphtTable->lpHandleRegistrar,
			ullKey,
			sizeof(ULONGLONG),
			FALSE,
			(ULONGLONG)hHandle
		);

		ullId = HashTableGetKey(
			lphtTable->lpHandleRegistrar,
			ullKey,
			sizeof(ULONGLONG),
			FALSE
		);
	}
	else if (lphtTable->ulFlags & HANDLE_TABLE_PERFORMANT_AND_CONCIOUS)
	{
		HANDLE_TABLE_NODE htnData;
		ZeroMemory(&htnData, sizeof(HANDLE_TABLE_NODE));

		htnData.hHandle = hHandle;
		htnData.ullId = (ULONGLONG)OBJECT_CAST(hHandle, LPVOID);
		BinarySearchTreeInsert(
			lphtTable->lpHandleRegistrar,
			&htnData,
			sizeof(HANDLE_TABLE_NODE)
		);
	}
	else if (lphtTable->ulFlags & HANDLE_TABLE_PERFORMANT_AND_CONCIOUS_EX)
	{
		HANDLE_TABLE_NODE htnData;
		ZeroMemory(&htnData, sizeof(HANDLE_TABLE_NODE));

		htnData.hHandle = hHandle;
		htnData.ullId = ullKey;
		BinarySearchTreeInsert(
			lphtTable->lpHandleRegistrar,
			&htnData,
			sizeof(HANDLE_TABLE_NODE)
		);
	}


        lphtTable->ullNumberOfHandles++;
        if (NULLPTR != lphtTable->hSpinLock)
        { ReleaseSingleObject(lphtTable->hSpinLock); }
        return (ullId + 1ULL);
}




_Success_(return != FALSE, ...)
PODNET_API
BOOL
DeregisterHandle(
        _In_                            HANDLE                  hHandleTable,
        _In_                            HANDLE                  hHandle
){
        LPHANDLE_TABLE lphtTable;
        HANDLE hCurrent;
        ULONGLONG ullIndex;

        lphtTable = (LPHANDLE_TABLE)GetHandleDerivative(hHandleTable);
	EXIT_IF_UNLIKELY_NULL(lphtTable, FALSE);

        if (NULLPTR != lphtTable->hSpinLock)
	{ WaitForSingleObject(lphtTable->hSpinLock, INFINITE_WAIT_TIME);}

        if (lphtTable->ulFlags & HANDLE_TABLE_HIGH_PERFORMANCE)
        {
                for (
                        ullIndex = 0; 
                        ullIndex < VectorSize((HVECTOR)lphtTable->lpHandleRegistrar); 
                        ullIndex++
                ){
                        hCurrent = *(LPHANDLE)VectorAt((HVECTOR)lphtTable->lpHandleRegistrar, ullIndex);
                        if (hCurrent == hHandle)
                        { 
                                *(LPHANDLE)VectorAt((HVECTOR)lphtTable->lpHandleRegistrar, ullIndex) = NULLPTR; 
                                JUMP(FoundAndRemoved);
                        }
                }
        }
        else if (lphtTable->ulFlags & HANDLE_TABLE_MEMORY_CONCIOUS)
        {
                LPVOID lpNode;
                LPHANDLE_TABLE_NODE lphtnNode;
                for (
                        lpNode = LinkedListTraverse((HLINKEDLIST)lphtTable->lpHandleRegistrar, 0);
                        NULLPTR != lpNode;
                        lpNode = LinkedListTraverse((HLINKEDLIST)lphtTable->lpHandleRegistrar, lpNode)
                ){
                        lphtnNode = (LPHANDLE_TABLE_NODE)LinkedListAtNode((HLINKEDLIST)lphtTable->lpHandleRegistrar, lpNode);
                        if (NULLPTR != lphtnNode)
                        {
                                if (lphtnNode->hHandle == hHandle)
                                {
                                        LinkedListRemoveAtNode((HLINKEDLIST)lphtTable->lpHandleRegistrar, lpNode, TRUE);
                                        JUMP(FoundAndRemoved);
                                }
                        }
                }
        }
	else if (lphtTable->ulFlags & HANDLE_TABLE_ULTRA_PERFORMANCE)
	{
		LPVOID lpDer;
		UARCHLONG ualKey;

		lpDer = OBJECT_CAST(hHandle, LPVOID);

		ualKey = HashTableGetKey(
			lphtTable->lpHandleRegistrar,
			(ULONGLONG)lpDer,
			sizeof(ULONGLONG),
			FALSE
		);

		if ((UARCHLONG)-1 == ualKey)
		{ JUMP(NotFound); }

		HashTableInsertEx(
			lphtTable->lpHandleRegistrar,
			(ULONGLONG)lpDer,
			sizeof(ULONGLONG),
			FALSE,
			(ULONGLONG)NULL_OBJECT
		);

		JUMP(FoundAndRemoved);

	}
	else if (lphtTable->ulFlags & HANDLE_TABLE_ULTRA_PERFORMANCE_EX)
	{ JUMP(NotFound); }
	else if (lphtTable->ulFlags & HANDLE_TABLE_PERFORMANT_AND_CONCIOUS)
	{
		HANDLE_TABLE_NODE htnQuery;
		LPVOID lpNode;

		ZeroMemory(&htnQuery, sizeof(HANDLE_TABLE_NODE));
		lpNode = NULLPTR;

		htnQuery.hHandle = hHandle;
		htnQuery.ullId = (ULONGLONG)OBJECT_CAST(hHandle, LPVOID);

		lpNode = BinarySearchTreeSearch(
			lphtTable->lpHandleRegistrar,
			&htnQuery,
			sizeof(HANDLE_TABLE_NODE)
		);

		if (NULLPTR == lpNode)
		{ JUMP(NotFound); }

		BinarySearchTreeDestroyAt(
			lphtTable->lpHandleRegistrar,
			lpNode
		);
	}
	else if (lphtTable->ulFlags & HANDLE_TABLE_PERFORMANT_AND_CONCIOUS_EX)
	{ JUMP(NotFound); }

	NotFound:
        ReleaseSpinLock(lphtTable->hSpinLock);
        return FALSE;

        FoundAndRemoved:
        lphtTable->ullNumberOfHandles--;
        ReleaseSpinLock(lphtTable->hSpinLock);
        return TRUE;
}




_Success_(return != FALSE, ...)
PODNET_API
BOOL
DeregisterHandleEx(
        _In_                            HANDLE                  hHandleTable,
        _In_                            HANDLE                  hHandle,
	_In_				ULONGLONG 		ullKey
){
        LPHANDLE_TABLE lphtTable;
        HANDLE hCurrent;
        ULONGLONG ullIndex;

        lphtTable = (LPHANDLE_TABLE)GetHandleDerivative(hHandleTable);
	EXIT_IF_UNLIKELY_NULL(lphtTable, FALSE);

        if (NULLPTR != lphtTable->hSpinLock)
	{ WaitForSingleObject(lphtTable->hSpinLock, INFINITE_WAIT_TIME);}

        if (lphtTable->ulFlags & HANDLE_TABLE_HIGH_PERFORMANCE)
        {
                for (
                        ullIndex = 0; 
                        ullIndex < VectorSize((HVECTOR)lphtTable->lpHandleRegistrar); 
                        ullIndex++
                ){
                        hCurrent = *(LPHANDLE)VectorAt((HVECTOR)lphtTable->lpHandleRegistrar, ullIndex);
                        if (hCurrent == hHandle)
                        { 
                                *(LPHANDLE)VectorAt((HVECTOR)lphtTable->lpHandleRegistrar, ullIndex) = NULLPTR; 
                                JUMP(FoundAndRemoved);
                        }
                }
        }
        else if (lphtTable->ulFlags & HANDLE_TABLE_MEMORY_CONCIOUS)
        {
                LPVOID lpNode;
                LPHANDLE_TABLE_NODE lphtnNode;
                for (
                        lpNode = LinkedListTraverse((HLINKEDLIST)lphtTable->lpHandleRegistrar, 0);
                        NULLPTR != lpNode;
                        lpNode = LinkedListTraverse((HLINKEDLIST)lphtTable->lpHandleRegistrar, lpNode)
                ){
                        lphtnNode = (LPHANDLE_TABLE_NODE)LinkedListAtNode((HLINKEDLIST)lphtTable->lpHandleRegistrar, lpNode);
                        if (NULLPTR != lphtnNode)
                        {
                                if (lphtnNode->hHandle == hHandle)
                                {
                                        LinkedListRemoveAtNode((HLINKEDLIST)lphtTable->lpHandleRegistrar, lpNode, TRUE);
                                        JUMP(FoundAndRemoved);
                                }
                        }
                }
        }
	else if (lphtTable->ulFlags & HANDLE_TABLE_ULTRA_PERFORMANCE)
	{
		LPVOID lpDer;
		UARCHLONG ualKey;

		lpDer = OBJECT_CAST(hHandle, LPVOID);

		ualKey = HashTableGetKey(
			lphtTable->lpHandleRegistrar,
			(ULONGLONG)lpDer,
			sizeof(ULONGLONG),
			FALSE
		);

		if ((UARCHLONG)-1 == ualKey)
		{ JUMP(NotFound); }

		HashTableInsertEx(
			lphtTable->lpHandleRegistrar,
			(ULONGLONG)lpDer,
			sizeof(ULONGLONG),
			FALSE,
			(ULONGLONG)NULL_OBJECT
		);

		JUMP(FoundAndRemoved);

	}
	else if (lphtTable->ulFlags & HANDLE_TABLE_ULTRA_PERFORMANCE_EX)
	{
		UARCHLONG ualKey;
		ualKey = HashTableGetKey(
			lphtTable->lpHandleRegistrar,
			ullKey,
			sizeof(ULONGLONG),
			FALSE
		);

		if ((UARCHLONG)-1 == ualKey)
		{ JUMP(NotFound); }

		HashTableInsertEx(
			lphtTable->lpHandleRegistrar,
			ullKey,
			sizeof(ULONGLONG),
			FALSE,
			(ULONGLONG)NULL_OBJECT
		);
	}
	else if (lphtTable->ulFlags & HANDLE_TABLE_PERFORMANT_AND_CONCIOUS)
	{
		HANDLE_TABLE_NODE htnQuery;
		LPVOID lpNode;

		ZeroMemory(&htnQuery, sizeof(HANDLE_TABLE_NODE));
		lpNode = NULLPTR;

		htnQuery.hHandle = hHandle;
		htnQuery.ullId = (ULONGLONG)OBJECT_CAST(hHandle, LPVOID);

		lpNode = BinarySearchTreeSearch(
			lphtTable->lpHandleRegistrar,
			&htnQuery,
			sizeof(HANDLE_TABLE_NODE)
		);

		if (NULLPTR == lpNode)
		{ JUMP(NotFound); }

		BinarySearchTreeDestroyAt(
			lphtTable->lpHandleRegistrar,
			lpNode
		);
	}
	else if (lphtTable->ulFlags & HANDLE_TABLE_PERFORMANT_AND_CONCIOUS_EX)
	{
		HANDLE_TABLE_NODE htnQuery;
		LPVOID lpNode;

		ZeroMemory(&htnQuery, sizeof(HANDLE_TABLE_NODE));
		lpNode = NULLPTR;

		htnQuery.hHandle = hHandle;
		htnQuery.ullId = ullKey;

		lpNode = BinarySearchTreeSearch(
			lphtTable->lpHandleRegistrar,
			&htnQuery,
			sizeof(HANDLE_TABLE_NODE)
		);

		if (NULLPTR == lpNode)
		{ JUMP(NotFound); }

		BinarySearchTreeDestroyAt(
			lphtTable->lpHandleRegistrar,
			lpNode
		);
	}


	NotFound:
        ReleaseSpinLock(lphtTable->hSpinLock);
        return FALSE;

        FoundAndRemoved:
        lphtTable->ullNumberOfHandles--;
        ReleaseSpinLock(lphtTable->hSpinLock);
        return TRUE;
}




_Success_(return != NULL, ...)
PODNET_API
HANDLE 
GrabHandle(
        _In_                            HANDLE                  hHandleTable,
        _In_                            ULONGLONG               ullId
){
        LPHANDLE_TABLE lphtTable;
        HANDLE hReturn;

        lphtTable = (LPHANDLE_TABLE)GetHandleDerivative(hHandleTable);
	EXIT_IF_UNLIKELY_NULL(lphtTable, NULL_OBJECT);
	hReturn = NULLPTR;
 
       	WaitForSingleObject(lphtTable->hSpinLock, INFINITE_WAIT_TIME);
        
	if (lphtTable->ulFlags & HANDLE_TABLE_HIGH_PERFORMANCE)
	{ hReturn = *(LPHANDLE)VectorAt((HVECTOR)lphtTable->lpHandleRegistrar, ullId - 1ULL); }
	else if (lphtTable->ulFlags & HANDLE_TABLE_MEMORY_CONCIOUS)
	{
		LPVOID lpNode;
		LPHANDLE_TABLE_NODE lphtnNode;
		for (
			lpNode = LinkedListTraverse((HLINKEDLIST)lphtTable->lpHandleRegistrar, 0);
			NULLPTR != lpNode;
			lpNode = LinkedListTraverse((HLINKEDLIST)lphtTable->lpHandleRegistrar, lpNode)
		){
			lphtnNode = (LPHANDLE_TABLE_NODE)LinkedListAtNode((HLINKEDLIST)lphtTable->lpHandleRegistrar, lpNode);
			if (NULLPTR != lphtnNode)
			{
				if (lphtnNode->ullId == ullId)
				{ 
					hReturn = lphtnNode->hHandle; 
					JUMP(Found);
				}
			}
		}
	}
	else if (lphtTable->ulFlags & HANDLE_TABLE_ULTRA_PERFORMANCE)
	{
		HashTableGetValueEx(
			lphtTable->lpHandleRegistrar,
			(UARCHLONG)ullId,
			NULLPTR,
			NULLPTR,
			(LPULONGLONG)&hReturn
		);
	}
	else if (lphtTable->ulFlags & HANDLE_TABLE_ULTRA_PERFORMANCE_EX)
	{
		HashTableGetValueEx(
			lphtTable->lpHandleRegistrar,
			(UARCHLONG)ullId,
			NULLPTR,
			NULLPTR,
			(LPULONGLONG)&hReturn
		);
	}
	
Found:
        ReleaseSpinLock(lphtTable->hSpinLock);
        return hReturn;
}




_Success_(return != NULLPTR, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Spin_Lock_)))
PODNET_API
HVECTOR
GrabAllHandles(
        _In_                            HANDLE                  hHandleTable
){
        EXIT_IF_UNLIKELY_NULL(hHandleTable, NULLPTR);
        EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hHandleTable), HANDLE_TYPE_HANDLE_TABLE, NULLPTR);
        LPHANDLE_TABLE lphtTable;
        HVECTOR hvOut;
        UARCHLONG ualNumberOfHandles;
        lphtTable = (LPHANDLE_TABLE)GetHandleDerivative(hHandleTable);
        EXIT_IF_UNLIKELY_NULL(lphtTable, NULLPTR);
        hvOut = NULLPTR;
        ualNumberOfHandles = NULL;

        WaitForSingleObject(lphtTable->hSpinLock, INFINITE_WAIT_TIME);

        if (lphtTable->ulFlags & HANDLE_TABLE_HIGH_PERFORMANCE)
        {
                HANDLE hTemp;
                ualNumberOfHandles = VectorSize((HVECTOR)lphtTable->lpHandleRegistrar);                
                if (0 == ualNumberOfHandles)
                {
                        ReleaseSingleObject(lphtTable->hSpinLock);
                        return NULLPTR;
                }

                hvOut = CreateVector(ualNumberOfHandles, sizeof(HANDLE), NULL);
                if (NULLPTR == hvOut)
                {
                        ReleaseSingleObject(lphtTable->hSpinLock);
                        return NULLPTR;
                }                

                for (
                        hTemp = NULLPTR;
                        ualNumberOfHandles != 0;
                        ualNumberOfHandles--
                ){
                        hTemp = *(LPHANDLE)VectorAt((HVECTOR)lphtTable->lpHandleRegistrar, ualNumberOfHandles - 1);
                        if (NULLPTR != hTemp)
                        { VectorPushBack(hvOut, &hTemp, NULL); }
                }

                VectorShrinkToFit(hvOut);
        	ReleaseSingleObject(lphtTable->hSpinLock);
        	return hvOut;
        }

        else if (lphtTable->ulFlags & HANDLE_TABLE_MEMORY_CONCIOUS)
        {

        }

        ReleaseSingleObject(lphtTable->hSpinLock);
        return hvOut;
}




_Success_(return != NULL, ...)
PODNET_API
HANDLE
GrabHandleByDerivative(
        _In_                            HANDLE                          hHandleTable,
        _In_                            HDERIVATIVE                     hDerivative,
        _In_                            ULONGLONG                       ullSizeOfDerivativeObject,
        _In_Opt_                        LPFN_HDERIVATIVE_COMPARE_PROC   lpfnDerivativeCompare
){
        UNREFERENCED_PARAMETER(ullSizeOfDerivativeObject);
        LPHANDLE_TABLE lphtTable;
        HANDLE hReturn;
        ULONGLONG ullIndex;
        HVECTOR hvRegistrar;
        
        lphtTable = (LPHANDLE_TABLE)GetHandleDerivative(hHandleTable);
	EXIT_IF_UNLIKELY_NULL(lphtTable, NULL_OBJECT);
        hReturn = NULLPTR;
        hvRegistrar = (HVECTOR)lphtTable->lpHandleRegistrar;
        WaitForSingleObject(lphtTable->hSpinLock, INFINITE_WAIT_TIME);
        
	if (lphtTable->ulFlags & HANDLE_TABLE_HIGH_PERFORMANCE)
	{
		HANDLE hTemp;
	        if (lpfnDerivativeCompare != NULLPTR)
	        {
	                for (ullIndex = 0; ullIndex < VectorSize(hvRegistrar); ullIndex++)
         	       	{
        	                hTemp = *(LPHANDLE)VectorAt(hvRegistrar, ullIndex);
	                        if (TRUE == lpfnDerivativeCompare(hDerivative, GetHandleDerivative(hTemp)))
                	        {
					hReturn = hTemp;
					JUMP(Found);
				}
        	        }
	        }
        	else
	        {
                	for (ullIndex = 0; ullIndex < VectorSize(hvRegistrar); ullIndex++)
        	        {
	                        hTemp = *(LPHANDLE)VectorAt(hvRegistrar, ullIndex);
	                        if (GetHandleDerivative(hTemp) == hDerivative)
                        	{
					hReturn = hTemp;
			 		JUMP(Found); 
				}
                	}
        	}

	}
	
	else if (lphtTable->ulFlags & HANDLE_TABLE_MEMORY_CONCIOUS)
	{
		if (lpfnDerivativeCompare != NULLPTR)
		{
			LPVOID lpNode;
			LPHANDLE_TABLE_NODE lphtnNode;
			for (
				lpNode = LinkedListTraverse((HLINKEDLIST)lphtTable->lpHandleRegistrar, 0);
				NULLPTR != lpNode;
				lpNode = LinkedListTraverse((HLINKEDLIST)lphtTable->lpHandleRegistrar, lpNode)
			){
				lphtnNode = (LPHANDLE_TABLE_NODE)LinkedListAtNode((HLINKEDLIST)lphtTable->lpHandleRegistrar, lpNode);
				if (TRUE == lpfnDerivativeCompare(hDerivative, GetHandleDerivative(lphtnNode->hHandle)))
				{
					hReturn = lphtnNode->hHandle; 
					JUMP(Found); 
				}
			}
		}
		else
		{
			LPVOID lpNode;
			LPHANDLE_TABLE_NODE lphtnNode;
			for (
				lpNode = LinkedListTraverse((HLINKEDLIST)lphtTable->lpHandleRegistrar, 0);
				NULLPTR != lpNode;
				lpNode = LinkedListTraverse((HLINKEDLIST)lphtTable->lpHandleRegistrar, lpNode)
			){
				lphtnNode = (LPHANDLE_TABLE_NODE)LinkedListAtNode((HLINKEDLIST)lphtTable->lpHandleRegistrar, lpNode);
				if (GetHandleDerivative(hReturn) == hDerivative)
				{
					hReturn = lphtnNode->hHandle;		
					JUMP(Found); 
				}
			}
		}
	}
	else if (lphtTable->ulFlags & HANDLE_TABLE_ULTRA_PERFORMANCE)
	{
		UARCHLONG ualKey;
		ualKey = HashTableGetKey(
			lphtTable->lpHandleRegistrar,
			(ULONGLONG)hDerivative,
			sizeof(ULONGLONG),
			FALSE
		);

		if ((UARCHLONG)-1 == ualKey)
		{ 
			ReleaseSingleObject(lphtTable->hSpinLock);
			return NULL_OBJECT;
		}

		HashTableGetValueEx(
			lphtTable->lpHandleRegistrar,
			ualKey,
			NULLPTR,
			NULLPTR,
			(LPULONGLONG)&hReturn
		);
	}
	else if (lphtTable->ulFlags & HANDLE_TABLE_ULTRA_PERFORMANCE_EX)
	{
		UARCHLONG ualKey;
		ualKey = HashTableGetKey(
			lphtTable->lpHandleRegistrar,
			(ULONGLONG)hDerivative,
			sizeof(ULONGLONG),
			FALSE
		);

		if ((UARCHLONG)-1 == ualKey)
		{ 
			ReleaseSingleObject(lphtTable->hSpinLock);
			return NULL_OBJECT;
		}

		HashTableGetValueEx(
			lphtTable->lpHandleRegistrar,
			ualKey,
			NULLPTR,
			NULLPTR,
			(LPULONGLONG)&hReturn
		);
	}
	else if (lphtTable->ulFlags & HANDLE_TABLE_PERFORMANT_AND_CONCIOUS)
	{
		LPHANDLE_TABLE_NODE lphtnData;
		HANDLE_TABLE_NODE htnQuery;
		LPVOID lpNode;

		ZeroMemory(&htnQuery, sizeof(HANDLE_TABLE_NODE));
		lpNode = NULLPTR;

		htnQuery.hHandle = NULL_OBJECT;
		htnQuery.ullId = (ULONGLONG)hDerivative;

		lpNode = BinarySearchTreeSearch(
			lphtTable->lpHandleRegistrar,
			&htnQuery,
			sizeof(HANDLE_TABLE_NODE)
		);

		if (NULLPTR == lpNode)
		{
			ReleaseSingleObject(lphtTable->hSpinLock);
			return NULL_OBJECT;
		}

		lphtnData = BinarySearchTreeAt(
			lphtTable->lpHandleRegistrar,
			lpNode
		);
		
		hReturn = lphtnData->hHandle;
	}
	else if (lphtTable->ulFlags & HANDLE_TABLE_PERFORMANT_AND_CONCIOUS_EX)
	{
		LPHANDLE_TABLE_NODE lphtnData;
		HANDLE_TABLE_NODE htnQuery;
		LPVOID lpNode;

		ZeroMemory(&htnQuery, sizeof(HANDLE_TABLE_NODE));
		lpNode = NULLPTR;

		htnQuery.hHandle = NULL_OBJECT;
		htnQuery.ullId = (ULONGLONG)hDerivative;

		lpNode = BinarySearchTreeSearch(
			lphtTable->lpHandleRegistrar,
			&htnQuery,
			sizeof(HANDLE_TABLE_NODE)
		);

		if (NULLPTR == lpNode)
		{
			ReleaseSingleObject(lphtTable->hSpinLock);
			return NULL_OBJECT;
		}

		lphtnData = BinarySearchTreeAt(
			lphtTable->lpHandleRegistrar,
			lpNode
		);
		
		hReturn = lphtnData->hHandle;
	}

        
        Found:
        ReleaseSpinLock(lphtTable->hSpinLock);
        return hReturn;
}
