/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CHANDLEOBJECT_C
#define CHANDLEOBJECT_C 




#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "CHandleFunctionPointers.h"

  


typedef struct __OBJECT
{
	LPHDERIVATIVE			lphDerivatives;
	LPULONG				lpulDerivativeTypes;
	ULONG				ulNumberOfDerivatives;
	LPULONGLONG			lpullIndexes;
	DLPFN_HANDLE_DESTROY		dlpfnDestroyers;
	LPULONG				lpulDestroyFlags;
	DLPFN_HANDLE_ALARM		dlpfnAlarmHandlers;
	LPULONG				lpulAlarmFlags;
	DLPFN_HANDLE_ERROR		dlpfnErrorHandlers;
	LPULONG				lpulErrorFlags;
	DLPFN_HANDLE_EVENT_SET		dlpfnEventSetHandlers;
	LPULONG				lpulEventSetFlags;
	DLPFN_HANDLE_EVENT_WAIT		dlpfnEventWaitHandlers;
	LPULONG				lpulEventWaitFlags;
	DLPFN_HANDLE_RELEASE		dlpfnReleaseHandlers;
	LPFN_LOCK_PROC			lpfnLockProc;
	LPFN_UNLOCK_PROC		lpfnUnlockProc;
	LPFN_TRY_LOCK_PROC		lpfnTryLockProc;
	HANDLE				hCritSec;
	LPHANDLE			lphEvents;
	ULONG				ulNumberOfEvents;
	LPFN_HANDLE_WRITE_PROC 		lpfnWriteProc;
	LPFN_HANDLE_READ_PROC		lpfnReadProc;
	UARCHLONG 			ualReferenceCount;
} OBJECT, *HANDLE, **LPHANDLE, ***DLPHANDLE;



#endif
