/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CHANDLEFUNCTIONPOINTERS_H
#define CHANDLEFUNCTIONPOINTERS_H


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"


#ifndef __HANDLE_DEFINED__
#define __HANDLE_DEFINED__ 		1
typedef struct __OBJECT 		*HANDLE, **LPHANDLE, ***DLPHANDLE;
typedef LPVOID				HDERIVATIVE, *LPHDERIVATIVE, **DLPHDERIVATIVE;
typedef LPVOID				HBASEOBJ, *LPHBASEOBJ, **DLPHBASEOBJ;
#endif



typedef _Call_Back_ BOOL(* LPFN_HANDLE_DESTROY)(
	_In_		HDERIVATIVE		hDerivative,
	_In_Opt_	ULONG			ulFlags
);


typedef LPFN_HANDLE_DESTROY		*DLPFN_HANDLE_DESTROY, **TLPFN_HANDLE_DESTROY;




typedef _Call_Back_ BOOL(* LPFN_HANDLE_ERROR)(
	_In_		HANDLE 			hObject,
	_In_ 		LPVOID 			lpMoreInfo
);


typedef LPFN_HANDLE_ERROR		*DLPFN_HANDLE_ERROR, **TLPFN_HANDLE_ERROR;




typedef _Call_Back_ BOOL(* LPFN_HANDLE_ALARM)(
	_In_		HANDLE 			hObject,
	_In_Opt_	LPVOID			lpMoreInfo
);


typedef LPFN_HANDLE_ALARM		*DLPFN_HANDLE_ALARM, **TLPFN_HANDLE_ALARM;




typedef _Call_Back_ BOOL(* LPFN_HANDLE_EVENT_SET)(
	_In_		HANDLE			hBase,
	_In_Opt_	LPVOID			lpParam,
	_In_Opt_	ULONG			ulFlags
);


typedef LPFN_HANDLE_EVENT_SET		*DLPFN_HANDLE_EVENT_SET, **TLPFN_HANDLE_EVENT_SET;




typedef _Call_Back_ BOOL(* LPFN_HANDLE_EVENT_WAIT)(
	_In_		HANDLE			hBase,
	_In_Opt_	ULONGLONG		ullMilliSeconds
);


typedef LPFN_HANDLE_EVENT_WAIT		*DLPFN_HANDLE_EVENT_WAIT, **TLPFN_HANDLE_EVENT_WAIT;




typedef _Call_Back_ BOOL(* LPFN_HANDLE_RELEASE)(
	_In_		HANDLE			hBase
);


typedef LPFN_HANDLE_RELEASE		*DLPFN_HANDLE_RELEASE, **TLPFN_HANDLE_RELEASE;




typedef _Call_Back_ LPVOID(* LPFN_HANDLE_CUSTOM_PROC)(
	_In_		HANDLE			hBase,
	_In_Opt_	HDERIVATIVE		hDerivative,
	_In_Opt_	LPVOID			lpParam,
	_In_Opt_	ULONG			ulFlags
);


typedef LPFN_HANDLE_CUSTOM_PROC	*DLPFN_HANDLE_CUSTOM_PROC, **TLPFN_HANDLE_CUSTOM_PROC;




typedef _Call_Back_ BOOL(* LPFN_LOCK_PROC)(
	_In_		HANDLE			hBase
);


typedef LPFN_LOCK_PROC *DLPFN_LOCK_PROC, **TLPFN_LOCK_PROC;




typedef _Call_Back_ BOOL(* LPFN_UNLOCK_PROC)(
	_In_		HANDLE			hBase
);


typedef LPFN_UNLOCK_PROC *DLPFN_UNLOCK_PROC, **TLPFN_UNLOCK_PROC;




typedef _Call_Back_ BOOL(* LPFN_TRY_LOCK_PROC)(
	_In_		HANDLE			hBase
);


typedef LPFN_TRY_LOCK_PROC *DLPFN_TRY_LOCK_PROC, **TLPFN_TRY_LOCK_PROC;




typedef _Call_Back_ LPVOID(* LPFN_HANDLE_WRITE_PROC)(
	_In_ 		HANDLE 			hFile,
	_In_ 		LPVOID 			lpData,
	_In_ 		ULONGLONG 		ullSize,
	_In_Opt_ 	LPVOID 			lpParam,
	_In_Opt_ 	ULONG 			ulFlags 
);


typedef LPFN_HANDLE_WRITE_PROC *DLPFN_HANDLE_WRITE_PROC, **TLPFN_HANDLE_WRITE_PROC;




typedef _Call_Back_ LPVOID(* LPFN_HANDLE_READ_PROC)(
	_In_ 		HANDLE 			hFile,
	_In_ 		LPVOID 			lpBuffer,
	_In_ 		ULONGLONG  		ullSize,
	_In_Opt_ 	LPVOID 			lpParam,
	_In_Opt_ 	ULONG 			ulFlags 
);


typedef LPFN_HANDLE_READ_PROC *DLPFN_HANDLE_READ_PROC, **TLPFN_HANDLE_READ_PROC;




#endif