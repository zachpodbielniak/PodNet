/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CWAIT_H
#define CWAIT_H


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "CHandle.h"





_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
SetHandleEventWaitFunction(
	_In_		HANDLE			hHandle,
	_In_		LPFN_HANDLE_EVENT_WAIT  lpfnWait,
	_In_Opt_	ULONG			ulFlags
);




_Calls_Call_Back_
_Success_(return != FALSE, _Inexpressible_ && _Maybe_(_Interlocked_))
PODNET_API
BOOL
WaitForSingleObject(
	_In_		HANDLE			hHandle,
	_In_Opt_	ULONGLONG		ullMilliSeconds
);




_Calls_Call_Back_
_Success_(return != FALSE, _Inexpressible_ && _Maybe_(_Interlocked_))
PODNET_API
BOOL
WaitForMultipleObjects(
	_In_		LPHANDLE		lphHandles,
	_In_		ULONGLONG		ullNumberOfObjects,
	_In_Opt_	ULONGLONG		ullMilliSeconds
);


#endif