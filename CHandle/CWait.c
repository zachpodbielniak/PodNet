/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CWait.h"
#include "CHandleObject.c"




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
SetHandleEventWaitFunction(
	_In_		HANDLE			hHandle,
	_In_		LPFN_HANDLE_EVENT_WAIT  lpfnWait,
	_In_Opt_	ULONG			ulFlags
){
	EXIT_IF_UNLIKELY_NULL(hHandle, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpfnWait, FALSE);

	if (hHandle->dlpfnEventWaitHandlers == NULLPTR)
	{ 
		hHandle->dlpfnEventWaitHandlers = LocalAlloc(sizeof(HANDLE)); 
		EXIT_IF_UNLIKELY_NULL(hHandle->dlpfnEventWaitHandlers, FALSE);
	}
	hHandle->dlpfnEventWaitHandlers[0] = lpfnWait;

	if (hHandle->lpulEventWaitFlags == NULLPTR)
	{ 
		hHandle->lpulEventWaitFlags = LocalAlloc(sizeof(ULONG)); 
		EXIT_IF_UNLIKELY_NULL(hHandle->lpulEventWaitFlags, FALSE);
	}
	hHandle->lpulEventWaitFlags[0] = ulFlags;

	return TRUE;
}




_Calls_Call_Back_
_Success_(return != FALSE, _Inexpressible_ && _Maybe_(_Interlocked_))
PODNET_API
BOOL
WaitForSingleObject(
	HANDLE			hHandle,
	ULONGLONG		ullMilliSeconds
){
	EXIT_IF_UNLIKELY_NULL(hHandle, FALSE);
	BOOL bRet;
	bRet = FALSE;
	
        /* Default Implementation if Undefined Function Pointer */
	
	if (NULLPTR == hHandle->dlpfnEventWaitHandlers)
	{
		TIMESPEC tsClock;
		HighResolutionClock(&tsClock);
		while (HighResolutionClockAndGetMilliSeconds(&tsClock) < ullMilliSeconds)
		{
			if (TRUE == GetEvent(hHandle->lphEvents[0]))
			{ JUMP(DoneWaiting); }
			Sleep(5); /* Allows for up to 200 fps */
		}	
	}
	else
	{ bRet = hHandle->dlpfnEventWaitHandlers[0](hHandle, ullMilliSeconds); }

DoneWaiting:
	return bRet;
}




_Calls_Call_Back_
_Success_(return != FALSE, _Inexpressible_ && _Maybe_(_Interlocked_))
PODNET_API
BOOL
WaitForMultipleObjects(
	_In_		LPHANDLE		lphHandles,
	_In_		ULONGLONG	        ullNumberOfObjects,
	_In_Opt_	ULONGLONG		ullMilliSeconds
){
	EXIT_IF_UNLIKELY_NULL(lphHandles, FALSE);
	ULONGLONG ullIndex;
	BOOL bRet;
	bRet = TRUE;

	for (
		ullIndex = 0;
		ullIndex < ullNumberOfObjects;
		ullIndex++
	){ bRet &= WaitForSingleObject(lphHandles[ullIndex], ullMilliSeconds); }
	
        return bRet;
}
