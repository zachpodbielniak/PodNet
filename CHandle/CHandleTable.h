/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CHANDLETABLE_H
#define CHANDLETABLE_H


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../CompilationFlags.h"
#include "CHandle.h"
#include "../CContainers/CContainers.h"
#include "../CContainers/CHashTable.h"
#include "../CContainers/CBinarySearchTree.h"
#include "../CLock/CSpinLock.h"



typedef enum __HANDLE_TABLE_FLAGS
{
        /* 
                In HIGH_PERFORMANCE mode, the HANDLE_TABLE stores
                the registered HANDLEs in a VECTOR. When in
                this mode, it is really fast to add a new
                HANDLE to this HANDLE_TABLE. But if the 
                HANDLEs are going to be destroyed a lot
                this will fragment the VECTOR, and make it
                larger than it needs to be.
        */
        HANDLE_TABLE_HIGH_PERFORMANCE           = (1 << 0),

        /*
                In MEMORY_CONCIOUS mode, the HANDLE_TABLE
                stores the registered HANLDEs in a 
                LINKED_LIST. When in this mode, and only
                a few HANDLEs exist, this will utilize more
                memory than the HIGH_PERFORMANCE mode. But
                where this mode shines, is when you are
                constantly creating and destroying HANDLEs,
                this mode will (eventually) occupy a lot
                less memory.
        */
        HANDLE_TABLE_MEMORY_CONCIOUS            = (1 << 1),

	/* 
		Uses a HASHTABLE to register the HANDLES.
		This mode takes up a static amount of memory,
		but will be ultra fast lookup. If requiring
		the use of data other than the HANDLE's derivative 
		for the lookup, use the EX version, which requires
		the use of the RegisterHandleEx and DeregisterHandleEx
		functions.
	*/
	HANDLE_TABLE_ULTRA_PERFORMANCE		= (1 << 2),
	HANDLE_TABLE_ULTRA_PERFORMANCE_EX	= (1 << 3),

	/*
		Uses a BINARYSEARCHTREE to register HANDLEs.
		This mode will increase in RAM as more are registered.
		However, this still allows for an extremely fast lookup
		time. Great for high performance needs, and low overhead.
	*/
	HANDLE_TABLE_PERFORMANT_AND_CONCIOUS	= (1 << 4),
	HANDLE_TABLE_PERFORMANT_AND_CONCIOUS_EX	= (1 << 5)
} HANDLE_TABLE_FLAGS;


typedef _Call_Back_ BOOL (* LPFN_HDERIVATIVE_COMPARE_PROC)(
        HDERIVATIVE             hLookingFor,
        HDERIVATIVE             hComparand
);



_Success_(return != NULLPTR, ...)
PODNET_API
HANDLE
CreateHandleTable(
        _In_                            ULONGLONG               ullSize,
        _Reserved_Must_Be_Null_         LPVOID                  lpReserved,
        _In_Opt_                        ULONG                   ulFlags
);




_Success_(return != NULL, ...)
PODNET_API
ULONGLONG
RegisterHandle(
        _In_                            HANDLE                  hHandleTable,
        _In_                            HANDLE                  hHandle
);




_Success_(return != NULL, ...)
PODNET_API
ULONGLONG
RegisterHandleEx(
        _In_                            HANDLE                  hHandleTable,
        _In_                            HANDLE                  hHandle,
	_In_ 				ULONGLONG 		ullKey
);




_Success_(return != FALSE, ...)
PODNET_API
BOOL
DeregisterHandle(
        _In_                            HANDLE                  hHandleTable,
        _In_                            HANDLE                  hHandle
);




_Success_(return != FALSE, ...)
PODNET_API
BOOL
DeregisterHandleEx(
        _In_                            HANDLE                  hHandleTable,
        _In_                            HANDLE                  hHandle,
	_In_				ULONGLONG 		ullKey
);




_Success_(return != NULL, ...)
PODNET_API
HANDLE 
GrabHandle(
        _In_                            HANDLE                  hHandleTable,
        _In_                            ULONGLONG               ullId
);




_Success_(return != NULLPTR, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Spin_Lock_)))
PODNET_API
HVECTOR
GrabAllHandles(
        _In_                            HANDLE                  hHandleTable
);




_Success_(return != NULL, ...)
PODNET_API
HANDLE
GrabHandleByDerivative(
        _In_                            HANDLE                          hHandleTable,
        _In_                            HDERIVATIVE                     hDerivative,
        _In_                            ULONGLONG                       ullSizeOfDerivativeObject,
        _In_Opt_                        LPFN_HDERIVATIVE_COMPARE_PROC   lpfnDerivativeCompare
);




_Success_(return != FALSE, ...)
PODNET_API
BOOL
HandleIsValid(
	_In_ 				HANDLE 				hHandleTable,
	_In_ 				HANDLE 				hHandleToCheck,
	_Out_ 				LPULONGLONG                     ullId 
);



#endif
