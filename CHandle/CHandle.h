/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CHANDLE_H
#define CHANDLE_H


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"




#ifndef __HANDLE_DEFINED__
#define __HANDLE_DEFINED__ 		1
typedef struct __OBJECT			*HANDLE, **LPHANDLE, ***DLPHANDLE;
typedef LPVOID				HDERIVATIVE, *LPHDERIVATIVE, **DLPHDERIVATIVE;
typedef LPVOID				HBASEOBJ, *LPHBASEOBJ, **DLPHBASEOBJ;
#endif


#define NULL_OBJECT 				NULLPTR



#define INHERITS_FROM_HANDLE()			HANDLE		hThis;\
						ULONGLONG	ullId

#define	HANDLE_FLAG_ONLY_1_DESTROYER		1<<0
#define HANDLE_FLAG_ONLY_1_ERROR_HANDLER	1<<1
#define HANDLE_FLAG_ONLY_1_ALARM_HANDLER	1<<2
#define HANDLE_FLAG_COPY_DATA_TO_NEW_INSTANCE	1<<4
#define HANDLE_FLAG_ALL_TYPES_ARE_SAME		HANDLE_FLAG_ONLY_1_DESTROYER | HANDLE_FLAG_ONLY_1_ERROR_HANDLER | HANDLE_FLAG_ONLY_1_ALARM_HANDLER	


typedef enum __HANDLE_TYPE
{
	HANDLE_TYPE_EVENT			= 0x0100,
	HANDLE_TYPE_CRIT_SEC			= 0x0101,
	HANDLE_TYPE_SEMAPHORE			= 0x0102,
	HANDLE_TYPE_MUTEX			= 0x0103,
	HANDLE_TYPE_SPINLOCK			= 0x0104,
	HANDLE_TYPE_PROCESS			= 0x010A,
	HANDLE_TYPE_THREAD			= 0x0110,
	HANDLE_TYPE_PROMISE			= 0x0111,
	HANDLE_TYPE_FUTURE			= 0x0112,
	HANDLE_TYPE_ONCE_CALLABLE		= 0x0113,
	HANDLE_TYPE_POLLER			= 0x0114,
	HANDLE_TYPE_FILE			= 0x0120,
	HANDLE_TYPE_LOG				= 0x0121,
	HANDLE_TYPE_ATOM_TABLE			= 0x0122,
	HANDLE_TYPE_QUARK_TABLE			= 0x0123,
	HANDLE_TYPE_HANDLE_TABLE		= 0x0124,
	HANDLE_TYPE_DIRECTORY			= 0x0125,
	HANDLE_TYPE_SOCKET			= 0x0130,
	HANDLE_TYPE_SOCKET6			= 0x0131,
	HANDLE_TYPE_SOCKETUNIX			= 0x0132,
	HANDLE_TYPE_WEBSERVER			= 0x0133,
	HANDLE_TYPE_HTTP_REQUEST		= 0x0134,
	HANDLE_TYPE_HTTPS_REQUEST		= 0x0135,
	HANDLE_TYPE_SMTP_CONNECTION		= 0x0140,
	HANDLE_TYPE_DEVICE_GPIO			= 0x0150,
	HANDLE_TYPE_DEVICE_SERIAL		= 0x0151,
	HANDLE_TYPE_DEVICE_I2C			= 0x0152,
	HANDLE_TYPE_DEVICE_SPI			= 0x0153,
	HANDLE_TYPE_HEAP 			= 0x0160,
	HANDLE_TYPE_DYNAMIC_HEAP_REGION		= 0x0163,
	HANDLE_TYPE_RNG				= 0x0170,
	HANDLE_TYPE_TIMER			= 0x0180,
	HANDLE_TYPE_MODULE 			= 0x0200,
	HANDLE_TYPE_WINDOW_SYSTEM		= 0x0290,
	HANDLE_TYPE_WINDOW 			= 0x0291,
	HANDLE_TYPE_DEVICE_INTERFACE 		= 0x0300,
	HANDLE_TYPE_DEVICE_ARDUINO_I2C		= 0x0310,
	HANDLE_TYPE_GPS				= 0x0320,
	HANDLE_TYPE_LEXER			= 0x0330,
	HANDLE_TYPE_TOKEN_TABLE			= 0x0331,
	HANDLE_TYPE_PARSER			= 0x0332,
	HANDLE_TYPE_LUA				= 0x0340,
	HANDLE_TYPE_INI_PARSER			= 0x0350,
	HANDLE_TYPE_AUDIO_BUFFER_MANAGER 	= 0x0400,
	HANDLE_TYPE_AUDIO_BUFFER_PLAYER 	= 0x0401,
	HANDLE_TYPE_AUDIO_BUFFER_RECORDER	= 0x0402
} HANDLE_TYPE;


#define __NUMBER_OF_DIFFERENT_HANDLE_TYPES	((HANDLE_TYPE_DEVICE_SPI - HANDLE_TYPE_EVENT) + 1U);

#define INFINITE_WAIT_TIME			(MAX_ULONGLONG)
#define MAX_FINITE_WAIT_TIME			((MAX_ULONGLONG) - 1ULL)

#define WRITE_ASYNC				(1 << 0)

#define READ_ASYNC				(1 << 0)


#define OBJECT_CAST(H,T)			((*(T **)(H))[0])
#define OBJECT_CAST_AT(H,T,I)			((*(T **)(H))[(I)])


#include "CHandleFunctionPointers.h"
#include "CWait.h"
#include "CRelease.h"
#include "../CLock/CCritSec.h"
#include "../CLock/CEvent.h"
#include "../CSystem/CSystem.h"
#include "../CClock/CClock.h"



PODNET_API
HANDLE
CreateHandleWithSingleInheritor(
	_In_		HDERIVATIVE		hDerivative,			/* Pointer to Base "Class" / Struct */
	_In_		LPHANDLE 		lphBase,			/* Pointer to Base->hObject (Inherited) */
	_In_		ULONG			ulHandleType,			/* The Handle Type (can be an enum) */
	_In_		LPFN_HANDLE_DESTROY	lpfnDestroy,			/* Function Pointer to Destroyer Function of Base */
	_In_Opt_	ULONG			ulDestroyFlags,			/* Flags you wished to be passed to Destroyer */
	_In_Opt_	LPFN_HANDLE_ERROR	lpfnErrorHandler,		/* Function Pointer to Error Handling Function */
	_In_Opt_	ULONG			ulErrorFlags,			/* Flags for Error Function */
	_In_Opt_	LPFN_HANDLE_ALARM	lpfnAlarmHandler,		/* Function Pointer to Alarm Handling Function */
	_In_Opt_	ULONG			ulAlarmFlags,			/* Flags for Alarm Function */
	_Out_Opt_	LPULONGLONG		lpullIdentifer,			/* Out value of the internal identifer */
	_In_Opt_	ULONG			ulFlags				/* Any flags */
);


PODNET_API
HANDLE
CreateHandleWithCriticalSectionControllingSingleInheritor(
	_In_		HDERIVATIVE		hDerivative,
	_In_		LPHANDLE		lphBase,
	_In_		ULONG			ulHandleType,
	_In_Opt_	HANDLE			hCritSec,
	_In_		LPFN_HANDLE_DESTROY	lpfnDestroy,
	_In_Opt_	ULONG			ulDestroyFlags,
	_In_Opt_	LPFN_HANDLE_ERROR	lpfnErrorHandler,
	_In_Opt_	ULONG			ulErrorFlags,
	_In_Opt_	LPFN_HANDLE_ALARM	lpfnAlarmHandler,
	_In_Opt_	ULONG			ulAlarmFlags,
	_Out_Opt_	LPULONGLONG		lpullIdentifier,
	_In_Opt_	ULONG			ulFlags
);


_Result_Null_On_Failure_
PODNET_API
HANDLE
CreateHandleWithSingleInheritorAndSingleSettableEvent(
	_In_		HDERIVATIVE		hDerivative,
	_In_		LPHANDLE		lphBase,
	_In_		ULONG			ulHandleType,
	_In_Opt_	HANDLE			hEvent,
	_In_		LPFN_HANDLE_DESTROY	lpfnDestroy,
	_In_Opt_	ULONG			ulDestroyFlags,
	_In_Opt_	LPFN_HANDLE_ERROR	lpfnErrorHandler,
	_In_Opt_	ULONG			ulErrorFlags,
	_In_Opt_	LPFN_HANDLE_ALARM	lpfnAlarmHandler,
	_In_Opt_	ULONG			ulAlarmFlags,
	_Out_Opt_	LPULONGLONG		lpullIdentifier,
	_In_Opt_	ULONG			ulFlags
);


/* TODO:Implement this */
_Result_Null_On_Failure_
PODNET_API
HANDLE
CreateHandleWithSingleCriticalSectionControllingSingleInheritorWithMultipleSettableEventsAndSingleUserDefinableProc(
	_In_		HDERIVATIVE			hDerivative,
	_In_		LPHANDLE			lphBaseObject,
	_In_		ULONG				ulHandleType,
	_In_Opt_	HANDLE				hCritSec,
	_In_Opt_	LPHANDLE			lphEvents,
	_In_Opt_	ULONG				ulNumberOfEvents,
	_In_		LPFN_HANDLE_DESTROY		lpfnDestroyer,
	_In_Opt_	ULONG				ulDestroyFlags,
	_In_Opt_	LPFN_HANDLE_ERROR		lpfnErrorHandler,
	_In_Opt_	ULONG				ulErrorFlags,
	_In_Opt_	LPFN_HANDLE_ALARM		lpfnAlarmHandler,
	_In_Opt_	ULONG				ulAlarmFlags,
	_In_Opt_	LPFN_HANDLE_EVENT_SET		lpfnEventSetHandler,
	_In_Opt_	ULONG				ulEventSetFlags,
	_In_Opt_	LPFN_HANDLE_EVENT_WAIT		lpfnEventWaitHandler,
	_In_Opt_	ULONG				ulEventWaitFlags,
	_In_Opt_	LPFN_HANDLE_CUSTOM_PROC 	lpfnCustomProc,
	_In_Opt_	ULONG				ulCustomProcFlags,
	_Out_Opt_	LPULONGLONG			lpullIdentifier,
	_Out_Opt_	LPHANDLE			lphCritSec,
	_Out_Opt_	DLPHANDLE			dlphEvents,
	_In_Opt_	ULONG				ulFlags
);


PODNET_API
HANDLE
CreateHandleWithMultipleInheritors(
	_In_		LPHDERIVATIVE		lphDerivatives,
	_In_		ULONG			ulNumberOfDerivatives,
	_In_		DLPHANDLE		dlphBaseObjects,
	_In_		LPULONG			lpulHandleTypes,
	_In_		DLPFN_HANDLE_DESTROY	dlpfnDestroyers,
	_In_Opt_	LPULONG			lpulDestroyFlags,
	_In_Opt_	DLPFN_HANDLE_ERROR	dlpfnErrorHandlers,
	_In_Opt_	LPULONG			lpulErrorFlags,
	_In_Opt_	DLPFN_HANDLE_ALARM	dlpfnAlarmHandlers,
	_In_Opt_	LPULONG			lpulAlarmFlags,
	_Out_Opt_	DLPULONGLONG		dlpullIdentifiers,
	_In_Opt_	ULONG			ulFlags
);


PODNET_API
HANDLE
CreateHandleWithSingleCriticalSectionControllingMultipleInheritors(
	_In_		LPHDERIVATIVE		lphDerivatives,
	_In_		ULONG			ulNumberOfDerivatives,
	_In_		DLPHANDLE		dlphBaseObjects,
	_In_		LPULONG			lpulHandleTypes,
	_In_Opt_	HANDLE			hCritSec,
	_In_		DLPFN_HANDLE_DESTROY	dlpfnDestroyers,
	_In_Opt_	LPULONG			lpulDestroyFlags,
	_In_Opt_	DLPFN_HANDLE_ERROR	dlpfnErrorHandlers,
	_In_Opt_	LPULONG			lpulErrorFlags,
	_In_Opt_	DLPFN_HANDLE_ALARM	dlpfnAlarmHandlers,
	_In_Opt_	LPULONG			lpulAlarmFlags,
	_Out_Opt_	DLPULONGLONG		dlpullIdentifiers,
	_In_Opt_	ULONG			ulFlags
);


PODNET_API
HANDLE
CreateHandleWithSingleCriticalSectionControllingMultipleInheritorsWithSingleSettableEvent(
	_In_		LPHDERIVATIVE		lphDerivatives,
	_In_		ULONG			ulNumberOfDerivatives,
	_In_		DLPHANDLE		dlphBaseObjects,
	_In_		LPULONG			lpulHandleTypes,
	_In_Opt_	HANDLE			hCritSec,
	_In_Opt_	HANDLE			hEvent,
	_In_		DLPFN_HANDLE_DESTROY	dlpfnDestroyers,
	_In_Opt_	LPULONG			lpulDestroyFlags,
	_In_Opt_	DLPFN_HANDLE_ERROR	dlpfnErrorHandlers,
	_In_Opt_	LPULONG			lpulErrorFlags,
	_In_Opt_	DLPFN_HANDLE_ALARM	dlpfnAlarmHandlers,
	_In_Opt_	LPULONG			lpulAlarmFlags,
	_In_		DLPFN_HANDLE_EVENT_SET	dlpfnEventSetHandlers,
	_In_Opt_	LPULONG			lpulEventSetFlags,
	_In_		DLPFN_HANDLE_EVENT_WAIT	dlpfnEventWaitHandlers,
	_In_Opt_	LPULONG			lpulEventWaitFlags,
	_Out_Opt_	DLPULONGLONG		dlpullIdentifiers,
	_Out_Opt_	LPHANDLE		lphCritSec,
	_Out_Opt_	LPHANDLE		lphEvent,
	_In_Opt_	ULONG			ulFlags
);



PODNET_API
_Result_Null_On_Failure_
HANDLE
CreateHandleWithSingleCriticalSectionControllingMultipleInheritorsWithMultipleSettableEvents(
	_In_		LPHDERIVATIVE		lphDerivatives,
	_In_		ULONG			ulNumberOfDerivatives,
	_In_		DLPHANDLE		dlphBaseObjects,
	_In_		LPULONG			lpulHandleTypes,
	_In_Opt_	HANDLE			hCritSec,
	_In_Opt_	LPHANDLE		lphEvents,
	_In_Opt_	ULONG			ulNumberOfEvents,
	_In_		DLPFN_HANDLE_DESTROY	dlpfnDestroyers,
	_In_Opt_	LPULONG			lpulDestroyFlags,
	_In_Opt_	DLPFN_HANDLE_ERROR	dlpfnErrorHandlers,
	_In_Opt_	LPULONG			lpulErrorFlags,
	_In_Opt_	DLPFN_HANDLE_ALARM	dlpfnAlarmHandlers,
	_In_Opt_	LPULONG			lpulAlarmFlags,
	_In_		DLPFN_HANDLE_EVENT_SET	dlpfnEventSetHandlers,
	_In_Opt_	LPULONG			lpulEventSetFlags,
	_In_		DLPFN_HANDLE_EVENT_WAIT	dlpfnEventWaitHandlers,
	_In_Opt_	LPULONG			lpulEventWaitFlags,
	_Out_Opt_	DLPULONGLONG		dlpullIdentifiers,
	_Out_Opt_	LPHANDLE		lphCritSec,
	_Out_Opt_	DLPHANDLE		dlphEvents,
	_In_Opt_	ULONG			ulFlags
);


_Result_Null_On_Failure_
PODNET_API
HANDLE
CreateHandleWithSingleCriticalSectionControllingMultipleInheritorsWithMultipleSettableEventsAndMultipleUserDefinableProcs(
	_In_		LPHDERIVATIVE			lphDerivatives,
	_In_		ULONG				ulNumberOfDerivatives,
	_In_		DLPHANDLE			dlphBaseObjects,
	_In_		LPULONG				lpulHandleTypes,
	_In_Opt_	HANDLE				hCritSec,
	_In_Opt_	LPHANDLE			lphEvents,
	_In_Opt_	ULONG				ulNumberOfEvents,
	_In_		DLPFN_HANDLE_DESTROY		dlpfnDestroyers,
	_In_Opt_	LPULONG				lpulDestroyFlags,
	_In_Opt_	DLPFN_HANDLE_ERROR		dlpfnErrorHandlers,
	_In_Opt_	LPULONG				lpulErrorFlags,
	_In_Opt_	DLPFN_HANDLE_ALARM		dlpfnAlarmHandlers,
	_In_Opt_	LPULONG				lpulAlarmFlags,
	_In_Opt_	DLPFN_HANDLE_EVENT_SET		dlpfnEventSetHandlers,
	_In_Opt_	LPULONG				lpulEventSetFlags,
	_In_Opt_	DLPFN_HANDLE_EVENT_WAIT		dlpfnEventWaitHandlers,
	_In_Opt_	LPULONG				lpulEventWaitFlags,
	_In_Opt_	DLPFN_HANDLE_CUSTOM_PROC 	dlpfnCustomProcs,
	_In_Opt_	ULONG 				ulNumberOfCustomProcs,
	_In_Opt_	LPULONG				lpulCustomProcFlags,
	_Out_Opt_	DLPULONGLONG			dlpullIdentifiers,
	_Out_Opt_	LPHANDLE			lphCritSec,
	_Out_Opt_	DLPHANDLE			dlphEvents,
	_In_Opt_	ULONG				ulFlags
);



/*
	"When the only tool you have is a hammer,
	beat it to death."
*/


_Result_Null_On_Failure_
PODNET_API
HANDLE
CreateHandleWithSingleCriticalSectionControllingMultipleInheritorsWithMultipleSettableEventsAndMultipleUserDefinableProcsAndData(
	_In_		LPHDERIVATIVE			lphDerivatives,
	_In_		ULONG				ulNumberOfDerivatives,
	_In_		DLPHANDLE			dlphBaseObjects,
	_In_		LPULONG				lpulHandleTypes,
	_In_Opt_	HANDLE				hCritSec,
	_In_Opt_	LPHANDLE			lphEvents,
	_In_Opt_	ULONG				ulNumberOfEvents,
	_In_		DLPFN_HANDLE_DESTROY		dlpfnDestroyers,
	_In_Opt_	LPULONG				lpulDestroyFlags,
	_In_Opt_	DLPFN_HANDLE_ERROR		dlpfnErrorHandlers,
	_In_Opt_	LPULONG				lpulErrorFlags,
	_In_Opt_	DLPFN_HANDLE_ALARM		dlpfnAlarmHandlers,
	_In_Opt_	LPULONG				lpulAlarmFlags,
	_In_Opt_	DLPFN_HANDLE_EVENT_SET		dlpfnEventSetHandlers,
	_In_Opt_	LPULONG				lpulEventSetFlags,
	_In_Opt_	DLPFN_HANDLE_EVENT_WAIT		dlpfnEventWaitHandlers,
	_In_Opt_	LPULONG				lpulEventWaitFlags,
	_In_Opt_	DLPFN_HANDLE_CUSTOM_PROC 	dlpfnCustomProcs,
	_In_Opt_	ULONG 				ulNumberOfCustomProcs,
	_In_Opt_	LPULONG 			lpulCustomProcFlags,
	_In_Opt_	LPVOID				lpDefinableData,
	_In_Opt_	ULONG				ulNumberOfDefinableDataSets,
	_In_Opt_	ULONG				ulSizeOfDefinableData,
	_Out_Opt_	DLPULONGLONG			dlpullIdentifiers,
	_Out_Opt_	LPHANDLE			lphCritSec,
	_Out_Opt_	DLPHANDLE			dlphEvents,
	_In_Opt_	ULONG				ulFlags
);


PODNET_API
CALL_TYPE(HOT)
BOOL
DestroyObject(
	_In_		HANDLE			hObject
);



#define DestroyHandle 				DestroyObject



_Success_(return != NULL, _Interlocked_Operation_)
PODNET_API
UARCHLONG
GetTotalNumberOfObjects(
	VOID
) CALL_TYPE(COLD);




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HDERIVATIVE
GetHandleDerivative(
	_In_		HANDLE RESTRICT		hObject
) CALL_TYPE(HOT);




PODNET_API
HDERIVATIVE
GetHandleDerivativeAtIndex(
	_In_		HANDLE			hObject,
	_In_		ULONGLONG		ullIndex
);


PODNET_API
BOOL
SetHandleDestroyFunction(
	_In_		HANDLE			hObject,
	_In_		LPFN_HANDLE_DESTROY	lpfnDestroy,
	_In_Opt_	ULONG			ulDestroyFlags
);




PODNET_API
BOOL
SetHandleDestroyFunctionAtIndex(
	_In_		HANDLE			hObject,
	_In_		ULONGLONG		ullIndex,
	_In_		LPFN_HANDLE_DESTROY	lpfnDestroy,
	_In_Opt_	ULONG			ulDestroyFlags
);


PODNET_API
LPFN_HANDLE_DESTROY
GetHandleDestroyFunction(
	_In_		HANDLE			hObject,
	_Out_Opt_	LPULONG			lpulFlagsOut
);


PODNET_API
LPFN_HANDLE_DESTROY
GetHandleDestroyFunctionAtIndex(
	_In_		HANDLE			hObject,
	_In_		ULONGLONG		ullIndex,
	_Out_Opt_	LPULONG			lpulFlagsOut
);

_Success_(return != NULLPTR, {})
PODNET_API
HANDLE
GetObjectEvent(
	_In_		HANDLE			hObject
);




_Success_(return != FALSE, _Acquires_Lock_())
PODNET_API
BOOL
SetHandleLockFunctions(
	_In_			HANDLE			hObject,
	_In_  _Call_Back_	LPFN_LOCK_PROC		lpfnLockProc,
	_In_  _Call_Back_	LPFN_UNLOCK_PROC	lpfnUnlockProc,
	_In_Opt_  _Call_Back_	LPFN_TRY_LOCK_PROC	lpfnTryLockProc
);



PODNET_API
ULONG
GetHandleType(
	_In_		HANDLE RESTRICT		hObject
) CALL_TYPE(HOT);


PODNET_API
ULONG
GetHandleTypeAtIndex(
	_In_		HANDLE			hObject,
	_In_		ULONGLONG		ullIndex
);



PODNET_API
BOOL
HandleHasMultipleInheritors(
	_In_		HANDLE			hObject
);


PODNET_API
ULONGLONG
GetNumberOfHandleDerivatives(
	_In_		HANDLE			hObject
);


PODNET_API
BOOL
ErrorOutHandle(
	_In_		HANDLE			hObject,
	_In_		ULONG			ulErrorCode
);


PODNET_API
BOOL
RaiseHandleAlarm(
	_In_		HANDLE			hObject,
	_In_		ULONG			ulAlarmCode,
	_In_Opt_	LPVOID			lpMoreInfo
);


_Success_(return != FALSE, _Acquires_Shared_Lock_(hObject->hCritSec, _Lock_Kind_Critical_Section_(hObject)))
PODNET_API
BOOL
LockHandle(
	_In_		HANDLE			hObject
);


_Success_(return != FALSE, _Acquires_Shared_Lock_(hObject->hCritSec, _Lock_Kind_Critical_Section_(hObject)))
PODNET_API
BOOL
TryLockHandle(
	_In_		HANDLE			hObject
);



_Success_(return != FALSE, _Releases_Shared_Lock_(hObject->hCritSec, _Lock_Kind_Critical_Section_(hObject)))
PODNET_API
BOOL
UnlockHandle(
	_In_		HANDLE			hObject
);


_Result_Null_On_Failure_
PODNET_API
ULONG
GetHandleDerivativeType(
	_In_		HANDLE			hObject
);



_Success_(return != FALSE, _Inexpressible_())
PODNET_API
BOOL
GetHandleInformation(
	_In_ 					HANDLE			hObject,
	_Out_Ptr_Opt_Result_May_Be_Null_	DLPHDERIVATIVE		dlphDerivatives,
	_Out_Ptr_Opt_Result_May_Be_Null_	DLPULONG		dlpulDerivativeTypes,
	_Out_Ptr_Opt_Result_May_Be_Null_	DLPULONGLONG		dlpullIndexes,
	_Out_Ptr_Opt_Result_May_Be_Null_	TLPFN_HANDLE_DESTROY	tlpfnDestroyers,
	_Out_Ptr_Opt_Result_May_Be_Null_	DLPULONG		dlpulDestroyFlags,
	_Out_Ptr_Opt_Result_May_Be_Null_	TLPFN_HANDLE_ALARM	tlpfnAlarmHandlers,
	_Out_Ptr_Opt_Result_May_Be_Null_	DLPULONG		dlpulAlarmFlags,
	_Out_Ptr_Opt_Result_May_Be_Null_	TLPFN_HANDLE_ERROR	tlpfnErrorHandlers,
	_Out_Ptr_Opt_Result_May_Be_Null_	DLPULONG		dlpulErrorFlags,
	_Out_Ptr_Opt_Result_May_Be_Null_	TLPFN_HANDLE_EVENT_SET	tlpfnEventSetHandlers,
	_Out_Ptr_Opt_Result_May_Be_Null_	DLPULONG		dlpulEventSetFlags,
	_Out_Ptr_Opt_Result_May_Be_Null_	TLPFN_HANDLE_EVENT_WAIT	tlpfnEventWaitHandlers,
	_Out_Ptr_Opt_Result_May_Be_Null_	DLPULONG		dlpulEventWaitFlags,
	_Out_Ptr_Opt_Result_May_Be_Null_	LPHANDLE		lphCritSec,
	_Out_Ptr_Opt_Result_May_Be_Null_	DLPHANDLE		dlphEvents,
	_Out_Ptr_Opt_Result_May_Be_Null_	LPULONG			lpulNumberOfEvents	
);




_Success_(return != FALSE, _Interlocked_Operation_) 
PODNET_API
BOOL
IncrementObjectReferenceCount(
	_In_ 		HANDLE RESTRICT 	hObject
) CALL_TYPE(HOT);




_Success_(return != FALSE, _Interlocked_Operation_)
PODNET_API
BOOL 
DecrementObjectReferenceCount(
	_In_ 		HANDLE RESTRICT 	hObject
) CALL_TYPE(HOT);




_Success_(return != NULL, _Interlocked_Operation_)
PODNET_API
UARCHLONG 
GetObjectReferenceCount(
	_In_ 		HANDLE RESTRICT 	hObject
);




_Calls_Call_Back_
_Success_(return != FALSE, _Releases_Lock_(hObject->lphEvent[0], _Lock_Kind_Event_))
PODNET_API
BOOL
TriggerSingleObject(
	_In_		HANDLE			hObject,
	_In_Opt_	LPVOID			lpParam,
	_In_Opt_	ULONG			ulFlags
);




_Calls_Call_Back_
_Success_(return != FALSE, ...)
PODNET_API
BOOL
ResetObject(	
	_In_		HANDLE			hObject
);




_Calls_Call_Back_
_Success_(return != FALSE, ...)
PODNET_API
BOOL
ProcHandle(
	_In_		HANDLE			hObject
);




_Success_(return != FALSE, ...)
PODNET_API 
BOOL
SetHandleReadAndWriteProcs(
	_In_ 		HANDLE 			hObject,
	_In_Opt_ 	LPFN_HANDLE_READ_PROC 	lpfnReadProc,
	_In_Opt_ 	LPFN_HANDLE_WRITE_PROC 	lpfnWriteProc 
);




_Calls_Call_Back_
_Success_(return != FALSE, ...)
PODNET_API
LPVOID
Write(
	_In_ 		HANDLE 			hObject,
	_In_ 		LPVOID 			lpData,
	_In_ 		ULONGLONG 		ullSize,
	_In_Opt_ 	LPVOID 			lpParam,
	_In_Opt_ 	ULONG 			ulFlags
);




_Calls_Call_Back_
_Success_(return != FALSE, ...)
PODNET_API
LPVOID
Read(
	_In_ 		HANDLE 			hObject,
	_In_ 		LPVOID 			lpBuffer,
	_In_ 		ULONGLONG 		ullSize,
	_In_Opt_ 	LPVOID 			lpParam,
	_In_Opt_ 	ULONG 			ulFlags
);



_Success_(return != FALSE, _Interlocked_Operation_)
PODNET_API
BOOL
EnableGarbageCollection(
	VOID
);




#endif
