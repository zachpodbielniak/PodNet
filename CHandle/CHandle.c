/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CHANDLE_C
#define CHANDLE_C




#include "CHandle.h"
#include "CHandleObject.c"
#include "CHandleTable.h"
#include "../CMemory/CGarbageCollector.h"


GLOBAL_VARIABLE UARCHLONG ualTotalNumberOfObjects = 0;
GLOBAL_VARIABLE BOOL bCollectGarbage = FALSE;



PODNET_API
HANDLE
CreateHandleWithSingleInheritor(
	HDERIVATIVE 			hDerivative, 
	LPHANDLE 			lphBase, 
	ULONG 				ulHandleType, 
	LPFN_HANDLE_DESTROY 		lpfnDestroy, 
	ULONG 				ulDestroyFlags, 
	LPFN_HANDLE_ERROR 		lpfnErrorHandler, 
	ULONG 				ulErrorFlags, 
	LPFN_HANDLE_ALARM 		lpfnAlarmHandler, 
	ULONG 				ulAlarmFlags, 
	LPULONGLONG 			lpullIdentifier, 
	ULONG 				ulFlags
)
{
	EXIT_IF_NULL(hDerivative, 0);
	EXIT_IF_NULL(lphBase, 0);
	EXIT_IF_NULL(lpfnDestroy, 0);
	UNREFERENCED_PARAMETER(ulFlags);
	
	KEEP_IN_REGISTER HANDLE hObject;
	hObject = LocalAlloc(sizeof(OBJECT));
	EXIT_IF_NULL(hObject, 0);
	ZeroMemory(hObject, sizeof(OBJECT));

	hObject->lphDerivatives = LocalAlloc(sizeof(HDERIVATIVE));
	hObject->lpulDerivativeTypes = LocalAlloc(sizeof(ULONG));
	hObject->lpullIndexes = LocalAlloc(sizeof(ULONGLONG));
	hObject->dlpfnDestroyers = LocalAlloc(sizeof(LPFN_HANDLE_DESTROY));
	hObject->lpulDestroyFlags = LocalAlloc(sizeof(ULONG));
	hObject->dlpfnErrorHandlers = LocalAlloc(sizeof(LPFN_HANDLE_ERROR));
	hObject->lpulErrorFlags = LocalAlloc(sizeof(ULONG));
	hObject->dlpfnAlarmHandlers = LocalAlloc(sizeof(LPFN_HANDLE_ALARM));
	hObject->lpulAlarmFlags = LocalAlloc(sizeof(ULONG));

	/* cppcheck-suppress memleak */
	EXIT_IF_NULL(hObject->lphDerivatives, 0);
	/* cppcheck-suppress memleak */
	EXIT_IF_NULL(hObject->lpulDerivativeTypes, 0);
	/* cppcheck-suppress memleak */
	EXIT_IF_NULL(hObject->lpullIndexes, 0);
	/* cppcheck-suppress memleak */
	EXIT_IF_NULL(hObject->lpulDestroyFlags, 0);
	/* cppcheck-suppress memleak */
	EXIT_IF_NULL(hObject->lpulErrorFlags, 0);
	/* cppcheck-suppress memleak */
	EXIT_IF_NULL(hObject->lpulAlarmFlags, 0);

	*(hObject->lphDerivatives) = hDerivative;
	*(hObject->lpulDerivativeTypes) = ulHandleType;
	*(hObject->dlpfnDestroyers) = lpfnDestroy;
	*(hObject->lpulDestroyFlags) = ulDestroyFlags;
	*(hObject->dlpfnErrorHandlers) = lpfnErrorHandler;
	*(hObject->lpulErrorFlags) = ulErrorFlags;
	*(hObject->dlpfnAlarmHandlers) = lpfnAlarmHandler;
	*(hObject->lpulAlarmFlags) = ulAlarmFlags;
	*(hObject->lpullIndexes) = 0x00U;
	hObject->hCritSec = NULLPTR;
	hObject->ulNumberOfDerivatives = 0x01U;
	*lpullIdentifier = *(hObject->lpullIndexes);
	hObject->ualReferenceCount = 0x01U;
	
	*lphBase = hObject;
	
	InterlockedIncrement(&ualTotalNumberOfObjects);

	return hObject;	
}


PODNET_API
HANDLE
CreateHandleWithCriticalSectionControllingSingleInheritor(
	HDERIVATIVE 			hDerivative, 
	LPHANDLE 			lphBase, 
	ULONG 				ulHandleType, 
	HANDLE 				hCritSec, 
	LPFN_HANDLE_DESTROY 		lpfnDestroy, 
	ULONG 				ulDestroyFlags, 
	LPFN_HANDLE_ERROR 		lpfnErrorHandler, 
	ULONG 				ulErrorFlags, 
	LPFN_HANDLE_ALARM 		lpfnAlarmHandler, 
	ULONG 				ulAlarmFlags, 
	LPULONGLONG 			lpullIdentifier, 
	ULONG 				ulFlags
)
{
	HANDLE hObject;
	hObject = CreateHandleWithSingleInheritor(	hDerivative, 
							lphBase, 
							ulHandleType, 
							lpfnDestroy, 
							ulDestroyFlags,
							lpfnErrorHandler,
							ulErrorFlags,
							lpfnAlarmHandler,
							ulAlarmFlags, 
							lpullIdentifier, 
							ulFlags);
	EXIT_IF_NULL(hObject, 0);
	
	if (hCritSec == NULLPTR)	
		hCritSec = CreateCriticalSection();
	EXIT_IF_NULL(hCritSec, 0);
	hObject->hCritSec = hCritSec;

	return hObject;	
}


_Result_Null_On_Failure_
PODNET_API
HANDLE
CreateHandleWithSingleInheritorAndSingleSettableEvent(
	_In_		HDERIVATIVE		hDerivative,
	_In_		LPHANDLE		lphBase,
	_In_		ULONG			ulHandleType,
	_In_Opt_	HANDLE			hEvent,
	_In_		LPFN_HANDLE_DESTROY	lpfnDestroy,
	_In_Opt_	ULONG			ulDestroyFlags,
	_In_Opt_	LPFN_HANDLE_ERROR	lpfnErrorHandler,
	_In_Opt_	ULONG			ulErrorFlags,
	_In_Opt_	LPFN_HANDLE_ALARM	lpfnAlarmHandler,
	_In_Opt_	ULONG			ulAlarmFlags,
	_Out_Opt_	LPULONGLONG		lpullIdentifier,
	_In_Opt_	ULONG			ulFlags
){
	HANDLE hObject;
	hObject = CreateHandleWithSingleInheritor(	hDerivative, 
							lphBase, 
							ulHandleType, 
							lpfnDestroy, 
							ulDestroyFlags,
							lpfnErrorHandler,
							ulErrorFlags,
							lpfnAlarmHandler,
							ulAlarmFlags, 
							lpullIdentifier, 
							ulFlags);
	EXIT_IF_NULL(hObject, 0);

	if (hEvent == NULLPTR)
		hEvent = CreateEvent(FALSE);
	EXIT_IF_NULL(hEvent, 0);
	
	hObject->lphEvents = LocalAlloc(sizeof(HANDLE));
	EXIT_IF_NULL(hObject->lphEvents, 0);
	hObject->lphEvents[0] = hEvent;

	return hObject;	
}



_Result_Null_On_Failure_
PODNET_API
HANDLE
CreateHandleWithSingleCriticalSectionControllingSingleInheritorWithMultipleSettableEventsAndSingleUserDefinableProc(
	_In_		HDERIVATIVE			hDerivative,
	_In_		LPHANDLE			lphBaseObject,
	_In_		ULONG				ulHandleType,
	_In_Opt_	HANDLE				hCritSec,
	_In_Opt_	LPHANDLE			lphEvents,
	_In_Opt_	ULONG				ulNumberOfEvents,
	_In_		LPFN_HANDLE_DESTROY		lpfnDestroyer,
	_In_Opt_	ULONG				ulDestroyFlags,
	_In_Opt_	LPFN_HANDLE_ERROR		lpfnErrorHandler,
	_In_Opt_	ULONG				ulErrorFlags,
	_In_Opt_	LPFN_HANDLE_ALARM		lpfnAlarmHandler,
	_In_Opt_	ULONG				ulAlarmFlags,
	_In_Opt_	LPFN_HANDLE_EVENT_SET		lpfnEventSetHandler,
	_In_Opt_	ULONG				ulEventSetFlags,
	_In_Opt_	LPFN_HANDLE_EVENT_WAIT		lpfnEventWaitHandler,
	_In_Opt_	ULONG				ulEventWaitFlags,
	_In_Opt_	LPFN_HANDLE_CUSTOM_PROC 	lpfnCustomProc,
	_In_Opt_	ULONG				ulCustomProcFlags,
	_Out_Opt_	LPULONGLONG			lpullIdentifier,
	_Out_Opt_	LPHANDLE			lphCritSec,
	_Out_Opt_	DLPHANDLE			dlphEvents,
	_In_Opt_	ULONG				ulFlags
){
	UNREFERENCED_PARAMETER(hDerivative);
	UNREFERENCED_PARAMETER(lphBaseObject);
	UNREFERENCED_PARAMETER(ulHandleType);
	UNREFERENCED_PARAMETER(hCritSec);
	UNREFERENCED_PARAMETER(lphEvents);
	UNREFERENCED_PARAMETER(ulNumberOfEvents);
	UNREFERENCED_PARAMETER(lpfnDestroyer);
	UNREFERENCED_PARAMETER(ulDestroyFlags);
	UNREFERENCED_PARAMETER(lpfnErrorHandler);
	UNREFERENCED_PARAMETER(ulErrorFlags);
	UNREFERENCED_PARAMETER(lpfnAlarmHandler);
	UNREFERENCED_PARAMETER(ulAlarmFlags);
	UNREFERENCED_PARAMETER(lpfnEventSetHandler);
	UNREFERENCED_PARAMETER(ulEventSetFlags);
	UNREFERENCED_PARAMETER(lpfnEventWaitHandler);
	UNREFERENCED_PARAMETER(ulEventWaitFlags);
	UNREFERENCED_PARAMETER(lpfnCustomProc);
	UNREFERENCED_PARAMETER(ulCustomProcFlags);
	UNREFERENCED_PARAMETER(lpullIdentifier);
	UNREFERENCED_PARAMETER(lphCritSec);
	UNREFERENCED_PARAMETER(dlphEvents);
	UNREFERENCED_PARAMETER(ulFlags);
	return 0;
}




PODNET_API
HANDLE
CreateHandleWithMultipleInheritors(	
	LPHDERIVATIVE 			lphDerivatives, 
	ULONG 				ulNumberOfDerivatives, 
	DLPHANDLE 			dlphBaseObjects, 
	LPULONG 			lpulHandleTypes, 
	DLPFN_HANDLE_DESTROY 		dlpfnDestroyers, 
	LPULONG 			lpulDestroyFlags, 
	DLPFN_HANDLE_ERROR 		dlpfnErrorHandlers, 
	LPULONG 			lpulErrorFlags, 
	DLPFN_HANDLE_ALARM 		dlpfnAlarmHandler, 
	LPULONG 			lpulAlarmFlags, 
	DLPULONGLONG 			dlpullIdentifiers,
	ULONG 				ulFlags
)
{
	UNREFERENCED_PARAMETER(dlpfnErrorHandlers);
	UNREFERENCED_PARAMETER(lpulErrorFlags);
	UNREFERENCED_PARAMETER(dlpfnAlarmHandler);
	UNREFERENCED_PARAMETER(lpulAlarmFlags);
	UNREFERENCED_PARAMETER(ulFlags);
	
	EXIT_IF_NULL(lphDerivatives, 0)
	EXIT_IF_NULL(ulNumberOfDerivatives, 0);
	EXIT_IF_NULL(dlphBaseObjects, 0);
	EXIT_IF_NULL(lpulHandleTypes, 0);
	EXIT_IF_NULL(dlpfnDestroyers, 0);
	HANDLE hObject;
	hObject = LocalAlloc(sizeof(OBJECT));
	EXIT_IF_NULL(hObject, 0);
	ZeroMemory(hObject, sizeof(OBJECT));	


	hObject->lphDerivatives = lphDerivatives;
	hObject->lpulDerivativeTypes = lpulHandleTypes;
	hObject->dlpfnDestroyers = dlpfnDestroyers;
	hObject->lpulDestroyFlags = lpulDestroyFlags;
	hObject->lpullIndexes = *dlpullIdentifiers; /* TODO: Fix - Only temporary */
	hObject->ulNumberOfDerivatives = ulNumberOfDerivatives;
	hObject->ualReferenceCount = 0x01U;

	ULONG ulI;
	LPVOID lpAddress;
	for (ulI = 0; ulI < ulNumberOfDerivatives; ulI++)
	{
		lpAddress = (dlphBaseObjects[ulI]);
		if (lpAddress != NULLPTR)
			*(HANDLE *)lpAddress = hObject;	
	}

	InterlockedIncrement(&ualTotalNumberOfObjects);

	return hObject;
}



PODNET_API
HANDLE
CreateHandleWithSingleCriticalSectionControllingMultipleInheritors(
	LPHDERIVATIVE 			lphDerivatives, 
	ULONG 				ulNumberOfDerivatives, 
	DLPHANDLE 			dlphBaseObjects, 
	LPULONG 			lpulHandleTypes, 
	HANDLE 				hCritSec, 
	DLPFN_HANDLE_DESTROY 		dlpfnDestroyers, 
	LPULONG 			lpulDestroyFlags, 
	DLPFN_HANDLE_ERROR 		dlpfnErrorHandlers, 
	LPULONG 			lpulErrorFlags, 
	DLPFN_HANDLE_ALARM 		dlpfnAlarmHandlers, 
	LPULONG 			lpulAlarmFlags, 
	DLPULONGLONG 			dlpullIdentifiers, 
	ULONG 				ulFlags
)
{
	HANDLE hObject;
	hObject = CreateHandleWithMultipleInheritors(	lphDerivatives, 
							ulNumberOfDerivatives, 
							dlphBaseObjects, 
							lpulHandleTypes, 
							dlpfnDestroyers, 
							lpulDestroyFlags,
							dlpfnErrorHandlers,
							lpulErrorFlags,
							dlpfnAlarmHandlers,
							lpulAlarmFlags, 
							dlpullIdentifiers, 
							ulFlags);
	EXIT_IF_NULL(hObject, 0);
	
	if (hCritSec == NULLPTR)	
		hCritSec = CreateCriticalSection();
	EXIT_IF_NULL(hCritSec, 0);
	hObject->hCritSec = hCritSec;

	return hObject;
}



PODNET_API
HANDLE
CreateHandleWithSingleCriticalSectionControllingMultipleInheritorsWithSingleSettableObject(
	LPHDERIVATIVE 			lphDerivatives, 
	ULONG 				ulNumberOfDerivatives, 
	DLPHANDLE 			dlphBaseObjects, 
	LPULONG 			lpulHandleTypes, 
	HANDLE 				hCritSec, 
	HANDLE 				hEvent, 
	DLPFN_HANDLE_DESTROY 		dlpfnDestroyers, 
	LPULONG 			lpulDestroyFlags, 
	DLPFN_HANDLE_ERROR 		dlpfnErrorHandlers, 
	LPULONG 			lpulErrorFlags, 
	DLPFN_HANDLE_ALARM 		dlpfnAlarmHandlers, 
	LPULONG 			lpulAlarmFlags, 
	DLPFN_HANDLE_EVENT_SET 		dlpfnEventSetHandlers, 
	LPULONG 			lpulEventSetFlags, 
	DLPFN_HANDLE_EVENT_WAIT 	dlpfnEventWaitHandlers, 
	LPULONG 			lpulEventWaitFlags, 
	DLPULONGLONG 			dlpullIdentifiers, 
	LPHANDLE 			lphCritSec, 
	LPHANDLE 			lphEvent, 
	ULONG 				ulFlags)
{
	UNREFERENCED_PARAMETER(dlpfnEventSetHandlers);
	UNREFERENCED_PARAMETER(lpulEventSetFlags);
	UNREFERENCED_PARAMETER(dlpfnEventWaitHandlers);
	UNREFERENCED_PARAMETER(lpulEventWaitFlags);
	HANDLE hObject;
	hObject = CreateHandleWithSingleCriticalSectionControllingMultipleInheritors(	lphDerivatives, 
											ulNumberOfDerivatives, 
											dlphBaseObjects, 
											lpulHandleTypes,
											hCritSec,
											dlpfnDestroyers,
											lpulDestroyFlags,
											dlpfnErrorHandlers,
											lpulErrorFlags,
											dlpfnAlarmHandlers,
											lpulAlarmFlags,
											dlpullIdentifiers,
											ulFlags	);
	EXIT_IF_NULL(hObject, 0);
	
	if (hEvent == NULLPTR)
	{
		hObject->lphEvents = LocalAlloc(sizeof(HANDLE));
		EXIT_IF_NULL(hObject->lphEvents, 0);
		hEvent = CreateEvent(FALSE);
	}
	EXIT_IF_NULL(hEvent, 0);
	hObject->lphEvents[0] = hEvent;

	if (lphCritSec != NULLPTR) *lphCritSec = hObject->hCritSec;
	if (lphEvent != NULLPTR) lphEvent[0] = hObject->lphEvents[0];

	return hObject;
}



PODNET_API
HANDLE
CreateHandleWithSingleCriticalSectionControllingMultipleInheritorsWithMultipleSettableEvents(
	LPHDERIVATIVE 			lphDerivatives, 
	ULONG 				ulNumberOfDerivatives, 
	DLPHANDLE 			dlphBaseObjects, 
	LPULONG 			lpulHandleTypes, 
	HANDLE 				hCritSec, 
	LPHANDLE 			lphEvents, 
	ULONG 				ulNumberOfEvents, 
	DLPFN_HANDLE_DESTROY 		dlpfnDestroyers, 
	LPULONG 			lpulDestroyFlags, 
	DLPFN_HANDLE_ERROR 		dlpfnErrorHandlers, 
	LPULONG 			lpulErrorFlags, 
	DLPFN_HANDLE_ALARM 		dlpfnAlarmHandlers, 
	LPULONG 			lpulAlarmFlags, 
	DLPFN_HANDLE_EVENT_SET 		dlpfnEventSetHandlers, 
	LPULONG				lpulEventSetFlags, 
	DLPFN_HANDLE_EVENT_WAIT 	dlpfnEventWaitHandlers, 
	LPULONG 			lpulEventWaitFlags, 
	DLPULONGLONG 			dlpullIdentifiers, 
	LPHANDLE 			lphCritSec, 
	DLPHANDLE 			dlphEvents, 
	ULONG 				ulFlags
)
{
	UNREFERENCED_PARAMETER(dlpfnEventSetHandlers);
	UNREFERENCED_PARAMETER(lpulEventSetFlags);
	UNREFERENCED_PARAMETER(dlpfnEventWaitHandlers);
	UNREFERENCED_PARAMETER(lpulEventWaitFlags);
	HANDLE hObject;
	ULONG ulIndex;
	hObject = CreateHandleWithSingleCriticalSectionControllingMultipleInheritors(	lphDerivatives, 
											ulNumberOfDerivatives, 
											dlphBaseObjects, 
											lpulHandleTypes,
											hCritSec,
											dlpfnDestroyers,
											lpulDestroyFlags,
											dlpfnErrorHandlers,
											lpulErrorFlags,
											dlpfnAlarmHandlers,
											lpulAlarmFlags,
											dlpullIdentifiers,
											ulFlags	);
	EXIT_IF_NULL(hObject, 0);

	if (lphEvents == NULLPTR)
	{
		hObject->lphEvents = LocalAlloc(sizeof(LPVOID) * ulNumberOfEvents);
		EXIT_IF_NULL(hObject->lphEvents, 0);
		for(ulIndex = 0; ulIndex < ulNumberOfEvents; ulIndex++)
		{
			hObject->lphEvents[ulIndex] = CreateEvent(FALSE);
			EXIT_IF_NULL(hObject->lphEvents[ulIndex], 0);
		}
	}
	else hObject->lphEvents = lphEvents; /* TODO: Add support for memcpy. */
	if (lphCritSec != NULLPTR) *lphCritSec = hObject->hCritSec;
	if (dlphEvents != NULLPTR) *dlphEvents = hObject->lphEvents;
	
	return hObject;
}

/* TODO: Add code to free all of the pointers in a handle... */

/*

.............................................................................................................................'''
...........................................          ...............................................................  ..........
...........................................                     ...   ...   .....................................      .........
.............................................                                ..................................         ........
.............................................               .....            ............................                 ......
..............................................             .'.......         ........................                         ..
....................'.......................              .';;;,,,'..         .........................                      ...
''''..............''''...................                 .:llcccc:,'..   ..  ........................                      ....
''''''..............'''''..''.............              ...lddolllc:;'....'.   .......................                     .....
'..'''''.........''''''''',,,..............             ...,lddollccc;'..''.    ......................                   .......
''''''''''........',,;;;::;;,'..............             ...collodddl;'....        ......................       ................
,'..'''''..........',:llllc;''...............            ...;odoodkko;'....         ......................   ...................
,,''...''''''''..',:clodool;'..................         .....;ddoodxo;'....           ..........................................
;;;,'',;;;,,,:::ccloddddooo:'........................... .....,lddol;'..                ........................................
c:;,',::cclllodddddddl:lolc:'..................................';;,''......                 ....................................
ol:,',;clllodxxxdoxdoc,;;,'''...........................'......    .... ..                .....'................................
ooc;,,,clc;:cclddoddooc;,,,''''........................''..''.........  ..             .......''................................
olc:,;lloolddoldxooddooc:c:;;,,,,;;;'''...............''..'''',;;,....  ....          ........',,''''...........................
lcllcclodooxOkdoxdldxolllool:;;:::cc;,,'''...........'.......',,,;,....  ....         ........';;::;,''.......................''
cclollddddxkxdolooldxdoodxxdl:::::cc:;;,,'''..''.....,'...,;::;,,'....   ...   .     ......'''';:ccc::,''''''...............'',,
ccloloxxkkxo::cooodxkkxxkO0Odolllcllc:::;;,''''',,;,'''.,clll:,'................ .. .......''',;clooooc;,,,,''''.........'',,,;;
::clodxkOkxlclkkxddoxO0KKXX0kxxdlcc:cccc:;;,,,,,'',;::;,'..........  ........... .. ....''''',,;codxxddlc:;,,,,''''....''',,,;;:
:;:codxkkkOOkO0Oxooook0KXXKOkO0kxolcloooocc:;,,,;;cll:,'......    .  .................''',;;::;codxkkkxdol:;,,,,''''''''''',,;;:
:;:clldxkkOO00000OOOkO0KKK0OkO00kxoooddxxdddoollollc:;'........       ..............,,,;::::::cloxkkOOkdolc;;;;;;;;,,''''''',,;:
;;;:clox0OO0KKXXK000O0KKK0000KKKK0OkOOOOO00Okkxxddollc;'.....         .............';:cc:clllccldkO000Ooccc;;;;:cc::;,''''''',,;
,,;cloodk00KXXXXNNXKKKKKK000KXXXXXKKK0000O00Oxxxddool:;,'...          .....'.......';clclodxdddkkO0K00klccc::;;;::cc;;::;;,'',,,
',;:oxxxO00KKXXXNNNNNXXXXXXXXXXXXXXXXKK00OOOkxxxddoolc;,'..            ............,clooddxOOO0000KK0Oxollcc::;;;;::ccllc:;,,,,,
',;coxkOO00KKKXXXXXXXXKXXXXXXXXXXXXXXK000OO0Okxxddolc:,',;.       ..    ..........':clxkOO0KXK0000KK0kxdolllc:::;:;:clol:;;;;;;,
,;:clodxk00OOO00KKKKKKKKKKKKKKKKKXXXXKKK00OOOkdolc:;,,'',;..     ..    ...........;lloxOKKXXNXK0000K0Okxdoolcccllc::ccll:;;;;;;;
,,;:cclodxdooddoodxkO0OkkkOOOOOOOOkkkO0Odollc::;,,'''''';;...    ..    ...........cl:cokKKXNNXK00KKKK0OOkxddooddoc:::cll:;;;;;;;
,,;;:::clollllcccloooolllllllcclllcccloocc:::;;,,''.''.';;..    ...   .......'...'cc;cox0KXNNNXKKKKKK000OOkkkkxdoolccc:::::;;;;;
;,,;;;;;;::;::::::cc:;;;:::::::::::::::::::;;;;,'''.''';;'.. . ....... ......,...,lc:loxOKXNNNXKKKKKKKKK000OOkxddxxdolcc:::;;;;;
;,,,;,,,,,,,,;;;;;;;;;;,;;;;;;;;;:::::;;;;;;;,,,'''''';:'...  ....  ...     .,...;olcldxO0XXNNXXKKKKKKKKK000Okxdoodolccccc:::;;;
,,,,,,,,,,,,,;;;;;;;;;;,,;;;;;;;;;;;;;;;;;;;,,,''''',;:,........       .    .'...,oolodxkO0KXNNXXXXKKKKKK000Okxdoolllcccccc:::;;
,,',,,',,,,,,;;;;;;;;;,,;;;,,;;;;;;;;;;;;;;,,,,,',,,cc;,......              .....'odlodddxk0KXNNXNXXKKKK00000kxdoolllllllc::::;;
,,,''',,,,,,,,;;;;;;;;;;;;;,;;;;;;;;;;;;;;,,,,,,,,:ool:,..  ..            .......'cddloddxxO0KXXXXXXKKKKK000Okxxdoollodolc::::;;
'''''',,,,,,,,,;;;;;;;;;;;;;;;;;;;;;;;;;,,,,,,,,;ldxoc;..                 ........;odollodxkkO0KXXXXXXXXKK0Okdddddooooolc:::;;;;
'''',,,,,,,,,,,,;;;;;;;;;;,,;;;;;;;;;;,,,,,,,,;lxxdl;'..       ....         ......,lddlcclodkkOO0KXX000000Oxdoooollolccc:::;;;;;
,,,'',,,,,,,,,,,,,,;;;;,,,,,,;;;;;;;;,,,,,,,;lxkdc,...        .....         ......':oddlcccllodxxkOK0OOO0Okdllclccccccc:::;;;;;;
,,,,,,,,,,',,,,,,,,,,,,,,,,,;;;;;;;;;;,,,,;lxkdc,....        .....         ........,:oddlcccclloodxkkkkkxxdollcccccc::::::;;;;;,
,'',,,,,,,,,,,,,,,,,,,,,,,,,;;;;;;;,,,,,:oxxdc,... .        ......        ..........,loddocccc::clooddooollccc::::::::::::;;;;,,
,,,,,,,,,,,,,,,,,,,''',,,,,;;;;;;,,,,;coxxoc,......        ...             ..........,codoolc:::::cclllcccc::::::::::::::;;;,,,,
,,,,,,,,,,,,,,,,,,,,',,,,;;;;;;,,,,:ldxdl:,.....         ....                ....   ...;coooollc::::::::::::::::::::::::;;;;,,,;
,,,,;;;,,,,,;;;;,,,,,,,;;;;;;;;,;coxxo:,'......         ....                 .....    ..';ccclllllllllllllllllllllloooooollc::;:
,,,;;;;;;;;;;;;;;;;;,,;;;;;;;;:ldxdl;'.......          .....                  ...      ....',::cllooooloooooolcc::;;;::cclllc:;;
,;;;;;;;;;;;;;::;;;;;;;;;;;;:oxxdc,........           .....                    ..         .....',:::cc:;;::c:::;,.............''
;;;;;;;;;;;;;:::::;;;;;;;;coxxdc,........            .......                                ..........................       ...
;;;;;;;;;;;;;::::::::;:cldkkdl;'......              ........                                          ... ..                    
;;;;;;;;;;;::::::::ccldxkxdl:,'...... ..          .................                                   ..  .....                 
;;;;;;:::::::::::cldxkkxdl:;,''...........       ....................                                                       .. .


The son of giants
Walks across the sky
To the gods a friend
Fathered beast and men

As a falcon he did fly
Far across the sky
To take back the hammer
Of Thor his friend

He lives between the worlds
The world of gods and men

He is LOKI, GOD OF FIRE!


*/


PODNET_API
CALL_TYPE(HOT)
BOOL
DestroyObject(HANDLE hObject)
{
	EXIT_IF_NULL(hObject, FALSE);
	EXIT_IF_NULL(hObject->lpulDerivativeTypes, FALSE);
	BOOL bRet;
	ULONG ulNumberOfIterations, ulIndex;
	bRet = TRUE;
	ulNumberOfIterations = hObject->ulNumberOfEvents;
	if (ulNumberOfIterations > 0)
		for (ulIndex = 0; ulIndex < ulNumberOfIterations; ulIndex++)
			bRet &= DestroyHandle(hObject->lphEvents[ulIndex]);
	ulNumberOfIterations = hObject->ulNumberOfDerivatives;
	if (hObject->hCritSec != NULLPTR) DestroyHandle(hObject->hCritSec);
	for (ulIndex = 0; ulIndex < ulNumberOfIterations; ulIndex++)
	{ bRet &= hObject->dlpfnDestroyers[ulIndex](hObject->lphDerivatives[ulIndex], hObject->lpulDestroyFlags[ulIndex]); }
	if (hObject->dlpfnAlarmHandlers != NULLPTR)
		FreeMemory(hObject->dlpfnAlarmHandlers);
	if (hObject->dlpfnDestroyers != NULLPTR)
		FreeMemory(hObject->dlpfnDestroyers);
	if (hObject->dlpfnErrorHandlers != NULLPTR)
		FreeMemory(hObject->dlpfnErrorHandlers);
	if (hObject->dlpfnEventSetHandlers != NULLPTR)
		FreeMemory(hObject->dlpfnEventSetHandlers);
	if (hObject->dlpfnEventWaitHandlers != NULLPTR)
		FreeMemory(hObject->dlpfnEventWaitHandlers);
	if (hObject->dlpfnReleaseHandlers != NULLPTR)
		FreeMemory(hObject->dlpfnReleaseHandlers);
	if (hObject->lphDerivatives != NULLPTR)
		FreeMemory(hObject->lphDerivatives);
	if (hObject->lpulAlarmFlags != NULLPTR)
		FreeMemory(hObject->lpulAlarmFlags);
	if (hObject->lpulDerivativeTypes != NULLPTR)
		FreeMemory(hObject->lpulDerivativeTypes);
	if (hObject->lpulDestroyFlags != NULLPTR)
		FreeMemory(hObject->lpulDestroyFlags);
	if (hObject->lpulErrorFlags != NULLPTR)
		FreeMemory(hObject->lpulErrorFlags);
	if (hObject->lpulEventSetFlags != NULLPTR)
		FreeMemory(hObject->lpulEventSetFlags);
	if (hObject->lpulEventWaitFlags != NULLPTR)
		FreeMemory(hObject->lpulEventWaitFlags);
	if (hObject->lpullIndexes != NULLPTR)
		FreeMemory(hObject->lpullIndexes);
	if (hObject->lphEvents != NULLPTR)
		FreeMemory(hObject->lphEvents);

	ZeroMemory(hObject, sizeof(OBJECT));

	FreeMemory(hObject);

	InterlockedDecrement(&ualTotalNumberOfObjects);

	return bRet;
}




_Success_(return != NULL, _Interlocked_Operation_)
PODNET_API
UARCHLONG
GetTotalNumberOfObjects(
	VOID
){ return (UARCHLONG)InterlockedAcquire(&ualTotalNumberOfObjects); }




PODNET_API
CALL_TYPE(HOT)
HDERIVATIVE
GetHandleDerivative(HANDLE RESTRICT hObject)
{
	EXIT_IF_UNLIKELY_NULL(hObject, FALSE);
	return *(hObject->lphDerivatives);
}


PODNET_API
HDERIVATIVE
GetHandleDerivativeAtIndex(HANDLE hObject, ULONGLONG ullIndex)
{
	EXIT_IF_NULL(hObject, 0);
	if (ullIndex >= hObject->ulNumberOfDerivatives) return 0;
	return hObject->lphDerivatives[ullIndex];
}




PODNET_API
BOOL
SetHandleDestroyFunction(HANDLE hObject, LPFN_HANDLE_DESTROY lpfnDestroy, ULONG ulDestroyFlags)
{
	EXIT_IF_NULL(hObject, FALSE);
	EXIT_IF_NULL(lpfnDestroy, FALSE);
	hObject->dlpfnDestroyers[0] = lpfnDestroy;
	hObject->lpulDestroyFlags[0] = ulDestroyFlags;
	return TRUE;
}


PODNET_API
BOOL
SetHandleDestroyFunctionAtIndex(HANDLE hObject, ULONGLONG ullIndex, LPFN_HANDLE_DESTROY lpfnDestroy, ULONG ulDestroyFlags)
{
	EXIT_IF_NULL(hObject, FALSE);
	EXIT_IF_NULL(lpfnDestroy, FALSE);
	if (ullIndex > hObject->ulNumberOfDerivatives) return FALSE;
	hObject->dlpfnDestroyers[ullIndex] = lpfnDestroy;
	hObject->lpulDestroyFlags[ullIndex] = ulDestroyFlags;
	return TRUE;
}




PODNET_API
LPFN_HANDLE_DESTROY
GetHandleDestroyFunction(HANDLE hObject, LPULONG lpulFlagsOut)
{
	EXIT_IF_NULL(hObject, FALSE);
	if (lpulFlagsOut)
		*lpulFlagsOut = hObject->lpulDestroyFlags[0];
	return hObject->dlpfnDestroyers[0];
}


PODNET_API
LPFN_HANDLE_DESTROY
GetHandleDestroyFunctionAtIndex(HANDLE hObject, ULONGLONG ullIndex, LPULONG lpulFlagsOut)
{
	EXIT_IF_NULL(hObject, FALSE);
	if (ullIndex > hObject->ulNumberOfDerivatives) return 0;
	if (lpulFlagsOut)
		*lpulFlagsOut = hObject->lpulDestroyFlags[ullIndex];
	return hObject->dlpfnDestroyers[ullIndex];
}


_Success_(return != NULLPTR, {})
PODNET_API
HANDLE
GetObjectEvent(
	_In_		HANDLE			hObject
){
	EXIT_IF_UNLIKELY_NULL(hObject, NULLPTR);
	if (hObject->lphEvents != NULLPTR)
		return hObject->lphEvents[0];
	return NULLPTR;
}




_Success_(return != FALSE, _Acquires_Lock_())
PODNET_API
BOOL
SetHandleLockFunctions(
	_In_			HANDLE			hObject,
	_In_  _Call_Back_	LPFN_LOCK_PROC		lpfnLockProc,
	_In_  _Call_Back_	LPFN_UNLOCK_PROC	lpfnUnlockProc,
	_In_Opt_  _Call_Back_	LPFN_TRY_LOCK_PROC	lpfnTryLockProc
){
	EXIT_IF_UNLIKELY_NULL(hObject, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpfnLockProc, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpfnUnlockProc, FALSE);

	hObject->lpfnLockProc = lpfnLockProc;
	hObject->lpfnUnlockProc = lpfnUnlockProc;
	if (lpfnTryLockProc != NULLPTR)
		hObject->lpfnTryLockProc = lpfnTryLockProc;
	return TRUE;
}

PODNET_API
ULONG
GetHandleType(HANDLE RESTRICT hObject)
{
	EXIT_IF_NULL(hObject, 0);
	return hObject->lpulDerivativeTypes[0];
}


PODNET_API
ULONG
GetHandleTypeAtIndex(HANDLE hObject, ULONGLONG ullIndex)
{
	EXIT_IF_NULL(hObject, 0);
	if (ullIndex > hObject->ulNumberOfDerivatives) return 0;
	return hObject->lpulDerivativeTypes[ullIndex];
}


PODNET_API
BOOL
HandleHasMultipleInheritors(HANDLE hObject)
{
	return ((hObject->ulNumberOfDerivatives > 1) ? TRUE : FALSE);
}



PODNET_API
ULONGLONG
GetNumberOfHandleDerivatives(HANDLE hObject)
{
	return hObject->ulNumberOfDerivatives;
}


PODNET_API
BOOL
LockHandle(HANDLE hObject)
{
	EXIT_IF_NULL(hObject, FALSE);
	BOOL bRet;
	if (hObject->lpfnLockProc != NULLPTR) 
		bRet = hObject->lpfnLockProc(hObject);
	else if (hObject->hCritSec != NULLPTR)
		bRet = EnterCriticalSection(hObject->hCritSec);
	else return FALSE;
	return bRet;
}


PODNET_API
BOOL
TryLockHandle(HANDLE hObject)
{
	EXIT_IF_NULL(hObject, FALSE);
	BOOL bRet;
	if (hObject->lpfnTryLockProc != NULLPTR)
		bRet = hObject->lpfnTryLockProc(hObject);
	else if (hObject->hCritSec != NULLPTR)
		bRet = TryToEnterCriticalSection(hObject->hCritSec);
	else return FALSE;
	return bRet;
}


PODNET_API
BOOL
UnlockHandle(HANDLE hObject)
{
	EXIT_IF_NULL(hObject, FALSE);
	BOOL bRet;
	if (hObject->lpfnUnlockProc != NULLPTR)
		bRet = hObject->lpfnUnlockProc(hObject);
	else if (hObject->hCritSec != NULLPTR)
		bRet = ExitCriticalSection(hObject->hCritSec);
	else return FALSE;
	return bRet;
}


PODNET_API
ULONG
GetHandleDerivativeType(HANDLE hObject)
{
	EXIT_IF_NULL(hObject, 0);
	return hObject->lpulDerivativeTypes[0];
}


PODNET_API
BOOL
GetHandleInformation(
	HANDLE			hObject,
	DLPHDERIVATIVE		dlphDerivatives,
	DLPULONG		dlpulDerivativeTypes,
	DLPULONGLONG		dlpullIndexes,
	TLPFN_HANDLE_DESTROY	tlpfnDestroyers,
	DLPULONG		dlpulDestroyFlags,
	TLPFN_HANDLE_ALARM	tlpfnAlarmHandlers,
	DLPULONG		dlpulAlarmFlags,
	TLPFN_HANDLE_ERROR	tlpfnErrorHandlers,
	DLPULONG		dlpulErrorFlags,
	TLPFN_HANDLE_EVENT_SET	tlpfnEventSetHandlers,
	DLPULONG		dlpulEventSetFlags,
	TLPFN_HANDLE_EVENT_WAIT	tlpfnEventWaitHandlers,
	DLPULONG		dlpulEventWaitFlags,
	LPHANDLE		lphCritSec,
	DLPHANDLE		dlphEvents,
	LPULONG			lpulNumberOfEvents	
)
{
	EXIT_IF_NULL(hObject, FALSE);

	if (dlphDerivatives != NULLPTR)
		*dlphDerivatives = hObject->lphDerivatives;
	
	if (dlpulDerivativeTypes != NULLPTR)
		*dlpulDerivativeTypes = hObject->lpulDerivativeTypes;

	if (dlpullIndexes != NULLPTR)
		*dlpullIndexes = hObject->lpullIndexes;

	if (tlpfnDestroyers != NULLPTR)
		*tlpfnDestroyers = hObject->dlpfnDestroyers;

	if (dlpulDestroyFlags != NULLPTR)
		*dlpulDestroyFlags = hObject->lpulDestroyFlags;

	if (tlpfnAlarmHandlers != NULLPTR)
		*tlpfnAlarmHandlers = hObject->dlpfnAlarmHandlers;

	if (dlpulAlarmFlags != NULLPTR)
		*dlpulAlarmFlags = hObject->lpulAlarmFlags;

	if (tlpfnErrorHandlers != NULLPTR)
		*tlpfnErrorHandlers = hObject->dlpfnErrorHandlers;

	if (dlpulErrorFlags != NULLPTR)
		*dlpulErrorFlags = hObject->lpulErrorFlags;

	if (tlpfnEventSetHandlers != NULLPTR)
		*tlpfnEventSetHandlers = hObject->dlpfnEventSetHandlers;
	
	if (dlpulEventSetFlags != NULLPTR)
		*dlpulEventSetFlags = hObject->lpulEventSetFlags;

	if (tlpfnEventWaitHandlers != NULLPTR)
		*tlpfnEventWaitHandlers = hObject->dlpfnEventWaitHandlers;

	if (dlpulEventWaitFlags != NULLPTR)
		*dlpulEventWaitFlags = hObject->lpulEventWaitFlags;

	if (lphCritSec != NULLPTR)
		*lphCritSec = hObject->hCritSec;
	
	if (dlphEvents != NULLPTR)
		*dlphEvents = hObject->lphEvents;
	
	if (lpulNumberOfEvents != NULLPTR)
		*lpulNumberOfEvents = hObject->ulNumberOfEvents;

	return TRUE;
}




_Success_(return != FALSE, _Interlocked_Operation_) 
PODNET_API
BOOL
IncrementObjectReferenceCount(
	_In_ 		HANDLE RESTRICT 	hObject
){
	EXIT_IF_UNLIKELY_NULL(hObject, FALSE);
	InterlockedIncrement(&(hObject->ualReferenceCount));
	return TRUE;
}




_Success_(return != FALSE, _Interlocked_Operation_)
PODNET_API
BOOL 
DecrementObjectReferenceCount(
	_In_ 		HANDLE RESTRICT 	hObject
){
	EXIT_IF_UNLIKELY_NULL(hObject, FALSE);
	BOOL bRet;
	bRet = TRUE;
	
	InterlockedDecrement(&(hObject->ualReferenceCount));
	
	if (0 == InterlockedAcquire(&(hObject->ualReferenceCount)))
	{ 
		if (FALSE == bCollectGarbage)
		{ bRet &= DestroyObject(hObject); }
		else
		{ bRet &= Dispose(hObject); }
	}
	
	return bRet;
}




_Success_(return != NULL, _Interlocked_Operation_)
PODNET_API
UARCHLONG 
GetObjectReferenceCount(
	_In_ 		HANDLE RESTRICT 	hObject
){
	EXIT_IF_UNLIKELY_NULL(hObject, NULL);
	return hObject->ualReferenceCount;
}




_Calls_Call_Back_
_Success_(return != FALSE, _Releases_Lock_(hObject->lphEvent[0], _Lock_Kind_Event_))
PODNET_API
BOOL
TriggerSingleObject(
	HANDLE			hObject,
	LPVOID			lpParam,
	ULONG			ulFlags
){
	EXIT_IF_UNLIKELY_NULL(hObject, FALSE);
	EXIT_IF_NULL(hObject->lphEvents, FALSE);
	EXIT_IF_NULL(hObject->lphEvents[0], FALSE);
	BOOL bRet;

	/* Default Implementation if NULLPTR */
	if (hObject->dlpfnEventSetHandlers == NULLPTR)
	{ bRet = SignalEvent(hObject->lphEvents[0]); }
	else
	{ bRet = hObject->dlpfnEventSetHandlers[0](hObject, lpParam, ulFlags); }
	return bRet;
}




_Calls_Call_Back_
_Success_(return != FALSE, ...)
PODNET_API
BOOL
ResetObject(	
	HANDLE			hObject
){
	EXIT_IF_UNLIKELY_NULL(hObject, FALSE);
	EXIT_IF_NULL(hObject->lphEvents, FALSE);
	EXIT_IF_NULL(hObject->lphEvents[0], FALSE);

	return ResetEvent(hObject->lphEvents[0]);
}




_Success_(return != FALSE, ...)
PODNET_API 
BOOL
SetHandleReadAndWriteProcs(
	_In_ 		HANDLE 			hObject,
	_In_Opt_ 	LPFN_HANDLE_READ_PROC 	lpfnReadProc,
	_In_Opt_ 	LPFN_HANDLE_WRITE_PROC 	lpfnWriteProc 
){
	EXIT_IF_UNLIKELY_NULL(hObject, FALSE);
	hObject->lpfnReadProc = lpfnReadProc;
	hObject->lpfnWriteProc = lpfnWriteProc;
	return TRUE;
}




_Calls_Call_Back_
_Success_(return != FALSE, ...)
PODNET_API
LPVOID
Write(
	_In_ 			HANDLE 			hObject,
	_In_ 			LPVOID 			lpData,
	_In_ 			ULONGLONG 		ullSize,
	_In_Opt_ 		LPVOID 			lpParam,
	_In_Opt_ 		ULONG 			ulFlags 
){
	EXIT_IF_UNLIKELY_NULL(hObject, FALSE);
	EXIT_IF_NULL(hObject->lpfnWriteProc, FALSE);
	return hObject->lpfnWriteProc(hObject, lpData, ullSize, lpParam, ulFlags);
}




_Calls_Call_Back_
_Success_(return != FALSE, ...)
PODNET_API
LPVOID
Read(
	_In_ 		HANDLE 			hObject,
	_In_ 		LPVOID 			lpBuffer,
	_In_ 		ULONGLONG 		ullSize,
	_In_Opt_ 	LPVOID 			lpParam,
	_In_Opt_ 	ULONG 			ulFlags
){
	EXIT_IF_UNLIKELY_NULL(hObject, FALSE);
	EXIT_IF_NULL(hObject->lpfnReadProc, FALSE);
	return hObject->lpfnReadProc(hObject, lpBuffer, ullSize, lpParam, ulFlags);
}




_Success_(return != FALSE, _Interlocked_Operation_)
PODNET_API
BOOL
EnableGarbageCollection(
	VOID
){
	if (0 == GetTotalNumberOfObjects())
	{ 
		InitializeGarbageCollector(NULL);
		return InterlockedCompareExchangeHappened(&(bCollectGarbage), FALSE, TRUE); 
	}
	return FALSE;
}



#endif
