/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include <errno.h>
#include "CPoller.h"
#include "../CFile/CFileObject.c"
#include "../CNetworking/CSocketObject.c"
#ifdef __FreeBSD__
#include "../CContainers/CBinarySearchTree.h"
#endif


#define MAX_NUMBER_EVENTS		0x100

#ifdef __FreeBSD__
typedef struct kevent			POLL_EVENT, *LPPOLL_EVENT;
#else
typedef struct epoll_event              POLL_EVENT, *LPPOLL_EVENT;
#endif




typedef struct __POLLER 
{
	INHERITS_FROM_HANDLE();
	HANDLE          hLock;
	HVECTOR		hvEvents;
	HVECTOR 	hvResults;
	HVECTOR 	hvResultsEx;
	#ifdef __FreeBSD__
	HBST 		hbstEvents;
	#endif
	LONG            lFd;
} POLLER, *LPPOLLER;




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__DestroyPollResultAntiDeadLock(
	_In_ 		LPPOLLER	lpPoller
){
	EXIT_IF_UNLIKELY_NULL(lpPoller, FALSE);

	if (NULLPTR == lpPoller->hvResults || NULLPTR == lpPoller->hvResultsEx)
	{ return FALSE; }

	DestroyVector(lpPoller->hvResults);
	DestroyVector(lpPoller->hvResultsEx);
	lpPoller->hvResults = NULLPTR;

	return TRUE;
}




#ifdef __FreeBSD__
_Success_(return == 0, _Non_Locking_)
INTERNAL_OPERATION
LONG 
__PollEventCompare(
	_In_ 		LPPOLL_EVENT	lppeEvt,
	_In_ 		LPPOLL_EVENT 	lppeComparand
){
	if (lppeComparand->ext[0] == lppeEvt->ext[0])
	{ return 0; }
	else if (lppeComparand->ext[0] < lppeEvt->ext[0])
	{ return -1; }
	else 
	{ return 1;}

	return MAX_LONG;
}


_Success_(return == NULLPTR, _Non_Locking_)
INTERNAL_OPERATION
LPVOID 
__TraverseTree(
	_In_ 		HBINARYSEARCHTREE	hbstData,
	_In_ 		LPVOID 			lpNode, 
	_In_ 		LPVOID 			lpData, 
	_In_Opt_ 	LPVOID 			lpUserData
){
	UNREFERENCED_PARAMETER(hbstData);
	UNREFERENCED_PARAMETER(lpNode);

	LPPOLL_EVENT lppeEvt;
	HVECTOR hvEvents;

	lppeEvt = lpData;
	hvEvents = lpUserData;

	VectorPushBack(hvEvents, lppeEvt, 0);
	return NULLPTR;
}

#endif




_Success_(return != FALSE, _Acquires_Shared_Lock_())
INTERNAL_OPERATION
BOOL
__DestroyPoller(
	_In_ 		HDERIVATIVE 		hDerivative,
	_In_Opt_ 	ULONG 			ulFlags
){
	UNREFERENCED_PARAMETER(ulFlags);
	EXIT_IF_UNLIKELY_NULL(hDerivative, FALSE);

	LPPOLLER lpPoller;
	lpPoller = (LPPOLLER)hDerivative;

	if (0 != lpPoller->lFd)
	{ close(lpPoller->lFd); }

	if (NULL_OBJECT != lpPoller->hLock)
	{ DestroyObject(lpPoller->hLock); }

	if (NULLPTR != lpPoller->hvEvents)
	{ DestroyVector(lpPoller->hvEvents); }

	#ifdef __FreeBSD__
	DestroyBinarySearchTree(lpPoller->hbstEvents);
	#endif

	FreeMemory(lpPoller);
	return TRUE;
}




_Success_(return != 0, _Non_Locking_)
PODNET_API
BOOL
__Poll(
	_In_ 		HANDLE 			hPoller,
	_In_ 		ULONGLONG 		ullWaitTime
){
	EXIT_IF_UNLIKELY_NULL(hPoller, 0);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hPoller), HANDLE_TYPE_POLLER, 0);
	
	LPPOLLER lpPoller;
	POLL_EVENT lppeEvt[MAX_NUMBER_EVENTS];
	ARCHLONG alCount, alIndex;

	lpPoller = GetHandleDerivative(hPoller);
	EXIT_IF_UNLIKELY_NULL(lpPoller, 0);

	ZeroMemory(lppeEvt, sizeof(POLL_EVENT) * MAX_NUMBER_EVENTS);

	WaitForSingleObject(lpPoller->hLock, ullWaitTime);

	#ifdef __FreeBSD__
	TIMESPEC tmSpec;
	CreateTimeSpec(&tmSpec, ullWaitTime);

	alCount = kevent(
		lpPoller->lFd, 
		NULLPTR,
		0,
		lppeEvt,
		MAX_NUMBER_EVENTS,
		(CONSTANT LPTIMESPEC)&tmSpec
	);
	#else
	alCount = epoll_wait(lpPoller->lFd, lppeEvt, MAX_NUMBER_EVENTS, ullWaitTime);
	#endif

	if (-1 == alCount)
	{ 
		ReleaseSingleObject(lpPoller->hLock);
		return FALSE;
	}

	__DestroyPollResultAntiDeadLock(lpPoller);

	lpPoller->hvResults = CreateVector(alCount, sizeof(HANDLE), 0);
	lpPoller->hvResultsEx = CreateVector(alCount, sizeof(POLL_DESCRIPTOR), 0);

	for (
		alIndex = 0;
		alIndex < alCount;
		alIndex++
	){ 
		POLL_DESCRIPTOR pdTemp;
		#ifdef __FreeBSD__
		pdTemp.hFile = lppeEvt[alIndex].udata;
		pdTemp.ulEvent = lppeEvt[alIndex].filter;
		VectorPushBack(lpPoller->hvResults, &(lppeEvt[alIndex].udata), 0);
		#else
		pdTemp.hFile = lppeEvt[alIndex].data.ptr;
		pdTemp.ulEvent = lppeEvt[alIndex].events;
		VectorPushBack(lpPoller->hvResults, &(lppeEvt[alIndex].data.ptr), 0);
		#endif
		VectorPushBack(lpPoller->hvResultsEx, &(pdTemp), 0); 
	}

	ReleaseSingleObject(lpPoller->hLock);

	return (0 != alCount) ? TRUE : FALSE;
}




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNET_API
HANDLE
CreatePoller(
	VOID       
){
	HANDLE hPoller;
	LPPOLLER lpPoller;

	lpPoller = GlobalAllocAndZero(sizeof(POLLER));
	/* cppcheck-suppress memleak */
	EXIT_IF_UNLIKELY_NULL(lpPoller, NULL_OBJECT);

	#ifdef __FreeBSD__
	lpPoller->lFd = kqueue();
	if (-1 == lpPoller->lFd)
	{
		FreeMemory(lpPoller);
		return NULL_OBJECT;
	}

	lpPoller->hbstEvents = CreateBinarySearchTree(
		0,
		sizeof(POLL_EVENT),
		(LPFN_BS_TREE_COMPARE_PROC)__PollEventCompare,
		NULLPTR
	);
	#else
	lpPoller->lFd = epoll_create1(0);	
	if (-1 == lpPoller->lFd)
	{
		FreeMemory(lpPoller);
		return NULL_OBJECT; 
	}
	#endif

	lpPoller->hvEvents = CreateVector(10, sizeof(POLL_EVENT), 0);
	if (NULLPTR == lpPoller->hvEvents)
	{
		close(lpPoller->lFd);
		FreeMemory(lpPoller);
		return NULL_OBJECT; 
	}

	lpPoller->hLock = CreateCriticalSection();
	if (NULL_OBJECT == lpPoller->hLock)
	{
		DestroyVector(lpPoller->hvEvents);
		close(lpPoller->lFd);
		FreeMemory(lpPoller);
		return NULL_OBJECT; 
	}

	hPoller = CreateHandleWithSingleInheritor(
		lpPoller,
		&(lpPoller->hThis),
		HANDLE_TYPE_POLLER,
		__DestroyPoller,
		0,
		NULLPTR,
		0,
		NULLPTR,
		0,
		&(lpPoller->ullId),
		0
	);

	if (NULL_OBJECT == hPoller)
	{
		DestroyObject(lpPoller->hLock);
		DestroyVector(lpPoller->hvEvents);
		close(lpPoller->lFd);
		FreeMemory(lpPoller);
		return NULL_OBJECT; 
	}

	SetHandleEventWaitFunction(hPoller, __Poll, 0);

	return hPoller;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
AddFileToPoller(
	_In_            HANDLE			hPoller,
	_In_ 		HANDLE 			hFile
){
	EXIT_IF_UNLIKELY_NULL(hPoller, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hPoller), HANDLE_TYPE_POLLER, FALSE);
	
	LPPOLLER lpPoller;
	POLL_EVENT peEvt;
	ULONG ulType;
	LONG lFd;

	lpPoller = GetHandleDerivative(hPoller);
	ulType = GetHandleDerivativeType(hFile);

	WaitForSingleObject(lpPoller->hLock, INFINITE_WAIT_TIME);
	
	#ifdef __FreeBSD__
	switch (ulType)
	{

		case HANDLE_TYPE_FILE:
		{
			LPFILE_OBJ lpFile;
			
			lpFile = GetHandleDerivative(hFile);
			EXIT_IF_UNLIKELY_NULL(lpFile, FALSE);

			lFd = fileno(lpFile->fFile);
			peEvt.udata = hFile;

			break;
		}

		case HANDLE_TYPE_SOCKET:
		{
			LPSOCKET lpSocket;
			lpSocket = GetHandleDerivative(hFile);

			lFd = lpSocket->lDescriptor;
			peEvt.udata = hFile;

			break;
		}

		case HANDLE_TYPE_SOCKET6:
		{
			LPSOCKET6 lpSocket6;
			lpSocket6 = GetHandleDerivative(hFile);

			lFd = lpSocket6->lDescriptor;
			peEvt.udata = hFile;

			break;
		}

		default: 
		{
			ReleaseSingleObject(lpPoller->hLock);
			return FALSE;
		}
	}

	EV_SET(
		&peEvt,			/* Event */
		lFd,			/* ID */
		EVFILT_READ,		/* Allow Reading */
		EV_ADD,			/* Add And Enable */
		0,			/* NULL */
		0,			/* Param */
		peEvt.udata		/* HANDLE To File */
	);
	peEvt.ext[0] = lFd;
	peEvt.ext[1] = lpPoller->lFd;

	VectorPushBack(lpPoller->hvEvents, &peEvt, 0);
	kevent(
		lpPoller->lFd,
		&peEvt,
		1,
		NULLPTR,
		0,
		NULLPTR
	);
	/* BinarySearchTreeInsert(lpPoller->hbstEvents, &peEvt, sizeof(POLL_EVENT)); */

	#else

	/* Linux */
	switch (ulType)
	{

		case HANDLE_TYPE_FILE:
		{
			LPFILE_OBJ lpFile;
			
			lpFile = GetHandleDerivative(hFile);
			EXIT_IF_UNLIKELY_NULL(lpFile, FALSE);

			lFd = fileno(lpFile->fFile);
			peEvt.data.ptr = hFile;

			break;
		}

		case HANDLE_TYPE_SOCKET:
		{
			LPSOCKET lpSocket;
			lpSocket = GetHandleDerivative(hFile);

			lFd = lpSocket->lDescriptor;
			peEvt.data.ptr = hFile;

			break;
		}

		case HANDLE_TYPE_SOCKET6:
		{
			LPSOCKET6 lpSocket6;
			lpSocket6 = GetHandleDerivative(hFile);

			lFd = lpSocket6->lDescriptor;
			peEvt.data.ptr = hFile;

			break;
		}

		default: 
		{
			ReleaseSingleObject(lpPoller->hLock);
			return FALSE;
		}
	}

	peEvt.events = EPOLLIN;

	epoll_ctl(lpPoller->lFd, EPOLL_CTL_ADD, lFd, &peEvt);
	/* VectorPushBack(lpPoller->hvEvents, &peEvt, 0); */
	#endif

	ReleaseSingleObject(lpPoller->hLock);

	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
AddFileToPollerEx(
	_In_            HANDLE			hPoller,
	_In_ 		HANDLE 			hFile,
	_In_		ULONG 			ulFlags
){
	EXIT_IF_UNLIKELY_NULL(hPoller, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hPoller), HANDLE_TYPE_POLLER, FALSE);
	
	LPPOLLER lpPoller;
	POLL_EVENT peEvt;
	ULONG ulType;
	LONG lFd;

	lpPoller = GetHandleDerivative(hPoller);
	ulType = GetHandleDerivativeType(hFile);

	WaitForSingleObject(lpPoller->hLock, INFINITE_WAIT_TIME);

	#ifdef __FreeBSD__
	
	switch (ulType)
	{

		case HANDLE_TYPE_FILE:
		{
			LPFILE_OBJ lpFile;
			
			lpFile = GetHandleDerivative(hFile);
			EXIT_IF_UNLIKELY_NULL(lpFile, FALSE);

			lFd = fileno(lpFile->fFile);
			peEvt.udata = hFile;

			break;
		}

		case HANDLE_TYPE_SOCKET:
		{
			LPSOCKET lpSocket;
			lpSocket = GetHandleDerivative(hFile);

			lFd = lpSocket->lDescriptor;
			peEvt.udata = hFile;

			break;
		}

		case HANDLE_TYPE_SOCKET6:
		{
			LPSOCKET6 lpSocket6;
			lpSocket6 = GetHandleDerivative(hFile);

			lFd = lpSocket6->lDescriptor;
			peEvt.udata = hFile;

			break;
		}

		default: 
		{
			ReleaseSingleObject(lpPoller->hLock);
			return FALSE;
		}
	}

	EV_SET(
		&peEvt,			/* Event */
		lpPoller->lFd,		/* File Descriptor */
		ulFlags,		/* Flags */
		EV_ADD | EV_ENABLE,	/* Add And Enable */
		0,
		0,			/* Param */
		peEvt.udata		/* HANDLE To File */
	);
	peEvt.ext[0] = lFd;

	VectorPushBack(lpPoller->hvEvents, &peEvt, 0);
	/* BinarySearchTreeInsert(lpPoller->hbstEvents, &peEvt, sizeof(POLL_EVENT)); */


	#else
	/* Linux */
	switch (ulType)
	{

		case HANDLE_TYPE_FILE:
		{
			LPFILE_OBJ lpFile;
			
			lpFile = GetHandleDerivative(hFile);
			EXIT_IF_UNLIKELY_NULL(lpFile, FALSE);

			lFd = fileno(lpFile->fFile);
			peEvt.data.ptr = hFile;

			break;
		}

		case HANDLE_TYPE_SOCKET:
		{
			LPSOCKET lpSocket;
			lpSocket = GetHandleDerivative(hFile);

			lFd = lpSocket->lDescriptor;
			peEvt.data.ptr = hFile;

			break;
		}

		case HANDLE_TYPE_SOCKET6:
		{
			LPSOCKET6 lpSocket6;
			lpSocket6 = GetHandleDerivative(hFile);

			lFd = lpSocket6->lDescriptor;
			peEvt.data.ptr = hFile;

			break;
		}

		default: 
		{
			ReleaseSingleObject(lpPoller->hLock);
			return FALSE;
		}
	}

	peEvt.events = ulFlags;

	epoll_ctl(lpPoller->lFd, EPOLL_CTL_ADD, lFd, &peEvt);
	/* VectorPushBack(lpPoller->hvEvents, &peEvt, 0); */

	#endif

	ReleaseSingleObject(lpPoller->hLock);

	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
RemoveFileFromPoller(
	_In_            HANDLE			hPoller,
	_In_ 		HANDLE 			hFile
){
	EXIT_IF_UNLIKELY_NULL(hPoller, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hPoller), HANDLE_TYPE_POLLER, FALSE);
	
	LPPOLLER lpPoller;
	ULONG ulType;
	LONG lFd;

	lFd = -1;

	lpPoller = GetHandleDerivative(hPoller);
	ulType = GetHandleDerivativeType(hFile);

	WaitForSingleObject(lpPoller->hLock, INFINITE_WAIT_TIME);

	#ifdef __FreeBSD__
	/*
	UNREFERENCED_VARIABLE(ulType);
	UNREFERENCED_VARIABLE(lFd);
	LPVOID lpNode;

	lpNode = BinarySearchTreeSearch(
		lpPoller->hbstEvents,
		hFile, 
		sizeof(POLL_EVENT)
	);

	if (NULLPTR == lpNode)
	{ 
		ReleaseSingleObject(lpPoller->hLock);
		return FALSE; 
	}

	BinarySearchTreeDestroyAt(
		lpPoller->hbstEvents,
		lpNode
	);
	*/

	UARCHLONG ualIndex;

	switch (ulType)
	{

		case HANDLE_TYPE_FILE:
		{
			LPFILE_OBJ lpFile;
			lpFile = GetHandleDerivative(hFile);

			if (NULLPTR != lpFile)
			{ lFd = fileno(lpFile->fFile); }

			break;
		}

		case HANDLE_TYPE_SOCKET:
		{
			LPSOCKET lpSocket;
			lpSocket = GetHandleDerivative(hFile);

			if (NULLPTR != lpSocket)
			{ lFd = lpSocket->lDescriptor; } 

			break;
		}

		case HANDLE_TYPE_SOCKET6:
		{
			LPSOCKET6 lpSocket6;
			lpSocket6 = GetHandleDerivative(hFile);

			if (NULLPTR != lpSocket6)
			{ lFd = lpSocket6->lDescriptor; }

			break;
		}

		default: 
		{
			ReleaseSingleObject(lpPoller->hLock);
			return FALSE;
		}
	}

	/*
	for (
		ualIndex = 0;
		ualIndex < VectorSize(lpPoller->hvEvents);
		ualIndex++
	){
		LPPOLL_EVENT lppeEvt;
		lppeEvt = NULLPTR;

		lppeEvt = (LPPOLL_EVENT)VectorAt(lpPoller->hvEvents, ualIndex);
		if (NULLPTR == lppeEvt)
		{ continue; }

		if (lFd == (LONG)lppeEvt->ext[0])
		{ 
			UARCHLONG ualRemainingSize;

			ZeroMemory(lppeEvt, sizeof(POLL_EVENT)); 

			ualRemainingSize = sizeof(POLL_EVENT) * (VectorSize(lpPoller->hvEvents) - ualIndex);
			MoveMemory(lppeEvt, (LPVOID)((UARCHLONG)lppeEvt + sizeof(POLL_EVENT)), ualRemainingSize);
			VectorShrinkToFit(lpPoller->hvEvents);
		}

	}
	*/

	if (-1 != lFd)
	{
		POLL_EVENT peEvt;
		EV_SET(
			&peEvt,			/* Event */
			lFd,			/* ID */
			EVFILT_READ,		/* Allow Reading */
			EV_DELETE,			/* Add And Enable */
			0,			/* NULL */
			0,			/* Param */
			peEvt.udata		/* HANDLE To File */
		);

		kevent(
			lpPoller->lFd,
			&peEvt,
			1,
			NULLPTR,
			0,
			NULLPTR
		);
	}
	
	#else
	/* Linux */
	switch (ulType)
	{

		case HANDLE_TYPE_FILE:
		{
			LPFILE_OBJ lpFile;
			lpFile = GetHandleDerivative(hFile);
			
			if (NULLPTR != lpFile)
			{ lFd = fileno(lpFile->fFile); }

			break;
		}

		case HANDLE_TYPE_SOCKET:
		{
			LPSOCKET lpSocket;
			lpSocket = GetHandleDerivative(hFile);

			if (NULLPTR != lpSocket)
			{ lFd = lpSocket->lDescriptor; } 

			break;
		}

		case HANDLE_TYPE_SOCKET6:
		{
			LPSOCKET6 lpSocket6;
			lpSocket6 = GetHandleDerivative(hFile);

			if (NULLPTR != lpSocket6)
			{ lFd = lpSocket6->lDescriptor; }
			break;
		}

		default: 
		{
			ReleaseSingleObject(lpPoller->hLock);
			return FALSE;
		}
	}

	if (-1 != lFd)
	{ epoll_ctl(lpPoller->lFd, EPOLL_CTL_DEL, lFd, NULLPTR); }
	#endif

	ReleaseSingleObject(lpPoller->hLock);

	if (-1 != lFd)
	{ return TRUE; }
	return FALSE;
}




_Success_(return != NULLPTR, _Acquires_Shared_Lock_())
PODNET_API
UARCHLONG
GetPollResult(
	_In_ 		HANDLE 			hPoller,
	_Out_ 		DLPHANDLE		dlphResults
){
	EXIT_IF_UNLIKELY_NULL(hPoller, 0);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hPoller), HANDLE_TYPE_POLLER, 0);
	
	LPPOLLER lpPoller;
	UARCHLONG ualCount;

	lpPoller = GetHandleDerivative(hPoller);
	EXIT_IF_UNLIKELY_NULL(lpPoller, 0);
	ualCount = 0;
	
	WaitForSingleObject(lpPoller->hLock, INFINITE_WAIT_TIME);

	if (NULLPTR != lpPoller->hvResults && NULLPTR != dlphResults)
	{ 
	 	*dlphResults = (LPHANDLE)VectorAt(lpPoller->hvResults, 0); 
		ualCount = VectorSize(lpPoller->hvResults); 
	}

	ReleaseSingleObject(lpPoller->hLock);	
	return ualCount;
}




_Success_(return != NULLPTR, _Acquires_Shared_Lock_())
PODNET_API
UARCHLONG
GetPollResultEx(
	_In_ 		HANDLE 			hPoller,
	_Out_ 		DLPPOLL_DESCRIPTOR	dlppdResults	
){
	EXIT_IF_UNLIKELY_NULL(hPoller, 0);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hPoller), HANDLE_TYPE_POLLER, 0);
	
	LPPOLLER lpPoller;
	UARCHLONG ualCount;

	lpPoller = GetHandleDerivative(hPoller);
	EXIT_IF_UNLIKELY_NULL(lpPoller, 0);
	ualCount = 0;
	
	WaitForSingleObject(lpPoller->hLock, INFINITE_WAIT_TIME);
	
	if (NULLPTR != lpPoller->hvResultsEx && NULLPTR != dlppdResults)
	{ 
	 	*dlppdResults = (LPPOLL_DESCRIPTOR)VectorAt(lpPoller->hvResultsEx, 0); 
		ualCount = VectorSize(lpPoller->hvResultsEx); 
	}
	
	ReleaseSingleObject(lpPoller->hLock);	
	return ualCount;
}




_Success_(return != FALSE, _Acquires_Shared_Lock_())
PODNET_API
BOOL
DestroyPollResult(
	_In_ 		HANDLE 			hPoller
){
	EXIT_IF_UNLIKELY_NULL(hPoller, 0);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hPoller), HANDLE_TYPE_POLLER, 0);
	
	LPPOLLER lpPoller;
	BOOL bRet;

	lpPoller = GetHandleDerivative(hPoller);
	EXIT_IF_UNLIKELY_NULL(lpPoller, FALSE);

	WaitForSingleObject(lpPoller->hLock, INFINITE_WAIT_TIME);
	bRet = __DestroyPollResultAntiDeadLock(lpPoller);
	ReleaseSingleObject(lpPoller->hLock);
	return bRet;
}
