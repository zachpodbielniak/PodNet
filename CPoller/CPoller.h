/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CPOLLER_H
#define CPOLLER_H


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../CHandle/CHandle.h"
#include "../CContainers/CContainers.h"






typedef struct __POLL_DESCRIPTOR
{
	HANDLE 		hFile;
	ULONG 		ulEvent;
} POLL_DESCRIPTOR, *LPPOLL_DESCRIPTOR, **DLPPOLL_DESCRIPTOR;




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNET_API
HANDLE
CreatePoller(
        VOID       
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
AddFileToPoller(
	_In_            HANDLE			hPoller,
	_In_ 		HANDLE 			hFile
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
AddFileToPollerEx(
	_In_            HANDLE			hPoller,
	_In_ 		HANDLE 			hFile,
	_In_		ULONG 			ulFlags
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
RemoveFileFromPoller(
	_In_            HANDLE			hPoller,
	_In_ 		HANDLE 			hFile
);




_Success_(return != NULLPTR, _Acquires_Shared_Lock_())
PODNET_API
UARCHLONG
GetPollResult(
	_In_ 		HANDLE 			hPoller,
	_Out_ 		DLPHANDLE		dlphResults
);




_Success_(return != NULLPTR, _Acquires_Shared_Lock_())
PODNET_API
UARCHLONG
GetPollResultEx(
	_In_ 		HANDLE 			hPoller,
	_Out_ 		DLPPOLL_DESCRIPTOR	dlppdResults
);




_Success_(return != FALSE, _Acquires_Shared_Lock_())
PODNET_API
BOOL
DestroyPollResult(
	_In_ 		HANDLE 			hPoller
);




#endif