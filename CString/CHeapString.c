/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CHeapString.h"


typedef enum __STRING_TYPE
{
	STRING_TYPE_UNKNOWN		= 0,
	STRING_TYPE_ASCII		= 1,	
	STRING_TYPE_WIDE		= 2
} STRING_TYPE;




typedef struct __HEAPSTRING
{
	union
	{
		LPSTR		lpszValue;
		LPWSTR		lpwszValue;
		LPVOID 		lpValue;
	};
	UARCHLONG 		ualSize;
	UARCHLONG 		ualLength;
	STRING_TYPE		stType;
} HEAPSTRING, *HSTRING;




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HSTRING 
CreateHeapString(
	_In_Z_ 		LPCSTR RESTRICT		lpcszValue
){
	EXIT_IF_UNLIKELY_NULL(lpcszValue, NULLPTR);

	HSTRING hsString;
	UARCHLONG ualLength;

	hsString = GlobalAllocAndZero(sizeof(HEAPSTRING));
	/* cppcheck-suppress memleak */
	EXIT_IF_UNLIKELY_NULL(hsString, NULLPTR);

	hsString->stType = STRING_TYPE_ASCII;
	ualLength = StringLength(lpcszValue);
	hsString->lpszValue = GlobalAllocAndZero(ualLength + 1);

	CopyMemory(
		hsString->lpValue,
		lpcszValue,
		ualLength
	);

	hsString->ualSize = ualLength + 1;
	hsString->ualLength = ualLength;

	return hsString;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
DestroyHeapString(
	_In_ 		HSTRING 		hsString
){
	EXIT_IF_UNLIKELY_NULL(hsString, FALSE);

	FreeMemory(hsString->lpValue);
	FreeMemory(hsString);
	return TRUE;
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPCSTR
HeapStringValue(
	_In_ 		HSTRING 		hsString
){
	EXIT_IF_UNLIKELY_NULL(hsString, NULLPTR);
	return hsString->lpValue;
}




_Success_(return != 0, _Non_Locking_)
PODNET_API
UARCHLONG
HeapStringLength(
	_In_ 		HSTRING 		hsString
){
	EXIT_IF_UNLIKELY_NULL(hsString, 0);
	return hsString->ualLength;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
HeapStringAppend(
	_In_ 		HSTRING 		hsString,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszAppend
){
	EXIT_IF_UNLIKELY_NULL(hsString, FALSE);

	UARCHLONG ualSize, ualOther;

	ualSize = hsString->ualSize;
	ualSize = ualSize - hsString->ualLength;
	ualOther = StringLength(lpcszAppend);

	if (ualSize <= ualOther)
	{
		hsString->ualSize = hsString->ualLength;
		hsString->ualSize += ualOther;
		hsString->ualSize++;
		
		hsString->lpValue = GlobalReAlloc(
			hsString->lpValue,
			hsString->ualSize
		);
		
		EXIT_IF_UNLIKELY_NULL(hsString->lpValue, FALSE);		

		StringConcatenateSafe(
			hsString->lpValue,
			lpcszAppend,
			hsString->ualSize - 1
		);

		hsString->ualLength = StringLength(hsString->lpszValue);
	}
	else
	{
		StringConcatenateSafe(
			hsString->lpValue,
			lpcszAppend,
			hsString->ualSize - 1
		);

		hsString->ualLength = StringLength(hsString->lpszValue);
	}

	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
HeapStringSet(
	_In_ 		HSTRING 		hsString,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszValue
){
	EXIT_IF_UNLIKELY_NULL(hsString, FALSE);

	if (NULLPTR == lpcszValue)
	{
		FreeMemory(hsString->lpValue);
		hsString->lpValue = NULLPTR;
	}
	else
	{
		FreeMemory(hsString->lpValue);
		hsString->ualLength = StringLength(lpcszValue);
		hsString->ualSize = hsString->ualLength + 1;
		hsString->lpValue = GlobalAllocAndZero(hsString->ualSize);
		
		if (NULLPTR == hsString->lpValue)
		{ return FALSE; }

		StringCopySafe(
			hsString->lpValue,
			lpcszValue,
			hsString->ualLength
		);
	}

	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
HeapStringCopy(
	_In_ 		HSTRING RESTRICT	hsSource,
	_In_ 		HSTRING RESTRICT	hsDestination
){
	EXIT_IF_UNLIKELY_NULL(hsSource, FALSE);
	EXIT_IF_UNLIKELY_NULL(hsDestination, FALSE);
	EXIT_IF_VALUE(hsSource, hsDestination, FALSE);
	EXIT_IF_NULL(hsSource->lpValue, FALSE);
	EXIT_IF_NULL(StringLength(hsSource->lpszValue), FALSE);

	if (NULLPTR != hsDestination->lpValue)
	{ FreeMemory(hsDestination->lpValue); }

	hsDestination->lpValue = GlobalAllocAndZero(hsSource->ualSize);
	EXIT_IF_UNLIKELY_NULL(hsDestination->lpValue, FALSE);

	StringCopySafe(
		hsDestination->lpValue,
		hsSource->lpValue,
		hsDestination->ualSize - 1
	);

	hsDestination->ualLength = hsSource->ualLength;
	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
HeapStringCopyByAlloc(
	_In_ 		HSTRING RESTRICT	hsSource,
	_Out_		DLPSTR			dlpszOut
){
	EXIT_IF_UNLIKELY_NULL(hsSource, FALSE);
	EXIT_IF_UNLIKELY_NULL(dlpszOut, FALSE);
	EXIT_IF_NULL(hsSource->lpValue, FALSE);
	EXIT_IF_NULL(StringLength(hsSource->lpszValue), FALSE);

	*dlpszOut = GlobalAllocAndZero(hsSource->ualSize);
	EXIT_IF_UNLIKELY_NULL(*dlpszOut, FALSE);

	StringCopySafe(
		*dlpszOut,
		hsSource->lpValue,
		hsSource->ualSize - 1
	);
	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
HeapStringCopyToBuffer(
	_In_ 		HSTRING RESTRICT	hsSource,
	_Out_		LPSTR			lpszBuffer,
	_In_ 		UARCHLONG		ualBufferSize
){
	EXIT_IF_UNLIKELY_NULL(hsSource, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpszBuffer, FALSE);
	EXIT_IF_UNLIKELY_NULL(ualBufferSize, FALSE);
	EXIT_IF_NULL(hsSource->lpValue, FALSE);
	EXIT_IF_NULL(StringLength(hsSource->lpszValue), FALSE);
	
	StringCopySafe(
		lpszBuffer,
		hsSource->lpValue,
		ualBufferSize
	);

	return TRUE;
}
