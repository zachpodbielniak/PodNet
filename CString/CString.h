/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CSTRING_H
#define CSTRING_H


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../Ascii.h"




#define STRING_SPLIT_NULL_TERMINATOR(X)		(MAX_ULONG == (*(LPULONG)(LPVOID)(X)))



_Success_(return != 0, _Non_Locking_)
PODNET_API
UARCHLONG
StringSplit(
	_In_Z_ 		LPCSTR RESTRICT lpcszString,
	_In_Z_		LPCSTR RESTRICT lpcszDelimeters,
	_Out_Z_ 	TLPSTR 		tlpOut
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
DestroySplitString(
	_In_Z_ 		DLPSTR RESTRICT 	dlpszSplit
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
StringReplaceCharacter(
	_In_Z_		LPSTR RESTRICT		lpszString,
	_In_ 		CHAR			cOld,
	_In_ 		CHAR			cNew
);



_Success_(return != FALSE, _Non_Locking_)
#define StringRemoveSpaces(S)		\
	StringReplaceCharacter((S), ASCII_SPACE, ASCII_NULL_TERMINATOR);




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPSTR 
StringDuplicate(
	_In_Z_ 		LPCSTR RESTRICT 	lpcszString
);




_Success_(return != (UARCHLONG)-1, _Non_Locking_)
PODNET_API 
UARCHLONG 
StringIndexOf(
	_In_Z_		LPCSTR RESTRICT 	lpcszString,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszSubString
);




_Success_(return != (UARCHLONG)-1, _Non_Locking_)
PODNET_API 
UARCHLONG
StringIndexOfNumbered(
	_In_Z_		LPCSTR RESTRICT		lpcszString,
	_In_Z_		LPCSTR RESTRICT		lpcszSubString,
	_In_		ARCHLONG 		alIndex
);




_Success_(return != (UARCHLONG)-1, _Non_Locking_)
PODNET_API 
UARCHLONG 
StringOccurrenceCount(
	_In_Z_ 		LPCSTR RESTRICT 	lpcszString,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszSubString,
	_In_ 		BOOL 			bCountOverlap
);




_Success_(return != (UARCHLONG)-1, _Non_Locking_)
PODNET_API 
UARCHLONG
StringStreakCount(
	_In_Z_ 		LPCSTR RESTRICT 	lpcszString,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszSubString
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API 
BOOL
StringContains(
	_In_Z_		LPCSTR RESTRICT 	lpcszString,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszSubString
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API 
BOOL 
StringStartsWith(
	_In_Z_ 		LPCSTR RESTRICT 	lpcszString,
	_In_Z_		LPCSTR RESTRICT		lpcszSubString
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API 
BOOL
StringEndsWith(
	_In_Z_ 		LPCSTR RESTRICT		lpcszString,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszSubString
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API 
BOOL
StringSlice(
	_In_Z_ 		LPCSTR RESTRICT 	lpcszString,
	_In_ 		UARCHLONG 		ualStart,
	_In_ 		UARCHLONG 		ualEnd
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API 
BOOL 
StringToUpper(
	_In_Z_ 		LPSTR RESTRICT		lpszString
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API 
BOOL 
StringToLower(
	_In_Z_		LPSTR RESTRICT		lpszString
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
StringIsOnly(
	_In_Z_		LPCSTR RESTRICT		lpcszString,
	_In_Z_		LPCSTR RESTRICT		lpcszAcceptedTokens
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API 
BOOL 
StringIsAlpha(
	_In_Z_		LPCSTR RESTRICT		lpcszString
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API 
BOOL 
StringIsDecimal(
	_In_Z_		LPCSTR RESTRICT		lpcszString
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API 
BOOL 
StringIsHex(
	_In_Z_		LPCSTR RESTRICT		lpcszString
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API 
BOOL 
StringIsOctal(
	_In_Z_		LPCSTR RESTRICT		lpcszString
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API 
BOOL 
StringIsBinary(
	_In_Z_		LPCSTR RESTRICT		lpcszString
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API 
BOOL 
StringIsLower(
	_In_Z_		LPCSTR RESTRICT		lpcszString
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API 
BOOL 
StringIsUpper(
	_In_Z_		LPCSTR RESTRICT		lpcszString
);




_Success_(?, _Non_Locking_)
PODNET_API 
BYTE 
StringToByte(
	_In_Z_		LPCSTR RESTRICT		lpcszString
);




_Success_(?, _Non_Locking_)
PODNET_API 
SHORT
StringToShort(
	_In_Z_		LPCSTR RESTRICT		lpcszString
);




_Success_(?, _Non_Locking_)
PODNET_API 
LONG 
StringToLong(
	_In_Z_		LPCSTR RESTRICT		lpcszString
);




_Success_(?, _Non_Locking_)
PODNET_API 
LONGLONG
StringToLongLong(
	_In_Z_		LPCSTR RESTRICT		lpcszString
);




_Success_(?, _Non_Locking_)
PODNET_API 
ARCHLONG 
StringToArchLong(
	_In_Z_		LPCSTR RESTRICT		lpcszString
);








#endif