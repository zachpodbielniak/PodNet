/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CHEAPSTRING_H
#define CHEAPSTRING_H


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"


typedef struct __HEAPSTRING             *HSTRING;




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HSTRING 
CreateHeapString(
	_In_Z_ 		LPCSTR RESTRICT		lpcszValue
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
DestroyHeapString(
	_In_ 		HSTRING 		hsString
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPCSTR
HeapStringValue(
	_In_ 		HSTRING 		hsString
);




_Success_(return != 0, _Non_Locking_)
PODNET_API
UARCHLONG
HeapStringLength(
	_In_ 		HSTRING 		hsString
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
HeapStringAppend(
	_In_ 		HSTRING 		hsString,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszAppend
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
HeapStringSet(
	_In_ 		HSTRING 		hsString,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszValue
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
HeapStringCopy(
	_In_ 		HSTRING RESTRICT	hsSource,
	_In_ 		HSTRING RESTRICT	hsDestination
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
HeapStringCopyByAlloc(
	_In_ 		HSTRING RESTRICT	hsSource,
	_Out_		DLPSTR			dlpszOut
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
HeapStringCopyToBuffer(
	_In_ 		HSTRING RESTRICT	hsSource,
	_Out_		LPSTR			lpszBuffer,
	_In_ 		UARCHLONG		ualBufferSize
);








#endif