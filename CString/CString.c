/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CString.h"




_Success_(return != 0, _Non_Locking_)
PODNET_API
UARCHLONG
StringSplit(
	_In_Z_ 		LPCSTR RESTRICT lpcszString,
	_In_Z_		LPCSTR RESTRICT lpcszDelimeters,
	_Out_Z_ 	TLPSTR 		tlpOut
){
	EXIT_IF_UNLIKELY_NULL(lpcszString, 0);
	EXIT_IF_UNLIKELY_NULL(lpcszDelimeters, 0);
	EXIT_IF_UNLIKELY_NULL(tlpOut, 0);

	UARCHLONG ualCount, ualLength, ualIndex;
	LPSTR lpszCurrent, lpszCurrentDelim, lpszCopy, lpszAlloc;

	lpszAlloc = LocalAllocAndZero((sizeof(CHAR) * StringLength(lpcszString)) + 1);
	lpszCopy = lpszAlloc;
	EXIT_IF_UNLIKELY_NULL(lpszCopy, 0);
	CopyMemory(lpszCopy, lpcszString, StringLength(lpcszString)); 

	/* Count must be one, because if there is one split, then there are two elements */
	ualCount = 1;
	ualLength = 0;
	ualIndex = 0;	

	/* Get number of delimeters */
	for (
		lpszCurrent = (LPSTR)lpszCopy;
		*lpszCurrent != 0;
		lpszCurrent++
	){
		for (
			lpszCurrentDelim = (LPSTR)lpcszDelimeters;
			*lpszCurrentDelim != 0;
			lpszCurrentDelim++
		){
			if (*lpszCurrent == *lpszCurrentDelim)
			{
				ualCount++;
				break;
			}
		}
	}

	*tlpOut = (DLPSTR)GlobalAllocAndZero((sizeof(LPSTR) * ualCount) + 1);
	EXIT_IF_UNLIKELY_NULL(*tlpOut, 0);

	while (NULLPTR != (lpszCurrent = strsep(&lpszCopy, lpcszDelimeters)))
	{
		ualLength = StringLength(lpszCurrent);
		if (4 > ualLength)
		{ ualLength = 4;}
		(*tlpOut)[ualIndex] = GlobalAllocAndZero((sizeof(CHAR) * ualLength) + 1);
		EXIT_IF_UNLIKELY_NULL((*tlpOut)[ualIndex], 0);
		CopyMemory((*tlpOut)[ualIndex], lpszCurrent, StringLength(lpszCurrent));
		ualIndex++;		
	}

	(*tlpOut)[ualCount] = GlobalAllocAndZero(sizeof(LONG));
	*(LPULONG)(LPVOID)(*tlpOut)[ualCount] = MAX_ULONG;

	FreeMemory(lpszAlloc);
	return ualCount;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
DestroySplitString(
	_In_Z_ 		DLPSTR RESTRICT 	dlpszSplit
){
	EXIT_IF_UNLIKELY_NULL(dlpszSplit, FALSE);

	UARCHLONG ualIndex;
	ualIndex = 0;

	while (MAX_ULONG != *(LPULONG)(LPVOID)(dlpszSplit[ualIndex]))
	{
		FreeMemory(dlpszSplit[ualIndex]);
		ualIndex++;
	}

	/* Free the last one */
	FreeMemory(dlpszSplit[ualIndex]);
	FreeMemory(dlpszSplit);
	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
StringReplaceCharacter(
	_In_Z_		LPSTR RESTRICT		lpszString,
	_In_ 		CHAR			cOld,
	_In_ 		CHAR			cNew
){
	EXIT_IF_UNLIKELY_NULL(lpszString, FALSE);
	EXIT_IF_UNLIKELY_NULL(cOld, FALSE);

	CSTRING csBuffer[8192];
	UARCHLONG ualLength, ualIterator, ualJiterator;
	LPSTR lpszTemp;
	BOOL bAlloc;

	ualLength = StringLength(lpszString);
	
	if (ualLength < sizeof(csBuffer))
	{ 
		lpszTemp = csBuffer; 
		ZeroMemory(csBuffer, sizeof(csBuffer));
		bAlloc = FALSE;
	}
	else
	{ 
		lpszTemp = LocalAllocAndZero(ualLength + 1); 
		/* cppcheck-suppress memleak */
		EXIT_IF_UNLIKELY_NULL(lpszTemp, FALSE);
		bAlloc = TRUE;
	}

	for (
		ualIterator = 0, ualJiterator = 0;
		ualIterator < ualLength;
		ualIterator++
	){
		CHAR cIndex;
		cIndex = lpszString[ualIterator];
		
		if (cOld == cIndex)
		{ 
			if (ASCII_NULL_TERMINATOR == cNew)
			{ continue; }
			
			lpszTemp[ualJiterator++] = cNew;
		}
		else
		{ lpszTemp[ualJiterator++] = cIndex; }
	}

	ZeroMemory(lpszString, ualLength);
	StringCopySafe(
		lpszString,
		lpszTemp,
		ualLength
	);

	if (FALSE != bAlloc)
	{ FreeMemory(lpszTemp); }	

	return TRUE;
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPSTR 
StringDuplicate(
	_In_Z_ 		LPCSTR RESTRICT 	lpcszString
){
	EXIT_IF_UNLIKELY_NULL(lpcszString, NULLPTR);

	LPSTR lpszRet;
	UARCHLONG ualLength;

	ualLength = StringLength(lpcszString);

	lpszRet = GlobalAllocAndZero(ualLength + 0x01U);

	if (0 == ualLength)
	{ return lpszRet; }

	StringCopySafe(lpszRet, lpcszString, ualLength);
	return lpszRet;
}




_Success_(return != (UARCHLONG)-1, _Non_Locking_)
PODNET_API 
UARCHLONG 
StringIndexOf(
	_In_Z_		LPCSTR RESTRICT 	lpcszString,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszSubString
){
	EXIT_IF_UNLIKELY_NULL(lpcszString, (UARCHLONG)-1);
	EXIT_IF_UNLIKELY_NULL(lpcszSubString, (UARCHLONG)-1); 

	UARCHLONG ualStringLength;
	UARCHLONG ualSubStringLength;
	UARCHLONG ualRet;
	LPSTR lpszIndex;

	ualRet = (UARCHLONG)-1;

	ualStringLength = StringLength(lpcszString);
	ualSubStringLength = StringLength(lpcszSubString);

	if (ualStringLength < ualSubStringLength)
	{ return ualRet; }

	if (0 < ualStringLength && 0 < ualSubStringLength)
	{
		lpszIndex = StringInString(lpcszString, lpcszSubString);
		ualRet = (UARCHLONG)((UARCHLONG)lpszIndex - (UARCHLONG)lpcszString);
	}

	return ualRet;
}




_Success_(return != (UARCHLONG)-1, _Non_Locking_)
PODNET_API 
UARCHLONG
StringIndexOfNumbered(
	_In_Z_		LPCSTR RESTRICT		lpcszString,
	_In_Z_		LPCSTR RESTRICT		lpcszSubString,
	_In_		ARCHLONG 		alIndex
){
	EXIT_IF_UNLIKELY_NULL(lpcszString, (UARCHLONG)-1);
	EXIT_IF_UNLIKELY_NULL(lpcszSubString, (UARCHLONG)-1); 

	UARCHLONG ualStringLength;
	UARCHLONG ualSubStringLength;
	UARCHLONG ualRet;
	UARCHLONG ualIndex;
	LPSTR lpszIndex;
	LPSTR lpszTest;

	ualRet = (UARCHLONG)-1;
	lpszTest = (LPSTR)lpcszString;
	
	ualStringLength = StringLength(lpcszString);
	ualSubStringLength = StringLength(lpcszSubString);

	if (ualStringLength < ualSubStringLength)
	{ return ualRet; }

	if (0 < ualStringLength && 0 < ualSubStringLength)
	{
		lpszIndex = StringInString(lpszTest, lpcszSubString);
		
		for (
			ualIndex = 0;
			ualIndex < (UARCHLONG)alIndex;
			ualIndex++
		){
			lpszTest = StringInString(lpszTest, lpcszSubString);
			if (NULLPTR == lpszTest)
			{ JUMP(__NOT_FINISHED); }

			lpszTest = (LPSTR)((UARCHLONG)lpszTest + 0x01U);
		}

		ualRet = (UARCHLONG)((UARCHLONG)lpszIndex - (UARCHLONG)lpcszString);
	}

	__NOT_FINISHED:
	return ualRet;
}




_Success_(return != (UARCHLONG)-1, _Non_Locking_)
PODNET_API 
UARCHLONG 
StringOccurrenceCount(
	_In_Z_ 		LPCSTR RESTRICT 	lpcszString,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszSubString,
	_In_ 		BOOL 			bCountOverlap
);




_Success_(return != (UARCHLONG)-1, _Non_Locking_)
PODNET_API 
UARCHLONG
StringStreakCount(
	_In_Z_ 		LPCSTR RESTRICT 	lpcszString,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszSubString
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API 
BOOL
StringContains(
	_In_Z_		LPCSTR RESTRICT 	lpcszString,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszSubString
){
	EXIT_IF_UNLIKELY_NULL(lpcszString, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpcszSubString, FALSE);

	BOOL bRet;
	LPSTR lpszIndex;

	lpszIndex = StringInString(lpcszString, lpcszSubString);
	bRet = FALSE;

	if (NULLPTR != lpszIndex)
	{ bRet = TRUE; }

	return bRet;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API 
BOOL 
StringStartsWith(
	_In_Z_ 		LPCSTR RESTRICT 	lpcszString,
	_In_Z_		LPCSTR RESTRICT		lpcszSubString
){
	EXIT_IF_UNLIKELY_NULL(lpcszString, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpcszSubString, FALSE);

	BOOL bRet;
	ARCHLONG alLength;

	bRet = FALSE;
	alLength = StringLength(lpcszSubString);
	EXIT_IF_UNLIKELY_NULL(alLength, FALSE);

	if (0 == CompareMemory(lpcszString, lpcszSubString, alLength))
	{ bRet = TRUE; }

	return bRet;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API 
BOOL
StringEndsWith(
	_In_Z_ 		LPCSTR RESTRICT		lpcszString,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszSubString
){
	EXIT_IF_UNLIKELY_NULL(lpcszString, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpcszSubString, FALSE);

	BOOL bRet;
	ARCHLONG alLengthMain;
	ARCHLONG alLengthSub;
	ARCHLONG alOffSet;
	LPSTR lpszOffset;

	bRet = FALSE;
	alLengthMain = StringLength(lpcszString);
	alLengthSub = StringLength(lpcszSubString);

	alOffSet = alLengthMain - alLengthSub;
	lpszOffset = (LPSTR)(LPVOID)((UARCHLONG)lpcszString + alOffSet);

	if (0 == CompareMemory(lpszOffset, lpcszSubString, alLengthSub))
	{ bRet = TRUE; }

	return bRet;
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API 
BOOL
StringSlice(
	_In_Z_ 		LPCSTR RESTRICT 	lpcszString,
	_In_ 		UARCHLONG 		ualStart,
	_In_ 		UARCHLONG 		ualEnd
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API 
BOOL 
StringToUpper(
	_In_Z_ 		LPSTR RESTRICT		lpszString
){
	EXIT_IF_UNLIKELY_NULL(lpszString, FALSE);

	ARCHLONG alIndex;
	ARCHLONG alLength;

	alLength = StringLength(lpszString);
	if (0 == alLength)
	{ return FALSE; }

	for (
		alIndex = 0;
		alIndex < alLength;
		alIndex++
	){ lpszString[alIndex] = (CHAR)toupper((int)lpszString[alIndex]); }

	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API 
BOOL 
StringToLower(
	_In_Z_		LPSTR RESTRICT		lpszString
){
	EXIT_IF_UNLIKELY_NULL(lpszString, FALSE);

	ARCHLONG alIndex;
	ARCHLONG alLength;

	alLength = StringLength(lpszString);
	if (0 == alLength)
	{ return FALSE; }

	for (
		alIndex = 0;
		alIndex < alLength;
		alIndex++
	){ lpszString[alIndex] = (CHAR)tolower((int)lpszString[alIndex]); }

	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
StringIsOnly(
	_In_Z_		LPCSTR RESTRICT		lpcszString,
	_In_Z_		LPCSTR RESTRICT		lpcszAcceptedTokens
){
	EXIT_IF_UNLIKELY_NULL(lpcszString, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpcszAcceptedTokens, FALSE);

	ARCHLONG alIndex;
	ARCHLONG alJndex;
	ARCHLONG alLength;
	ARCHLONG alLengthAccepted;
	BOOL bRet;

	alLength = StringLength(lpcszString);
	alLengthAccepted = StringLength(lpcszAcceptedTokens);
	bRet = TRUE; 

	for (
		alIndex = 0;
		alIndex < alLength;
		alIndex++
	){
		BOOL bLocal;
		bLocal = FALSE;
		for (
			alJndex = 0;
			alJndex < alLengthAccepted;
			alJndex++
		){ 
			if (lpcszString[alIndex] == lpcszAcceptedTokens[alJndex])
			{ 
				bLocal = TRUE; 
				JUMP(__LOCAL_DONE);
			}
		}
		__LOCAL_DONE:
		if (FALSE == bLocal)
		{ return FALSE; }
	}

	return bRet;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API 
BOOL 
StringIsAlpha(
	_In_Z_		LPCSTR RESTRICT		lpcszString
){ 
	return StringIsOnly(
		lpcszString,
		"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	);
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API 
BOOL 
StringIsDecimal(
	_In_Z_		LPCSTR RESTRICT		lpcszString
){
	return StringIsOnly(
		lpcszString,
		"0123456789"
	);
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API 
BOOL 
StringIsHex(
	_In_Z_		LPCSTR RESTRICT		lpcszString
){
	return StringIsOnly(
		lpcszString,
		"abcdefABCDEF0123456789"
	);
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API 
BOOL 
StringIsOctal(
	_In_Z_		LPCSTR RESTRICT		lpcszString
){
	return StringIsOnly(
		lpcszString,
		"01234567"
	);
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API 
BOOL 
StringIsBinary(
	_In_Z_		LPCSTR RESTRICT		lpcszString
){
	return StringIsOnly(
		lpcszString,
		"01"
	);
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API 
BOOL 
StringIsLower(
	_In_Z_		LPCSTR RESTRICT		lpcszString
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API 
BOOL 
StringIsUpper(
	_In_Z_		LPCSTR RESTRICT		lpcszString
);




_Success_(?, _Non_Locking_)
PODNET_API 
BYTE 
StringToByte(
	_In_Z_		LPCSTR RESTRICT		lpcszString
){
	EXIT_IF_UNLIKELY_NULL(lpcszString, 0);

	BYTE byRet;
	StringScanFormat(
		lpcszString,
		"%hhu",
		&byRet
	);

	return byRet;
}




_Success_(?, _Non_Locking_)
PODNET_API 
SHORT
StringToShort(
	_In_Z_		LPCSTR RESTRICT		lpcszString
){
	EXIT_IF_UNLIKELY_NULL(lpcszString, 0);

	SHORT sRet;
	StringScanFormat(
		lpcszString,
		"%hd",
		&sRet
	);

	return sRet;
}




_Success_(?, _Non_Locking_)
PODNET_API 
LONG 
StringToLong(
	_In_Z_		LPCSTR RESTRICT		lpcszString
){
	EXIT_IF_UNLIKELY_NULL(lpcszString, 0);

	LONG lRet;
	StringScanFormat(
		lpcszString,
		"%d",
		&lRet
	);

	return lRet;
}




_Success_(?, _Non_Locking_)
PODNET_API 
LONGLONG
StringToLongLong(
	_In_Z_		LPCSTR RESTRICT		lpcszString
){
	EXIT_IF_UNLIKELY_NULL(lpcszString, 0);

	LONGLONG llRet;
	StringScanFormat(
		lpcszString,
#ifdef __LP64__
		"%ld",
#else
		"%lld",
#endif
		&llRet
	);

	return llRet;
}




_Success_(?, _Non_Locking_)
PODNET_API 
ARCHLONG 
StringToArchLong(
	_In_Z_		LPCSTR RESTRICT		lpcszString
){
	EXIT_IF_UNLIKELY_NULL(lpcszString, 0);

	ARCHLONG alRet;

	#ifdef __LP64__
	StringScanFormat(
		lpcszString,
#ifdef __LP64__
		"%ld",
#else
		"%lld",
#endif
		&alRet
	);
	#else 
	StringScanFormat(
		lpcszString,
		"%d",
		&alRet
	);
	#endif

	return alRet;
}



