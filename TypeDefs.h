/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef TYPEDEFS_H
#define TYPEDEFS_h

#include <stdint.h>
#include <wchar.h>

#define GLOBAL_VARIABLE					static
#define ONE_INSTANCE					static
#define INTERNAL_OPERATION				static
#define	KEEP_IN_REGISTER				register
#define VOLATILE					volatile
#define DONT_OPTIMIZE_OUT				VOLATILE
#define CONSTANT					const


typedef char 						CHAR;
typedef unsigned char 					UCHAR;
typedef wchar_t						WCHAR;
typedef uint8_t						BYTE;
typedef BYTE 						BOOL;
typedef BOOL            				BOOLEAN;
typedef int16_t 					SHORT;
typedef uint16_t 					USHORT;
typedef USHORT						WORD;
typedef int32_t 					LONG;
typedef uint32_t 					ULONG;
typedef ULONG						DWORD;
typedef int64_t 					LONGLONG;
typedef uint64_t					ULONGLONG;
typedef ULONGLONG					QWORD;
#if __LP64__
typedef __int128_t					ULTRALONG;
typedef __uint128_t					UULTRALONG;
typedef LONGLONG 					ARCHLONG;
typedef ULONGLONG 					UARCHLONG;
#else
typedef long long					ULTRALONG;
typedef unsigned long long				UULTRALONG;
typedef LONG 						ARCHLONG;
typedef ULONG 						UARCHLONG;
#endif
typedef float						FLOAT;
typedef double						DOUBLE;
typedef long double					LONGDOUBLE;
typedef void 						VOID;



/* Host Pointer Typedefs */
typedef CHAR 						*LPCHAR, **DLPCHAR, ***TLPCHAR;
typedef CHAR						*LPSTR, **DLPSTR, ***TLPSTR;
typedef CHAR						CSTRING, *LPCSTRING, **DLPCSTRING, ***TLPCSTRING;
typedef CONSTANT CHAR					*LPCSTR, **DLPCSTR, ***TLPCSTR;
typedef CONSTANT CHAR * CONSTANT 			CLPCSTR;
typedef CONSTANT CHAR * CONSTANT * CONSTANT 		DCLPCSTR;
typedef CHAR *CONSTANT 					*DCLPSTR;
typedef WCHAR 						*LPWCHAR, **DLPWCHAR, ***TLPWCHAR;
typedef WCHAR 						*LPWSTR, **DLPWSTR, ***TLPWSTR;
typedef CONSTANT WCHAR 					*LPCWSTR, **DLPCWSTR, ***TLPCWSTR;
typedef BYTE 						*LPBYTE, **DLPBYTE, ***TLPBYTE;
typedef BOOL						*LPBOOL, **DLPBOOL, ***TLPBOOL;
typedef SHORT 						*LPSHORT, **DLPSHORT, ***TLPSHORT;
typedef USHORT 						*LPUSHORT, **DLPUSHORT, ***TLPUSHORT;
typedef LONG 						*LPLONG, **DLPLONG, ***TLPLONG;
typedef ULONG 						*LPULONG, **DLPULONG, ***TLPULONG;
typedef LONGLONG 					*LPLONGLONG, **DLPLONGLONG, ***TLPLONGLONG;
typedef ULONGLONG 					*LPULONGLONG, **DLPULONGLONG, ***TLPULONGLONG;
typedef ULTRALONG					*LPULTRALONG, **DLPULTRALONG, ***TLPULTRALONG;
typedef UULTRALONG					*LPUULTRALONG, **DLPUULTRALONG, ***TLPUULTRALONG;
typedef ARCHLONG 					*LPARCHLONG, **DLPARCHLONG, ***TLPARCHLONG;
typedef UARCHLONG 					*LPUARCHLONG, **DLPUARCHLONG, ***TLPUARCHLONG;
typedef FLOAT						*LPFLOAT, **DLPFLOAT, ***TLPFLOAT;
typedef DOUBLE						*LPDOUBLE, **DLPDOUBLE, ***TLPDOUBLE;
typedef LONGDOUBLE					*LPLONGDOUBLE, **DLPLONGDOUBLE, ***TLPLONGDOUBLE;
typedef VOID 						*LPVOID, **DLPVOID, ***TLPVOID;
typedef CONSTANT VOID					*LPCVOID, **DLPCVOID, ***TLPCVOID;
typedef VOID * CONSTANT 				CLPVOID;
typedef CONSTANT VOID * CONSTANT 			CLPCVOID;
typedef CONSTANT VOID * CONSTANT * CONSTANT 		*DCLPCVOID;
typedef DCLPCVOID CONSTANT * CONSTANT 			TCLPCVOID;



#ifndef DATATYPE_ENUM
#define DATATYPE_ENUM

typedef enum __TYPE
{
	TYPE_CHAR 		= 1,
	TYPE_BYTE, 		
	TYPE_SHORT,		
	TYPE_USHORT,		
	TYPE_WORD,
	TYPE_LONG, 		
	TYPE_ULONG, 		
	TYPE_DWORD, 		
	TYPE_LONGLONG, 		
	TYPE_ULONGLONG,
	TYPE_QWORD,
	TYPE_ULTRALONG,
	TYPE_UULTRALONG,
	TYPE_ARCHLONG,
	TYPE_UARCHLONG,
	TYPE_LPVOID,
	TYPE_DLPVOID,
	TYPE_HANDLE,
	TYPE_LPSTR,
	TYPE_DLPSTR
} TYPE, *LPTYPE;

#endif

#endif
