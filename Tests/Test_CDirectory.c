#include "../CUnitTest/CUnitTest.h"
#include "../CFile/CDirectory.h"


TEST_CALLBACK(__TestDirectoryToVectorListing)
{
	TEST_BEGIN("CDirectory - DirectoryToVectorListing()");

	HANDLE hDirectory;
	HVECTOR hvData;
	LPDIRECTORY_ENTRY lpdeData;
	ARCHLONG alIndex;

	hDirectory = OpenDirectory(".");
	TEST_EXPECT_VALUE_NOT_EQUAL(hDirectory, NULL_OBJECT);

	hvData = DirectoryToVectorListing(hDirectory);
	TEST_EXPECT_VALUE_NOT_EQUAL(hvData, NULLPTR);

	for (
		alIndex = 0;
		alIndex < (ARCHLONG)VectorSize(hvData);
		alIndex++
	){
		lpdeData = VectorAt(hvData, (UARCHLONG)alIndex);

		PrintFormat("%s\t%d\n", lpdeData->lpszFileName, lpdeData->bType);
	}


	DestroyVector(hvData);
	DestroyObject(hDirectory);

	TEST_END();
}




TEST_START("CDirectory");
TEST_ADD(__TestDirectoryToVectorListing);
TEST_RUN();
TEST_CONCLUDE();
