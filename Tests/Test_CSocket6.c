#include "../CNetworking/CSocket6.h"
#include <signal.h>

GLOBAL_VARIABLE DONT_OPTIMIZE_OUT HANDLE hSocket, hClient;

INTERNAL_OPERATION
VOID
__SigHandler(
	_In_ 	LONG 	lParam
){
	UNREFERENCED_PARAMETER(lParam);

	if (NULL_OBJECT != hClient)
	{
		SocketClose6(hClient);
		DestroyObject(hClient);
	}

	DestroyObject(hSocket);
	exit(0);
}

LONG 
Main(
	_In_ 	LONG 	lArgCount, 
	_In_Z_ 	DLPSTR 	dlpszArgValues
){
	UNREFERENCED_PARAMETER(lArgCount);
	UNREFERENCED_PARAMETER(dlpszArgValues);

	LPIPV6_ADDRESS lpip6Address;
	CSTRING csBuffer[0x400];

	signal(SIGSEGV, __SigHandler);
	signal(SIGINT, __SigHandler);

	ZeroMemory(csBuffer, sizeof(csBuffer));
	lpip6Address = CreateIpv6AddressFromString("::1");
	hSocket = CreateSocketWithAddress6(8888, lpip6Address);
	BindOnSocket6(hSocket);
	ListenOnBoundSocket6(hSocket, 2);

	




	INFINITE_LOOP()
	{
		hClient = AcceptConnection6(hSocket);

		SocketRecieve6(hClient, csBuffer, sizeof(csBuffer));

		PrintFormat("Recieved: %s\n", csBuffer);

		ZeroMemory(csBuffer, sizeof(csBuffer));

		SocketClose6(hClient);
		DestroyObject(hClient);
		hClient = NULL_OBJECT;
	}


	return 0;
}