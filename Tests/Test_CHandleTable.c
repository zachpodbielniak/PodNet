#include "../CUnitTest/CUnitTest.h"
#include "../CHandle/CHandleTable.h"

TEST_CALLBACK(__CreateHandleTable)
{
	TEST_BEGIN("CHandleTable - CreateHandleTable");

	HANDLE hTable;
	BOOL bRet;

	hTable = CreateHandleTable(2048, NULLPTR, HANDLE_TABLE_ULTRA_PERFORMANCE);
	TEST_EXPECT_VALUE_NOT_EQUAL(hTable, NULL_OBJECT);

	bRet = DestroyObject(hTable);
	TEST_EXPECT_VALUE_NOT_EQUAL(bRet, FALSE);

	TEST_END();
}

TEST_CALLBACK(__RegisterHandleAndGrab)
{
	TEST_BEGIN("CHandleTable - RegisterHandleAndGrab");

	HANDLE hTable;
	HANDLE hLock1, hLock2, hLock3;
	HANDLE hGrabbed;
	ULONGLONG ullId;
	BOOL bRet;

	hTable = CreateHandleTable(2048, NULLPTR, HANDLE_TABLE_ULTRA_PERFORMANCE);
	TEST_EXPECT_VALUE_NOT_EQUAL(hTable, NULL_OBJECT);

	hLock1 = CreateMutex();
	TEST_EXPECT_VALUE_NOT_EQUAL(hLock1, NULL_OBJECT);
	hLock2 = CreateMutex();
	TEST_EXPECT_VALUE_NOT_EQUAL(hLock2, NULL_OBJECT);
	hLock3 = CreateMutex();
	TEST_EXPECT_VALUE_NOT_EQUAL(hLock3, NULL_OBJECT);

	ullId = RegisterHandle(hTable, hLock1);
	TEST_EXPECT_VALUE_NOT_EQUAL(ullId, 0);
	ullId = RegisterHandle(hTable, hLock2);
	TEST_EXPECT_VALUE_NOT_EQUAL(ullId, 0);
	ullId = RegisterHandle(hTable, hLock3);
	TEST_EXPECT_VALUE_NOT_EQUAL(ullId, 0);

	hGrabbed = GrabHandleByDerivative(
		hTable,
		OBJECT_CAST(hLock1, LPVOID),
		sizeof(ULONGLONG),
		NULLPTR
	);
	TEST_EXPECT_VALUE_EQUAL(hGrabbed, hLock1);

	hGrabbed = GrabHandleByDerivative(
		hTable,
		OBJECT_CAST(hLock2, LPVOID),
		sizeof(ULONGLONG),
		NULLPTR
	);
	TEST_EXPECT_VALUE_EQUAL(hGrabbed, hLock2);

	hGrabbed = GrabHandleByDerivative(
		hTable,
		OBJECT_CAST(hLock3, LPVOID),
		sizeof(ULONGLONG),
		NULLPTR
	);
	TEST_EXPECT_VALUE_EQUAL(hGrabbed, hLock3);

	bRet = DestroyObject(hTable);
	TEST_EXPECT_VALUE_NOT_EQUAL(bRet, FALSE);

	TEST_END();
}

TEST_CALLBACK(__RegisterHandleExAndGrab)
{
	TEST_BEGIN("CHandleTable - RegisterHandleAndGrab");

	HANDLE hTable;
	HANDLE hLock1, hLock2, hLock3;
	ULONGLONG ullId1, ullId2, ullId3;
	HANDLE hGrabbed;
	ULONGLONG ullId;
	BOOL bRet;

	ullId1 = 100;
	ullId2 = 300;
	ullId3 = 950;

	hTable = CreateHandleTable(0x4000, NULLPTR, HANDLE_TABLE_ULTRA_PERFORMANCE_EX);
	TEST_EXPECT_VALUE_NOT_EQUAL(hTable, NULL_OBJECT);

	hLock1 = CreateMutex();
	TEST_EXPECT_VALUE_NOT_EQUAL(hLock1, NULL_OBJECT);
	hLock2 = CreateMutex();
	TEST_EXPECT_VALUE_NOT_EQUAL(hLock2, NULL_OBJECT);
	hLock3 = CreateMutex();
	TEST_EXPECT_VALUE_NOT_EQUAL(hLock3, NULL_OBJECT);

	ullId = RegisterHandleEx(hTable, hLock1, (ULONGLONG)&ullId1);
	TEST_EXPECT_VALUE_NOT_EQUAL(ullId, 0);
	ullId = RegisterHandleEx(hTable, hLock2, (ULONGLONG)&ullId2);
	TEST_EXPECT_VALUE_NOT_EQUAL(ullId, 0);
	ullId = RegisterHandleEx(hTable, hLock3, (ULONGLONG)&ullId3);
	TEST_EXPECT_VALUE_NOT_EQUAL(ullId, 0);

	hGrabbed = GrabHandleByDerivative(
		hTable,
		(LPVOID)&ullId1,
		sizeof(ULONGLONG),
		NULLPTR
	);
	TEST_EXPECT_VALUE_EQUAL(hGrabbed, hLock1);

	hGrabbed = GrabHandleByDerivative(
		hTable,
		(LPVOID)&ullId2,
		sizeof(ULONGLONG),
		NULLPTR
	);
	TEST_EXPECT_VALUE_EQUAL(hGrabbed, hLock2);

	hGrabbed = GrabHandleByDerivative(
		hTable,
		(LPVOID)&ullId3,
		sizeof(ULONGLONG),
		NULLPTR
	);
	TEST_EXPECT_VALUE_EQUAL(hGrabbed, hLock3);

	bRet = DestroyObject(hTable);
	TEST_EXPECT_VALUE_NOT_EQUAL(bRet, FALSE);

	TEST_END();
}


TEST_CALLBACK(__RegisterHandleAndGrabBst)
{
	TEST_BEGIN("CHandleTable - RegisterHandleAndGrabBst");

	HANDLE hTable;
	HANDLE hLock1, hLock2, hLock3;
	HANDLE hGrabbed;
	ULONGLONG ullId;
	BOOL bRet;

	hTable = CreateHandleTable(2048, NULLPTR, HANDLE_TABLE_PERFORMANT_AND_CONCIOUS);
	TEST_EXPECT_VALUE_NOT_EQUAL(hTable, NULL_OBJECT);

	hLock1 = CreateMutex();
	TEST_EXPECT_VALUE_NOT_EQUAL(hLock1, NULL_OBJECT);
	hLock2 = CreateMutex();
	TEST_EXPECT_VALUE_NOT_EQUAL(hLock2, NULL_OBJECT);
	hLock3 = CreateMutex();
	TEST_EXPECT_VALUE_NOT_EQUAL(hLock3, NULL_OBJECT);

	ullId = RegisterHandle(hTable, hLock1);
	TEST_EXPECT_VALUE_NOT_EQUAL(ullId, 0);
	ullId = RegisterHandle(hTable, hLock2);
	TEST_EXPECT_VALUE_NOT_EQUAL(ullId, 0);
	ullId = RegisterHandle(hTable, hLock3);
	TEST_EXPECT_VALUE_NOT_EQUAL(ullId, 0);

	hGrabbed = GrabHandleByDerivative(
		hTable,
		OBJECT_CAST(hLock1, LPVOID),
		sizeof(ULONGLONG),
		NULLPTR
	);
	TEST_EXPECT_VALUE_EQUAL(hGrabbed, hLock1);

	hGrabbed = GrabHandleByDerivative(
		hTable,
		OBJECT_CAST(hLock2, LPVOID),
		sizeof(ULONGLONG),
		NULLPTR
	);
	TEST_EXPECT_VALUE_EQUAL(hGrabbed, hLock2);

	hGrabbed = GrabHandleByDerivative(
		hTable,
		OBJECT_CAST(hLock3, LPVOID),
		sizeof(ULONGLONG),
		NULLPTR
	);
	TEST_EXPECT_VALUE_EQUAL(hGrabbed, hLock3);

	bRet = DestroyObject(hTable);
	TEST_EXPECT_VALUE_NOT_EQUAL(bRet, FALSE);

	TEST_END();
}

TEST_CALLBACK(__RegisterHandleExAndGrabBst)
{
	TEST_BEGIN("CHandleTable - RegisterHandleAndGrabBst");

	HANDLE hTable;
	HANDLE hLock1, hLock2, hLock3;
	ULONGLONG ullId1, ullId2, ullId3;
	HANDLE hGrabbed;
	ULONGLONG ullId;
	BOOL bRet;

	ullId1 = 100;
	ullId2 = 300;
	ullId3 = 950;

	hTable = CreateHandleTable(0x4000, NULLPTR, HANDLE_TABLE_PERFORMANT_AND_CONCIOUS_EX);
	TEST_EXPECT_VALUE_NOT_EQUAL(hTable, NULL_OBJECT);

	hLock1 = CreateMutex();
	TEST_EXPECT_VALUE_NOT_EQUAL(hLock1, NULL_OBJECT);
	hLock2 = CreateMutex();
	TEST_EXPECT_VALUE_NOT_EQUAL(hLock2, NULL_OBJECT);
	hLock3 = CreateMutex();
	TEST_EXPECT_VALUE_NOT_EQUAL(hLock3, NULL_OBJECT);

	ullId = RegisterHandleEx(hTable, hLock1, (ULONGLONG)&ullId1);
	TEST_EXPECT_VALUE_NOT_EQUAL(ullId, 0);
	ullId = RegisterHandleEx(hTable, hLock2, (ULONGLONG)&ullId2);
	TEST_EXPECT_VALUE_NOT_EQUAL(ullId, 0);
	ullId = RegisterHandleEx(hTable, hLock3, (ULONGLONG)&ullId3);
	TEST_EXPECT_VALUE_NOT_EQUAL(ullId, 0);

	hGrabbed = GrabHandleByDerivative(
		hTable,
		(LPVOID)&ullId1,
		sizeof(ULONGLONG),
		NULLPTR
	);
	TEST_EXPECT_VALUE_EQUAL(hGrabbed, hLock1);

	hGrabbed = GrabHandleByDerivative(
		hTable,
		(LPVOID)&ullId2,
		sizeof(ULONGLONG),
		NULLPTR
	);
	TEST_EXPECT_VALUE_EQUAL(hGrabbed, hLock2);

	hGrabbed = GrabHandleByDerivative(
		hTable,
		(LPVOID)&ullId3,
		sizeof(ULONGLONG),
		NULLPTR
	);
	TEST_EXPECT_VALUE_EQUAL(hGrabbed, hLock3);

	bRet = DestroyObject(hTable);
	TEST_EXPECT_VALUE_NOT_EQUAL(bRet, FALSE);

	TEST_END();
}


TEST_START("CHandleTable");
TEST_ADD(__CreateHandleTable);
TEST_ADD(__RegisterHandleAndGrab);
TEST_ADD(__RegisterHandleExAndGrab);
TEST_ADD(__RegisterHandleAndGrabBst);
TEST_ADD(__RegisterHandleExAndGrabBst);
TEST_RUN();
TEST_CONCLUDE();