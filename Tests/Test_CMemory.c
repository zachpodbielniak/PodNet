#include "../CMemory/CMemSet.h"
#include "../CMemory/CHeap.h"
#include "../CClock/CClock.h"
#include "../Ascii.h"
#include "../CFile/CFile.h"

/* Temporary */
#include "../CSystem/CDump.h"


VOID VerifyZeroMemoryFast()
{
        LPVOID lpMem;
        lpMem = LocalAlloc(1024);
        
        SetMemory(lpMem, 0xCD, 1024);

        ZeroMemoryFast(lpMem, 9);

        FreeMemory(lpMem);
}


#define MEM_SIZE        (1 * ONE_BILLION)

VOID CompareZeroSpeeds(VOID)
{
        LPVOID lpMem;
        CLOCK clkTimer;
        ULONGLONG ullDiff1, ullDiff2;

        lpMem = RequestPage(MEM_SIZE);
        SetMemory(lpMem, 0xCD, MEM_SIZE);

        Clock(&clkTimer);
        ZeroMemory(lpMem, MEM_SIZE);
        ullDiff1 = ClockAndGetMicroSeconds(&clkTimer);

        SetMemory(lpMem, 0xCD, MEM_SIZE);
        

        Clock(&clkTimer);
        ZeroMemoryFast(lpMem, MEM_SIZE);
        ullDiff2 = ClockAndGetMicroSeconds(&clkTimer);

        PrintFormat("Results: \nNormal: %lu\nFast: %lu\nDiff: %d\n", ullDiff1, ullDiff2, (LONG)((LONGLONG)ullDiff2 - (LONGLONG)ullDiff1));

        ReturnPage(lpMem, MEM_SIZE);
}


VOID TestFastCopy(VOID)
{
        LPVOID lpDestination, lpSource;
        
        lpDestination = LocalAllocAndZero(1024);
        lpSource = LocalAllocAndZero(1024);

        SetMemory(lpSource, 0xCD, 1024);

        CopyMemoryFast(lpDestination, lpSource, 1020);

        FreeMemory(lpDestination);
        FreeMemory(lpSource);
}


VOID TestFastCompare(VOID)
{
        LPVOID lpSource, lpComparand;
        BOOL bRet;
        lpSource = LocalAllocAndZero(512);
        lpComparand = LocalAllocAndZero(512);

        SetMemory(lpSource, 0xAF, 512);
        SetMemory(lpComparand, 0xAF, 512);

        bRet = CompareMemoryFast(lpSource, lpComparand, 512);
	UNREFERENCED_PARAMETER(bRet);

        FreeMemory(lpSource);
        FreeMemory(lpComparand);      
}

VOID TestFastFind(VOID)
{
        LPVOID lpMemory, lpByte;
        LPSTR lpszTest = "This is a test string, omg, what are you doing?";
        SHORT sValue;
        lpMemory = LocalAlloc(128);
        CopyMemory(lpMemory, (LPVOID)lpszTest, StringLength(lpszTest));

        sValue = BIG_TO_LITTLE_ENDIAN_16(MAKEWORD(ASCII_M_LOWER, ASCII_G_LOWER));        

        lpByte = FindShortFast(lpMemory, sValue, StringLength(lpszTest));        
	UNREFERENCED_PARAMETER(lpByte);

        FreeMemory(lpMemory);
}

VOID TestDwordDump(VOID)
{
        LPSTR lpszTest = "This is a test string, omg, what are you doing?";
        UNREFERENCED_PARAMETER(lpszTest);
        DwordDump(CreateHandleWithSingleInheritor, 0x80);
}

VOID TestDwordDumpToFile(VOID)
{
        HANDLE hStdOut;
	hStdOut = GetStandardOut();

        DwordDumpToFile(hStdOut, CreateHandleWithSingleInheritor, 0x80);
}

VOID TestQwordDump(VOID)
{
	QwordDump(CreateHandleWithSingleInheritor, 0x80);
}

VOID TestQwordDumpToFile(VOID)
{
	HANDLE hStdOut;
	hStdOut = GetStandardOut();
	QwordDumpToFile(hStdOut, CreateHandleWithSingleInheritor, 0x80);
}

VOID TestWordDump(VOID)
{
	WordDump(CreateHandleWithSingleInheritor, 0x80);
}

VOID TestWordDumpToFile(VOID)
{
	HANDLE hFile;
	hFile = OpenFile("out.txt", FILE_PERMISSION_READ | FILE_PERMISSION_WRITE | FILE_PERMISSION_CREATE, 0);
	WordDumpToFile(hFile, CreateHandleWithSingleInheritor, 0x80);
	DestroyHandle(hFile);
}

LONG Main(LONG lArgCount, DLPSTR dlpszArgValues)
{
        UNREFERENCED_PARAMETER(lArgCount);
        UNREFERENCED_PARAMETER(dlpszArgValues);
        
        VerifyZeroMemoryFast();
        /* CompareZeroSpeeds(); */
        /* TestFastCopy(); */
        /* TestFastCompare(); */
 	/* TestFastFind(); */
	/* TestDwordDump(); */
	/* TestDwordDumpToFile(); */
	/* TestQwordDumpToFile(); */
	TestWordDumpToFile();


        return 0;
}
