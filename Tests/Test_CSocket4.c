#include "../CUnitTest/CUnitTest.h"
#include "../CNetworking/CSocket4.h"


#define TEST_PORT	40000


TEST_CALLBACK(__TestCSocket4Multicast)
{
	TEST_BEGIN("CSocket4Multicast");

	HANDLE hSocket;
	BOOL bTest;
	IPV4_ADDRESS ip4Group, ip4Bind;
	CSTRING csBuffer[0x400];

	ip4Group = CreateIpv4AddressFromString("239.255.22.20");
	ip4Bind.ulData = 0;
	ZeroMemory(csBuffer, sizeof(csBuffer));

	hSocket = CreateUdpSocket4(TEST_PORT);
	TEST_EXPECT_VALUE_NOT_EQUAL(hSocket, NULL_OBJECT);

	bTest = BindOnSocket4(hSocket);
	TEST_EXPECT_VALUE_NOT_EQUAL(bTest, FALSE);

	bTest = JoinMulticastGroup4(hSocket, ip4Group, ip4Bind);
	TEST_EXPECT_VALUE_NOT_EQUAL(bTest, FALSE);

	bTest = ReceiveOnSocket4(hSocket, csBuffer, sizeof(csBuffer));
	TEST_EXPECT_VALUE_NOT_EQUAL(bTest, FALSE);

	bTest = DestroyObject(hSocket);
	TEST_EXPECT_VALUE_NOT_EQUAL(bTest, FALSE);

	TEST_END();
}





TEST_START("CSocket4");
TEST_ADD(__TestCSocket4Multicast);
TEST_RUN();
TEST_CONCLUDE();