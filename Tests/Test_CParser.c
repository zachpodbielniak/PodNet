#include "../CUnitTest/CUnitTest.h"
#include "../CLex/CParser.h"

INTERNAL_OPERATION
MACHINESTATE
__ParserCallback(
	_In_		HANDLE 		hParser,
	_In_ 		MACHINESTATE	msCurrent,
	_In_		LEXEME 		lxLeft,
	_In_ 		LEXEME 		lxRight,
	_In_ 		TOKENFLAG 	tfType,
	_In_Opt_ 	LPVOID 		lpData
){
	PrintFormat(
		"HANDLE: %p\n"
		"MACHINE_STATE: %lu\n"
		"LEXEME_LEFT: %s\n"
		"LEXEME_RIGHT: %s\n"
		"TOKEN_FLAG: %lu\n"
		"POINTER_DATA: %p\n\n",
		hParser,
		msCurrent,
		lxLeft,
		lxRight,
		tfType,
		lpData
	);

	if (MACHINE_STATE_NULL == msCurrent)
	{ return 1; }
	return msCurrent + 1;
}



TEST_CALLBACK(__TestCreateParser)
{
	TEST_BEGIN("CParser-CreateParser");

	HANDLE hLexer, hTokenTable, hParser;
	CSTRING *dlpszSplitters[] = {"-", "+"};
	LPTOKEN lptknDesc;
	TOKENFLAG tfValue;
	BOOL bRet;
	LPSTR lpszOne, lpszTwo, lpszThree;
	lpszOne = LocalAllocAndZero(8);
	lpszTwo = LocalAllocAndZero(8);
	lpszThree = LocalAllocAndZero(8);

	strcpy(lpszOne, "One");
	strcpy(lpszTwo, "Two");
	strcpy(lpszThree, "Three");

	lptknDesc = LocalAllocAndZero(sizeof(TOKEN) * 3);

	lptknDesc[0].lxValue = lpszOne;
	lptknDesc[0].tfValue = 1;
	lptknDesc[1].lxValue = lpszTwo;
	lptknDesc[1].tfValue = 2;
	lptknDesc[2].lxValue = lpszThree;
	lptknDesc[2].tfValue = 3;

	hLexer = CreateLexer(8192, dlpszSplitters, 2);
	hTokenTable = CreateTokenTable(lptknDesc, 3);
	hParser = CreateParser(hLexer, hTokenTable);
	
	ParserRegisterCallback(
		hParser,
		0,
		__ParserCallback,
		NULLPTR
	);
	ParserRegisterCallback(
		hParser,
		1,
		__ParserCallback,
		NULLPTR
	);
	ParserRegisterCallback(
		hParser,
		2,
		__ParserCallback,
		NULLPTR
	);
	ParserRegisterCallback(
		hParser,
		3,
		__ParserCallback,
		NULLPTR
	);

	ParserSetText(hParser, "One--TickleMe-Two+Three");
	PrintFormat("Text to be parsed: %s\n\n", "One-Two+Three");
	Parse(hParser);

	WaitForSingleObject(hParser, INFINITE_WAIT_TIME);	

	TEST_END();
}


TEST_START("CParser");
TEST_ADD(__TestCreateParser);
TEST_RUN();
TEST_CONCLUDE();