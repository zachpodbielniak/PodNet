/*


 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		Test_CEvent.c
Author:		Zach Podbielniak
Last Updated: 	11/22/2017

Overview:	This file is used for testing the PodNet C API.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


*/


#include "../CLock/CEvent.h"

LONG Main(LONG lArgCount, DLPSTR dlpszArgValues)
{
	UNREFERENCED_PARAMETER(lArgCount);
	UNREFERENCED_PARAMETER(dlpszArgValues);
	HANDLE hEvent;
	hEvent = CreateEvent(FALSE);
	
	printf("Event is at: %x\n", (ULONG)GetEvent(hEvent));
	SignalEvent(hEvent);
	printf("Event is at: %x\n", (ULONG)GetEvent(hEvent));
	ResetEvent(hEvent);
	printf("Event is at: %x\n", (ULONG)GetEvent(hEvent));
	DestroyHandle(hEvent);
	printf("Closed Handle\n");
	return 0;
}
