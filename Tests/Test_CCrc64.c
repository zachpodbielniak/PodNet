#include "../CUnitTest/CUnitTest.h"
#include "../CAlgorithms/CCrc64.h"



TEST_CALLBACK(__TestCrc64Null)
{
	TEST_BEGIN("CCrc64-NullData");

	ULONGLONG ullOutput;
	ullOutput = Crc64(NULLPTR, 0, 0);

	TEST_EXPECT_VALUE_EQUAL(ullOutput, 0);

	TEST_END();
}


TEST_CALLBACK(__TestCrc64TestData)
{
	TEST_BEGIN("CCrc64-TestData");

	ULONGLONG ullOutput;
	CSTRING csData[] = "Test";

	ullOutput = Crc64((LPBYTE)csData, StringLength(csData), 0);
	TEST_EXPECT_VALUE_EQUAL(ullOutput, 0xcf321222f28d02aa);

	TEST_END();
}



TEST_START("CCrc64");
TEST_ADD(__TestCrc64Null);
TEST_ADD(__TestCrc64TestData);
TEST_RUN();
TEST_CONCLUDE();