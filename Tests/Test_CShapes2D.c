/*


 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		Test_CShapes2D.c
Author:		Zach Podbielniak
Last Updated: 	11/22/2017

Overview:	This file is used for testing the PodNet C API.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


*/


#include "../CMath/CShapes2D.h"


LONG Main(LONG lArgCount, DLPSTR dlpszArgValues)
{
        UNREFERENCED_PARAMETER(lArgCount);
        UNREFERENCED_PARAMETER(dlpszArgValues);

        LPSHAPE lpShp;
        DOUBLE dbArea;
        lpShp = CreateShapeEx(2.0, 3.0, 0.0, 0.0, 75.0, 0.0, 0.0, SHAPE_TRIANGLE, OBLIQUE_TRIANGLE);
        dbArea = Perimeter(lpShp);
        dbArea = FastPerimeter(lpShp);
        return 0;
}