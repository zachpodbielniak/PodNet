/*


 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		Test_CLog.c
Author:		Zach Podbielniak
Last Updated: 	11/22/2017

Overview:	This file is used for testing the PodNet C API.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


*/


#include "../CLog/CLog.h"



VOID TestCreateLogAndWrite(VOID)
{
        HANDLE hLog;
        hLog = CreateLog("output.log.txt", NULL);
        WriteLog(hLog, "This is a test write to a log.\n", NULL, NULL);
        WriteLog(hLog, "This is a fatal error!.\n", LOG_FATAL, NULL);
        DestroyHandle(hLog);
}


LONG Main(LONG lArgCount, DLPSTR dlpszArgValues)
{
        UNREFERENCED_PARAMETER(lArgCount);
        UNREFERENCED_PARAMETER(dlpszArgValues);

        TestCreateLogAndWrite();
        return 0;
}