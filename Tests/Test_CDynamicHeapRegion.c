#include "../CUnitTest/CUnitTest.h"
#include "../CMemory/CDynamicHeapRegion.h"



TEST_CALLBACK(__TestCreateDynamicHeapRegion)
{
	TEST_BEGIN("CDynamicHeapRegion - CreateDynamicHeapRegion");

	HANDLE hRegion;
	BOOL bRet;
	
	hRegion = CreateDynamicHeapRegion(NULL_OBJECT, 1000, FALSE, 0);
	TEST_EXPECT_VALUE_NOT_EQUAL(hRegion, NULL_OBJECT);

	bRet = DestroyObject(hRegion);
	TEST_EXPECT_VALUE_NOT_EQUAL(bRet, FALSE);

	TEST_END();
}


TEST_CALLBACK(__TestDynamicHeapRegionAppend)
{
	TEST_BEGIN("CDynamicHeapRegion - DynamicHeapRegionAppend");

	HANDLE hRegion;
	BOOL bRet;

	CSTRING csData[] = "This is some test data.";
	CSTRING csOther[] = "This is more data.";

	hRegion = CreateDynamicHeapRegion(NULL_OBJECT, StringLength(csData) + 1, FALSE, 0);
	TEST_EXPECT_VALUE_NOT_EQUAL(hRegion, NULL_OBJECT);

	bRet = DynamicHeapRegionAppend(hRegion, csData, StringLength(csData));
	TEST_EXPECT_VALUE_NOT_EQUAL(bRet, FALSE);

	bRet = DynamicHeapRegionAppend(hRegion, csOther, StringLength(csOther));
	TEST_EXPECT_VALUE_NOT_EQUAL(bRet, FALSE);

	bRet = DestroyObject(hRegion);
	TEST_EXPECT_VALUE_NOT_EQUAL(bRet, FALSE);

	TEST_END();
}

TEST_CALLBACK(__TestDynamicHeapRegionData)
{
	TEST_BEGIN("CDynamicHeapRegion - DynamicheapRegionData");

	HANDLE hRegion;
	BOOL bRet;

	CSTRING csData[] = "This is some test data.";
	CSTRING csOther[] = " This is more data.";
	CSTRING csSomeMore[] = " This is even more data.";
	LPSTR lpszData;

	hRegion = CreateDynamicHeapRegion(NULL_OBJECT, StringLength(csData) + 1, FALSE, 0);
	TEST_EXPECT_VALUE_NOT_EQUAL(hRegion, NULL_OBJECT);

	bRet = DynamicHeapRegionAppend(hRegion, csData, StringLength(csData));
	TEST_EXPECT_VALUE_NOT_EQUAL(bRet, FALSE);

	bRet = DynamicHeapRegionAppend(hRegion, csOther, StringLength(csOther));
	TEST_EXPECT_VALUE_NOT_EQUAL(bRet, FALSE);
	
	bRet = DynamicHeapRegionAppend(hRegion, csSomeMore, StringLength(csSomeMore));
	TEST_EXPECT_VALUE_NOT_EQUAL(bRet, FALSE);

	lpszData = DynamicHeapRegionData(hRegion, NULLPTR);
	TEST_EXPECT_VALUE_NOT_EQUAL(lpszData, NULLPTR);

	TEST_EXPECT_STRING_EQUAL(
		lpszData,
		"This is some test data. This is more data. This is even more data."
	);

	bRet = DestroyObject(hRegion);
	TEST_EXPECT_VALUE_NOT_EQUAL(bRet, FALSE);

	TEST_END();
}


TEST_START("CDynamicHeapRegion");
TEST_ADD(__TestCreateDynamicHeapRegion);
TEST_ADD(__TestDynamicHeapRegionAppend);
TEST_ADD(__TestDynamicHeapRegionData);
TEST_RUN();
TEST_CONCLUDE();