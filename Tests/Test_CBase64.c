#include "../CUnitTest/CUnitTest.h"
#include "../CAlgorithms/CBase64.h"


TEST_CALLBACK(__TestBase64Encode)
{
	TEST_BEGIN("Base64 -- Base64Encode()");

	LPSTR lpszTest = "The quick brown fox jumps over the lazy dog! \"1234567890\"";
	LPSTR lpszOut;

	lpszOut = Base64Encode(lpszTest, StringLength(lpszTest), NULLPTR);
	PrintFormat("%s\n", lpszOut);

	TEST_EXPECT_STRING_EQUAL(lpszOut, "VGhlIHF1aWNrIGJyb3duIGZveCBqdW1wcyBvdmVyIHRoZSBsYXp5IGRvZyEgIjEyMzQ1Njc4OTAi")

	TEST_END();
}


TEST_CALLBACK(__TestBase64Decode)
{
	TEST_BEGIN("Base64 -- Base64Decode()");

	LPSTR lpszTest = "VGhlIHF1aWNrIGJyb3duIGZveCBqdW1wcyBvdmVyIHRoZSBsYXp5IGRvZyEgIjEyMzQ1Njc4OTAi";
	LPSTR lpszOut;
	CSTRING csOut[1024];
	ZeroMemory(csOut, sizeof(csOut));

	Base64Decode(lpszTest, (LPBYTE)csOut, sizeof(csOut) - 1);
	PrintFormat("%s\n", csOut);

	TEST_EXPECT_STRING_EQUAL(csOut, "The quick brown fox jumps over the lazy dog! \"1234567890\"");

	TEST_END();
}


TEST_CALLBACK(__TestBase64DecodeAllocOut)
{
	TEST_BEGIN("Base64 -- Base64DecodeAllocOut()");

	LPSTR lpszTest = "VGhlIHF1aWNrIGJyb3duIGZveCBqdW1wcyBvdmVyIHRoZSBsYXp5IGRvZyEgIjEyMzQ1Njc4OTAi";
	LPSTR lpszOut;

	lpszOut = (LPSTR)Base64DecodeAllocOut((LPCSTR)lpszTest, TRUE);
	PrintFormat("%s\n", lpszOut);

	TEST_EXPECT_STRING_EQUAL(lpszOut, "The quick brown fox jumps over the lazy dog! \"1234567890\"");
	FreeMemory(lpszOut);

	TEST_END();
}


TEST_START("CBase64");
TEST_ADD(__TestBase64Encode);
TEST_ADD(__TestBase64Decode);
TEST_ADD(__TestBase64DecodeAllocOut);
TEST_RUN();
TEST_CONCLUDE();