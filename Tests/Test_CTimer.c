#include "../CUnitTest/CUnitTest.h"
#include "../CClock/CTimer.h"


TEST_CALLBACK(__TestTimerWait)
{
        TEST_BEGIN("CTimerWait");

        HANDLE hTimer;
        BOOL bRet;
	ULONGLONG ullValue;
        hTimer = CreateTimer(UNIT_TYPE_MILLISECOND, 5000);

        TEST_EXPECT_VALUE_NOT_EQUAL(hTimer, NULL_OBJECT);

	ullValue = GetTimerValue(hTimer);
	TEST_EXPECT_VALUE_EQUAL(ullValue, 5000);

	Sleep(2000);
	ullValue = GetTimerRemaining(hTimer);
	TEST_EXPECT_VALUE_NOT_EQUAL(ullValue, 5);

        bRet = WaitForSingleObject(hTimer, 5000);
        TEST_EXPECT_VALUE_EQUAL(bRet, TRUE);

	bRet = DestroyObject(hTimer);
	TEST_EXPECT_VALUE_EQUAL(bRet, TRUE);

        TEST_END();
}



TEST_START("CTimer");
TEST_ADD(__TestTimerWait);
TEST_RUN();
TEST_CONCLUDE();