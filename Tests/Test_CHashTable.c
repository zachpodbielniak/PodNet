#include "../CUnitTest/CUnitTest.h"
#include "../CContainers/CHashTable.h"


TEST_CALLBACK(__TestCreateHashTable)
{
	TEST_BEGIN("CHashTable - CreateHashTable");

	HHASHTABLE hhtData;
	BOOL bRet;

	hhtData = CreateHashTable(1000);
	TEST_EXPECT_VALUE_NOT_EQUAL(hhtData, NULLPTR);

	bRet = DestroyHashTable(hhtData);
	TEST_EXPECT_VALUE_NOT_EQUAL(bRet, FALSE);

	TEST_END();	
}


TEST_CALLBACK(__TestHashTableInsert)
{
	TEST_BEGIN("CHashTable - HashTableInsert");

	HHASHTABLE hhtData;
	ULONGLONG ullValue;
	LPSTR lpszData;
	BOOL bRet;

	hhtData = CreateHashTable(1000);
	TEST_EXPECT_VALUE_NOT_EQUAL(hhtData, NULLPTR);

	ullValue = 152431;
	bRet = HashTableInsert(
		hhtData,
		ullValue,
		sizeof(ULONGLONG),
		TRUE
	);
	TEST_EXPECT_VALUE_NOT_EQUAL(bRet, FALSE);

	
	
	lpszData = LocalAllocAndZero(4096);
	StringCopySafe(
		lpszData,
		"This is a string of data.",
		4095
	);

	bRet = HashTableInsert(
		hhtData,
		(ULONGLONG)lpszData,
		StringLength(lpszData),
		FALSE
	);
	TEST_EXPECT_VALUE_NOT_EQUAL(bRet, FALSE);



	bRet = DestroyHashTable(hhtData);
	TEST_EXPECT_VALUE_NOT_EQUAL(bRet, FALSE);

	TEST_END();
}


TEST_CALLBACK(__TestHashTableGetKey)
{
	TEST_BEGIN("CHashTable - HashTableGetKey");

	HHASHTABLE hhtData;
	UARCHLONG ualKey, ualSize;
	ULONGLONG ullValue;
	LPSTR lpszData;
	BOOL bRet;

	hhtData = CreateHashTable(1000);
	TEST_EXPECT_VALUE_NOT_EQUAL(hhtData, NULLPTR);

	ullValue = 152431;
	bRet = HashTableInsert(
		hhtData,
		ullValue,
		sizeof(ULONGLONG),
		TRUE
	);
	TEST_EXPECT_VALUE_NOT_EQUAL(bRet, FALSE);

	
	
	lpszData = LocalAllocAndZero(4096);
	StringCopySafe(
		lpszData,
		"This is a string of data.",
		4095
	);

	bRet = HashTableInsert(
		hhtData,
		(ULONGLONG)lpszData,
		StringLength(lpszData),
		FALSE
	);
	TEST_EXPECT_VALUE_NOT_EQUAL(bRet, FALSE);


	ualKey = HashTableGetKey(
		hhtData,
		ullValue,
		sizeof(ULONGLONG),
		TRUE
	);
	TEST_EXPECT_VALUE_EQUAL(ualKey, 5);


	ualKey = HashTableGetKey(
		hhtData,
		(ULONGLONG)lpszData,
		StringLength(lpszData),
		FALSE
	);
	TEST_EXPECT_VALUE_EQUAL(ualKey, 281);




	bRet = DestroyHashTable(hhtData);
	TEST_EXPECT_VALUE_NOT_EQUAL(bRet, FALSE);

	TEST_END();
}


TEST_CALLBACK(__TestHashTableGetValue)
{
	TEST_BEGIN("CHashTable - HashTableGetValue");

	HHASHTABLE hhtData;
	UARCHLONG ualKey, ualSize;
	ULONGLONG ullValue, ullRet;
	LPSTR lpszData;
	BOOL bRet;

	hhtData = CreateHashTable(1000);
	TEST_EXPECT_VALUE_NOT_EQUAL(hhtData, NULLPTR);

	ullValue = 152431;
	bRet = HashTableInsert(
		hhtData,
		ullValue,
		sizeof(ULONGLONG),
		TRUE
	);
	TEST_EXPECT_VALUE_NOT_EQUAL(bRet, FALSE);

	
	
	lpszData = LocalAllocAndZero(4096);
	StringCopySafe(
		lpszData,
		"This is a string of data.",
		4095
	);

	bRet = HashTableInsert(
		hhtData,
		(ULONGLONG)lpszData,
		StringLength(lpszData),
		FALSE
	);
	TEST_EXPECT_VALUE_NOT_EQUAL(bRet, FALSE);


	ualKey = HashTableGetKey(
		hhtData,
		ullValue,
		sizeof(ULONGLONG),
		TRUE
	);
	TEST_EXPECT_VALUE_EQUAL(ualKey, 5);

	ullRet = HashTableGetValue(
		hhtData,
		ualKey,
		&ualSize,
		&bRet
	);
	TEST_EXPECT_VALUE_EQUAL(ullRet, ullValue);
	TEST_EXPECT_VALUE_EQUAL(ualSize, sizeof(ULONGLONG));
	TEST_EXPECT_VALUE_EQUAL(bRet, TRUE);



	ualKey = HashTableGetKey(
		hhtData,
		(ULONGLONG)lpszData,
		StringLength(lpszData),
		FALSE
	);
	TEST_EXPECT_VALUE_EQUAL(ualKey, 281);


	ullRet = HashTableGetValue(
		hhtData,
		ualKey,
		&ualSize,
		&bRet
	);
	TEST_EXPECT_STRING_EQUAL(
		(LPSTR)ullRet,
		lpszData
	);
	TEST_EXPECT_VALUE_EQUAL(
		StringLength((LPSTR)ullRet),
		StringLength(lpszData)
	);
	TEST_EXPECT_VALUE_EQUAL(bRet, FALSE);




	bRet = DestroyHashTable(hhtData);
	TEST_EXPECT_VALUE_NOT_EQUAL(bRet, FALSE);

	TEST_END();
}


TEST_START("CHashTable");
TEST_ADD(__TestCreateHashTable);
TEST_ADD(__TestHashTableInsert);
TEST_ADD(__TestHashTableGetKey);
TEST_ADD(__TestHashTableGetValue);
TEST_RUN();
TEST_CONCLUDE();