/*


 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		Test_CSpinLock.c
Author:		Zach Podbielniak
Last Updated: 	11/22/2017

Overview:	This file is used for testing the PodNet C API.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


*/


#include "../CLock/CSpinLock.h"
#include "../CThread/CThread.h"

HANDLE hSpinLock;


_Thread_Proc_
LPVOID SpinLockProc(LPVOID lpParam)
{
        WaitForSingleObject(hSpinLock, NULLPTR, NULL);
        PrintFormat("Thread %llu acquired.\n", (ULONGLONG)lpParam);
        Sleep(1);
        PrintFormat("Thread %llu now releasing\n", (ULONGLONG)lpParam);
        ReleaseSpinLock(hSpinLock);
        return NULLPTR;
}


VOID TestSpinLockBySpawningThreads(VOID)
{
        HANDLE hThreads[10];
        ULONGLONG ullIndex;

        hSpinLock = CreateSpinLock();

        for (ullIndex = 0; ullIndex < 10; ullIndex++)
                hThreads[ullIndex] = CreateThread(SpinLockProc, (LPVOID)ullIndex, NULL);
        for (ullIndex = 0; ullIndex < 10; ullIndex++)
                WaitForSingleObject(hThreads[ullIndex], NULLPTR, NULL);
        for (ullIndex = 0; ullIndex < 10; ullIndex++)
                DestroyHandle(hThreads[ullIndex]);
}




LONG Main(LONG lArgCount, DLPSTR dlpszArgValues)
{
        UNREFERENCED_PARAMETER(lArgCount);
        UNREFERENCED_PARAMETER(dlpszArgValues);

        TestSpinLockBySpawningThreads();

        return 0;
}