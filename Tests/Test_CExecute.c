#include "../CUnitTest/CUnitTest.h"
#include "../CProcess/CExecute.h"


TEST_CALLBACK(__TestExecute)
{
	TEST_BEGIN("CExecute - Execute");

	DLPSTR dlpszArgs;
	dlpszArgs = LocalAllocAndZero(sizeof(LPCSTR) * 4);
	dlpszArgs[0] = "bash";
	dlpszArgs[1] = "-c";
	dlpszArgs[2] = "echo \"hello\"";
	dlpszArgs[3] = NULLPTR;

	Execute(
		NULL_OBJECT,
		"bash",
		(DLPCSTR)dlpszArgs,
		(DLPCSTR)NULLPTR,
		EXECUTE_INHERIT_ENVIRONMENT | EXECUTE_SEARCH_PATH
	);

	TEST_END();
}


TEST_START("CExecute");
TEST_ADD(__TestExecute);
TEST_RUN();
TEST_CONCLUDE();