#include "../CUnitTest/CUnitTest.h"
#include "../CLex/CIniFileParser.h"

TEST_CALLBACK(__TestCreateIniFileParser)
{
        TEST_BEGIN("CIniFileParser - CreateIniFileParser");

        HANDLE hIni;
        BOOL bRet;

        hIni = OpenIniFile("./TestData/Test.ini", 0);
        TEST_EXPECT_VALUE_NOT_EQUAL(hIni, NULL_OBJECT);

        bRet = DestroyObject(hIni);
        TEST_EXPECT_VALUE_NOT_EQUAL(bRet, FALSE);

        TEST_END();
}

TEST_CALLBACK(__TestParseIniFile)
{
        TEST_BEGIN("CIniFileParser - ParseIniFile");

        HANDLE hIni;
        BOOL bRet;

        hIni = OpenIniFile("./TestData/Test.ini", 0);
        TEST_EXPECT_VALUE_NOT_EQUAL(hIni, NULL_OBJECT);

        bRet = ParseIniFile(hIni, PARSE_INI_USE_VECTORS);
        TEST_EXPECT_VALUE_NOT_EQUAL(bRet, FALSE);

        bRet = DestroyObject(hIni);
        TEST_EXPECT_VALUE_NOT_EQUAL(bRet, FALSE);

        TEST_END();
}

TEST_CALLBACK(__TestGetIniParseResultVector)
{
        TEST_BEGIN("CIniFileParser - GetIniParseResultVector");

        HANDLE hIni;
        HVECTOR hvResults;
        LPINI_SECTION_VECTOR lpisvSection;
        LPINI_ENTRY lpieEntry;
        UARCHLONG ualIndex, ualJndex;
        BOOL bRet;

        hIni = OpenIniFile("./TestData/Test.ini", 0);
        TEST_EXPECT_VALUE_NOT_EQUAL(hIni, NULL_OBJECT);

        bRet = ParseIniFile(hIni, PARSE_INI_USE_VECTORS);
        TEST_EXPECT_VALUE_NOT_EQUAL(bRet, FALSE);

        hvResults = GetIniParseResultVector(hIni);
        TEST_EXPECT_VALUE_NOT_EQUAL(hvResults, NULLPTR);
        TEST_EXPECT_VALUE_NOT_EQUAL(VectorSize(hvResults), 0);

        for (
                ualIndex = 0;
                ualIndex < VectorSize(hvResults);
                ualIndex++
        ){
                lpisvSection = VectorAt(hvResults, ualIndex);
                TEST_EXPECT_VALUE_NOT_EQUAL(lpisvSection, NULLPTR);
                TEST_EXPECT_VALUE_NOT_EQUAL(VectorSize(lpisvSection->hvEntries), 0);

                PrintFormat("Section:%s\n", lpisvSection->lpszSection);

                for (
                        ualJndex = 0;
                        ualJndex < VectorSize(lpisvSection->hvEntries);
                        ualJndex++
                ){
                        lpieEntry = VectorAt(lpisvSection->hvEntries, ualJndex);
                        TEST_EXPECT_VALUE_NOT_EQUAL(lpieEntry, NULLPTR);

                        PrintFormat("%s=%s\n", lpieEntry->lpszKey, lpieEntry->lpszValue);
                }
        }

        bRet = DestroyObject(hIni);
        TEST_EXPECT_VALUE_NOT_EQUAL(bRet, FALSE);

        TEST_END();
}
TEST_START("CIniFileParser");
TEST_ADD(__TestCreateIniFileParser);
TEST_ADD(__TestParseIniFile);
TEST_ADD(__TestGetIniParseResultVector);
TEST_RUN();
TEST_CONCLUDE();