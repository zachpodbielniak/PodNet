
/*


 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		Test_CCritSec.c
Author:		Zach Podbielniak
Last Updated: 	11/22/2017

Overview:	This file is used for testing the PodNet C API.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


*/

#include "../CLock/CCritSec.h"


LONG Main(LONG lArgCount, DLPSTR dlpszArgValues)
{
	UNREFERENCED_PARAMETER(lArgCount);
	UNREFERENCED_PARAMETER(dlpszArgValues);
	HANDLE hCritSec = CreateCriticalSection();
	BOOL bRet = TRUE;
	PrintFormat("Address of hCritSec: %p\n", hCritSec);

	bRet &= EnterCriticalSection(hCritSec);

	PrintFormat("Entered Critical Section\n");

	bRet &= ExitCriticalSection(hCritSec);

	PrintFormat("Exited Critical Section\n");
	
	bRet &= DestroyHandle(hCritSec);
	PrintFormat("bRet: %x\n", bRet);
	return 0;
}
