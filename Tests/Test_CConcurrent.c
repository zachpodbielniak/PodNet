/*


 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		Test_CConcurrent.c
Author:		Zach Podbielniak
Last Updated: 	11/22/2017

Overview:	This file is used for testing the PodNet C API.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


*/


#include "../CConcurrent/CPromise.h"
#include "../CConcurrent/CFuture.h"
#include "../CConcurrent/CCallOnce.h"

typedef struct _TEST
{
        LONG a;
        LONG b;
} TEST, *LPTEST;

ULONG PromiseProc(LPVOID lpParam, DLPVOID dlpOut)
{
        LPTEST lpTest;
        lpTest = (LPTEST)lpParam;
        *dlpOut = LocalAlloc(sizeof(LONG));
        **(LONG **)(dlpOut) = lpTest->a * lpTest->b;
        Sleep(3);
        return sizeof(LONG);
}

VOID TestFutureAndPromise()
{
        HANDLE hPromise, hFuture;
        LPTEST lpTest;
        lpTest = LocalAlloc(sizeof(TEST));
        lpTest->a = 15;
        lpTest->b = 6;

        hPromise = CreatePromise(PromiseProc, (LPVOID)lpTest, NULL, NULLPTR, 0);
        hFuture = GetFuture(hPromise);

        WaitForSingleObject(hFuture, 0, 0);
        LONG ulAnswer;
        
        RetrieveFutureValue(hFuture, &ulAnswer, sizeof(LONG));

        PrintFormat("The answer is %d\n", ulAnswer);

        DestroyHandle(hPromise);
        DestroyHandle(hFuture);
}

LPVOID CallOnceProc(LPVOID lpParam)
{
        UNREFERENCED_PARAMETER(lpParam);
        PrintFormat("Called exactly once\n");
        return NULL;
}

LPVOID CallIt(LPVOID lpParam)
{
        HANDLE hCall;
        hCall = (HANDLE)lpParam;
        CallExactlyOnce(hCall, 0);
        Sleep(8);
        return 0;
}

LPVOID TestCallOnce(LPVOID lpParam)
{
        UNREFERENCED_PARAMETER(lpParam);
        HANDLE hOnce;
        HANDLE hThreads[10000];
        hOnce = CreateOnceCallableObject(CallOnceProc);
        ULONG ulIndex;
        /* Create 10000 Threads to try and break CallExactlyOnce */
        for (ulIndex = 0; ulIndex < 10000; ulIndex++)
                hThreads[ulIndex] = CreateThread(CallIt, hOnce, 0);
        Sleep(8);
        for (ulIndex = 0; ulIndex < 10000; ulIndex++)
                DestroyHandle(hThreads[ulIndex]);
        return 0;
}

LONG Main(LONG lArgCount, DLPSTR dlpszArgValues)
{
        UNREFERENCED_PARAMETER(lArgCount);
        UNREFERENCED_PARAMETER(dlpszArgValues);
        TestCallOnce(0);
        TestFutureAndPromise();
        return 0;
}



