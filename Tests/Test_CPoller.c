#include "../CUnitTest/CUnitTest.h"
#include "../CPoller/CPoller.h"


TEST_CALLBACK(__CreatePoller)
{
        TEST_BEGIN("CreatePoller");

        HANDLE hPoller;
        BOOL bTest;

        hPoller = CreatePoller();
        TEST_EXPECT_VALUE_NOT_EQUAL(hPoller, NULL_OBJECT);

        bTest = DestroyObject(hPoller);
        TEST_EXPECT_VALUE_NOT_EQUAL(bTest, FALSE);

        TEST_END();       
}


TEST_CALLBACK(__PollPoller)
{
        TEST_BEGIN("PollPoller");

        HANDLE hPoller, hInput;
        LPHANDLE lphResults;
        UARCHLONG ualCount;
        BOOL bTest;
        CSTRING csBuffer[0x100];
        LPSTR lpszBuffer;
        lpszBuffer = (LPSTR)csBuffer;

        hInput = GetStandardIn();
        hPoller = CreatePoller();
        TEST_EXPECT_VALUE_NOT_EQUAL(hPoller, NULL_OBJECT);

        bTest = AddFileToPoller(hPoller, hInput);
        TEST_EXPECT_VALUE_NOT_EQUAL(bTest, FALSE);

        bTest = WaitForSingleObject(hPoller, INFINITE_WAIT_TIME);
        TEST_EXPECT_VALUE_NOT_EQUAL(bTest, FALSE);

        ualCount = GetPollResult(hPoller, &lphResults);
        TEST_EXPECT_VALUE_NOT_EQUAL(ualCount, 0);
        TEST_EXPECT_VALUE_NOT_EQUAL(lphResults, NULLPTR);
        TEST_EXPECT_VALUE_EQUAL(lphResults[0], hInput);

        ReadLineFromFile(lphResults[0], &lpszBuffer, sizeof(csBuffer), 0);
        WriteFile(GetStandardOut(), lpszBuffer, 0);

        bTest = DestroyObject(hPoller);
        TEST_EXPECT_VALUE_NOT_EQUAL(bTest, FALSE);
        
        TEST_END();
}





TEST_START("CPoller");
TEST_ADD(__CreatePoller);
TEST_ADD(__PollPoller);
TEST_RUN();
TEST_CONCLUDE();
