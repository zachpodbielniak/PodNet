#include "../CUnitTest/CUnitTest.h"
#include "../CContainers/CBinarySearchTree.h"


TEST_CALLBACK(__TestCreateBinarySearchTree)
{
	TEST_BEGIN("CreateBinarySearchTree");

	HBINARYSEARCHTREE hbstTree;
	BOOL bRet; 

	hbstTree = CreateBinarySearchTree(
		TYPE_LONG,
		sizeof(LONG),
		NULLPTR,
		NULLPTR
	);

	TEST_EXPECT_VALUE_NOT_EQUAL(NULLPTR, hbstTree);
	
	bRet = DestroyBinarySearchTree(hbstTree);
	TEST_EXPECT_VALUE_NOT_EQUAL(FALSE, bRet);

	TEST_END();
}
	
	
TEST_CALLBACK(__TestBinarySearchTreeInsert)
{
	TEST_BEGIN("BinarySearchTreeInsert");

	HBINARYSEARCHTREE hbstTree;
	LONG lValue;
	BOOL bRet;

	hbstTree = CreateBinarySearchTree(
		TYPE_LONG,
		sizeof(LONG),
		NULLPTR,
		NULLPTR
	);

	TEST_EXPECT_VALUE_NOT_EQUAL(NULLPTR, hbstTree);

	lValue = 0xDEADL;
	bRet = BinarySearchTreeInsert(hbstTree, &lValue, sizeof(LONG));
	TEST_EXPECT_VALUE_NOT_EQUAL(FALSE, bRet);
	
	bRet = DestroyBinarySearchTree(hbstTree);
	TEST_EXPECT_VALUE_NOT_EQUAL(FALSE, bRet);

	TEST_END();
}


TEST_CALLBACK(__TestBinarySearchTreeTripleInsert)
{
	TEST_BEGIN("BinarySearchTreeTripleInsert");

	HBINARYSEARCHTREE hbstTree;
	LONG lValue1, lValue2, lValue3;
	BOOL bRet;

	hbstTree = CreateBinarySearchTree(
		TYPE_LONG,
		sizeof(LONG),
		NULLPTR,
		NULLPTR
	);

	TEST_EXPECT_VALUE_NOT_EQUAL(NULLPTR, hbstTree);

	lValue1 = 0xDEADL;
	lValue2 = 0xDEACL;
	lValue3 = 0xDEAFL;
	
	bRet = BinarySearchTreeInsert(hbstTree, &lValue1, sizeof(LONG));
	TEST_EXPECT_VALUE_NOT_EQUAL(FALSE, bRet);
	bRet = BinarySearchTreeInsert(hbstTree, &lValue2, sizeof(LONG));
	TEST_EXPECT_VALUE_NOT_EQUAL(FALSE, bRet);
	bRet = BinarySearchTreeInsert(hbstTree, &lValue3, sizeof(LONG));
	TEST_EXPECT_VALUE_NOT_EQUAL(FALSE, bRet);
	
	bRet = DestroyBinarySearchTree(hbstTree);
	TEST_EXPECT_VALUE_NOT_EQUAL(FALSE, bRet);

	TEST_END();
}


TEST_CALLBACK(__TestBinarySearchTreeAt)
{
	TEST_BEGIN("BinarySearchTreeAt");

	HBINARYSEARCHTREE hbstTree;
	LPVOID lpNode;
	LONG lValue1, lValue2, lValue3;
	LPLONG lplOut;
	BOOL bRet;

	hbstTree = CreateBinarySearchTree(
		TYPE_LONG,
		sizeof(LONG),
		NULLPTR,
		NULLPTR
	);

	TEST_EXPECT_VALUE_NOT_EQUAL(NULLPTR, hbstTree);

	lValue1 = 0xDEADL;
	lValue2 = 0xDEACL;
	lValue3 = 0xDEAFL;
	
	bRet = BinarySearchTreeInsert(hbstTree, &lValue1, sizeof(LONG));
	TEST_EXPECT_VALUE_NOT_EQUAL(FALSE, bRet);
	bRet = BinarySearchTreeInsert(hbstTree, &lValue2, sizeof(LONG));
	TEST_EXPECT_VALUE_NOT_EQUAL(FALSE, bRet);
	bRet = BinarySearchTreeInsert(hbstTree, &lValue3, sizeof(LONG));
	TEST_EXPECT_VALUE_NOT_EQUAL(FALSE, bRet);

	/* lValue2 */
	lpNode = BinarySearchTreeSearch(hbstTree, &(lValue2), sizeof(LONG));
	TEST_EXPECT_VALUE_NOT_EQUAL(NULLPTR, lpNode);

	lplOut = BinarySearchTreeAt(hbstTree, lpNode);
	TEST_EXPECT_VALUE_EQUAL(lValue2, *lplOut);

	/* lValue1 */
	lpNode = BinarySearchTreeSearch(hbstTree, &(lValue1), sizeof(LONG));
	TEST_EXPECT_VALUE_NOT_EQUAL(NULLPTR, lpNode);

	lplOut = BinarySearchTreeAt(hbstTree, lpNode);
	TEST_EXPECT_VALUE_EQUAL(lValue1, *lplOut);

	/* lValue3 */
	lpNode = BinarySearchTreeSearch(hbstTree, &(lValue3), sizeof(LONG));
	TEST_EXPECT_VALUE_NOT_EQUAL(NULLPTR, lpNode);

	lplOut = BinarySearchTreeAt(hbstTree, lpNode);
	TEST_EXPECT_VALUE_EQUAL(lValue3, *lplOut);

	bRet = DestroyBinarySearchTree(hbstTree);
	TEST_EXPECT_VALUE_NOT_EQUAL(FALSE, bRet);

	TEST_END();
}


_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
LPVOID
__Traverse(
	_In_ 		HBINARYSEARCHTREE	hbstData,
	_In_ 		LPVOID 			lpNode, 
	_In_ 		LPVOID 			lpData, 
	_In_Opt_ 	LPVOID 			lpUserData
){
	UNREFERENCED_PARAMETER(hbstData);
	UNREFERENCED_PARAMETER(lpNode);
	UNREFERENCED_PARAMETER(lpUserData);
	PrintFormat("%d\n", *(LPLONG)lpData);
	return NULLPTR;
}


TEST_CALLBACK(__TestBinarySearchTreeTraverse)
{
	TEST_BEGIN("BinarySearchTreeTraverse");

	HBINARYSEARCHTREE hbstTree;
	LPVOID lpNode;
	LONG lValue1, lValue2, lValue3;
	LPLONG lplOut;
	BOOL bRet;

	hbstTree = CreateBinarySearchTree(
		TYPE_LONG,
		sizeof(LONG),
		NULLPTR,
		NULLPTR
	);

	TEST_EXPECT_VALUE_NOT_EQUAL(NULLPTR, hbstTree);

	lValue1 = 0xDEADL;
	lValue2 = 0xDEACL;
	lValue3 = 0xDEAFL;
	
	bRet = BinarySearchTreeInsert(hbstTree, &lValue1, sizeof(LONG));
	TEST_EXPECT_VALUE_NOT_EQUAL(FALSE, bRet);
	bRet = BinarySearchTreeInsert(hbstTree, &lValue2, sizeof(LONG));
	TEST_EXPECT_VALUE_NOT_EQUAL(FALSE, bRet);
	bRet = BinarySearchTreeInsert(hbstTree, &lValue3, sizeof(LONG));
	TEST_EXPECT_VALUE_NOT_EQUAL(FALSE, bRet);

	/* lValue2 */
	lpNode = BinarySearchTreeSearch(hbstTree, &(lValue2), sizeof(LONG));
	TEST_EXPECT_VALUE_NOT_EQUAL(NULLPTR, lpNode);

	lplOut = BinarySearchTreeAt(hbstTree, lpNode);
	TEST_EXPECT_VALUE_EQUAL(lValue2, *lplOut);

	/* lValue1 */
	lpNode = BinarySearchTreeSearch(hbstTree, &(lValue1), sizeof(LONG));
	TEST_EXPECT_VALUE_NOT_EQUAL(NULLPTR, lpNode);

	lplOut = BinarySearchTreeAt(hbstTree, lpNode);
	TEST_EXPECT_VALUE_EQUAL(lValue1, *lplOut);

	/* lValue3 */
	lpNode = BinarySearchTreeSearch(hbstTree, &(lValue3), sizeof(LONG));
	TEST_EXPECT_VALUE_NOT_EQUAL(NULLPTR, lpNode);

	lplOut = BinarySearchTreeAt(hbstTree, lpNode);
	TEST_EXPECT_VALUE_EQUAL(lValue3, *lplOut);

	bRet = BinarySearchTreeTraverse(
		hbstTree,
		__Traverse,
		NULLPTR
	);
	TEST_EXPECT_VALUE_NOT_EQUAL(FALSE, bRet);

	bRet = DestroyBinarySearchTree(hbstTree);
	TEST_EXPECT_VALUE_NOT_EQUAL(FALSE, bRet);

	TEST_END();
}




TEST_START("CBinarySearchTree");
TEST_ADD(__TestCreateBinarySearchTree);
TEST_ADD(__TestBinarySearchTreeInsert);
TEST_ADD(__TestBinarySearchTreeTripleInsert);
TEST_ADD(__TestBinarySearchTreeAt);
TEST_ADD(__TestBinarySearchTreeTraverse);
TEST_RUN();
TEST_CONCLUDE();