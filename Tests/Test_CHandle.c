/*


 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		Test_CHandle.c
Author:		Zach Podbielniak
Last Updated: 	11/22/2017

Overview:	This file is used for testing the PodNet C API.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


*/


#include "../CHandle/CHandle.h"

#define HANDLE_TYPE_TEST_OBJECT		512

typedef struct _TEST_STRUCT
{
	INHERITS_FROM_HANDLE();
	ULONG	ulVal;
} TEST_STRUCT, *LPTEST_STRUCT;

BOOL DestroyTest(HDERIVATIVE hDerivative, ULONG ulFlags)
{
	LPTEST_STRUCT lpTest = (LPTEST_STRUCT)hDerivative;
	free(lpTest);
	return 1;
}

BOOL PrintTest(HANDLE hHandle)
{
	LPTEST_STRUCT lpTest = (LPTEST_STRUCT)GetHandleDerivative(hHandle);
	printf("Value stored is: %x\n", lpTest->ulVal);
	return TRUE;
}

HANDLE CreateTest(ULONG ulValue)
{
	HANDLE hHandle;
	LPTEST_STRUCT lpTest = malloc(sizeof(TEST_STRUCT));
	lpTest->ulVal = ulValue;
	ULONGLONG ullID;

	hHandle = CreateHandleWithSingleInheritor(	lpTest,
							&(lpTest->hHandle),
							HANDLE_TYPE_TEST_OBJECT,
							DestroyTest,
							0,
							NULLPTR,
							NULL,
							NULLPTR,
							NULL,
							&ullID,
							0);
}


int main()
{
	HANDLE hHandle;
	hHandle = CreateTest(5UL);
	printf("Handle created at: %p\n", hHandle);
	PrintTest(hHandle);
	DestroyHandle(hHandle);
	return 0;
}
