/*


 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		Test_CError.c
Author:		Zach Podbielniak
Last Updated: 	11/22/2017

Overview:	This file is used for testing the PodNet C API.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


*/


#include "../CError/CError.h"

LONG Main(LONG lArgc, DLPSTR dlpszArgV)
{
        UNREFERENCED_PARAMETER(lArgc);
        UNREFERENCED_PARAMETER(dlpszArgV);

        SetLastGlobalError(ERROR_BROKEN_PROMISE);

        PrintFormat("The last error code was: 0x%x\n", GetLastGlobalError());


        return 0;
}