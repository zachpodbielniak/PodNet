#include "../CDevices/CSerial.h"

LONG Main(LONG lArgCount, DLPSTR dlpszArgValues)
{
        UNREFERENCED_PARAMETER(lArgCount);
        UNREFERENCED_PARAMETER(dlpszArgValues);

        HANDLE hSerial, hSerialLookup;

        hSerial = CreateSerialInterface("/dev/ttyS0", 9600, NULLPTR, NULL);

        hSerialLookup = GetSerialInterfaceHandle("/dev/ttyS0", NULLPTR);

        return 0;
}