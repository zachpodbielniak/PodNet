/*


 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		Test_CSha256.c
Author:		Zach Podbielniak
Last Updated: 	11/22/2017

Overview:	This file is used for testing the PodNet C API.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


*/


#include "../CAlgorithms/CSha256.h"

VOID PrintSha256Value(LPBYTE lpbData)
{
        BYTE bIndex;
        for (bIndex = 0; bIndex < 4; bIndex++)
        {
                CSTRING csBuffer[64];
                LPULONGLONG lpullIndex;
                lpullIndex = (LPULONGLONG)lpbData;
                StringPrintFormat(csBuffer, "%llx", LITTLE_TO_BIG_ENDIAN_64(lpullIndex[bIndex]));
                printf("%s", csBuffer);
        }
}


LONG Main(LONG lArgCount, DLPSTR dlpszArgValues)
{
        UNREFERENCED_PARAMETER(lArgCount);
        UNREFERENCED_PARAMETER(dlpszArgValues);

        BYTE lpbOut[SHA256_BUFFER_SIZE];
/*        CSTRING csData[] = "My name is Zach"; */
        ZeroMemory(lpbOut, sizeof(BYTE) * SHA256_BUFFER_SIZE);
        CSTRING csTextToWrite[64];
        ZeroMemory(csTextToWrite, 64);
        Sha256((LPBYTE)dlpszArgValues[1], StringLength(dlpszArgValues[1]), lpbOut);
        Sha256ToHexString(lpbOut, csTextToWrite);
        printf("%s\n", csTextToWrite);
        return 0;
}