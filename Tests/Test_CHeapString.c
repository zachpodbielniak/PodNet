#include "../CUnitTest/CUnitTest.h"
#include "../CString/CHeapString.h"




TEST_CALLBACK(__CreateHeapString)
{
        TEST_BEGIN("CreateHeapString");

        HSTRING hsString;
        BOOL bTest;

        hsString = CreateHeapString("This is a test");
        TEST_EXPECT_VALUE_NOT_EQUAL(hsString, NULLPTR);

        bTest = DestroyHeapString(hsString);
        TEST_EXPECT_VALUE_NOT_EQUAL(hsString, FALSE);

        TEST_END();
}




TEST_CALLBACK(__HeapStringValue)
{
        TEST_BEGIN("HeapStringValue");

        HSTRING hsString;
        BOOL bTest;

        hsString = CreateHeapString("This is a test");
        TEST_EXPECT_VALUE_NOT_EQUAL(hsString, NULLPTR);

        PrintFormat(ANSI_YELLOW_BOLD "%s\n", HeapStringValue(hsString));

        bTest = DestroyHeapString(hsString);
        TEST_EXPECT_VALUE_NOT_EQUAL(hsString, FALSE);

        TEST_END();
}




TEST_START("CUnitTest");
TEST_ADD(__CreateHeapString);
TEST_ADD(__HeapStringValue);
TEST_RUN();
TEST_CONCLUDE();
