#include "../CUnitTest/CUnitTest.h"




TEST_CALLBACK(__Test)
{
	TEST_BEGIN("CUnitTest-Values");
	TEST_EXPECT_VALUE_EQUAL(1,1);
	TEST_EXPECT_VALUE_EQUAL(3,3);
	TEST_END();
}


TEST_CALLBACK(__Test2)
{
	TEST_BEGIN("CUnitTest-Floats");
	TEST_EXPECT_FLOAT_EQUAL(1.0f, 1.0f);
	TEST_EXPECT_FLOAT_EQUAL(3.14159f, 3.14159f);
	TEST_END();
}

TEST_CALLBACK(__Test3)
{
	TEST_BEGIN("CUnitTest-Doubles");
	TEST_EXPECT_DOUBLE_EQUAL(1.0, 1.0);
	TEST_EXPECT_DOUBLE_EQUAL(3.1415926, 3.1415926);
	TEST_END();
}


TEST_CALLBACK(__Test4)
{
	TEST_BEGIN("CUnitTest-Strings");
	TEST_EXPECT_STRING_EQUAL("Test", "Test");
	TEST_EXPECT_STRING_EQUAL("This is a more complex string", "This is a more complex string");
	TEST_END();
}


TEST_CALLBACK(__Test5)
{
	TEST_BEGIN("CUnitTest-Memory");
	LPVOID lpMem;
	lpMem = LocalAllocAndZero(0x100);
	TEST_EXPECT_MEMORY_EQUAL(lpMem, lpMem, 0x100);
	FreeMemory(lpMem);
	TEST_END();
}





TEST_START("CUnitTest");
TEST_ADD(__Test);
TEST_ADD(__Test2);
TEST_ADD(__Test3);
TEST_ADD(__Test4);
TEST_ADD(__Test5);
TEST_RUN();
TEST_CONCLUDE();
