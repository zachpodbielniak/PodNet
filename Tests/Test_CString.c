#include "../CUnitTest/CUnitTest.h"
#include "../CString/CString.h"



TEST_CALLBACK(__TestStringSplit)
{
	TEST_BEGIN("StringSplit");

	CSTRING csString[] = "This,is,a,csv,string";
	DLPSTR dlpszOut;
	UARCHLONG ualTokens;
	BOOL bRet;

	ualTokens = StringSplit(csString, ",", &dlpszOut);

	TEST_EXPECT_VALUE_EQUAL(ualTokens, 5);
	
	TEST_EXPECT_STRING_EQUAL(dlpszOut[0], "This");
	TEST_EXPECT_STRING_EQUAL(dlpszOut[1], "is");
	TEST_EXPECT_STRING_EQUAL(dlpszOut[2], "a");
	TEST_EXPECT_STRING_EQUAL(dlpszOut[3], "csv");
	TEST_EXPECT_STRING_EQUAL(dlpszOut[4], "string");

	bRet = DestroySplitString(dlpszOut);
	TEST_EXPECT_VALUE_EQUAL(bRet, TRUE);

	TEST_END();
}


TEST_CALLBACK(__TestStringReplace)
{
	TEST_BEGIN("StringReplace");

	CSTRING csBuffer[] = "This Is A Test";

	StringRemoveSpaces(csBuffer);

	TEST_EXPECT_STRING_EQUAL(csBuffer, "ThisIsATest");
	TEST_END();
}


TEST_CALLBACK(__TestStringContains)
{
	TEST_BEGIN("StringContains");

	BOOL bRet;
	CSTRING csActual[] = "Starting This Is A Test String For Testing";
	CSTRING csActualSub[] = "This Is A Test";
	CSTRING csNot[] =  "This A Is Test";

	bRet = StringContains(csActual, csActualSub);
	TEST_EXPECT_VALUE_EQUAL(TRUE, bRet);

	bRet = StringContains(csActual, csNot);
	TEST_EXPECT_VALUE_EQUAL(FALSE, bRet); 

	TEST_END();
}


TEST_CALLBACK(__TestStringStartsWith)
{
	TEST_BEGIN("StringStartsWith");

	BOOL bRet;
	CSTRING csActual[] = "This Is A Test String For Testing";
	CSTRING csActualSub[] = "This Is A Test";
	CSTRING csNot[] =  "This A Is Test";

	bRet = StringStartsWith(csActual, csActualSub);
	TEST_EXPECT_VALUE_EQUAL(TRUE, bRet);

	bRet = StringStartsWith(csActual, csNot);
	TEST_EXPECT_VALUE_EQUAL(FALSE, bRet); 

	TEST_END();
}


TEST_CALLBACK(__TestStringEndsWith)
{
	TEST_BEGIN("StringStartsWith");

	BOOL bRet;
	CSTRING csActual[] = "This Is A Test String For Testing";
	CSTRING csActualSub[] = "String For Testing";
	CSTRING csNot[] =  "This A Is Test";

	bRet = StringEndsWith(csActual, csActualSub);
	TEST_EXPECT_VALUE_EQUAL(TRUE, bRet);

	bRet = StringEndsWith(csActual, csNot);
	TEST_EXPECT_VALUE_EQUAL(FALSE, bRet); 

	TEST_END();
}



TEST_START("CString");
TEST_ADD(__TestStringSplit);
TEST_ADD(__TestStringReplace);
TEST_ADD(__TestStringContains);
TEST_ADD(__TestStringStartsWith);
TEST_ADD(__TestStringEndsWith);
TEST_RUN();
TEST_CONCLUDE();