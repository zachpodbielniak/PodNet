#include "../CNetworking/CHttpsRequest.h"
#include <onesdk/onesdk.h>


LONG Main(LONG lArgCount, DLPSTR dlpszArgValues)
{
	UNREFERENCED_PARAMETER(lArgCount);
	UNREFERENCED_PARAMETER(dlpszArgValues);

	HANDLE hHttpsRequest;
	LPSTR lpszOut, lpszMessage;
	UARCHLONG ualOut;
	
	lpszOut = LocalAllocAndZero(65536);
	/* cppcheck-suppress memleak */
	EXIT_IF_UNLIKELY_NULL(lpszOut, 2);

	/*
	hHttpsRequest = CreateHttpsRequest(
		HTTP_REQUEST_GET,
		HTTP_VERSION_1_1,
		"cloudflare-dns.com",
		443,
		"/dns-query?name=ws-zach.podbielniak.com&type=A",
		HTTP_CONNECTION_CLOSE,
		NULLPTR,
		"application/dns-json",
		NULLPTR,
		FALSE,
		FALSE
	);
	*/

	hHttpsRequest = CreateHttpsRequest(
		HTTP_REQUEST_GET,
		HTTP_VERSION_1_1,
		"ipv4life.xyz",
		443,
		"/about",
		HTTP_CONNECTION_CLOSE,
		NULLPTR,
		HTTP_ACCEPT_HTML,
		FALSE,
		TRUE
	);

	ExecuteHttpsRequest(hHttpsRequest, lpszOut, 65535, &ualOut);
	PrintFormat("%s\n", lpszOut);	

	FreeMemory(lpszOut);
	DestroyObject(hHttpsRequest);

	return 0;
}