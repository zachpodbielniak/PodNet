#include "../CMath/CRandom.h"


VOID TestRandom()
{
        HANDLE hRng;
        ULONGLONG ullSeed, ullOut, ullIndex;

        ullSeed = time(NULLPTR);
        hRng = CreateRandomNumberGenerator(RANDOM_SIZE_LONGLONG, &ullSeed, NULL);

        for (
                ullIndex = 0;
                ullIndex < TEN;
                ullIndex++
        ){
                GetSingleRawRandom(hRng, &ullOut, sizeof(ULONGLONG));
                PrintFormat("%llu\n", ullOut % 1000);
        }

        DestroyObject(hRng);
}


VOID TestGetRng()
{
        HANDLE hRng, hLookup;
        ULONGLONG ullSeed;
        ullSeed = time(NULLPTR);
        hRng = CreateRandomNumberGenerator(RANDOM_SIZE_LONGLONG, &ullSeed, "MainRng");
        hLookup = GetRandomNumberGeneratorByName("MainRng");
        PrintFormat("%p\t%p\n", hRng, hLookup);
        DestroyObject(hRng);
}


VOID TestGetRngByAtom()
{
        HANDLE hRng, hLookup;
        ATOM atRngName;
        ULONGLONG ullSeed;

        atRngName = PushGlobalAtom("AtomRng");
        ullSeed = time(NULLPTR);
        hRng = CreateRandomNumberGenerator(RANDOM_SIZE_LONGLONG, &ullSeed, "AtomRng");
        hLookup = GetRandomNumberGeneratorByAtom(NULLPTR, atRngName);
        PrintFormat("%p\t%p\n", hRng, hLookup);
        DestroyObject(hRng);
}


VOID TestGlobalRandom()
{
        ULONGLONG ullOut;
        GetSingleGlobalRawRandom(&ullOut, NULL);
        PrintFormat("TestGlobalRandom()\n%lu\n", ullOut);
}

VOID TestMultipleGlobalRandom()
{
        ULONGLONG ullOuts[0x10];
        UARCHLONG ualNumberWritten, ualIndex;
        GetMultipleGlobalRawRandoms(0x10, ullOuts, sizeof(ullOuts)-24, &ualNumberWritten, NULL);
        PrintFormat("TestMultipleGlobalRandom()\n");
        
        for (
                ualIndex = 0;
                ualIndex < ualNumberWritten;
                ualIndex++
        ){ PrintFormat("%lu\n", ullOuts[ualIndex] % 100); }

        PrintFormat("Number Written: %lu\n", ualNumberWritten);
}


LONG Main(LONG lArgCount, DLPSTR dlpszArgValues)
{
        UNREFERENCED_PARAMETER(lArgCount);
        UNREFERENCED_PARAMETER(dlpszArgValues);

        TestGetRng();
        TestGetRngByAtom();
        TestGlobalRandom();
        TestMultipleGlobalRandom();
        return 0;
}
