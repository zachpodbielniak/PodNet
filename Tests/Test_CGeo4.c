#include "../CUnitTest/CUnitTest.h"
#include "../CNetworking/CGeo4.h"


#define IPV4_ONE	"71.78.219.188"
#define IPV4_TWO 	"131.183.223.0"
#define IPV4_THREE	"46.220.13.5"


TEST_CALLBACK(__TestGetCountryByIpv4Address)
{
	TEST_BEGIN("GetCountryByIpv4Address");

	IPV4_ADDRESS ip4One, ip4Two, ip4Three;

	ip4One = CreateIpv4AddressFromString(IPV4_ONE);
	ip4Two = CreateIpv4AddressFromString(IPV4_TWO);
	ip4Three = CreateIpv4AddressFromString(IPV4_THREE);	

	TEST_EXPECT_STRING_EQUAL(GetCountryByIpv4Address(ip4One), "USA");
	TEST_EXPECT_STRING_EQUAL(GetCountryByIpv4Address(ip4Two), "USA");
	TEST_EXPECT_STRING_EQUAL(GetCountryByIpv4Address(ip4Three), "AUT");

	TEST_END();
}


TEST_CALLBACK(__TestGetCountryByIpv4String)
{
	TEST_BEGIN("GetCountryByIpv4String");

	TEST_EXPECT_STRING_EQUAL(GetCountryByIpv4String(IPV4_ONE), "USA");
	TEST_EXPECT_STRING_EQUAL(GetCountryByIpv4String(IPV4_TWO), "USA");
	TEST_EXPECT_STRING_EQUAL(GetCountryByIpv4String(IPV4_THREE), "AUT");

	TEST_END();
}


TEST_CALLBACK(__TestGetCityByIpv4Address)
{
	TEST_BEGIN("GetCityByIpv4Address");

	IPV4_ADDRESS ip4One, ip4Two, ip4Three;
	CSTRING csBuffer[0x20];

	ip4One = CreateIpv4AddressFromString(IPV4_ONE);
	ip4Two = CreateIpv4AddressFromString(IPV4_TWO);
	ip4Three = CreateIpv4AddressFromString(IPV4_THREE);	

	GetCityByIpv4Address(ip4One, csBuffer, sizeof(csBuffer));
	TEST_EXPECT_STRING_EQUAL(csBuffer, "\0");

	GetCityByIpv4Address(ip4Two, csBuffer, sizeof(csBuffer));
	TEST_EXPECT_STRING_EQUAL(csBuffer, "Woodville");

	GetCityByIpv4Address(ip4Three, csBuffer, sizeof(csBuffer));
	TEST_EXPECT_STRING_EQUAL(csBuffer, "\0");


	TEST_END();
}


TEST_CALLBACK(__TestGetCityByIpv4String)
{
	TEST_BEGIN("GetCityByIpv4String");

	CSTRING csBuffer[0x20];

	GetCityByIpv4String(IPV4_ONE, csBuffer, sizeof(csBuffer));
	TEST_EXPECT_STRING_EQUAL(csBuffer, "\0");

	GetCityByIpv4String(IPV4_TWO, csBuffer, sizeof(csBuffer));
	TEST_EXPECT_STRING_EQUAL(csBuffer, "Woodville");

	GetCityByIpv4String(IPV4_THREE, csBuffer, sizeof(csBuffer));
	TEST_EXPECT_STRING_EQUAL(csBuffer, "\0");

	TEST_END();
}



TEST_CALLBACK(__TestGetRecordByIpv4Address)
{
	TEST_BEGIN("GetRecordByIpv4Address");

	IPV4_ADDRESS ip4One, ip4Two, ip4Three;
	LPGEOIPRECORD lpgipRecord;

	ip4One = CreateIpv4AddressFromString(IPV4_ONE);
	ip4Two = CreateIpv4AddressFromString(IPV4_TWO);
	ip4Three = CreateIpv4AddressFromString(IPV4_THREE);	

	lpgipRecord = GetRecordByIpv4Address(ip4One);
	TEST_EXPECT_VALUE_NOT_EQUAL(lpgipRecord, NULLPTR);
	TEST_EXPECT_STRING_EQUAL(lpgipRecord->country_name, "United States");
	TEST_EXPECT_VALUE_NOT_EQUAL(DestroyIpv4Record(lpgipRecord), FALSE);

	lpgipRecord = GetRecordByIpv4Address(ip4Two);
	TEST_EXPECT_VALUE_NOT_EQUAL(lpgipRecord, NULLPTR);
	TEST_EXPECT_STRING_EQUAL(lpgipRecord->country_name, "United States");
	TEST_EXPECT_VALUE_NOT_EQUAL(DestroyIpv4Record(lpgipRecord), FALSE);
	
	lpgipRecord = GetRecordByIpv4Address(ip4Three);
	TEST_EXPECT_VALUE_NOT_EQUAL(lpgipRecord, NULLPTR);
	TEST_EXPECT_STRING_EQUAL(lpgipRecord->country_name, "Austria");
	TEST_EXPECT_VALUE_NOT_EQUAL(DestroyIpv4Record(lpgipRecord), FALSE);

	TEST_END()
}


TEST_CALLBACK(__TestGetRecordByIpv4String)
{
	TEST_BEGIN("GetRecordByIpv4String");

	LPGEOIPRECORD lpgipRecord;

	lpgipRecord = GetRecordByIpv4String(IPV4_ONE);
	TEST_EXPECT_VALUE_NOT_EQUAL(lpgipRecord, NULLPTR);
	TEST_EXPECT_STRING_EQUAL(lpgipRecord->country_name, "United States");
	TEST_EXPECT_VALUE_NOT_EQUAL(DestroyIpv4Record(lpgipRecord), FALSE);

	lpgipRecord = GetRecordByIpv4String(IPV4_TWO);
	TEST_EXPECT_VALUE_NOT_EQUAL(lpgipRecord, NULLPTR);
	TEST_EXPECT_STRING_EQUAL(lpgipRecord->country_name, "United States");
	TEST_EXPECT_VALUE_NOT_EQUAL(DestroyIpv4Record(lpgipRecord), FALSE);
	
	lpgipRecord = GetRecordByIpv4String(IPV4_THREE);
	TEST_EXPECT_VALUE_NOT_EQUAL(lpgipRecord, NULLPTR);
	TEST_EXPECT_STRING_EQUAL(lpgipRecord->country_name, "Austria");
	TEST_EXPECT_VALUE_NOT_EQUAL(DestroyIpv4Record(lpgipRecord), FALSE);

	TEST_END()
}


TEST_CALLBACK(__TestGetNetRangeByIpv4Address)
{
	TEST_BEGIN("GetNetRangeByIpv4Address");

	IPV4_ADDRESS ip4One, ip4Two, ip4Three;
	DLPSTR dlpszRange;

	ip4One = CreateIpv4AddressFromString(IPV4_ONE);
	ip4Two = CreateIpv4AddressFromString(IPV4_TWO);
	ip4Three = CreateIpv4AddressFromString(IPV4_THREE);	

	dlpszRange = GetNetRangeByIpv4Address(ip4One);
	TEST_EXPECT_VALUE_NOT_EQUAL(dlpszRange, NULLPTR);
	TEST_EXPECT_STRING_NOT_EQUAL(dlpszRange[0], "\0");
	TEST_EXPECT_STRING_NOT_EQUAL(dlpszRange[1], "\0");
	TEST_EXPECT_VALUE_EQUAL(DestroyIpv4NetRange(dlpszRange), TRUE);

	dlpszRange = GetNetRangeByIpv4Address(ip4Two);
	TEST_EXPECT_VALUE_NOT_EQUAL(dlpszRange, NULLPTR);
	TEST_EXPECT_STRING_NOT_EQUAL(dlpszRange[0], "\0");
	TEST_EXPECT_STRING_NOT_EQUAL(dlpszRange[1], "\0");
	TEST_EXPECT_VALUE_EQUAL(DestroyIpv4NetRange(dlpszRange), TRUE);

	dlpszRange = GetNetRangeByIpv4Address(ip4Three);
	TEST_EXPECT_VALUE_NOT_EQUAL(dlpszRange, NULLPTR);
	TEST_EXPECT_STRING_NOT_EQUAL(dlpszRange[0], "\0");
	TEST_EXPECT_STRING_NOT_EQUAL(dlpszRange[1], "\0");
	TEST_EXPECT_VALUE_EQUAL(DestroyIpv4NetRange(dlpszRange), TRUE);

	TEST_END();
}




TEST_START("CGeo4");
TEST_ADD(__TestGetCountryByIpv4Address);
TEST_ADD(__TestGetCountryByIpv4String);
TEST_ADD(__TestGetCityByIpv4Address);
TEST_ADD(__TestGetCityByIpv4String);
TEST_ADD(__TestGetRecordByIpv4Address);
TEST_ADD(__TestGetRecordByIpv4String);
TEST_ADD(__TestGetNetRangeByIpv4Address);
TEST_RUN();
TEST_CONCLUDE();