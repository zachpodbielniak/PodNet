/*


 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		Test_CAtomTable.c
Author:		Zach Podbielniak
Last Updated: 	11/22/2017

Overview:	This file is used for testing the PodNet C API.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


*/


#include "../CAtom/CAtomTable.h"






VOID TestGlobalAtom()
{
        ATOM atString, atError, atWarning;
        LPSTR lpszTest;
        lpszTest = "This is a test string.";
        CSTRING csBuffer[1024];
        atString = PushGlobalAtom(lpszTest);
        atError = PushGlobalAtom("This is a critical error!");
        atWarning = PushGlobalAtom("This is just a warning, you can ignore me.");        
        PrintFormat("ATOM returned was %d\n", (ULONG)atString);
        GetGlobalAtomValue(atString, csBuffer);
        PrintFormat("Retrieving ATOM Value: %s\n", csBuffer);
        GetGlobalAtomValue(atError, csBuffer);
        PrintFormat("Error: %s\n", csBuffer);
        GetGlobalAtomValue(atWarning, csBuffer);
        PrintFormat("Warning: %s\n", csBuffer);
}


VOID TestCreatingAtomTable()
{
        HANDLE hAtomTable;
        ATOM atAtom;
        CSTRING csBuffer[1024];
        ZeroMemory(csBuffer, 1024);
        hAtomTable = CreateAtomTable(256,256);
        atAtom = PushAtom(hAtomTable, "Test");
        GetAtomValue(hAtomTable, atAtom, csBuffer);
        PrintFormat("Atom recieved back: %s\n", csBuffer);
}



LONG Main(LONG lArgCount, DLPSTR dlpszArgValues)
{
        UNREFERENCED_PARAMETER(lArgCount);
        UNREFERENCED_PARAMETER(dlpszArgValues);

        TestCreatingAtomTable();

        return 0;
}