#include "../CUnitTest/CUnitTest.h"
#include "../CModule/CModule.h"
#include "../CModule/CCompileModule.h"


TEST_CALLBACK(__TestCompileModule)
{
	TEST_BEGIN("CCompileModule - CompileAndLoadModule");

	HANDLE hModule;
	BOOL bTest;

	hModule = CompileAndLoadModule(
		"Test.c",
		"-g -lpodnet_d",
		NULLPTR,
		NULLPTR,
		LOCK_TYPE_MUTEX,
		0,
		NULLPTR,
		TRUE
	);

	TEST_EXPECT_VALUE_NOT_EQUAL(hModule, NULL_OBJECT);

	bTest = DestroyObject(hModule);
	TEST_EXPECT_VALUE_NOT_EQUAL(bTest, FALSE);

	TEST_END();
}


typedef LONG (*MAIN_PROC)(
	LONG 		lArgCount,
	DLPSTR 		dlpszArgValues
);


TEST_CALLBACK(__TestCompileModuleAndCallProc)
{
	TEST_BEGIN("CCompileModule - CompileAndCallProc");

	HANDLE hModule;
	MAIN_PROC lpMain;
	BOOL bTest;

	hModule = CompileAndLoadModule(
		"Test.c",
		"-g -lpodnet_d",
		NULLPTR,
		NULLPTR,
		LOCK_TYPE_MUTEX,
		0,
		NULLPTR,
		TRUE
	);

	TEST_EXPECT_VALUE_NOT_EQUAL(hModule, NULL_OBJECT);

	lpMain = (MAIN_PROC)GetModuleRawProcByName(hModule, "main");
	TEST_EXPECT_VALUE_NOT_EQUAL(lpMain, NULLPTR);

	lpMain(1, NULLPTR);

	bTest = DestroyObject(hModule);
	TEST_EXPECT_VALUE_NOT_EQUAL(bTest, FALSE);

	TEST_END();
}


TEST_START("CCompileModule");
TEST_ADD(__TestCompileModule);
TEST_ADD(__TestCompileModuleAndCallProc);
TEST_RUN();
TEST_CONCLUDE();