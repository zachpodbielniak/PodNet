#include "../CContainers/CLinkedList.h"


LONG Main(LONG lArgCount, DLPSTR dlpszArgValues)
{
        UNREFERENCED_PARAMETER(lArgCount);
        UNREFERENCED_PARAMETER(dlpszArgValues);

        HLINKEDLIST hllList[2];
        BOOL bRet;
        ULONGLONG ullValue, ullTemp;
        LPULONGLONG lpullOut;
        hllList[0] = CreateLinkedList(sizeof(ULONGLONG), FALSE, NULLPTR);
        hllList[1] = CreateLinkedList(sizeof(ULONGLONG), FALSE, NULLPTR);

        ullValue = 0x05;
        bRet = LinkedListAttachBack(hllList[0], &ullValue);        
        ullValue = 0x06;
        bRet = LinkedListAttachFront(hllList[0], &ullValue);
        ullValue = 0x10;
        bRet = LinkedListAttachBack(hllList[1], &ullValue);

        bRet = LinkedListMerge(hllList, 2);

        lpullOut = (LPULONGLONG)LinkedListDetachFront(hllList);
        FreeMemory(lpullOut);
        lpullOut = (LPULONGLONG)LinkedListDetachBack(hllList);
        FreeMemory(lpullOut);


        bRet = DestroyLinkedList(hllList);        

        return 0;
}
