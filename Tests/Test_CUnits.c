#include "../CUnit/CUnits.h"
#include "../CUnit/CConvertUnit.h"


VOID TestUnitConvert(VOID)
{
	DOUBLE dbVal;
	dbVal = 5.0;
	dbVal = ConvertUnit(dbVal, UNIT_TYPE_METER, UNIT_TYPE_CENTIMETER);
	PrintFormat("%f\n", dbVal);
	
}

VOID TestUnitType(VOID)
{
	UNIT uVal;
	uVal.dbValue = 5.0;
	uVal.utUnitType = UNIT_TYPE_METER;
	ConvertUnitEx(&uVal, UNIT_TYPE_CENTIMETER);
	PrintFormat("%f\n", uVal.dbValue);
}


LONG Main(LONG lArgCount, DLPSTR dlpszArgValues)
{
	UNREFERENCED_PARAMETER(lArgCount);
	UNREFERENCED_PARAMETER(dlpszArgValues);

	TestUnitConvert();	
	TestUnitType();

	return 0;
}
