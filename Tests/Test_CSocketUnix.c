#include "../CUnitTest/CUnitTest.h"
#include "../CNetworking/CSocketUnix.h"

TEST_CALLBACK(__CreateUnixSocket)
{
	TEST_BEGIN("CSocketUnix - CreateUnixSocket");

	HANDLE hSocket;
	BOOL bRet;

	hSocket = CreateSocketUnix("./socket.sock");
	TEST_EXPECT_VALUE_NOT_EQUAL(hSocket, NULL_OBJECT);

	bRet = DestroyObject(hSocket);
	TEST_EXPECT_VALUE_NOT_EQUAL(bRet, FALSE);

	TEST_END();
}

LPVOID 
__Callback(
	_In_ 		LPVOID 		lpParam
){
	UNREFERENCED_PARAMETER(lpParam);
	HANDLE hSocket;
	
	Sleep(2000);
	hSocket = CreateSocketUnix("/home/zach/Documents/Projects/PodNet/socket.sock");
	SocketConnectUnix(hSocket);

	SocketSendUnix(hSocket, "Data", StringLength("Data"));

	Sleep(10000);

	return 0;
}


TEST_CALLBACK(__SendAndReceiveUnixSocket)
{
	TEST_BEGIN("CSocketUnix - SendAndReceiveUnixSocket");

	HANDLE hSocket;
	HANDLE hClient;
	BOOL bRet;
	CSTRING csBuffer[128];

	ZeroMemory(csBuffer, sizeof(csBuffer));

	hSocket = CreateSocketUnix("/home/zach/Documents/Projects/PodNet/socket.sock");
	TEST_EXPECT_VALUE_NOT_EQUAL(hSocket, NULL_OBJECT);

	bRet = BindOnSocketUnix(hSocket);
	TEST_EXPECT_VALUE_NOT_EQUAL(bRet, FALSE);

	bRet = ListenOnBoundSocketUnix(hSocket, 1000);
	TEST_EXPECT_VALUE_NOT_EQUAL(bRet, FALSE);

	CreateThread(__Callback, NULLPTR, 0);
	
	hClient = AcceptConnectionUnix(hSocket);

	SocketReceiveUnix(hClient, csBuffer, sizeof(csBuffer));
	PrintFormat("%s\n", csBuffer);

	bRet = DestroyObject(hClient);
	TEST_EXPECT_VALUE_NOT_EQUAL(bRet, FALSE);

	bRet = DestroyObject(hSocket);
	TEST_EXPECT_VALUE_NOT_EQUAL(bRet, FALSE);

	TEST_END();
}


TEST_START("CSocketUnix");
TEST_ADD(__CreateUnixSocket);
TEST_ADD(__SendAndReceiveUnixSocket);
TEST_RUN();
TEST_CONCLUDE();