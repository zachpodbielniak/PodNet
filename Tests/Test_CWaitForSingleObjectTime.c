#include "CLock/CMutex.h"
#include "CClock/CClock.h"

int main()
{
	TIMESPEC tmSpec;
	HANDLE hLock;
	LPVOID lpRet;
	hLock = CreateMutex();

	HighResolutionClock(&tmSpec);
	lpRet = (LPVOID)GetHandleDerivative(hLock);
	PrintFormat("%llu\n", HighResolutionClockAndGetNanoSeconds(&tmSpec));
	return 0;
}
