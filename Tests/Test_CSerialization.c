#include "../CUnitTest/CUnitTest.h"
#include "../CSerialization/CSerialization.h"



TEST_CALLBACK(__TestByte)
{
	TEST_BEGIN("SerializeByte");

	HANDLE hRegion;
	BYTE byData;
	BYTE byTest;

	hRegion = NULL_OBJECT;
	byData = 0x6f;
	SerializeByte(&hRegion, byData);

	byTest = *(LPBYTE)DynamicHeapRegionData(hRegion, NULLPTR);
	TEST_EXPECT_VALUE_EQUAL(byTest, byData);

	TEST_END();
}

TEST_CALLBACK(__TestWord)
{
	TEST_BEGIN("SerializeWord");

	HANDLE hRegion;
	WORD wData;
	WORD wTest;

	hRegion = NULL_OBJECT;
	wData = 0x6f3f;
	SerializeWord(&hRegion, wData);

	wTest = *(WORD *)DynamicHeapRegionData(hRegion, NULLPTR);
	TEST_EXPECT_VALUE_EQUAL(NetworkToHostShort(wTest), wData);

	TEST_END();
}

TEST_CALLBACK(__TestDword)
{
	TEST_BEGIN("SerializeDword");

	HANDLE hRegion;
	DWORD dwData;
	DWORD dwTest;

	hRegion = NULL_OBJECT;
	dwData = 0x6f3f2f1f;
	SerializeDword(&hRegion, dwData);

	dwTest = *(DWORD *)DynamicHeapRegionData(hRegion, NULLPTR);
	TEST_EXPECT_VALUE_EQUAL(NetworkToHostLong(dwTest), dwData);

	TEST_END();
}

TEST_CALLBACK(__TestQword)
{
	TEST_BEGIN("SerializeQword");

	HANDLE hRegion;
	QWORD qwData;
	QWORD qwTest;

	hRegion = NULL_OBJECT;
	qwData = 0x6f3f2f1f0fcfbfdf;
	SerializeQword(&hRegion, qwData);

	qwTest = *(QWORD *)DynamicHeapRegionData(hRegion, NULLPTR);
	TEST_EXPECT_VALUE_EQUAL(NetworkToHostLongLong(qwTest), qwData);

	TEST_END();
}

TEST_CALLBACK(__TestString)
{
	TEST_BEGIN("SerializeString");

	HANDLE hRegion;
	LPSTR lpszData = "Hello World!";
	LPSTR lpszOut;
	QWORD qwLength;
	QWORD qwOut;
	LPVOID lpData;

	hRegion = NULL_OBJECT;
	qwLength = StringLength(lpszData);
	SerializeString(&hRegion, lpszData);

	lpData = DynamicHeapRegionData(hRegion, NULLPTR);
	qwOut = *(QWORD *)lpData;
	qwOut = NetworkToHostLongLong(qwOut);
	lpData = (LPVOID)((UARCHLONG)lpData + sizeof(QWORD));
	lpszOut = lpData;

	TEST_EXPECT_VALUE_EQUAL(qwOut, qwLength);
	TEST_EXPECT_STRING_EQUAL(lpszOut, lpszData);


	TEST_END();
}


TEST_START("CSerialization");
TEST_ADD(__TestByte);
TEST_ADD(__TestWord);
TEST_ADD(__TestDword);
TEST_ADD(__TestQword);
TEST_ADD(__TestString);
TEST_RUN();
TEST_CONCLUDE();