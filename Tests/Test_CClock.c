/*


 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		Test_CClock.c
Author:		Zach Podbielniak
Last Updated: 	11/22/2017

Overview:	This file is used for testing the PodNet C API.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


*/


#include "../CClock/CClock.h"




LONG Main(LONG lArgCount, DLPSTR dlpszArgValues)
{
        UNREFERENCED_PARAMETER(lArgCount);
        UNREFERENCED_PARAMETER(dlpszArgValues);

        CLOCK clkTimer;
        DONT_OPTIMIZE_OUT ULONG ulIndex;
        TIMESPEC tmSpec;
        Clock(&clkTimer);
        for (ulIndex = 0; ulIndex < MAX_ULONG / 4; ulIndex++) { (VOID)0; }
        CreateTimeSpec(&tmSpec, 1500);

        printf("Nanoseconds:  %llu\n", ClockAndGetNanoSeconds(&clkTimer));
        printf("Microseconds: %llu\n", ClockAndGetMicroSeconds(&clkTimer));
        printf("Milliseconds: %llu\n", ClockAndGetMilliseconds(&clkTimer));
        printf("Seconds:      %llu\n", ClockAndGetSeconds(&clkTimer));
        printf("Float Sec:    %f\n", ClockAndGetSecondsFloat(&clkTimer));
        printf("TimeSpec:     %llu\t%llu\n", (ULONGLONG)tmSpec.tv_sec, (ULONGLONG)tmSpec.tv_nsec);

        return 0;
}