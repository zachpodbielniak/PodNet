#include "../CNetworking/CHttpRequest.h"
#include <onesdk/onesdk.h>


#define HTTP_ENG_UTOLEDO 		"131.183.18.23"


VOID TestTextFetch(VOID)
{
        HANDLE hRequest;
        LPSTR lpszOut;
	UARCHLONG ualBytesOut;

        lpszOut = LocalAllocAndZero(sizeof(CHAR) * 65536);

        hRequest = CreateHttpRequest(
                HTTP_REQUEST_GET,
                HTTP_VERSION_1_0,
                "icanhazip.com",
                80,
                "/",
                NULLPTR,
		HTTP_ACCEPT_PLAIN_TEXT,
                FALSE
        );

        ExecuteHttpRequest(hRequest, lpszOut, 65536, &ualBytesOut);
        PrintFormat("%s\n", lpszOut);
	PrintFormat("\n%lu Bytes Were Read.\n", ualBytesOut);

	ZeroMemory(lpszOut, 65536);

	PrintFormat("\n\n\n");

        ExecuteHttpRequest(hRequest, lpszOut, 65536, &ualBytesOut);
        PrintFormat("%s\n", lpszOut);
	PrintFormat("\n%lu Bytes Were Read.\n", ualBytesOut);
	
	FreeMemory(lpszOut);
	DestroyObject(hRequest);
}

VOID TestPost(VOID)
{
	HANDLE hRequest;
	DLPSTR dlpszHeaders;
	DLPSTR dlpszData; 
	
	dlpszHeaders = LocalAllocAndZero(sizeof(LPSTR) * 2);
	dlpszData = LocalAllocAndZero(sizeof(LPSTR));
	dlpszHeaders[0] = "PodNet-Token: 123456789";
	dlpszHeaders[1] = "Content-Type: application/json";
	dlpszData[0] = "{\"data\": \"value\"}";

	CSTRING csData[8192];
	ZeroMemory(csData, sizeof(csData));

	hRequest = CreateHttpRequestEx(
		HTTP_REQUEST_POST,
		HTTP_VERSION_1_1,
		"127.0.0.1",
		5001,
		"/",
		HTTP_CONNECTION_KEEP_ALIVE,
		NULLPTR,
		HTTP_ACCEPT_PLAIN_TEXT,
		dlpszHeaders,
		2,
		dlpszData,
		1,
		FALSE
	);

	ExecuteHttpRequest(hRequest, csData, sizeof(csData) - 1, NULLPTR);
	PrintFormat("%s\n", csData);
	DestroyObject(hRequest);
}

LONG Main(LONG lArgCount, DLPSTR dlpszArgValues)
{
        UNREFERENCED_PARAMETER(lArgCount);
        UNREFERENCED_PARAMETER(dlpszArgValues);

	TestTextFetch();
	TestPost();

        return 0;
}