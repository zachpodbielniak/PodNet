#include "../CUnitTest/CUnitTest.h"
#include "../CProcess/CProcess.h"



TEST_CALLBACK(__TestCreateProcess)
{
	TEST_BEGIN("CreateProcess");

	HANDLE hProcess, hFile;
	LPSTR lpszOut;
	BOOL bTest;
	
	lpszOut = LocalAllocAndZero(4096);

	hProcess = CreateProcess("/usr/bin/ls", "-lah *", FALSE);
	TEST_EXPECT_VALUE_NOT_EQUAL(hProcess, NULL_OBJECT);

	hFile = GetProcessOutput(hProcess);
	TEST_EXPECT_VALUE_NOT_EQUAL(hFile, NULL_OBJECT);

	bTest = WaitForSingleObject(hProcess, INFINITE_WAIT_TIME);
	TEST_EXPECT_VALUE_NOT_EQUAL(bTest, FALSE);

	/*while (0 < ReadLineFromFile(hFile, &lpszOut, 4095, 0))
	{ PrintFormat("%s", lpszOut); }*/

	
	bTest = DestroyObject(hProcess);
	TEST_EXPECT_VALUE_EQUAL(bTest, TRUE);

	FreeMemory(lpszOut);

	TEST_END();
}

TEST_CALLBACK(__TestCreateProcessEx)
{
	TEST_BEGIN("CreateProcessEx");
	HANDLE hProcess, hOut;
	LPSTR lpszOut;
	BOOL bTest;
	
	hOut = CreateFile("out.txt", FILE_PERMISSION_CREATE | FILE_PERMISSION_READ | FILE_PERMISSION_WRITE, 0);
	TEST_EXPECT_VALUE_NOT_EQUAL(hOut, NULL_OBJECT);

	lpszOut = LocalAllocAndZero(4096);

	hProcess = CreateProcessEx("ls", "* -lah", TRUE, hOut);
	TEST_EXPECT_VALUE_NOT_EQUAL(hProcess, NULL_OBJECT);

	bTest = WaitForSingleObject(hProcess, INFINITE_WAIT_TIME);
	TEST_EXPECT_VALUE_NOT_EQUAL(bTest, FALSE);

	/*while (0 < ReadLineFromFile(hFile, &lpszOut, 4095, 0))
	{ PrintFormat("%s", lpszOut); }*/

	
	bTest = DestroyObject(hProcess);
	TEST_EXPECT_VALUE_EQUAL(bTest, TRUE);

	FreeMemory(lpszOut);

	TEST_END();
}



TEST_START("CProcess");
TEST_ADD(__TestCreateProcess);
TEST_ADD(__TestCreateProcessEx);
TEST_RUN();
TEST_CONCLUDE();