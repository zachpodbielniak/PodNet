/*


 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		Test_CFastApprox.c
Author:		Zach Podbielniak
Last Updated: 	11/22/2017

Overview:	This file is used for testing the PodNet C API.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


*/


#include "../CMath/CFastApprox.h"
#include "../CClock/CClock.h"





VOID TestFastSinApprox(VOID)
{
	FLOAT fIndex;
	FLOAT fVal;
	CLOCK clkClock;
	Clock(&clkClock);
	

	for (fIndex = -(PI_2); fIndex < (PI_2); fIndex += 0.0000001f)
		fVal = atanf(fIndex);
	PrintFormat("StdFun : %f -> %llu.\n", atanf(PI_8), ClockAndGetMicroSeconds(&clkClock));

	Clock(&clkClock);
	for (fIndex = -(PI_2); fIndex < (PI_2); fIndex += 0.0000001f)
		fVal = FAST_APPROX_ARCTAN(fIndex);
	UNREFERENCED_PARAMETER(fVal);
	PrintFormat("My implementation : %f -> %llu\n", FastApproxSin(PI_8), ClockAndGetMicroSeconds(&clkClock));
}




LONG Main(LONG lArgCount, DLPSTR dlpszArgValues)
{
	UNREFERENCED_PARAMETER(lArgCount);
	UNREFERENCED_PARAMETER(dlpszArgValues);

	TestFastSinApprox();

	return 0;
}
