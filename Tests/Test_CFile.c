/*


 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		Test_CFile.c
Author:		Zach Podbielniak
Last Updated: 	11/22/2017

Overview:	This file is used for testing the PodNet C API.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


*/


#include "../CFile/CFile.h"


VOID TestOpenFileAndWrite(VOID)
{
        HANDLE hFile;
        CSTRING csBuffer[100];
        hFile = OpenFile("output.txt", FILE_PERMISSION_WRITE, NULL);
        StringPrintFormat(csBuffer, "This is a formatted string. Testing %d, %d, %d\n", 1, 2, 3);

        WriteFile(hFile, "This is a test.\n", NULL);
        WriteFile(hFile, "This is a second line, does it work?\n", NULL);
        WriteFile(hFile, csBuffer, NULL);
        DestroyHandle(hFile);
}

VOID TestOpenFileAndRead(VOID)
{
        HANDLE hFile;
        hFile = OpenFile("/proc/self/statm", FILE_PERMISSION_READ, NULL);
        LPSTR lpstrBuffer;

        ReadWholeFile(hFile, &lpstrBuffer, FILE_READ_ALLOC_OUT);

        PrintFormat("Opened File and Read: %s\n", lpstrBuffer);
        FreeMemory(lpstrBuffer);
        DestroyHandle(hFile);
}

VOID TestStdOut(VOID)
{
        WriteFile(GetStandardOut(), "This is a test to Standard Out.\n", NULL);
}

VOID TestMemFile(VOID)
{
	HANDLE hFile;
	CSTRING csData[] = "This is a line\nAnother line\nEnding\n";
	LPSTR lpszBuffer;

	lpszBuffer = LocalAllocAndZero(4096);
	hFile = OpenFileFromMemory(csData, sizeof(csData), FILE_PERMISSION_READ, 0);

	while(-1 != ReadLineFromFile(hFile, &lpszBuffer, 4095, 0))
	{ PrintFormat("%s", lpszBuffer); }

	FreeMemory(lpszBuffer);
	DestroyObject(hFile);
}


LONG Main(LONG lArgCount, DLPSTR dlpszArgValues)
{
        UNREFERENCED_PARAMETER(lArgCount);
        UNREFERENCED_PARAMETER(dlpszArgValues);

	/*
        TestOpenFileAndWrite();
        TestOpenFileAndRead();
        TestStdOut();
	*/
	TestMemFile();

        return 0;
}