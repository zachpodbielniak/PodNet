#include "../CDevices/CGpio.h"


LONG Main(LONG lArgCount, DLPSTR dlpszArgValues)
{
        UNREFERENCED_PARAMETER(lArgCount);
        UNREFERENCED_PARAMETER(dlpszArgValues);

        HANDLE hPin1, hPin2, hPin3;

        hPin1 = CreateGpioInterfaceAndSpecifyPinMode(0x02, GPIO_MODE_OUTPUT, GPIO_PULL_MODE_OFF);
        hPin2 = CreateGpioInterfaceAndSpecifyPinMode(0x02, GPIO_MODE_OUTPUT, GPIO_PULL_MODE_OFF);
        hPin3 = CreateGpioInterfaceAndSpecifyPinMode(0x03, GPIO_MODE_PWM, GPIO_PULL_MODE_OFF);

        return 0;
}