#include "../CUnitTest/CUnitTest.h"
#include "../CLex/CLexer.h"


TEST_CALLBACK(__TestLexeme)
{
	TEST_BEGIN("CLex-Lexeme");

	CSTRING *dlpszSplitters[] = {" ", "\n"};
	CSTRING csText[] = "this  that\n4";
	CSTRING csBuffer0[4096];
	CSTRING csBuffer1[4096];

	HANDLE hLex;
	BOOL bRet;
	UARCHLONG ualSize;

	ZeroMemory(csBuffer0, sizeof(csBuffer0));
	ZeroMemory(csBuffer1, sizeof(csBuffer1));

	hLex = CreateLexer(
		4096,
		(DLPSTR)dlpszSplitters,
		2
	);
	TEST_EXPECT_VALUE_NOT_EQUAL(hLex, NULL_OBJECT);
	
	bRet = LexerSetText(hLex, csText);
	TEST_EXPECT_VALUE_NOT_EQUAL(bRet, FALSE);

	while ((UARCHLONG)-1 != LexerAnalyzeNextLexeme(hLex))
	{
		LexerGetLastLexeme(
			hLex,
			csBuffer0,
			sizeof(csBuffer0),
			csBuffer1,
			sizeof(csBuffer1)
		);
		PrintFormat("Lexeme: %s\nSplitter: %s\n", csBuffer0, csBuffer1);
		ZeroMemory(csBuffer0, sizeof(csBuffer0));
		ZeroMemory(csBuffer1, sizeof(csBuffer1));
	}

	DestroyObject(hLex);
	
	TEST_END();
}


TEST_START("CLex");
TEST_ADD(__TestLexeme);
TEST_RUN();
TEST_CONCLUDE();