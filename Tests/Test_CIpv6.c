#include "../CNetworking/CIpv6.h"


LONG Main(LONG lArgCount, DLPSTR dlpszArgValues)
{
	UNREFERENCED_PARAMETER(lArgCount);
	UNREFERENCED_PARAMETER(dlpszArgValues);

        LPIPV6_ADDRESS lpip6Address, lpip6AddressDup;
	CSTRING csOut[64];
	BOOL bComp;

	ZeroMemory(csOut, sizeof(csOut));
        lpip6Address = CreateIpv6AddressFromString("2600:387:0:802::81");
	ConvertIpv6AddressToCompressedString(lpip6Address, csOut, sizeof(csOut));
	
	lpip6AddressDup = CreateIpv6AddressFromString(csOut);
	
	PrintFormat("IP Address Was: %s\n", csOut);

	/*
	lpip6AddressDup = CreateIpv6AddressFromString(csOut);
	bComp = TRUE;
	bComp &= lpip6Address->ultuData.ullX[0] == lpip6AddressDup->ultuData.ullX[0];
	bComp &= lpip6Address->ultuData.ullX[1] == lpip6AddressDup->ultuData.ullX[1];

	PrintFormat("IPs are Equal: %hhu\n", bComp);
	*/
	
        DestroyIpv6Address(lpip6Address);
        return 0;
}