/*


 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		Test_CVector.c
Author:		Zach Podbielniak
Last Updated: 	11/22/2017

Overview:	This file is used for testing the PodNet C API.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


*/


#include "../CContainers/CVector.c"

LONG Main(LONG lArgCount, DLPSTR dlpszArgValues)
{
	UNREFERENCED_PARAMETER(lArgCount);
	UNREFERENCED_PARAMETER(dlpszArgValues);
	HINSTANCE hiVectorLib;
	HVECTOR hVector;
	ULONG ulData;
	LPULONG lpulData;
    lpulData = malloc(sizeof(ULONG));
	hiVectorLib = InitializeVectorLibrary(NULL);
	hVector = CreateVector(5, sizeof(ULONG), NULL);
	ulData = 123;
    VectorPushBack(hVector, &ulData, 0);
	VectorPopBack(hVector, lpulData);	

	printf("Value at 0 : %lu\n", (*lpulData));
	CloseVector(hVector);
	CloseVectorLibrary(NULL);
	return 0;
}
