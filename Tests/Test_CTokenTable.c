#include "../CUnitTest/CUnitTest.h"
#include "../CLex/CTokenTable.h"


TEST_CALLBACK(__TestCreateTokenTable)
{
	TEST_BEGIN("CTokenTable - CreateTokenTable");

	HANDLE hTokenTable;
	LPTOKEN lptknDesc;
	BOOL bRet;

	lptknDesc = LocalAllocAndZero(sizeof(TOKEN) * 3);

	lptknDesc[0].lxValue = "One";
	lptknDesc[0].tfValue = 1;
	lptknDesc[1].lxValue = "Two";
	lptknDesc[1].tfValue = 2;
	lptknDesc[2].lxValue = "Three";
	lptknDesc[2].tfValue = 3;


	hTokenTable = CreateTokenTable(lptknDesc, 3);
	TEST_EXPECT_VALUE_NOT_EQUAL(hTokenTable, NULL_OBJECT);

	bRet = DestroyObject(hTokenTable);
	TEST_EXPECT_VALUE_NOT_EQUAL(bRet, FALSE);

	TEST_END();
}


TEST_CALLBACK(__TestTokenTableGetType)
{
	TEST_BEGIN("CTokenTable - TokenTableGetType");

	HANDLE hTokenTable;
	LPTOKEN lptknDesc;
	TOKENFLAG tfValue;
	BOOL bRet;
	LPSTR lpszOne, lpszTwo, lpszThree;
	lpszOne = LocalAllocAndZero(8);
	lpszTwo = LocalAllocAndZero(8);
	lpszThree = LocalAllocAndZero(8);

	strcpy(lpszOne, "One");
	strcpy(lpszTwo, "Two");
	strcpy(lpszThree, "Three");

	lptknDesc = LocalAllocAndZero(sizeof(TOKEN) * 3);

	lptknDesc[0].lxValue = lpszOne;
	lptknDesc[0].tfValue = 1;
	lptknDesc[1].lxValue = lpszTwo;
	lptknDesc[1].tfValue = 2;
	lptknDesc[2].lxValue = lpszThree;
	lptknDesc[2].tfValue = 3;


	hTokenTable = CreateTokenTable(lptknDesc, 3);
	TEST_EXPECT_VALUE_NOT_EQUAL(hTokenTable, NULL_OBJECT);

	tfValue = TokenTableGetType(hTokenTable, "One");
	TEST_EXPECT_VALUE_EQUAL(tfValue, 1);
	tfValue = TokenTableGetType(hTokenTable, "Two");
	TEST_EXPECT_VALUE_EQUAL(tfValue, 2);
	tfValue = TokenTableGetType(hTokenTable, "Three");
	TEST_EXPECT_VALUE_EQUAL(tfValue, 3);


	bRet = DestroyObject(hTokenTable);
	TEST_EXPECT_VALUE_NOT_EQUAL(bRet, FALSE);

	TEST_END();
}


TEST_START("CTokenTable");
TEST_ADD(__TestCreateTokenTable);
TEST_ADD(__TestTokenTableGetType);
TEST_RUN();
TEST_CONCLUDE();