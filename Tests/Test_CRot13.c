/*


 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		Test_CRot13.c
Author:		Zach Podbielniak
Last Updated: 	11/22/2017

Overview:	This file is used for testing the PodNet C API.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


*/


#include "../CAlgorithms/CRot13.h"


LONG Main(LONG lArgCount, DLPSTR dlpszArgValues)
{
        UNREFERENCED_PARAMETER(lArgCount);

        CSTRING csIn[256];
        StringCopy(csIn, dlpszArgValues[1]);
        Rot13(csIn);
        PrintFormat("After Rot13(): %s\n", csIn);
        Rot13(csIn);
        PrintFormat("Should be normal: %s\n", csIn);

        return 0;
}