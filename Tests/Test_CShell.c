#include "../CUnitTest/CUnitTest.h"
#include "../CScripting/CShell.h"



TEST_CALLBACK(__TestRunShell)
{
	TEST_BEGIN("RunShell");

	LONG lRet;
	LPSTR lpszOutput;

	#ifdef __FreeBSD__
	RunShell(
		"/usr/local/bin/bash",
		"echo -e 'hello\n1\n2\n3\n'",
		&lpszOutput
	);
	#else
	RunShell(
		"/bin/bash",
		"echo -e 'hello\n1\n2\n3\n'",
		&lpszOutput
	);
	#endif

	/* Expect extra '\n' due to how echo works */
	TEST_EXPECT_STRING_EQUAL(lpszOutput, "hello\n1\n2\n3\n\n");

	PrintFormat("%s\n", lpszOutput);
	FreeMemory(lpszOutput);

	TEST_END()
}


TEST_CALLBACK(__TestRunShellFile)
{
	TEST_BEGIN("RunShellFile");

	LONG lRet;
	LPSTR lpszOutput;

	#ifdef __FreeBSD__
	RunShellFile(
		"/usr/local/bin/bash",
		"./Scripts/Test",
		"1 2",
		&lpszOutput
	);
	#else
	RunShellFile(
		"/bin/bash",
		"./Scripts/Test",
		"1 2",
		&lpszOutput
	);
	#endif

	/* Expect extra '\n' due to how echo works */
	TEST_EXPECT_STRING_EQUAL(lpszOutput, "1 2\n");

	PrintFormat("%s\n", lpszOutput);
	FreeMemory(lpszOutput);

	TEST_END()
}


TEST_CALLBACK(__TestRunShellFormatted)
{
	TEST_BEGIN("RunShell");

	LONG lRet;
	LPSTR lpszOutput;

	#ifdef __FreeBSD__
	RunShellFormatted(
		"/usr/local/bin/bash",
		"%s %s '%s'",
		&lpszOutput,
		"echo",
		"-e",
		"hello\n1\n2\n3\n"
	);
	#else
	RunShellFormatted(
		"/bin/bash",
		"%s %s '%s'",
		&lpszOutput,
		"echo",
		"-e",
		"hello\n1\n2\n3\n"
	);
	#endif

	/* Expect extra '\n' due to how echo works */
	TEST_EXPECT_STRING_EQUAL(lpszOutput, "hello\n1\n2\n3\n\n");

	PrintFormat("%s\n", lpszOutput);
	FreeMemory(lpszOutput);

	TEST_END()
}


TEST_START("CShell");
TEST_ADD(__TestRunShell);
TEST_ADD(__TestRunShellFile);
TEST_ADD(__TestRunShellFormatted);
TEST_RUN();
TEST_CONCLUDE();
