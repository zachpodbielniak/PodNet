#include "../CUnitTest/CUnitTest.h"
#include "../CSerialization/CSerialization.h"
#include "../CSerialization/CDeserialization.h"


TEST_CALLBACK(__TestByte)
{
	TEST_BEGIN("DeserializeByte");

	HANDLE hRegion;
	BYTE byData;
	BYTE byTest;
	UARCHLONG ualOffset;

	hRegion = NULL_OBJECT;
	byData = 0x6f;
	ualOffset = 0;

	SerializeByte(&hRegion, byData);
	byTest = DeserializeByte(hRegion, &ualOffset);

	TEST_EXPECT_VALUE_EQUAL(byTest, byData);
	TEST_EXPECT_VALUE_EQUAL(ualOffset, 1);

	TEST_END();
}

TEST_CALLBACK(__TestWord)
{
	TEST_BEGIN("DeserializeWord");

	HANDLE hRegion;
	WORD wData;
	WORD wTest;
	UARCHLONG ualOffset;

	hRegion = NULL_OBJECT;
	wData = 0x6f3f;
	ualOffset = 0;

	SerializeWord(&hRegion, wData);
	wTest = DeserializeWord(hRegion, &ualOffset);

	TEST_EXPECT_VALUE_EQUAL(wTest, wData);
	TEST_EXPECT_VALUE_EQUAL(ualOffset, 2);

	TEST_END();
}

TEST_CALLBACK(__TestDword)
{
	TEST_BEGIN("DeserializeDword");

	HANDLE hRegion;
	DWORD dwData;
	DWORD dwTest;
	UARCHLONG ualOffset;

	hRegion = NULL_OBJECT;
	dwData = 0x6f3f2f1f;
	ualOffset = 0;

	SerializeDword(&hRegion, dwData);
	dwTest = DeserializeDword(hRegion, &ualOffset);

	TEST_EXPECT_VALUE_EQUAL(dwTest, dwData);
	TEST_EXPECT_VALUE_EQUAL(ualOffset, 4);

	TEST_END();
}

TEST_CALLBACK(__TestQword)
{
	TEST_BEGIN("DeserializeQword");

	HANDLE hRegion;
	QWORD qwData;
	QWORD qwTest;
	UARCHLONG ualOffset;

	hRegion = NULL_OBJECT;
	qwData = 0x6f3f2f1f0fcfbfdf;
	ualOffset = 0;

	SerializeQword(&hRegion, qwData);
	qwTest = DeserializeQword(hRegion, &ualOffset);

	TEST_EXPECT_VALUE_EQUAL(qwTest, qwData);
	TEST_EXPECT_VALUE_EQUAL(ualOffset, 8);

	TEST_END();
}

TEST_CALLBACK(__TestString)
{
	TEST_BEGIN("DeserializeString");

	HANDLE hRegion;
	LPSTR lpszData = "Hello World!";
	LPVOID lpData;
	UARCHLONG ualOffset;

	hRegion = NULL_OBJECT;
	ualOffset = 0;

	SerializeString(&hRegion, lpszData);
	lpData = DeserializeString(hRegion, &ualOffset);

	TEST_EXPECT_STRING_EQUAL(lpszData, (LPSTR)lpData);

	TEST_END();
}


TEST_START("CDeserialization");
TEST_ADD(__TestByte);
TEST_ADD(__TestWord);
TEST_ADD(__TestDword);
TEST_ADD(__TestQword);
TEST_ADD(__TestString);
TEST_RUN();
TEST_CONCLUDE();