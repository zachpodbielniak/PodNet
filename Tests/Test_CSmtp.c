#include "../CUnitTest/CUnitTest.h"
#include "../CNetworking/CSmtp.h"


TEST_CALLBACK(__TestCreateSmtpConnection)
{
	TEST_BEGIN("CreateSmtpConnection");

	HANDLE hSmtp;
	BOOL bRet;

	hSmtp = CreateSmtpConnection(
		SMTP_PROTOCOL_SSL,
		"disroot.org",
		NULLPTR,
		NULLPTR,
		TRUE,
		FALSE
	);
	TEST_EXPECT_VALUE_NOT_EQUAL(hSmtp, NULL_OBJECT);

	bRet = DestroyObject(hSmtp);
	TEST_EXPECT_VALUE_NOT_EQUAL(bRet, FALSE);

	TEST_END()
}


TEST_CALLBACK(__TestSendEmail)
{
	TEST_BEGIN("SendEmail");

	HANDLE hSmtp;
	BOOL bRet;


	hSmtp = CreateSmtpConnection(
		SMTP_PROTOCOL_SSL,
		"disroot.org",
		NULLPTR,
		NULLPTR,
		TRUE,
		FALSE
	);
	TEST_EXPECT_VALUE_NOT_EQUAL(hSmtp, NULL_OBJECT);

	bRet = SendEmail(
		hSmtp,
		"z.podbielniak@disroot.org",
		"z.podbielniak@disroot.org",
		NULLPTR,
		"Test From PodNet",
		"This is a test email from PodNet!"
	);
	TEST_EXPECT_VALUE_NOT_EQUAL(bRet, FALSE);

	bRet = DestroyObject(hSmtp);
	TEST_EXPECT_VALUE_NOT_EQUAL(bRet, FALSE);

	TEST_END()
}


TEST_START("CSmtp");
TEST_ADD(__TestCreateSmtpConnection);
TEST_ADD(__TestSendEmail);
TEST_RUN();
TEST_CONCLUDE();