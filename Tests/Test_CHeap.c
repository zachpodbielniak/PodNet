#include "../CMemory/CHeap.h"
#include "../CClock/CClock.h"


VOID CALL_TYPE(HOT) TimeMallocVsHeapAlloc(
	VOID
){
        HANDLE hHeap;
	LPVOID lpTest, lpTest2, lpTest3;	
	TIMESPEC tmSpec;
	UARCHLONG ualIndex;

	hHeap = CreateHeap(65536, 65536 * 10, NULL);

	lpTest = HeapAlloc(hHeap, 256, NULL);
	HighResolutionClock(&tmSpec);
	lpTest2 = HeapAlloc(hHeap, 256, NULL);
	PrintFormat("%llu\n", HighResolutionClockAndGetNanoSeconds(&tmSpec));
	lpTest3 = HeapAlloc(hHeap, 1024, NULL);


	
	for (ualIndex = 0; ualIndex < 1024; ualIndex++)
	{ lpTest = malloc(128); }
	
	HighResolutionClock(&tmSpec);
	lpTest = malloc(256);
	PrintFormat("%llu\n", HighResolutionClockAndGetNanoSeconds(&tmSpec));


	DestroyHandle(hHeap);
}


LONG Main(LONG lArgCount, DLPSTR dlpszArgValues)
{
        UNREFERENCED_PARAMETER(lArgCount);
        UNREFERENCED_PARAMETER(dlpszArgValues);
	TimeMallocVsHeapAlloc();
        return 0;
}
