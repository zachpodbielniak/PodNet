#include <PodNet/CNetworking/CIpv4.h>



LONG Main(LONG lArgCount, DLPSTR dlpszArgValues)
{
	UNREFERENCED_PARAMETER(lArgCount);
	UNREFERENCED_PARAMETER(dlpszArgValues);

	IPV4_ADDRESS ip4Address;
	CSTRING csBuffer[64];

	ZeroMemory(csBuffer, sizeof(csBuffer));
	ip4Address = GetCurrentPublicIpv4Address();
	ConvertIpv4AddressToString(ip4Address, csBuffer, sizeof(csBuffer));

	PrintFormat("%s\n", csBuffer);

	return 0;
}