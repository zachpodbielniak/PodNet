/*


 ____           _ _   _      _
|  _ \ ___   __| | \ | | ___| |_
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/

package PodNet

// #cgo LDFLAGS: -lpodnet
// #include <PodNet/PodNet.h>
// #include <PodNet/CAlgorithms/CRot13.h>
// #include <PodNet/CAlgorithms/CSha256.h>
// #include <PodNet/CAtom/CAtomTable.h>
// #include <PodNet/CClock/CClock.h>
// #include <PodNet/CClock/CTimer.h>
// #include <PodNet/CConcurrent/CAsync.h>
// #include <PodNet/CContainers/CContainers.h>
// #include <PodNet/CDevices/CGpio.h>
// #include <PodNet/CDevices/CI2C.h>
// #include <PodNet/CDevices/CSerial.h>
// #include <PodNet/CDevices/CSPI.h>
// #include <PodNet/CError/CError.h>
// #include <PodNet/CFile/CFile.h>
// #include <PodNet/CGps/CGps.h>
// #include <PodNet/CHandle/CHandle.h>
// #include <PodNet/CLock/CCritSec.h>
// #include <PodNet/CLock/CEvent.h>
// #include <PodNet/CLock/CMutex.h>
// #include <PodNet/CLock/CSemaphore.h>
// #include <PodNet/CLock/CSpinLock.h>
// #include <PodNet/CLog/CLog.h>
// #include <PodNet/CMath/CMath.h>
// #include <PodNet/CMemory/CGarbageCollector.h>
// #include <PodNet/CMemory/CHeap.h>
// #include <PodNet/CMemory/CMemory.h>
// #include <PodNet/CModule/CModule.h>
// #include <PodNet/CNetworking/CNetworking.h>
// #include <PodNet/CProcess/CProcess.h>
// #include <PodNet/CScripting/CLua.h>
// #include <PodNet/CString/CString.h>
// #include <PodNet/CSystem/CHardware.h>
// #include <PodNet/CSystem/CSystem.h>
// #include <PodNet/CUnit/CConvertUnit.h>
// #include <PodNet/CThread/CThread.h>
import "C"
