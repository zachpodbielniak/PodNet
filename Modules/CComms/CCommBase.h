#ifndef CCOMMBASE_H
#define CCOMMBASE_H
#include <PodNet/PodNet.h>
#include <PodNet/CModule/CModule.h>


/*
#define INHERITS_FROM_COMM_MODULE_DESCRIPTOR_TABLE(NAME)	NAME##_ON_SEND,			\
								NAME##_ON_RECIEVE,		\
								NAME##_CONNECT_TO,		\
								NAME##_ON_CONNECTED,		\
								NAME##_ON_NEW_CONNECTION,	\
								NAME##_GET_CONNECTION,



#define DEFINE_MODULE_COMM_DESCRIPTOR_TABLE(NAME,...)	typedef enum __MODULE_##NAME##_DESCRIPTOR		\
							{							\
								INHERITS_FROM_MODULE_DESCRIPTOR_TABLE(NAME)	\
								INHERITS_FROM_MODULE_DESCRIPTOR_TABLE(NAME)	\
								__VA_ARGS__,					\
								END_OF_MODULE_DESCRIPTOR_TABLE(NAME)		\
							} MODULE_##NAME##_DESCRIPTOR;				
*/


_Success_(return != NULLPTR, _Non_Locking_)
PODNET_MODULE
LPVOID
OnCreate(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
);



#endif