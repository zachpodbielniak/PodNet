#include <PodNet/PodNet.h>
#include <PodNet/CModule/CModule.h>


LONG Main(LONG lArgCount, DLPSTR dlpszArgValues)
{
	UNREFERENCED_PARAMETER(dlpszArgValues);

	LPHANDLE lphModules;
	ULONG ulIndex;
	CHAR szBuffer[512];

	lphModules = (LPHANDLE)LocalAllocAndZero(sizeof(HANDLE) * (lArgCount - 1));

	for (
		ulIndex = 1;
		ulIndex < (ULONG)lArgCount;
		ulIndex++
	){ 
		ZeroMemory(szBuffer, sizeof(szBuffer));
		StringConcatenate(szBuffer, (LPCSTR)"./");
		StringConcatenate(szBuffer, (LPCSTR)dlpszArgValues[ulIndex]);
		lphModules[ulIndex - 1U] = LoadModule(
			(LPCSTR)szBuffer,
			NULLPTR,
			NULLPTR,
			LOCK_TYPE_MUTEX,
			NULL,
			NULLPTR
		);
	}

	CallModuleProcByName(lphModules[0], "main", NULLPTR);


	for (
		ulIndex = 1;
		ulIndex < (ULONG)lArgCount;
		ulIndex++
	){ DestroyObject(lphModules[ulIndex - 1U]); }

	return 0;
}






