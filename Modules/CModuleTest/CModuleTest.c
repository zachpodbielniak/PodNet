#include <PodNet/PodNet.h>
#include <PodNet/CModule/CModule.h>



PODNET_API
LPVOID
OnCreate(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
){
	UNREFERENCED_PARAMETER(hModule);
	UNREFERENCED_PARAMETER(lpParam);
	PrintFormat("Module %d was loaded!\n", 1);
	return NULLPTR;
}


PODNET_API
LPVOID
SpecialFunction(
	_In_		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
){
	UNREFERENCED_PARAMETER(lpParam);
	PrintFormat("SpecialFunction() was called from module %p\n", hModule);
	return NULLPTR;
}


