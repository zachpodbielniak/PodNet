[DEFAULT REL]
[BITS 64]

global OnCreate
global AsmTest
global Ret3
global GetModule


extern printf

segment .data
lpszLoaded: 	db 	"Asm Module loaded!",0x0A,0x00
lpszFormat: 	db 	"AsmTest() was called from module %p",0x0A,0x00
lpszDestroy: 	db 	"Unloading and destroying the Asm Module.",0x0A,0x00


segment .text



OnCreate:
	sub rsp, 0x18
	lea rdi, [lpszLoaded]
	xor rcx, rcx
	xor rdx, rdx
	xor rsi, rsi
	call printf wrt ..plt
	add rsp, 0x18
	ret


OnDestroy:
	sub rsp, 0x18
	lea rdi, [lpszDestroy]
	xor rcx, rcx
	xor rdx, rdx
	xor rsi, rsi
	call printf wrt ..plt
	add rsp, 0x18
	ret	


AsmTest:
	sub rsp, 0x18
	mov rsi, rdi
	lea rdi, [lpszFormat]
	call printf wrt ..plt
	add rsp, 0x18
	ret


Ret3:
	mov rax, 3
	ret


GetModule:
	mov rax, rdi
	ret