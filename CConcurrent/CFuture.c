/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CFuture.h"

typedef struct __FUTURE_OBJ
{
        INHERITS_FROM_HANDLE();
        HANDLE                  hPromise;
        DLPVOID                 dlpSharedData;
        LPUARCHLONG             lpualSharedDataSize;
        BOOL                    bReadyToBeRetrieved;
} FUTURE_OBJ, *LPFUTURE_OBJ;




INTERNAL_OPERATION
BOOL
__DestroyFuture(
        _In_                    HDERIVATIVE                     hDerivative,
        _In_Opt_                ULONG                           ulFlags
){
        UNREFERENCED_PARAMETER(ulFlags);
        EXIT_IF_UNLIKELY_NULL(hDerivative, FALSE);
        LPFUTURE_OBJ lpfoFuture;
        lpfoFuture = (LPFUTURE_OBJ)hDerivative;
        EXIT_IF_NULL(lpfoFuture, FALSE);        
        FreeMemory(lpfoFuture);
        return TRUE;
}




INTERNAL_OPERATION
BOOL
__WaitForFutureToBeAvailable(
        _In_                    HANDLE                          hBase,
        _In_Opt_                ULONGLONG                       ullMilliseconds
){
        EXIT_IF_UNLIKELY_NULL(hBase, FALSE);
        EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hBase), HANDLE_TYPE_FUTURE, FALSE);

        LPFUTURE_OBJ lpfoFuture;
        BOOL bRet;
        lpfoFuture = (LPFUTURE_OBJ)GetHandleDerivative(hBase);
        EXIT_IF_NULL(lpfoFuture, FALSE);

        bRet = WaitForSingleObject(lpfoFuture->hPromise, ullMilliseconds);  
        if (TRUE == bRet)      
        { lpfoFuture->bReadyToBeRetrieved = TRUE; }

        return bRet;        
}




/* This should only be called by a newly initialized Promise. */

_Result_Null_On_Failure_
_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HANDLE
CreateFuture(
        _In_                    HANDLE                          hPromise,
        _In_                    DLPVOID                         dlpSharedData,
        _In_                    LPUARCHLONG                     lpualSharedDataSize,
        _In_Opt_                ULONG                           ulFlags
){
        UNREFERENCED_PARAMETER(ulFlags);
        EXIT_IF_UNLIKELY_NULL(hPromise, 0);
        EXIT_IF_UNLIKELY_NULL(dlpSharedData, 0);
        EXIT_IF_UNLIKELY_NULL(lpualSharedDataSize, 0);
        HANDLE hFuture;
        LPFUTURE_OBJ lpfoFuture;
        ULONGLONG ullID;
        lpfoFuture = LocalAlloc(sizeof(FUTURE_OBJ));
        EXIT_IF_NULL(lpfoFuture, 0);
        ZeroMemory(lpfoFuture, sizeof(FUTURE_OBJ));

        lpfoFuture->hPromise = hPromise;
        lpfoFuture->dlpSharedData = dlpSharedData;
        lpfoFuture->lpualSharedDataSize = lpualSharedDataSize;
        lpfoFuture->bReadyToBeRetrieved = FALSE;

        hFuture = CreateHandleWithSingleInheritor(
                lpfoFuture,
                &(lpfoFuture->hThis),
                HANDLE_TYPE_FUTURE,
                __DestroyFuture,
                0,
                NULLPTR,
                0,
                NULLPTR,
                0,
                &ullID,
                0
        );

        SetHandleEventWaitFunction(hFuture, __WaitForFutureToBeAvailable, 0);

        EXIT_IF_NULL(hFuture, 0);
        return hFuture;
}




_Success_(return > 0, _Acquires_Lock_(hPromise->hEvent, _Has_Lock_Kind_(_Lock_Kind_Event_)))
PODNET_API
UARCHLONG
RetrieveFutureValue(
        _In_                                    HANDLE                          hFuture,
        _Out_Writes_Bytes_(sizeof(return))      LPVOID                          lpResult,
        _In_                                    UARCHLONG                       ualSizeOfResultBuffer
){
        EXIT_IF_UNLIKELY_NULL(hFuture, 0);
        EXIT_IF_UNLIKELY_NULL(lpResult, 0);
        EXIT_IF_UNLIKELY_NULL(ualSizeOfResultBuffer, 0);
        EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hFuture), HANDLE_TYPE_FUTURE, 0);

        LPFUTURE_OBJ lpfoFuture;
        UARCHLONG ualSize;
        lpfoFuture = GetHandleDerivative(hFuture);

        SET_ERROR_AND_EXIT_IF_NOT_VALUE(lpfoFuture->bReadyToBeRetrieved, TRUE, ERROR_FUTURE_NOT_READY, FALSE);

        ualSize = *(lpfoFuture->lpualSharedDataSize);
        
        /* Check to see if we have enough room to copy, if not, set the max we can. */
        if (ualSize > ualSizeOfResultBuffer) ualSize = ualSizeOfResultBuffer; 

        /* BROKEN PROMISE! */
        SET_ERROR_AND_EXIT_IF_NULL(*(lpfoFuture->dlpSharedData), ERROR_BROKEN_PROMISE, FALSE);
        
        CopyMemory(lpResult, *(lpfoFuture->dlpSharedData), ualSize);
        return *(lpfoFuture->lpualSharedDataSize);
}




_Success_(return > 0, _Acquires_Lock_(hPromise->hEvent, _Has_Lock_Kind_(_Lock_Kind_Event_)))
PODNET_API
UARCHLONG
RetrieveFutureValueByAlloc(
        _In_                                    HANDLE                          hFuture,
        _Out_Writes_To_Ptr_(*dlpResult)         DLPVOID                         dlpResult
){
        EXIT_IF_UNLIKELY_NULL(hFuture, 0);
        EXIT_IF_UNLIKELY_NULL(dlpResult, 0);
        EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hFuture), HANDLE_TYPE_FUTURE, 0);

        LPFUTURE_OBJ lpfoFuture;
        UARCHLONG ualSize;
        lpfoFuture = (LPFUTURE_OBJ)GetHandleDerivative(hFuture);

        SET_ERROR_AND_EXIT_IF_NOT_VALUE(lpfoFuture->bReadyToBeRetrieved, TRUE, ERROR_FUTURE_NOT_READY, FALSE);

        /* BROKEN PROMISE! */
        SET_ERROR_AND_EXIT_IF_NULL(*(lpfoFuture->dlpSharedData), ERROR_BROKEN_PROMISE, FALSE);
        ualSize = *(lpfoFuture->lpualSharedDataSize);

        *dlpResult = LocalAllocAndZero(ualSize);
        EXIT_IF_NULL(*dlpResult, 0);
        CopyMemory(*dlpResult, *(lpfoFuture->dlpSharedData), ualSize);
        return ualSize;        
}