/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "../Prereqs.h"
#include "../Macros.h"
#include "../TypeDefs.h"
#include "../Annotation.h"
#include "../Defs.h"
#include "../CHandle/CHandle.h"

typedef LPVOID (* LPFN_ONCE_CALLABLE)(
        _In_            LPVOID          lpParam
);


_Result_Null_On_Failure_
_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HANDLE
CreateOnceCallableObject(
        _In_            LPFN_ONCE_CALLABLE              lpfnOnceCallable
);



_Success_(return != FALSE, _Acquires_Shared_Lock_(hCallable->hEvent, _Has_Lock_Kind_(_Lock_Kind_Event_)))
PODNET_API
BOOL
CallExactlyOnce(
        _In_            HANDLE                          hOnceCallable,
        _In_Opt_        LPVOID                          lpParam  
);