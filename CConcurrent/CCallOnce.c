/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CCallOnce.h"


typedef struct __CALL_ONCE_OBJ
{
        INHERITS_FROM_HANDLE();
        LPFN_ONCE_CALLABLE              lpfnCallable;
        HANDLE                          hEvent;
} CALL_ONCE_OBJ, *LPCALL_ONCE_OBJ;


_Success_(return != FALSE, {})
INTERNAL_OPERATION
BOOL
__DestroyOnceCallable(
        _In_            HDERIVATIVE                     hDerivative,
        _In_Opt_        ULONG                           ulFlags
){
        EXIT_IF_LIKELY_NULL(hDerivative, FALSE);
        UNREFERENCED_PARAMETER(ulFlags);
        LPCALL_ONCE_OBJ lpCall;
        lpCall = (LPCALL_ONCE_OBJ)hDerivative;
        FreeMemory(lpCall);
        return TRUE;
}




_Result_Null_On_Failure_
_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HANDLE
CreateOnceCallableObject(
        _In_            LPFN_ONCE_CALLABLE              lpfnOnceCallable
){
        EXIT_IF_UNLIKELY_NULL(lpfnOnceCallable, NULLPTR);
        HANDLE hCallable;
        LPCALL_ONCE_OBJ lpOnce;
        ULONGLONG ullID;
        lpOnce = LocalAlloc(sizeof(CALL_ONCE_OBJ));
        EXIT_IF_NULL(lpOnce, NULLPTR);
        lpOnce->lpfnCallable = lpfnOnceCallable;

        hCallable = CreateHandleWithSingleInheritorAndSingleSettableEvent(
                        lpOnce,
                        &(lpOnce->hThis),
                        HANDLE_TYPE_ONCE_CALLABLE,
                        NULLPTR,
                        __DestroyOnceCallable,
                        NULL,
                        NULLPTR,
                        NULL,
                        NULLPTR,
                        NULL,
                        &ullID,
                        NULL
        );
        EXIT_IF_NULL(hCallable, NULLPTR);
        lpOnce->hEvent = GetObjectEvent(hCallable);
        return hCallable;        
}



_Success_(return != FALSE, _Acquires_Shared_Lock_(hCallable->hEvent, _Has_Lock_Kind_(_Lock_Kind_Event_)))
PODNET_API
BOOL
CallExactlyOnce(
        _In_            HANDLE                          hOnceCallable,
        _In_Opt_        LPVOID                          lpParam  
){
        EXIT_IF_UNLIKELY_NULL(hOnceCallable, FALSE);
        EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hOnceCallable), HANDLE_TYPE_ONCE_CALLABLE, FALSE);
        LPCALL_ONCE_OBJ lpCall;
        BOOL bRet;
        lpCall = (LPCALL_ONCE_OBJ)GetHandleDerivative(hOnceCallable);

        bRet = SignalEvent(lpCall->hEvent); 
        if (bRet == TRUE)
                lpCall->lpfnCallable(lpParam);
        return bRet;
}