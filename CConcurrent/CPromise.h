/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CPROMISE_H
#define CPROMISE_H


#include "../Prereqs.h"
#include "../Macros.h"
#include "../TypeDefs.h"
#include "../Annotation.h"
#include "../Defs.h"
#include "../CHandle/CHandle.h"
#include "CFuture.h"
#include "../CThread/CThread.h"




/* 
        Must allocate dlpDataOut after you get the solution to the Promise.
        Data size to allocated dlpDataOut is equal to value returned. 
*/
typedef UARCHLONG (* LPFN_PROMISE_PROC)(
        _In_    LPVOID          lpParam,
        _Out_   DLPVOID         dlpDataOut
);

typedef LPFN_PROMISE_PROC               *DLPFN_PROMISE_PROC;




_Result_Null_On_Failure_
_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HANDLE
CreatePromise(
        _In_                            LPFN_PROMISE_PROC               lpfnProc,
        _In_                            LPVOID                          lpParam,
        _Reserved_Must_Be_Null_         LPVOID                          lpReserved,
        _Out_Opt_                       LPHANDLE                        lphFuture,
        _In_Opt_                        ULONG                           ulFlags
);




_Result_Null_On_Failure_
_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HANDLE
GetFuture(
        _In_                            HANDLE                          hPromise
);



#endif