/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CFUTURE_H
#define CFUTURE_H

/*
#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
*/
#include "../Macros.h"
#include "../CHandle/CHandle.h"
#include "../CError/CError.h"

/* #include "../CThread/CThread.h" */



/* This should only be called by a newly initialized Promise. */

_Result_Null_On_Failure_
_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HANDLE
CreateFuture(
        _In_                    HANDLE                          hPromise,
        _In_                    DLPVOID                         dlpSharedData,
        _In_                    LPUARCHLONG                     lpualSharedDataSize,
        _In_Opt_                ULONG                           ulFlags
);




_Success_(return > 0, _Acquires_Lock_(hPromise->hEvent, _Has_Lock_Kind_(_Lock_Kind_Event_)))
PODNET_API
UARCHLONG
RetrieveFutureValue(
        _In_                                    HANDLE                          hFuture,
        _Out_Writes_Bytes_(sizeof(return))      LPVOID                          lpResult,
        _In_                                    UARCHLONG                       ualSizeOfResultBuffer
);




_Success_(return > 0, _Acquires_Lock_(hPromise->hEvent, _Has_Lock_Kind_(_Lock_Kind_Event_)))
PODNET_API
UARCHLONG
RetrieveFutureValueByAlloc(
        _In_                                    HANDLE                          hFuture,
        _Out_Writes_To_Ptr_(*dlpResult)         DLPVOID                         dlpResult
);


#endif