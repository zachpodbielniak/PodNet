/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CPromise.h"

typedef struct __PROMISE_OBJ
{
        INHERITS_FROM_HANDLE();
        HANDLE                  hFuture;
        HANDLE                  hThread;
        LPFN_PROMISE_PROC       lpfnPromiseProc;
        LPVOID                  lpParam;
        LPVOID                  lpData;
        UARCHLONG               ualDataSize;
} PROMISE_OBJ, *LPPROMISE_OBJ;




_Call_Back_
_Success_(_Inexpressible_, _Non_Locking_)
INTERNAL_OPERATION
LPVOID
__PromiseProc(
        _In_                            LPVOID                  lpParam
){
        LPPROMISE_OBJ lppoProm;
        lppoProm = (LPPROMISE_OBJ)lpParam;
        lppoProm->ualDataSize = lppoProm->lpfnPromiseProc(lppoProm->lpParam, &(lppoProm->lpData));
        EXIT_IF_NULL(lppoProm->ualDataSize, 0);

        /* Signal the event; stating we are done, have the result, and it is ready. */
        TriggerSingleObject(lppoProm->hThis, NULLPTR, 0);               
        return lppoProm->lpData;
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__DestroyPromise(
        _In_                            HDERIVATIVE             hDerivative,
        _In_Opt_                        ULONG                   ulFlags
){
        UNREFERENCED_PARAMETER(ulFlags);
        LPPROMISE_OBJ lppoProm;
        BOOL bRet;
        lppoProm = (LPPROMISE_OBJ)GetHandleDerivative(hDerivative);
        EXIT_IF_UNLIKELY_NULL(lppoProm, FALSE);
        bRet = TRUE;
        if (NULLPTR != lppoProm->hThread)
        { bRet &= DestroyHandle(lppoProm->hThread); }
        FreeMemory(lppoProm);
        return bRet;
}




_Result_Null_On_Failure_
_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HANDLE
CreatePromise(
        _In_                            LPFN_PROMISE_PROC               lpfnProc,
        _In_                            LPVOID                          lpParam,
        _Reserved_Must_Be_Null_         LPVOID                          lpReserved,
        _Out_Opt_                       LPHANDLE                        lphFuture,
        _In_Opt_                        ULONG                           ulFlags
){
        UNREFERENCED_PARAMETER(ulFlags);
        HANDLE hPromise, hFuture;
        LPPROMISE_OBJ lppoProm;
        ULONGLONG ullID;
        EXIT_IF_UNLIKELY_NULL(lpfnProc, 0);
        EXIT_IF_UNLIKELY_NULL(lpParam, 0);
        UNREFERENCED_PARAMETER(lpReserved);

        lppoProm = LocalAlloc(sizeof(PROMISE_OBJ));
        EXIT_IF_NULL(lppoProm, 0);
        ZeroMemory(lppoProm, sizeof(PROMISE_OBJ));

        lppoProm->lpfnPromiseProc = lpfnProc;
        lppoProm->lpParam = lpParam;

        hPromise = CreateHandleWithSingleInheritorAndSingleSettableEvent(
                lppoProm,
                &(lppoProm->hThis),
                HANDLE_TYPE_PROMISE,
                NULLPTR,
                __DestroyPromise,
                0,
                NULLPTR,
                0,
                NULLPTR,
                0,
                &ullID,
                0
        );

        EXIT_IF_NULL(hPromise, 0);

        hFuture = CreateFuture(hPromise, &(lppoProm->lpData), &(lppoProm->ualDataSize), 0);
        EXIT_IF_NULL(hFuture, 0);
        lppoProm->hFuture = hFuture;

        if (NULLPTR != lphFuture)
        { *lphFuture = hFuture; }

        lppoProm->hThread = CreateThread(__PromiseProc, lppoProm, NULLPTR);
        EXIT_IF_NULL(lppoProm->hThread, 0);

        return hPromise;
}




_Result_Null_On_Failure_
_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HANDLE
GetFuture(
        _In_                            HANDLE                          hPromise
){
        EXIT_IF_UNLIKELY_NULL(hPromise, 0);
        LPPROMISE_OBJ lppoProm;
        EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hPromise), HANDLE_TYPE_PROMISE, 0);
        lppoProm = (LPPROMISE_OBJ)GetHandleDerivative(hPromise);
        EXIT_IF_NULL(lppoProm, 0);

        return lppoProm->hFuture;
}