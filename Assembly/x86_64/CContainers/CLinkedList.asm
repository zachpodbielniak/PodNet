; 
; 
; 
;  ____           _ _   _      _   
; |  _ \ ___   __| | \ | | ___| |_ 
; | |_) / _ \ / _` |  \| |/ _ \ __|
; |  __/ (_) | (_| | |\  |  __/ |_ 
; |_|   \___/ \__,_|_| \_|\___|\__|
; 
; 
; General Purpose C Library.
; Copyright (C) 2017-2019 Zach Podbielniak
; 
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU Affero General Public License as
; published by the Free Software Foundation, either version 3 of the
; License, or (at your option) any later version.
; 
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU Affero General Public License for more details.
; 
; You should have received a copy of the GNU Affero General Public License
; along with this program.  If not, see <https://www.gnu.org/licenses/>.
; 
;
; Parameters are passed in RDI, RSI, RDX, RCX, R8, R9, Stack : in that order.




extern CreateLinkedList
extern DestroyLinkedList
extern DestroyLinkedListEx
extern LinkedListFront
extern LinkedListBack
extern LinkedListTraverse
extern LinkedListAtNode
extern LinkedListAttachFront
extern LinkedListDetachBack
extern LinkedListAttachBack
extern LinkedListDetachBack
extern LinkedListInsert
extern LinkedListRemove
extern LinkedListRemoveAtNode
extern LinkedListRemoveItemWithSpecificData
extern LinkedListErase
extern LinkedListSwap
extern LinkedListCopy
extern LinkedListIsEmpty
extern LinkedListSize
extern LinkedListSplice
extern LinkedListIsUnique
extern LinkedListMerge
extern LinkedListReverse
