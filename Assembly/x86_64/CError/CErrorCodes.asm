; 
; 
; 
;  ____           _ _   _      _   
; |  _ \ ___   __| | \ | | ___| |_ 
; | |_) / _ \ / _` |  \| |/ _ \ __|
; |  __/ (_) | (_| | |\  |  __/ |_ 
; |_|   \___/ \__,_|_| \_|\___|\__|
; 
; 
; General Purpose C Library.
; Copyright (C) 2017-2019 Zach Podbielniak
; 
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU Affero General Public License as
; published by the Free Software Foundation, either version 3 of the
; License, or (at your option) any later version.
; 
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU Affero General Public License for more details.
; 
; You should have received a copy of the GNU Affero General Public License
; along with this program.  If not, see <https://www.gnu.org/licenses/>.
; 
;
; Parameters are passed in RDI, RSI, RDX, RCX, R8, R9, Stack : in that order.





; Generic Errors 
ERROR_HANDLE_SUPPLIED_WAS_NULL                                          equ 0x0001
ERROR_REQUIRED_IN_PARAMETER_WAS_NULL                                    equ 0x0002
ERROR_REQUIRED_OUT_PARAMETER_WAS_NULL                                   equ 0x0003
ERROR_HANDLE_DERIVATIVE_WAS_NULL                                        equ 0x0004


; Gpio Pin Errors
ERROR_GPIO_HANDLE_SUPPLIED_WAS_NULL                                     equ 0x0040
ERROR_GPIO_DATA_COULD_NOT_BE_ALLOC                                      equ 0x0041
ERROR_GPIO_DATA_WAS_NULL                                                equ 0x0042
ERROR_GPIO_REQUIRED_ARGUMENT_WAS_NULL                                   equ 0x0043
ERROR_GPIO_COULD_NOT_CREATE_SPIN_LOCK                                   equ 0x0044


; Atom Table Errors
ERROR_ATOM_TABLE_HANDLE_SUPPLIED_WAS_NULL                               equ 0x0050
ERROR_ATOM_TABLE_COULD_NOT_CREATE_HANDLE                                equ 0x0051
ERROR_ATOM_TABLE_COULD_ALLOC_ATOM_BUFFER                                equ 0x0052
ERROR_ATOM_TABLE_ATOM_BUFFER_WAS_NULL                                   equ 0x0053
ERROR_ATOM_NON_EXISTENT                                                 equ 0x0054
ERROR_ATOM_TABLE_COULD_NOT_BE_ALLOC                                     equ 0x0055
ERROR_ATOM_TABLE_COULD_NOT_ALLOC_TABLE                                  equ 0x0056
ERROR_ATOM_TABLE_NO_STRING_PROVIDED                                     equ 0x0057
ERROR_ATOM_TABLE_STRING_PROVIDED_WAS_LARGER_THAN_ATOM_BUFFER            equ 0x0058
ERROR_ATOM_TABLE_HANDLE_SUPPLIED_WAS_NOT_ATOM_TABLE_HANDLE              equ 0x0059
ERROR_ATOM_TABLE_NON_EXISTENT                                           equ 0x005A
ERROR_ATOM_TABLE_FULL                                                   equ 0x005B

; Clock Errors
ERROR_CLOCK_NOT_DEFINED                                                 equ 0x0070


; Mutex Errors
ERROR_MUTEX_HANDLE_SUPPLIED_WAS_NULL                                    equ 0x0170
ERROR_MUTEX_HANDLE_SUPPLIED_WAS_NOT_MUTEX_HANDLE                        equ 0x0171
ERROR_MUTEX_DATA_IS_NON_EXISTENT                                        equ 0x0172
ERROR_MUTEX_COULD_NOT_ALLOC_DATA                                        equ 0x0173
ERROR_MUTEX_COULD_NOT_CREATE_HANDLE                                     equ 0x0174

; Event Errors
ERROR_EVENT_HANDLE_SUPPLIED_WAS_NULL                                    equ 0x0180
ERROR_EVENT_HANDLE_SUPPLIED_WAS_NOT_EVENT_HANDLE                        equ 0x0181
ERROR_EVENT_DATA_IS_NON_EXISTENT                                        equ 0x0182
ERROR_EVENT_COULD_NOT_ALLOC_DATA                                        equ 0x0183
ERROR_EVENT_COULD_NOT_CREATE_HANDLE                                     equ 0x0184


; SpinLock Errors
ERROR_SPINLOCK_HANDLE_SUPPLIED_WAS_NULL                                 equ 0x0190
ERROR_SPINLOCK_HANDLE_SUPPLIED_IS_NOT_SPINLOCK_HANDLE_TYPE              equ 0x0191
ERROR_SPINLOCK_DATA_IS_NON_EXISTENT                                     equ 0x0192
ERROR_SPINLOCK_COULD_NOT_ALLOC_DATA                                     equ 0x0193
ERROR_SPINLOCK_COULD_NOT_CREATE_HANDLE                                  equ 0x0194


; Log Errors
ERROR_LOG_COULD_NOT_OPEN_FILE                                           equ 0x0220


; File Errors
ERROR_FILE_HANDLE_COULD_NOT_BE_OPENED                                   equ 0x0250
ERROR_FILE_NOT_WRITABLE                                                 equ 0x0251
ERROR_FILE_DOES_NOT_EXIST                                               equ 0x0252
ERROR_FILE_COULD_NOT_ALLOCATE_OBJECT_FOR_STANDARD_STREAM                equ 0x0253
ERROR_FILE_COULD_NOT_BE_CREATED                                         equ 0x0254
ERROR_FILE_COULD_NOT_BE_OPENED                                          equ 0x0255
ERROR_FILE_HANDLE_COULD_NOT_BE_CREATED                                  equ 0x0256
ERROR_FILE_CRITICAL_SECTION_COULD_NOT_BE_CREATED                        equ 0x0257
ERROR_FILE_CRITICAL_SECTION_DOES_NOT_EXIST                              equ 0x0258
ERROR_FILE_CRITICAL_SECTION_COULD_NOT_BE_ENTERED                        equ 0x0259
ERROR_FILE_CRITICAL_SECTION_COULD_NOT_BE_EXITED                         equ 0x025A
ERROR_FILE_HANDLE_SUPPLIED_IS_NOT_FILE_HANDLE_TYPE                      equ 0x025B
ERROR_FILE_NOT_OPENED_WITH_READ_FLAG                                    equ 0x025C
ERROR_FILE_NOT_OPENED_WITH_WRITE_FLAG                                   equ 0x025D
ERROR_FILE_NAME_NOT_DEFINED                                             equ 0x025E
ERROR_FILE_IS_NOT_LEGIT_FILE                                            equ 0x025F
ERROR_FILE_PROMISE_DATA_COULD_NOT_BE_ALLOC                              equ 0x0260
ERROR_FILE_NO_BUFFER_SIZE_WAS_SUPPLIED                                  equ 0x0261


; Event Errors
ERROR_EVENT_COULD_NOT_BE_CREATED                                        equ 0x0270
ERROR_EVENT_HANDLE_SUPPLIED_IS_NOT_EVENT_HANDLE_TYPE                    equ 0x0271
ERROR_EVENT_HANDLE_COULD_NOT_BE_CREATED                                 equ 0x0272


; Promise and Future Errors
ERROR_BROKEN_PROMISE                                                    equ 0x0300
ERROR_FUTURE_NOT_READY                                                  equ 0x0301
ERROR_FUTURE_OUTPUT_BUFFER_NOT_ALLOC                                    equ 0x0302
ERROR_PROMISE_NOT_SUPPLIED_WITH_DATA                                    equ 0x0303
ERROR_PROMISE_NOT_SUPPLIED_WITH_ALLOCABLE_BUFFER                        equ 0x0304
ERROR_PROMISE_COULD_NOT_ALLOC_BUFFER                                    equ 0x0305
ERROR_PROMISE_COULD_NOT_BE_CREATED                                      equ 0x0306
