; 
; 
; 
;  ____           _ _   _      _   
; |  _ \ ___   __| | \ | | ___| |_ 
; | |_) / _ \ / _` |  \| |/ _ \ __|
; |  __/ (_) | (_| | |\  |  __/ |_ 
; |_|   \___/ \__,_|_| \_|\___|\__|
; 
; 
; General Purpose C Library.
; Copyright (C) 2017-2019 Zach Podbielniak
; 
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU Affero General Public License as
; published by the Free Software Foundation, either version 3 of the
; License, or (at your option) any later version.
; 
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU Affero General Public License for more details.
; 
; You should have received a copy of the GNU Affero General Public License
; along with this program.  If not, see <https://www.gnu.org/licenses/>.
; 
;
; Parameters are passed in RDI, RSI, RDX, RCX, R8, R9, Stack : in that order.




SHAPE_UNDEFINED			equ 0x00
SHAPE_NOT_A_SHAPE		equ 0x01
SHAPE_CIRCLE			equ 0x02
SHAPE_RECTANGLE			equ 0x03
SHAPE_TRIANGLE			equ 0x04
SHAPE_ELLIPSE			equ 0x05
SHAPE_PARALLELOGRAM		equ 0x06
SHAPE_TRAPEZOID			equ 0x07
SHAPE_TRAPEZIUM			equ 0x08
SHAPE_ANNULUS			equ 0x09
SHAPE_POLYGON			equ 0x0A
SHAPE_CIRCLULAR_SECTOR		equ 0x0B
SHAPE_ANNULAR_SECTOR		equ 0x0C
SHAPE_FILLET			equ 0x0D 
SHAPE_OTHER 			equ 0xFF


STRUC SHAPE 
	.lpChild:		RESQ	1
	.ulShapeType:		RESD 	1
	.corCoordinate:		RESQ 	4
	.dbRotation:		RESQ 	1
	.Size:
ENDSTRUC


STRUC CIRCLE
	.lpshpParent:		RESQ 	1
	.dbRadius:		RESQ 	1
	.Size:
ENDSTRUC 


STRUC RECTANGLE
	.lpshpParent: 		RESQ 	1
	.dbBase:		RESQ 	1
	.dbHeight: 		RESQ 	1
ENDSTRUC 


EQUILATERAL_TRIANGLE		equ 0x01
RIGHT_TRIANGLE			equ 0x02 
OBLIQUE_TRIANGLE		equ 0x03


STRUC TRIANGLE
	.lpshpParent:		RESQ 	1
	.dbBase:		RESQ 	1
	.dbHeight: 		RESQ 	1
	.dbTheta: 		RESQ 	1
	.ulType:		RESD 	1
	.Size:
ENDSTRUC 


STRUC ELLIPSE
	.lpshpParent: 		RESQ 	1
	.dbR1:			RESQ 	1
	.dbR2: 			RESQ 	1
	.Size:
ENDSTRUC 


STRUC PARALLELOGRAM
	.lpshpParent: 		RESQ 	1
	.dbBase:		RESQ 	1
	.dbHeight: 		RESQ 	1
	.dbTheta: 		RESQ 	1
	.Size:
ENDSTRUC 


STRUC TRAPEZOID
	.lpshpParent: 		RESQ 	1
	.dbBase: 		RESQ 	1
	.dbHead: 		RESQ 	1
	.dbHeight: 		RESQ 	1
	.dbTheta: 		RESQ 	1
	.Size:
ENDSTRUC 


STRUC TRAPEZIUM 
	.lpshpParent 		RESQ 	1
	.dbBase: 		RESQ 	1
	.dbHead: 		RESQ 	1
	.dbHeightLeft: 		RESQ 	1
	.dbHeightRight: 	RESQ 	1
	.dbTheta: 		RESQ 	1
ENDSTRUC 	


STRUC ANNULUS 
	.lpshpParent: 		RESQ 	1
	.dbInnerRadius:		RESQ 	1
	.dbOuterRadius: 	RESQ 	1
	.Size:
ENDSTRUC 


STRUC POLYGON 
	.lpshpParent: 		RESQ 	1
	.dbSideLenght: 		RESQ 	1
	.ulNumberOfSides: 	RESD 	1
	.dbRadius: 		RESQ 	1
	.Size:
ENDSTRUC 


STRUC CIRCULAR_SECTOR
	.lpshpParent:		RESQ 	1
	.dbRadius: 		RESQ 	1
	.dbTheta: 		RESQ 	1
	.Size:
ENDSTRUC 


STRUC ANNULAR_SECTOR
	.lpshpParent: 		RESQ 	1
	.dbInnerRadius: 	RESQ 	1
	.dbOuterRadius: 	RESQ 	1
	.dbTheta: 		RESQ 	1
	.Size:  
ENDSTRUC 


STRUC FILLET
	.lpshpParent: 		RESQ 	1
	.dbRadius: 		RESQ 	1
	.Size:
ENDSTRUC 


extern CreateShape
extern CreateShapeEx
extern DestroyShape
extern Area
extern Perimeter
extern FastArea
extern FastPerimeter
