; 
; 
; 
;  ____           _ _   _      _   
; |  _ \ ___   __| | \ | | ___| |_ 
; | |_) / _ \ / _` |  \| |/ _ \ __|
; |  __/ (_) | (_| | |\  |  __/ |_ 
; |_|   \___/ \__,_|_| \_|\___|\__|
; 
; 
; General Purpose C Library.
; Copyright (C) 2017-2019 Zach Podbielniak
; 
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU Affero General Public License as
; published by the Free Software Foundation, either version 3 of the
; License, or (at your option) any later version.
; 
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU Affero General Public License for more details.
; 
; You should have received a copy of the GNU Affero General Public License
; along with this program.  If not, see <https://www.gnu.org/licenses/>.
; 
;
; Parameters are passed in RDI, RSI, RDX, RCX, R8, R9, Stack : in that order.



; Handle Types
HANDLE_TYPE_EVENT		equ 0x100
HANDLE_TYPE_CRIT_SEC		equ 0x101
HANDLE_TYPE_SEMAPHORE		equ 0x102
HANDLE_TYPE_MUTEX		equ 0x103
HANDLE_TYPE_SPINLOCK		equ 0x104
HANDLE_TYPE_THREAD		equ 0x105
HANDLE_TYPE_PROMISE		equ 0x108
HANDLE_TYPE_FUTURE		equ 0x109
HANDLE_TYPE_FILE		equ 0x110
HANDLE_TYPE_LOG			equ 0x111
HANDLE_TYPE_ATOM_TABLE		equ 0x112
HANDLE_TYPE_HANDLE_TABLE	equ 0x113
HANDLE_TYPE_ONCE_CALLABLE	equ 0x120
HANDLE_TYPE_SOCKET		equ 0x130
HANDLE_TYPE_WEBSERVER		equ 0x133
HANDLE_TYPE_LUA			equ 0x140
HANDLE_TYPE_DEVICE_GPIO		equ 0x150

extern CreateHandleWithSingleInheritor
extern CreateHandleWithCriticalSectionControllingSingleInheritor
extern CreateHandleWithSingleInheritorAndSingleSettableEvent
extern CreateHandleWithSingleCriticalSectionControllingSingleInheritorWithMultipleSettableEventsAndSingleUserDefinableProc
extern CreateHandleWithMultipleInheritors
extern CreateHandleWithSingleCriticalSectionControllingMultipleInheritors
extern CreateHandleWithSingleCriticalSectionControllingMultipleInheritorsWithSingleSettableEvent
extern CreateHandleWithSingleCriticalSectionControllingMultipleInheritorsWithMultipleSettableEvents
extern CreateHandleWithSingleCriticalSectionControllingMultipleInheritorsWithMultipleSettableEventsAndMultipleUserDefinableProcs
extern CreateHandleWithSingleCriticalSectionControllingMultipleInheritorsWithMultipleSettableEventsAndMultipleUserDefinableProcsAndData
extern DestroyHandle
extern GetHandleDerivative
extern GetHandleDerivativeAtIndex
extern SetHandleDestroyFunction
extern SetHandleDestroyFunctionAtIndex
extern GetHandleDestroyFunction
extern GetHandleDestroyFunctionAtIndex
extern GetObjectEvent
extern SetHandleEventWaitFunction
extern SetHandleReleaseFunction
extern SetHandlLockFunctions
extern GetHandleType
extern GetHandleTypeAtIndex
extern HandleHasMultipleInheritors
extern GetNumberOfHandleDerivatives
extern ErrorOutHandle
extern RaiseHandleAlarm
extern LockHandle
extern TryLockHandle
extern UnlockHandle
extern GetHandleDerivativeType
extern GetHandleInformation
extern WaitForSingleObject
extern WaitForMultipleObjects
extern ReleaseSingleObject
extern ReleaseMultipleObjects
extern TriggerSingleObject
extern ResetObject
extern ProcHandle
extern SetHandleReadAndWriteProc
extern Write
extern Read

