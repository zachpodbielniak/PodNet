; 
; 
; 
;  ____           _ _   _      _   
; |  _ \ ___   __| | \ | | ___| |_ 
; | |_) / _ \ / _` |  \| |/ _ \ __|
; |  __/ (_) | (_| | |\  |  __/ |_ 
; |_|   \___/ \__,_|_| \_|\___|\__|
; 
; 
; General Purpose C Library.
; Copyright (C) 2017-2019 Zach Podbielniak
; 
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU Affero General Public License as
; published by the Free Software Foundation, either version 3 of the
; License, or (at your option) any later version.
; 
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU Affero General Public License for more details.
; 
; You should have received a copy of the GNU Affero General Public License
; along with this program.  If not, see <https://www.gnu.org/licenses/>.
; 
;
; Parameters are passed in RDI, RSI, RDX, RCX, R8, R9, Stack : in that order.




LOG_DEBUG		equ 1
LOG_INFO		equ 2
LOG_WARNING		equ 3
LOG_ERROR		equ 4
LOG_FATAL		equ 5


STRUC PRIORITY_LOG_DESCRIPTOR
	.lphDebug: 		RESQ	1
	.ulNumDebugFiles: 	RESD	1
	.lphInfo:		RESQ 	1
	.ulNumInfoFile:		RESD 	1
	.lphWarning:		RESQ 	1
	.ulNumWarningFiles:	RESD 	1
	.lphError:		RESQ 	1
	.ulNumErrorFiles:	RESD 	1
	.lphFatal:		RESQ 	1
	.ulNumFatalFiles:	RESD 	1
	.Size:
ENDSTRUC


extern CreateLog
extern CreateLogFromOpenedFile
extern CreatePriorityLog
extern CreateLogWithMultipleFiles
extern CreateLogWithMultipleFilesAndDefinableProc
extern WriteLog
extern WriteLogSingleFile
