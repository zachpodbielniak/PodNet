/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CSerialization.h"




_Success_(return != FALSE, _Non_Locking_)
PODNET_API 
BOOL 
SerializeByte(
	_In_ 		LPHANDLE 		lphDynamicRegion,
	_In_ 		BYTE			byData
){ return SerializeRaw(lphDynamicRegion, &byData, sizeof(BYTE)); }




_Success_(return != FALSE, _Non_Locking_)
PODNET_API 
BOOL 
SerializeWord(
	_In_ 		LPHANDLE 		lphDynamicRegion,
	_In_ 		WORD			wData
){ 
	WORD wMod;
	wMod = HostToNetworkShort(wData);

	return SerializeRaw(lphDynamicRegion, &wMod, sizeof(WORD)); 
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API 
BOOL 
SerializeDword(
	_In_ 		LPHANDLE 		lphDynamicRegion,
	_In_ 		DWORD			dwData
){ 
	DWORD dwMod;
	dwMod = HostToNetworkLong(dwData);

	return SerializeRaw(lphDynamicRegion, &dwMod, sizeof(DWORD)); 
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API 
BOOL 
SerializeQword(
	_In_ 		LPHANDLE 		lphDynamicRegion,
	_In_ 		QWORD			qwData
){ 
	QWORD qwMod;
	qwMod = HostToNetworkLongLong(qwData);

	return SerializeRaw(lphDynamicRegion, &qwMod, sizeof(QWORD)); 
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API 
BOOL 
SerializeArch(
	_In_ 		LPHANDLE 		lphDynamicRegion,
	_In_ 		UARCHLONG		ualData
){ 
	UARCHLONG ualMod;
	
	#ifdef __LP64__
	ualMod = HostToNetworkLongLong(ualData);
	#else
	ualMod = HostToNetworkLong(ualData);
	#endif

	return SerializeRaw(lphDynamicRegion, &ualMod, sizeof(UARCHLONG)); 
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API 
BOOL 
SerializeString(
	_In_ 		LPHANDLE 		lphDynamicRegion,
	_In_ 		LPSTR			lpszData
){
	EXIT_IF_UNLIKELY_NULL(lpszData, FALSE);

	QWORD qwLength;
	qwLength = StringLength(lpszData);

	SerializeQword(lphDynamicRegion, qwLength);
	return SerializeRaw(lphDynamicRegion, lpszData, qwLength);
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API 
BOOL 
SerializeRaw(
	_In_ 		LPHANDLE 		lphDynamicRegion,
	_In_ 		LPVOID 			lpData,
	_In_ 		UARCHLONG 		ualSize
){
	EXIT_IF_UNLIKELY_NULL(lpData, FALSE);
	EXIT_IF_UNLIKELY_NULL(ualSize, FALSE);

	if (NULLPTR == *lphDynamicRegion)
	{ *lphDynamicRegion = CreateDynamicHeapRegion(NULLPTR, ualSize, FALSE, 0); }

	if (NULLPTR == *lphDynamicRegion)
	{ return FALSE; }

	return DynamicHeapRegionAppend(
		*lphDynamicRegion,
		lpData,
		ualSize
	);
}
