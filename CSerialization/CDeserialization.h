/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CDESERIALIZATION_H
#define CDESERIALIZATION_H


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../CMemory/CDynamicHeapRegion.h"




_Success_(return != 0, _Non_Locking_)
PODNET_API 
BYTE
DeserializeByte(
	_In_ 		HANDLE 			hDynamicRegion,
	_In_Out_ 	LPUARCHLONG 		lpualOffSet
);




_Success_(return != 0, _Non_Locking_)
PODNET_API 
WORD
DeserializeWord(
	_In_ 		HANDLE 			hDynamicRegion,
	_In_Out_ 	LPUARCHLONG 		lpualOffSet
);




_Success_(return != 0, _Non_Locking_)
PODNET_API 
DWORD
DeserializeDword(
	_In_ 		HANDLE 			hDynamicRegion,
	_In_Out_ 	LPUARCHLONG 		lpualOffSet
);




_Success_(return != 0, _Non_Locking_)
PODNET_API 
QWORD
DeserializeQword(
	_In_ 		HANDLE 			hDynamicRegion,
	_In_Out_ 	LPUARCHLONG 		lpualOffSet
);




/* NOT COMPATIBLE ACROSS DIFFERENT BITNESS ARCHITECTURES!!! */
_Success_(return != 0, _Non_Locking_)
PODNET_API 
UARCHLONG
DeserializeArch(
	_In_ 		HANDLE 			hDynamicRegion,
	_In_Out_ 	LPUARCHLONG 		lpualOffSet
);




_Success_(return != 0, _Non_Locking_)
PODNET_API 
LPSTR
DeserializeString(
	_In_ 		HANDLE 			hDynamicRegion,
	_In_Out_ 	LPUARCHLONG 		lpualOffSet
);




_Success_(return != 0, _Non_Locking_)
PODNET_API 
LPVOID
DeserializeRaw(
	_In_ 		HANDLE 			hDynamicRegion,
	_Out_Opt_ 	LPVOID 			lpOut,
	_Out_Opt_	LPUARCHLONG 		lpualSize,
	_In_Out_ 	LPUARCHLONG 		lpualOffSet
);








#endif
