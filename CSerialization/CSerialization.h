/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CSERIALIZATION_H
#define CSERIALIZATION_H


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../CMemory/CDynamicHeapRegion.h"




_Success_(return != FALSE, _Non_Locking_)
PODNET_API 
BOOL 
SerializeByte(
	_In_ 		LPHANDLE 		lphDynamicRegion,
	_In_ 		BYTE			byData
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API 
BOOL 
SerializeWord(
	_In_ 		LPHANDLE 		lphDynamicRegion,
	_In_ 		WORD			wData
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API 
BOOL 
SerializeDword(
	_In_ 		LPHANDLE 		lphDynamicRegion,
	_In_ 		DWORD			dwData
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API 
BOOL 
SerializeQword(
	_In_ 		LPHANDLE 		lphDynamicRegion,
	_In_ 		QWORD			qwData
);




/* NOT COMPATIBLE ACROSS DIFFERENT BITNESS ARCHITECTURES!!! */
_Success_(return != FALSE, _Non_Locking_)
PODNET_API 
BOOL 
SerializeArch(
	_In_ 		LPHANDLE 		lphDynamicRegion,
	_In_ 		UARCHLONG		ualData
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API 
BOOL 
SerializeString(
	_In_ 		LPHANDLE 		lphDynamicRegion,
	_In_ 		LPSTR			lpszData
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API 
BOOL 
SerializeRaw(
	_In_ 		LPHANDLE 		lphDynamicRegion,
	_In_ 		LPVOID 			lpData,
	_In_ 		UARCHLONG 		ualSize
);








#endif