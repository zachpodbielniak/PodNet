/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CDeserialization.h"


#define CALC_REGION_OFFSET()		(LPVOID)(((UARCHLONG)DynamicHeapRegionData(hDynamicRegion, NULLPTR) + *lpualOffSet))



_Success_(return != 0, _Non_Locking_)
PODNET_API 
BYTE
DeserializeByte(
	_In_ 		HANDLE 			hDynamicRegion,
	_In_Out_ 	LPUARCHLONG 		lpualOffSet
){
	BYTE byOut;
	UARCHLONG ualSize;

	byOut = 0;
	ualSize = sizeof(BYTE);

	DeserializeRaw(hDynamicRegion, (LPVOID)&byOut, &ualSize, lpualOffSet);
	return byOut;
}




_Success_(return != 0, _Non_Locking_)
PODNET_API 
WORD
DeserializeWord(
	_In_ 		HANDLE 			hDynamicRegion,
	_In_Out_ 	LPUARCHLONG 		lpualOffSet
){
	WORD wOut;
	UARCHLONG ualSize;

	wOut = 0;
	ualSize = sizeof(WORD);

	DeserializeRaw(hDynamicRegion, (LPVOID)&wOut, &ualSize, lpualOffSet);
	wOut = NetworkToHostShort(wOut);
	return wOut;
}




_Success_(return != 0, _Non_Locking_)
PODNET_API 
DWORD
DeserializeDword(
	_In_ 		HANDLE 			hDynamicRegion,
	_In_Out_ 	LPUARCHLONG 		lpualOffSet
){
	DWORD dwOut;
	UARCHLONG ualSize;

	dwOut = 0;
	ualSize = sizeof(DWORD);
	
	DeserializeRaw(hDynamicRegion, (LPVOID)&dwOut, &ualSize, lpualOffSet);
	dwOut = NetworkToHostLong(dwOut);
	return dwOut;
}




_Success_(return != 0, _Non_Locking_)
PODNET_API 
QWORD
DeserializeQword(
	_In_ 		HANDLE 			hDynamicRegion,
	_In_Out_ 	LPUARCHLONG 		lpualOffSet
){
	QWORD qwOut;
	UARCHLONG ualSize;

	qwOut = 0;
	ualSize = sizeof(QWORD);

	DeserializeRaw(hDynamicRegion, (LPVOID)&qwOut, &ualSize, lpualOffSet);
	qwOut = NetworkToHostLongLong(qwOut);
	return qwOut;
}




/* NOT COMPATIBLE ACROSS DIFFERENT BITNESS ARCHITECTURES!!! */
_Success_(return != 0, _Non_Locking_)
PODNET_API 
UARCHLONG
DeserializeArch(
	_In_ 		HANDLE 			hDynamicRegion,
	_In_Out_ 	LPUARCHLONG 		lpualOffSet
){
	UARCHLONG ualOut;
	UARCHLONG ualSize;

	ualOut = 0;
	ualSize = sizeof(UARCHLONG);

	DeserializeRaw(hDynamicRegion, (LPVOID)&ualOut, &ualSize, lpualOffSet);

	#ifdef __LP64__
	ualOut = NetworkToHostLongLong(ualOut);
	#else 
	ualOut = NetworkToHostLong(ualOut);
	#endif 

	return ualOut;
}




_Success_(return != 0, _Non_Locking_)
PODNET_API 
LPSTR
DeserializeString(
	_In_ 		HANDLE 			hDynamicRegion,
	_In_Out_ 	LPUARCHLONG 		lpualOffSet
){
	LPSTR lpszOut;
	lpszOut = NULLPTR;

	lpszOut = DeserializeRaw(hDynamicRegion, NULLPTR, NULLPTR, lpualOffSet);
	return lpszOut;
}




_Success_(return != 0, _Non_Locking_)
PODNET_API 
LPVOID
DeserializeRaw(
	_In_ 		HANDLE 			hDynamicRegion,
	_Out_Opt_ 	LPVOID 			lpOut,
	_Out_Opt_	LPUARCHLONG 		lpualSize,
	_In_Out_ 	LPUARCHLONG 		lpualOffSet
){
	EXIT_IF_UNLIKELY_NULL(hDynamicRegion, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpualOffSet, NULLPTR);

	/* Allocate Output For String */
	if (NULLPTR == lpualSize)
	{
		LPSTR lpszData;
		QWORD qwLength;

		qwLength = DeserializeQword(hDynamicRegion, lpualOffSet);
		lpszData = GlobalAllocAndZero(qwLength + 1);

		if (NULLPTR == lpszData)
		{ return NULLPTR; }

		CopyMemory(lpszData, CALC_REGION_OFFSET(), qwLength);
		*lpualOffSet += qwLength;

		return lpszData;
	}

	/* Allocate And Write Size */
	else if (0 == *lpualSize)
	{
		LPVOID lpData;
		QWORD qwLength;

		qwLength = DeserializeQword(hDynamicRegion, lpualOffSet);
		lpData = GlobalAllocAndZero(qwLength);

		if (NULLPTR == lpData)
		{ return NULLPTR; }

		CopyMemory(lpData, CALC_REGION_OFFSET(), qwLength);
		*lpualOffSet += qwLength;

		return lpData;
	}

	/* Literal Value */
	else
	{
		CopyMemory(lpOut, CALC_REGION_OFFSET(), *lpualSize);
		*lpualOffSet += *lpualSize;
		
		return NULLPTR;
	}

	/* Should Never Happen */
	return NULLPTR;
}