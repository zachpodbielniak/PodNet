/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef ICONTROLLER_H
#define ICONTROLLER_H

#include "../CHandle/CHandle.h"

typedef LPCONTROLLER      HCONTROLLER;

#define INHERITS_FROM_ICONTROLLER()              HCONTROLLER     hController;


typedef _Call_Back_ BOOL(* LPFN_CONTROLLER_ON_BUTTON_PRESSED)(
        _In_            HCONTROLLER             hController,
        _In_            ULONG                   ulButton,
        _In_            LPVOID                  lpParam
);

typedef _Call_Back_ BOOL(* LPFN_CONTROLLER_ON_BUTTON_RELEASED)(
        _In_            HCONTROLLER             hController,
        _In_            ULONG                   ulButton,
        _In_            LPVOID                  lpParam
);

typedef _Call_Back_ BOOL(* LPFN_CONTROLLER_ON_ANALOG_STICK_MOVED)(
        _In_            HCONTROLLER             hController,
        _In_            ULONG                   ulAnalogStick,
        _In_            DOUBLE                  dbX,
        _In_            DOUBLE                  dbY,
        _In_            LPVOID                  lpParam
);

typedef struct _ICONTROLLER
{
        HANDLE                                  hCritSec;
        LPFN_CONTROLLER_ON_BUTTON_PRESSED       OnButtonPressed;        
        LPFN_CONTROLLER_ON_BUTTON_RELEASED      OnButtonReleased;        
        LPFN_CONTROLLER_ON_ANALOG_STICK_MOVED   OnAnalogStickMoved;
        ULONG                                   ulControllerType;
        LPVOID                                  lpData;
} CONTROLLER, *LPCONTROLLER;


_Result_Null_On_Failure_
PODNET_API
HCONTROLLER
CreateController(
        _In_                    ULONG                                   ulControllerType,
        _In_Opt_                LPVOID                                  lpData,
        _In_                    LPFN_CONTROLLER_ON_BUTTON_PRESSED       lpfnButtonPressed,
        _In_                    LPFN_CONTROLLER_ON_BUTTON_RELEASED      lpfnButtonReleased,                      
        _In_Opt_                LPFN_CONTROLLER_ON_ANALOG_STICK_MOVED   lpfnAnalogProc
);



_Success_(return != FALSE, ...)
_Call_Back_Default_Implementation_
PODNET_API
BOOL
IControllerOnButtonPressed(
        _In_                    HCONTROLLER                             hController,
        _In_                    ULONG                                   ulButton,
        _In_                    LPVOID                                  lpParam
);



#endif