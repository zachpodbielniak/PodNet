/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef ITOSTRING_H
#define ITOSTRING_H

#include "../TypeDefs.h" 
#include "../Prereqs.h"
#define ITOSTRING()		\
	VOID (*ToString)(	\
		LPVOID lpData,	\
		ULONG ulSize	\
	);			

#define ITOSTRING_DEFAULT_IMPLEMENTATION(STRUCTURE)	STRUCTURE.ToString = ToString

VOID 
ToString(
	LPVOID lpData,
	ULONG ulSize
)
{
	ULONG ulIndex;
	for (ulIndex = 0; ulSize >= 8; ulSize -= 8)
		printf("%llu",(ULONGLONG)(lpData + (sizeof(LPVOID) * ulIndex)));
	if (ulSize != 0)
	{
		switch(ulSize)
		{
			case 7:
				printf("%u", (ULONG)(lpData + (sizeof(LPVOID) * ulIndex) + 4);
				printf("%hu", (USHORT)(lpData + (sizeof(LPVOID) * ulIndex) + 6);
				printf("%hhu", (UCHAR)(lpData + (sizeof(LPVOID) * ulIndex) + 7);
				break;
			case 6:
				printf("%u", (ULONG)(lpData + (sizeof(LPVOID) * ulIndex) + 4);
				printf("%hu", (USHORT)(lpData + (sizeof(LPVOID) * ulIndex) + 6);
				break;
			case 5:
				printf("%u", (ULONG)(lpData + (sizeof(LPVOID) * ulIndex) + 4);
				printf("%hhu", (UCHAR)(lpData + (sizeof(LPVOID) * ulIndex) + 5);
				break;
			case 4:
				printf("%u", (ULONG)(lpData + (sizeof(LPVOID) * ulIndex) + 4);
				break;
			case 3:
				printf("%hu", (USHORT)(lpData + (sizeof(LPVOID) * ulIndex) + 2);
				printf("%hhu", (UCHAR)(lpData + (sizeof(LPVOID) * ulIndex) + 3);
				break;
			case 2:
				printf("%hu", (USHORT)(lpData + (sizeof(LPVOID) * ulIndex) + 2);
				break;
			case 1: 
				printf("%hhu", (UCHAR)(lpData + (sizeof(LPVOID) * ulIndex) + 1);
				break;
		}
	}
}

#endif

