'''



 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		    CFastApprox.py
Author:	        Zach Podbielniak
Last Update:	01/12/2018

Overview:	This file sets forth forwarding the PodNet C API to Python.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


'''

from PodNet.PodNetLib import *

PodNetLib.FastApproxSin.restype = FLOAT
PodNetLib.FastApproxSin.argtypes = [FLOAT]

PodNetLib.FastApproxCos.restype = FLOAT
PodNetLib.FastApproxCos.argtypes = [FLOAT]

PodNetLib.FastApproxTan.restype = FLOAT
PodNetLib.FastApproxTan.argtypes = [FLOAT]

PodNetLib.FastApproxSinD.restype = DOUBLE
PodNetLib.FastApproxSinD.argtypes = [DOUBLE]

PodNetLib.FastApproxCosD.restype = DOUBLE
PodNetLib.FastApproxCosD.argtypes = [DOUBLE]

PodNetLib.FastApproxTanD.restype = DOUBLE
PodNetLib.FastApproxTanD.argtypes = [DOUBLE]

PodNetLib.FastApproxSec.restype = DOUBLE
PodNetLib.FastApproxSec.argtypes = [DOUBLE]

PodNetLib.FastApproxCsc.restype = DOUBLE
PodNetLib.FastApproxCsc.argtypes = [DOUBLE]

PodNetLib.FastApproxCot.restype = DOUBLE
PodNetLib.FastApproxCot.argtypes = [DOUBLE]

PodNetLib.FastApproxSecD.restype = DOUBLE
PodNetLib.FastApproxSecD.argtypes = [DOUBLE]

PodNetLib.FastApproxCscD.restype = DOUBLE
PodNetLib.FastApproxCscD.argtypes = [DOUBLE]

PodNetLib.FastApproxCotD.restype = DOUBLE
PodNetLib.FastApproxCotD.argtypes = [DOUBLE]

PodNetLib.FastApproxArcSin.restype = DOUBLE
PodNetLib.FastApproxArcSin.argtypes = [DOUBLE]

PodNetLib.FastApproxArcCos.restype = DOUBLE
PodNetLib.FastApproxArcCos.argtypes = [DOUBLE]

PodNetLib.FastApproxArcTan.restype = DOUBLE
PodNetLib.FastApproxArcTan.argtypes = [DOUBLE]

PodNetLib.FastApproxArcSinD.restype = DOUBLE
PodNetLib.FastApproxArcSinD.argtypes = [DOUBLE]

PodNetLib.FastApproxArcCosD.restype = DOUBLE
PodNetLib.FastApproxArcCosD.argtypes = [DOUBLE]

PodNetLib.FastApproxArcTanD.restype = DOUBLE
PodNetLib.FastApproxArcTanD.argtypes = [DOUBLE]



def FastApproxSin(fRads):
    return PodNetLib.FastApproxSin(fRads)


def FastApproxCos(fRads):
    return PodNetLib.FastApproxCos(fRads)


def FastApproxTan(fRads):
    return PodNetLib.FastApproxTan(fRads)


def FastApproxSinD(dbRads):
    return PodNetLib.FastApproxSinD(dbRads)


def FastApproxCosD(dbRads):
    return PodNetLib.FastApproxCosD(dbRads)


def FastApproxTanD(dbRads):
    return PodNetLib.FastApproxTanD(dbRads)


def FastApproxSec(fRads):
    return PodNetLib.FastApproxSec(fRads)


def FastApproxCsc(fRads):
    return PodNetLib.FastApproxCsc(fRads)


def FastApproxCot(fRads):
    return PodNetLib.FastApproxCot(fRads)


def FastApproxSecD(dbRads):
    return PodNetLib.FastApproxSecD(dbRads)


def FastApproxCscD(dbRads):
    return PodNetLib.FastApproxCscD(dbRads)


def FastApproxCotD(dbRads):
    return PodNetLib.FastApproxCotD(dbRads)


def FastApproxArcSin(fValue):
    return PodNetLib.FastApproxArcSin(fValue)


def FastApproxArcCos(fValue):
    return PodNetLib.FastApproxArcCos(fValue)


def FastApproxArcTan(fValue):
    return PodNetLib.FastApproxArcTan(fValue) 


def FastApproxArcSinD(dbValue):
    return PodNetLib.FastApproxArcSinD(dbValue)


def FastApproxArcCosD(dbValue):
    return PodNetLib.FastApproxArcCosD(dbValue)


def FastApproxArcTanD(dbValue):
    return PodNetLib.FastApproxArcTanD(dbValue) 

