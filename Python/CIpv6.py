'''



 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		CIpv4.py
Author:	        Zach Podbielniak
Last Update:	06/25/18

Overview:	This file sets forth forwarding the PodNet C API to Python.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


'''


from PodNet.PodNetLib import *
from PodNet.CIpv4 import *
import subprocess 



class IPV6_ADDRESS(Structure):
	_fields_ = [("ullX"[2], ULONGLONG)]

LPIPV6_ADDRESS = POINTER(IPV6_ADDRESS)


IPV6_ADDRESS_STRING_LENGTH = 40


IPV6_ADDRESS_NOT_VALID 		= 0
IPV6_ADDRESS_NULL		= 1
IPV6_ADDRESS_LOOPBACK		= 2
IPV6_ADDRESS_IPV4_TRANSLATION	= 3
IPV6_ADDRESS_DISCARD_PREFIX 	= 4
IPV6_ADDRESS_TOREDO 		= 5
IPV6_ADDRESS_ORCHIDv2		= 6
IPV6_ADDRESS_DOCUMENTATION	= 7
IPV6_ADDRESS_6TO4		= 8
IPV6_ADDRESS_UNIQUE_LOCAL	= 9
IPV6_ADDRESS_LINK_LOCAL		= 10
IPV6_ADDRESS_MULTICAST		= 11



PodNetLib.CreateIpv6AddressFromString.restype = LPIPV6_ADDRESS
PodNetLib.CreateIpv6AddressFromString.argtypes = [LPSTR]

PodNetLib.DestroyIpv6Address.restype = BOOL 
PodNetLib.DestroyIpv6Address.argtypes = [LPIPV6_ADDRESS]

PodNetLib.ConvertIpv6AddressToString.restype = BOOL 
PodNetLib.ConvertIpv6AddressToString.argtypes = [LPIPV6_ADDRESS, LPSTR, ULONG]

PodNetLib.ConvertIpv6AddressToCompressedString.restype = BOOL 
PodNetLib.ConvertIpv6AddressToCompressedString.argtypes = [LPIPV6_ADDRESS, LPSTR, ULONG]

PodNetLib.CompressIpv6AddressString.restype = BOOL 
PodNetLib.CompressIpv6AddressString.argtypes = [LPSTR, LPSTR, ULONG]

PodNetLib.GetIpv6AddressType.restype = ULONG 
PodNetLib.GetIpv6AddressType.argtypes = [LPIPV6_ADDRESS]

PodNetLib.GetIpv6AddressTypeByString.restype = ULONG 
PodNetLib.GetIpv6AddressTypeByString.argtypes = [LPSTR]

PodNetLib.ConvertIpv6AddressToIpv4.restype = IPV4_ADDRESS
PodNetLib.ConvertIpv6AddressToIpv4.argtypes = [LPIPV6_ADDRESS]

PodNetLib.ConvertIpv4AddressToIpv6ByCreate.restype = LPIPV6_ADDRESS
PodNetLib.ConvertIpv4AddressToIpv6ByCreate.argtypes = [IPV4_ADDRESS]

PodNetLib.ConvertIpv4AddressToIpv6.restype = BOOL 
PodNetLib.ConvertIpv4AddressToIpv6.argtypes = [IPV4_ADDRESS, LPIPV6_ADDRESS]

PodNetLib.GetCurrentPublicIpv6Address.restype = LPIPV6_ADDRESS




def CreateIpv6AddressFromString(lpszAddress):
	return PodNetLib.CreateIpv6AddressFromString(lpszAddress.encode('utf-8'))


def DestroyIpv6Address(lpip6Address):
	return PodNetLib.DestroyIpv6Address(lpip6Address)


def ConvertIpv6AddressToString(lpip6Address):
	lpszOut = create_string_buffer(b'\0' * IPV6_ADDRESS_STRING_LENGTH)
	PodNetLib.ConvertIpv6AddressToString(lpip6Address, lpszOut, IPV6_ADDRESS_STRING_LENGTH)
	return lpszOut.value.decode('utf-8')


def ConvertIpv6AddressToCompressedString(lpip6Address):
	lpszOut = create_string_buffer(b'\0' * IPV6_ADDRESS_STRING_LENGTH)
	PodNetLib.ConvertIpv6AddressToCompressedString(lpip6Address, lpszOut, IPV6_ADDRESS_STRING_LENGTH)
	return lpszOut.value.decode('utf-8')


def CompressIpv6AddressString(lpszAddress):
	lpszOut = create_string_buffer(b'\0' * IPV6_ADDRESS_STRING_LENGTH)
	PodNetLib.CompressIpv6AddressString(lpszAddress.encode('utf-8'), lpszOut, IPV6_ADDRESS_STRING_LENGTH)
	return lpszOut.value.decode('utf-8')


def GetIpv6AddressType(lpip6Address):
	return PodNetLib.GetIpv6AddressType(lpip6Address)


def GetIpv6AddressTypeByString(lpszAddress):
	return PodNetLib.GetIpv6AddressTypeByString(lpszAddress.encode('utf-8'))


def ConvertIpv6AddressToIpv4(lpip6Address):
	return PodNetLib.ConvertIpv6AddressToIpv4(lpip6Address)


def ConvertIpv4AddressToIpv6ByCreate(ip4Address):
	return PodNetLib.ConvertIpv4AddressToIpv6ByCreate(ip4Address)


def ConvertIpv4AddressToIpv6(ip4Address, lpip6Address):
	return PodNetLib.ConvertIpv4AddressToIpv6(ip4Address, lpip6Address)


def GetCurrentPublicIpv6Address():
	return PodNetLib.GetCurrentPublicIpv6Address()