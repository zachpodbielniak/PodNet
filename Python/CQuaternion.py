'''



 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		CSemaphore.py
Author:	        Zach Podbielniak
Last Update:	04/29/18

Overview:	This file sets forth forwarding the PodNet C API to Python.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


'''


from PodNet.PodNetLib import *


class QUATERNION(Structure):
	_fields_ = [ 	("fW", FLOAT),
			("fX", FLOAT),
			("fY", FLOAT),
			("fZ", FLOAT)]


LPQUATERNION = POINTER(QUATERNION)


PodNetLib.CreateQuaternion.restype = LPQUATERNION
PodNetLib.CreateQuaternion.argtypes = [FLOAT, FLOAT, FLOAT, FLOAT]

PodNetLib.DestroyQuaternion.restype = BOOL 
PodNetLib.DestroyQuaternion.argtypes = [LPQUATERNION]

'''
PodNetLib.QuaternionGetAxisX.restype = LPVECTOR3D
PodNetLib.QuaternionGetAxisX.argtypes = [LPQUATERNION]

PodNetLib.QuaternionGetAxisY.restype = LPVECTOR3D
PodNetLib.QuaternionGetAxisY.argtypes = [LPQUATERNION]

PodNetLib.QuaternionGetAxisZ.restype = LPVECTOR3D
PodNetLib.QuaternionGetAxisZ.argtypes = [LPQUATERNION]
'''

PodNetLib.QuaternionAdd.restype = LPQUATERNION
PodNetLib.QuaternionAdd.argtypes = [LPQUATERNION, LPQUATERNION, LPQUATERNION]

PodNetLib.QuaternionSubtract.restype = LPQUATERNION
PodNetLib.QuaternionSubtract.argtypes = [LPQUATERNION, LPQUATERNION, LPQUATERNION]

PodNetLib.QuaternionMultiply.restype = LPQUATERNION
PodNetLib.QuaternionMultiply.argtypes = [LPQUATERNION, LPQUATERNION, LPQUATERNION]

PodNetLib.QuaternionMultiplyByScalar.restype = LPQUATERNION
PodNetLib.QuaternionMultiplyByScalar.argtypes = [LPQUATERNION, LPQUATERNION, FLOAT]

'''
PodNetLib.QuaternionDivide.restype = LPQUATERNION
PodNetLib.QuaternionDivide.argtypes = [LPQUATERNION, LPQUATERNION, LPQUATERNION]
'''

PodNetLib.QuaternionNegate.restype = LPQUATERNION
PodNetLib.QuaternionNegate.argtypes = [LPQUATERNION, LPQUATERNION]

PodNetLib.QuaternionDotProduct.restype = FLOAT 
PodNetLib.QuaternionDotProduct.argtypes = [LPQUATERNION, LPQUATERNION]

PodNetLib.QuaternionNorm.restype = FLOAT 
PodNetLib.QuaternionNorm.argtypes = [LPQUATERNION]

PodNetLib.QuaternionNormalize.restype = FLOAT
PodNetLib.QuaternionNormalize.argtypes = [LPQUATERNION]

PodNetLib.QuaternionInverse.restype = LPQUATERNION
PodNetLib.QuaternionInverse.argtypes = [LPQUATERNION, LPQUATERNION]

PodNetLib.QuaternionUnitInverse.restype = LPQUATERNION
PodNetLib.QuaternionUnitInverse.argtypes = [LPQUATERNION, LPQUATERNION]

PodNetLib.QuaternionExp.restype = LPQUATERNION
PodNetLib.QuaternionExp.argtypes = [LPQUATERNION, LPQUATERNION]

PodNetLib.QuaternionExpFast.restype = LPQUATERNION
PodNetLib.QuaternionExpFast.argtypes = [LPQUATERNION, LPQUATERNION]

PodNetLib.QuaternionLog.restype = LPQUATERNION
PodNetLib.QuaternionLog.argtypes = [LPQUATERNION, LPQUATERNION]

PodNetLib.QuaternionLogFast.restype = LPQUATERNION
PodNetLib.QuaternionLogFast.argtypes = [LPQUATERNION, LPQUATERNION]

'''
PodNetLib.QuaternionMultiplyByVector3D.restype = LPVECTOR3D
PodNetLib.QuaternionMultiplyByVector3D.argtypes = [LPVECTOR3D, LPQUATERNION, LPVECTOR3D]
'''

PodNetLib.QuaternionEqual.restype = BOOL 
PodNetLib.QuaternionEqual.argtypes = [LPQUATERNION, LPQUATERNION]

PodNetLib.QuaternionSlerp.restype = LPQUATERNION
PodNetLib.QuaternionSlerp.argtypes = [LPQUATERNION, LPQUATERNION, LPQUATERNION, FLOAT, BOOL]

PodNetLib.QuaternionSlerpExtraSpins.restype = LPQUATERNION
PodNetLib.QuaternionSlerpExtraSpins.argtypes = [LPQUATERNION, LPQUATERNION, LPQUATERNION, FLOAT, LONG]

PodNetLib.QuaternionGetRoll.restype = FLOAT 
PodNetLib.QuaternionGetRoll.argtypes = [LPQUATERNION, BOOL]

PodNetLib.QuaternionGetPitch.restype = FLOAT 
PodNetLib.QuaternionGetPitch.argtypes = [LPQUATERNION, BOOL]

PodNetLib.QuaternionGetYaw.restype = FLOAT 
PodNetLib.QuaternionGetYaw.argtypes = [LPQUATERNION, BOOL]



def CreateQuaternion(fW, fX, fY, fZ):
	return PodNetLib.CreateQuaternion(fW, fX, fY, fZ)


def DestroyQuaternion(lpquaQuat):
	return PodNetLib.DestroyQuaternion(lpquaQuat)


def QuaternionAdd(lpquaSum, lpquaAugend, lpquaAddend):
	if (NULLPTR == lpquaSum):
		return PodNetLib.QuaternionAdd(cast(NULLPTR, LPQUATERNION), lpquaAugend, lpquaAddend)
	b = PodNetLib.QuaternionAdd(lpquaSum, lpquaAugend, lpquaAddend)
	return BOOL(cast(b, LPVOID).value).value


def QuaternionSubtract(lpquaDifference, lpquaMinuend, lpquaSubtrahend):
	if (NULLPTR == lpquaDifference):
		return PodNetLib.QuaternionSubtract(cast(NULLPTR, LPQUATERNION), lpquaMinuend, lpquaSubtrahend)
	b = PodNetLib.QuaternionSubtract(lpquaDifference, lpquaMinuend, lpquaSubtrahend)
	return BOOL(cast(b, LPVOID).value).value


def QuaternionMultiply(lpquaProduct, lpquaMultiplicand, lpquaMultiplier):
	if (NULLPTR == lpquaProduct):
		return PodNetLib.QuaternionMultiply(cast(NULLPTR, LPQUATERNION), lpquaMultiplicand, lpquaMultiplier)
	b = PodNetLib.QuaternionMultiply(lpquaProduct, lpquaMultiplicand, lpquaMultiplier)
	return BOOL(cast(b, LPVOID).value).value


def QuaternionMultiplyByScalar(lpquaProduct, lpquaMultiplicand, fMultiplier):
	if (NULLPTR == lpquaProduct):
		return PodNetLib.QuaternionMultiplyByScalar(cast(NULLPTR, LPQUATERNION), lpquaMultiplicand, fMultiplier)
	b = PodNetLib.QuaternionMultiplyByScalar(lpquaProduct, lpquaMultiplicand, fMultiplier)
	return BOOL(cast(b, LPVOID).value).value


def QuaternionNegate(lpquaOut, lpquaToBeNegated):
	if (NULLPTR == lpquaOut):
		return PodNetLib.QuaternionNegate(cast(NULLPTR, LPQUATERNION), lpquaToBeNegated)
	b = PodNetLib.QuaternionNegate(lpquaOut, lpquaToBeNegated)
	return BOOL(cast(b, LPVOID).value).value


def QuaternionNegateCurrent(lpquaToBeNegated):
	return QuaternionNegate(lpquaToBeNegated, lpquaToBeNegated)


def QuaternionDotProduct(lpquaDoterand, lpquaDoterplier):
	return PodNetLib.QuaternionDotProduct(lpquaDoterand, lpquaDoterplier)


def QuaternionNorm(lpquaNormerand):
	return PodNetLib.QuaternionNorm(lpquaNormerand)


def QuaternionNormalize(lpquaNormerand):
	return PodNetLib.QuaternionNormalize(lpquaNormerand)


def QuaternionInverse(lpquaInverted, lpquaInverterand):
	if (NULLPTR == lpquaInverted):
		return PodNetLib.QuaternionInverse(cast(NULLPTR, LPQUATERNION), lpquaInverterand)
	b = PodNetLib.QuaternionInverse(lpquaInverted, lpquaInverterand)
	return BOOL(cast(b, LPVOID).value).value	


def QuaternionUnitInverse(lpquaUnitInverted, lpquaUnitInverterand):
	if (NULLPTR == lpquaUnitInverted):
		return PodNetLib.QuaternionInverse(cast(NULLPTR, LPQUATERNION), lpquaUnitInverterand)
	b = PodNetLib.QuaternionInverse(lpquaUnitInverted, lpquaUnitInverterand)
	return BOOL(cast(b, LPVOID).value).value	


def QuaternionExp(lpquaExped, lpquaExperand):
	if (NULLPTR == lpquaExped):
		return PodNetLib.QuaternionExp(cast(NULLPTR, LPQUATERNION), lpquaExped, lpquaExperand)
	b = PodNetLib.QuaternionExp(lpquaExped, lpquaExperand)
	return BOOL(cast(b, LPVOID).value).value


def QuaternionExpFast(lpquaExped, lpquaExperand):
	if (NULLPTR == lpquaExped):
		return PodNetLib.QuaternionExpFast(cast(NULLPTR, LPQUATERNION), lpquaExped, lpquaExperand)
	b = PodNetLib.QuaternionExpFast(lpquaExped, lpquaExperand)
	return BOOL(cast(b, LPVOID).value).value


def QuaternionLog(lpquaLogout, lpquaLogerand):
	if (NULLPTR == lpquaLogout):
		return PodNetLib.QuaternionLog(cast(NULLPTR, LPQUATERNION), lpquaLogerand)
	b = PodNetLib.QuaternionLog(lpquaLogout, lpquaLogerand)
	return BOOL(cast(b, LPVOID).value).value


def QuaternionLogFast(lpquaLogout, lpquaLogerand):
	if (NULLPTR == lpquaLogout):
		return PodNetLib.QuaternionLogFast(cast(NULLPTR, LPQUATERNION), lpquaLogerand)
	b = PodNetLib.QuaternionLogFast(lpquaLogout, lpquaLogerand)
	return BOOL(cast(b, LPVOID).value).value


'''
def QuaternionMultiplyByVector3D(lpv3Out, lpquaMultiplicand, lpv3Multiplier):
'''


def QuaternionEqual(lpquaLefterand, lpquaRighterand):
	return PodNetLib.QuaternionEqual(lpquaLefterand, lpquaRighterand)


def QuaternionSlerp(lpquaOut, lpquaP, lpquaQ, fDistance, bShortestPath):
	if (NULLPTR == lpquaOut):
		return PodNetLib.QuaternionSlerp(cast(NULLPTR, LPQUATERNION), lpquaP, lpquaQ, fDistance, bShortestPath)
	b = PodNetLib.QuaternionSlerp(lpquaOut, lpquaP, lpquaQ, fDistance, bShortestPath)
	return BOOL(cast(b, LPVOID).value).value


def QuaternionSlerpExtraSpins(lpquaOut, lpquaP, lpquaQ, fDistance, lExtraSpins):
	if (NULLPTR == lpquaOut):
		return PodNetLib.QuaternionSlerpExtraSpins(cast(NULLPTR, LPQUATERNION), lpquaP, lpquaQ, fDistance, lExtraSpins)
	b = PodNetLib.QuaternionSlerpExtraSpins(lpquaOut, lpquaP, lpquaQ, fDistance, lExtraSpins)
	return BOOL(cast(b, LPVOID).value).value


def QuaternionGetRoll(lpquaRollerand, bReProject):
	return PodNetLib.QuaternionGetRoll(lpquaRollerand, bReProject)


def QuaternionGetPitch(lpquaPitcherand, bReProject):
	return PodNetLib.QuaternionGetPitch(lpquaPitcherand, bReProject)


def QuaternionGetYaw(lpquaYawerand, bReProject):
	return PodNetLib.QuaternionGetYaw(lpquaYawerand, bReProject)







class Quaternion(object):
	lpquaCore = cast(NULLPTR, LPQUATERNION)
	def __init__(self, fW, fX, fY, fZ):
		self.lpquaCore = CreateQuaternion(fW, fX, fY, fZ)

	def __del__(self):
		DestroyQuaternion(self.lpquaCore)

	def __str__(self):
		return "Quaternion(%f, %f, %f, %f)" % (self.lpquaCore.contents.fW, self.lpquaCore.contents.fX, self.lpquaCore.contents.fY, self.lpquaCore.contents.fZ)

	def __add__(self, Addend):
		if (True == isinstance(Addend, Quaternion)):
			return self.Add(Addend)
		
	def __iadd__(self, Addend):
		self = self + Addend
		return

	def __sub__(self, Subtrahend):
		if (True == isinstance(Subtrahend, Quaternion)):
			return self.Subtract(Subtrahend)

	def __isub__(self, Subtrahend):
		self = self - Subtrahend
		return

	def __mul__(self, Multiplier):
		if (True == isinstance(Multiplier, Quaternion)):
			return self.Multiply(Multiplier)
		return self.MultiplyByScalar(Multiplier)

	def __imul__(self, Multiplier):
		self = self * Multiplier
		return

	def __rshift__(self, count):
		if (True == isinstance(count, int)):
			while (count > 0):
				temp = self[0]
				self[0] = self[1]
				self[1] = self[2]
				self[2] = self[3]
				self[3] = temp
				count = count - 1
		else:
			out = self[0]
			self[0] = self[1]
			self[1] = self[2]
			self[2] = self[3]
			self[4] = 0.0
			return out
		return

	def __irshift__(self, count):
		self = self >> count
		return

	def __lshift__(self, count):
		if (True == isinstance(count, int)):
			while (count > 0):
				temp = [0]
				self[0] = self[3]
				self[3] = self[2]
				self[2] = self[1]
				self[1] = temp
				count = count - 1
		else:
			self[3] = self[2]
			self[2] = self[1]
			self[1] = self[0]
			self[0] = count
		return

	def __ilshift__(self, count):
		self = self << count
		return

	def __neg__(self):
		self.NegateThis()
		return

	def __eq__(self, other):
		return self.Equal(other)

	def __ne__(self, other):
		return not (self.Equal(other))

	def __bool__(self):
		if ((self.lpquaCore != case(NULLPTR, LPQUATERNION)) or (self.lpquaCore != 0)):
			return True
		return False

	def __len__(self):
		return 4

	def __getitem__(self, key):
		if (True == isinstance(key, int)):
			if (0 == key):
				return self.lpquaCore.contents.fW
			elif (1 == key):
				return self.lpquaCore.contents.fX
			elif (2 == key):
				return self.lpquaCore.contents.fY
			elif (3 == key):
				return self.lpquaCore.contents.fZ
			elif (-1 == key):
				return self.lpquaCore.contents.fZ
			elif (-2 == key):
				return self.lpquaCore.contents.fY
			elif (-3 == key):
				return self.lpquaCore.contents.fX
			elif (-4 == key):
				return self.lpquaCore.contents.fW
			else:
				raise ValueError("Invalid Key Index.")
		elif (True == isinstance(key, str)):
			if ("fW" == key):
				return self.lpquaCore.contents.fW
			elif ("fX" == key):
				return self.lpquaCore.contents.fX
			elif ("fY" == key):
				return self.lpquaCore.contents.fY
			elif ("fZ" == key):
				return self.lpquaCore.contents.fZ
			else:
				raise ValueError("Invalid Key Index.")			

	def __setitem__(self, key, value):
		if (not (True == isinstance(value, int) or True == isinstance(value, float))):
			raise ValueError("Must be of type float or int.")
		if (True == isinstance(key, int)):
			if (0 == key):
				self.lpquaCore.contents.fW = value
			elif (1 == key):
				self.lpquaCore.contents.fX = value
			elif (2 == key):
				self.lpquaCore.contents.fY = value
			elif (3 == key):
				self.lpquaCore.contents.fZ = value
			elif (-1 == key):
				self.lpquaCore.contents.fZ = value
			elif (-2 == key):
				self.lpquaCore.contents.fY = value
			elif (-3 == key):
				self.lpquaCore.contents.fX = value
			elif (-4 == key):
				self.lpquaCore.contents.fW = value
			else:
				raise ValueError("Invalid Key Index.")
			return
		elif (True == isinstance(key, str)):
			if ("fW" == key):
				self.lpquaCore.contents.fW = value
			elif ("fX" == key):
				self.lpquaCore.contents.fX = value
			elif ("fY" == key):
				self.lpquaCore.contents.fY = value
			elif ("fZ" == key):
				self.lpquaCore.contents.fZ = value
			else:
				raise ValueError("Invalid Key Index.")			
			return


	def Add(self, quaAddend):
		quaOut = Quaternion(0.0, 0.0, 0.0, 0.0)
		QuaternionAdd(quaOut.lpquaCore, self.lpquaCore, quaAddend.lpquaCore)
		return quaOut

	def Subtract(self, quaSubtrahend):
		quaOut = Quaternion(0.0, 0.0, 0.0, 0.0)
		QuaternionSubtract(quaOut.lpquaCore, self.lpquaCore, quaSubtrahend.lpquaCore)
		return quaOut

	def Multiply(self, quaMultiplier):
		quaOut = Quaternion(0.0, 0.0, 0.0, 0.0)
		QuaternionMultiply(quaOut.lpquaCore, self.lpquaCore, quaMultiplier.lpquaCore)
		return quaOut

	def MultiplyByScalar(self, fScalar):
		quaOut = Quaternion(0.0, 0.0, 0.0, 0.0)
		QuaternionMultiplyByScalar(quaOut.lpquaCore, self.lpquaCore, fScalar)
		return quaOut

	def Negate(self):
		quaOut = Quaternion(0.0, 0.0, 0.0, 0.0)
		QuaternionNegate(quaOut.lpquaCore, self.lpquaCore)
		return quaOut

	def NegateThis(self):
		return QuaternionNegateCurrent(self.lpquaCore)

	def DotProduct(self):
		return QuaternionDotProduct(NULLPTR, self.lpquaCore)

	def Norm(self):
		return QuaternionNorm(self.lpquaCore)

	def Normalize(self):
		return QuaternionNormalize(self.lpquaCore)

	def Inverse(self):
		quaOut = Quaternion(0.0, 0.0, 0.0, 0.0)
		QuaternionInverse(quaOut.lpquaCore, self.lpquaCore)
		return quaOut

	def UnitInverse(self):
		quaOut = Quaternion(0.0, 0.0, 0.0, 0.0)
		QuaternionUnitInverse(quaOut.lpquaCore, self.lpquaCore)
		return quaOut

	def Exp(self):
		quaOut = Quaternion(0.0, 0.0, 0.0, 0.0)
		QuaternionExp(quaOut.lpquaCore, self.lpquaCore)
		return quaOut

	def ExpFast(self):
		quaOut = Quaternion(0.0, 0.0, 0.0, 0.0)
		QuaternionExpFast(quaOut.lpquaCore, self.lpquaCore)
		return quaOut

	def Log(self):
		quaOut = Quaternion(0.0, 0.0, 0.0, 0.0)
		QuaternionLog(quaOut.lpquaCore, self.lpquaCore)
		return quaOut

	def LogFast(self):
		quaOut = Quaternion(0.0, 0.0, 0.0, 0.0)
		QuaternionLogFast(quaOut.lpquaCore, self.lpquaCore)
		return quaOut

	def Equal(self, quaEquerand):
		bRet = QuaternionEqual(self.lpquaCore, quaEquerand.lpquaCore)
		return CBoolToPyBool(bRet)

	def Slerp(self, quaQ, fDistance, bShortestPath):
		quaOut = Quaternion(0.0, 0.0, 0.0, 0.0)
		QuaternionSlerp(quaOut.lpquaCore, self.lpquaCore, quaQ.lpquaCore, fDistance, PyBoolToCBool(bShortestPath))
		return quaOut

	def SlerpExtraSpins(self, quaQ, fDistance, lExtraSpins):
		quaOut = Quaternion(0.0, 0.0, 0.0, 0.0)
		QuaternionSlerpExtraSpins(quaOut.lpquaCore, self.lpquaCore, quaQ.lpquaCore, fDistance, lExtraSpins)
		return quaOut

	def GetRoll(self, bReproject):
		return QuaternionGetRoll(self.lpquaCore, PyBoolToCBool(bReproject))

	def GetPitch(self, bReproject):
		return QuaternionGetPitch(self.lpquaCore, PyBoolToCBool(bReproject))

	def GetYaw(self, bReproject):
		return QuaternionGetYaw(self.lpquaCore, PyBoolToCBool(bReproject))
