'''



 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		PodNet.py
Author:	        Zach Podbielniak
Last Update:	01/05/2018

Overview:	This file sets forth forwarding the PodNet C API to Python, as
		well as exposing all other Python related utilties.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


'''


from PodNet.PodNetLib import *


PodNetLib.CreateSerialInterface.restype = HANDLE
PodNetLib.CreateSerialInterface.argtypes = [LPSTR, ULONG, LPVOID, ULONG]

PodNetLib.GetSerialInterfaceHandle.restype = HANDLE
PodNetLib.GetSerialInterfaceHandle.argtypes = [LPSTR]

PodNetLib.GetSerialInterfaceBaudRate.restype = ULONG
PodNetLib.GetSerialInterfaceBaudRate.argtypes = [HANDLE]

PodNetLib.WriteSerialCharacter.restype = BOOL
PodNetLib.WriteSerialCharacter.argtypes = [HANDLE, CHAR]

PodNetLib.WriteSerialString.restype = BOOL
PodNetLib.WriteSerialString.argtypes = [HANDLE, LPSTR]

PodNetLib.WriteSerial.restype = BOOL
PodNetLib.WriteSerial.argtypes = [HANDLE, LPBYTE, ULONGLONG, ULONG]

PodNetLib.WriteSerialAsync.restype = HANDLE
PodNetLib.WriteSerialAsync.argtypes = [HANDLE, LPBYTE, ULONGLONG, ULONG]

PodNetLib.SerialHasNewData.restype = ULONG
PodNetLib.SerialHasNewData.argtypes = [HANDLE]

PodNetLib.ReadSerialNextByte.restype = USHORT
PodNetLib.ReadSerialNextByte.argtypes = [HANDLE]

PodNetLib.ReadSerial.restype = BOOL
PodNetLib.ReadSerial.argtypes = [HANDLE, LPVOID, ULONGLONG, ULONG]

PodNetLib.ReadSerialAsync.restype = HANDLE
PodNetLib.ReadSerialAsync.argtypes = [HANDLE, ULONGLONG, ULONG]

PodNetLib.FlushSerial.restype = BOOL
PodNetLib.FlushSerial.argtypes = [HANDLE]


def CreateSerialInterface(lpcszDevice, ulBaud, lpReserved, ulFlags):
        return PodNetLib.CreateSerialInterface(lpcszDevice.encode('utf-8'), ulBaud, lpReserved, ulFlags)


def GetSerialInterfaceHandle(lpcszDevice):
        result = None
        result = PodNetLib.GetSerialInterfaceHandle(lpcszDevice.encode('utf-8'))
        if (result == None):
                result = GetSerialInterfaceHandle(lpcszDevice)
        return result


def GetSerialInterfaceBaudRate(hSerial):
        return PodNetLib.GetSerialInterfaceBaudRate(hPin)


def WriteSerialCharacter(hSerial, cChar):
        return PodNetLib.WriteSerialCharacter(hSerial, cChar)


def WriteSerialString(hSerial, lpcszString):
        return PodNetLib.WriteSerialString(hSerial, lpcszString)


def WriteSerial(hSerial, lpbyData, ullSize, ulFlags):
        return PodNetLib.WriteSerial(hSerial, lpbyData, ullSize, ulFlags)


def WriteSerialAsync(hSerial, lpbyData, ullSize, ulFlags):
        return PodNetLib.WriteSerialAsync(hSerial, lpbyData, ullSize, ulFlags)


def SerialHasNewData(hSerial):
        return PodNetLib.SerialHasNewData(hSerial)


def ReadSerialNextByte(hSerial):
        return PodNetLib.ReadSerialNextByte(hSerial)


def ReadSerial(hSerial, lpDataOut, ullDataSize, ulFlags):
        return PodNetLib.ReadSerial(hSerial, lpDataOut, ullDataSize, ulFlags)


def ReadSerialAsync(hSerial, ullDataSize, ulFlags):
        return PodNetLib.ReadSerialAsync(hSerial, ullDataSize, ulFlags)


def FlushSerial(hSerial):
        return PodNetLib.FlushSerial(hSerial)
