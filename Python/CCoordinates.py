'''



 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		CCoordinates.py
Author:	        Zach Podbielniak
Last Update:	01/05/18

Overview:	This file sets forth forwarding the PodNet C API to Python.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


'''



from PodNet.PodNetLib import *


class COORDINATE(Structure):
        _fields_ = [    ("dX", DOUBLE),
                        ("dY", DOUBLE),
                        ("dZ", DOUBLE),
                        ("bIs3DCoordinate", BOOL)]

LPCOORDINATE = POINTER(COORDINATE)

PodNetLib.CreateCoordinate.restype = LPCOORDINATE
PodNetLib.CreateCoordinate.argtypes = [LPCOORDINATE, DOUBLE, DOUBLE, DOUBLE, BOOL]
PodNetLib.FreeCoordinate.restype = BOOL
PodNetLib.FreeCoordinate.argtypes = [LPCOORDINATE]
PodNetLib.Distance.restype = DOUBLE
PodNetLib.Distance.argtypes = [LPCOORDINATE, LPCOORDINATE]
PodNetLib.DistanceBetweenCoordinateAndPoint.restype = DOUBLE
PodNetLib.DistanceBetweenCoordinateAndPoint.argtypes = [LPCOORDINATE, DOUBLE, DOUBLE, DOUBLE]




def CreateCoordinate(x, y):
        s = COORDINATE()
        PodNetLib.CreateCoordinate(byref(s), x, y, 0.0, FALSE)
        return s

def CreateCoordinate3D(x, z, y):
        s = COORDINATE()
        PodNetLib.CreateCoordinate(byref(s), x, y, z, TRUE)
        return s

def FreeCoordinate(Coordinate):
        return PodNetLib.FreeCoordinate(byref(Coordinate))

def Distance(CoordinateX, CoordinateY):
        return PodNetLib.Distance(byref(CoordinateX), byref(CoordinateY))

def DistanceBetweenCoordinateAndPoint(Coordinate, X, Y, Z):
        return PodNetLib.DistanceBetweenCoordinateAndPoint(byref(Coordinate), X, Y, Z)

