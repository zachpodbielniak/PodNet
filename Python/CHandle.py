'''



 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		CHandle.py
Author:	        Zach Podbielniak
Last Update:    04/29/18

Overview:	This file sets forth forwarding the PodNet C API to Python.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


'''


from PodNet.PodNetLib import *


PodNetLib.DestroyObject.restype = BOOL
PodNetLib.DestroyObject.argtypes = [HANDLE]

PodNetLib.GetTotalNumberOfObjects.restype = UARCHLONG

PodNetLib.ReleaseSingleObject.restype = BOOL
PodNetLib.ReleaseSingleObject.argtypes = [HANDLE]

PodNetLib.ReleaseMultipleObjects.restype = BOOL
PodNetLib.ReleaseMultipleObjects.argtypes = [HANDLE, ULONG]

PodNetLib.WaitForSingleObject.restype = BOOL
PodNetLib.WaitForSingleObject.argtypes = [HANDLE, ULONGLONG]

PodNetLib.WaitForMultipleObjects.restype = BOOL
PodNetLib.WaitForMultipleObjects.argtypes = [LPHANDLE, ULONG, ULONGLONG]

PodNetLib.IncrementObjectReferenceCount.restype = BOOL 
PodNetLib.IncrementObjectReferenceCount.argtypes = [HANDLE]

PodNetLib.DecrementObjectReferenceCount.restype = BOOL 
PodNetLib.DecrementObjectReferenceCount.argtypes = [HANDLE]

PodNetLib.GetObjectReferenceCount.restype = UARCHLONG
PodNetLib.GetObjectReferenceCount.argtypes = [HANDLE]



def DestroyObject(hObject):
        return PodNetLib.DestroyObject(hObject)


DestroyHandle = DestroyObject


def GetTotalNumberOfObjects():
        return PodNetLib.GetTotalNumberOfObjects()


def ReleaseSingleObject(hObject):
        return PodNetLib.ReleaseSingleObject(hObject)


def ReleaseMultipleObjects(hObjects):
        arr = (LPVOID * len(hObjects))(*Handles)
        return PodNetLib.ReleaseMultipleObjects(arr, len(hObjects))


def WaitForSingleObject(hObject, Time):
        return PodNetLib.WaitForSingleObject(hObject, Time)


def WaitForMultipleObjects(hObjects, Time):
        arr = (LPVOID * len(hObjects))(*hObjects)
        return PodNetLib.WaitForMultipleObjects(arr, len(hObjects), Time)


def IncrementObjectReferenceCount(hObject):
	return PodNetLib.IncrementObjectReferenceCount(hObject)


def DecrementObjectReferenceCount(hObject):
	return PodNetLib.DecrementObjectReferenceCount(hObject)


def GetObjectReferenceCount(hObject):
	return PodNetLib.GetObjectReferenceCount(hObject)



class Handle(Object):

	def __init__(self, hObject):
		self.hCore = hObject
		return

	def __del__(self):
		DecrementObjectReferenceCount(self.hCore)
		return
	
	def __iadd__(self, count):
		while (1 < count):
			IncrementObjectReferenceCount(self.hCore)
			count = count - 1
		return

	def __isub__(self, count):
		while (1 < count):
			DecrementObjectReferenceCount(self.hCore)
			count = count - 1
		return

	def DecrementReferenceCount(self):
		DecrementObjectReferenceCount(self.hCore)
		return

	def IncrementReferenceCount(self):
		IncrementObjectReferenceCount(self.hCore)
		return

	def GetReferenceCount(self):
		return GetObjectReferenceCount(self.hCore)

	def Wait(self, milliseconds):
		return CBoolToPyBool(WaitForSingleObject(self.hCore, milliseconds))

	def WaitForever(self):
		return self.Wait(0xFFFFFFFFFFFFFFFF)

	def Release(self):
		return CBoolToPyBool(ReleaseSingleObject(self.hCore))
