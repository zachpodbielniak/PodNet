'''



 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		CFuture.py
Author:	        Zach Podbielniak
Last Update:	01/05/18

Overview:	This file sets forth forwarding the PodNet C API to Python.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


'''


from PodNet.PodNetLib import *


PodNetLib.RetrieveFutureValue.restype = ULONG
PodNetLib.RetrieveFutureValue.argtypes = [HANDLE, LPVOID, ULONG]
PodNetLib.RetrieveFutureValueByAlloc.restype = ULONG
PodNetLib.RetrieveFutureValueByAlloc.argtypes = [HANDLE, DLPVOID]



def RetrieveFutureValue(hFuture, ulSize):
        s = create_string_buffer(b'\0' * Size)
        PodNetLib.RetrieveFutureValue(hFuture, cast(s, LPVOID), ulSize)
        return s



def RetrieveFutureValueByAlloc(hFuture):
        s = LPVOID()
        PodNetLib.RetrieveFutureValueByAlloc(hFuture, POINTER(s))
        return s