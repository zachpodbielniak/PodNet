
from PodNet.PodNetLib import *

PodNetLib.IsFloatEqual.restype = BOOL
PodNetLib.IsFloatEqual.argtypes = [FLOAT, FLOAT]

PodNetLib.IsDoubleEqual.restype = BOOL
PodNetLib.IsDoubleEqual.argtypes = [DOUBLE, DOUBLE]


def IsFloatEqual(fX, fY):
        return PodNetLib.IsFloatEqual(fX, fY)


def IsDoubleEqual(dbX, dbY):
        return PodNetLib.IsDoubleEqual(dbX, dbY)