'''



 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		CSpinLock.py
Author:	        Zach Podbielniak
Last Update:	04/30/18

Overview:	This file sets forth forwarding the PodNet C API to Python.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


'''


from PodNet.PodNetLib import *
from PodNet.CHandle import *


PodNetLib.CreateSpinLock.restype = HANDLE

PodNetLib.ReleaseSpinLock.restype = BOOL
PodNetLib.ReleaseSpinLock.argtypes = [HANDLE]


def CreateSpinLock():
        return PodNetLib.CreateSpinLock()

def ReleaseSpinLock(hSpinLock):
        return PodNetLib.ReleaseSpinLock(hSpinLock)




class SpinLock(Handle):
	
	def __init__(self):
		self.hCore = CreateSpinLock()

		if (NULLPTR == cast(self.hCore, LPVOID) or (0 == self.hCore)):
			raise ValueError("Return of CreateSpinLock() was NULL.")

		return

	def __str__(self):
		return "SPIN_LOCK_HANDLE(0x%X)" % (self.hCore)