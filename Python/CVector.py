'''



 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		CVector.py
Author:	        Zach Podbielniak
Last Update:	01/05/2018

Overview:	This file sets forth forwarding the PodNet C API to Python.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


'''


from PodNet.PodNetLib import *


HVECTOR = LPVOID


PodNetLib.CreateVector.restype = HVECTOR
PodNetLib.CreateVector.argtypes = [ULONG, ULONG, ULONG]
PodNetLib.CloseVector.restype = BOOL
PodNetLib.CloseVector.argtypes = [HVECTOR]
PodNetLib.VectorPushBack.restype = ULONG
PodNetLib.VectorPushBack.argtypes = [HVECTOR, LPVOID, ULONG]
PodNetLib.VectorAt.restype = LPVOID
PodNetLib.VectorAt.argtypes = [HVECTOR, ULONG]
PodNetLib.VectorPopBack.restype = BOOL
PodNetLib.VectorPopBack.argtypes = [HVECTOR, LPVOID]
PodNetLib.VectorShrinkToFit.restype = BOOL
PodNetLib.VectorShrinkToFit.argtypes = [HVECTOR]
PodNetLib.VectorClear.restype = BOOL
PodNetLib.VectorClear.argtypes = [HVECTOR]
PodNetLib.VectorSize.restype = ULONG
PodNetLib.VectorSize.argtypes = [HVECTOR]
PodNetLib.VectorCapacity.restype = ULONG
PodNetLib.VectorCapacity.argtypes = [HVECTOR]
PodNetLib.VectorObjectSize.restype = ULONG
PodNetLib.VectorObjectSize.argtypes = [HVECTOR]
PodNetLib.VectorResize.restype = BOOL
PodNetLib.VectorResize.argtypes = [HVECTOR, ULONG]
PodNetLib.VectorFront.restype = LPVOID
PodNetLib.VectorFront.argtypes = [HVECTOR]
PodNetLib.VectorBack.restype = LPVOID
PodNetLib.VectorBack.argtypes = [HVECTOR]


def CreateVector(ArraySize, SizeOfObject, Flags):
        return PodNetLib.CreateVector(ArraySize, SizeOfObject, Flags)

def CloseVector(Handle):
        return PodNetLib.CloseVector(Handle)

def VectorPushBack(Handle, LpParam):
        return PodNetLib.VectorPushBack(Handle, LpParam, 0)

def VectorAt(Handle, Index):
        return PodNetLib.VectorAt(Handle, Index)

def VectorPopBack(Handle, LpOut):
        return PodNetLib.VectorPopBack(Handle, LpOut)

def VectorShrinkToFit(Handle):
        return PodNetLib.VectorShrinkToFit(Handle)

def VectorClear(Handle):
        return PodNetLib.VectorClear(Handle)

def VectorSize(Handle):
        return PodNetLib.VectorSize(Handle)

def VectorCapacity(Handle):
        return PodNetLib.VectorCapacity(Handle)

def VectorObjectSize(Handle):
        return PodNetLib.VectorObjectSize(Handle)

def VectorResize(Handle, NewSize):
        return PodNetLib.VectorResize(Handle, NewSize)

def VectorFront(Handle):
        return PodNetLib.VectorFront(Handle)

def VectorBack(Handle):
        return PodNetLib.VectorBack(Handle)




class Vector(object):
        hVector = 0
        ulObjectSize = 0
        __FrontFunc = 0
        def __init__(self, InitialArraySize, SizeOfObject, Type):
                self.hVector = CreateVector(InitialArraySize, SizeOfObject, 0)
                self.ulObjectSize = SizeOfObject
                if Type == CHAR:
                        self.__FrontFunc = self.__FrontToChar
                elif Type == BYTE:
                        self.__FrontFunc = self.__FrontToByte
                elif Type == SHORT:
                        self.__FrontFunc = self.__FrontToShort
                elif Type == USHORT:
                        self.__FrontFunc = self.__FrontToUshort
                elif Type == ULONG:
                        self.__FrontFunc = self.__FrontToUlong
                elif Type == ULONGLONG:
                        self.__FrontFunc = self.__FrontToLongLong
                elif Type == LONG:
                        self.__FrontFunc = self.__FrontToLong
                elif Type == LONGLONG:
                        self.__FrontFunc = self.__FrontToLongLong
                else:
                        self.__FrontFunc = self.FrontAddress


        def Front(self):
                return self.__FrontFunc()

        def FrontAddress(self):
                return VectorFront(self.hVector)

        def __FrontToChar(self):
                x = VectorFront(self.hVector)
                p = cast(x, LPSTR)
                return p.contents

        def __FrontToByte(self):
                x = VectorFront(self.hVector)
                p = cast(x, LPBYTE)
                return p.contents

        def __FrontToShort(self):
                x = VectorFront(self.hVector)
                p = cast(x, LPSHORT)
                return p.contents

        def __FrontToUshort(self):
                x = VectorFront(self.hVector)
                p = cast(x, LPUSHORT)
                return p.contents

        def __FrontToLong(self):
                x = VectorFront(self.hVector)
                p = cast(x, LPLONG)
                return p.contents

        def __FrontToUlong(self):
                x = VectorFront(self.hVector)
                p = cast(x, LPULONG)
                return p.contents

        def __FrontToLongLong(self):
                x = VectorFront(self.hVector)
                p = cast(x, LPLONGLONG)
                return p.contents

        def __FrontToUlongLong(self):
                x = VectorFront(self.hVector)
                p = cast(x, LPULONGLONG)
                return p.contents