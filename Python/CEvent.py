'''



 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		CEvent.py
Author:	        Zach Podbielniak
Last Update:	04/30/18

Overview:	This file sets forth forwarding the PodNet C API to Python.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


'''


from PodNet.PodNetLib import *
from PodNet.CHandle import *

LPFN_EVENT_CALLBACK_PROC = CFUNCTYPE(LPVOID, LPVOID)



PodNetLib.CreateEvent.restype = HANDLE
PodNetLib.CreateEvent.argtypes = [BOOL]
PodNetLib.CreateEventEx.restype = HANDLE
PodNetLib.CreateEventEx.argtypes = [BOOL, LPFN_EVENT_CALLBACK_PROC, LPVOID]
PodNetLib.CreateBinaryEvent.restype = HANDLE
PodNetLib.SignalEvent.restype = BOOL
PodNetLib.SignalEvent.argtypes = [HANDLE]
PodNetLib.SignalEventEx.restype = BOOL
PodNetLib.SignalEventEx.argtypes = [HANDLE, LPVOID]
PodNetLib.ResetEvent.restype = BOOL
PodNetLib.ResetEvent.argtypes = [HANDLE]
PodNetLib.GetEvent.restype = BOOL
PodNetLib.GetEvent.argtypes = [HANDLE]



def CreateEvent(bState):
        return PodNetLib.CreateEvent(State)

def CreateEventEx(bState, lpfnCallback, lpParam):
        cb = LPFN_EVENT_CALLBACK_PROC(lpfnCallback)
        return PodNetLib.CreateEventEx(bState, cb, 0)

def CreateBinaryEvent():
        return PodNetLib.CreateBinaryEvent()

def SignalEvent(hEvent):
        return PodNetLib.SignalEvent(hEvent)

def SignalEventEx(hEvent, lpParam):
        return PodNetLib.SignalEventEx(hEvent, lpParam)

def ResetEvent(hEvent):
        return PodNetLib.ResetEvent(hEvent)

def GetEvent(hEvent):
        return PodNetLib.GetEvent(hEvent)





#def CallBackTest(Param):
#        print("This was from the callback")
#        q = cast(Param, LPULONGLONG).contents.value
#        print(q)
#        return 0
#
#val = ULONGLONG(42)
#x = CreateEventEx(FALSE, CallBackTest, 0)
#p = cast(addressof(val), LPVOID)
#print(p)
#SignalEventEx(x, p)


class Event(Handle):

	def __init__(self, bState):
		self.hCore = CreateEvent(PyBoolToCBool(bState))
		
		if (NULLPTR == case(self.hCore, LPVOID) or (0 == self.hCore)):
			raise ValueError("CreateEvent() returned a NULL_OBJECT")
		
		return

	def __str__(self):
		return "EVENT_HANDLE(0x%X)" % (self.hCore)

	def Signal(self):
		return CBoolToPyBool(SignalEvent(self.hCore))

	def Reset(self):
		return CBoolToPyBool(ResetEvent(self.hCore))

	def GetStatus(self):
		return CBoolToPyBool(GetEvent(self.hCore))
	
