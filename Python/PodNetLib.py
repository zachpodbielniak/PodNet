'''



 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		PodNetLib.py
Author:	        Zach Podbielniak
Last Update:	01/17/2018

Overview:	This file sets forth forwarding the PodNet C API to Python.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


'''


import os
import platform
from ctypes import *
PodNetLib = CDLL('libpodnet.so')

LPVOID = c_void_p
DLPVOID = POINTER(LPVOID)
LPSTR = c_char_p
DLPSTR = POINTER(LPSTR)


HANDLE = c_void_p
LPHANDLE = DLPVOID


CHAR = c_char
BYTE = c_ubyte
LPBYTE = POINTER(BYTE)
BOOL = c_ubyte
LPBOOL = POINTER(BOOL)
SHORT = c_short
USHORT = c_ushort
LPSHORT = POINTER(SHORT)
LPUSHORT = POINTER(USHORT)
LONG = c_int
ULONG = c_uint
LPLONG = POINTER(LONG)
LPULONG = POINTER(ULONG)
LONGLONG = c_longlong
ULONGLONG = c_ulonglong
LPLONGLONG = POINTER(LONGLONG)
LPULONGLONG = POINTER(ULONGLONG)


UARCHLONG = 0
if (platform.machine() == 'x86_64'):
	UARCHLONG = ULONGLONG
	LPUARCHLONG = POINTER(UARCHLONG)
else:
	UARCHLONG = ULONG
	LPUARCHLONG = POINTER(UARCHLONG)

FLOAT = c_float
DOUBLE = c_double
LPFLOAT = POINTER(FLOAT)
LPDOUBLE = POINTER(DOUBLE)

ATOM = USHORT

TRUE = 1
FALSE = 0
NULL = 0
NULLPTR = LPVOID(NULL)
NULL_OBJECT = cast(NULLPTR, HANDLE)



def CBoolToPyBool(bVal):
	if (FALSE == bVal):
		return False 
	return True


def PyBoolToCBool(pbVal):
	if (False == pbVal):
		return FALSE
	return True

'''
class Bool(object):
	bCore = 0
	__init__(self, bVal):
		if (TRUE == bVal or FALSE == bVal):
			bCore = bVal
			return

		if (True == bVal or False == bVal):
			bCore = PyBoolToCBool(bVal)
			return
			
		raise ValueError("Expected type BOOL or Python bool.")

	def ToBoolC(self):
		return self.bCore

	def ToBoolPy(self):
		return PyBoolToCBool(self.bCore) 
'''


class Object(object):
	hCore = 0

	def __str__(self):
		return "HANDLE(0x%X)" % (self.hCore)

	def GetCore(self):
		return self.hCore


class LightObject(object):
	def GetCore(self):
		raise NotImplementedError("This object has not overridden the GetCore virtual method.")