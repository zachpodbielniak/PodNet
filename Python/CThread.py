'''



 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		CThread.py
Author:	        Zach Podbielniak
Last Update:	01/05/18

Overview:	This file sets forth forwarding the PodNet C API to Python.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


'''


from PodNet.PodNetLib import *

LPFN_THREAD_PROC = CFUNCTYPE(LPVOID, LPVOID)


PodNetLib.CreateThread.restype = HANDLE
PodNetLib.CreateThread.argtypes = [LPFN_THREAD_PROC, LPVOID, LPVOID]

PodNetLib.KillThread.restype = BOOL
PodNetLib.KillThread.argtypes = [HANDLE]

PodNetLib.GetCurrentThread.restype = HANDLE



def CreateThread(ThreadProc, LpArgument):
        tp = LPFN_THREAD_PROC(ThreadProc)
        return PodNetLib.CreateThread(tp, LpArgument, 0)


def KillThread(Handle):
        return PodNetLib.KillThread(Handle)


def GetCurrentThread():
        return PodNetLib.GetCurrentThread()




#CreateThread example in Python.
#def ThreadTest(LpArgument):
#        s = cast(LpArgument, LPSTR).value
#        for i in range (1,100000):
#                PrintLine(str(i))
#
#lpArg = LPSTR(b"This is a passed string")
#x = CreateThread(ThreadTest, cast(lpArg, LPVOID))
#
#for i in range(-500000,-390000):
#        PrintLine(str(i))
#WaitForSingleObject(x, 10000)
