'''



 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		CLog.py
Author:	        Zach Podbielniak
Last Update:	01/12/18

Overview:	This file sets forth forwarding the PodNet C API to Python.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


'''


from PodNet.PodNetLib import *

LOG_DEBUG = 1
LOG_INFO = 2
LOG_WARNING = 3
LOG_ERROR = 4
LOG_FATAL = 5


PodNetLib.CreateLog.restype = HANDLE
PodNetLib.CreateLog.argtypes = [LPSTR, ULONG]
#PodNetLib.CreateLogFromOpenedFile.restype = HANDLE
#PodNetLib.CreateLogFromOpenedFile.argtypes = [HANDLE]
PodNetLib.WriteLog.restype = BOOL
PodNetLib.WriteLog.argtypes = [HANDLE, LPSTR, ULONG, ULONG]


def CreateLog(lpszFileName, ulFlags):
        return PodNetLib.CreateLog(lpszFileName.encode('utf-8'), ulFlags)

def CreateLogFromOpenedFile(hLog):
        return PodNetLib.CreateLogFromOpenedFile(hLog)

def WriteLog(hLog, lpszMessage, ulPriority, ulFlags):
        return PodNetLib.WriteLog(hLog, lpszMessage.encode('utf-8'), ulPriority, ulFlags)

