'''



 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		CHttpsRequest.py
Author:	        Zach Podbielniak
Last Update:    06/28/18

Overview:	This file sets forth forwarding the PodNet C API to Python.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


'''


from PodNet.PodNetLib import *



HTTP_REQUEST_INVALID = 0
HTTP_REQUEST_GET = 1
HTTP_REQUEST_PUT = 2

HTTP_VERSION_INVALID = 0
HTTP_VERSION_1_0 = 1
HTTP_VERSION_1_1 = 2


HTTP_ACCEPT_PLAIN_TEXT = "text/plain"
HTTP_ACCEPT_HTML = "text/html"

HTTP_CONNECTION_CLOSE = "close"
HTTP_CONNECTION_KEEP_ALIVE = "keep-alive"


PodNetLib.CreateHttpsRequest.restype = HANDLE
PodNetLib.CreateHttpsRequest.argtype = [ULONG, ULONG, LPSTR, USHORT, LPSTR, LPSTR, LPSTR, LPSTR, LPSTR]

PodNetLib.ExecuteHttpsRequest.restype = BOOL 
PodNetLib.ExecuteHttpsRequest.argtype = [HANDLE, LPVOID, UARCHLONG, LPUARCHLONG]


def CreateHttpsRequest(ulType, ulVersion, lpszHost, usPort, lpszPath, lpszConnectionOption, lpszUserAgent, lpszAccept, lpszOptions):
	return PodNetLib.CreateHttpsRequest(ulType, ulVersion, lpszHost.encode('utf-8'), usPort, lpszPath.encode('utf-8'), lpszConnectionOption.encode('utf-8'), lpszUserAgent.encode('utf-8'), lpszAccept.encode('utf-8'), lpszOptions.encode('utf-8'))


def ExecuteHttpsRequest(hRequest, ualSize):
	lpOut = create_string_buffer(b'\0' * (ualSize + 1))
	ualOut = UARCHLONG(0)
	PodNetLib.ExecuteHttpsRequest(hRequest, cast(lpOut, LPVOID), ualSize + 1, NULLPTR)
	return ualOut.value, lpOut
