'''



 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		CModule.py
Author:	        Zach Podbielniak
Last Update:	04/14/18

Overview:	This file sets forth forwarding the PodNet C API to Python.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


'''


from PodNet.PodNetLib import *
from PodNet.CLock import *


LPFN_MODULE_DISPATCH_PROC = CFUNCTYPE(HANDLE, LPVOID)


PodNetLib.LoadModule.restype = HANDLE
PodNetLib.LoadModule.argtypes = [LPSTR, LPVOID, LPVOID, ULONG, ULONG, LPVOID]

PodNetLib.LaunchModule.restype = LPVOID
PodNetLib.LaunchModule.argtypes = [HANDLE, LPVOID, BOOL]

PodNetLib.CallModuleProc.restype = LPVOID
PodNetLib.CallModuleProc.argtypes = [HANDLE, ULONG, LPVOID]

PodNetLib.CallModuleProcByName.restype = LPVOID
PodNetLib.CallModuleProcByName.argtypes = [HANDLE, LPSTR, LPVOID]

PodNetLib.GetModuleProc.restype = LPFN_MODULE_DISPATCH_PROC
PodNetLib.GetModuleProc.argtypes = [HANDLE, ULONG]

PodNetLib.GetModuleProcByName.restype = LPFN_MODULE_DISPATCH_PROC
PodNetLib.GetModuleProcByName.argtypes = [HANDLE, LPSTR]

PodNetLib.RegisterModuleThunk.restype = BOOL 
PodNetLib.RegisterModuleThunk.argtypes = [HANDLE, ULONG, LPSTR, LPVOID]

PodNetLib.RegisterModuleThunkByProc.restype = BOOL 
PodNetLib.RegisterModuleThunkByProc.argtypes = [HANDLE, ULONG, LPFN_MODULE_DISPATCH_PROC, LPVOID]

PodNetLib.GetModuleUserData.restype = LPVOID
PodNetLib.GetModuleUserData.argtypes = [HANDLE]

PodNetLib.SetModuleUserData.restype = BOOL 
PodNetLib.SetModuleUserData.argtypes = [HANDLE, LPVOID]



def LoadModule(lpszModuleFile, lpOnCreateParam, lpOnDestroyParam, ltLock, ulLockFlags, lpUserData):
	return PodNetLib.LoadModule(lpszModuleFile.encode('utf-8'), lpOnCreateParam, lpOnDestroyParam, ltLock, ulLockFlags, lpUserData)


def LaunchModule(hModule, lpParam, bLaunchOnSeparateThread):
	return PodNetLib.LaunchModule(hModule, lpParam, bLaunchOnSeparateThread)


def CallModuleProc(hModule, ulDescriptor, lpParam):
	return PodNetLib.CallModuleProc(hModule, ulDescriptor, lpParam)


def CallModuleProcByName(hModule, lpszProcName, lpParam):
	return PodNetLib.CallModuleProcByName(hModule, lpszProcName.encode('utf-8'), lpParam)


def GetModuleProc(hModule, ulDescriptor):
	return PodNetLib.GetModuleProc(hModule, ulDescriptor)


def GetModuleProcByName(hModule, lpszProcName):
	return PodNetLib.GetModuleProcByName(hModule, lpszProcName)


def RegisterModuleThunk(hModule, ulDescriptor, lpszProcName, lpParam):
	return PodNetLib.RegisterModuleThunk(hModule, ulDescriptor, lpszProcName, lpParam)


def RegisterModuleThunkByProc(hModule, ulDescriptor, lpfnDispatchProc, lpParam):
	return PodNetLib.RegisterModuleThunkByProc(hModule, ulDescriptor, lpfnDispatchProc, lpParam)


def GetModuleUserData(hModule):
	return PodNetLib.GetModuleUserData(hModule)


def SetModuleUserData(hModule, lpData):
	return PodNetLib.SetModuleUserData(hModule, lpData)


class Module(object):
	hModule = 0
	def __init__(self, lpszModuleFile, lpOnCreateParam, lpOnDestroyParam, lpUserData):
		self.hModule = LoadModule(lpszModuleFile, lpOnCreateParam, lpOnDestroyParam, LOCK_TYPE_MUTEX, 0, lpUserData)

	def Launch(self, lpParam, bLaunchOnSeparateThread):
		return LaunchModule(self.hModule, lpParam, bLaunchOnSeparateThread)

	def CallProc(self, ulDescriptor, lpParam):
		return CallModuleProc(self.hModule, ulDescriptor, lpParam)

	def CallProcByName(self, lpszName, lpParam):
		return CallModuleProcByName(self.hModule, lpszName, lpParam)

	def GetProc(self, ulDescriptor):
		return GetModuleProc(self.hModule, ulDescriptor)

	def GetProcByName(self, lpszName):
		return GetModuleProcByName(self.hModule, lpszName)

	def RegisterThunk(self, ulDescriptor, lpszProcName, lpParam):
		return RegisterModuleThunk(self.hModule, ulDescriptor, lpszProcName, lpParam)

	def RegisterThunkByProc(self, ulDescriptor, lpfnDispatchProc, lpParam):
		return RegisterModuleThunkByProc(self.hModule, ulDescriptor, lpfnDispatchProc, lpParam)

	def GetUserData(self):
		return GetModuleUserData(self.hModule)

	def SetUserData(self, lpData):
		return SetModuleUserData(self.hModule, lpData)
