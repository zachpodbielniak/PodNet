'''



 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		CClock.py
Author:	        Zach Podbielniak
Last Update:	01/05/18

Overview:	This file sets forth forwarding the PodNet C API to Python.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


'''


from PodNet.PodNetLib import *

class TIMESPEC(Structure):
        _fields_ = [    ("tm_sec", ULONGLONG),
                        ("tm_nsec", ULONGLONG)]

LPTIMESPEC = POINTER(TIMESPEC)



PodNetLib.Clock.restype = ULONGLONG
PodNetLib.Clock.argtypes = [LPULONGLONG]

PodNetLib.ClockAndGetNanoSeconds.restype = ULONGLONG
PodNetLib.ClockAndGetNanoSeconds.argtypes = [LPULONGLONG]

PodNetLib.ClockAndGetMicroSeconds.restype = ULONGLONG
PodNetLib.ClockAndGetMicroSeconds.argtypes = [LPULONGLONG]

PodNetLib.ClockAndGetMilliSeconds.restype = ULONGLONG
PodNetLib.ClockAndGetMilliSeconds.argtypes = [LPULONGLONG]

PodNetLib.ClockAndGetSeconds.restype = ULONGLONG
PodNetLib.ClockAndGetSeconds.argtypes = [LPULONGLONG]

PodNetLib.ClockAndGetSecondsFloat.restype = FLOAT
PodNetLib.ClockAndGetSecondsFloat.argtypes = [LPULONGLONG]

PodNetLib.CreateTimeSpec.restype = BOOL
PodNetLib.CreateTimeSpec.argtypes = [LPTIMESPEC, ULONGLONG]

PodNetLib.CreateTimeSpecNano.restype = BOOL
PodNetLib.CreateTimeSpecNano.argtypes = [LPTIMESPEC, ULONGLONG]

PodNetLib.TimeSpecToSeconds.restype = ULONGLONG
PodNetLib.TimeSpecToSeconds.argtypes = [LPTIMESPEC]

PodNetLib.TimeSpecToMilliSeconds.restype = ULONGLONG
PodNetLib.TimeSpecToMilliSeconds.argtypes = [LPTIMESPEC]

PodNetLib.TimeSpecToMicroSeconds.restype = ULONGLONG
PodNetLib.TimeSpecToMicroSeconds.argtypes = [LPTIMESPEC]

PodNetLib.TimeSpecToNanoSeconds.restype = ULONGLONG
PodNetLib.TimeSpecToNanoSeconds.argtypes = [LPTIMESPEC]

PodNetLib.HighResolutionClock.restype = BOOL
PodNetLib.HighResolutionClock.argtypes = [LPTIMESPEC]

PodNetLib.HighResolutionClockAndGetSeconds.restype = ULONGLONG
PodNetLib.HighResolutionClockAndGetSeconds.argtypes = [LPTIMESPEC]

PodNetLib.HighResolutionClockAndGetMilliSeconds.restype = ULONGLONG
PodNetLib.HighResolutionClockAndGetMilliSeconds.argtypes = [LPTIMESPEC]

PodNetLib.HighResolutionClockAndGetMicroSeconds.restype = ULONGLONG
PodNetLib.HighResolutionClockAndGetMicroSeconds.argtypes = [LPTIMESPEC]

PodNetLib.HighResolutionClockAndGetNanoSeconds.restype = ULONGLONG
PodNetLib.HighResolutionClockAndGetNanoSeconds.argtypes = [LPTIMESPEC]



def Clock():
        s = ULONGLONG()
        PodNetLib.Clock(byref(s))
        return s.value


def ClockAndGetNanoSeconds(x):
        s = ULONGLONG(x)
        return PodNetLib.ClockAndGetNanoSeconds(byref(s))


def ClockAndGetMicroSeconds(x):
        s = ULONGLONG(x)
        return PodNetLib.ClockAndGetMicroSeconds(byref(s))


def ClockAndGetMilliSeconds(x):
        s = ULONGLONG(x)
        return PodNetLib.ClockAndGetMilliSeconds(byref(s))


def ClockAndGetSeconds(x):
        s = ULONGLONG(x)
        return PodNetLib.ClockAndGetSeconds(byref(s))


def ClockAndGetSecondsFloat(x):
        s = ULONGLONG(x)
        return PodNetLib.ClockAndGetSecondsFloat(byref(s))


def CreateTimeSpec(MilliSeconds):
        s = TIMESPEC()
        PodNetLib.CreateTimeSpec(byref(s), MilliSeconds)
        return s


def CreateTimeSpecNano(NanoSeconds):
        s = TIMESPEC()
        PodNetLib.CreateTimeSpecNano(byref(s), NanoSeconds)
        return s


def TimeSpecToSeconds(TimeSpec):
        return PodNetLib.TimeSpecToSeconds(byref(TimeSpec))


def TimeSpecToMilliSeconds(TimeSpec):
        return PodNetLib.TimeSpecToMilliSeconds(byref(TimeSpec))


def TimeSpecToMicroSeconds(TimeSpec):
        return PodNetLib.TimeSpecToMicroSeconds(byref(TimeSpec))


def TimeSpecToNanoSeconds(TimeSpec):
        return PodNetLib.TimeSpecToNanoSeconds(byref(TimeSpec))


def TimeSpecToNanoSeconds(TimeSpec):
        return PodNetLib.TimeSpecToNanoSeconds(byref(TimeSpec))


def HighResolutionClock():
        s = TIMESPEC()
        PodNetLib.HighResolutionClock(byref(s))
        return s


def HighResolutionClockAndGetSeconds(TimeSpec):
        return PodNetLib.HighResolutionClockAndGetSeconds(byref(TimeSpec))


def HighResolutionClockAndGetMilliSeconds(TimeSpec):
        return PodNetLib.HighResolutionClockAndGetMilliSeconds(byref(TimeSpec))


def HighResolutionClockAndGetMicroSeconds(TimeSpec):
        return PodNetLib.HighResolutionClockAndGetMicroSeconds(byref(TimeSpec))


def HighResolutionClockAndGetNanoSeconds(TimeSpec):
        return PodNetLib.HighResolutionClockAndGetNanoSeconds(byref(TimeSpec))