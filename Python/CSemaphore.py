'''



 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		CSemaphore.py
Author:	        Zach Podbielniak
Last Update:	04/30/18

Overview:	This file sets forth forwarding the PodNet C API to Python.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


'''


from PodNet.PodNetLib import *
from PodNet.CHandle import *


PodNetLib.CreateSemaphore.restype = HANDLE
PodNetLib.CreateSemaphore.argtypes = [ULONG]

PodNetLib.ReleaseSemaphore.restype = BOOL
PodNetLib.ReleaseSemaphore.argtypes = [HANDLE]

PodNetLib.SemaphoreCount.restype = LONG
PodNetLib.SemaphoreCount.argtypes = [HANDLE]


def CreateSemaphore(ulCount):
        return PodNetLib.CreateSemaphore(ulCount)


def ReleaseSemaphore(hSemaphore):
        return PodNetLib.ReleaseSemaphore(hSemaphore)


def SemaphoreCount(hSemaphore):
        return PodNetLib.SemaphoreCount(hSemaphore)




class Semaphore(Handle):

	def __init__(self, Count):
		if (1 > Count):
			raise ValueError("Count cannot be less than 1.")
			return

		self.hCore = CreateSemaphore(Count)

		if (NULLPTR == cast(self.hCore, LPVOID) or (0 == self.hCore)):
			raise ValueError("Return of CreateMutex() was NULL.")
		
		return

	def __str__(self):
		return "SEMAPHORE_HANDLE(0x%X)" % (self.hCore)
