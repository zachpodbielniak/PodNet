'''



 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		CSocket4.py
Author:	        Zach Podbielniak
Last Update:	06/25/18

Overview:	This file sets forth forwarding the PodNet C API to Python.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


'''


from PodNet.PodNetLib import *
from PodNet.CIpv4 import *



PodNetLib.CreateSocket4.restype = HANDLE
PodNetLib.CreateSocket4.argtypes = [USHORT]

PodNetLib.CreateSocketWithAddress4.restype = HANDLE
PodNetLib.CreateSocketWithAddress4.argtypes = [USHORT, IPV4_ADDRESS]

PodNetLib.CreateSocketEx4.restype = HANDLE
PodNetLib.CreateSocketEx4.argtypes = [USHORT, IPV4_ADDRESS, LONG, LONG, LONG]

PodNetLib.DefineSocket4.restype = BOOL 
PodNetLib.DefineSocket4.argtypes = [HANDLE, USHORT, IPV4_ADDRESS]

PodNetLib.BindOnSocket4.restype = BOOL 
PodNetLib.BindOnSocket4.argtypes = [HANDLE]

PodNetLib.ListenOnBoundSocket4.restype = BOOL 
PodNetLib.ListenOnBoundSocket4.argtypes = [HANDLE, ULONG]

PodNetLib.SocketConnect4.restype = BOOL 
PodNetLib.SocketConnect4.argtypes = [HANDLE]

PodNetLib.SocketClose4.restype = BOOL 
PodNetLib.SocketClose4.argtypes = [HANDLE]

PodNetLib.SocketSend4.restype = UARCHLONG
PodNetLib.SocketSend4.argtypes = [HANDLE, LPVOID, UARCHLONG]

PodNetLib.SocketReceive4.restype = UARCHLONG
PodNetLib.SocketReceive4.argtypes = [HANDLE, LPVOID, UARCHLONG]

PodNetLib.AcceptConnection4.restype = HANDLE 
PodNetLib.AcceptConnection4.argtypes = [HANDLE] 

PodNetLib.SocketBytesInQueue4.restype = UARCHLONG
PodNetLib.SocketBytesInQueue4.argtypes = [HANDLE]

PodNetLib.GetSocketIpAddress4.restype = IPV4_ADDRESS
PodNetLib.GetSocketIpAddress4.argtypes = [HANDLE]

PodNetLib.GetSocketPort4.restype = USHORT
PodNetLib.GetSocketPort4.argtypes = [HANDLE]

PodNetLib.GetIpAddressByHost4.restype = IPV4_ADDRESS
PodNetLib.GetIpAddressByHost4.argtypes = [LPSTR]



def CreateSocket4(usPort):
	return PodNetLib.CreateSocket4(usPort)


def CreateSocketWithAddress4(usPort, ip4Address):
	return PodNetLib.CreateSocketWithAddress4(usPort, ip4Address)


def CreateSocketEx4(usPort, ip4Address, lDomain, lType, lProtocol):
	return PodNetLib.CreateSocketEx4(usPort, ip4Address, lDomain, lType, lProtocol)


def DefineSocket4(hSocket, usPort, ip4Address):
	return PodNetLib.DefineSocket4(hSocket, usPort, ip4Address)


def BindOnSocket4(hSocket):
	return PodNetLib.BindOnSocket4(hSocket)


def ListenOnBoundSocket4(hSocket, ulConnectionCount):
	return PodNetLib.ListenOnBoundSocket4(hSocket, ulConnectionCount)


def SocketConnect4(hSocket):
	return PodNetLib.SocketConnect4(hSocket)


def SocketClose4(hSocket):
	return PodNetLib.SocketClose4(hSocket)


def SocketSend4(hSocket, Data):
	lpBuffer = create_string_buffer(Data.encode('utf-8'))
	return PodNetLib.SocketSend4(hSocket, cast(lpBuffer, LPVOID), sizeof(lpBuffer))


def SocketReceive4(hSocket, ualSize):
	if (ualSize <= 1):
		return 0
	lpBuffer = create_string_buffer(b'\0' * (ualSize - 1))
	return PodNetLib.SocketReceive4(hSocket, cast(lpBuffer, LPVOID), ualSize), lpBuffer


def AcceptConnection4(hSocket):
	return PodNetLib.AcceptConnection4(hSocket)


def SocketBytesInQueue4(hSocket):
	return PodNetLib.SocketBytesInQueue4(hSocket)


def GetSocketIpAddress4(hSocket):
	return PodNetLib.GetSocketIpAddress4(hSocket)


def GetSocketPort4(hSocket):
	return PodNetLib.GetSocketPort4(hSocket)


def GetIpAddressByHost4(lpszHost):
	return PodNetLib.GetIpAddressByHost4(lpszHost.encode('utf-8'))

