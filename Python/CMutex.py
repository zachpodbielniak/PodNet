'''



 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		CMutex.py
Author:	        Zach Podbielniak
Last Update:	04/30/18

Overview:	This file sets forth forwarding the PodNet C API to Python.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


'''


from PodNet.PodNetLib import *
from PodNet.CHandle import *


PodNetLib.CreateMutex.restype = HANDLE
PodNetLib.ReleaseMutex.restype = BOOL
PodNetLib.ReleaseMutex.artypes = [HANDLE]


def CreateMutex():
        return PodNetLib.CreateMutex()

def ReleaseMutex(hMutex):
        return PodNetLib.ReleaseMutex(hMutex)

       


class Mutex(Handle):

	def __init__(self):
		self.hCore = CreateMutex()
		
		if ((NULLPTR == cast(self.hCore, LPVOID)) or (0 == self.hCore)):
			raise ValueError("CreateMutex() returned a NULL_OBJECT")

		return

	def __str__(self):
		return "MUTEX_HANDLE(0x%X)" % (self.hCore)