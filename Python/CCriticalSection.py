'''



 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		CCriticalSection.py
Author:	        Zach Podbielniak
Last Update:	04/30/18

Overview:	This file sets forth forwarding the PodNet C API to Python.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


'''


from PodNet.PodNetLib import *
from PodNet.CHandle import *


PodNetLib.CreateCriticalSection.restype = HANDLE
PodNetLib.CreateCriticalSectionAndSpecifySpinCount.restype = HANDLE
PodNetLib.CreateCriticalSectionAndSpecifySpinCount.argtypes = [ULONG]
PodNetLib.EnterCriticalSection.restype = BOOL
PodNetLib.EnterCriticalSection.argtypes = [HANDLE]
PodNetLib.TryToEnterCriticalSection.restype = BOOL
PodNetLib.TryToEnterCriticalSection.argtypes = [HANDLE]
PodNetLib.ExitCriticalSection.restype = BOOL
PodNetLib.ExitCriticalSection.argtypes = [HANDLE]


def CreateCriticalSection():
        return PodNetLib.CreateCriticalSection()

def CreateCriticalSectionAndSpecifySpinCount(ulSpinCount):
        return PodNetLib.CreateCriticalSectionAndSpecifySpinCount(ulSpinCount)

def EnterCriticalSection(hCritSec):
        return PodNetLib.EnterCriticalSection(hCritSec)

def TryToEnterCriticalSection(hCritSec):
        return PodNetLib.TryToEnterCriticalSection(hCritSec)

def ExitCriticalSection(hCritSec):
        return PodNetLib.ExitCriticalSection(hCritSec)




class CriticalSection(Handle):

	def __init__(self, SpinCount):
		if (0 == SpinCount):
			self.hCore = CreateCriticalSection()
		else:
			self.hCore = CreateCriticalSectionAndSpecifySpinCount(SpinCount)
		
		if (NULLPTR == cast(self.hCore, LPVOID) or (0 == self.hCore)):
			raise ValueError("Return of CreateCriticalSection() was NULL.")
		
		return

	def __str__(self):
		return "CRITICAL_SECTION_HANDLE(0x%X)" % (self.hCore)

	def Enter(self):
		return CBoolToPyBool(EnterCriticalSection(self.hCore))

	def TryToEnter(self):
		return CBoolToPy(TryToEnterCriticalSection(self.hCore))

	def Exit(self):
		return CBoolToPy(ExitCriticalSection(self.hCore))