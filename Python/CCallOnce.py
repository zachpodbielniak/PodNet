'''



 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		CCallOnce.py
Author:	        Zach Podbielniak
Last Update:	01/12/18

Overview:	This file sets forth forwarding the PodNet C API to Python.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


'''


from PodNet.PodNetLib import *

LPFN_ONCE_CALLABLE = CFUNCTYPE(LPVOID, LPVOID)

PodNetLib.CreateOnceCallableObject.restype = HANDLE
PodNetLib.CreateOnceCallableObject.args = [LPFN_ONCE_CALLABLE]
PodNetLib.CallExactlyOnce.restype = BOOL
PodNetLib.CallExactlyOnce.args = [HANDLE, LPVOID]


def CreateOnceCallableObject(lpfnCallback):
        cb = LPFN_ONCE_CALLABLE(lpfnCallback)
        return PodNetLib.CreateOnceCallableObject(cb)


def CallExactlyOnce(hCallOnce, lpParam):
        return PodNetLib.CallExactlyOnce(hCallOnce, lpParam)