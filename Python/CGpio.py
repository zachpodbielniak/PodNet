'''



 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		PodNet.py
Author:	        Zach Podbielniak
Last Update:	01/05/2018

Overview:	This file sets forth forwarding the PodNet C API to Python, as
		well as exposing all other Python related utilties.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


'''


from PodNet.PodNetLib import *
from PodNet.CHandle import *

GPIO_PULL_MODE_OFF = 0x00
GPIO_PULL_MODE_UP = 0x01
GPIO_PULL_MODE_DOWN = 0x02

GPIO_MODE_INPUT = 0x01
GPIO_MODE_OUTPUT = 0x02
GPIO_MODE_PWM = 0x03
GPIO_MODE_CLOCK = 0x04

GPIO_OFF = 0x00
GPIO_ON = 0x01 

GPIO_INTERLOCKED_OPERATION = 1

PodNetLib.OpenGpioPinAndSpecifyMode.restype = HANDLE
PodNetLib.OpenGpioPinAndSpecifyMode.argtypes = [ULONG, ULONG, ULONG]

PodNetLib.GetGpioPinMode.restype = ULONG
PodNetLib.GetGpioPinMode.argtypes = [HANDLE]

PodNetLib.GetGpioPullMode.restype = ULONG
PodNetLib.GetGpioPullMode.argtypes = [HANDLE]

PodNetLib.GetGpioPinHandle.restype = HANDLE
PodNetLib.GetGpioPinHandle.argtypes = [ULONG]

PodNetLib.GpioSpecifyPinMode.restype = BOOL
PodNetLib.GpioSpecifyPinMode.argtypes = [HANDLE, ULONG, ULONG]

PodNetLib.GpioChangePullMode.restype = BOOL
PodNetLib.GpioChangePullMode.argtypes = [HANDLE, ULONG]

PodNetLib.WriteGpio.restype = BOOL
PodNetLib.WriteGpio.argtypes = [HANDLE, BOOL, LPVOID, ULONG]

PodNetLib.WriteGpioAnalog.restype = BOOL
PodNetLib.WriteGpioAnalog.argtypes = [HANDLE, ULONG, LPVOID, ULONG]

PodNetLib.WriteGpioPwm.restype = BOOL
PodNetLib.WriteGpioPwm.argtypes = [HANDLE, BYTE, LPVOID, ULONG]

PodNetLib.ReadGpio.restype = BOOL
PodNetLib.ReadGpio.argtypes = [HANDLE, LPVOID, ULONG]

PodNetLib.ReadGpioAnalog.restype = ULONG
PodNetLib.ReadGpioAnalog.argtypes = [HANDLE, ULONG]


def OpenGpioPinAndSpecifyMode(ulPin, ulMode, ulPullMode):
    return PodNetLib.OpenGpioPinAndSpecifyMode(ulPin, ulMode, ulPullMode)


def GetGpioPullMode(hPin):
    return PodNetLib.GetGpioPullMode(hPin)


def GetGpioPinHandle(ulPin):
    return PodNetLib.GetGpioPinHandle(ulPin)


def GpioSpecifyPinMode(hPin, ulMode, ulPullMode):
    return PodNetLib.GpioSpecifyPinMode(hPin, ulMode, ulPullMode)


def GpioChangePullMode(hPin, ulPullMode):
    return PodNetLib.GpioChangePullMode(hPin, ulPullMode)


def GetGpioPinMode(hPin):
    return PodNetLib.GetGpioPinMode(Handle)


def WriteGpio(hPin, bValue, lpReserved, ulFlags):
    return PodNetLib.WriteGpio(hPin, bValue, lpReserved, ulFlags)


def WriteGpioAnalog(hPin, ulValue, lpReserved, ulFlags):
    return PodNetLib.WriteGpioAnalog(hPin, ulValue, lpReserved, ulFlags)


def WriteGpioPwm(hPin, byDutyCycle, lpReserved, ulFlags):
    return PodNetLib.WriteGpioPwm(hPin, byDutyCycle, lpReserved, ulFlags)


def ReadGpio(hPin, lpReserved, ulFlags):
    return PodNetLib.ReadGpio(hPin, lpReserved, ulFlags)


def ReadGpioAnalog(hPin, ulFlags):
    return PodNetLib.ReadGpioAnalog(hPin, ulFlags)





class Gpio(Handle):

	def __init__(self):
		raise NotImplemented("Gpio is an abstract class.")

	def Write(self, Data):
		raise NotImplemented("Gpio is an abstract class.")

	def Read(self):
		raise NotImplemented("Gpio is an abstract class.")


class GpioDigital(Gpio):
	bInput = 0
	ulPin = 0

	def __init__(self, Pin, bInput):
		#TODO: Add logic to handle pin input?
		if (True == bInput):
			self.hCore = OpenGpioPinAndSpecifyMode(Pin, GPIO_MODE_INPUT, NULL)
		else:
			self.hCore = OpenGpioPinAndSpecifyMode(Pin, GPIO_MODE_OUTPUT, NULL)

		if (NULLPTR == cast(self.hCore, LPVOID) or (0 == self.hCore)):
			raise ValueError("Return of OpenGpioPinAndSpecifyMode() was NULL_OBJECT.")
		
		self.bInput = bInput
		self.ulPin = ulPin
		return

	def __str__(self):
		if (True == self.bInput):
			return "GPIO_OBJECT(0x%X, %X, GPIO_MODE_INPUT)" % (self.hCore, self.ulPin)
		return "GPIO_HANDLE(0x%X, %X, GPIO_MODE_OUTPUT)" % (self.hCore, self.ulPin)

	def Write(self, Data):
		if (True == self.bInput):
			raise IOError(str(self.hCore) + " is of type GPIO_MODE_INPUT.")

		return CBoolToPyBool(WriteGpio(self.hCore, Data, NULLPTR, NULL))

	def WriteIntelocked(self, Data):
		if (True == self.bInput):
			raise IOError(str(self.hCore) + " is of type GPIO_MODE_INPUT.")
			
		return CBoolToPyBool(WriteGpio(self.hCore, Data, NULLPTR, GPIO_INTERLOCKED_OPERATION))

	


	