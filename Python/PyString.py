'''



 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		PodNet.py
Author:	        Zach Podbielniak
Last Update:	11/24/2017

Overview:	This file sets forth Python String manipulation functions

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


'''


def ConvertAsciiToUnicode(String):
        return String.decode('utf-8')

def ConvertUnicodeToAscii(String):
        return String.encode('utf-8')