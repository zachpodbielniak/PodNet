'''



 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		CLock.py
Author:	        Zach Podbielniak
Last Update:	04/14/18

Overview:	This file sets forth forwarding the PodNet C API to Python.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


'''


from PodNet.PodNetLib import *


LOCK_TYPE_INVALID = 0
LOCK_TYPE_CRITICAL_SECTION = 1
LOCK_TYPE_EVENT = 2
LOCK_TYPE_MUTEX = 3
LOCK_TYPE_SEMAPHORE = 4
LOCK_TYPE_SPIN_LOCK = 5