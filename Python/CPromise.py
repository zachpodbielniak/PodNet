'''



 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		CPromise.py
Author:	        Zach Podbielniak
Last Update:	01/05/18

Overview:	This file sets forth forwarding the PodNet C API to Python.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


'''


from PodNet.PodNetLib import *


LPFN_PROMISE_PROC = CFUNCTYPE(ULONGLONG, LPVOID, DLPVOID)


PodNetLib.CreatePromise.restype = HANDLE
PodNetLib.CreatePromise.argtypes = [LPFN_PROMISE_PROC, LPVOID, LPVOID, DLPVOID, ULONG]
PodNetLib.GetFuture.restype = HANDLE
PodNetLib.GetFuture.argtypes = [HANDLE]

def CreatePromise(Callback, LpParam, Flags):
        cb = LPFN_PROMISE_PROC(Callback)
        return PodNetLib.CreatePromise(cb, LpParam, 0, 0, Flags)


def GetFuture(Handle):
        return PodNetLib.GetFuture(Handle)