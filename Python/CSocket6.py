'''



 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		CSocket6.py
Author:	        Zach Podbielniak
Last Update:	06/25/18

Overview:	This file sets forth forwarding the PodNet C API to Python.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


'''


from PodNet.PodNetLib import *
from PodNet.CIpv6 import *



PodNetLib.CreateSocket6.restype = HANDLE
PodNetLib.CreateSocket6.argtypes = [USHORT]

PodNetLib.CreateSocketWithAddress6.restype = HANDLE
PodNetLib.CreateSocketWithAddress6.argtypes = [USHORT, LPIPV6_ADDRESS]

PodNetLib.CreateSocketEx6.restype = HANDLE
PodNetLib.CreateSocketEx6.argtypes = [USHORT, LPIPV6_ADDRESS, LONG, LONG, LONG]

PodNetLib.DefineSocket6.restype = BOOL 
PodNetLib.DefineSocket6.argtypes = [HANDLE, USHORT, LPIPV6_ADDRESS]

PodNetLib.BindOnSocket6.restype = BOOL 
PodNetLib.BindOnSocket6.argtypes = [HANDLE]

PodNetLib.ListenOnBoundSocket6.restype = BOOL 
PodNetLib.ListenOnBoundSocket6.argtypes = [HANDLE, ULONG]

PodNetLib.SocketConnect6.restype = BOOL 
PodNetLib.SocketConnect6.argtypes = [HANDLE]

PodNetLib.SocketClose6.restype = BOOL 
PodNetLib.SocketClose6.argtypes = [HANDLE]

PodNetLib.SocketSend6.restype = UARCHLONG
PodNetLib.SocketSend6.argtypes = [HANDLE, LPVOID, UARCHLONG]

PodNetLib.SocketReceive6.restype = UARCHLONG
PodNetLib.SocketReceive6.argtypes = [HANDLE, LPVOID, UARCHLONG]

PodNetLib.AcceptConnection6.restype = HANDLE 
PodNetLib.AcceptConnection6.argtypes = [HANDLE] 

PodNetLib.SocketBytesInQueue6.restype = UARCHLONG
PodNetLib.SocketBytesInQueue6.argtypes = [HANDLE]

PodNetLib.GetSocketIpAddress6.restype = LPIPV6_ADDRESS
PodNetLib.GetSocketIpAddress6.argtypes = [HANDLE]

PodNetLib.GetSocketPort6.restype = USHORT
PodNetLib.GetSocketPort6.argtypes = [HANDLE]

PodNetLib.GetIpAddressByHost6.restype = LPIPV6_ADDRESS
PodNetLib.GetIpAddressByHost6.argtypes = [LPSTR]



def CreateSocket6(usPort):
	return PodNetLib.CreateSocket6(usPort)


def CreateSocketWithAddress6(usPort, ip6Address):
	return PodNetLib.CreateSocketWithAddress6(usPort, ip6Address)


def CreateSocketEx6(usPort, ip6Address, lDomain, lType, lProtocol):
	return PodNetLib.CreateSocketEx6(usPort, ip6Address, lDomain, lType, lProtocol)


def DefineSocket6(hSocket, usPort, ip6Address):
	return PodNetLib.DefineSocket6(hSocket, usPort, ip6Address)


def BindOnSocket6(hSocket):
	return PodNetLib.BindOnSocket6(hSocket)


def ListenOnBoundSocket6(hSocket, ulConnectionCount):
	return PodNetLib.ListenOnBoundSocket6(hSocket, ulConnectionCount)


def SocketConnect6(hSocket):
	return PodNetLib.SocketConnect6(hSocket)


def SocketClose6(hSocket):
	return PodNetLib.SocketClose6(hSocket)


def SocketSend6(hSocket, Data):
	lpBuffer = create_string_buffer(Data.encode('utf-8'))
	return PodNetLib.SocketSend6(hSocket, cast(lpBuffer, LPVOID), sizeof(lpBuffer))


def SocketReceive6(hSocket, ualSize):
	if (ualSize <= 1):
		return 0
	lpBuffer = create_string_buffer(b'\0' * (ualSize - 1))
	return PodNetLib.SocketReceive6(hSocket, cast(lpBuffer, LPVOID), ualSize), lpBuffer


def AcceptConnection6(hSocket):
	return PodNetLib.AcceptConnection6(hSocket)


def SocketBytesInQueue6(hSocket):
	return PodNetLib.SocketBytesInQueue6(hSocket)


def GetSocketIpAddress6(hSocket):
	return PodNetLib.GetSocketIpAddress6(hSocket)


def GetSocketPort6(hSocket):
	return PodNetLib.GetSocketPort6(hSocket)


def GetIpAddressByHost6(lpszHost):
	return PodNetLib.GetIpAddressByHost6(lpszHost.encode('utf-8'))


