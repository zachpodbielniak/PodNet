'''



 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		CAtomTable.py
Author:	        Zach Podbielniak
Last Update:	01/12/18

Overview:	This file sets forth forwarding the PodNet C API to Python.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


'''


from PodNet.PodNetLib import *


PodNetLib.CreateAtomTable.restype = HANDLE
PodNetLib.CreateAtomTable.argtypes = [USHORT, USHORT]

PodNetLib.PushAtom.restype = ATOM
PodNetLib.PushAtom.argtypes = [HANDLE, LPSTR]

PodNetLib.GetAtomValue.restype = BOOL
PodNetLib.GetAtomValue.argtypes = [HANDLE, ATOM, LPSTR]

PodNetLib.PushGlobalAtom.restype = ATOM
PodNetLib.PushGlobalAtom.argtypes = [LPSTR]

PodNetLib.GetGlobalAtomValue.restype = BOOL
PodNetLib.GetGlobalAtomValue.argtypes = [ATOM, LPSTR]



def CreateAtomTable(usBufferSize, usMaxAtoms):
        return PodNetLib.CreateAtomTable(usBufferSize, usMaxAtoms)


def PushAtom(hAtomTable, lpszAtomMessage):
        return PodNetLib.PushAtom(hAtomTable, lpszAtomMessage.encode('utf-8'))

def GetAtomValue(hAtomTable, atmAtom):
        s = create_string_buffer(b'\0' * 1024)
        PodNetLib.GetAtomValue(hAtomTable, atmAtom, s)
        return s.value

def PushGlobalAtom(lpszAtomMessage):
        return PodNetLib.PushGlobalAtom(lpszAtomMessage.encode('utf-8'))

def GetGlobalAtomValue(atmAtom):
        s = create_string_buffer(b'\0' * 256)
        PodNetLib.GetGlobalAtomValue(atmAtom, s)
        return s.value

