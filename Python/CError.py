'''



 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		CError.py
Author:	        Zach Podbielniak
Last Update:	01/12/18

Overview:	This file sets forth forwarding the PodNet C API to Python.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


'''


from PodNet.PodNetLib import *

PodNetLib.GetLastGlobalError.restype = ULONG
PodNetLib.SetLastGlobalError.restype = BOOL
PodNetLib.SetLastGlobalError.argtypes = [ULONG]


def GetLastGlobalError():
        return PodNetLib.GetLastGlobalError()

def SetLastGlobalError(ulError):
        return PodNetLib.SetLastGlobalError(ulError)