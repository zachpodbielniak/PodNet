'''



 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		CAlgorithms.py
Author:	        Zach Podbielniak
Last Update:	01/12/18

Overview:	This file sets forth forwarding the PodNet C API to Python.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


'''

from PodNet.PodNetLib import *


PodNetLib.Sha256.restype = BOOL
PodNetLib.Sha256.argtypes = [LPSTR, ULONG, LPSTR]


#To get a LPBYTE cast(s.value, POINTER(c_ubyte))
def Sha256(lpbyData):
        s = create_string_buffer(b'\0'*64)
        p = cast(s, POINTER(c_ubyte))
        PodNetLib.Sha256(lpbyData.encode('utf-8'), len(lpbyData), s)
        return p