'''



 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		CVector3D.py
Author:	        Zach Podbielniak
Last Update:	04/29/2018

Overview:	This file sets forth forwarding the PodNet C API to Python.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


'''


from PodNet.PodNetLib import *


class VECTOR3D(Structure):
	_fields_ = [	("fX", FLOAT),
			("fY", FLOAT),
			("fZ", FLOAT)]


LPVECTOR3D = POINTER(VECTOR3D)


PodNetLib.CreateVector3D.restype = LPVECTOR3D
PodNetLib.CreateVector3D.argtypes = [FLOAT, FLOAT, FLOAT]

PodNetLib.CreateVector3DFromVector3D.restype = LPVECTOR3D
PodNetLib.CreateVector3DFromVector3D.argtypes = [LPVECTOR3D]

PodNetLib.DestroyVector3D.restype = BOOL 
PodNetLib.DestroyVector3D.argtypes = [LPVECTOR3D]

PodNetLib.Vector3DAdd.restype = LPVECTOR3D
PodNetLib.Vector3DAdd.argtypes = [LPVECTOR3D, LPVECTOR3D, LPVECTOR3D]

PodNetLib.Vector3DSubtract.restype = LPVECTOR3D
PodNetLib.Vector3DSubtract.argtypes = [LPVECTOR3D, LPVECTOR3D, LPVECTOR3D]

PodNetLib.Vector3DMultiply.restype = LPVECTOR3D
PodNetLib.Vector3DMultiply.argtypes = [LPVECTOR3D, LPVECTOR3D, LPVECTOR3D]

PodNetLib.Vector3DDivide.restype = LPVECTOR3D
PodNetLib.Vector3DDivide.argtypes = [LPVECTOR3D, LPVECTOR3D, LPVECTOR3D]

PodNetLib.Vector3DAddScalar.restype = LPVECTOR3D
PodNetLib.Vector3DAddScalar.argtypes = [LPVECTOR3D, LPVECTOR3D, FLOAT]

PodNetLib.Vector3DSubtractScalar.restype = LPVECTOR3D
PodNetLib.Vector3DSubtractScalar.argtypes = [LPVECTOR3D, LPVECTOR3D, FLOAT]

PodNetLib.Vector3DMultiplyScalar.restype = LPVECTOR3D
PodNetLib.Vector3DMultiplyScalar.argtypes = [LPVECTOR3D, LPVECTOR3D, FLOAT]

PodNetLib.Vector3DDivideScalar.restype = LPVECTOR3D
PodNetLib.Vector3DDivideScalar.argtypes = [LPVECTOR3D, LPVECTOR3D, FLOAT]

PodNetLib.Vector3DLength.restype = FLOAT 
PodNetLib.Vector3DLength.argtypes = [LPVECTOR3D]

PodNetLib.Vector3DLengthSquared.restype = FLOAT 
PodNetLib.Vector3DLengthSquared.argtypes = [LPVECTOR3D]

PodNetLib.Vector3DIsZeroLength.restype = BOOL 
PodNetLib.Vector3DIsZeroLength.argtypes = [LPVECTOR3D]

PodNetLib.Vector3DDistance.restype = FLOAT 
PodNetLib.Vector3DDistance.argtypes = [LPVECTOR3D, LPVECTOR3D]

PodNetLib.Vector3DDistanceSquared.restype = FLOAT 
PodNetLib.Vector3DDistanceSquared.argtypes = [LPVECTOR3D, LPVECTOR3D]

PodNetLib.Vector3DDotProduct.restype = FLOAT 
PodNetLib.Vector3DDotProduct.argtypes = [LPVECTOR3D, LPVECTOR3D]

PodNetLib.Vector3DDotProductAbsolute.restype = FLOAT 
PodNetLib.Vector3DDotProductAbsolute.argtypes = [LPVECTOR3D, LPVECTOR3D]

PodNetLib.Vector3DNormalize.restype = FLOAT 
PodNetLib.Vector3DNormalize.argtypes = [LPVECTOR3D, LPVECTOR3D]

PodNetLib.Vector3DCrossProduct.restype = LPVECTOR3D
PodNetLib.Vector3DCrossProduct.argtypes = [LPVECTOR3D, LPVECTOR3D, LPVECTOR3D]

PodNetLib.Vector3DMidPoint.restype = LPVECTOR3D
PodNetLib.Vector3DMidPoint.argtypes = [LPVECTOR3D, LPVECTOR3D, LPVECTOR3D]

PodNetLib.Vector3DLessThan.restype = BOOL 
PodNetLib.Vector3DLessThan.argtypes = [LPVECTOR3D, LPVECTOR3D]

PodNetLib.Vector3DGreaterThan.restype = BOOL
PodNetLib.Vector3DGreaterThan.argtypes = [LPVECTOR3D, LPVECTOR3D]

PodNetLib.Vector3DEqual.restype = BOOL
PodNetLib.Vector3DEqual.argtypes = [LPVECTOR3D, LPVECTOR3D]

PodNetLib.Vector3DPerpendicular.restype = LPVECTOR3D
PodNetLib.Vector3DPerpendicular.argtypes = [LPVECTOR3D, LPVECTOR3D]

PodNetLib.Vector3DAngleBetween.restype = FLOAT 
PodNetLib.Vector3DAngleBetween.argtypes = [LPVECTOR3D, LPVECTOR3D]

PodNetLib.Vector3DAngleBetweenFast.restype = FLOAT 
PodNetLib.Vector3DAngleBetweenFast.argtypes = [LPVECTOR3D, LPVECTOR3D]

PodNetLib.Vector3DReflect.restype = LPVECTOR3D
PodNetLib.Vector3DReflect.argtypes = [LPVECTOR3D, LPVECTOR3D, LPVECTOR3D]

PodNetLib.Vector3DPrimaryAxis.restype = LPVECTOR3D
PodNetLib.Vector3DPrimaryAxis.argtypes = [LPVECTOR3D, LPVECTOR3D]

PodNetLib.Vector3DPositionEquals.restype = BOOL 
PodNetLib.Vector3DPositionEquals.argtypes = [LPVECTOR3D, LPVECTOR3D]

PodNetLib.Vector3DIsNotANumber.restype = BOOL 
PodNetLib.Vector3DIsNotANumber.argtypes = [LPVECTOR3D]




def CreateVector3D(fX, fY, fZ):
	return PodNetLib.CreateVector3D(fX, fY, fZ)


def CreateVector3DFromVector3D(lpv3Vec):
	return PodNetLib.CreateVector3DFromVector3D(lpv3Vec)


def DestroyVector3D(lpv3Vec):
	return PodNetLib.DestroyVector3D(lpv3Vec)


def Vector3DAdd(lpv3Out, lpv3Vec1, lpv3Vec2):
	if (NULLPTR == lpv3Out):
		return PodNetLib.Vector3DAdd(cast(NULLPTR, LPVECTOR3D), lpv3Vec1, lpv3Vec2)
	b = PodNetLib.Vector3DAdd(lpv3Out, lpv3Vec1, lpv3Vec2)
	return BOOL(cast(b, LPVOID).value).value


def Vector3DSubtract(lpv3Out, lpv3Vec1, lpv3Vec2):
	if (NULLPTR == lpv3Out):
		return PodNetLib.Vector3DSubtract(cast(NULLPTR, LPVECTOR3D), lpv3Vec1, lpv3Vec2)
	b = PodNetLib.Vector3DSubtract(lpv3Out, lpv3Vec1, lpv3Vec2)
	return BOOL(cast(b, LPVOID).value).value


def Vector3DMultiply(lpv3Out, lpv3Vec1, lpv3Vec2):
	if (NULLPTR == lpv3Out):
		return PodNetLib.Vector3DMultiply(cast(NULLPTR, LPVECTOR3D), lpv3Vec1, lpv3Vec2)
	b = PodNetLib.Vector3DMultiply(lpv3Out, lpv3Vec1, lpv3Vec2)
	return BOOL(cast(b, LPVOID).value).value


def Vector3DDivide(lpv3Out, lpv3Vec1, lpv3Vec2):
	if (NULLPTR == lpv3Out):
		return PodNetLib.Vector3DDivide(cast(NULLPTR, LPVECTOR3D), lpv3Vec1, lpv3Vec2)
	b = PodNetLib.Vector3DDivide(lpv3Out, lpv3Vec1, lpv3Vec2)
	return BOOL(cast(b, LPVOID).value).value


def Vector3DAddScalar(lpv3Out, lpv3Vec, fScalar):
	if (NULLPTR == lpv3Out):
		return PodNetLib.Vector3DAddScalar(cast(NULLPTR, LPVECTOR3D), lpv3Vec, fScalar)
	b = PodNetLib.Vector3DAddScalar(lpv3Out, lpv3Vec, fScalar)
	return BOOL(cast(b, LPVOID).value).value


def Vector3DSubtractScalar(lpv3Out, lpv3Vec, fScalar):
	if (NULLPTR == lpv3Out):
		return PodNetLib.Vector3DSubtractScalar(cast(NULLPTR, LPVECTOR3D), lpv3Vec, fScalar)
	b = PodNetLib.Vector3DSubtractScalar(lpv3Out, lpv3Vec, fScalar)
	return BOOL(cast(b, LPVOID).value).value


def Vector3DMultiplyScalar(lpv3Out, lpv3Vec, fScalar):
	if (NULLPTR == lpv3Out):
		return PodNetLib.Vector3DMultiplyScalar(cast(NULLPTR, LPVECTOR3D), lpv3Vec, fScalar)
	b = PodNetLib.Vector3DMultiplyScalar(lpv3Out, lpv3Vec, fScalar)
	return BOOL(cast(b, LPVOID).value).value


def Vector3DDivideScalar(lpv3Out, lpv3Vec, fScalar):
	if (NULLPTR == lpv3Out):
		return PodNetLib.Vector3DDivideScalar(cast(NULLPTR, LPVECTOR3D), lpv3Vec, fScalar)
	b = PodNetLib.Vector3DDivideScalar(lpv3Out, lpv3Vec, fScalar)
	return BOOL(cast(b, LPVOID).value).value


def Vector3DLength(lpv3Vec):
	return PodNetLib.Vector3DLength(lpv3Vec)


def Vector3DLengthSquared(lpv3Vec):
	return PodNetLib.Vector3DLengthSquared(lpv3Vec)


def Vector3DIsZeroLength(lpv3Vec):
	return PodNetLib.Vector3DIsZeroLength(lpv3Vec)


def Vector3DDistance(lpv3Vec1, lpv3Vec2):
	return PodNetLib.Vector3DDistance(lpv3Vec1, lpv3Vec2)


def Vector3DDistanceSquared(lpv3Vec1, lpv3Vec2):
	return PodNetLib.Vector3DDistanceSquared(lpv3Vec1, lpv3Vec2)


def Vector3DDotProduct(lpv3Vec1, lpv3Vec2):
	return PodNetLib.Vector3DDotProduct(lpv3Vec1, lpv3Vec2)


def Vector3DDotProductAbsolute(lpv3Vec1, lpv3Vec2):
	return PodNetLib.Vector3DDotProductAbsolute(lpv3Vec1, lpv3Vec2)


def Vector3DNormalize(lpv3Out, lpv3Vec):
	if (NULLPTR == lpv3Out):
		return PodNetLib.Vector3DNormalize(cast(NULLPTR, LPVECTOR3D), lpv3Vec)
	b = PodNetLib.Vector3DNormalize(lpv3Out, lpv3Vec)
	return BOOL(cast(b, LPVOID).value).value


def Vector3DCrossProduct(lpv3Out, lpv3Vec1, lpv3Vec2):
	if (NULLPTR == lpv3Out):
		return PodNetLib.Vector3DCrossProduct(cast(NULLPTR, LPVECTOR3D), lpv3Vec1, lpv3Vec2)
	b = PodNetLib.Vector3DCrossProduct(lpv3Out, lpv3Vec1, lpv3Vec2)
	return BOOL(cast(b, LPVOID).value).value

def Vector3DMidPoint(lpv3Out, lpv3Vec1, lpv3Vec2):
	if (NULLPTR == lpv3Out):
		return PodNetLib.Vector3DMidPoint(cast(NULLPTR, LPVECTOR3D), lpv3Vec1, lpv3Vec2)
	b = PodNetLib.Vector3DMidPoint(lpv3Out, lpv3Vec1, lpv3Vec2)
	return BOOL(cast(b, LPVOID).value).value


def Vector3DLessThan(lpv3Left, lpv3Right):
	return PodNetLib.Vector3DLessThan(lpv3Left, lpv3Right)


def Vector3DGreaterThan(lpv3Left, lpv3Right):
	return PodNetLib.Vector3DGreaterThan(lpv3Left, lpv3Right)


def Vector3DEqual(lpv3Left, lpv3Right):
	return PodNetLib.Vector3DEqual(lpv3Left, lpv3Right)


def Vector3DPerpendicular(lpv3Out, lpv3Vec):
	if (NULLPTR == lpv3Out):
		return PodNetLib.Vector3DPerpendicular(cast(NULLPTR, LPVOID), lpv3Vec)
	b = PodNetLib.Vector3DPerpendicular(lpv3Out, lpv3Vec)
	return BOOL(cast(b, LPVOID).value).value


def Vector3DAngleBetween(lpv3Vec1, lpv3Vec2):
	return PodNetLib.Vector3DAngleBetween(lpv3Vec1, lpv3Vec2)


def Vector3DAngleBetweenFast(lpv3Vec1, lpv3Vec2):
	return PodNetLib.Vector3DAngleBetweenFast(lpv3Vec1, lpv3Vec2)


def Vector3DReflect(lpv3Out, lpv3Vec1, lpv3Vec2):
	if (NULLPTR == lpv3Out):
		return PodNetLib.Vector3DReflect(cast(NULLPTR, LPVECTOR3D), lpv3Vec1, lpv3Vec2)
	b = PodNetLib.Vector3DReflect(lpv3Out, lpv3Vec1, lpv3Vec2)
	return BOOL(cast(b, LPVOID).value).value


def Vector3DPrimaryAxis(lpv3Out, lpv3Vec):
	if (NULLPTR == lpv3Out):
		return PodNetLib.Vector3DPrimaryAxis(cast(NULLPTR, LPVOID), lpv3Vec)
	b = PodNetLib.Vector3DPrimaryAxis(lpv3Out, lpv3Vec)
	return BOOL(cast(b, LPVOID).value).value


def Vector3DPositionEquals(lpv3Vec1, lpv3Vec2):
	return PodNetLib.Vector3DPositionEquals(lpv3Vec1, lpv3Vec2)


def Vector3DIsNotANumber(lpv3Vec):
	return PodNetLib.Vector3DIsNotANumber(lpv3Vec)




class Vector3D(object):
	lpv3Core = 0
	def __init__(self, fX, fY, fZ):
		self.lpv3Core = CreateVector3D(fX, fY, fZ)

	def __del__(self):
		DestroyVector3D(self.lpv3Core)

	def __str__(self):
		return "Vector3D(%f, %f, %f)" % (self.lpv3Core.contents.fX, self.lpv3Core.contents.fY, self.lpv3Core.contents.fZ)

	def __add__(self, Addend):
		if (True == isinstance(Addend, Vector3D)):
			return self.Add(Addend)
		return self.AddScalar(Addend)

	def __iadd__(self, Addend):
		self = self + Addend
		return

	def __sub__(self, Subtrahend):
		if (True == isinstance(Subtrahend, Vector3D)):
			return self.Subtract(Subtrahend)
		return self.SubtractScalar(Subtrahend)

	def __isub__(self, Subtrahend):
		self = self - Subtrahend
		return

	def __mul__(self, Multiplier):
		if (True == isinstance(Multiplier, Vector3D)):
			return self.Multiply(Multiplier)
		return self.MultiplyScalar(Multiplier)

	def __imul__(self, Multiplier):
		self = self * Multiplier
		return

	def __truediv__(self, Divisor):
		if (True == isinstance(Divisor, Vector3D)):
			return self.Divide(Divisor)
		return self.DivideScalar(Divisor)

	def __itruediv__(self, Divisor):
		self = self / Divisor
		return

	def __rshift__(self, count):
		if (True == isinstance(count, int)):
			while (count > 0):
				temp = self[0]
				self[0] = self[1]
				self[1] = self[2]
				self[2] = temp
				count = count - 1
		else:
			out = self[0]
			self[0] = self[1]
			self[1] = self[2]
			self[2] = 0.0
			return out
		return

	def __irshift__(self, count):
		self = self >> count
		return

	def __lshift__(self, count):
		if (True == isinstance(count, int)):
			while (count > 0):
				temp = self[0]
				self[0] = self[2]
				self[2] = self[1]
				self[1] = temp
				count = count - 1
		else:
			self[2] = self[1]
			self[1] = self[0]
			self[0] = count
		return

	def __ilshift__(self, count):
		self = self << count
		return

	def __neg__(self):
		self[0] = -(self[0])
		self[1] = -(self[1])
		self[2] = -(self[2])
		return

	def __eq__(self, other):
		return self.Equal(other)

	def __ne__(self, other):
		return not(self.Equal(other))

	def __lt__(self, other):
		return self.LessThan(other)

	def __le__(self, other):
		return (self.LessThan(other) or self.Equal(other))

	def __gt__(self, other):
		return self.GreaterThan(other)
	
	def __ge__(self, other):
		return (self.GreaterThan(other) or self.Equal(other))

	def __bool__(self):
		if ((self.lpv3Core != cast(NULLPTR, LPVECTOR3D)) or (self.lpv3 != 0)):
			return True
		return False

	def __len__(self):
		return 3

	def __getitem__(self, key):
		if (True == isinstance(key, int)):
			if (0 == key):
				return self.lpv3Core.contents.fX
			elif (1 == key):
				return self.lpv3Core.contents.fY
			elif (2 == key):
				return self.lpv3Core.contents.fZ
			elif (-1 == key):
				return self.lpv3Core.contents.fZ
			elif (-2 == key):
				return self.lpv3Core.contents.fY
			elif (-3 == key):
				return self.lpv3Core.contents.fX
			else:
				raise ValueError("Invalid Key Index.")
		elif (True == isinstance(key, str)):
			if ("fX" == key):
				return self.lpv3Core.contents.fX
			elif ("fY" == key):
				return self.lpv3Core.contents.fY
			elif ("fZ" == key):
				return self.lpv3Core.contents.fZ
			else:
				raise ValueError("Invalid Key Index.")			

	def __setitem__(self, key, value):
		if (not (True == isinstance(value, int) or True == isinstance(value, float))):
			raise ValueError("Must be of type float or int.")
		if (True == isinstance(key, int)):
			if (0 == key):
				self.lpv3Core.contents.fX = value
			elif (1 == key):
				self.lpv3Core.contents.fY = value
			elif (2 == key):
				self.lpv3Core.contents.fZ = value
			elif (-1 == key):
				self.lpv3Core.contents.fZ = value
			elif (-2 == key):
				self.lpv3Core.contents.fY = value
			elif (-3 == key):
				self.lpv3Core.contents.fX = value
			else:
				raise ValueError("Invalid Key Index.")
			return
		elif (True == isinstance(key, str)):
			if ("fX" == key):
				self.lpv3Core.contents.fX = value
			elif ("fY" == key):
				self.lpv3Core.contents.fY = value
			elif ("fZ" == key):
				self.lpv3Core.contents.fZ = value
			else:
				raise ValueError("Invalid Key Index.")			
			return

	def FromOtherVector3D(self, v3Other):
		self.lpv3Core = CreateVector3DFromVector3D(v3Other.lpv3Core)

	def Add(self, v3Other):
		v3Out = Vector3D(0.0, 0.0, 0.0)
		Vector3DAdd(v3Out.lpv3Core, self.lpv3Core, v3Other.lpv3Core)
		return v3Out

	def Subtract(self, v3Other):
		v3Out = Vector3D(0.0, 0.0, 0.0)
		Vector3DSubtract(v3Out.lpv3Core, self.lpv3Core, v3Other.lpv3Core)
		return v3Out

	def Multiply(self, v3Other):
		v3Out = Vector3D(0.0, 0.0, 0.0)
		Vector3DMultiply(v3Out.lpv3Core, self.lpv3Core, v3Other.lpv3Core)
		return v3Out

	def Divide(self, v3Other):
		v3Out = Vector3D(0.0, 0.0, 0.0)
		Vector3DDivide(v3Out.lpv3Core, self.lpv3Core, v3Other.lpv3Core)
		return v3Out

	def AddScalar(self, fScalar):
		v3Out = Vector3D(0.0, 0.0, 0.0)
		Vector3DAddScalar(v3Out.lpv3Core, self.lpv3Core, fScalar)
		return v3Out

	def SubtractScalar(self, fScalar):
		v3Out = Vector3D(0.0, 0.0, 0.0)
		Vector3DSubtractScalar(v3Out.lpv3Core, self.lpv3Core, fScalar)
		return v3Out

	def MultiplyScalar(self, fScalar):
		v3Out = Vector3D(0.0, 0.0, 0.0)
		Vector3DMultiplyScalar(v3Out.lpv3Core, self.lpv3Core, fScalar)
		return v3Out

	def DivideScalar(self, fScalar):
		v3Out = Vector3D(0.0, 0.0, 0.0)
		Vector3DDivideScalar(v3Out.lpv3Core, self.lpv3Core, fScalar)
		return v3Out

	def Length(self):
		return Vector3DLength(self.lpv3Core)

	def LengthSquared(self):
		return Vector3DLengthSquared(self.lpv3Core)

	def IsZeroLength(self):
		return CBoolToPyBool(Vector3DIsZeroLength(self.lpv3Core))

	def Distance(self, v3Other):
		return Vector3DDistance(self.lpv3Core, v3Other.lpv3Core)

	def DistanceSquared(self, v3Other):
		return Vector3DDistanceSquared(self.lpv3Core, v3Other.lpv3Core)

	def DotProduct(self, v3Other):
		return Vector3DDotProduct(self.lpv3Core, v3Other.lpv3Core)

	def DotProductAbsolute(self, v3Other):
		return Vector3DDotProductAbsolute(self.lpv3Core, v3Other.lpv3Core)

	def Normalize(self, v3Out):
		return PodNetLib.Normalize(v3Out.lpv3Core, self.lpv3Core)

	def CrossProduct(self, v3Other):
		v3Out = Vector3D(0.0, 0.0, 0.0)
		Vector3DCrossProduct(v3Out.lpv3Core, self.lpv3Core, v3Other.lpv3Core)
		return v3Out

	def MidPoint(self, v3Other):
		v3Out = Vector3D(0.0, 0.0, 0.0)
		Vector3DMidPoint(v3Out.lpv3Core, self.lpv3Core, v3Other.lpv3Core)
		return v3Out
	
	def LessThan(self, v3Other):
		return Vector3DLessThan(self.lpv3Core, v3Other.lpv3Core)

	def GreaterThan(self, v3Other):
		return Vector3DGreaterThan(self.lpv3Core, v3Other.lpv3Core)

	def Equal(self, v3Other):
		return Vector3DEqual(self.lpv3Core, v3Other.lpv3Core)

	def Perpendicular(self):
		v3Out = Vector3D(0.0, 0.0, 0.0)
		Vector3DPerpendicular(v3Out.lpv3Core, self.lpv3Core)
		return v3Out

	def AngleBetween(self, v3Other):
		return Vector3DAngleBetween(self.lpv3Core, v3Other.lpv3Core)

	def AngleBetweenFast(self, v3Other):
		return Vector3DAngleBetweenFast(self.lpv3Core, v3Other.lpv3Core)
	
	def Reflect(self, v3Other):
		v3Out = Vector3D(0.0, 0.0, 0.0)
		Vector3DReflect(v3Out.lpv3Core, self.lpv3Core, v3Other.lpv3Core)
		return v3Out

	def PrimaryAxis(self):
		v3Out = Vector3D(0.0, 0.0, 0.0)
		Vector3DPrimaryAxis(v3Out.lpv3Core, self.lpv3Core)
		return v3Out

	def PositionEquals(self, v3Other):
		return CBoolToPyBool(Vector3DPositionEquals(self.lpv3Core, v3Other.lpv3Core))

	def IsNotANumber(self):
		return CBoolToPyBool(Vector3DIsNotANumber(self.lpv3Core))
	










