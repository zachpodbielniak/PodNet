'''



 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_      \  |  _ \_ _|
| |_)  _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		CSystem.py
Author:	        Zach Podbielniak
Last Update:	01/17/18

Overview:	This file sets forth forwarding the PodNet C API to Python.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


'''


from PodNet.PodNetLib import *

LPFN_DIE_PROC = CFUNCTYPE(BOOL, LPVOID)


PodNetLib.LongSleep.restype = ULONGLONG
PodNetLib.LongSleep.argtypes = [ULONGLONG]

PodNetLib.Sleep.restype = ULONGLONG
PodNetLib.Sleep.argtypes = [ULONGLONG]

PodNetLib.MicroSleep.restype = ULONGLONG
PodNetLib.MicroSleep.argtypes = [ULONGLONG]

PodNetLib.NanoSleep.restype = ULONGLONG
PodNetLib.NanoSleep.argtypes = [ULONGLONG]

PodNetLib.PrintLine.restype = BOOL
PodNetLib.PrintLine.argtypes = [LPSTR]

PodNetLib.PostQuitMessage.argtypes = [LONG]
PodNetLib.RegisterOnDieCallBack.restype = BOOL

PodNetLib.RegisterOnDieCallBack.argtypes = [LPFN_DIE_PROC]
PodNetLib.DieViolently.argtypes = [LPSTR]

PodNetLib.DiePeacefully.argtypes = [LPSTR, LPVOID]
PodNetLib.DieByTheAtom.argtypes = [HANDLE, USHORT, LPVOID]

PodNetLib.GetNumberOfCpuCores.restype = UARCHLONG
PodNetLib.GetSystemVirtualMemorySize.restype = UARCHLONG

if (platform.machine() == 'x86_64'):
    PodNetLib.__WordDumpX64.restype = BOOL
    PodNetLib.__WordDumpX64.argtypes = [LPVOID, ULONGLONG]
    
    PodNetLib.__WordDumpToFileX64.restype = BOOL
    PodNetLib.__WordDumpToFileX64.argtypes = [LPVOID, ULONGLONG, HANDLE]
    
    PodNetLib.__DwordDumpX64.restype = BOOL
    PodNetLib.__DwordDumpX64.argtypes = [LPVOID, ULONGLONG]
    
    PodNetLib.__DwordDumpToFileX64.restype = BOOL
    PodNetLib.__DwordDumpToFileX64.argtypes = [LPVOID, ULONGLONG, HANDLE]
    
    PodNetLib.__QwordDumpX64.restype = BOOL
    PodNetLib.__QwordDumpX64.argtypes = [LPVOID, ULONGLONG]
    
    PodNetLib.__QwordDumpToFileX64.restype = BOOL
    PodNetLib.__QwordDumpToFileX64.argtypes = [LPVOID, ULONGLONG, HANDLE]





def LongSleep(ullSeconds):
        return PodNetLib.LongSleep(ullSeconds)


def Sleep(ullMilliSeconds):
        return PodNetLib.Sleep(ullMilliSeconds)


def MicroSleep(ullMicroSeconds):
        return PodNetLib.MicroSleep(ullMicroSeconds)


def NanoSleep(ullNanoSeconds):
        return PodNetLib.NanoSleep(ullNanoSeconds)


def PrintLine(Message):
        return PodNetLib.PrintLine(Message.encode('utf-8'))


def PostQuitMessage(Message):
        PodNetLib.PostQuitMessage(Message)
        return


def RegisterOnDieCallBack(Callback):
        cb = LPFN_DIE_PROC(Callback)
        return PodNetLib.RegisterOnDieCallBack(cb)


def DieViolently(DieMessage):
        PodNetLib.DieViolently(DieMessage.encode('utf-8'))
        return


def DiePeacefully(DieMessage, LpParam):
        PodNetLib.DiePeacefully(DieMessage, LpParam)
        return


def DieByTheAtom(Handle, Atom):
        PodNetLib.DieViolently(Handle, Atom, 0)
        return


def GetNumberOfCpuCores():
        return PodNetLib.GetNumberOfCpuCores()


def GetSystemVirtualMemorySize():
        return PodNetLib.GetSystemVirtualMemorySize()


if (platform.machine() == 'x86_64'):

    def WordDump(Address, Size):
            return PodNetLib.__WordDumpX64(Address, Size)


    def WordDumpToFile(Handle, Address, Size):
            return PodNetLib.__WordDumpToFileX64(Address, Size, Handle)


    def DwordDump(Address, Size):
            return PodNetLib.__DwordDumpX64(Address, Size)


    def DwordDumpToFile(Handle, Address, Size):
            return PodNetLib.__DwordDumpToFileX64(Address, Size, Handle)


    def QwordDump(Address, Size):
            return PodNetLib.__QwordDumpX64(Address, Size)


    def QwordDumpToFile(Handle, Address, Size):
            return PodNetLib.__QwordDumpToFileX64(Address, Size, Handle)
