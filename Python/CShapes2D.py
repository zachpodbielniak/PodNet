'''



 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		CShapes2D.py
Author:	        Zach Podbielniak
Last Update:	01/05/18

Overview:	This file sets forth forwarding the PodNet C API to Python.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


'''


from PodNet.PodNetLib import *
from PodNet.CCoordinates import *

SHAPE_NOT_A_SHAPE = 1
SHAPE_CIRCLE = 2
SHAPE_RECTANGLE = 3
SHAPE_TRIANGLE = 4
SHAPE_ELLIPSE = 5
SHAPE_PARALLELOGRAM = 6
SHAPE_TRAPEZOID = 7
SHAPE_TRAPEZIUM = 8
SHAPE_ANNULUS = 9
SHAPE_POLYGON = 10
SHAPE_CIRCULAR_SECTOR = 11
SHAPE_ANNULAR_SECTOR = 12
SHAPE_FILLET = 13
SHAPE_OTHER = 255




class SHAPE(Structure):
        _fields_ = [("lpChild", LPVOID), 
                        ("ulShapeType", ULONG), 
                        ("corCoordinate", COORDINATE), 
                        ("dbRotation", DOUBLE)]


LPSHAPE = POINTER(SHAPE)


class CIRCLE(Structure):
        _fields_ = [("lpshpParent", LPSHAPE),
                        ("dbRadius", DOUBLE)]

class RECTANGLE(Structure):
        _fields_ = [("lpshpParent", LPSHAPE),
                        ("dbBase", DOUBLE),
                        ("dbHeight", DOUBLE)]

class TRIANGLE(Structure):
        _fields_ = [("lpshpParent", LPSHAPE),
                        ("dbBase", DOUBLE),
                        ("dbHeight", DOUBLE),
                        ("dbTheta", DOUBLE),
                        ("ulType", ULONG)]

class ELLIPSE(Structure):
        _fields_ = [("lpshpParent", LPSHAPE),
                        ("dbR1", DOUBLE),
                        ("dbR2", DOUBLE)]

class PARALLELOGRAM(Structure):
        _fields_ = [("lpshpParent", LPSHAPE),
                        ("dbBase", DOUBLE),
                        ("dbHeight", DOUBLE),
                        ("dbTheta", DOUBLE)]

class TRAPEZOID(Structure):
        _fields_ = [("lpshpParent", LPSHAPE),
                        ("dbBase", DOUBLE),
                        ("dbHead", DOUBLE),
                        ("dbHeight", DOUBLE),
                        ("dbTheta", DOUBLE)]

class TRAPEZIUM(Structure):
        _fields_ = [("lpshpParent", LPSHAPE),
                        ("dbBase", DOUBLE),
                        ("dbHead", DOUBLE),
                        ("dbHeightLeft", DOUBLE),
                        ("dbHeightRight", DOUBLE),
                        ("dbTheta", DOUBLE)]

class ANNULUS(Structure):
        _fields_ = [("lpshpParent", LPSHAPE),
                        ("dbInnerRadius", DOUBLE),
                        ("dbOuterRadius", DOUBLE)]

class POLYGON(Structure):
        _fields_ = [("lpshpParent", LPSHAPE),
                        ("dbSideLenght", DOUBLE),
                        ("ulNumberOfSides", ULONG),
                        ("dbRadius", DOUBLE)]

class CIRCULAR_SECTOR(Structure):
        _fields_ = [("lpshpParent", LPSHAPE),
                        ("dbRadius", DOUBLE),
                        ("dbTheta", DOUBLE)]

class ANNULAR_SECTOR(Structure):
        _fields_ = [("lpshpParent", LPSHAPE),
                        ("dbInnerRadius", DOUBLE),
                        ("dbOuterRadius", DOUBLE),
                        ("dbTheta", DOUBLE)]

class FILLET(Structure):
        _fields_ = [("lpshpParent", LPSHAPE),
                        ("dbRadius", DOUBLE)]



LPTRIANGLE = POINTER(TRIANGLE)
LPRECTANGLE = POINTER(RECTANGLE)
LPCIRCLE = POINTER(CIRCLE)
LPELLIPSE = POINTER(ELLIPSE)
LPPARALLELOGRAM = POINTER(PARALLELOGRAM)
LPTRAPEZOID = POINTER(TRAPEZOID)
LPTRAPEZIUM = POINTER(TRAPEZIUM)
LPANNULUS = POINTER(ANNULUS)
LPPOLYGON = POINTER(POLYGON)
LPCIRCULAR_SECTOR = POINTER(CIRCULAR_SECTOR)
LPANNULAR_SECTOR = POINTER(ANNULAR_SECTOR)
LPFILLET = POINTER(FILLET)


PodNetLib.CreateShapeEx.restype = LPVOID
PodNetLib.CreateShapeEx.argtypes = [DOUBLE,
                                        DOUBLE,
                                        DOUBLE,
                                        DOUBLE,
                                        DOUBLE,
                                        DOUBLE,
                                        DOUBLE,
                                        ULONG,
                                        ULONG]

PodNetLib.CreateShape.restype = LPVOID
PodNetLib.CreateShape.argtypes = [DOUBLE, DOUBLE, DOUBLE, ULONG]
PodNetLib.DestroyShape.restypes = BOOL
PodNetLib.DestroyShape.argtypes = [LPSHAPE]

PodNetLib.Area.restype = DOUBLE
PodNetLib.Area.argtypes = [LPSHAPE]
PodNetLib.Perimeter.restype = DOUBLE
PodNetLib.Perimeter.argtypes = [LPSHAPE]
PodNetLib.FastPerimeter.restypes = DOUBLE
PodNetLib.FastPerimeter.argtypes = [LPSHAPE]

def CreateShape(x, y, z, ShapeType):
        return PodNetLib.CreateShape(x, y, z, ShapeType)

def CreateShapeEx(x, y, z, Misc, Theta, CoorX, CoorY, ShapeType, ShapeFlags):
        return PodNetLib.CreateShapeEx(x, y, z, Misc, Theta, CoorX, CoorY, ShapeType, ShapeFlags)

def DestroyShape(shape):
        return PodNetLib.DestroyShape(shape.lpParent)


class Shape(object):
        Coordinate = 0
        lpParent = 0
        lpShape = 0
        def __init__(self, Coordinate, Rotation):
                pass

        def Area(self):
                return PodNetLib.Area(self.lpParent)

        def Perimeter(self):
                return PodNetLib.Perimeter(self.lpParent)

        def FastPerimeter(self):
                return PodNetLib.FastPerimeter(self.lpParent)

        def GetInternalShapeState(self):
                x = cast(self.lpParent, LPSHAPE)
                return x.contents

        def GetRotation(self):
                return self.lpParent.contents.dbRotation

        def SetRotation(self, Theta):
                self.lpParent.contents.dbRotation = Theta

        def SetCoordinate(self, X, Y):
                self.Coordinate.dbX = X
                self.Coordinate.dbY = Y

        def GetCoordinateX(self):
                return self.Coordinate.dbX

        def GetCoordinateY(self):
                return self.Coordinate.dbY
                



EQUILATERAL_TRIANGLE = 1
RIGHT_TRIANGLE = 2
OBLIQUE_TRIANGLE = 3




class Triangle(Shape):
        tritype = 0
        def __init__(self, height, base, theta, tritype):
                self.tritype = tritype
                self.lpParent = cast(PodNetLib.CreateShapeEx(height, base, 0.0, 0.0, theta, 0.0, 0.0, SHAPE_TRIANGLE, tritype), LPSHAPE)
                self.lpShape = cast(self.lpParent.contents.lpChild, LPTRIANGLE)
                self.Coordinate = self.lpParent.contents.corCoordinate


class Rectangle(Shape):
        def __init__(self, base, height):
                self.lpParent = cast(PodNetLib.CreateShapeEx(height, base, 0.0, 0.0, 0.0, 0.0, 0.0, SHAPE_RECTANGLE, 0), LPSHAPE)
                self.lpShape = cast(self.lpParent.contents.lpChild, LPRECTANGLE)
                self.Coordinate = self.lpParent.contents.corCoordinate




class Circle(Shape):
        def __init__(self, radius):
                self.lpParent = cast(PodNetLib.CreateShape(radius, 0.0, 0.0, SHAPE_CIRCLE), LPSHAPE)
                self.lpShape = cast(self.lpParent.contents.lpChild, LPCIRCLE)
                self.Coordinate = self.lpParent.contents.corCoordinate




class Ellipse(Shape):
        def __init__(self, radiusX, radiusY):
                self.lpParent = cast(PodNetLib.CreateShape(radiusX, radiusY, 0.0, SHAPE_ELLIPSE), LPSHAPE)
                self.lpShape = cast(self.lpParent.contents.lpChild, LPCIRCLE)
                self.Coordinate = self.lpParent.contents.corCoordinate




class Parallelogram(Shape):
        def __init__(self, Height, Base, Theta):
                self.lpParent = cast(PodNetLib.CreateShape(Height, Base, Theta, SHAPE_PARALLELOGRAM), LPSHAPE)
                self.lpShape = cast(self.lpParent.contents.lpChild, LPPARALLELOGRAM)
                self.Coordinate = self.lpParent.contents.corCoordinate




class Trapezoid(Shape):
        def __init__(self, Height, Base, Head, Theta):
                self.lpParent = cast(PodNetLib.CreateShapeEx(Height, Base, Head, 0.0, Theta, 0.0, 0.0, SHAPE_TRAPEZOID, 0), LPSHAPE)
                self.lpShape = cast(self.lpParent.contents.lpChild, LPTRAPEZOID)
                self.Coordinate = self.lpParent.contents.corCoordinate




class Trapezium(Shape):
        def __init__(self, Base, Head, HeightLeft, HeightRight, Theta):
                self.lpParent = cast(PodNetLib.CreateShapeEx(Base, Head, HeightLeft, HeightRight, Theta, 0.0, 0.0, SHAPE_TRAPEZIUM, 0), LPSHAPE)
                self.lpShape = cast(self.lpParent.contents.lpChild, LPTRAPEZIUM)
                self.Coordinate = self.lpParent.contents.corCoordinate




class Annulus(Shape):
        def __init__(self, InnerRadius, OuterRadius):
                self.lpParent = cast(PodNetLib.CreateShapeEx(InnerRadius, OuterRadius, 0.0, 0.0, 0.0, 0.0, 0.0, SHAPE_ANNULUS, 0.0), LPSHAPE)
                self.lpShape = cast(self.lpParent.contents.lpChild, LPANNULUS)
                self.Coordinate = self.lpParent.contents.corCoordinate




class Polygon(Shape):
        def __init__(self, SideLength, Radius, NumberOfSides):
                self.lpParent = cast(PodNetLib.CreateShapeEx(SideLength, Radius, 0.0, 0.0, 0.0, 0.0, 0.0, SHAPE_POLYGON, NumberOfSides), LPSHAPE)
                self.lpShape = cast(self.lpParent.contents.lpChild, LPPOLYGON)
                self.Coordinate = self.lpParent.contents.corCoordinate




class CircularSector(Shape):
        def __init__(self, Radius, Theta):
                self.lpParent = cast(PodNetLib.CreateShapeEx(Radius, 0.0, 0.0, 0.0, Theta, 0.0, 0.0, SHAPE_CIRCLE, 0), LPSHAPE)
                self.lpShape = cast(self.lpParent.contents.lpChild, LPCIRCULAR_SECTOR)
                self.Coordinate = self.lpParent.contents.corCoordinate
        



class AnnularSector(Shape):
        def __init__ (self, InnerRadius, OuterRadius, Theta):
                self.lpParent = cast(PodNetLib.CreateShapeEx(InnerRadius, OuterRadius, 0.0, 0.0, Theta, 0.0, 0.0, SHAPE_ANNULAR_SECTOR, 0), LPSHAPE)
                self.lpShape = cast(self.lpParent.contents.lpChild, LPANNULAR_SECTOR)
                self.Coordinate = self.lpParent.contents.corCoordinate
        



class Fillet(Shape):
        def __init__(self, Radius):
                self.lpParent = cast(PodNetLib.CreateShapeEx(Radius, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, SHAPE_FILLET, 0), LPSHAPE)
                self.lpShape = cast(self.lpParent.contents.lpChild, LPFILLET)
                self.Coordinate = self.lpParent.contents.corCoordinate
