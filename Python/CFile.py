'''



 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		CFile.py
Author:	        Zach Podbielniak
Last Update:	01/12/18

Overview:	This file sets forth forwarding the PodNet C API to Python.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


'''


from PodNet.PodNetLib import *

FILE_PERMISSION_READ = 1
FILE_PERMISSION_WRITE = 2
FILE_PERMISSION_APPEND = 4
FILE_PERMISSION_CREATE = 8
FILE_PERMISSION_ASYNC = 16


PodNetLib.CreateFile.restype = HANDLE
PodNetLib.CreateFile.argtypes = [LPSTR, ULONG, ULONG]

PodNetLib.OpenFile.restype = HANDLE
PodNetLib.OpenFile.argtypes = [LPSTR, ULONG, ULONG]

PodNetLib.WriteFile.restype = BOOL
PodNetLib.WriteFile.argtypes = [HANDLE, LPSTR, ULONG]

PodNetLib.ReadFile.restype = BOOL
PodNetLib.ReadFile.argtypes = [HANDLE, DLPSTR, ULONGLONG, ULONG]

PodNetLib.ReadFile.restype = BOOL
PodNetLib.ReadFile.argtypes = [HANDLE, DLPSTR, ULONG]

PodNetLib.GetFileSize.restype = ULONGLONG
PodNetLib.GetFileSize.argtypes = [HANDLE]

PodNetLib.FlushFileBuffer.restype = BOOL
PodNetLib.FlushFileBuffer.argtypes = [HANDLE]

PodNetLib.RewindFile.restype = BOOL
PodNetLib.RewindFile.argtypes = [HANDLE]

PodNetLib.FileTell.restype = ULONGLONG
PodNetLib.FileTell.argtypes = [HANDLE]

PodNetLib.FileSeek.restype = ULONGLONG
PodNetLib.FileSeek.argtypes = [HANDLE, ULONGLONG, ULONGLONG]

PodNetLib.GetStandardOut.restype = HANDLE

PodNetLib.GetStandardIn.restype = HANDLE

PodNetLib.GetStandardError.restype = HANDLE


def CreateFile(lpszFileName, ulPermission, ulFlags):
        return PodNetLib.CreateFile(lpszFileName.encode('utf-8'), ulPermission, ulFlags)


def OpenFile(lpszFileName, ulPermission, ulFlags):
        return PodNetLib.OpenFile(lpszFileName.encode('utf-8'), ulPermission, ulFlags)


def WriteFile(hFile, lpszString, ulFlags):
        return PodNetLib.WriteFile(hFile, lpszString.encode('utf-8'), ulFlags)


def ReadFile(hFile, ullBytes, ulFlags):
        s = create_string_buffer(b'\0' * (ullBytes + 1))
        p = cast(s, LPSTR)
        PodNetLib.ReadFile(hFile, cast(addressof(p), DLPSTR), ullBytes, ulFlags)
        return s.value


def ReadWholeFile(hFile, ulFlags):
        s = create_string_buffer(b'\0' * GetFileSize(hFile))
        p = cast(s, LPSTR)
        PodNetLib.ReadFile(hFile, cast(addressof(p), DLPSTR), GetFileSize(hFile), ulFlags)
        return s.value.decode('utf-8')


def GetFileSize(hFile):
        return PodNetLib.GetFileSize(hFile)


def FlushFileBuffer(hFile):
        return PodNetLib.FlushFileBuffer(hFile)


def RewindFile(hFile):
        return PodNetLib.RewindFile(hFile)


def FileTell(hFile):
        return PodNetLib.FileTell(hFile)


def FileSeek(hFile, ullOffset, ullOrigin):
        return PodNetLib.FileSeek(hFile, ullOffset, ullOrigin)


def GetStandardOut():
        return PodNetLib.GetStandardOut()


def GetStandardIn():
        return PodNetLib.GetStandardIn()


def GetStandardError():
        return PodNetLib.GetStandardError()
