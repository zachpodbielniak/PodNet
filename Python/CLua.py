'''



 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		CFile.py
Author:	        Zach Podbielniak
Last Update:	01/05/18

Overview:	This file sets forth forwarding the PodNet C API to Python.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


'''


from PodNet.PodNetLib import *


PodNetLib.CreateLuaState.restype = HANDLE
PodNetLib.RunLuaScript.restype = BOOL
PodNetLib.RunLuaScript.argtypes = [HANDLE, HANDLE]
PodNetLib.RunLuaScriptDirect.restype = BOOL
PodNetLib.RunLuaScriptDirect.argtypes = [HANDLE, LPSTR]
PodNetLib.RunLuaString.restype = BOOL
PodNetLib.RunLuaScript.argtypes = [HANDLE, LPSTR]


def CreateLuaState():
        return PodNetLib.CreateLuaState()

def RunLuaScript(HLua, HFile):
        return PodNetLib.RunLuaScript(HLua, HFile)

def RunLuaScriptDirect(HLua, Filename):
        return PodNetLib.RunLuaScriptDirect(HLua, Filename.encode('utf-8'))

def RunLuaString(HLua, Code):
        return PodNetLib.RunLuaString(HLua, Code)