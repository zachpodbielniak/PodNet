'''



 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		CHttpRequest.py
Author:	        Zach Podbielniak
Last Update:    06/28/18

Overview:	This file sets forth forwarding the PodNet C API to Python.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


'''


from PodNet.PodNetLib import *



HTTP_REQUEST_INVALID = 0
HTTP_REQUEST_GET = 1
HTTP_REQUEST_PUT = 2

HTTP_VERSION_INVALID = 0
HTTP_VERSION_1_0 = 1
HTTP_VERSION_1_1 = 2


HTTP_ACCEPT_PLAIN_TEXT = "text/plain"
HTTP_ACCEPT_HTML = "text/html"

HTTP_CONNECTION_CLOSE = "close"
HTTP_CONNECTION_KEEP_ALIVE = "keep-alive"



PodNetLib.CreateHttpRequest.restype = HANDLE 
PodNetLib.CreateHttpRequest.argtypes = [ULONG, ULONG, LPSTR, USHORT, LPSTR, LPSTR, LPSTR, LPSTR, BOOL] 

PodNetLib.ExecuteHttpRequest.restype = BOOL 
PodNetLib.ExecuteHttpRequest.argtypes = [HANDLE, LPVOID, UARCHLONG, LPUARCHLONG]



def CreateHttpRequest(ulType, ulVersion, lpszHost, usPort, lpszPath, lpszUserAgent, lpszAccept, lpszOptions, bUseIpv6):
	return PodNetLib.CreateHttpRequest(ulType, ulVersion, lpszHost.encode('utf-8'), usPort, lpszPath.encode('utf-8'), lpszUserAgent.encode('utf-8'), lpszAccept.encode('utf-8'), lpszOptions.encode('utf-8'), bUseIpv6)


def ExecuteHttpRequest(hHttpRequest, ualSize):
	lpOut = create_string_buffer(b'\0' * (ualSize + 1))
	ualOut = UARCHLONG(0)
	PodNetLib.ExecuteHttpRequest(hHttpRequest, cast(lpOut, LPVOID), ualSize + 1, cast(addressof(ualOut), LPUARCHLONG))
	return ualOut.value, lpOut