'''



 ____           _ _   _      _        _    ____ ___ 
|  _ \ ___   __| | \ | | ___| |_     / \  |  _ \_ _|
| |_) / _ \ / _` |  \| |/ _ \ __|   / _ \ | |_) | | 
|  __/ (_) | (_| | |\  |  __/ |_   / ___ \|  __/| | 
|_|   \___/ \__,_|_| \_|\___|\__| /_/   \_\_|  |___|


File:		CIpv4.py
Author:	        Zach Podbielniak
Last Update:	04/13/18

Overview:	This file sets forth forwarding the PodNet C API to Python.

		This file is part of the PodNet API and comes with no warranty,
		use with your own discretion.


'''


from PodNet.PodNetLib import *
import subprocess 


IPV4_CLASS_A = 1
IPV4_CLASS_B = 2
IPV4_CLASS_C = 3
IPV4_CLASS_D = 4
IPV4_CLASS_E = 5
IPV4_CLASS_UNKNOWN = 6


IPV4_ADDRESS_STRING_LENGTH = 0x10



class IPV4_ADDRESS(Structure):
	_fields_ = [("ulData", ULONG)]



LPIPV4_ADDRESS = POINTER(IPV4_ADDRESS)


PodNetLib.CreateIpv4Address.restype = IPV4_ADDRESS
PodNetLib.CreateIpv4Address.argtypes = [BYTE, BYTE, BYTE, BYTE]

PodNetLib.CreateIpv4AddressFromString.restype = IPV4_ADDRESS
PodNetLib.CreateIpv4AddressFromString.argtypes = [LPSTR]

PodNetLib.ConvertIpv4AddressToString.restype = BOOL 
PodNetLib.ConvertIpv4AddressToString.argtypes = [IPV4_ADDRESS, LPSTR, ULONG]

PodNetLib.GetIpv4AddressClass.restype = ULONG
PodNetLib.GetIpv4AddressClass.argtypes = [IPV4_ADDRESS]

PodNetLib.GetCurrentPublicIpv4Address.restype = IPV4_ADDRESS


def IPV4_ADDRESSES_EQUAL(ipv4X, ipv4Y):
	if (ipv4X.ulData == ipv4Y.ulData):
		return TRUE 
	return FALSE


def CreateIpv4Address(byFirstOctet, bySecondOctet, byThirdOctet, byFourthOctet):
	return PodNetLib.CreateIpv4Address(byFirstOctet, bySecondOctet, byThirdOctet, byFourthOctet)


def CreateIpv4AddressFromString(lpszIpAddress):
	return PodNetLib.CreateIpv4AddressFromString(lpszIpAddress.encode('utf-8'))


def ConvertIpv4AddressToString(ipv4Address):
	lpszOut = create_string_buffer(b'\0' * IPV4_ADDRESS_STRING_LENGTH)
	PodNetLib.ConvertIpv4AddressToString(ipv4Address, lpszOut, IPV4_ADDRESS_STRING_LENGTH)
	return lpszOut.value.decode('utf-8')


def GetIpv4AddressClass(ipv4Address):
	return PodNetLib.GetIpv4AddressClass(ipv4Address)


def GetCurrentPublicIpv4Address():
	return PodNetLib.GetCurrentPublicIpv4Address()


class Ipv4(object):
	ipv4Address = 0
	def __init__(self, lpszIpAddress):
		self.ipv4Address = CreateIpv4AddressFromString(lpszIpAddress)
	
	def ToString(self):
		return ConvertIpv4AddressToString(self.ipv4Address)
	
	def GetInnerLong(self):
		return self.ipv4Address.ulData

	def GetAddressClass(self):
		return GetIpv4AddressClass(self.ipv4Address)

	def SetAddress(self, lpszAddress):
		self.ipv4Address = CreateIpv4AddressFromString(lpszIpAddress)

	def AddressesEqual(self, ipv4Other):
		if (self.ipv4Address.ulData == ipv4Other.ipv4Address.ulData):
			return True
		return False
