/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CCompileModule.h"
#include "CModule.h"
#include "CModuleObject.c"




GLOBAL_VARIABLE UARCHLONG ualId = 0;




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HANDLE
CompileAndLoadModule(
	_In_ 		LPCSTR 			lpcszSourceFile,
	_In_Opt_Z_ 	LPCSTR 			lpcszCompilationOptions,
	_In_Opt_ 	LPVOID 			lpOnCreateParam,
	_In_Opt_ 	LPVOID 			lpOnDestroyParam,
	_In_ 		LOCK_TYPE 		ltLock,
	_In_Opt_ 	ULONG 			ulLockFlags,
	_In_Opt_ 	LPVOID 			lpUserData,
	_In_ 		BOOL 			bVerbose
){
	return CompileAndLoadModuleCachedEx(
		lpcszSourceFile,
		lpcszCompilationOptions,
		lpOnCreateParam,
		lpOnDestroyParam,
		ltLock,
		ulLockFlags,
		lpUserData,
		0,
		NULLPTR,
		0,
		bVerbose
	);
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HANDLE
CompileAndLoadModuleCached(
	_In_ 		LPCSTR 			lpcszSourceFile,
	_In_Opt_Z_ 	LPCSTR 			lpcszCompilationOptions,
	_In_Opt_ 	LPVOID 			lpOnCreateParam,
	_In_Opt_ 	LPVOID 			lpOnDestroyParam,
	_In_ 		LOCK_TYPE 		ltLock,
	_In_Opt_ 	ULONG 			ulLockFlags,
	_In_Opt_ 	LPVOID 			lpUserData,
	_In_Opt_	ULONGLONG		ullCacheHash,
	_In_ 		BOOL 			bVerbose
){
	return CompileAndLoadModuleCachedEx(
		lpcszSourceFile,
		lpcszCompilationOptions,
		lpOnCreateParam,
		lpOnDestroyParam,
		ltLock,
		ulLockFlags,
		lpUserData,
		ullCacheHash,
		NULLPTR,
		0,
		bVerbose
	);
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HANDLE
CompileAndLoadModuleCachedEx(
	_In_ 		LPCSTR 			lpcszSourceFile,
	_In_Opt_Z_ 	LPCSTR 			lpcszCompilationOptions,
	_In_Opt_ 	LPVOID 			lpOnCreateParam,
	_In_Opt_ 	LPVOID 			lpOnDestroyParam,
	_In_ 		LOCK_TYPE 		ltLock,
	_In_Opt_ 	ULONG 			ulLockFlags,
	_In_Opt_ 	LPVOID 			lpUserData,
	_In_Opt_	ULONGLONG		ullCacheHash,
	_Reserved_	LPVOID			lpReserved,
	_In_Opt_	ULONGLONG		ullFlags,
	_In_ 		BOOL 			bVerbose
){
	HANDLE hModule;
	HANDLE hProcess;
	LPMODULE lpModule;
	CSTRING csBuffer[8192];
	CSTRING csOutput[512];
	DLPSTR dlpszSplit;
	UARCHLONG ualLocalId;
	BOOL bCached;
	BOOL bWantsCache;

	ZeroMemory(csBuffer, sizeof(csBuffer));
	ZeroMemory(csOutput, sizeof(csOutput));
	bCached = FALSE;
	bWantsCache = FALSE;

	StringSplit(lpcszSourceFile, ".", &dlpszSplit);

	if (NULLPTR == dlpszSplit)
	{ return NULL_OBJECT; }

	if (NULLPTR == lpcszCompilationOptions)
	{ lpcszCompilationOptions = ""; }

	ualLocalId = InterlockedIncrement(&ualId);

	if (0 != ullCacheHash)
	{
		struct stat statInfo;

		StringPrintFormatSafe(
			csOutput,
			sizeof(csOutput) - 1,
			"/tmp/podnet-module-%lx",
			ullCacheHash
		);

		if (-1 != stat(csOutput, &statInfo))
		{ bCached = TRUE; }

		bWantsCache = TRUE;
	}
	else 
	{ 
		StringPrintFormatSafe(
			csOutput,
			sizeof(csOutput) - 1,
			"/tmp/podnet-module-%lu-%lu",
			(UARCHLONG)getpid(),
			ualLocalId
		);
	}

	StringPrintFormatSafe(
		csBuffer,
		sizeof(csBuffer) - 1,
		"-fPIC -shared %s -o %s %s",
		lpcszSourceFile,
		csOutput,
		lpcszCompilationOptions
	);

	if (FALSE == bCached && !ullFlags & COMPILE_AND_LOAD_MODULE_DRY_RUN)
	{ 
		hProcess = CreateProcess(
			"gcc",
			csBuffer,
			bVerbose
		);

		if (NULL_OBJECT == hProcess)
		{ 
			DestroySplitString(dlpszSplit);
			return NULL_OBJECT; 
		}
	
		WaitForSingleObject(hProcess, INFINITE_WAIT_TIME);

		if (0 != GetProcessReturnStatus(hProcess))
		{
			DestroySplitString(dlpszSplit);
			DestroyObject(hProcess);
			return NULL_OBJECT;
		}
	}

	if (ullFlags & COMPILE_AND_LOAD_MODULE_PRINT_COMPILE_INVOCATION)
	{
		PrintFormat(
			"gcc %s\n",
			csBuffer
		);
	}

	if (!ullFlags & COMPILE_AND_LOAD_MODULE_DRY_RUN)
	{
		hModule = LoadModule(
			csOutput,
			lpOnCreateParam,
			lpOnDestroyParam,
			ltLock,
			ulLockFlags,
			lpUserData
		);
	}

	DestroySplitString(dlpszSplit);

	if (!ullFlags & COMPILE_AND_LOAD_MODULE_DRY_RUN)
	{ 
		if (FALSE == bCached)
		{ DestroyObject(hProcess); }
	}

	if (NULL_OBJECT == hModule)
	{ return NULL_OBJECT; }

	lpModule = OBJECT_CAST(hModule, LPMODULE);
	if (NULLPTR == lpModule)
	{ 
		if (FALSE == bCached)
		{ DestroyObject(hModule); }
		return NULL_OBJECT; 
	}

	lpModule->bCompiledFromSource = TRUE;
	lpModule->bCachedModule = bWantsCache;

	return hModule;
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HANDLE
AssembleAndLoadModuleX64(
	_In_ 		LPCSTR 			lpcszSourceFile,
	_In_Opt_ 	LPVOID 			lpOnCreateParam,
	_In_Opt_ 	LPVOID 			lpOnDestroyParam,
	_In_ 		LOCK_TYPE 		ltLock,
	_In_Opt_ 	ULONG 			ulLockFlags,
	_In_Opt_ 	LPVOID 			lpUserData
);