/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CWINDOWCLASS_H
#define CWINDOWCLASS_H


#include "CModule.h"
#include "CModuleClass.h" 



#define INHERITS_FROM_WINDOW_MODULE_DESCRIPTOR_TABLE(NAME)		\
	NAME##_ON_CREATE_WINDOW						\
	NAME##_ON_DESTROY_WINDOW 					\
	NAME##_ON_SHOW_WINDOW						\
	NAME##_ON_HIDE_WINDOW						\
	NAME##_ON_UPDATE_WINDOW						\
	NAME##_ON_EDIT_WINDOW 						\
	NAME##_ON_WINDOW_EVENT	 					\
	NAME##_GET_WINDOW_INFORMATION 				




#define DEFINE_WINDOW_MODULE_DESCRIPTOR_TABLE(NAME,...)			\
	typedef enum __MODULE_##NAME##_DESCRIPTOR 			\
	{								\
		INHERITS_FROM_MODULE_DESCRIPTOR_TABLE(NAME)		\
		INHERITS_FROM_WINDOW_MODULE_DESCRIPTOR_TABLE(NAME)	\
		__VA_ARGS,						\
		END_OF_MODULE_DESCRIPTOR_TABLE(NAME)			\
	} MODULE_##NAME##_DESCRIPTOR;




#define REGISTER_WINDOW_MODULE_THUNKS(HMODULE,NAME)							\
	RegisterModuleThunk(HMODULE, NAME##_ON_SHOW_WINDOW, "OnShowWindow", NULLPTR); 			\
	RegisterModuleThunk(HMODULE, NAME##_ON_HIDE_WINDOW, "OnHideWindow", NULLPTR); 			\
	RegisterModuleThunk(HMODULE, NAME##_ON_UPDATE_WINDOW, "OnUpdateWindow", NULLPTR);		\
	RegisterModuleThunk(HMODULE, NAME##_ON_CREATE_WINDOW, "OnCreateWindow", NULLPTR);		\
	RegisterModuleThunk(HMODULE, NAME##_ON_DESTROY_WINDOW, "OnDestroyWindow", NULLPTR);		\
	RegisterModuleThunk(HMODULE, NAME##_ON_EDIT_WINDOW, "OnEditWindow", NULLPTR);			\
	RegisterModuleThunk(HMODULE, NAME##_ON_WINDOW_EVENT, "OnWindowEvent", NULLPTR);			\
	RegisterModuleThunk(HMODULE, NAME##_GET_WINDOW_INFORMATION, "GetWindowInformation", NULLPTR);	




_Success_(_Inexpressible_, _Inexpressible_)
PODNET_MODULE
VIRTUAL
LPVOID
OnShowWindow(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
);




_Success_(_Inexpressible_, _Inexpressible_)
PODNET_MODULE
VIRTUAL
LPVOID
OnHideWindow(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
);




_Success_(_Inexpressible_, _Inexpressible_)
PODNET_MODULE
VIRTUAL
LPVOID
OnUpdateWindow(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
);




_Success_(_Inexpressible_, _Inexpressible_)
PODNET_MODULE
VIRTUAL
LPVOID
OnCreateWindow(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
);




_Success_(_Inexpressible_, _Inexpressible_)
PODNET_MODULE
VIRTUAL
LPVOID
OnDestroyWindow(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
);




_Success_(_Inexpressible_, _Inexpressible_)
PODNET_MODULE
VIRTUAL
LPVOID
OnEditWindow(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
);




_Success_(_Inexpressible_, _Inexpressible_)
PODNET_MODULE
VIRTUAL
LPVOID
OnWindowEvent(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
);




_Success_(_Inexpressible_, _Inexpressible_)
PODNET_MODULE
VIRTUAL
LPVOID
GetWindowInformation(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
);




#endif
	
	
	