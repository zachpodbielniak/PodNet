/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../CompilationFlags.h"
#include "../CHandle/CHandle.h"


#ifndef CMODULEOBJECT_H
#define CMODULEOBJECT_H


#ifndef MODULE_DEFS
#define MODULE_DEFS

#define INHERITS_FROM_MODULE()					HANDLE 		hModule


#define INHERITS_FROM_MODULE_DESCRIPTOR_TABLE(NAME)		NAME##_ON_CREATE 	= 0x00, \
								NAME##_ON_DESTROY 	= 0x01, \
								NAME##_ON_LAUNCH 	= 0x02, \
								NAME##_ON_ERROR 	= 0x03, \
								NAME##_ON_ALARM 	= 0x04, \
								NAME##_ON_EVENT 	= 0x05, \
								NAME##_ON_MESSAGE_PASS 	= 0x06, \
								NAME##_GET_MODULE_INFO 	= 0x07, \


#define END_OF_MODULE_DESCRIPTOR_TABLE(NAME) 			NAME##_DESCRIPTOR_SIZE


#define DEFINE_MODULE_DESCRIPTOR_TABLE(NAME,...)		typedef enum __MODULE_##NAME##_DESCRIPTOR 		\
								{							\
									INHERITS_FROM_MODULE_DESCRIPTOR_TABLE(NAME)	\
									__VA_ARGS__,					\
									END_OF_MODULE_DESCRIPTOR_TABLE(NAME)		\
								} MODULE_##NAME##_DESCRIPTOR


typedef struct __MODULE_EVENT_DESCRIPTOR
{
	INHERITS_FROM_MODULE();
	HANDLE 		hLock;	/* The lock that should be acquired to handle the data, if any */
	HANDLE 		hEvent;	/* HANDLE to underlying EVENT to signal that it was handled */
	LPVOID 		lpData;
} MODULE_EVENT_DESCRIPTOR, *LPMODULE_EVENT_DESCRIPTOR, **DLPMODULE_EVENT_DESCRIPTOR;


typedef _Call_Back_ LPVOID(* LPFN_MODULE_DISPATCH_PROC)(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
);


typedef LPFN_MODULE_DISPATCH_PROC				*DLPFN_MODULE_DISPATCH_PROC, **TLPFN_MODULE_DISPATCH_PROC;

#endif



typedef LPVOID 			HLIB;


typedef struct __MODULE_THUNK_DESCRIPTOR
{
	LPFN_MODULE_DISPATCH_PROC 	lpfnCallBack;
	LPVOID 				lpParam;
} MODULE_THUNK_DESCRIPTOR;




typedef struct __MODULE_DISPATCH_DESCRIPTOR
{
	INHERITS_FROM_MODULE();
	union
	{
		struct 
		{
			MODULE_THUNK_DESCRIPTOR		mtdOnCreate;
			MODULE_THUNK_DESCRIPTOR 	mtdOnDestroy;
			MODULE_THUNK_DESCRIPTOR		mtdOnLaunch;
			MODULE_THUNK_DESCRIPTOR 	mtdOnError;
			MODULE_THUNK_DESCRIPTOR 	mtdOnAlarm;
			MODULE_THUNK_DESCRIPTOR 	mtdOnEvent;
			MODULE_THUNK_DESCRIPTOR 	mtdOnMessagePass;
		};
		MODULE_THUNK_DESCRIPTOR 		lpmtdTrampolineThunkDispatchDescriptorTable[MODULE_TRAMPOLINE_THUNK_DISPATCH_TABLE_SIZE];
	};
} MODULE_DISPATCH_DESCRIPTOR, *LPMODULE_DISPATCH_DESCRIPTOR;




typedef struct __MODULE 
{
	INHERITS_FROM_HANDLE();
	HANDLE 				hDispatchThread;
	HANDLE 				hLock;
	HLIB 				hlLib;
	LPSTR 				lpszModuleFile;
	LPVOID 				lpUserData;
	MODULE_DISPATCH_DESCRIPTOR 	mddCore;
	BOOL 				bCompiledFromSource;
	BOOL				bCachedModule;
} MODULE, *LPMODULE;




typedef struct __MODULE_THUNK_DISPATCH_PARAM
{
	HANDLE 				hModule;
	MODULE_THUNK_DESCRIPTOR 	mcbdCallBack;
} MODULE_THUNK_DISPATCH_PARAM, *LPMODULE_THUNK_DISPATCH_PARAM;


#endif