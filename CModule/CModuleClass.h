/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CMODULECLASS_H
#define CMODULECLASS_H


#include "CModule.h"



_Success_(return != NULLPTR, _Non_Locking_)
PODNET_MODULE
VIRTUAL
LPVOID
OnCreate(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_MODULE
VIRTUAL
LPVOID
OnDestroy(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_MODULE
VIRTUAL
LPVOID
OnLaunch(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_MODULE
VIRTUAL
LPVOID
OnError(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_MODULE
VIRTUAL
LPVOID
OnAlarm(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_MODULE
VIRTUAL
LPVOID
OnEvent(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_MODULE
VIRTUAL
LPVOID
OnMessagePass(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
);




_Success_(_Inexpressible_, _Inexpressible_)
PODNET_MODULE
VIRTUAL
LPVOID 
GetModuleInfo(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
);







#endif