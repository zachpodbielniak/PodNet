/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CMODULE_C
#define CMODULE_C


#include "CModule.h"
#include "../CompilationFlags.h"
#include "CModuleObject.c"
#include <dlfcn.h>




GLOBAL_VARIABLE DONT_OPTIMIZE_OUT HHASHTABLE hhtModules = NULLPTR;
GLOBAL_VARIABLE DONT_OPTIMIZE_OUT HANDLE hModuleLock = NULLPTR;



_Success_(return != FALSE, _Interlocked_Operation_)
INTERNAL_OPERATION
BOOL 
__InitializeModuleSubsystem(
	VOID
){
	HHASHTABLE hhtModulesTemp;
	HANDLE hLockTemp;
	BOOL bHappened;

	hhtModulesTemp = CreateHashTable(512);
	bHappened = InterlockedCompareExchangeHappened(&hhtModules, NULLPTR, hhtModulesTemp);
	if (FALSE == bHappened)
	{ DestroyHashTable(hhtModulesTemp); }

	bHappened = FALSE;
	hLockTemp = CreateCriticalSectionAndSpecifySpinCount(0x0C00);
	bHappened = InterlockedCompareExchangeHappened(&hModuleLock, NULL_OBJECT, hLockTemp);
	if (FALSE == bHappened)
	{ DestroyObject(hLockTemp); }

	return TRUE;	
}




_Success_(return != NULL_OBJECT, _Acquires_Shared_Lock_)
INTERNAL_OPERATION
HANDLE 
__LookupModule(
	_In_		HLIB		hlLib
){
	EXIT_IF_UNLIKELY_NULL(hlLib, NULL_OBJECT);
	UARCHLONG ualKey;
	ULONGLONG ullOutData;
	HANDLE hModule;

	WaitForSingleObject(hModuleLock, INFINITE_WAIT_TIME); 

	ualKey = HashTableGetKey(
		hhtModules, 
		(ULONGLONG)hlLib,
		sizeof(LPVOID),
		TRUE
	);

	HashTableGetValueEx(
		hhtModules,
		ualKey,
		NULLPTR,
		NULLPTR,
		&ullOutData
	);

	ReleaseSingleObject(hModuleLock);
	hModule = (HANDLE)ullOutData;
	return hModule;
}




_Success_(return != FALSE, _Acquires_Shared_Lock_)
INTERNAL_OPERATION
BOOL 
__RegisterModule(
	_In_		LPMODULE	lpModule
){
	EXIT_IF_UNLIKELY_NULL(lpModule, FALSE);
	ULONGLONG ullData;
	ullData = (ULONGLONG)lpModule->hThis;

	WaitForSingleObject(hModuleLock, INFINITE_WAIT_TIME);
	HashTableInsertEx(
		hhtModules,
		(ULONGLONG)lpModule->hlLib,
		sizeof(HLIB),
		TRUE,
		ullData
	);
	ReleaseSingleObject(hModuleLock);

	return TRUE;
}




_Success_(return != FALSE, _Acquires_Shared_Lock_)
INTERNAL_OPERATION
BOOL 
__DeregisterModule(
	_In_ 		LPMODULE	lpModule
){
	EXIT_IF_UNLIKELY_NULL(lpModule, FALSE);
	ULONGLONG ullData;
	ullData = 0;

	WaitForSingleObject(hModuleLock, INFINITE_WAIT_TIME);
	HashTableInsertEx(
		hhtModules,
		(LONGLONG)lpModule->hlLib,
		sizeof(HLIB),
		TRUE,
		ullData
	);
	ReleaseSingleObject(hModuleLock);

	return TRUE;
}





_Thread_Proc_
INTERNAL_OPERATION
LPVOID
__ThunkDispatchThreadProc(
	_In_ 		LPVOID 		lpParam
){
	EXIT_IF_UNLIKELY_NULL(lpParam, NULLPTR);
	LPMODULE_THUNK_DISPATCH_PARAM lpmtdpParam;
	lpmtdpParam = (LPMODULE_THUNK_DISPATCH_PARAM)lpParam;

	lpmtdpParam->mcbdCallBack.lpfnCallBack(lpmtdpParam->hModule, lpmtdpParam->mcbdCallBack.lpParam);
	FreeMemory(lpParam);
	return NULLPTR;
}




INTERNAL_OPERATION
BOOL
__DestroyModule(
	_In_ 		HDERIVATIVE 		hDerivative,
	_In_Opt_ 	ULONG 			ulFlags
){
	EXIT_IF_UNLIKELY_NULL(hDerivative, FALSE);
	UNREFERENCED_PARAMETER(ulFlags);
	
	LPMODULE lpModule;
	lpModule = (LPMODULE)hDerivative;

	__DeregisterModule(lpModule);

	if (NULLPTR != lpModule->mddCore.mtdOnDestroy.lpfnCallBack)
	{ lpModule->mddCore.mtdOnDestroy.lpfnCallBack(lpModule->hThis, lpModule->mddCore.mtdOnDestroy.lpParam); }

	if (NULLPTR != lpModule->hDispatchThread)
	{ DestroyObject(lpModule->hDispatchThread); }
	
	if (NULLPTR != lpModule->hLock)
	{ DestroyObject(lpModule->hLock); }
	
	/* If compiled from source, and not cached, remove the file */
	if (FALSE != lpModule->bCompiledFromSource && FALSE == lpModule->bCachedModule)
	{ unlink(lpModule->lpszModuleFile); }


	dlclose(lpModule->hlLib);
	FreeMemory(lpModule->lpszModuleFile);
	FreeMemory(lpModule);
	return TRUE;
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HANDLE
LoadModule(
	_In_ 		LPCSTR RESTRICT		lpcszModuleFile,
	_In_Opt_ 	LPVOID RESTRICT		lpOnCreateParam,
	_In_Opt_ 	LPVOID RESTRICT		lpOnDestroyParam,
	_In_ 		LOCK_TYPE 		ltLock,
	_In_Opt_ 	ULONG 			ulLockFlags,
	_In_Opt_ 	LPVOID RESTRICT		lpUserData
){
	EXIT_IF_NULL(lpcszModuleFile, NULLPTR);
	EXIT_IF_CONDITION(LOCK_TYPE_SIZE <= ltLock, NULLPTR);
	HANDLE hModule;
	LPMODULE lpModule;

	if (NULLPTR == hhtModules)
	{ __InitializeModuleSubsystem(); }

	lpModule = (LPMODULE)GlobalAllocAndZero(sizeof(MODULE));
	EXIT_IF_NULL(lpModule, NULLPTR);

	lpModule->hlLib = dlopen(lpcszModuleFile, RTLD_NOW | RTLD_GLOBAL);
	if (NULLPTR == lpModule->hlLib)
	{
		FreeMemory(lpModule);
		return NULL_OBJECT;
	}

	hModule = __LookupModule(lpModule->hlLib);
	if (NULL_OBJECT != hModule)
	{
		FreeMemory(lpModule);
		return hModule;
	}

	lpModule->mddCore.mtdOnCreate.lpfnCallBack = (LPFN_MODULE_DISPATCH_PROC)dlsym(lpModule->hlLib, (LPCSTR)"OnCreate");
	lpModule->mddCore.mtdOnCreate.lpParam = lpOnCreateParam;

	lpModule->mddCore.mtdOnDestroy.lpfnCallBack = (LPFN_MODULE_DISPATCH_PROC)dlsym(lpModule->hlLib, (LPCSTR)"OnDestroy");
	lpModule->mddCore.mtdOnDestroy.lpParam = lpOnDestroyParam;

	lpModule->mddCore.mtdOnLaunch.lpfnCallBack = (LPFN_MODULE_DISPATCH_PROC)dlsym(lpModule->hlLib, (LPCSTR)"OnLaunch");
	lpModule->mddCore.mtdOnAlarm.lpfnCallBack = (LPFN_MODULE_DISPATCH_PROC)dlsym(lpModule->hlLib, (LPCSTR)"OnAlarm");
	lpModule->mddCore.mtdOnError.lpfnCallBack = (LPFN_MODULE_DISPATCH_PROC)dlsym(lpModule->hlLib, (LPCSTR)"OnError");
	lpModule->mddCore.mtdOnEvent.lpfnCallBack = (LPFN_MODULE_DISPATCH_PROC)dlsym(lpModule->hlLib, (LPCSTR)"OnEvent");
	lpModule->mddCore.mtdOnMessagePass.lpfnCallBack = (LPFN_MODULE_DISPATCH_PROC)dlsym(lpModule->hlLib, (LPCSTR)"OnMessagePass");

	lpModule->lpszModuleFile = GlobalAllocAndZero(StringLength(lpcszModuleFile) + 1);
	CopyMemory(
		lpModule->lpszModuleFile,
		lpcszModuleFile,
		StringLength(lpcszModuleFile)
	);


	lpModule->lpUserData = lpUserData;
	lpModule->bCompiledFromSource = FALSE;

	/* Cast to ULONG to remove warning about not handling LOCK_TYPE_SIZE */
	switch ((ULONG)ltLock)
	{

		case LOCK_TYPE_INVALID:
		{
			lpModule->hLock = CreateMutex();
			if (NULL_OBJECT == lpModule->hLock)
			{
				dlclose(lpModule->hlLib);
				FreeMemory(lpModule);
				return NULL_OBJECT;
			}
			break;
		}

		case LOCK_TYPE_CRITICAL_SECTION:
		{
			lpModule->hLock = (NULL == ulLockFlags) ? 
						CreateCriticalSection() : 
						CreateCriticalSectionAndSpecifySpinCount(ulLockFlags);
			if (NULL_OBJECT == lpModule->hLock)
			{
				dlclose(lpModule->hlLib);
				FreeMemory(lpModule);
				return NULL_OBJECT;
			}
			break;
		}

		case LOCK_TYPE_EVENT:
		{
			lpModule->hLock = CreateEvent((BOOL)ulLockFlags);
			if (NULL_OBJECT == lpModule->hLock)
			{
				dlclose(lpModule->hlLib);
				FreeMemory(lpModule);
				return NULL_OBJECT;
			}
			break;
		}

		case LOCK_TYPE_MUTEX:
		{
			lpModule->hLock = CreateMutex();
			if (NULL_OBJECT == lpModule->hLock)
			{
				dlclose(lpModule->hlLib);
				FreeMemory(lpModule);
				return NULL_OBJECT;
			}
			break;
		}

		case LOCK_TYPE_SEMAPHORE:
		{
			lpModule->hLock = (NULL == ulLockFlags) ?
						CreateSemaphore(1U) :
						CreateSemaphore(ulLockFlags);
			if (NULL_OBJECT == lpModule->hLock)
			{
				dlclose(lpModule->hlLib);
				FreeMemory(lpModule);
				return NULL_OBJECT;
			}
			break;
		}

		case LOCK_TYPE_SPIN_LOCK:
		{
			lpModule->hLock = CreateSpinLock();
			if (NULL_OBJECT == lpModule->hLock)
			{
				dlclose(lpModule->hlLib);
				FreeMemory(lpModule);
				return NULL_OBJECT;
			}
			break;
		}

	}	

	/* If for whatever reason the lock still happens to be NULLPTR, we need to exit! */
	if (NULL_OBJECT == lpModule->hLock)
	{
		dlclose(lpModule->hlLib);
		FreeMemory(lpModule);
		return NULL_OBJECT;
	}

	hModule = CreateHandleWithSingleInheritor(
		lpModule,
		&(lpModule->hThis),
		HANDLE_TYPE_MODULE,
		__DestroyModule,
		NULL,
		(LPFN_HANDLE_ERROR)lpModule->mddCore.mtdOnError.lpfnCallBack,
		NULL,
		(LPFN_HANDLE_ALARM)lpModule->mddCore.mtdOnAlarm.lpfnCallBack,
		NULL,
		&(lpModule->ullId),
		NULL
	);

	if (NULL_OBJECT == hModule)
	{
		dlclose(lpModule->hlLib);
		FreeMemory(lpModule);
		return NULL_OBJECT;
	}

	__RegisterModule(lpModule);

	
	if (NULLPTR != lpModule->mddCore.mtdOnCreate.lpfnCallBack)
	{ lpModule->mddCore.mtdOnCreate.lpfnCallBack(hModule, lpModule->mddCore.mtdOnCreate.lpParam); }

	return hModule;
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPVOID
LaunchModule(
	_In_ 		HANDLE RESTRICT		hModule,
	_In_Opt_ 	LPVOID RESTRICT		lpParam,
	_In_ 		BOOL 			bLaunchOnSeparateThread
){
	EXIT_IF_UNLIKELY_NULL(hModule, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hModule), HANDLE_TYPE_MODULE, NULLPTR);
	LPMODULE lpModule;
	LPMODULE_THUNK_DISPATCH_PARAM lpmtdpParam;
	lpModule = (LPMODULE)GetHandleDerivative(hModule);
	EXIT_IF_UNLIKELY_NULL(lpModule, NULLPTR);

	if (FALSE == bLaunchOnSeparateThread)
	{ return lpModule->mddCore.mtdOnLaunch.lpfnCallBack(hModule, (NULLPTR != lpParam) ? lpParam : lpModule->mddCore.mtdOnLaunch.lpParam); }

	lpmtdpParam = (LPMODULE_THUNK_DISPATCH_PARAM)LocalAllocAndZero(sizeof(MODULE_THUNK_DISPATCH_PARAM));
	/* cppcheck-suppress memleak */
	EXIT_IF_UNLIKELY_NULL(lpmtdpParam, NULLPTR);

	lpmtdpParam->hModule = hModule;
	lpmtdpParam->mcbdCallBack = lpModule->mddCore.mtdOnLaunch;

	return (LPVOID)CreateThread(__ThunkDispatchThreadProc, (LPVOID)lpmtdpParam, NULLPTR);
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPVOID
CallModuleProc(
	_In_ 		HANDLE RESTRICT		hModule,
	_In_ 		ULONG 			ulDescriptor,
	_In_Opt_ 	LPVOID RESTRICT		lpParam
){
	EXIT_IF_UNLIKELY_NULL(hModule, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hModule), HANDLE_TYPE_MODULE, NULLPTR);
	LPMODULE lpModule;
	lpModule = (LPMODULE)GetHandleDerivative(hModule);
	EXIT_IF_UNLIKELY_NULL(lpModule, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpModule->mddCore.lpmtdTrampolineThunkDispatchDescriptorTable[ulDescriptor].lpfnCallBack, NULLPTR);

	return (NULLPTR != lpParam) ? 
			lpModule->mddCore.lpmtdTrampolineThunkDispatchDescriptorTable[ulDescriptor].lpfnCallBack(hModule, lpParam) :
			lpModule->mddCore.lpmtdTrampolineThunkDispatchDescriptorTable[ulDescriptor].lpfnCallBack(
				hModule,
				lpModule->mddCore.lpmtdTrampolineThunkDispatchDescriptorTable[ulDescriptor].lpParam
			);
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPVOID 
CallModuleProcByName(
	_In_ 		HANDLE RESTRICT		hModule,
	_In_Z_ 		LPCSTR RESTRICT		lpcszProcName,
	_In_		LPVOID RESTRICT		lpParam
){
	EXIT_IF_UNLIKELY_NULL(hModule, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hModule), HANDLE_TYPE_MODULE, NULLPTR);
	LPMODULE lpModule;
	LPFN_MODULE_DISPATCH_PROC lpfnCallBack;
	lpModule = (LPMODULE)GetHandleDerivative(hModule);
	EXIT_IF_UNLIKELY_NULL(lpModule, NULLPTR);

	lpfnCallBack = (LPFN_MODULE_DISPATCH_PROC)dlsym(lpModule->hlLib, lpcszProcName);
	EXIT_IF_UNLIKELY_NULL(lpfnCallBack, NULLPTR);
	return lpfnCallBack(hModule, lpParam);
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPFN_MODULE_DISPATCH_PROC
GetModuleProc(
	_In_ 		HANDLE RESTRICT		hModule,
	_In_ 		ULONG 			ulDescriptor
){
	EXIT_IF_UNLIKELY_NULL(hModule, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hModule), HANDLE_TYPE_MODULE, NULLPTR);
	LPMODULE lpModule;
	lpModule = (LPMODULE)GetHandleDerivative(hModule);
	EXIT_IF_UNLIKELY_NULL(lpModule, NULLPTR);
	return lpModule->mddCore.lpmtdTrampolineThunkDispatchDescriptorTable[ulDescriptor].lpfnCallBack;
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPFN_MODULE_DISPATCH_PROC
GetModuleProcByName(
	_In_ 		HANDLE RESTRICT		hModule,
	_In_Z_ 		LPCSTR RESTRICT		lpcszProcName
){
	EXIT_IF_UNLIKELY_NULL(hModule, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hModule), HANDLE_TYPE_MODULE, NULLPTR);
	LPMODULE lpModule;
	lpModule = (LPMODULE)GetHandleDerivative(hModule);
	EXIT_IF_UNLIKELY_NULL(lpModule, NULLPTR);
	return (LPFN_MODULE_DISPATCH_PROC)dlsym(lpModule->hlLib, lpcszProcName);
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPVOID
GetModuleRawProcByName(
	_In_ 		HANDLE RESTRICT		hModule,
	_In_Z_ 		LPCSTR RESTRICT		lpcszProcName
){
	EXIT_IF_UNLIKELY_NULL(hModule, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hModule), HANDLE_TYPE_MODULE, NULLPTR);
	LPMODULE lpModule;
	lpModule = (LPMODULE)GetHandleDerivative(hModule);
	EXIT_IF_UNLIKELY_NULL(lpModule, NULLPTR);
	return (LPVOID)dlsym(lpModule->hlLib, lpcszProcName);
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
RegisterModuleThunk(
	_In_ 		HANDLE RESTRICT		hModule,
	_In_ 		ULONG 			ulDescriptor,
	_In_Z_ 		LPCSTR RESTRICT		lpcszProcName,
	_In_Opt_ 	LPVOID RESTRICT		lpParam
){
	EXIT_IF_UNLIKELY_NULL(hModule, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpcszProcName, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hModule), HANDLE_TYPE_MODULE, FALSE);
	LPMODULE lpModule;
	lpModule = (LPMODULE)GetHandleDerivative(hModule);
	EXIT_IF_UNLIKELY_NULL(lpModule, FALSE);

	lpModule->mddCore.lpmtdTrampolineThunkDispatchDescriptorTable[ulDescriptor].lpfnCallBack = dlsym(lpModule->hlLib, lpcszProcName);
	lpModule->mddCore.lpmtdTrampolineThunkDispatchDescriptorTable[ulDescriptor].lpParam = lpParam;
	return (NULLPTR != lpModule->mddCore.lpmtdTrampolineThunkDispatchDescriptorTable[ulDescriptor].lpfnCallBack) ? TRUE : FALSE;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
RegisterModuleThunkByProc(
	_In_ 		HANDLE RESTRICT			hModule,
	_In_ 		ULONG 				ulDescriptor,
	_In_ 		LPFN_MODULE_DISPATCH_PROC 	lpfnProc,
	_In_Opt_ 	LPVOID RESTRICT			lpParam
){
	EXIT_IF_UNLIKELY_NULL(hModule, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpfnProc, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hModule), HANDLE_TYPE_MODULE, FALSE);
	LPMODULE lpModule;
	lpModule = (LPMODULE)GetHandleDerivative(hModule);
	EXIT_IF_UNLIKELY_NULL(lpModule, FALSE);

	lpModule->mddCore.lpmtdTrampolineThunkDispatchDescriptorTable[ulDescriptor].lpfnCallBack = lpfnProc;
	lpModule->mddCore.lpmtdTrampolineThunkDispatchDescriptorTable[ulDescriptor].lpParam = lpParam;
	return TRUE;
}




_Success_(return != NULLPTR, _Interlocked_Operation_)
PODNET_API
LPVOID
GetModuleUserData(
	_In_ 		HANDLE RESTRICT			hModule
){
	EXIT_IF_UNLIKELY_NULL(hModule, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hModule), HANDLE_TYPE_MODULE, FALSE);
	LPMODULE lpModule;
	lpModule = (LPMODULE)GetHandleDerivative(hModule);
	EXIT_IF_UNLIKELY_NULL(lpModule, FALSE);
	return InterlockedAcquire(&(lpModule->lpUserData));
}




_Success_(return != FALSE, _Interlocked_Operation_)
PODNET_API
BOOL
SetModuleUserData(
	_In_ 		HANDLE RESTRICT			hModule,
	_In_ 		LPVOID RESTRICT			lpUserData
){
	EXIT_IF_UNLIKELY_NULL(hModule, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hModule), HANDLE_TYPE_MODULE, FALSE);
	LPMODULE lpModule;
	lpModule = (LPMODULE)GetHandleDerivative(hModule);
	EXIT_IF_UNLIKELY_NULL(lpModule, FALSE);
	return (BOOL)InterlockedAcquireExchangeHappened(&(lpModule->lpUserData), lpUserData);
}



#endif
