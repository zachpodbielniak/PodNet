/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CMODULE_H
#define CMODULE_H


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../CHandle/CHandle.h"
#include "../CLock/CLock.h"
#include "../CThread/CThread.h"



#ifndef MODULE_DEFS
#define MODULE_DEFS

#define INHERITS_FROM_MODULE()					HANDLE 		hModule


#define INHERITS_FROM_MODULE_DESCRIPTOR_TABLE(NAME)		NAME##_ON_CREATE 	= 0x00, \
								NAME##_ON_DESTROY 	= 0x01, \
								NAME##_ON_LAUNCH 	= 0x02, \
								NAME##_ON_ERROR 	= 0x03, \
								NAME##_ON_ALARM 	= 0x04, \
								NAME##_ON_EVENT 	= 0x05, \
								NAME##_ON_MESSAGE_PASS 	= 0x06, \
								NAME##_GET_MODULE_INFO 	= 0x07, \


#define END_OF_MODULE_DESCRIPTOR_TABLE(NAME) 			NAME##_DESCRIPTOR_SIZE


#define DEFINE_MODULE_DESCRIPTOR_TABLE(NAME,...)		typedef enum __MODULE_##NAME##_DESCRIPTOR 		\
								{							\
									INHERITS_FROM_MODULE_DESCRIPTOR_TABLE(NAME)	\
									__VA_ARGS__,					\
									END_OF_MODULE_DESCRIPTOR_TABLE(NAME)		\
								} MODULE_##NAME##_DESCRIPTOR


typedef struct __MODULE_EVENT_DESCRIPTOR
{
	INHERITS_FROM_MODULE();
	HANDLE 		hLock;	/* The lock that should be acquired to handle the data, if any */
	HANDLE 		hEvent;	/* HANDLE to underlying EVENT to signal that it was handled */
	LPVOID 		lpData;
} MODULE_EVENT_DESCRIPTOR, *LPMODULE_EVENT_DESCRIPTOR, **DLPMODULE_EVENT_DESCRIPTOR;

#endif




typedef _Call_Back_ LPVOID(* LPFN_MODULE_DISPATCH_PROC)(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
);


typedef LPFN_MODULE_DISPATCH_PROC				*DLPFN_MODULE_DISPATCH_PROC, **TLPFN_MODULE_DISPATCH_PROC;




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HANDLE
LoadModule(
	_In_ 		LPCSTR RESTRICT		lpcszModuleFile,
	_In_Opt_ 	LPVOID RESTRICT		lpOnCreateParam,
	_In_Opt_ 	LPVOID RESTRICT		lpOnDestroyParam,
	_In_ 		LOCK_TYPE 		ltLock,
	_In_Opt_ 	ULONG 			ulLockFlags,
	_In_Opt_ 	LPVOID RESTRICT		lpUserData
);




_Success_(return != NULLPTR, _Non_Locking_)
_When_((TRUE == bLaunchOnSeparateThread) { typeof(return) = HANDLE  && _Spawns_New_Thread_ })
PODNET_API
LPVOID
LaunchModule(
	_In_ 		HANDLE RESTRICT		hModule,
	_In_Opt_ 	LPVOID RESTRICT		lpParam,
	_In_ 		BOOL 			bLaunchOnSeparateThread
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPVOID
CallModuleProc(
	_In_ 		HANDLE RESTRICT		hModule,
	_In_ 		ULONG 			ulDescriptor,
	_In_Opt_ 	LPVOID RESTRICT		lpParam
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPVOID 
CallModuleProcByName(
	_In_ 		HANDLE RESTRICT		hModule,
	_In_Z_ 		LPCSTR RESTRICT		lpcszProcName,
	_In_		LPVOID RESTRICT		lpParam
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPFN_MODULE_DISPATCH_PROC
GetModuleProc(
	_In_ 		HANDLE RESTRICT		hModule,
	_In_ 		ULONG 			ulDescriptor
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPFN_MODULE_DISPATCH_PROC
GetModuleProcByName(
	_In_ 		HANDLE RESTRICT		hModule,
	_In_Z_ 		LPCSTR RESTRICT		lpcszProcName
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPVOID
GetModuleRawProcByName(
	_In_ 		HANDLE RESTRICT		hModule,
	_In_Z_ 		LPCSTR RESTRICT		lpcszProcName
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
RegisterModuleThunk(
	_In_ 		HANDLE RESTRICT		hModule,
	_In_ 		ULONG 			ulDescriptor,
	_In_Z_ 		LPCSTR RESTRICT		lpcszProcName,
	_In_Opt_ 	LPVOID RESTRICT		lpParam
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
RegisterModuleThunkByProc(
	_In_ 		HANDLE RESTRICT			hModule,
	_In_ 		ULONG 				ulDescriptor,
	_In_ 		LPFN_MODULE_DISPATCH_PROC 	lpfnProc,
	_In_Opt_ 	LPVOID RESTRICT			lpParam
);




_Success_(return != NULLPTR, _Interlocked_Operation_)
PODNET_API
LPVOID
GetModuleUserData(
	_In_ 		HANDLE RESTRICT			hModule
);




_Success_(return != FALSE, _Interlocked_Operation_)
PODNET_API
BOOL
SetModuleUserData(
	_In_ 		HANDLE RESTRICT			hModule,
	_In_ 		LPVOID RESTRICT			lpUserData
);



#endif