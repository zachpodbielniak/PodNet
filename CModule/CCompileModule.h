/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CCOMPILEMODULE_H
#define CCOMPILEMODULE_H


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../CHandle/CHandle.h"
#include "../CLock/CLock.h"
#include "../CProcess/CProcess.h"
#include "../CString/CString.h"



typedef enum __COMPILE_AND_LOAD_MODULE_FLAGS
{
	COMPILE_AND_LOAD_MODULE_DRY_RUN				= 1 << 0,
	COMPILE_AND_LOAD_MODULE_PRINT_COMPILE_INVOCATION	= 1 << 1
} COMPILE_AND_LOAD_MODULE_FLAGS;




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HANDLE
CompileAndLoadModule(
	_In_ 		LPCSTR 			lpcszSourceFile,
	_In_Opt_Z_ 	LPCSTR 			lpcszCompilationOptions,
	_In_Opt_ 	LPVOID 			lpOnCreateParam,
	_In_Opt_ 	LPVOID 			lpOnDestroyParam,
	_In_ 		LOCK_TYPE 		ltLock,
	_In_Opt_ 	ULONG 			ulLockFlags,
	_In_Opt_ 	LPVOID 			lpUserData,
	_In_ 		BOOL 			bVerbose
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HANDLE
CompileAndLoadModuleCached(
	_In_ 		LPCSTR 			lpcszSourceFile,
	_In_Opt_Z_ 	LPCSTR 			lpcszCompilationOptions,
	_In_Opt_ 	LPVOID 			lpOnCreateParam,
	_In_Opt_ 	LPVOID 			lpOnDestroyParam,
	_In_ 		LOCK_TYPE 		ltLock,
	_In_Opt_ 	ULONG 			ulLockFlags,
	_In_Opt_ 	LPVOID 			lpUserData,
	_In_Opt_	ULONGLONG		ullCacheHash,
	_In_ 		BOOL 			bVerbose
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HANDLE
CompileAndLoadModuleCachedEx(
	_In_ 		LPCSTR 			lpcszSourceFile,
	_In_Opt_Z_ 	LPCSTR 			lpcszCompilationOptions,
	_In_Opt_ 	LPVOID 			lpOnCreateParam,
	_In_Opt_ 	LPVOID 			lpOnDestroyParam,
	_In_ 		LOCK_TYPE 		ltLock,
	_In_Opt_ 	ULONG 			ulLockFlags,
	_In_Opt_ 	LPVOID 			lpUserData,
	_In_Opt_	ULONGLONG		ullCacheHash,
	_Reserved_	LPVOID			lpReserved,
	_In_Opt_	ULONGLONG		ullFlags,
	_In_ 		BOOL 			bVerbose
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HANDLE
AssembleAndLoadModuleX64(
	_In_ 		LPCSTR 			lpcszSourceFile,
	_In_Opt_ 	LPVOID 			lpOnCreateParam,
	_In_Opt_ 	LPVOID 			lpOnDestroyParam,
	_In_ 		LOCK_TYPE 		ltLock,
	_In_Opt_ 	ULONG 			ulLockFlags,
	_In_Opt_ 	LPVOID 			lpUserData
);


#endif