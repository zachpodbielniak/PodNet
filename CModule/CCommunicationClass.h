/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CCOMMCLASS_H
#define CCOMMCLASS_H


#include "CModule.h"


#define INHERITS_FROM_COMM_MODULE_DESCRIPTOR_TABLE(NAME)	\
	NAME##_ON_SEND,						\
	NAME##_ON_RECEIVE,					\
	NAME##_ON_CONNECT,					\
	NAME##_GET_CONNECTION,					\
	NAME##_CONNECTION_INFO,					\
	NAME##_GET_COMM_INFO,


#define DEFINE_COMM_MODULE_DESCRIPTOR_TABLE(NAME,...)		\
	typedef enum __MODULE_##NAME##_DESCRIPTOR		\
	{							\
		INHERITS_FROM_MODULE_DESCRIPTOR_TABLE(NAME)	\
		INHERITS_FROM_COMM_MODULE_DESCRIPTOR_TABLE(NAME)\
		__VA_ARGS__,					\
		END_OF_MODULE_DESCRIPTOR_TABLE(NAME)		\
	} MODULE_##NAME##_DESCRIPTOR;


#define REGISTER_COMM_MODULE_THUNKS(HMODULE,NAME)						\
	RegisterModuleThunk(HMODULE, NAME##_ON_SEND, "OnSend", NULLPTR);			\
	RegisterModuleThunk(HMODULE, NAME##_ON_RECEIVE, "OnReceive", NULLPTR);			\
	RegisterModuleThunk(HMDOULE, NAME##_ON_CONNECT, "OnConnect", NULLPTR); 			\
	RegisterModuleThunk(HMODULE, NAME##_GET_CONNECTION, "GetConnection", NULLPTR);		\
	RegisterModuleThunk(HMODULE, NAME##_CONNECTION_INFO, "GetConnectionInfo", NULLPTR); 	\
	RegisterModuleThunk(HMODULE, NAME##_GET_COMM_INFO, "GetCommunicationInfo", NULLPTR)




_Success_(_Inexpressible_, _Inexpressible_)
PODNET_MODULE
VIRTUAL
LPVOID
OnSend(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
);




_Success_(_Inexpressible_, _Inexpressible_)
PODNET_MODULE
VIRTUAL
LPVOID
OnReceive(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
);




_Success_(_Inexpressible_, _Inexpressible_)
PODNET_MODULE
VIRTUAL
LPVOID
OnConnect(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
);




_Success_(_Inexpressible_, _Inexpressible_)
PODNET_MODULE
VIRTUAL
LPVOID
GetConnection(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
);




_Success_(_Inexpressible_, _Inexpressible_)
PODNET_MODULE
VIRTUAL
LPVOID
GetConnectionInfo(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
);




_Success_(_Inexpressible_, _Inexpressible_)
PODNET_MODULE
VIRTUAL
LPVOID
GetCommunicationInfo(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
);




#endif