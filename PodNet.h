/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef PODNET_H
#define PODNET_H





/* Most minimal PodNet instance */
#ifdef PODNET_BASICS


#include "Prereqs.h"
#include "TypeDefs.h"
#include "Defs.h"
#include "Macros.h"


/* Most common sub components */
#elif PODNET_LEAN_AND_MEAN

#include "Prereqs.h"
#include "TypeDefs.h"
#include "Defs.h"
#include "Macros.h"

/* Error Handling */
#include "CError/CError.h"

/* Clocks */
#include "CClock/CClock.h"

/* Containers */
#include "CContainers/CContainers.h"

/* HANDLE */
#include "CHandle/CHandle.h"
#include "CHandle/CHandleTable.h"

/* Files */
#include "CFile/CFile.h"

/* Locks */
#include "CLock/CCritSec.h"
#include "CLock/CEvent.h"
#include "CLock/CMutex.h"
#include "CLock/CSemaphore.h"
#include "CLock/CSpinLock.h"

/* Threads */
#include "CThread/CThread.h"

/* System */
#include "CSystem/CSystem.h"

#elif PODNET_LEAN 

#include "Prereqs.h"
#include "TypeDefs.h"
#include "Defs.h"
#include "Macros.h"

/* Error Handling */
#include "CError/CError.h"

/* Clocks */
#include "CClock/CClock.h"

/* Containers */
#include "CContainers/CContainers.h"

/* HANDLE */
#include "CHandle/CHandle.h"
#include "CHandle/CHandleTable.h"

/* Concurrent Stuff */
#include "CConcurrent/CAsync.h"

/* Atoms */
#include "CAtom/CAtomTable.h"

/* Files */
#include "CFile/CFile.h"

/* Logs */
#include "CLog/CLog.h"

/* Locks */
#include "CLock/CCritSec.h"
#include "CLock/CEvent.h"
#include "CLock/CMutex.h"
#include "CLock/CSemaphore.h"
#include "CLock/CSpinLock.h"

/* Threads */
#include "CThread/CThread.h"

/* System */
#include "CSystem/CSystem.h"

#else


#include "Prereqs.h"
#include "TypeDefs.h"
#include "Defs.h"
#include "Macros.h"



#endif







#endif