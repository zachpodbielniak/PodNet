/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CERROR_H
#define CERROR_H

#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "CErrorCodes.h"




/****f* CError/CError.h/GetLastGlobalError
 * NAME
 * 	GetLastGlobalError -- Get the value of the last global error.
 * COPYRIGHT 
 * 	Copyright (C) 2020 Zach Podbielniak 
 * AUTHOR 
 * 	Zach Podbielniak 
 * SYNOPSIS
 * 	_Result_Null_On_Failure_
 * 	PODNET_API
 * 	ULONG 
 * 	GetLastGlobalError(
 * 		VOID
 * 	);
 * FUNCTION 
 * 	Returns the last global error value for the process.
 * INPUTS
 * 	VOID
 * RESULT 
 * 	ULONG -- A ULONG indicating the last global error for the process.
 ******
 *
 */
_Result_Null_On_Failure_
PODNET_API
ULONG
GetLastGlobalError(
        VOID
);




/****f* CError/CError.h/SetLastGlobalError
 * NAME
 * 	SetLastGlobalError -- Set the value of the last global error.
 * COPYRIGHT 
 * 	Copyright (C) 2020 Zach Podbielniak 
 * AUTHOR 
 * 	Zach Podbielniak 
 * SYNOPSIS
 *	_Success_(return != FALSE, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Critical_Section_)))
 * 	PODNET_API
 * 	BOOL
 * 	SetLastGlobalError(
 * 		_In_ 		ULONG 		ulError
 * 	);
 * FUNCTION 
 * 	Sets the last global error value for the process.
 * INPUTS
 * 	ulError:
 * 		A ULONG representing the last error for the process.
 * RESULT 
 * 	BOOL -- Returns FALSE on error, and !(FALSE) for success.
 ******
 *
 */
_Success_(return != FALSE, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Critical_Section_)))
PODNET_API
BOOL
SetLastGlobalError(
        _In_                                            ULONG                   ulError
);


#endif