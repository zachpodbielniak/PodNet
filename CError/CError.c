/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CError.h"


GLOBAL_VARIABLE VOLATILE ULONG  ulLastError = 0;


_Result_Null_On_Failure_
PODNET_API
ULONG
GetLastGlobalError(
        VOID
){ return InterlockedAcquire(&ulLastError); }


_Success_(return != FALSE, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Critical_Section_)))
PODNET_API
BOOL
SetLastGlobalError(
        _In_                                    ULONG           ulError
){ return (BOOL)InterlockedCompareExchangeHappened(&ulLastError, InterlockedAcquire(&ulLastError), ulError); }