/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/



#ifndef CERRORCODES_H
#define CERRORCODES_H



/****d* CError/CErrorCodes.h/GenericErrors
 * NAME 
 * 	Generic Errors
 * COPYRIGHT 
 * 	Copyright (C) 2020 Zach Podbielniak
 * AUTHOR 
 * 	Zach Podbielniak 
 * SOURCE 
 */
#define ERROR_HANDLE_SUPPLIED_WAS_NULL                                          0x0001
#define ERROR_REQUIRED_IN_PARAMETER_WAS_NULL                                    0x0002
#define ERROR_REQUIRED_OUT_PARAMETER_WAS_NULL                                   0x0003
#define ERROR_HANDLE_DERIVATIVE_WAS_NULL                                        0x0004
/*
 * SEE ALSO 
 * 	SetLastGlobalError, GetLastGlobalError
 ******/




/****d* CError/CErrorCodes.h/GpioPinErrors
 * NAME 
 * 	GPIO Pin Errors
 * COPYRIGHT 
 * 	Copyright (C) 2020 Zach Podbielniak
 * AUTHOR 
 * 	Zach Podbielniak 
 * SOURCE
 */
#define ERROR_GPIO_HANDLE_SUPPLIED_WAS_NULL                                     0x0040
#define ERROR_GPIO_DATA_COULD_NOT_BE_ALLOC                                      0x0041
#define ERROR_GPIO_DATA_WAS_NULL                                                0x0042
#define ERROR_GPIO_REQUIRED_ARGUMENT_WAS_NULL                                   0x0043
#define ERROR_GPIO_COULD_NOT_CREATE_SPIN_LOCK                                   0x0044
/*
 * SEE ALSO 
 * 	SetLastGlobalError, GetLastGlobalError
 ******/




/****d* CError/CErrorCodes.h/AtomTableErrors
 * NAME 
 * 	Atom Table Errors
 * COPYRIGHT 
 * 	Copyright (C) 2020 Zach Podbielniak
 * AUTHOR 
 * 	Zach Podbielniak 
 * SOURCE
 */
#define ERROR_ATOM_TABLE_HANDLE_SUPPLIED_WAS_NULL                               0x0050
#define ERROR_ATOM_TABLE_COULD_NOT_CREATE_HANDLE                                0x0051
#define ERROR_ATOM_TABLE_COULD_ALLOC_ATOM_BUFFER                                0x0052
#define ERROR_ATOM_TABLE_ATOM_BUFFER_WAS_NULL                                   0x0053
#define ERROR_ATOM_NON_EXISTENT                                                 0x0054
#define ERROR_ATOM_TABLE_COULD_NOT_BE_ALLOC                                     0x0055
#define ERROR_ATOM_TABLE_COULD_NOT_ALLOC_TABLE                                  0x0056
#define ERROR_ATOM_TABLE_NO_STRING_PROVIDED                                     0x0057
#define ERROR_ATOM_TABLE_STRING_PROVIDED_WAS_LARGER_THAN_ATOM_BUFFER            0x0058
#define ERROR_ATOM_TABLE_HANDLE_SUPPLIED_WAS_NOT_ATOM_TABLE_HANDLE              0x0059
#define ERROR_ATOM_TABLE_NON_EXISTENT                                           0x005A
#define ERROR_ATOM_TABLE_FULL                                                   0x005B
/*
 * SEE ALSO 
 * 	SetLastGlobalError, GetLastGlobalError
 ******/




/****d* CError/CErrorCodes.h/ClockErrors
 * NAME 
 * 	Clock Errors
 * COPYRIGHT 
 * 	Copyright (C) 2020 Zach Podbielniak
 * AUTHOR 
 * 	Zach Podbielniak 
 * SOURCE
 */
#define ERROR_CLOCK_NOT_DEFINED                                                 0x0070
/*
 * SEE ALSO 
 * 	SetLastGlobalError, GetLastGlobalError
 ******/




/****d* CError/CErrorCodes.h/MutexErrors
 * NAME 
 * 	Mutex Errors
 * COPYRIGHT 
 * 	Copyright (C) 2020 Zach Podbielniak
 * AUTHOR 
 * 	Zach Podbielniak 
 * SOURCE
 */
#define ERROR_MUTEX_HANDLE_SUPPLIED_WAS_NULL                                    0x0170
#define ERROR_MUTEX_HANDLE_SUPPLIED_WAS_NOT_MUTEX_HANDLE                        0x0171
#define ERROR_MUTEX_DATA_IS_NON_EXISTENT                                        0x0172
#define ERROR_MUTEX_COULD_NOT_ALLOC_DATA                                        0x0173
#define ERROR_MUTEX_COULD_NOT_CREATE_HANDLE                                     0x0174
#define ERROR_MUTEX_THREAD_ATTEMPTING_UNLOCK_WAS_NOT_LOCKING_THREAD             0x0175
/*
 * SEE ALSO 
 * 	SetLastGlobalError, GetLastGlobalError
 ******/




/****d* CError/CErrorCodes.h/EventErrors
 * NAME 
 * 	Event Errors
 * COPYRIGHT 
 * 	Copyright (C) 2020 Zach Podbielniak
 * AUTHOR 
 * 	Zach Podbielniak 
 * SOURCE
 */
#define ERROR_EVENT_HANDLE_SUPPLIED_WAS_NULL                                    0x0180
#define ERROR_EVENT_HANDLE_SUPPLIED_WAS_NOT_EVENT_HANDLE                        0x0181
#define ERROR_EVENT_DATA_IS_NON_EXISTENT                                        0x0182
#define ERROR_EVENT_COULD_NOT_ALLOC_DATA                                        0x0183
#define ERROR_EVENT_COULD_NOT_CREATE_HANDLE                                     0x0184
#define ERROR_EVENT_COULD_NOT_BE_CREATED                                        0x0270
#define ERROR_EVENT_HANDLE_SUPPLIED_IS_NOT_EVENT_HANDLE_TYPE                    0x0271
#define ERROR_EVENT_HANDLE_COULD_NOT_BE_CREATED                                 0x0272
/*
 * SEE ALSO 
 * 	SetLastGlobalError, GetLastGlobalError
 ******/




/****d* CError/CErrorCodes.h/SpinLockErrors
 * NAME 
 * 	SpinLock Errors
 * COPYRIGHT 
 * 	Copyright (C) 2020 Zach Podbielniak
 * AUTHOR 
 * 	Zach Podbielniak 
 * SOURCE
 */
#define ERROR_SPINLOCK_HANDLE_SUPPLIED_WAS_NULL                                 0x0190
#define ERROR_SPINLOCK_HANDLE_SUPPLIED_IS_NOT_SPINLOCK_HANDLE_TYPE              0x0191
#define ERROR_SPINLOCK_DATA_IS_NON_EXISTENT                                     0x0192
#define ERROR_SPINLOCK_COULD_NOT_ALLOC_DATA                                     0x0193
#define ERROR_SPINLOCK_COULD_NOT_CREATE_HANDLE                                  0x0194
/*
 * SEE ALSO 
 * 	SetLastGlobalError, GetLastGlobalError
 ******/




/****d* CError/CErrorCodes.h/LogErrors
 * NAME 
 * 	Log Errors
 * COPYRIGHT 
 * 	Copyright (C) 2020 Zach Podbielniak
 * AUTHOR 
 * 	Zach Podbielniak 
 * SOURCE
 */
#define ERROR_LOG_COULD_NOT_OPEN_FILE                                           0x0220
/*
 * SEE ALSO 
 * 	SetLastGlobalError, GetLastGlobalError
 ******/




/****d* CError/CErrorCodes.h/FileErrors
 * NAME 
 * 	File Errors
 * COPYRIGHT 
 * 	Copyright (C) 2020 Zach Podbielniak
 * AUTHOR 
 * 	Zach Podbielniak 
 * SOURCE
 */
#define ERROR_FILE_HANDLE_COULD_NOT_BE_OPENED                                   0x0250
#define ERROR_FILE_NOT_WRITABLE                                                 0x0251
#define ERROR_FILE_DOES_NOT_EXIST                                               0x0252
#define ERROR_FILE_COULD_NOT_ALLOCATE_OBJECT_FOR_STANDARD_STREAM                0x0253
#define ERROR_FILE_COULD_NOT_BE_CREATED                                         0x0254
#define ERROR_FILE_COULD_NOT_BE_OPENED                                          0x0255
#define ERROR_FILE_HANDLE_COULD_NOT_BE_CREATED                                  0x0256
#define ERROR_FILE_CRITICAL_SECTION_COULD_NOT_BE_CREATED                        0x0257
#define ERROR_FILE_CRITICAL_SECTION_DOES_NOT_EXIST                              0x0258
#define ERROR_FILE_CRITICAL_SECTION_COULD_NOT_BE_ENTERED                        0x0259
#define ERROR_FILE_CRITICAL_SECTION_COULD_NOT_BE_EXITED                         0x025A
#define ERROR_FILE_HANDLE_SUPPLIED_IS_NOT_FILE_HANDLE_TYPE                      0x025B
#define ERROR_FILE_NOT_OPENED_WITH_READ_FLAG                                    0x025C
#define ERROR_FILE_NOT_OPENED_WITH_WRITE_FLAG                                   0x025D
#define ERROR_FILE_NAME_NOT_DEFINED                                             0x025E
#define ERROR_FILE_IS_NOT_LEGIT_FILE                                            0x025F
#define ERROR_FILE_PROMISE_DATA_COULD_NOT_BE_ALLOC                              0x0260
#define ERROR_FILE_NO_BUFFER_SIZE_WAS_SUPPLIED                                  0x0261
/*
 * SEE ALSO 
 * 	SetLastGlobalError, GetLastGlobalError
 ******/




/****d* CError/CErrorCodes.h/PromiseFutureErrors
 * NAME 
 * 	Promise And Future Errors
 * COPYRIGHT 
 * 	Copyright (C) 2020 Zach Podbielniak
 * AUTHOR 
 * 	Zach Podbielniak 
 * SOURCE
 */
#define ERROR_BROKEN_PROMISE                                                    0x0300
#define ERROR_FUTURE_NOT_READY                                                  0x0301
#define ERROR_FUTURE_OUTPUT_BUFFER_NOT_ALLOC                                    0x0302
#define ERROR_PROMISE_NOT_SUPPLIED_WITH_DATA                                    0x0303
#define ERROR_PROMISE_NOT_SUPPLIED_WITH_ALLOCABLE_BUFFER                        0x0304
#define ERROR_PROMISE_COULD_NOT_ALLOC_BUFFER                                    0x0305
#define ERROR_PROMISE_COULD_NOT_BE_CREATED                                      0x0306
/*
 * SEE ALSO 
 * 	SetLastGlobalError, GetLastGlobalError
 ******/




#endif