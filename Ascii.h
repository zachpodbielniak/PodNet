/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef ASCII_HEADER
#define ASCII_HEADER

#define ASCII_NULL_TERMINATOR                           0x00
#define ASCII_START_OF_HEADING                          0x01
#define ASCII_START_OF_TEXT                             0x02
#define ASCII_END_OF_TEXT                               0x03
#define ASCII_END_OF_TRANSMISSION                       0x04
#define ASCII_ENQUIRY                                   0x05
#define ASCII_ACK                                       0x06
#define ASCII_BELL                                      0x07
#define ASCII_BACK_SPACE                                0x08
#define ASCII_HORIZONTAL_TAB                            0x09
#define ASCII_LINE_FEED                                 0x0A
#define ASCII_VERTICAL_TAB                              0x0B
#define ASCII_FORM_FEED                                 0x0C
#define ASCII_CARRIAGE_RETURN                           0x0D
#define ASCII_SHIFT_OUT                                 0x0E
#define ASCII_SHIFT_IN                                  0x0F
#define ASCII_DATA_LINE_ESCAPE                          0x10
#define ASCII_DEVICE_CONTROL_1                          0x11
#define ASCII_DEVICE_CONTROL_2                          0x12
#define ASCII_DEVICE_CONTROL_3                          0x13
#define ASCII_DEVICE_CONTROL_4                          0x14
#define ASCII_NEGATIVE_ACK                              0x15
#define ASCII_SYNCHRONOUS_IDLE                          0x16
#define ASCII_END_OF_BLOCK                              0x17
#define ASCII_CANCEL                                    0x18
#define ASCII_END_OF_MEDIUM                             0x19
#define ASCII_SUBSTITUTE                                0x1A
#define ASCII_ESCAPE                                    0x1B
#define ASCII_FILE_SEPARATOR                            0x1C
#define ASCII_GROUP_SEPARATOR                           0x1D
#define ASCII_RECORD_SEPARATOR                          0x1E
#define ASCII_UNIT_SEPARATOR                            0x1F
#define ASCII_SPACE                                     0x20
#define ASCII_EXCLAMATION_POINT                         0x21
#define ASCII_QUOTE                                     0x22
#define ASCII_POUND                                     0x23
#define ASCII_DOLLAR                                    0x24
#define ASCII_PERCENT                                   0x25
#define ASCII_AMPERSAND                                 0x26
#define ASCII_SINGLE_QUOTE                              0x27
#define ASCII_PARENTHESES_LEFT                          0x28
#define ASCII_PARENTHESES_RIGHT                         0x29
#define ASCII_ASTERISK                                  0x2A
#define ASCII_PLUS                                      0x2B
#define ASCII_COMMA                                     0x2C
#define ASCII_HYPHEN                                    0x2D
#define ASCII_PERIOD                                    0x2E
#define ASCII_FORWARD_SLASH                             0x2F
#define ASCII_0                                         0x30
#define ASCII_1                                         0x31
#define ASCII_2                                         0x32
#define ASCII_3                                         0x33
#define ASCII_4                                         0x34
#define ASCII_5                                         0x35
#define ASCII_6                                         0x36
#define ASCII_7                                         0x37
#define ASCII_8                                         0x38
#define ASCII_9                                         0x39
#define ASCII_COLON                                     0x3A
#define ASCII_SEMI_COLON                                0x3B
#define ASCII_LESS_THAN                                 0x3C
#define ASCII_EQUAL                                     0x3D
#define ASCII_GREATER_THAN                              0x3E
#define ASCII_QUESTION                                  0x3F
#define ASCII_AT                                        0x40
#define ASCII_A                                         0x41
#define ASCII_B                                         0x42
#define ASCII_C                                         0x43
#define ASCII_D                                         0x44
#define ASCII_E                                         0x45
#define ASCII_F                                         0x46
#define ASCII_G                                         0x47
#define ASCII_H                                         0x48
#define ASCII_I                                         0x49
#define ASCII_J                                         0x4A
#define ASCII_K                                         0x4B
#define ASCII_L                                         0x4C
#define ASCII_M                                         0x4D
#define ASCII_N                                         0x4E
#define ASCII_O                                         0x4F
#define ASCII_P                                         0x50
#define ASCII_Q                                         0x51
#define ASCII_R                                         0x52
#define ASCII_S                                         0x53
#define ASCII_T                                         0x54
#define ASCII_U                                         0x55
#define ASCII_V                                         0x56
#define ASCII_W                                         0x57
#define ASCII_X                                         0x58
#define ASCII_Y                                         0x59
#define ASCII_Z                                         0x5A
#define ASCII_LEFT_BRACE                                0x5B
#define ASCII_BACK_SLASH                                0x5C
#define ASCII_RIGHT_BRACE                               0x5D
#define ASCII_CARAT                                     0x5E
#define ASCII_UNDER_SCORE                               0x5F
#define ASCII_GRAVE                                     0x60
#define ASCII_A_LOWER                                   0x61
#define ASCII_B_LOWER                                   0x62
#define ASCII_C_LOWER                                   0x63
#define ASCII_D_LOWER                                   0x64
#define ASCII_E_LOWER                                   0x65
#define ASCII_F_LOWER                                   0x66
#define ASCII_G_LOWER                                   0x67
#define ASCII_H_LOWER                                   0x68
#define ASCII_I_LOWER                                   0x69
#define ASCII_J_LOWER                                   0x6A
#define ASCII_K_LOWER                                   0x6B
#define ASCII_L_LOWER                                   0x6C
#define ASCII_M_LOWER                                   0x6D
#define ASCII_N_LOWER                                   0x6E
#define ASCII_O_LOWER                                   0x6F
#define ASCII_P_LOWER                                   0x70
#define ASCII_Q_LOWER                                   0x71
#define ASCII_R_LOWER                                   0x72
#define ASCII_S_LOWER                                   0x73
#define ASCII_T_LOWER                                   0x74
#define ASCII_U_LOWER                                   0x75
#define ASCII_V_LOWER                                   0x76
#define ASCII_W_LOWER                                   0x77
#define ASCII_X_LOWER                                   0x78
#define ASCII_Y_LOWER                                   0x79
#define ASCII_Z_LOWER                                   0x7A
#define ASCII_LEFT_BRACKET                              0x7B
#define ASCII_PIPE                                      0x7C
#define ASCII_RIGHT_BRACKET                             0x7D
#define ASCII_TILDE                                     0x7E
#define ASCII_DELETE                                    0x7F
#define ASCII_EURO                                      0x80
#define ASCII_RIGHT_MOON                                0x81
#define ASCII_SINGLE_LOW_QUOTE                          0x82
#define ASCII_FUNCTION                                  0x83
#define ASCII_DOUBLE_LOW_QUOTE                          0x84
#define ASCII_ELLIPSIS                                  0x85
#define ASCII_DAGGER                                    0x86
#define ASCII_DOUBLE_DAGGER                             0x87
#define ASCII_HATCHEK                                   0x88
#define ASCII_PER_THOUSAND                              0x89
#define ASCII_CAPITAL_ESH                               0x8A
#define ASCII_LEFT_SINGLE_ANGLE_QUOTE                   0x8B
#define ASCII_OE_LIGATURE                               0x8C
#define ASCII_BOLD_RIGHT_BRACKET                        0x8D
#define ASCII_Z_HATCHEK                                 0x8E
#define ASCII_STOCK_CHART                               0x8F
#define ASCII_OIL_BARREL                                0x90
#define ASCII_LEFT_SINGLE_QUOTE                         0x91
#define ASCII_RIGHT_SINGLE_QUOTE                        0x92
#define ASCII_LEFT_DOUBLE_QUOTE                         0x93
#define ASCII_RIGHT_DOUBLE_QUOTE                        0x94
#define ASCII_SMALL_BULLET                              0x95
#define ASCII_EN_DASH                                   0x96
#define ASCII_EM_DASH                                   0x97
#define ASCII_TILDE_2                                   0x98
#define ASCII_TRADE_MARK                                0x99
#define ASCII_LOWERCASE_ESH                             0x9A
#define ASCII_RIGHT_SINGLE_ANGLE_QUOTE                  0x9B
#define ASCII_OE_LIGATURE_LOWER                         0x9C
#define ASCII_PAPER                                     0x9D
#define ASCII_Z_HATCHEK_LOWER                           0x9E
#define ASCII_Y_UMLAUT                                  0x9F
#define ASCII_NON_BREAKING_SPACE                        0xA0
#define ASCII_EXCLAMATION_POINT_INVERTED                0xA1 
#define ASCII_CENT                                      0xA2
#define ASCII_PENCE                                     0xA3
#define ASCII_CURRENCY                                  0xA4
#define ASCII_YEN                                       0xA5
#define ASCII_BROKEN_VERTICAL_BAR                       0xA6
#define ASCII_SECTION_SYMBOL                            0xA7
#define ASCII_UMLAUT                                    0xA8
#define ASCII_COPYRIGHT                                 0xA9
#define ASCII_SUPERSCRIPT_A_LOWER                       0xAA
#define ASCII_LEFT_ANGLE_QUOTE                          0xAB
#define ASCII_NOT_SIGN                                  0xAC
#define ASCII_SOFT_HYPEN                                0xAD
#define ASCII_REGISTERED_SIGN                           0xAE
#define ASCII_MACRON                                    0xAF
#define ASCII_DEGREE_SIGN                               0xB0
#define ASCII_PLUS_MINUS_SIGN                           0xB1
#define ASCII_SUPERSCRIPT_2                             0xB2
#define ASCII_SUPERSCRIPT_3                             0xB3
#define ASCII_ACUTE_ACCENT                              0xB4
#define ASCII_MICRO_SIGN                                0xB5
#define ASCII_PILCROW_SIGN                              0xB6
#define ASCII_MIDDLE_DOT                                0xB7
#define ASCII_CEDILLA                                   0xB8
#define ASCII_SUPERSCRIPT_1                             0xB9
#define ASCII_SUPERSCRIPT_O_LOWER                       0xBA
#define ASCII_RIGHT_ANGLE_QUOTE                         0xBB
#define ASCII_FRACTION_ONE_QUARTER                      0xBC
#define ASCII_FRACTION_ONE_HALF                         0xBD
#define ASCII_FRACTION_THREE_QUARTER                    0xBE
#define ASCII_QUESTION_MARK_INVERTED                    0xBF
#define ASCII_A_GRAVE_ACCENT                            0xC0
#define ASCII_A_ACUTE_ACCENT                            0xC1
#define ASCII_A_CIRCUMFLEX                              0xC2
#define ASCII_A_TILDE                                   0xC3
#define ASCII_A_UMLAUT                                  0xC4
#define ASCII_A_RING                                    0xC5
#define ASCII_AE_LIGATURE                               0xC6
#define ASCII_C_CEDILLA                                 0xC7
#define ASCII_E_GRAVE                                   0xC8
#define ASCII_E_ACUTE                                   0xC9
#define ASCII_E_CIRCUMFLEX                              0xCA
#define ASCII_E_UMLAUT                                  0xCB
#define ASCII_I_GRAVE                                   0xCC
#define ASCII_I_ACUTE                                   0xCD
#define ASCII_I_CIRCUMFLEX                              0xCE
#define ASCII_I_UMLAUT                                  0xCF
#define ASCII_ETH                                       0xD0
#define ASCII_N_TILDE                                   0xD1
#define ASCII_O_GRAVE                                   0xD2
#define ASCII_O_ACUTE                                   0xD3
#define ASCII_O_CIRCUMFLEX                              0xD4
#define ASCII_O_TILDE                                   0xD5
#define ASCII_O_UMLAUT                                  0xD6
#define ASCII_MULTIPLICATION_SIGN                       0xD7
#define ASCII_O_SLASH                                   0xD8
#define ASCII_U_GRAVE                                   0xD9
#define ASCII_U_ACUTE                                   0xDA
#define ASCII_U_CIRCUMFLEX                              0xDB
#define ASCII_U_UMLAUT                                  0xDC
#define ASCII_Y_ACUTE                                   0xDD
#define ASCII_THORN                                     0xDE
#define ASCII_SZ_LIGATURE                               0xDF
#define ASCII_A_GRAVE_LOWER                             0xE0
#define ASCII_A_ACUTE_LOWER                             0xE1
#define ASCII_A_CIRCUMFLEX_LOWER                        0xE2
#define ASCII_A_TILDE_LOWER                             0xE3
#define ASCII_A_UMLAUT_LOWER                            0xE4
#define ASCII_A_RING_LOWER                              0xE5
#define ASCII_AE_LIGATURE_LOWER                         0xE6
#define ASCII_C_CEDILLA_LOWER                           0xE7
#define ASCII_E_GRAVE_LOWER                             0xE8
#define ASCII_E_ACUTE_LOWER                             0xE9
#define ASCII_E_CIRCUMFLEX_LOWER                        0xEA
#define ASCII_E_UMLAUT_LOWER                            0xEB
#define ASCII_I_GRAVE_LOWER                             0xEC
#define ASCII_I_ACUTE_LOWER                             0xED
#define ASCII_I_CIRCUMFLEX_LOWER                        0xEE
#define ASCII_I_UMLAUT_LOWER                            0xEF
#define ASCII_ETH_LOWER                                 0xF0
#define ASCII_N_TILDER_LOWER                            0xF1
#define ASCII_O_GRAVE_LOWER                             0xF2
#define ASCII_O_ACUTE_LOWER                             0xF3
#define ASCII_O_CIRCUMFLEX_LOWER                        0xF4
#define ASCII_O_TILDE_LOWER                             0xF5
#define ASCII_O_UMLAUT_LOWER                            0xF6
#define ASCII_DIVISION_SYMBOL                           0xF7
#define ASCII_O_SLASH_LOWER                             0xF8
#define ASCII_U_GRAVE_LOWER                             0xF9
#define ASCII_U_ACUTE_LOWER                             0xFA
#define ASCII_U_CIRCUMFLEX_LOWER                        0xFB
#define ASCII_U_UMLAUT_LOWER                            0xFC
#define ASCII_Y_ACUTE_LOWER                             0xFD
#define ASCII_THORN_LOWER                               0xFE
#define ASCII_Y_UMLAUT_LOWER                            0xFF




#endif