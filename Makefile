#  ____           _ _   _      _   
# |  _ \ ___   __| | \ | | ___| |_ 
# | |_) / _ \ / _` |  \| |/ _ \ __|
# |  __/ (_) | (_| | |\  |  __/ |_ 
# |_|   \___/ \__,_|_| \_|\___|\__|
# 
# 
# General Purpose C Library.
# Copyright (C) 2017-2019 Zach Podbielniak
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


CC = gcc
NCC = nvcc
ASM = nasm
STD = -std=c89
WARNINGS = -Wall -Wextra -Wshadow -Wunsafe-loop-optimizations -Wpointer-arith
WARNINGS += -Wfloat-equal -Wswitch-enum -Wstrict-aliasing -Wno-missing-braces
WARNINGS += -Wno-cast-function-type -Wno-stringop-truncation #-Wno-switch-enum
WARNINGS += -Wno-int-conversion 
#WARNINGS += -Werror
DEFINES = -D _DEFAULT_SOURCE
DEFINES += -D _GNU_SOURCE
ifeq ($(BUILD_TYPE),onesdk)
DEFINES += -D BUILD_WITH_ONEAGENT_SDK
endif

DEFINES_D = $(DEFINES)
DEFINES_D += -D __DEBUG__
ifeq ($(BUILD_TYPE),onesdk)
DEFINES_D += -D BUILD_WITH_ONEAGENT_SDK
endif


# While we have not experienced any issues with -Ofast, it is recommended to
# verify the results for yourself. If you find anything, simply drop down to
# -O3. If you want to play it 100% safe, just use -O2. 
#
# For those of you who may not know, -Ofast optimizes the code "so hard" that
# it breaks standards compliance. If you want to read more about this, check
# the GCC manual.

OPTIMIZE = -O2 -funroll-loops -fstrict-aliasing
OPTIMIZE += -fstack-protector-strong
#MARCH = -march=native
#MTUNE = -mtune=native

CC_FLAGS = $(STD)
CC_FLAGS += $(WARNINGS)
CC_FLAGS += $(DEFINES)
CC_FLAGS += $(OPTIMIZE)
#CC_FLAGS += $(MARCH)
#CC_FLAGS += $(MTUNE)

CC_FLAGS_D = $(STD)
CC_FLAGS_D += $(WARNINGS)
CC_FLAGS_D += $(DEFINES_D)

CC_FLAGS_T = $(CC_FLAGS_D)
CC_FLAGS_T += -Wno-unused-variable

ASM_FLAGS = -Wall
#ASM_FLAGS += -Ox










# Handle
FILES_HANDLE = CHandle.o CWait.o CRelease.o CHandleTable.o

# Algorithms
FILES_ALGORITHMS = CRot13.o CSha256.o CCrc64.o CBase64.o

# Concurrent
FILES_CONCURRENT = CCallOnce.o CFuture.o CPromise.o

# Containers
FILES_CONTAINERS = CVector.o CLinkedList.o CQueue.o
FILES_CONTAINERS += CStack.o CPriorityQueue.o CHashTable.o
FILES_CONTAINERS += CBinarySearchTree.o

# Devices
ifeq ($(BUILD_TYPE),docker)
FILES_DEVICES = CSerial.o
else ifeq ($(BUILD_TYPE),bsd)

else
FILES_DEVICES = CGpio.o CI2C.o CSerial.o CSPI.o
endif

# Lexer
FILES_LEX = CLexer.o CTokenTable.o CParser.o CIniFileParser.o

# Locks
FILES_LOCKS = CCritSec.o CEvent.o CSpinLock.o 
FILES_LOCKS += CMutex.o CSemaphore.o

# Math
FILES_MATH = CCoordinates.o CShapes2D.o CFastApprox.o 
FILES_MATH += CRandom.o CArbitraryLong.o CQuaternion.o
FILES_MATH += CVector3D.o CMath.o

# Memory 
FILES_MEMORY += CHeap.o CGarbageCollector.o CDynamicHeapRegion.o

# Module
FILES_MODULE = CModule.o CWindowSystem.o CCompileModule.o

# Networking
FILES_NETWORKING = CSocket4.o CSocket6.o CSocketUnix.o CSocket.o
FILES_NETWORKING += CIpv4.o CIpv6.o CHttp.o
FILES_NETWORKING += CHttpRequest.o CHttpsRequest.o
ifeq ($(BUILD_TYPE),bsd)
FILES_NETWORKING += CSmtp.o
else
FILES_NETWORKING += CGeo4.o CSmtp.o
endif

# Poller
FILES_POLLER = CPoller.o

# System
FILES_SYSTEM = CSystem.o CHardware.o

# Threading
FILES_THREAD = CThread.o




#Release .o Files
FILES = CFile.o CDirectory.o CLog.o CError.o CClock.o CTimer.o CTime.o CAtomTable.o 
FILES += CSerialization.o CDeserialization.o CShell.o
ifeq ($(BUILD_TYPE),bsd)
FILES += CConvertUnit.o CString.o CHeapString.o CProcess.o CFork.o 
else
FILES += CLua.o CConvertUnit.o CString.o CHeapString.o CProcess.o CFork.o CExecute.o
endif

FILES += $(FILES_HANDLE)
FILES += $(FILES_ALGORITHMS)
FILES += $(FILES_CONCURRENT)
FILES += $(FILES_CONTAINERS)
FILES += $(FILES_DEVICES)
FILES += $(FILES_LEX)
FILES += $(FILES_LOCKS)
FILES += $(FILES_MATH)
FILES += $(FILES_MEMORY)
FILES += $(FILES_MODULE)
FILES += $(FILES_NETWORKING)
FILES += $(FILES_POLLER)
FILES += $(FILES_SYSTEM)
FILES += $(FILES_THREAD)


#If the CPU is of type x86_64
ifeq ($(shell uname -m),x86_64)
FILES += AsmMemSetX86.o
FILES += AsmFastZeroX64.o
FILES += AsmFastCopyX64.o
FILES += AsmFastCompareX64.o
FILES += AsmFastFindX64.o
FILES += AsmWordDumpX64.o
FILES += AsmDwordDumpX64.o
FILES += AsmQwordDumpX64.o
endif

#############################################################################
#############################################################################
#############################################################################
#############################################################################



# Handle Debug
FILES_HANDLE_D = CHandle_d.o CWait_d.o CRelease_d.o CHandleTable_d.o

# Algorithms
FILES_ALGORITHMS_D = CRot13_d.o CSha256_d.o CCrc64_d.o CBase64_d.o

# Concurrent
FILES_CONCURRENT_D = CCallOnce_d.o CFuture_d.o CPromise_d.o

# Containers Debug
FILES_CONTAINERS_D = CVector_d.o CLinkedList_d.o CQueue_d.o
FILES_CONTAINERS_D += CStack_d.o CPriorityQueue_d.o CHashTable_d.o
FILES_CONTAINERS_D += CBinarySearchTree_d.o

# Devices Debug
ifeq ($(BUILD_TYPE),docker)
FILES_DEVICES_D = CSerial_d.o
else ifeq ($(BUILD_TYPE),bsd)

else
FILES_DEVICES_D = CGpio_d.o CI2C_d.o CSerial_d.o CSPI_d.o
endif

# Lexer Debug
FILES_LEX_D = CLexer_d.o CTokenTable_d.o CParser_d.o CIniFileParser_d.o

# Locks Debug
FILES_LOCKS_D = CCritSec_d.o CEvent_d.o CSpinLock_d.o 
FILES_LOCKS_D += CMutex_d.o CSemaphore_d.o

# Math Debug
FILES_MATH_D = CCoordinates_d.o CShapes2D_d.o CFastApprox_d.o 
FILES_MATH_D += CRandom_d.o CArbitraryLong_d.o CQuaternion_d.o
FILES_MATH_D += CVector3D_d.o CMath_d.o

# Memory Debug
FILES_MEMORY_D += CHeap_d.o CGarbageCollector_d.o CDynamicHeapRegion_d.o

# Module Debug
FILES_MODULE_D = CModule_d.o CWindowSystem_d.o CCompileModule_d.o

# Networking Debug
FILES_NETWORKING_D = CSocket4_d.o CSocket6_d.o CSocketUnix_d.o CSocket_d.o
FILES_NETWORKING_D += CIpv4_d.o CIpv6_d.o CHttp_d.o
FILES_NETWORKING_D += CHttpRequest_d.o CHttpsRequest_d.o
ifeq ($(BUILD_TYPE),bsd)
FILES_NETWORKING_D += CSmtp_d.o
else
FILES_NETWORKING_D += CGeo4_d.o CSmtp_d.o
endif

# Poller Debug
FILES_POLLER_D = CPoller_d.o

# System Debug
FILES_SYSTEM_D = CSystem_d.o CHardware_d.o

# Threading Debug
FILES_THREAD_D = CThread_d.o







#Debug .o Files
FILES_D = CFile_d.o CDirectory_d.o CLog_d.o
FILES_D += CError_d.o CTime_d.o
FILES_D += CClock_d.o CTimer_d.o CAtomTable_d.o 
ifneq ($(BUILD_TYPE),bsd)
FILES_D += CLua_d.o CExecute_d.o
endif
FILES_D += CShell_d.o
FILES_D += CConvertUnit_d.o 
FILES_D += CModule_d.o
FILES_D += CProcess_d.o CFork_d.o
FILES_D += CSerialization_d.o CDeserialization_d.o
FILES_D += CString_d.o CHeapString_d.o
FILES_D += CWindowSystem_d.o
FILES_D += $(FILES_HANDLE_D)
FILES_D += $(FILES_ALGORITHMS_D)
FILES_D += $(FILES_CONCURRENT_D)
FILES_D += $(FILES_CONTAINERS_D)
FILES_D += $(FILES_DEVICES_D)
FILES_D += $(FILES_LEX_D)
FILES_D += $(FILES_LOCKS_D)
FILES_D += $(FILES_MATH_D)
FILES_D += $(FILES_MEMORY_D)
FILES_D += $(FILES_MODULE_D)
FILES_D += $(FILES_NETWORKING_D)
FILES_D += $(FILES_POLLER_D)
FILES_D += $(FILES_SYSTEM_D)
FILES_D += $(FILES_THREAD_D)


#If the CPU is of type x86_64
ifeq ($(shell uname -m),x86_64)
FILES_D += AsmMemSetX86_d.o
FILES_D += AsmFastZeroX64_d.o
FILES_D += AsmFastCopyX64_d.o
FILES_D += AsmFastCompareX64_d.o
FILES_D += AsmFastFindX64_d.o
FILES_D += AsmWordDumpX64_d.o
FILES_D += AsmDwordDumpX64_d.o
FILES_D += AsmQwordDumpX64_d.o
endif



#Test Progams
TEST = CCrc64_Test CBase64_Test CDynamicHeapRegion_Test CHashTable_Test
#TEST += CGeo4_Test
TEST += CHeapString_Test CLex_Test
TEST += CParser_Test CProcess_Test
TEST += CString_Test CTimer_Test CTokenTable_Test CUnitTest_Test
TEST += CHandleTable_Test CShell_Test
TEST += CIniFileParser_Test CBinarySearchTree_Test
TEST += CDeserialization_Test CSerialization_Test
TEST += CDirectory_Test





all:	libpodnet.so

debug:	libpodnet_d.so 

test:	$(TEST)

doc:
	mkdir -p docs/html/
	robodoc --src ./ --doc ./docs/html/ --multidoc --html --syntaxcolors --nosort

man:
	mkdir -p docs/man/
	robodoc --src ./ --doc ./docs/man/ --multidoc --troff --compress

check: 
	mkdir -p bin/
	#infer --no-liveness --no-ownership --fail-on-issue -- make -j
	cppcheck . --std=c89 -j8 --inline-suppr --error-exitcode=1

libpodnet.so:	$(FILES)
ifeq ($(BUILD_TYPE),bsd)
	$(CC) -shared -fPIC -s -o bin/libpodnet.so bin/*.o -pthread -lm -lgmp -lssl -lcrypto -lcurl $(STD) $(OPTIMIZE) $(MARCH)
	rm bin/*.o
else ifeq ($(BUILD_TYPE),docker)
	$(CC) -shared -fPIC -s -o bin/libpodnet.so bin/*.o -pthread -lm -llua -lgmp -lssl -lgps -lcrypto -lGeoIP -lcurl $(STD) $(OPTIMIZE) $(MARCH)
	rm bin/*.o
else ifeq ($(BUILD_TYPE),onesdk)
	$(CC) -shared -fPIC -s -o bin/libpodnet.so bin/*.o -pthread -lm -llua -lwiringPi -lgmp -lssl -lgps -lcrypto -lGeoIP -lcurl -lonesdk_shared $(STD) $(OPTIMIZE) $(MARCH)
	rm bin/*.o
else 
	$(CC) -shared -fPIC -s -o bin/libpodnet.so bin/*.o -pthread -lm -llua -lwiringPi -lgmp -lssl -lgps -lcrypto -lGeoIP -lcurl $(STD) $(OPTIMIZE) $(MARCH)
	rm bin/*.o
endif

libpodnet_d.so:	$(FILES_D)
ifeq ($(BUILD_TYPE),bsd)
	$(CC) -g -shared -fPIC -o bin/libpodnet_d.so bin/*_d.o -pthread -lm -lgmp -lssl -lcrypto -lcurl $(STD)
	rm bin/*.o
else ifeq ($(BUILD_TYPE), docker)
	$(CC) -g -shared -fPIC -o bin/libpodnet_d.so bin/*_d.o -pthread -lm -llua -lgmp -lssl -lgps -lcrypto -lGeoIP -lcurl $(STD)
	rm bin/*.o
else ifeq ($(BUILD_TYPE),onesdk)
	$(CC) -g -shared -fPIC -o bin/libpodnet_d.so bin/*_d.o -pthread -lm -llua -lwiringPi -lgmp -lssl -lgps -lcrypto -lGeoIP -lcurl -lonesdk_shared $(STD)
	rm bin/*.o
else 
	$(CC) -g -shared -fPIC -o bin/libpodnet_d.so bin/*_d.o -pthread -lm -llua -lwiringPi -lgmp -lssl -lgps -lcrypto -lGeoIP -lcurl $(STD)
	rm bin/*.o
endif

asm_output:	CHandle.o CCritSec.o CContainers.o CEvent.o CLog.o CPromise.o CFuture.o CCoordinates.o CShapes2D.o CThread.o
	$(CC) -shared -fPIC -s -o bin/libpodnet.s bin/*.o -lpthread -S -masm=intel
	rm bin/*.o


# RELEASE BUILD OBJECT FILES

CHandle.o:
	$(CC) -fPIC -c -o bin/CHandle.o CHandle/CHandle.c $(CC_FLAGS)

CWait.o:
	$(CC) -fPIC -c -o bin/CWait.o CHandle/CWait.c $(CC_FLAGS)

CRelease.o:
	$(CC) -fPIC -c -o bin/CRelease.o CHandle/CRelease.c $(CC_FLAGS)

CHandleTable.o:
	$(CC) -fPIC -c -o bin/CHandleTable.o CHandle/CHandleTable.c $(CC_FLAGS)

CHeap.o:
	$(CC) -fPIC -c -o bin/CHeap.o CMemory/CHeap.c $(CC_FLAGS)

CDynamicHeapRegion.o:
	$(CC) -fPIC -c -o bin/CDynamicHeapRegion.o CMemory/CDynamicHeapRegion.c $(CC_FLAGS)

CCritSec.o:
	$(CC) -fPIC -c -o bin/CCritSec.o CLock/CCritSec.c $(CC_FLAGS)

CVector.o:
	$(CC) -fPIC -c -o bin/CVector.o CContainers/CVector.c $(CC_FLAGS)

CLinkedList.o:
	$(CC) -fPIC -c -o bin/CLinkedList.o CContainers/CLinkedList.c $(CC_FLAGS)

CStack.o:
	$(CC) -fPIC -c -o bin/CStack.o CContainers/CStack.c $(CC_FLAGS)

CQueue.o:
	$(CC) -fPIC -c -o bin/CQueue.o CContainers/CQueue.c $(CC_FLAGS)

CPriorityQueue.o:
	$(CC) -fPIC -c -o bin/CPriorityQueue.o CContainers/CPriorityQueue.c $(CC_FLAGS)

CHashTable.o:
	$(CC) -fPIC -c -o bin/CHashTable.o CContainers/CHashTable.c $(CC_FLAGS)

CEvent.o:
	$(CC) -fPIC -c -o bin/CEvent.o CLock/CEvent.c $(CC_FLAGS)

CMutex.o:
	$(CC) -fPIC -c -o bin/CMutex.o CLock/CMutex.c $(CC_FLAGS)

CSemaphore.o:
	$(CC) -fPIC -c -o bin/CSemaphore.o CLock/CSemaphore.c $(CC_FLAGS)

CSpinLock.o:
	$(CC) -fPIC -c -o bin/CSpinLock.o CLock/CSpinLock.c $(CC_FLAGS)

CFile.o:
	$(CC) -fPIC -c -o bin/CFile.o CFile/CFile.c $(CC_FLAGS)

CDirectory.o:
	$(CC) -fPIC -c -o bin/CDirectory.o CFile/CDirectory.c $(CC_FLAGS)

CLog.o:
	$(CC) -fPIC -c -o bin/CLog.o CLog/CLog.c $(CC_FLAGS)

CPromise.o:
	$(CC) -fPIC -c -o bin/CPromise.o CConcurrent/CPromise.c $(CC_FLAGS)

CFuture.o:
	$(CC) -fPIC -c -o bin/CFuture.o CConcurrent/CFuture.c $(CC_FLAGS)

CCallOnce.o:
	$(CC) -fPIC -c -o bin/CCallOnce.o CConcurrent/CCallOnce.c $(CC_FLAGS)

CMath.o:
	$(CC) -fPIC -c -o bin/CMath.o CMath/CMath.c $(CC_FLAGS)

CCoordinates.o:
	$(CC) -fPIC -c -o bin/CCoordinates.o CMath/CCoordinates.c $(CC_FLAGS)

CShapes2D.o:
	$(CC) -fPIC -c -o bin/CShapes2D.o CMath/CShapes2D.c $(CC_FLAGS)

CThread.o:
	$(CC) -fPIC -c -o bin/CThread.o CThread/CThread.c $(CC_FLAGS)

CProcess.o:
	$(CC) -fPIC -c -o bin/CProcess.o CProcess/CProcess.c $(CC_FLAGS_D)

CSocket4.o:
	$(CC) -fPIC -c -o bin/CSocket4.o CNetworking/CSocket4.c $(CC_FLAGS)

CSocket6.o:
	$(CC) -fPIC -c -o bin/CSocket6.o CNetworking/CSocket6.c $(CC_FLAGS)

CSocketUnix.o:
	$(CC) -fPIC -c -o bin/CSocketUnix.o CNetworking/CSocketUnix.c $(CC_FLAGS)

CSocket.o:
	$(CC) -fPIC -c -o bin/CSocket.o CNetworking/CSocket.c $(CC_FLAGS)

CError.o:
	$(CC) -fPIC -c -o bin/CError.o CError/CError.c $(CC_FLAGS)

CClock.o:
	$(CC) -fPIC -c -o bin/CClock.o CClock/CClock.c $(CC_FLAGS)

CTime.o:
	$(CC) -fPIC -c -o bin/CTime.o CClock/CTime.c $(CC_FLAGS)

CTimer.o:
	$(CC) -fPIC -c -o bin/CTimer.o CClock/CTimer.c $(CC_FLAGS)

CAtomTable.o:
	$(CC) -fPIC -c -o bin/CAtomTable.o CAtom/CAtomTable.c $(CC_FLAGS)

CSystem.o:
	$(CC) -fPIC -c -o bin/CSystem.o CSystem/CSystem.c $(CC_FLAGS)

CHardware.o:
	$(CC) -fPIC -c -o bin/CHardware.o CSystem/CHardware.c $(CC_FLAGS)

CSha256.o:
	$(CC) -fPIC -c -o bin/CSha256.o CAlgorithms/CSha256.c $(CC_FLAGS)

CRot13.o:
	$(CC) -fPIC -c -o bin/CRot13.o CAlgorithms/CRot13.c $(CC_FLAGS)

CCrc64.o:
	$(CC) -fPIC -c -o bin/CCrc64.o CAlgorithms/CCrc64.c $(CC_FLAGS)

CBase64.o:
	$(CC) -fPIC -c -o bin/CBase64.o CAlgorithms/CBase64.c $(CC_FLAGS)

CLua.o:
	$(CC) -fPIC -c -o bin/CLua.o CScripting/CLua.c $(CC_FLAGS)

CShell.o:
	$(CC) -fPIC -c -o bin/CShell.o CScripting/CShell.c $(CC_FLAGS)

CGpio.o:
	$(CC) -fPIC -c -o bin/CGpio.o CDevices/CGpio.c $(CC_FLAGS)

CSerial.o:
	$(CC) -fPIC -c -o bin/CSerial.o CDevices/CSerial.c $(CC_FLAGS)

CI2C.o:
	$(CC) -fPIC -c -o bin/CI2C.o CDevices/CI2C.c $(CC_FLAGS)

CSPI.o:
	$(CC) -fPIC -c -o bin/CSPI.o CDevices/CSPI.c $(CC_FLAGS)

CConvertUnit.o:
	$(CC) -fPIC -c -o bin/CConvertUnit.o CUnit/CConvertUnit.c $(CC_FLAGS)

CFastApprox.o:
	$(CC) -fPIC -c -o bin/CFastApprox.o CMath/CFastApprox.c $(CC_FLAGS)

CRandom.o:
	$(CC) -fPIC -c -o bin/CRandom.o CMath/CRandom.c $(CC_FLAGS)

CArbitraryLong.o:
	$(CC) -fPIC -c -o bin/CArbitraryLong.o CMath/CArbitraryLong.c $(CC_FLAGS)

CModule.o:
	$(CC) -fPIC -c -o bin/CModule.o CModule/CModule.c $(CC_FLAGS)

CCompileModule.o:
	$(CC) -fPIC -c -o bin/CCompileModule.o CModule/CCompileModule.c $(CC_FLAGS)

CWindowSystem.o:
	$(CC) -fPIC -c -o bin/CWindowSystem.o CGui/CWindowSystem.c $(CC_FLAGS)

CIpv4.o:
	$(CC) -fPIC -c -o bin/CIpv4.o CNetworking/CIpv4.c $(CC_FLAGS)

CIpv6.o:
	$(CC) -fPIC -c -o bin/CIpv6.o CNetworking/CIpv6.c $(CC_FLAGS)

CHttp.o:
	$(CC) -fPIC -c -o bin/CHttp.o CNetworking/CHttp.c $(CC_FLAGS)

CHttpRequest.o:
	$(CC) -fPIC -c -o bin/CHttpRequest.o CNetworking/CHttpRequest.c $(CC_FLAGS)

CHttpsRequest.o:
	$(CC) -fPIC -c -o bin/CHttpsRequest.o CNetworking/CHttpsRequest.c $(CC_FLAGS)

CSmtp.o:
	$(CC) -fPIC -c -o bin/CSmtp.o CNetworking/CSmtp.c $(CC_FLAGS)

CGarbageCollector.o:
	$(CC) -fPIC -c -o bin/CGarbageCollector.o CMemory/CGarbageCollector.c $(CC_FLAGS)

CQuaternion.o:
	$(CC) -fPIC -c -o bin/CQuaternion.o CMath/CQuaternion.c $(CC_FLAGS)

CVector3D.o:
	$(CC) -fPIC -c -o bin/CVector3D.o CMath/CVector3D.c $(CC_FLAGS)

CString.o:
	$(CC) -fPIC -c -o bin/CString.o CString/CString.c $(CC_FLAGS)

CGeo4.o:
	$(CC) -fPIC -c -o bin/CGeo4.o CNetworking/CGeo4.c $(CC_FLAGS)

CPoller.o:
	$(CC) -fPIC -c -o bin/CPoller.o CPoller/CPoller.c $(CC_FLAGS)

CHeapString.o:
	$(CC) -fPIC -c -o bin/CHeapString.o CString/CHeapString.c $(CC_FLAGS)

CLexer.o:
	$(CC) -fPIC -c -o bin/CLexer.o CLex/CLexer.c $(CC_FLAGS)

CTokenTable.o:
	$(CC) -fPIC -c -o bin/CTokenTable.o CLex/CTokenTable.c $(CC_FLAGS)

CParser.o:
	$(CC) -fPIC -c -o bin/CParser.o CLex/CParser.c $(CC_FLAGS)

CBinarySearchTree.o:
	$(CC) -fPIC -c -o bin/CBinarySearchTree.o CContainers/CBinarySearchTree.c $(CC_FLAGS)

CIniFileParser.o:
	$(CC) -fPIC -c -o bin/CIniFileParser.o CLex/CIniFileParser.c $(CC_FLAGS)

CSerialization.o:
	$(CC) -fPIC -c -o bin/CSerialization.o CSerialization/CSerialization.c $(CC_FLAGS)

CDeserialization.o:
	$(CC) -fPIC -c -o bin/CDeserialization.o CSerialization/CDeserialization.c $(CC_FLAGS)

CFork.o:
	$(CC) -fPIC -c -o bin/CFork.o CProcess/CFork.c $(CC_FLAGS)

CExecute.o:
	$(CC) -fPIC -c -o bin/CExecute.o CProcess/CExecute.c $(CC_FLAGS)











#Asm
AsmMemSetX86.o:
	$(ASM) -f elf64 $(ASM_FLAGS) -o bin/AsmMemSetX86.o CMemory/AsmMemSetX86.asm

AsmFastZeroX64.o:
	$(ASM) -f elf64 $(ASM_FLAGS) -o bin/AsmFastZeroX64.o CMemory/AsmFastZeroX64.asm

AsmFastCopyX64.o:
	$(ASM) -f elf64 $(ASM_FLAGS) -o bin/AsmFastCopyX64.o CMemory/AsmFastCopyX64.asm

AsmFastCompareX64.o:
	$(ASM) -f elf64 $(ASM_FLAGS) -o bin/AsmFastCompareX64.o CMemory/AsmFastCompareX64.asm

AsmFastFindX64.o:
	$(ASM) -f elf64 $(ASM_FLAGS) -o bin/AsmFastFindX64.o CMemory/AsmFastFindX64.asm

AsmWordDumpX64.o:
	$(ASM) -f elf64 $(ASM_FLAGS) -o bin/AsmWordDumpX64.o CSystem/AsmWordDumpX64.asm

AsmDwordDumpX64.o:
	$(ASM) -f elf64 $(ASM_FLAGS) -o bin/AsmDwordDumpX64.o CSystem/AsmDwordDumpX64.asm

AsmQwordDumpX64.o:
	$(ASM) -f elf64 $(ASM_FLAGS) -o bin/AsmQwordDumpX64.o CSystem/AsmQwordDumpX64.asm






# DEBUG BUILD OBJECT FILES



CHandle_d.o:
	$(CC) -g -fPIC -c -o bin/CHandle_d.o CHandle/CHandle.c $(CC_FLAGS_D)

CWait_d.o:
	$(CC) -g -fPIC -c -o bin/CWait_d.o CHandle/CWait.c $(CC_FLAGS_D)

CRelease_d.o:
	$(CC) -g -fPIC -c -o bin/CRelease_d.o CHandle/CRelease.c $(CC_FLAGS_D)

CHandleTable_d.o:
	$(CC) -g -fPIC -c -o bin/CHandleTable_d.o CHandle/CHandleTable.c $(CC_FLAGS_D)

CHeap_d.o:
	$(CC) -g -fPIC -c -o bin/CHeap_d.o CMemory/CHeap.c $(CC_FLAGS_D)

CDynamicHeapRegion_d.o:
	$(CC) -g -fPIC -c -o bin/CDynamicHeapRegion_d.o CMemory/CDynamicHeapRegion.c $(CC_FLAGS_D)

CCritSec_d.o:
	$(CC) -g -fPIC -c -o bin/CCritSec_d.o CLock/CCritSec.c $(CC_FLAGS_D)

CVector_d.o:
	$(CC) -g -fPIC -c -o bin/CVector_d.o CContainers/CVector.c $(CC_FLAGS_D)

CLinkedList_d.o:
	$(CC) -g -fPIC -c -o bin/CLinkedList_d.o CContainers/CLinkedList.c $(CC_FLAGS_D)

CStack_d.o:
	$(CC) -g -fPIC -c -o bin/CStack_d.o CContainers/CStack.c $(CC_FLAGS_D)

CQueue_d.o:
	$(CC) -g -fPIC -c -o bin/CQueue_d.o CContainers/CQueue.c $(CC_FLAGS_D)

CPriorityQueue_d.o:
	$(CC) -g -fPIC -c -o bin/CPriorityQueue_d.o CContainers/CPriorityQueue.c $(CC_FLAGS_D)

CHashTable_d.o:
	$(CC) -g -fPIC -c -o bin/CHashTable_d.o CContainers/CHashTable.c $(CC_FLAGS_D)

CEvent_d.o:
	$(CC) -g -fPIC -c -o bin/CEvent_d.o CLock/CEvent.c $(CC_FLAGS_D)

CMutex_d.o:
	$(CC) -g -fPIC -c -o bin/CMutex_d.o CLock/CMutex.c $(CC_FLAGS_D)

CSemaphore_d.o:
	$(CC) -g -fPIC -c -o bin/CSemaphore_d.o CLock/CSemaphore.c $(CC_FLAGS_D)

CSpinLock_d.o:
	$(CC) -g -fPIC -c -o bin/CSpinLock_d.o CLock/CSpinLock.c $(CC_FLAGS_D)

CFile_d.o:
	$(CC) -g -fPIC -c -o bin/CFile_d.o CFile/CFile.c $(CC_FLAGS_D)

CDirectory_d.o:
	$(CC) -g -fPIC -c -o bin/CDirectory_d.o CFile/CDirectory.c $(CC_FLAGS_D)

CLog_d.o:
	$(CC) -g -fPIC -c -o bin/CLog_d.o CLog/CLog.c $(CC_FLAGS_D)

CFuture_d.o:
	$(CC) -g -fPIC -c -o bin/CFuture_d.o CConcurrent/CFuture.c $(CC_FLAGS_D)

CPromise_d.o:
	$(CC) -g -fPIC -c -o bin/CPromise_d.o CConcurrent/CPromise.c $(CC_FLAGS_D)

CCallOnce_d.o:
	$(CC) -g -fPIC -c -o bin/CCallOnce_d.o CConcurrent/CCallOnce.c $(CC_FLAGS_D)

CMath_d.o:
	$(CC) -g -fPIC -c -o bin/CMath_d.o CMath/CMath.c $(CC_FLAGS_D)

CCoordinates_d.o:
	$(CC) -g -fPIC -c -o bin/CCoordinates_d.o CMath/CCoordinates.c $(CC_FLAGS_D)

CShapes2D_d.o:
	$(CC) -g -fPIC -c -o bin/CShapes2D_d.o CMath/CShapes2D.c $(CC_FLAGS_D)

CThread_d.o:
	$(CC) -g -fPIC -c -o bin/CThread_d.o CThread/CThread.c $(CC_FLAGS_D)

CProcess_d.o:
	$(CC) -g -fPIC -c -o bin/CProcess_d.o CProcess/CProcess.c $(CC_FLAGS_D)

CSocket4_d.o:
	$(CC) -g -fPIC -c -o bin/CSocket4_d.o CNetworking/CSocket4.c $(CC_FLAGS_D)

CSocket6_d.o:
	$(CC) -g -fPIC -c -o bin/CSocket6_d.o CNetworking/CSocket6.c $(CC_FLAGS_D)

CSocketUnix_d.o:
	$(CC) -g -fPIC -c -o bin/CSocketUnix_d.o CNetworking/CSocketUnix.c $(CC_FLAGS_D)

CSocket_d.o:
	$(CC) -g -fPIC -c -o bin/CSocket_d.o CNetworking/CSocket.c $(CC_FLAGS_D)

CError_d.o:
	$(CC) -g -fPIC -c -o bin/CError_d.o CError/CError.c $(CC_FLAGS_D)

CClock_d.o:
	$(CC) -g -fPIC -c -o bin/CClock_d.o CClock/CClock.c $(CC_FLAGS_D)

CTime_d.o:
	$(CC) -g -fPIC -c -o bin/CTime_d.o CClock/CTime.c $(CC_FLAGS_D)

CTimer_d.o:
	$(CC) -g -fPIC -c -o bin/CTimer_d.o CClock/CTimer.c $(CC_FLAGS_D)

CAtomTable_d.o:
	$(CC) -g -fPIC -c -o bin/CAtomTable_d.o CAtom/CAtomTable.c $(CC_FLAGS_D)

CSystem_d.o:
	$(CC) -g -fPIC -c -o bin/CSystem_d.o CSystem/CSystem.c $(CC_FLAGS_D)

CHardware_d.o:
	$(CC) -g -fPIC -c -o bin/CHardware_d.o CSystem/CHardware.c $(CC_FLAGS_D)

CSha256_d.o:
	$(CC) -g -fPIC -c -o bin/CSha256_d.o CAlgorithms/CSha256.c $(CC_FLAGS_D)

CRot13_d.o:
	$(CC) -g -fPIC -c -o bin/CRot13_d.o CAlgorithms/CRot13.c $(CC_FLAGS_D)

CCrc64_d.o:
	$(CC) -g -fPIC -c -o bin/CCrc64_d.o CAlgorithms/CCrc64.c $(CC_FLAGS_D)

CBase64_d.o:
	$(CC) -g -fPIC -c -o bin/CBase64_d.o CAlgorithms/CBase64.c $(CC_FLAGS_D)

CLua_d.o:
	$(CC) -g -fPIC -c -o bin/CLua_d.o CScripting/CLua.c $(CC_FLAGS_D)

CShell_d.o:
	$(CC) -g -fPIC -c -o bin/CShell_d.o CScripting/CShell.c $(CC_FLAGS_D)

CGpio_d.o:
	$(CC) -g -fPIC -c -o bin/CGpio_d.o CDevices/CGpio.c $(CC_FLAGS_D)

CSerial_d.o:
	$(CC) -g -fPIC -c -o bin/CSerial_d.o CDevices/CSerial.c $(CC_FLAGS_D)

CI2C_d.o:
	$(CC) -g -fPIC -c -o bin/CI2C_d.o CDevices/CI2C.c $(CC_FLAGS_D)

CSPI_d.o:
	$(CC) -g -fPIC -c -o bin/CSPI_d.o CDevices/CSPI.c $(CC_FLAGS)

CConvertUnit_d.o:
	$(CC) -g -fPIC -c -o bin/CConvertUnit_d.o CUnit/CConvertUnit.c $(CC_FLAGS_D)

CFastApprox_d.o:
	$(CC) -g -fPIC -c -o bin/CFastApprox_d.o CMath/CFastApprox.c $(CC_FLAGS_D)

CRandom_d.o:
	$(CC) -g -fPIC -c -o bin/CRandom_d.o CMath/CRandom.c $(CC_FLAGS_D)

CArbitraryLong_d.o:
	$(CC) -g -fPIC -c -o bin/CArbitraryLong_d.o CMath/CArbitraryLong.c $(CC_FLAGS_D)

CModule_d.o:
	$(CC) -g -fPIC -c -o bin/CModule_d.o CModule/CModule.c $(CC_FLAGS_D)

CCompileModule_d.o:
	$(CC) -g -fPIC -c -o bin/CCompileModule_d.o CModule/CCompileModule.c $(CC_FLAGS_D)
	
CWindowSystem_d.o:
	$(CC) -g -fPIC -c -o bin/CWindowSystem_d.o CGui/CWindowSystem.c $(CC_FLAGS_D)

CIpv4_d.o:
	$(CC) -g -fPIC -c -o bin/CIpv4_d.o CNetworking/CIpv4.c $(CC_FLAGS_D)

CIpv6_d.o:
	$(CC) -g -fPIC -c -o bin/CIpv6_d.o CNetworking/CIpv6.c $(CC_FLAGS_D)

CHttp_d.o:
	$(CC) -g -fPIC -c -o bin/CHttp_d.o CNetworking/CHttp.c $(CC_FLAGS_D)

CHttpRequest_d.o:
	$(CC) -g -fPIC -c -o bin/CHttpRequest_d.o CNetworking/CHttpRequest.c $(CC_FLAGS_D)

CHttpsRequest_d.o:
	$(CC) -g -fPIC -c -o bin/CHttpsRequest_d.o CNetworking/CHttpsRequest.c $(CC_FLAGS_D)

CSmtp_d.o:
	$(CC) -g -fPIC -c -o bin/CSmtp_d.o CNetworking/CSmtp.c $(CC_FLAGS_D)

CGarbageCollector_d.o:
	$(CC) -g -fPIC -c -o bin/CGarbageCollector_d.o CMemory/CGarbageCollector.c $(CC_FLAGS_D)

CQuaternion_d.o:
	$(CC) -g -fPIC -c -o bin/CQuaternion_d.o CMath/CQuaternion.c $(CC_FLAGS_D)

CVector3D_d.o:
	$(CC) -g -fPIC -c -o bin/CVector3D_d.o CMath/CVector3D.c $(CC_FLAGS_D)

CString_d.o:
	$(CC) -g -fPIC -c -o bin/CString_d.o CString/CString.c $(CC_FLAGS_D)

CGeo4_d.o:
	$(CC) -g -fPIC -c -o bin/CGeo4_d.o CNetworking/CGeo4.c $(CC_FLAGS_D)

CPoller_d.o:
	$(CC) -g -fPIC -c -o bin/CPoller_d.o CPoller/CPoller.c $(CC_FLAGS_D)

CHeapString_d.o:
	$(CC) -g -fPIC -c -o bin/CHeapString_d.o CString/CHeapString.c $(CC_FLAGS_D)

CLexer_d.o:
	$(CC) -g -fPIC -c -o bin/CLexer_d.o CLex/CLexer.c $(CC_FLAGS_D)

CTokenTable_d.o:
	$(CC) -g -fPIC -c -o bin/CTokenTable_d.o CLex/CTokenTable.c $(CC_FLAGS_D)

CParser_d.o:
	$(CC) -g -fPIC -c -o bin/CParser_d.o CLex/CParser.c $(CC_FLAGS_D)

CBinarySearchTree_d.o:
	$(CC) -g -fPIC -c -o bin/CBinarySearchTree_d.o CContainers/CBinarySearchTree.c $(CC_FLAGS_D)

CIniFileParser_d.o:
	$(CC) -g -fPIC -c -o bin/CIniFileParser_d.o CLex/CIniFileParser.c $(CC_FLAGS_D)

CSerialization_d.o:
	$(CC) -g -fPIC -c -o bin/CSerialization_d.o CSerialization/CSerialization.c $(CC_FLAGS_D)

CDeserialization_d.o:
	$(CC) -g -fPIC -c -o bin/CDeserialization_d.o CSerialization/CDeserialization.c $(CC_FLAGS_D)

CFork_d.o:
	$(CC) -g -fPIC -c -o bin/CFork_d.o CProcess/CFork.c $(CC_FLAGS_D)

CExecute_d.o:
	$(CC) -g -fPIC -c -o bin/CExecute_d.o CProcess/CExecute.c $(CC_FLAGS_D)










#Asm
AsmMemSetX86_d.o:
	$(ASM) -g -F dwarf -f elf64 $(ASM_FLAGS) -o bin/AsmMemSetX86_d.o CMemory/AsmMemSetX86.asm

AsmFastZeroX64_d.o:
	$(ASM) -g -F dwarf -f elf64 $(ASM_FLAGS) -o bin/AsmFastZeroX64_d.o CMemory/AsmFastZeroX64.asm

AsmFastCopyX64_d.o:
	$(ASM) -g -F dwarf -f elf64 $(ASM_FLAGS) -o bin/AsmFastCopyX64_d.o CMemory/AsmFastCopyX64.asm

AsmFastCompareX64_d.o:
	$(ASM) -g -F dwarf -f elf64 $(ASM_FLAGS) -o bin/AsmFastCompareX64_d.o CMemory/AsmFastCompareX64.asm

AsmFastFindX64_d.o:
	$(ASM) -g -F dwarf -f elf64 $(ASM_FLAGS) -o bin/AsmFastFindX64_d.o CMemory/AsmFastFindX64.asm

AsmWordDumpX64_d.o:
	$(ASM) -g -F dwarf -f elf64 $(ASM_FLAGS) -o bin/AsmWordDumpX64_d.o CSystem/AsmWordDumpX64.asm

AsmDwordDumpX64_d.o:
	$(ASM) -g -F dwarf -f elf64 $(ASM_FLAGS) -o bin/AsmDwordDumpX64_d.o CSystem/AsmDwordDumpX64.asm

AsmQwordDumpX64_d.o:
	$(ASM) -g -F dwarf -f elf64 $(ASM_FLAGS) -o bin/AsmQwordDumpX64_d.o CSystem/AsmQwordDumpX64.asm











#Test makes

CConcurrent_Test:
	$(CC) -g -fPIC -o bin/Concurrent_Test Tests/Test_CConcurrent.c -lpodnet_d $(CC_FLAGS_D)

CContainers_Test:
	$(CC) -g -fPIC -o bin/CContainers_Test Tests/Test_CVector.c -lpodnet_d $(CC_FLAGS_D)

CHashTable_Test:
	$(CC) -g -fPIC -o bin/CHashTable_Test Tests/Test_CHashTable.c -lpodnet_d $(CC_FLAGS_T)

CLinkedList_Test:
	$(CC) -g -fPIC -o bin/CLinkedList_Test Tests/Test_CLinkedList.c -lpodnet_d $(CC_FLAGS_D)

CError_Test:
	$(CC) -g -fPIC -o bin/CError_Test Tests/Test_CError.c -lpodnet_d $(CC_FLAGS_D)

CFile_Test:
	$(CC) -g -fPIC -o bin/CFile_Test Tests/Test_CFile.c -lpodnet_d $(CC_FLAGS_D)

CDirectory_Test:
	$(CC) -g -fPIC -o bin/CDirectory_Test Tests/Test_CDirectory.c -lpodnet_d $(CC_FLAGS_T)

CLog_Test:
	$(CC) -g -fPIC -o bin/CLog_Test Tests/Test_CLog.c -lpodnet_d $(CC_FLAGS_D)

CSpinLock_Test:
	$(CC) -g -fPIC -o bin/CSpinLock_Test Tests/Test_CSpinLock.c -lpodnet_d $(CC_FLAGS_D)

CEvent_Test:
	$(CC) -g -fPIC -o bin/CEvent_Test Tests/Test_CEvent.c -lpodnet_d $(CC_FLAGS_D)

CCritSec_Test:
	$(CC) -g -fPIC -o bin/CCritSec_Test Tests/Test_CCritSec.c -lpodnet_d $(CC_FLAGS_D)

CClock_Test:
	$(CC) -g -fPIC -o bin/CClock_Test Tests/Test_CClock.c -lpodnet_d $(CC_FLAGS_D)

CTimer_Test:
	$(CC) -g -fPIC -o bin/CTimer_Test Tests/Test_CTimer.c -lpodnet_d $(CC_FLAGS_D) -Wno-unused-variable

CFastApprox_Test:
	$(CC) -g -fPIC -o bin/CMath_Test Tests/Test_CFastApprox.c -lpodnet_d $(CC_FLAGS_D)

CAtom_Test:
	$(CC) -g -fPIC -o bin/CAtom_Test Tests/Test_CAtomTable.c -lpodnet_d $(CC_FLAGS_D)

CSystem_Test:
	$(CC) -g -fPIC -o bin/CSystem_Test Tests/Test_CSystem.c -lpodnet_d $(CC_FLAGS_D)

CSha256_Test:
	$(CC) -g -fPIC -o bin/CSha256_Test Tests/Test_CSha256.c -lpodnet_d $(CC_FLAGS_D)

CRot13_Test:
	$(CC) -g -fPIC -o bin/CRot13_Test Tests/Test_CRot13.c -lpodnet_d $(CC_FLAGS_D)

CCrc64_Test:
	$(CC) -g -fPIC -o bin/CCrc64_Test Tests/Test_CCrc64.c -lpodnet_d $(CC_FLAGS_T)

CBase64_Test:
	$(CC) -g -fPIC -o bin/CBase64_Test Tests/Test_CBase64.c -lpodnet_d $(CC_FLAGS_T)

CShapes2D_Test:
	$(CC) -g -fPIC -o bin/CShapes2D_Test Tests/Test_CShapes2D.c -lpodnet_d  $(CC_FLAGS_D)

CLua_Test:
	$(CC) -g -fPIC -o bin/CLua_Test Tests/Test_CLua.c -lpodnet_d $(CC_FLAGS_D)

CHeap_Test:
	$(CC) -g -fPIC -o bin/CHeap_Test Tests/Test_CHeap.c -lpodnet_d $(CC_FLAGS_D)

CDynamicHeapRegion_Test:
	$(CC) -g -fPIC -o bin/CDynamicHeapRegion_Test Tests/Test_CDynamicHeapRegion.c -lpodnet_d $(CC_FLAGS_T)

CMemory_Test:
	$(CC) -g -fPIC -o bin/CMemory_Test Tests/Test_CMemory.c -lpodnet_d $(CC_FLAGS_D)

CConvertUnit_Test:
	$(CC) -g -fPIC -o bin/CConvertUnit_Test Tests/Test_CUnits.c -lpodnet_d $(CC_FLAGS_D)

CGpio_Test:
	$(CC) -g -fPIC -o bin/CGpio_Test Tests/Test_CGpio.c -lpodnet_d $(CC_FLAGS_D)

CSerial_Test:
	$(CC) -g -fPIC -o bin/CSerial_Test Tests/Test_CSerial.c -lpodnet_d $(CC_FLAGS_D)

CRandom_Test:
	$(CC) -g -fPIC -o bin/CRandom_Test Tests/Test_CRandom.c -lpodnet_d -pthread -march=native -mtune=native $(CC_FLAGS_D)

CSocket4_Test:
	$(CC) -g -fPIC -o bin/CSocket4_Test Tests/Test_CSocket4.c -lpodnet_d $(CC_FLAGS_T)

CSocket6_Test:
	$(CC) -g -fPIC -o bin/CSocket6_Test Tests/Test_CSocket6.c -lpodnet_d $(CC_FLAGS_D)

CSocketUnix_Test:
	$(CC) -g -fPIC -o bin/CSocketUnix_Test Tests/Test_CSocketUnix.c -lpodnet_d $(CC_FLAGS_T)

CIpv4_Test:
	$(CC) -g -fPIC -o bin/CIpv4_Test Tests/Test_CIpv4.c -lpodnet_d $(CC_FLAGS_D)

CIpv6_Test:
	$(CC) -g -fPIC -o bin/CIpv6_Test Tests/Test_CIpv6.c -lpodnet_d $(CC_FLAGS_D)

CHttpRequest_Test:
	$(CC) -g -fPIC -o bin/CHttpRequest_Test Tests/Test_CHttpRequest.c -lpodnet_d $(CC_FLAGS_D)

CHttpsRequest_Test:
	$(CC) -g -fPIC -o bin/CHttpsRequest_Test Tests/Test_CHttpsRequest.c -lpodnet_d $(CC_FLAGS_D)

CUnitTest_Test:
	$(CC) -g -fPIC -o bin/CUnitTest_Test Tests/Test_CUnitTest.c -lpodnet_d $(CC_FLAGS_T)

CString_Test:
	$(CC) -g -fPIC -o bin/CString_Test Tests/Test_CString.c -lpodnet_d $(CC_FLAGS_T)

CGeo4_Test:
	$(CC) -g -fPIC -o bin/CGeo4_Test Tests/Test_CGeo4.c -lpodnet_d $(CC_FLAGS_T)

CProcess_Test:
	$(CC) -g -fPIC -o bin/CProcess_Test Tests/Test_CProcess.c -lpodnet_d $(CC_FLAGS_T)

CPoller_Test:
	$(CC) -g -fPIC -o bin/CPoller_Test Tests/Test_CPoller.c -lpodnet_d $(CC_FLAGS_T)

CHeapString_Test:
	$(CC) -g -fPIC -o bin/CHeapString_Test Tests/Test_CHeapString.c -lpodnet_d $(CC_FLAGS_T)

CLex_Test:
	$(CC) -g -fPIC -o bin/CLex_Test Tests/Test_CLex.c -lpodnet_d $(CC_FLAGS_T)

CTokenTable_Test:
	$(CC) -g -fPIC -o bin/CTokenTable_Test Tests/Test_CTokenTable.c -lpodnet_d $(CC_FLAGS_T)

CParser_Test:
	$(CC) -g -fPIC -o bin/CParser_Test Tests/Test_CParser.c -lpodnet_d $(CC_FLAGS_T)

CSmtp_Test:
	$(CC) -g -fPIC -o bin/CSmtp_Test Tests/Test_CSmtp.c -lpodnet_d $(CC_FLAGS_T)

CCompileModule_Test:
	$(CC) -g -fPIC -o bin/CCompileModule_Test Tests/Test_CCompileModule.c -lpodnet_d $(CC_FLAGS_T)

CBinarySearchTree_Test:
	$(CC) -g -fPIC -o bin/CBinarySearchTree_Test Tests/Test_CBinarySearchTree.c -lpodnet_d $(CC_FLAGS_T)

CIniFileParser_Test:
	$(CC) -g -fPIC -o bin/CIniFileParser_Test Tests/Test_CIniFileParser.c -lpodnet_d $(CC_FLAGS_T)

CHandleTable_Test:
	$(CC) -g -fPIC -o bin/CHandleTable_Test Tests/Test_CHandleTable.c -lpodnet_d $(CC_FLAGS_T)

CSerialization_Test:
	$(CC) -g -fPIC -o bin/CSerialization_Test Tests/Test_CSerialization.c -lpodnet_d $(CC_FLAGS_T)

CDeserialization_Test:
	$(CC) -g -fPIC -o bin/CDeserialization_Test Tests/Test_CDeserialization.c -lpodnet_d $(CC_FLAGS_T)

CExecute_Test:
	$(CC) -g -fPIC -o bin/CExecute_Test Tests/Test_CExecute.c -lpodnet_d $(CC_FLAGS_T)

CShell_Test:
	$(CC) -g -fPIC -o bin/CShell_Test Tests/Test_CShell.c -lpodnet_d $(CC_FLAGS_T)






clean:
	rm bin/*

install:
	mkdir -p ./bin
	cp bin/libpodnet.so /usr/lib

install_debug:
	mkdir -p ./bin
	cp bin/libpodnet_d.so /usr/lib

install_python:
	mkdir -p /usr/lib/python3.7/site-packages/PodNet/
	cp -r Python/* /usr/lib/python3.7/site-packages/PodNet/

install_headers:
ifeq ($(shell uname),FreeBSD)
	mkdir -p /usr/local/include/PodNet
	find . -type d -name "C" -exec mkdir /usr/local/include/PodNet/{}/ \;
	find . -type f -name "*.h" -exec install -d {} /usr/local/include/PodNet/{} \;
else
	mkdir -p /usr/include/PodNet
	find . -type f -name "*.h" -exec install -D {} /usr/include/PodNet/{} \;
endif

