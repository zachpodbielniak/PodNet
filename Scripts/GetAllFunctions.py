#!/usr/bin/python
import subprocess

sCommand = "grep -P '^(PODNET_API|INTERNAL_OPERATION)' */*.c -A2 | grep -P '\\('"

sResult = subprocess.run([sCommand], shell=True, stdout=subprocess.PIPE).stdout.decode('utf-8')
sResult = sResult.split('\n')

ulIndex = 0
while (ulIndex < len(sResult)):
	if (sResult[ulIndex] != ''):
		print(sResult[ulIndex].split('-')[1].replace('(',''))
	ulIndex += 1
