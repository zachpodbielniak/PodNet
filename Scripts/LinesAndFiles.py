#!/usr/bin/python
import subprocess

Result = subprocess.run(["wc -l *.h */*.h */*.c */*.cu */*.asm */*/*/*.asm */*.py Documentation/*.tex Documentation/*/*.tex"], shell=True, stdout=subprocess.PIPE).stdout.decode('utf-8')
TotalLines = Result.split('\n')
TotalLines = TotalLines[len(TotalLines)-2].split(' ')[2]

Result = subprocess.run(["wc -l */*.c"], shell=True, stdout=subprocess.PIPE).stdout.decode('utf-8')
TotalCLines = Result.split('\n')
TotalCLines = TotalCLines[len(TotalCLines)-2].split(' ')[2]

Result = subprocess.run(["wc -l */*.c *.h */*.h"], shell=True, stdout=subprocess.PIPE).stdout.decode('utf-8')
TotalCLinesWH = Result.split('\n')
TotalCLinesWH = TotalCLinesWH[len(TotalCLinesWH)-2].split(' ')[2]

Result = subprocess.run(["wc -l */*.asm */*/*/*.asm"], shell=True, stdout=subprocess.PIPE).stdout.decode('utf-8')
TotalAsmLines = Result.split('\n')
TotalAsmLines = TotalAsmLines[len(TotalAsmLines)-2].split(' ')[1]
#TotalAsmLines = TotalAsmLines[0]

Result = subprocess.run(["wc -l */*.py"], shell=True, stdout=subprocess.PIPE).stdout.decode('utf-8')
TotalPythonLines = Result.split('\n')
TotalPythonLines = TotalPythonLines[len(TotalPythonLines)-2].split(' ')[2]

Result = subprocess.run(["wc -l */*.cu"], shell=True, stdout=subprocess.PIPE).stdout.decode('utf-8')
TotalCudaLines = Result.split(' ')
TotalCudaLines = TotalCudaLines[0]

Result = subprocess.run(["wc -l Documentation/*.tex Documentation/*/*.tex"], shell=True, stdout=subprocess.PIPE).stdout.decode('utf-8')
TotalLatexLines = Result.split(' ')
TotalLatexLines = TotalLatexLines[len(TotalLatexLines)-2]

PercentC = (int(TotalCLinesWH) / int(TotalLines)) * 100.0
PercentCuda = (int(TotalCudaLines) / int(TotalLines)) * 100.0
PercentAsm = (int(TotalAsmLines) / int(TotalLines)) * 100.0
PercentPython = (int(TotalPythonLines) / int(TotalLines)) * 100.0
PercentLatex = (int(TotalLatexLines) / int(TotalLines)) * 100.0

LineBar = "+-----------------------------------------+"
LineBarPlus = "+-------------------------------+---------+"

print("")
print(LineBar)
print("|\t\t  PodNet\t\t  |")
print(LineBarPlus)
print("| Total Lines Of Code: \t\t| " + str(TotalLines).zfill(7) + " |")
print("| Total Lines Of C Code: \t| "  + str(TotalCLines).zfill(7) + " |")
print("| Total Lines Of C Headers: \t| " + str(int(TotalCLinesWH) - int(TotalCLines)).zfill(7) + " |")
print("| Total Lines Of C With Headers:| " + str(TotalCLinesWH).zfill(7) + " |")
print("| Total Lines Of CUDA C: \t| " + str(TotalCudaLines).zfill(7) + " |")
print("| Total Lines Of Assembly: \t| " + str(TotalAsmLines).zfill(7) + " |")
print("| Total Lines Of Python: \t| " + str(TotalPythonLines).zfill(7) + " |")
print("| Total Lines Of LaTeX: \t| " + str(TotalLatexLines).zfill(7) + " |")
print(LineBarPlus)
print("| This Code Base Is {:2.2f}% C. \t\t  |".format(PercentC))

if (PercentCuda < 10.0):
	print("| This Code Base Is 0{:2.2f}% CUDA C. \t  |".format(PercentCuda))
else:
	print("| This Code Base Is {:2.2f}% CUDA C. \t  |".format(PercentCuda))

if (PercentAsm < 10.0):	
	print("| This Code Base Is 0{:02.2f}% Assembly. \t  |".format(PercentAsm))
else:
	print("| This Code Base Is {:02.2f}% Assembly. \t  |".format(PercentAsm))

if (PercentPython < 10.0):
	print("| This Code Base Is 0{:02.2f}% Python. \t  |".format(PercentPython))
else:
	print("| This Code Base Is {:02.2f}% Python. \t  |".format(PercentPython))

if (PercentLatex < 10.0):
	print("| This Code Base Is 0{:02.2f}% LaTeX. \t  |".format(PercentLatex))
else:
	print("| This Code Base Is {:02.2f}% LaTeX. \t  |".format(PercentLatex))

print(LineBar)
print("")
