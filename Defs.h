/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef DEFS_H
#define DEFS_H

#ifdef __cplusplus
#define EXTERN				extern "C"
#else
#define EXTERN				extern
#endif


#include "TypeDefs.h"
#include "Macros.h"
#include "Annotation.h"

#ifndef FALSE
#define FALSE 				0
#define TRUE				(!(FALSE))
#endif

#undef NULL
#define NULL				0

#ifndef NULLPTR
#define	NULLPTR				(VOID *)0
#endif


#if __LP64__ 	/*|| __ppc64__ */
#define __ENV_64__
#else
#define __ENV_32__
#endif


#define HOT 						hot
#define COLD 						cold

#define INLINE						__inline__ 
#define OPTIMIZE_INLINE
#define FORCE_INLINE 					__attribute__((always_inline))
#define CALL_TYPE(X)					__attribute__((X))

#ifdef __cplusplus
#define VIRTUAL 					virtual 
#else
#define VIRTUAL 					
#endif

#define RESTRICT					__restrict




#ifndef __cplusplus

#ifndef bool
#define bool 						BOOL 
#endif

#ifndef true
#define true 						TRUE
#endif

#ifndef false 
#define false 						FALSE
#endif 

#endif




#ifdef __cplusplus
#define EXTERN						extern "C"
#else
#define EXTERN						extern
#endif
#define PODNET_API					EXTERN
#define PODNET_ASM_API					extern 
#define ASM_CALL					__asm__
#define MODULE_API					EXTERN
#define PODNET_MODULE 					EXTERN



#define MAX_UULTRALONG					((UULTRALONG)0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)
#define MAX_ULONGLONG 					((ULONGLONG)0xFFFFFFFFFFFFFFFF)
#define MAX_LONGLONG 					((LONGLONG)0x7FFFFFFFFFFFFFFF)
#define MIN_LONGLONG 					((LONGLONG)~0x7FFFFFFFFFFFFFFF)	
#define MAX_ULONG					((ULONG)0xFFFFFFFF)
#define MAX_LONG 					((LONG)0x7FFFFFFF)
#define MIN_LONG 					((LONG)~0x7FFFFFFF)
#define MAX_USHORT					((USHORT)0xFFFF)
#define MAX_SHORT 					((SHORT)0x7FFF)
#define MIN_USHORT 					((SHORT)~0x7FFF)
#define MAX_UBYTE					((UBYTE)0xFF)
#define MAX_CHAR					((CHAR)0x7F)
#define MIN_CHAR 					((CHAR)~0x7F)


#ifdef __LP64__
#define MAX_PTR						MAX_ULONGLONG
#else
#define MAX_PTR						MAX_ULONG
#endif

#define EPSILON						((LONG)0x00000001)
#define EPSILON_D 					((LONGLONG)0x0000000000000002LL)

#define POS_INFINITY					(((LONG_UNION)0x7F800000U).fX)
#define NEG_INFINITY					(((LONG_UNION)0xFF800000U).fX)

#define MAX_FLOAT					(((LONG_UNION)0x7F7FFFFFU).fX)
#define MIN_FLOAT					(((LONG_UNION)0x00800000U).fX)

#define NAN32						(((LONG_UNION)0xFFC00000).fX)
#define NAN64						(((LONGLONG_UNION)0x7ff8000000000000U).dbX)

#define htonll(x) 					((1==htonl(1)) ? (x) : ((uint64_t)htonl((x) & 0xFFFFFFFF) << 32) | htonl((x) >> 32))
#define ntohll(x) 					((1==ntohl(1)) ? (x) : ((uint64_t)ntohl((x) & 0xFFFFFFFF) << 32) | ntohl((x) >> 32))

#define PI_CUBED					31.00627668f
#define PI_SQUARED					9.86960440f
#define PI2						6.28318531f
#define PI						3.14159265f
#define PI_SQRT						1.77245385f
#define PI_CBRT						1.46459189f
#define PI_2						1.57079633f
#define PI_3						1.04719755f
#define PI_4						0.78539816f
#define PI_5						0.62831853f
#define PI_6						0.52359878f
#define PI_7						0.44879895f
#define PI_8						0.39269908f
#define PI_9						0.34906585f
#define PI_10						0.31415927f



#define PI_CUBED_D					31.00627668
#define PI_SQUARED_D					9.86960440
#define PI2_D						6.28318531
#define PI_D						3.14159265
#define PI_SQRT_D					1.77245385
#define PI_CBRT_D					1.46459189
#define PI_2_D						1.57079633
#define PI_3_D						1.04719755
#define PI_4_D						0.78539816
#define PI_5_D						0.62831853
#define PI_6_D						0.52359878
#define PI_7_D						0.44879895
#define PI_8_D						0.39269908
#define PI_9_D						0.34906585
#define PI_10_D						0.31415927


#define GOLDEN_RATIO 					1.6180339887f
#define PHI 						GOLDEN_RATIO
#define GOLDEN_RATIO_D					1.6180339887
#define PHI_D						GOLDEN_RATIO_D



/* Makes conversions more readable */
#define TEN						0x000000000000000AULL
#define ONE_HUNDRED					0x0000000000000064ULL
#define ONE_THOUSAND					0x00000000000003E8ULL
#define TEN_THOUSAND					0x0000000000002710ULL
#define ONE_HUNDRED_THOUSAND				0x00000000000186A0ULL
#define ONE_MILLION					0x00000000000F4240ULL
#define TEN_MILLION					0x0000000000989680ULL
#define ONE_HUNDRED_MILLION				0x0000000005F5E100ULL
#define ONE_BILLION					0x000000003B9ACA00ULL
#define TEN_BILLION					0x00000002540BE400ULL
#define ONE_HUNDRED_BILLION				0x000000174876E800ULL
#define ONE_TRILLION					0x000000E8D4A51000ULL
#define TEN_TRILLION					0x000009184E72A000ULL
#define ONE_HUNDRED_TRILLION				0x00005AF3107A4000ULL
#define ONE_QUADRILLION					0x00038D7EA4C68000ULL
#define TEN_QUADRILLION					0x002386F26FC10000ULL
#define ONE_HUNDRED_QUADRILLION				0x016345785D8A0000ULL
#define ONE_QUINTILLION					0x0DE0B6B3A7640000ULL
#define TEN_QUINTILLION					0x8AC7230489E80000ULL


/* DOUBLE equivilants to the constants above */
#define ONE_ONE_TRILLIONTH_D				1.0E-12
#define ONE_ONE_HUNDRED_BILLIONTH_D			1.0E-11
#define ONE_TEN_BILLIONTH_D				1.0E-10
#define ONE_ONE_BILLIONTH_D				1.0E-9
#define ONE_ONE_HUNDRED_MILLIONTH_D			1.0E-8
#define ONE_TE_MILLIONTH_D				1.0E-7
#define ONE_ONE_MILLIONTH_D				1.0E-6
#define ONE_ONE_HUNDRED_THOUSANDTH_D			1.0E-5
#define ONE_TEN_THOUSANDTH_D				1.0E-4
#define ONE_ONE_THOUSANDTH_D				0.001
#define ONE_ONE_HUNDREDTH_D				0.01
#define ONE_TENTH_D					0.1
#define ONE_D						1.0
#define TEN_D						10.0
#define ONE_HUNDRED_D					100.0
#define ONE_THOUSAND_D					1.0E3
#define TEN_THOUSAND_D					1.0E4
#define ONE_HUNDRED_THOUSAND_D				1.0E5
#define ONE_MILLION_D					1.0E6
#define TEN_MILLION_D					1.0E7
#define ONE_HUNDRED_MILLION_D				1.0E8
#define ONE_BILLION_D					1.0E9
#define TEN_BILLION_D					1.0E10
#define ONE_HUNDRED_BILLION_D				1.0E11
#define ONE_TRILLION_D					1.0E12
#define TEN_TRILLION_D					1.0E13
#define ONE_HUNDRED_TRILLION_D				1.0E14
#define ONE_QUADRILLION_D				1.0E15
#define TEN_QUADRILLION_D				1.0E16
#define ONE_HUNDRED_QUADRILLION_D			1.0E17
#define ONE_QUINTILLION_D				1.0E18
#define TEN_QUINTILLION_D				1.0E19
#define ONE_HUNDRED_QUINTILLION_D			1.0E20
#define ONE_SEXTILLION_D				1.0E21


#define ANSI_RESET   					"\x1b[0m"
#define ANSI_ATTRIBUTE_BOLD				"1"
#define ANSI_ATTRIBUTE_DIM				"2"
#define ANSI_ATTRIBUTE_UNDERLINE			"3"
#define ANSI_ATTRIBUTE_BLINK				"5"
#define ANSI_ATTRIBUTE_REVERSE				"7"
#define ANSI_ATTRIBUTE_HIDDEN				"8"

#define ANSI_ATTRIBUTE_BLACK_FG				"30"
#define ANSI_ATTRIBUTE_RED_FG  				"31"
#define ANSI_ATTRIBUTE_GREEN_FG				"32"
#define ANSI_ATTRIBUTE_YELLOW_FG			"33"
#define ANSI_ATTRIBUTE_BLUE_FG 				"34"
#define ANSI_ATTRIBUTE_MAGENTA_FG			"35"
#define ANSI_ATTRIBUTE_CYAN_FG   			"36"
#define ANSI_ATTRIBUTE_WHITE_FG				"37"
#define ANSI_ATTRIBUTE_BLACK_BG				"40"
#define ANSI_ATTRIBUTE_RED_BG  				"41"
#define ANSI_ATTRIBUTE_GREEN_BG				"42"
#define ANSI_ATTRIBUTE_YELLOW_BG			"43"
#define ANSI_ATTRIBUTE_BLUE_BG 				"44"
#define ANSI_ATTRIBUTE_MAGENTA_BG			"45"
#define ANSI_ATTRIBUTE_CYAN_BG   			"46"
#define ANSI_ATTRIBUTE_WHITE_BG				"47"


#define ANSI_BLACK					"\x1b[30m"
#define ANSI_RED 					"\x1b[31m"
#define ANSI_GREEN 					"\x1b[32m"
#define ANSI_YELLOW 					"\x1b[33m"
#define ANSI_BLUE 					"\x1b[34m"
#define ANSI_MAGENTA 					"\x1b[35m"
#define ANSI_CYAN 					"\x1b[36m"
#define ANSI_WHITE 					"\x1b[37m" 	



#define ANSI_BLACK_BOLD					MAKE_ANSI_STRING(ANSI_ATTRIBUTE_BOLD, ANSI_ATTRIBUTE_BLACK_FG, ANSI_ATTRIBUTE_BLACK_BG)
#define ANSI_RED_BOLD					MAKE_ANSI_STRING(ANSI_ATTRIBUTE_BOLD, ANSI_ATTRIBUTE_RED_FG, ANSI_ATTRIBUTE_BLACK_BG)
#define ANSI_GREEN_BOLD					MAKE_ANSI_STRING(ANSI_ATTRIBUTE_BOLD, ANSI_ATTRIBUTE_GREEN_FG, ANSI_ATTRIBUTE_BLACK_BG)
#define ANSI_YELLOW_BOLD 				MAKE_ANSI_STRING(ANSI_ATTRIBUTE_BOLD, ANSI_ATTRIBUTE_YELLOW_FG, ANSI_ATTRIBUTE_BLACK_BG)
#define ANSI_BLUE_BOLD 					MAKE_ANSI_STRING(ANSI_ATTRIBUTE_BOLD, ANSI_ATTRIBUTE_BLUE_FG, ANSI_ATTRIBUTE_BLACK_BG)
#define ANSI_MAGENTA_BOLD 				MAKE_ANSI_STRING(ANSI_ATTRIBUTE_BOLD, ANSI_ATTRIBUTE_MAGENTA_FG, ANSI_ATTRIBUTE_BLACK_BG)
#define ANSI_CYAN_BOLD 					MAKE_ANSI_STRING(ANSI_ATTRIBUTE_BOLD, ANSI_ATTRIBUTE_CYAN_FG, ANSI_ATTRIBUTE_BLACK_BG)
#define ANSI_WHITE_BOLD 				MAKE_ANSI_STRING(ANSI_ATTRIBUTE_BOLD, ANSI_ATTRIBUTE_WHITE_FG, ANSI_ATTRIBUTE_BLACK_BG)




#define MAKE_ANSI_STRING(A,FG,BG)			\
	"\x1b[" A ";" FG ";" BG "m"



/* Function "Renames" */
#define InterlockedExchange				__sync_lock_test_and_set

#if __LP64__
#define InterlockedExchangePointer(P,V)			InterlockedExchange((LPULONGLONG)(P), (ULONGLONG)(V))
#else
#define InterlockedExchangePointer(P,V)			InterlockedExchange((LPULONG)(P), (ULONG)(V))
#endif

#define InterlockedAdd					__sync_add_and_fetch
#define InterlockedSub					__sync_sub_and_fetch
#define InterlockedIncrement(PTR)			InterlockedAdd((PTR), 1)
#define InterlockedDecrement(PTR)			InterlockedSub((PTR), 1)
#define InterlockedAcquire(PTR)				InterlockedAdd((PTR), 0)
#define InterlockedCompareExchange			__sync_val_compare_and_swap
#define InterlockedCompareExchangeHappened		__sync_bool_compare_and_swap
#define InterlockedAcquireExchange(PTR, VAL)		InterlockedCompareExchange((PTR), InterlockedAcquire((PTR)), (VAL))
#define InterlockedAcquireExchangeHappened(PTR, VAL)	InterlockedCompareExchangeHappened((PTR), InterlockedAcquire((PTR)), (VAL))		


#define SetMemory(ADDR,VAL,SZ)				memset((ADDR),(VAL),(SZ))
#define ZeroMemory(ADDR,SZ)				SetMemory((ADDR),0,(SZ))
#define CopyMemory(DEST,SRC,SZ)				memcpy((DEST),(SRC),(SZ))
#define MoveMemory(DEST,SRC,SZ)				memmove((DEST),(SRC),(SZ))
#define LocateCharacterInMemory(PTR,VAL,SZ)		memchr((PTR),(VAL),(SZ))
#define LocateCharacterInBuffer				LocateCharacterInMemory
#define CompareMemory(CMP1,CMP2,SZ)			memcmp((CMP1),(CMP2),(SZ))

#define LocalAlloc(SZ)					malloc((SZ))
#define LocalAllocAndZero(SZ)				calloc(1, (SZ))
#define GlobalAlloc(SZ)					malloc((SZ))
#define GlobalAllocAndZero(SZ)				calloc(1, (SZ))
#define LocalReAlloc(P,SZ)				realloc((P),(SZ))
#define GlobalReAlloc(P,SZ)				realloc((P),(SZ))
#define FreeMemory(P)					free((P))


#define JUMP(L)						goto L

#define PrintFormat					printf
#define StringPrintFormat				sprintf
#define StringPrintFormatSafe 				snprintf
#define ScanFormat					scanf
#define StringScanFormat				sscanf

#define StringLength					strlen
#define StringCopy					strcpy
#define StringCopySafe					strncpy
#define StringConcatenate				strcat
#define StringConcatenateSafe				strncat
#define StringCompare					strcmp
#define StringInString					strstr

#define AsciiToChar(STR, OUT)				StringScanFormat((STR), "%c", (OUT))
#define AsciiToByte(STR, OUT)				StringScanFormat((STR), "%hhu", (OUT))
#define AsciiToShort(STR, OUT)				StringScanFormat((STR), "%hd", (OUT))
#define AsciiToUnsignedShort(STR, OUT)			StringScanFormat((STR), "%hu", (OUT))
#define AsciiToLong(STR, OUT)				StringScanFormat((STR), "%d", (OUT))
#define AsciiToUnsignedLong(STR, OUT)			StringScanFormat((STR), "%u", (OUT))
#define AsciiToLongLong(STR, OUT)			StringScanFormat((STR), "%ll", (OUT))
#define AsciiToUnsignedLongLong(STR, OUT)		StringScanFormat((STR), "%llu", (OUT))
#define AsciiToPointer(STR, OUT)			StringScanFormat((STR), "%p", (OUT))
#define AsciiToFloat(STR, OUT)				StringScanFormat((STR), "%f", (OUT))
#define AsciiToDouble(STR, OUT)				StringScanFormat((STR), "%lf", (OUT))
#define AsciiToLongDouble(STR, OUT)			StringScanFormat((STR), "%L", (OUT))

/*
#define Write						write
#define Read 						read 
#define Open						open
#define Close						close
*/

#define HostToNetworkLong				htonl
#define HostToNetworkShort				htons
#define HostToNetworkLongLong				htonll
#define NetworkToHostLong				ntohl
#define NetworkToHostShort				ntohs
#define NetworkToHostLongLong				ntohll



#define Main						main

#endif
