#  ____           _ _   _      _   
# |  _ \ ___   __| | \ | | ___| |_ 
# | |_) / _ \ / _` |  \| |/ _ \ __|
# |  __/ (_) | (_| | |\  |  __/ |_ 
# |_|   \___/ \__,_|_| \_|\___|\__|
# 
# 
# General Purpose C Library.
# Copyright (C) 2017-2019 Zach Podbielniak
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


#FROM zachpodbielniak/arch:latest
FROM archlinux:latest

LABEL maintainer="Zach Podbielniak"

WORKDIR /home
RUN pacman -Syyu --noconfirm

# Update repos
#COPY mirrorlist /etc/pacman.d/mirrorlist
RUN pacman -S --noconfirm reflector
RUN reflector --latest 20 --sort rate --protocol https  --save /etc/pacman.d/mirrorlist

# Install dependencies
RUN pacman -S --noconfirm --needed base-devel cgdb git wget lua geoip geoip-database geoip-database-extra gpsd nasm

# Do the makepkg edit
COPY Req/makepkg-x64 .
RUN mv makepkg-x64 /usr/bin/makepkg

# Get Yaourt
RUN git clone https://aur.archlinux.org/package-query.git
RUN git clone https://aur.archlinux.org/yaourt.git

WORKDIR /home/package-query
RUN makepkg -si --asroot --noconfirm

WORKDIR /home/yaourt
RUN makepkg -si --asroot --noconfirm

WORKDIR /home
RUN rm -rf yaourt/ package-query/

# Install Only AUR Package
#RUN yaourt -S --noconfirm wiringpi-git


WORKDIR /PodNet
COPY . .

# Build 
RUN mkdir -p bin
RUN make -j $(nproc) BUILD_TYPE=docker
RUN make -j $(nproc) debug BUILD_TYPE=docker
RUN make install
RUN make install_debug
RUN make install_headers
RUN make install_python
RUN make test -j $(nproc)
