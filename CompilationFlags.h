/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef COMPILATIONFLAGS_H
#define COMPILATIONFLAGS_H


/* HANDLE Table Flags */
#define HANDLE_TABLE_HASHTABLE_SIZE				0x4000

/* MODULE Flags */
#define MODULE_TRAMPOLINE_THUNK_DISPATCH_TABLE_SIZE 		0x100

/* HTTP Request Flags */
#define HTTP_REQUEST_BUFFER_SIZE 				0x2000

/* IP Address of GetPublicIpAddress Server for IPv4 (ipv4life.xyz) */
#define HTTP_GET_IPV4_SERVER 					"ipv4life.xyz" /*"198.58.127.106"*/

/* IP Address of GetPublicIpAddress Server for IPv6 (ipv6life.xyz) */
#define HTTP_GET_IPV6_SERVER 					"ipv6life.xyz"

/* Database file for GeoIP library */
#define GEO_IPV4_DATABASE					"/usr/share/GeoIP/GeoIP.dat"
#define GEO_IPV6_DATABASE					"/usr/share/GeoIP/GeoIPv6.dat"
#define GEO_IPV4_CITY_DATABASE					"/usr/share/GeoIP/GeoIPCity.dat"
#define GEO_IPV6_CITY_DATABASE					"/usr/share/GeoIP/GeoIPCityv6.dat"

/* SMTP Connection Settings */
#define SMTP_PASSWORD_FORMAT					"%s %s"



#endif