/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CRANDOM_H
#define CRANDOM_H




#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../CSystem/CSystem.h"
#include "../CMemory/CMemory.h"
#include "../CAtom/CAtomTable.h"




typedef enum __RANDOM_SIZE
{
        RANDOM_SIZE_BYTE        = 1,
        RANDOM_SIZE_SHORT,
        RANDOM_SIZE_LONG,
        RANDOM_SIZE_LONGLONG,
        RANDOM_SIZE_ULTRALONG,
        RANDOM_SIZE_ARCHLONG
	/* RANDOM_SIZE_ARBLONG TODO: Implement */
} RANDOM_SIZE;




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNET_API
HANDLE 
CreateRandomNumberGenerator(
        _In_            RANDOM_SIZE             rsBaseSize,
        _In_            LPVOID                  lpSeedValue,
        _In_Opt_        LPCSTR                  lpcszName    
);




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNET_API
HANDLE 
CreateRandomNumberGeneratorByAtom(
	_In_ 		RANDOM_SIZE 		rsBaseSize,
	_In_ 		LPVOID RESTRICT 	lpSeedValue,
	_In_Opt_	HANDLE 			hAtomTable,
	_In_ 		ATOM 			atValue
);




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNET_API
HANDLE
GetRandomNumberGeneratorByName(
        _In_            LPCSTR                  lpcszName
);




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNET_API
HANDLE
GetRandomNumberGeneratorByAtom(
        _In_            HANDLE                  hAtomTable,
        _In_            ATOM                    atValue
);




_Success_(return != FALSE, _Acquires_Shared_Lock_(_Has_Lock_Kind_(_Lock_Kind_Spin_Lock_)))
PODNET_API
BOOL 
GetSingleRawRandom(
        _In_            HANDLE                  hRng,
        _Out_           LPVOID                  lpVal,
        _In_            UARCHLONG               ualOutBufferSize
);




_Success_(return != FALSE, _Acquires_Shared_Lock_(_Has_Lock_Kind_(_Lock_Kind_Spin_Lock_)))
PODNET_API
BOOL
GetMultipleRawRandoms(
        _In_            HANDLE                  hRng,
        _In_            UARCHLONG               ualNumberOfRandoms,
        _Out_           LPVOID                  lpOut,
        _In_            UARCHLONG               ualOutBufferSize,
        _Out_Opt_       LPUARCHLONG             lpualRandomsWritten
);




_Success_(return != FALSE, _Acquires_Shared_Lock_(_Has_Lock_Kind_(_Lock_Kind_Spin_Lock_)))
PODNET_API
BOOL
GetSingleRandomInRange(
        _In_            HANDLE                  hRng,
        _In_            UULTRALONG              uultLowerRange,
        _In_            UULTRALONG              uultUpperRange,
        _Out_           LPVOID                  lpOut,
        _In_            UARCHLONG               ualOutBufferSize
);




_Success_(return != FALSE, _Acquires_Shared_Lock_(_Has_Lock_Kind_(_Lock_Kind_Spin_Lock_)))
PODNET_API
BOOL
GetMultipleRandomsInRange(
        _In_            HANDLE                  hRng,
        _In_            UULTRALONG              uultLowerRange,
        _In_            UULTRALONG              uultUpperRange,
        _Out_           LPVOID                  lpOut,
        _In_            UARCHLONG               ualOutBufferSize,
        _Out_Opt_       LPUARCHLONG             lpualRandomsWritten
);




_Success_(return != FALSE, _Acquires_Shared_Lock_(_Has_Lock_Kind_(_Lock_Kind_Spin_Lock_)))
PODNET_API
BOOL
ReSeedRandomNumberGenerator(
        _In_            HANDLE                  hRng,
        _In_            LPVOID                  lpSeedValue
);




_Success_(return != FALSE, _Acquires_Shared_Lock_(_Has_Lock_Kind_(_Lock_Kind_Spin_Lock_)))
PODNET_API
BOOL 
GetSingleGlobalRawRandom(
        _Out_           LPULONGLONG             lpullOut,
        _Reserved_      ULONG                   ulFlags
);




_Success_(return != FALSE, _Acquires_Shared_Lock_(_Has_Lock_Kind_(_Lock_Kind_Spin_Lock_)))
PODNET_API
BOOL
GetMultipleGlobalRawRandoms(
        _In_            UARCHLONG               ualNumberOfRandoms,
        _Out_           LPULONGLONG             lpullOutBuffer,
        _In_            UARCHLONG               ualSizeOfOutBuffer,
        _Out_Opt_       LPUARCHLONG             lpualNumberOfRandomsWritten,
        _Reserved_      ULONG                   ulFlags
);




_Success_(return != FALSE, _Acquires_Shared_Lock_(_Has_Lock_Kind_(_Lock_Kind_Spin_Lock_)))
PODNET_API
BOOL
GetSingleGlobalRandomInRange(
        _In_            ULONGLONG               ullLowerRange,
        _In_            ULONGLONG               ullUpperRange,
        _Out_           LPULONGLONG             lpullOut
);




_Success_(return != FALSE, _Acquires_Shared_Lock_(_Has_Lock_Kind_(_Lock_Kind_Spin_Lock_)))
PODNET_API
BOOL
GetMultipleGlobalRandomsInRange(
        _In_            ULONGLONG               ullLowerRange,
        _In_            ULONGLONG               ullUpperRange,
        _In_            UARCHLONG               ualNumberOfRandoms,
        _Out_           LPULONGLONG             lpullOut,
        _In_            UARCHLONG               ualOutBufferSize,
        _Out_Opt_       LPUARCHLONG             lpualRandomsWritten
);




#endif