/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CArbitraryLong.h"
#include "../CMemory/CMemory.h"
#include <gmp.h>

typedef struct __ARBITRARYLONG
{
        mpz_t           mpzState;
} ARBITRARYLONG, *LPARBITRARYLONG;




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HARBLONG
CreateArbitraryLong(
        _In_Z_          LPCSTR          lpcszValue,
        _In_            LONG            lBase 
){
        HARBLONG halVal;
        mpz_init_set_str(halVal.mpzState, lpcszValue, lBase);
        return halVal;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
DestroyArbitraryLong(
        _In_            HARBLONG        halValue
){
        mpz_clear(halValue.mpzState);
        return TRUE;
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HARBLONG
AddArbitraryLong(
        _In_            HARBLONG        halAugend,
        _In_            HARBLONG        halAddend
){
        HARBLONG halOut;
        mpz_init(halOut.mpzState);
        mpz_add(halOut.mpzState, halAugend.mpzState, halAddend.mpzState);
        return halOut;
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HARBLONG
SubtractArbitraryLong(
        _In_            HARBLONG        halMinuend,
        _In_            HARBLONG        halSubtrahend
){
        HARBLONG halOut;
        mpz_init(halOut.mpzState);
        mpz_sub(halOut.mpzState, halMinuend.mpzState, halSubtrahend.mpzState);
        return halOut;
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HARBLONG
MultiplyArbitraryLong(
        _In_            HARBLONG        halMultiplier,
        _In_            HARBLONG        halMultiplicand
){
        HARBLONG halOut;
        mpz_init(halOut.mpzState);
        mpz_mul(halOut.mpzState, halMultiplier.mpzState, halMultiplicand.mpzState);
        return halOut;
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HARBLONG
DivideArbitraryLong(
        _In_            HARBLONG        halDividend,
        _In_            HARBLONG        halDivisor
){
        HARBLONG halOut;
        mpz_init(halOut.mpzState);
        mpz_div(halOut.mpzState, halDividend.mpzState, halDivisor.mpzState);
        return halOut;
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HARBLONG
AddAndMultiplyArbitraryLong(
        _In_            HARBLONG        halAugend,
        _In_            HARBLONG        halMultiplier,
        _In_            HARBLONG        halMultiplicand
){
        HARBLONG halOut;
        mpz_init(halOut.mpzState);
        mpz_addmul(halAugend.mpzState, halMultiplier.mpzState, halMultiplicand.mpzState);
        return halOut;
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HARBLONG
SubtractAndMultiplyArbitraryLong(
        _In_            HARBLONG        halMinuend,
        _In_            HARBLONG        halMultiplier,
        _In_            HARBLONG        halMultiplicand
){
        HARBLONG halOut;
        mpz_init(halOut.mpzState);
        mpz_submul(halMinuend.mpzState, halMultiplier.mpzState, halMultiplicand.mpzState);
        return halOut;
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HARBLONG
MultiplyArbitraryLongByPowerOfTwo(
        _In_            HARBLONG        halMultiplier,
        _In_            ULONGLONG       ullPower 
){
        HARBLONG halOut;
        mpz_init(halOut.mpzState);
        mpz_mul_2exp(halOut.mpzState, halMultiplier.mpzState, (mp_bitcnt_t)ullPower);
        return halOut;
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HARBLONG
AbsoluteValueOfArbitraryLong(
        _In_            HARBLONG        halValue
){
        HARBLONG halOut;
        mpz_init(halOut.mpzState);
        mpz_abs(halOut.mpzState, halValue.mpzState);
        return halOut;
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HARBLONG
NegateArbitraryLong(
        _In_            HARBLONG        halValue
){
        HARBLONG halOut;
        mpz_init(halOut.mpzState);
        mpz_neg(halOut.mpzState, halValue.mpzState);
        return halOut;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
ArbitraryLongToString(
        _In_                    HARBLONG        halValue,
        _In_Range_(-36,62)      LONG            lBase,            
        _In_                    DLPSTR          dlpszOut,
        _In_                    UARCHLONG       ualBufferSize
){
        EXIT_IF_UNLIKELY_NULL(dlpszOut, FALSE);
        EXIT_IF_CONDITION(mpz_sizeinbase(halValue.mpzState, lBase) < ualBufferSize, FALSE);
        mpz_get_str(*dlpszOut, lBase, halValue.mpzState);
        return TRUE;
}




_Success_(return != NULL, _Non_Locking_)
PODNET_API
UARCHLONG
ArbitraryLongGetStringSize(
        _In_            HARBLONG        halValue,
        _In_            LONG            lBase
){ return (UARCHLONG)mpz_sizeinbase(halValue.mpzState, lBase); }




_Success_(_Inexpressible_, _Non_Locking_)
PODNET_API
UARCHLONG
ArbitraryLongToUnsignedArchLongWithTruncation(
        _In_            HARBLONG        halValue
){ return (UARCHLONG)mpz_get_ui(halValue.mpzState); }




_Success_(_Inexpressible_, _Non_Locking_)
ARCHLONG
ArbitraryLongToSignedArchLongWithTruncation(
        _In_            HARBLONG        halValue
){ return (ARCHLONG)mpz_get_si(halValue.mpzState); }




_Success_(_Inexpressible_, _Non_Locking_)
DOUBLE
ArbitraryLongToDoubleWithTruncation(
        _In_            HARBLONG        halValue
){ return (DOUBLE)mpz_get_d(halValue.mpzState); }




/*
        This returns the base multiplier, and the 
        value 2 is raised to the power of. For example
        return(DOUBLE) * 2 ^ lpalExponentOut.
*/
_Success_(_Inexpressible_, _Non_Locking_)
DOUBLE
ArbitraryLongToDoubleMultiplierAndLogBaseTwoWithTruncation(
        _In_            HARBLONG        halValue,
        _Out_           LPARCHLONG      lpalExponentOut
){ 
        EXIT_IF_UNLIKELY_NULL(lpalExponentOut, NAN64);
        return (DOUBLE)mpz_get_d_2exp(lpalExponentOut, halValue.mpzState); 
}