/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CVector3D.h"



/* GLOBAL_VARIABLE CONSTANT VECTOR3D v3Zero = {0.0f, 0.0f, 0.0f}; */
GLOBAL_VARIABLE CONSTANT VECTOR3D v3UnitX = {1.0f, 0.0f, 0.0f};
GLOBAL_VARIABLE CONSTANT VECTOR3D v3UnitY = {0.0f, 1.0f, 0.0f};
GLOBAL_VARIABLE CONSTANT VECTOR3D v3UnitZ = {0.0f, 0.0f, 1.0f};
GLOBAL_VARIABLE CONSTANT VECTOR3D v3NegUnitX = {-1.0f, 0.0f, 0.0f};
GLOBAL_VARIABLE CONSTANT VECTOR3D v3NegUnitY = {0.0f, -1.0f, 0.0f};
GLOBAL_VARIABLE CONSTANT VECTOR3D v3NegUnitZ = {0.0f, 0.0f, -1.0f};
/* GLOBAL_VARIABLE CONSTANT VECTOR3D v3UnitScale = {1.0f, 1.0f, 1.0f}; */





_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPVECTOR3D
CreateVector3D(
	_In_ 		FLOAT 			fX,
	_In_ 		FLOAT 			fY,
	_In_ 		FLOAT 			fZ
){
	LPVECTOR3D lpv3Out;
	lpv3Out = (LPVECTOR3D)GlobalAllocAndZero(sizeof(VECTOR3D));

	/* cppcheck-suppress memleak */
	EXIT_IF_UNLIKELY_NULL(lpv3Out, NULLPTR);

	lpv3Out->fX = fX;
	lpv3Out->fY = fY;
	lpv3Out->fZ = fZ;
	return lpv3Out;
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPVECTOR3D
CreateVector3DFromArray(
	_In_ 		LPFLOAT RESTRICT 	lpfVals
){
	EXIT_IF_LIKELY_NULL(lpfVals, NULLPTR);

	LPVECTOR3D lpv3Out;
	lpv3Out = (LPVECTOR3D)GlobalAllocAndZero(sizeof(VECTOR3D));

	/* cppcheck-suppress memleak */
	EXIT_IF_UNLIKELY_NULL(lpv3Out, NULLPTR);

	CopyMemory(lpv3Out->fVals, lpfVals, sizeof(FLOAT) * 0x03U);
	return lpv3Out;
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPVECTOR3D
CreateVector3DFromVector3D(
	_In_ 		LPVECTOR3D RESTRICT 	lpv3Vec
){
	EXIT_IF_UNLIKELY_NULL(lpv3Vec, NULLPTR);
	return CreateVector3D(lpv3Vec->fX, lpv3Vec->fY, lpv3Vec->fZ);
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
DestroyVector3D(
	_In_ 		LPVECTOR3D RESTRICT 	lpv3Vec
){
	EXIT_IF_UNLIKELY_NULL(lpv3Vec, FALSE);
	FreeMemory(lpv3Vec);
	return TRUE;
}




/* If lpv3Out == NULLPTR, lpv3Vec will be the NORMALIZED VECTOR3D */
_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPVECTOR3D
Vector3DAdd(
	_Out_Opt_ 	LPVECTOR3D  		lpv3Out,
	_In_ 		LPVECTOR3D  		lpv3Vec1,
	_In_ 		LPVECTOR3D		lpv3Vec2 
){
	EXIT_IF_UNLIKELY_NULL(lpv3Vec1, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpv3Vec2, NULLPTR);

	BOOL bRet;
	bRet = FALSE;

	if (NULLPTR == lpv3Out)
	{
		lpv3Out = CreateVector3D(0.0f, 0.0f, 0.0f);
		EXIT_IF_UNLIKELY_NULL(lpv3Out, NULLPTR);
		bRet = TRUE;
	}

	lpv3Out->fX = lpv3Vec1->fX + lpv3Vec2->fX;
	lpv3Out->fY = lpv3Vec1->fY + lpv3Vec2->fY;
	lpv3Out->fZ = lpv3Vec1->fZ + lpv3Vec2->fZ;

	if (TRUE == bRet)
	{ return lpv3Out; }
	return (LPVECTOR3D)(LPVOID)TRUE;
}




/* If lpv3Out == NULLPTR, lpv3Vec will be the NORMALIZED VECTOR3D */
_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPVECTOR3D
Vector3DSubtract(
	_Out_Opt_ 	LPVECTOR3D  		lpv3Out,
	_In_ 		LPVECTOR3D  		lpv3Vec1,
	_In_ 		LPVECTOR3D		lpv3Vec2 
){
	EXIT_IF_UNLIKELY_NULL(lpv3Vec1, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpv3Vec2, NULLPTR);

	BOOL bRet;
	bRet = FALSE;

	if (NULLPTR == lpv3Out)
	{
		lpv3Out = CreateVector3D(0.0f, 0.0f, 0.0f);
		EXIT_IF_UNLIKELY_NULL(lpv3Out, NULLPTR);
		bRet = TRUE;
	}

	lpv3Out->fX = lpv3Vec1->fX - lpv3Vec2->fX;
	lpv3Out->fY = lpv3Vec1->fY - lpv3Vec2->fY;
	lpv3Out->fZ = lpv3Vec1->fZ - lpv3Vec2->fZ;

	if (TRUE == bRet)
	{ return lpv3Out; }
	return (LPVECTOR3D)(LPVOID)TRUE;
}




/* If lpv3Out == NULLPTR, lpv3Vec will be the NORMALIZED VECTOR3D */
_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPVECTOR3D
Vector3DMultiply(
	_Out_Opt_ 	LPVECTOR3D  		lpv3Out,
	_In_ 		LPVECTOR3D  		lpv3Vec1,
	_In_ 		LPVECTOR3D		lpv3Vec2 
){
	EXIT_IF_UNLIKELY_NULL(lpv3Vec1, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpv3Vec2, NULLPTR);

	BOOL bRet;
	bRet = FALSE;

	if (NULLPTR == lpv3Out)
	{
		lpv3Out = CreateVector3D(0.0f, 0.0f, 0.0f);
		EXIT_IF_UNLIKELY_NULL(lpv3Out, NULLPTR);
		bRet = TRUE;
	}

	lpv3Out->fX = lpv3Vec1->fX * lpv3Vec2->fX;
	lpv3Out->fY = lpv3Vec1->fY * lpv3Vec2->fY;
	lpv3Out->fZ = lpv3Vec1->fZ * lpv3Vec2->fZ;

	if (TRUE == bRet)
	{ return lpv3Out; }
	return (LPVECTOR3D)(LPVOID)TRUE;
}




/* If lpv3Out == NULLPTR, lpv3Vec will be the NORMALIZED VECTOR3D */
_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPVECTOR3D
Vector3DDivide(
	_Out_Opt_ 	LPVECTOR3D  		lpv3Out,
	_In_ 		LPVECTOR3D  		lpv3Vec1,
	_In_ 		LPVECTOR3D		lpv3Vec2 
){
	EXIT_IF_UNLIKELY_NULL(lpv3Vec1, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpv3Vec2, NULLPTR);

	BOOL bRet;
	bRet = FALSE;

	if (NULLPTR == lpv3Out)
	{
		lpv3Out = CreateVector3D(0.0f, 0.0f, 0.0f);
		EXIT_IF_UNLIKELY_NULL(lpv3Out, NULLPTR);
		bRet = TRUE;
	}

	lpv3Out->fX = lpv3Vec1->fX / lpv3Vec2->fX;
	lpv3Out->fY = lpv3Vec1->fY / lpv3Vec2->fY;
	lpv3Out->fZ = lpv3Vec1->fZ / lpv3Vec2->fZ;

	if (TRUE == bRet)
	{ return lpv3Out; }
	return (LPVECTOR3D)(LPVOID)TRUE;
}




_Success_(return != NULLPTR, _Non_Locking_) 
PODNET_API
LPVECTOR3D
Vector3DAddScalar(
	_Out_Opt_ 	LPVECTOR3D 		lpv3Out,
	_In_ 		LPVECTOR3D 		lpv3Vec,
	_In_ 		FLOAT 			fScalar
){
	EXIT_IF_UNLIKELY_NULL(lpv3Vec, NULLPTR);
	EXIT_IF_UNLIKELY_VALUE(FLOAT_EQUAL(fScalar, NAN32), TRUE, NULLPTR);

	BOOL bRet;
	bRet = FALSE;

	if (NULLPTR == lpv3Out)
	{
		lpv3Out = CreateVector3D(0.0f, 0.0f, 0.0f);
		EXIT_IF_UNLIKELY_NULL(lpv3Out, NULLPTR);
		bRet = TRUE;
	}

	lpv3Out->fX = lpv3Vec->fX + fScalar;
	lpv3Out->fY = lpv3Vec->fY + fScalar;
	lpv3Out->fZ = lpv3Vec->fZ + fScalar;

	if (TRUE == bRet)
	{ return lpv3Out; }
	return (LPVECTOR3D)(LPVOID)TRUE;
}




_Success_(return != NULLPTR, _Non_Locking_) 
PODNET_API
LPVECTOR3D
Vector3DSubtractScalar(
	_Out_Opt_ 	LPVECTOR3D 		lpv3Out,
	_In_ 		LPVECTOR3D 		lpv3Vec,
	_In_ 		FLOAT 			fScalar
){
	EXIT_IF_UNLIKELY_NULL(lpv3Vec, NULLPTR);
	EXIT_IF_UNLIKELY_VALUE(FLOAT_EQUAL(fScalar, NAN32), TRUE, NULLPTR);

	BOOL bRet;
	bRet = FALSE;

	if (NULLPTR == lpv3Out)
	{
		lpv3Out = CreateVector3D(0.0f, 0.0f, 0.0f);
		EXIT_IF_UNLIKELY_NULL(lpv3Out, NULLPTR);
		bRet = TRUE;
	}

	lpv3Out->fX = lpv3Vec->fX - fScalar;
	lpv3Out->fY = lpv3Vec->fY - fScalar;
	lpv3Out->fZ = lpv3Vec->fZ - fScalar;

	if (TRUE == bRet)
	{ return lpv3Out; }
	return (LPVECTOR3D)(LPVOID)TRUE;
}




_Success_(return != NULLPTR, _Non_Locking_) 
PODNET_API
LPVECTOR3D
Vector3DMultiplyScalar(
	_Out_Opt_ 	LPVECTOR3D 		lpv3Out,
	_In_ 		LPVECTOR3D 		lpv3Vec,
	_In_ 		FLOAT 			fScalar
){
	EXIT_IF_UNLIKELY_NULL(lpv3Vec, NULLPTR);
	EXIT_IF_UNLIKELY_VALUE(FLOAT_EQUAL(fScalar, NAN32), TRUE, NULLPTR);

	BOOL bRet;
	bRet = FALSE;

	if (NULLPTR == lpv3Out)
	{
		lpv3Out = CreateVector3D(0.0f, 0.0f, 0.0f);
		EXIT_IF_UNLIKELY_NULL(lpv3Out, NULLPTR);
		bRet = TRUE;
	}

	lpv3Out->fX = lpv3Vec->fX * fScalar;
	lpv3Out->fY = lpv3Vec->fY * fScalar;
	lpv3Out->fZ = lpv3Vec->fZ * fScalar;

	if (TRUE == bRet)
	{ return lpv3Out; }
	return (LPVECTOR3D)(LPVOID)TRUE;
}




_Success_(return != NULLPTR, _Non_Locking_) 
PODNET_API
LPVECTOR3D
Vector3DDivideScalar(
	_Out_Opt_ 	LPVECTOR3D 		lpv3Out,
	_In_ 		LPVECTOR3D 		lpv3Vec,
	_In_ 		FLOAT 			fScalar
){
	EXIT_IF_UNLIKELY_NULL(lpv3Vec, NULLPTR);
	EXIT_IF_UNLIKELY_VALUE(FLOAT_EQUAL(fScalar, NAN32), TRUE, NULLPTR);
	EXIT_IF_UNLIKELY_VALUE(FLOAT_EQUAL(fScalar, 0.0f), TRUE, NULLPTR);

	BOOL bRet;
	bRet = FALSE;

	if (NULLPTR == lpv3Out)
	{
		lpv3Out = CreateVector3D(0.0f, 0.0f, 0.0f);
		EXIT_IF_UNLIKELY_NULL(lpv3Out, NULLPTR);
		bRet = TRUE;
	}

	fScalar = 1.0f / fScalar;

	lpv3Out->fX = lpv3Vec->fX * fScalar;
	lpv3Out->fY = lpv3Vec->fY * fScalar;
	lpv3Out->fZ = lpv3Vec->fZ * fScalar;

	if (TRUE == bRet)
	{ return lpv3Out; }
	return (LPVECTOR3D)(LPVOID)TRUE;
}




_Success_(return != NAN32, _Non_Locking_)
PODNET_API
FLOAT
Vector3DLength(
	_In_ 		LPVECTOR3D RESTRICT 	lpv3Vec
){
	EXIT_IF_UNLIKELY_NULL(lpv3Vec, NAN32);
	KEEP_IN_REGISTER LPFLOAT lpfVals;
	lpfVals = lpv3Vec->fVals;
	return sqrtf(
		(lpfVals[0] * lpfVals[0]) + 
		(lpfVals[1] * lpfVals[1]) + 
		(lpfVals[2] * lpfVals[2])
	);
}




_Success_(return != NAN32, _Non_Locking_)
PODNET_API
FLOAT
Vector3DLengthSquared(
	_In_ 		LPVECTOR3D RESTRICT 	lpv3Vec
){
	EXIT_IF_UNLIKELY_NULL(lpv3Vec, NAN32);
	KEEP_IN_REGISTER LPFLOAT lpfVals;
	lpfVals = lpv3Vec->fVals;
	return (
		(lpfVals[0] * lpfVals[0]) + 
		(lpfVals[1] * lpfVals[1]) + 
		(lpfVals[2] * lpfVals[2])
	);
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL 
Vector3DIsZeroLength(
	_In_ 		LPVECTOR3D RESTRICT 	lpv3Vec
){
	EXIT_IF_UNLIKELY_NULL(lpv3Vec, FALSE);
	KEEP_IN_REGISTER LPFLOAT lpfVals;
	lpfVals = lpv3Vec->fVals;
	return ((
		(lpfVals[0] * lpfVals[0]) + 
		(lpfVals[1] * lpfVals[1]) + 
		(lpfVals[2] + lpfVals[2])
	) < (FLOAT)(1E-06 * 1E-06) ? TRUE : FALSE);
}




_Success_(return != NAN32, _Non_Locking_)
PODNET_API
FLOAT
Vector3DDistance(
	_In_ 		LPVECTOR3D 		lpv3Vec1,
	_In_ 		LPVECTOR3D 		lpv3Vec2
){
	EXIT_IF_UNLIKELY_NULL(lpv3Vec1, NAN32);
	EXIT_IF_UNLIKELY_NULL(lpv3Vec2, NAN32);
	
	FLOAT fDifference[3];

	fDifference[0] = (lpv3Vec1->fX - lpv3Vec2->fX) * (lpv3Vec1->fX - lpv3Vec2->fX);
	fDifference[1] = (lpv3Vec1->fY - lpv3Vec2->fY) * (lpv3Vec1->fY - lpv3Vec2->fY);
	fDifference[2] = (lpv3Vec2->fZ - lpv3Vec2->fZ) * (lpv3Vec2->fZ - lpv3Vec2->fZ);

	return sqrtf(fDifference[0] + fDifference[1] + fDifference[2]);
}




_Success_(return != NAN32, _Non_Locking_)
PODNET_API
FLOAT
Vector3DDistanceSquared(
	_In_ 		LPVECTOR3D 		lpv3Vec1,
	_In_ 		LPVECTOR3D 		lpv3Vec2
){
	EXIT_IF_UNLIKELY_NULL(lpv3Vec1, NAN32);
	EXIT_IF_UNLIKELY_NULL(lpv3Vec2, NAN32);
	
	FLOAT fDifference[3];

	fDifference[0] = (lpv3Vec1->fX - lpv3Vec2->fX) * (lpv3Vec1->fX - lpv3Vec2->fX);
	fDifference[1] = (lpv3Vec1->fY - lpv3Vec2->fY) * (lpv3Vec1->fY - lpv3Vec2->fY);
	fDifference[2] = (lpv3Vec2->fZ - lpv3Vec2->fZ) * (lpv3Vec2->fZ - lpv3Vec2->fZ);

	return (fDifference[0] + fDifference[1] + fDifference[2]);
}




_Success_(return != NAN32, _Non_Locking_)
PODNET_API
FLOAT
Vector3DDotProduct(
	_In_ 		LPVECTOR3D 		lpv3Vec1,
	_In_ 		LPVECTOR3D 		lpv3Vec2
){
	EXIT_IF_UNLIKELY_NULL(lpv3Vec1, NAN32);
	EXIT_IF_UNLIKELY_NULL(lpv3Vec2, NAN32);

	return(
		(lpv3Vec1->fX * lpv3Vec2->fX) +
		(lpv3Vec1->fY * lpv3Vec2->fY) +
		(lpv3Vec1->fZ * lpv3Vec2->fZ)
	);
}




_Success_(return != NAN32, _Non_Locking_)
PODNET_API
FLOAT
Vector3DDotProductAbsolute(
	_In_ 		LPVECTOR3D 		lpv3Vec1,
	_In_ 		LPVECTOR3D 		lpv3Vec2
){
	EXIT_IF_UNLIKELY_NULL(lpv3Vec1, NAN32);
	EXIT_IF_UNLIKELY_NULL(lpv3Vec2, NAN32);

	return fabsf(
		(lpv3Vec1->fX * lpv3Vec2->fX) +
		(lpv3Vec1->fY * lpv3Vec2->fY) +
		(lpv3Vec1->fZ * lpv3Vec2->fZ)
	);
}




/* If lpv3Out == NULLPTR, lpv3Vec will be the NORMALIZED VECTOR3D */
/* FLOAT return is the length */
_Success_(return != NAN32, _Non_Locking_)
PODNET_API
FLOAT
Vector3DNormalize(
	_Out_Opt_ 	LPVECTOR3D RESTRICT 	lpv3Out,
	_In_ 		LPVECTOR3D RESTRICT 	lpv3Vec
){
	EXIT_IF_UNLIKELY_NULL(lpv3Vec, NAN32);

	FLOAT fLength, fInverseLength;

	if (NULLPTR == lpv3Out)
	{ lpv3Out = lpv3Vec; }

	fLength = sqrtf(
		(lpv3Out->fX * lpv3Out->fX) +
		(lpv3Out->fY * lpv3Out->fY) +
		(lpv3Out->fZ * lpv3Out->fZ)
	);

	if (0.0f < fLength)
	{
		fInverseLength = 1.0f / fLength;
		lpv3Out->fX *= fInverseLength;
		lpv3Out->fY *= fInverseLength;
		lpv3Out->fZ *= fInverseLength;
	}

	return fLength;
}




/* If lpv3Out == NULLPTR, return will be a newly alloc'd VECTOR3D */
_Success_(return != NULLPTR, _Non_Locking_)
_When_(lpv3Out != NULLPTR { _Success_(return = (LPVECTOR3D)(LPVOID)TRUE) })
PODNET_API
LPVECTOR3D
Vector3DCrossProduct(
	_Out_Opt_ 	LPVECTOR3D 		lpv3Out,
	_In_ 		LPVECTOR3D 		lpv3Vec1,
	_In_ 		LPVECTOR3D 		lpv3Vec2
){
	EXIT_IF_UNLIKELY_NULL(lpv3Vec1, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpv3Vec2, NULLPTR);

	if (NULLPTR == lpv3Out)
	{
		lpv3Out = CreateVector3D(
			(lpv3Vec1->fY * lpv3Vec2->fZ) - (lpv3Vec1->fZ * lpv3Vec2->fY), 
			(lpv3Vec1->fZ * lpv3Vec2->fX) - (lpv3Vec1->fX * lpv3Vec2->fZ), 
			(lpv3Vec1->fX * lpv3Vec2->fY) - (lpv3Vec1->fY * lpv3Vec2->fX)
		);

		EXIT_IF_UNLIKELY_NULL(lpv3Out, NULLPTR);
		return lpv3Out;
	}

	lpv3Out->fX = (lpv3Vec1->fY * lpv3Vec2->fZ) - (lpv3Vec1->fZ * lpv3Vec2->fY); 
	lpv3Out->fY = (lpv3Vec1->fZ * lpv3Vec2->fX) - (lpv3Vec1->fX * lpv3Vec2->fZ); 
	lpv3Out->fZ = (lpv3Vec1->fX * lpv3Vec2->fY) - (lpv3Vec1->fY * lpv3Vec2->fX);

	return (LPVECTOR3D)(LPVOID)TRUE;
}




/* If lpv3Out == NULLPTR, return will be a newly alloc'd VECTOR3D */
_Success_(return != NULLPTR, _Non_Locking_)
_When_(lpv3Out != NULLPTR { _Success_(return = (LPVECTOR3D)(LPVOID)TRUE) })
PODNET_API
LPVECTOR3D
Vector3DMidPoint(
	_Out_Opt_ 	LPVECTOR3D 		lpv3Out,
	_In_ 		LPVECTOR3D 		lpv3Vec1,
	_In_ 		LPVECTOR3D 		lpv3Vec2
){
	EXIT_IF_UNLIKELY_NULL(lpv3Vec1, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpv3Vec2, NULLPTR);

	if (NULLPTR == lpv3Out)
	{
		lpv3Out = CreateVector3D(
			0.5f * (lpv3Vec1->fX + lpv3Vec2->fX),
			0.5f * (lpv3Vec1->fY + lpv3Vec2->fY),
			0.5f * (lpv3Vec1->fZ + lpv3Vec2->fZ)
		);

		EXIT_IF_UNLIKELY_NULL(lpv3Out, NULLPTR);
		return lpv3Out;
	}

	lpv3Out->fX = 0.5f * (lpv3Vec1->fX + lpv3Vec2->fX);
	lpv3Out->fY = 0.5f * (lpv3Vec1->fY + lpv3Vec2->fY);
	lpv3Out->fZ = 0.5f * (lpv3Vec1->fZ + lpv3Vec2->fZ);

	return (LPVECTOR3D)(LPVOID)TRUE;	
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL 
Vector3DLessThan(
	_In_ 		LPVECTOR3D 		lpv3Left,
	_In_ 		LPVECTOR3D 		lpv3Right
){
	EXIT_IF_UNLIKELY_NULL(lpv3Left, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpv3Right, FALSE);

	return (
		(lpv3Left->fX < lpv3Right->fX) &
		(lpv3Left->fY < lpv3Right->fY) &
		(lpv3Left->fZ < lpv3Right->fZ)
	) ? TRUE : FALSE;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL 
Vector3DGreaterThan(
	_In_ 		LPVECTOR3D 		lpv3Left,
	_In_ 		LPVECTOR3D 		lpv3Right
){
	EXIT_IF_UNLIKELY_NULL(lpv3Left, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpv3Right, FALSE);

	return (
		(lpv3Left->fX > lpv3Right->fX) &
		(lpv3Left->fY > lpv3Right->fY) &
		(lpv3Left->fZ > lpv3Right->fZ)
	) ? TRUE : FALSE;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL 
Vector3DEqual(
	_In_ 		LPVECTOR3D 		lpv3Left,
	_In_ 		LPVECTOR3D 		lpv3Right
){
	EXIT_IF_UNLIKELY_NULL(lpv3Left, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpv3Right, FALSE);

	return (
		FLOAT_EQUAL(lpv3Left->fX, lpv3Right->fX) &
		FLOAT_EQUAL(lpv3Left->fY, lpv3Right->fY) &
		FLOAT_EQUAL(lpv3Left->fZ, lpv3Right->fZ)
	) ? TRUE : FALSE;
}




/* If lpv3Out == NULLPTR, return will be a newly alloc'd VECTOR3D */
_Success_(return != NULLPTR, _Non_Locking_)
_When_(lpv3Out != NULLPTR { _Success_(return = (LPVECTOR3D)(LPVOID)TRUE) })
PODNET_API
LPVECTOR3D
Vector3DPerpendicular(
	_Out_Opt_ 	LPVECTOR3D 		lpv3Out,
	_In_ 		LPVECTOR3D 		lpv3Vec
){
	EXIT_IF_UNLIKELY_NULL(lpv3Vec, NULLPTR);

	FLOAT fReallySmall;
	BOOL bRet;
	bRet = FALSE;

	if (NULLPTR == lpv3Out)
	{
		lpv3Out = CreateVector3D(0.0f, 0.0f, 0.0f);
		EXIT_IF_UNLIKELY_NULL(lpv3Out, NULLPTR);
		bRet = TRUE;
	}

	fReallySmall = (FLOAT)(1E-06 * 1E-06);

	Vector3DCrossProduct(lpv3Out, lpv3Vec, (LPVECTOR3D)&v3UnitX);

	if (Vector3DLengthSquared(lpv3Out) < fReallySmall)
	{ Vector3DCrossProduct(lpv3Out, lpv3Out, (LPVECTOR3D)&v3UnitY); }

	Vector3DNormalize(NULLPTR, lpv3Out);

	if (TRUE == bRet)
	{ return lpv3Out; }
	return (LPVECTOR3D)(LPVOID)TRUE;
}




_Success_(return != NAN32, _Non_Locking_)
PODNET_API
FLOAT
Vector3DAngleBetween(
	_In_ 		LPVECTOR3D 		lpv3Vec1,
	_In_ 		LPVECTOR3D 		lpv3Vec2
){
	EXIT_IF_UNLIKELY_NULL(lpv3Vec1, NAN32);
	EXIT_IF_UNLIKELY_NULL(lpv3Vec2, NAN32);

	FLOAT fLength;
	FLOAT fAngle;
	fAngle = 0.0f;

	fLength = Vector3DLength(lpv3Vec1) * Vector3DLength(lpv3Vec2);

	if (fLength < 1E-06f)
	{ fLength = 1E-06f; }

	fAngle = Vector3DDotProduct(lpv3Vec1, lpv3Vec2) / fLength;

	fAngle = CLAMP(fAngle, -1.0f, 1.0f);
	
	return acosf(fAngle);
}




_Success_(return != NAN32, _Non_Locking_)
PODNET_API
FLOAT
Vector3DAngleBetweenFast(
	_In_ 		LPVECTOR3D 		lpv3Vec1,
	_In_ 		LPVECTOR3D 		lpv3Vec2
){
	EXIT_IF_UNLIKELY_NULL(lpv3Vec1, NAN32);
	EXIT_IF_UNLIKELY_NULL(lpv3Vec2, NAN32);

	FLOAT fLength;
	FLOAT fAngle;

	fLength = Vector3DLength(lpv3Vec1) * Vector3DLength(lpv3Vec2);

	if (fLength < 1E-06f)
	{ fLength = 1E-06f; }

	fAngle = Vector3DDotProduct(lpv3Vec1, lpv3Vec2) / fLength;

 	fAngle = CLAMP(fAngle, -1.0f, 1.0f);
	
	return FAST_APPROX_ARCCOS(fAngle);
}




/* If lpv3Out == NULLPTR, return will be a newly alloc'd VECTOR3D */
_Success_(return != NULLPTR, _Non_Locking_)
_When_(lpv3Out != NULLPTR { _Success_(return = (LPVECTOR3D)(LPVOID)TRUE) })
PODNET_API
LPVECTOR3D
Vector3DReflect(
	_Out_Opt_ 	LPVECTOR3D 		lpv3Out,
	_In_ 		LPVECTOR3D 		lpv3Vec1,
	_In_ 		LPVECTOR3D 		lpv3Vec2
){
	EXIT_IF_UNLIKELY_NULL(lpv3Vec1, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpv3Vec2, NULLPTR);

	FLOAT fScalar;
	BOOL bRet;
	bRet = FALSE;

	if (NULLPTR == lpv3Out)
	{
		lpv3Out = CreateVector3D(0.0f, 0.0f, 0.0f);
		EXIT_IF_UNLIKELY_NULL(lpv3Out, NULLPTR);
		bRet = TRUE;
	}

	fScalar = 2.0f * Vector3DDotProduct(lpv3Vec1, lpv3Vec2);
	Vector3DMultiplyScalar(lpv3Out, lpv3Vec2, fScalar);
	Vector3DSubtract(lpv3Out, lpv3Vec1, lpv3Vec2);

	if (TRUE == bRet)
	{ return lpv3Out; }
	return (LPVECTOR3D)(LPVOID)TRUE;
}




/* If lpv3Out == NULLPTR, return will be a newly alloc'd VECTOR3D */
_Success_(return != NULLPTR, _Non_Locking_)
_When_(lpv3Out != NULLPTR { _Success_(return = (LPVECTOR3D)(LPVOID)TRUE) })
PODNET_API
LPVECTOR3D
Vector3DPrimaryAxis(
	_Out_Opt_ 	LPVECTOR3D RESTRICT 	lpv3Out,
	_In_		LPVECTOR3D RESTRICT 	lpv3Vec
){
	EXIT_IF_UNLIKELY_NULL(lpv3Out, NULLPTR);

	FLOAT fAbsoluteX, fAbsoluteY, fAbsoluteZ;
	BOOL bRet;
	bRet = FALSE;

	if (NULLPTR == lpv3Out)
	{
		lpv3Out = CreateVector3D(0.0f, 0.0f, 0.0f);
		EXIT_IF_UNLIKELY_NULL(lpv3Out, NULLPTR);
		bRet = TRUE;
	}

	fAbsoluteX = fabsf(lpv3Vec->fX);
	fAbsoluteY = fabsf(lpv3Vec->fY);
	fAbsoluteZ = fabsf(lpv3Vec->fZ);

	if (fAbsoluteX > fAbsoluteY)
	{
		if (fAbsoluteX > fAbsoluteZ)
		{ 
			(lpv3Vec->fX > 0.0f) ? 
				CopyMemory(lpv3Out->fVals, v3UnitX.fVals, sizeof(FLOAT) * 3) :
				CopyMemory(lpv3Out->fVals, v3NegUnitX.fVals, sizeof(FLOAT) * 3);
		}
		else
		{
			(lpv3Vec->fZ > 0.0f) ? 
				CopyMemory(lpv3Out->fVals, v3UnitZ.fVals, sizeof(FLOAT) * 3) :
				CopyMemory(lpv3Out->fVals, v3NegUnitZ.fVals, sizeof(FLOAT) * 3);
		}
	}
	else
	{
		if (fAbsoluteY > fAbsoluteZ)
		{ 
			(lpv3Vec->fY > 0.0f) ? 
				CopyMemory(lpv3Out->fVals, v3UnitY.fVals, sizeof(FLOAT) * 3) :
				CopyMemory(lpv3Out->fVals, v3NegUnitY.fVals, sizeof(FLOAT) * 3);
		}
		else
		{
			(lpv3Vec->fZ > 0.0f) ? 
				CopyMemory(lpv3Out->fVals, v3UnitZ.fVals, sizeof(FLOAT) * 3) :
				CopyMemory(lpv3Out->fVals, v3NegUnitZ.fVals, sizeof(FLOAT) * 3);
		}
	}


	if (TRUE == bRet)
	{ return lpv3Out; }
	return (LPVECTOR3D)(LPVOID)TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL 
Vector3DPositionEquals(
	_In_ 		LPVECTOR3D RESTRICT 	lpv3Vec1,
	_In_ 		LPVECTOR3D RESTRICT 	lpv3Vec2
){
	EXIT_IF_UNLIKELY_NULL(lpv3Vec1, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpv3Vec2, FALSE);

	FLOAT fTolerance;
	BOOL bRet;

	fTolerance = 1E-03f;
	bRet = TRUE;

	bRet &= (fabsf(lpv3Vec1->fX - lpv3Vec2->fX) > fTolerance);
	bRet &= (fabsf(lpv3Vec1->fY - lpv3Vec2->fY) > fTolerance);
	bRet &= (fabsf(lpv3Vec1->fZ - lpv3Vec2->fZ) > fTolerance);

	return bRet;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL 
Vector3DIsNotANumber(
	_In_ 		LPVECTOR3D RESTRICT 	lpv3Vec
){
	EXIT_IF_UNLIKELY_NULL(lpv3Vec, FALSE);

	return (
		FLOAT_EQUAL(NAN32, lpv3Vec->fX) |
		FLOAT_EQUAL(NAN32, lpv3Vec->fY) |
		FLOAT_EQUAL(NAN32, lpv3Vec->fZ)
	);
}