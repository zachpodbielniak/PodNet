/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CSHAPES2D_H
#define CSHAPES2D_H
#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "CCoordinates.h"
#include "CFastApprox.h"

typedef enum __SHAPE_TYPE
{
	SHAPE_UNDEFINED		= 0x00,
        SHAPE_NOT_A_SHAPE       = 0x01,
        SHAPE_CIRCLE            = 0x02,
        SHAPE_RECTANGLE         = 0x03,
        SHAPE_TRIANGLE          = 0x04,
        SHAPE_ELLIPSE           = 0x05,
        SHAPE_PARALLELOGRAM     = 0x06,
        SHAPE_TRAPEZOID         = 0x07,
        SHAPE_TRAPEZIUM         = 0x08,
        SHAPE_ANNULUS           = 0x09,
        SHAPE_POLYGON           = 0x0A,
        SHAPE_CIRCULAR_SECTOR   = 0x0B,
        SHAPE_ANNULAR_SECTOR    = 0x0C,
        SHAPE_FILLET            = 0x0D,
        SHAPE_OTHER             = 0xFF
} SHAPE_TYPE;

typedef struct __SHAPE
{
        LPVOID          lpChild;
        ULONG           ulShapeType;
        COORDINATE      corCoordinate;          /* This is the center. */
        DOUBLE          dbRotation;
} SHAPE, *LPSHAPE, **DLPSHAPE, ***TLPSHAPE;

#define INHERITS_FROM_SHAPE()           LPSHAPE         lpshpParent


typedef struct __CIRCLE
{
        INHERITS_FROM_SHAPE();
        DOUBLE          dbRadius;
} CIRCLE, *LPCIRCLE, **DLPCIRCLE, ***TLPCIRCLE;

typedef CIRCLE                  *GPCIRCLE;

typedef struct __RECTANGLE
{
        INHERITS_FROM_SHAPE();
        DOUBLE          dbBase;
        DOUBLE          dbHeight;
} RECTANGLE, *LPRECTANGLE, **DLPRECTANGLE, ***TLPRECTANGLE;

typedef RECTANGLE               *GPRECTANGLE;

typedef enum __TRIANGLE_TYPE
{
        EQUILATERAL_TRIANGLE    = 0x01,
        RIGHT_TRIANGLE          = 0x02,
        OBLIQUE_TRIANGLE        = 0x03
} TRIANGLE_TYPE;

typedef struct __TRIANGLE
{
        INHERITS_FROM_SHAPE();
        DOUBLE          dbBase;
        DOUBLE          dbHeight;
        DOUBLE          dbTheta;        
        ULONG           ulType;
} TRIANGLE, *LPTRIANGLE, **DLPTRIANGLE, ***TLPTRIANGLE;

typedef TRIANGLE                *GPTRIANGLE;

typedef struct __ELLIPSE
{
        INHERITS_FROM_SHAPE();
        DOUBLE          dbR1;
        DOUBLE          dbR2;
} ELLIPSE, *LPELLIPSE, **DLPELLIPSE, ***TLPELLIPSE;


typedef struct __PARALLELOGRAM
{
        INHERITS_FROM_SHAPE();
        DOUBLE          dbBase;
        DOUBLE          dbHeight;
        DOUBLE          dbTheta;
} PARALLELOGRAM, *LPPARALLELOGRAM, **DLPPARALLELOGRAM, ***TLPPARALLELOGRAM;

typedef PARALLELOGRAM           *GPPARALLELOGRAM;


typedef struct __TRAPEZOID
{
        INHERITS_FROM_SHAPE();
        DOUBLE          dbBase;
        DOUBLE          dbHead;
        DOUBLE          dbHeight;
        DOUBLE          dbTheta;
} TRAPEZOID, *LPTRAPEZOID, **DLPTRAPEZOID, ***TLPTRAPEZOID;

typedef TRAPEZOID               *GPTRAPEZOID;

typedef struct __TRAPEZIUM       /* A = (hR(base - b2) + hL(base - b1)) / 2 */
{
        INHERITS_FROM_SHAPE();
        DOUBLE          dbBase;
        DOUBLE          dbHead;
        DOUBLE          dbHeightLeft;
        DOUBLE          dbHeightRight;
        DOUBLE          dbTheta;
} TRAPEZIUM, *LPTRAPEZIUM, **DLPTRAPEZIUM, ***TLPTRAPEZIUM;

typedef TRAPEZIUM               *GPTRAPEZIUM;

typedef struct __ANNULUS         /* A = (PI * (r2^2 - r1^2))/2 */
{
        INHERITS_FROM_SHAPE();
        DOUBLE          dbInnerRadius;
        DOUBLE          dbOuterRadius;
} ANNULUS, *LPANNULUS, **DLPANNULUS, ***TLPANNULUS;

typedef ANNULUS                 *GPANNULUS;

typedef struct __POLYGON         /* A = (n * sLen * r) / 2 */
{
        INHERITS_FROM_SHAPE();
        DOUBLE          dbSideLength;
        ULONG           ulNumberOfSides;
        DOUBLE          dbRadius;
} POLYGON, *LPPOLYGON, **DLPPOLYGON, ***TLPPOLYGON;

typedef POLYGON                 *GPPOLYGON;

typedef struct __CIRCULAR_SECTOR
{
        INHERITS_FROM_SHAPE();
        DOUBLE          dbRadius;
        DOUBLE          dbTheta;
} CIRCULAR_SECTOR, *LPCIRCULAR_SECTOR, **DLPCIRCULAR_SECTOR, ***TLPCIRCULAR_SECTOR;

typedef CIRCULAR_SECTOR           *GPCIRCULAR_SECTOR;

typedef struct __ANNULAR_SECTOR  /* A = (PI * Theta * (r2^2 - r1^2)) / 360 */
{
        INHERITS_FROM_SHAPE();
        DOUBLE          dbInnerRadius;
        DOUBLE          dbOuterRadius;
        DOUBLE          dbTheta;
} ANNULAR_SECTOR, *LPANNULAR_SECTOR, **DLPANNULAR_SECTOR, ***TLPANNULAR_SECTOR;

typedef ANNULAR_SECTOR          *GPANNULAR_SECTOR;

typedef struct __FILLET          /* A = r^2(1 - pi/4) */
{
        INHERITS_FROM_SHAPE();
        DOUBLE          dbRadius;
} FILLET, *LPFILLET, **DLPFILLET, ***TLPFILLET;

typedef FILLET                  *GPFILLET;



_Result_Null_On_Failure_
PODNET_API
LPSHAPE
CreateShape(
        _In_                            DOUBLE          dbX,
        _In_                            DOUBLE          dbY,
        _In_Opt_                        DOUBLE          dbZ,
        _In_                            ULONG           ulShapeType
);


_Result_Null_On_Failure_
PODNET_API
LPSHAPE
CreateShapeEx(
        _In_                            DOUBLE          dbX,
        _In_                            DOUBLE          dbY,
        _In_Opt_                        DOUBLE          dbZ,
        _In_Opt_                        DOUBLE          dbMisc,
        _In_Opt_                        DOUBLE          dbTheta,
        _In_                            DOUBLE          dbCoorX,
        _In_                            DOUBLE          dbCoorY,
        _In_                            ULONG           ulShapeType,
        _In_Opt_                        ULONG           ulShapeFlags
);


_Success_(return != NAN, ...)
PODNET_API
BOOL
DestroyShape(
        _In_                            LPSHAPE         lpshpShape
);



_Success_(return != NAN, ...)
PODNET_API
DOUBLE
Area(
        _In_                            LPSHAPE         lpshpShape
);



_Success_(return != NAN, ...)
PODNET_API
DOUBLE
Perimeter(
        _In_                            LPSHAPE         lpshpShape
);


_Success_(return != NAN, ...)
PODNET_API
DOUBLE
FastPerimeter(
        _In_                            LPSHAPE         lpshpShape
);

#endif
