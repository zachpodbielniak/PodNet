/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CMATH_H
#define CMATH_H



#include "CArbitraryLong.h"
#include "CCoordinates.h"
#include "CFastApprox.h"
#include "CMatrix.h"
#include "CQuaternion.h"
#include "CRandom.h"
#include "CShapes2D.h"
#include "CVector3D.h"



_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
IsFloatEqual(
        _In_                    FLOAT           fX,
        _In_                    FLOAT           fY
) CALL_TYPE(HOT);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
IsDoubleEqual(
        _In_                    DOUBLE          dX,
        _In_                    DOUBLE          dY
) CALL_TYPE(HOT);


#endif
