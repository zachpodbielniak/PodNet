/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CQUATERNION_H
#define CQUATERNION_H

#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../CMemory/CMemory.h"
#include "CVector3D.h"




typedef struct __QUATERNION
{
	union
	{
		FLOAT fVals[4];
		struct { FLOAT fW, fX, fY, fZ; };
	};
} QUATERNION, *LPQUATERNION, **DLPQUATERNION, ***TLPQUATERNION;



#define QUATERNION_SET(QUAT,W,X,Y,Z)			\
	(QUAT)->fW = (W);				\
	(QUAT)->fX = (X);				\
	(QUAT)->fY = (Y);				\
	(QUAT)->fZ = (Z)


#define QUATERNION_ZERO(QUAT)				QUATERNION_SET((QUAT), 0.0f, 0.0f, 0.0f, 0.0f)


#define QUATERNION_COPY(LEFT,RIGHT)			CopyMemory((LEFT)->fVals, (RIGHT)->fVals, sizeof(FLOAT) * 4)





_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPQUATERNION
CreateQuaternion(
	_In_ 		FLOAT 			fW,
	_In_ 		FLOAT 			fX,
	_In_ 		FLOAT 			fY,
	_In_ 		FLOAT 			fZ 
);



_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPQUATERNION
CreateQuaternionFromArray(
	_In_ 		LPFLOAT RESTRICT 	lpfVals
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
BOOL
DestroyQuaternion(
	_In_ 		LPQUATERNION RESTRICT	lpquaQuat
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPVECTOR3D
QuaternionGetAxisX(
	_In_ 		LPQUATERNION RESTRICT	lpquaQuat
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPVECTOR3D
QuaternionGetAxisY(
	_In_ 		LPQUATERNION RESTRICT	lpquaQuat
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPVECTOR3D
QuaternionGetAxisZ(
	_In_ 		LPQUATERNION RESTRICT	lpquaQuat
);




/* If first parameter is NULLPTR, then a new quaternion will be allocated and return */
_Success_(return != NULLPTR, _Non_Locking_)
_When_(lpquaSum != NULLPTR { _Success_(return == (LPQUATERNION)(LPVOID)TRUE) })
PODNET_API
LPQUATERNION
QuaternionAdd(
	_In_Opt_	LPQUATERNION 		lpquaSum,
	_In_ 		LPQUATERNION 		lpquaAugend,
	_In_ 		LPQUATERNION 		lpquaAddend
);




/* If first parameter is NULLPTR, then a new quaternion will be allocated and return */
_Success_(return != NULLPTR, _Non_Locking_)
_When_(lpquaDifference != NULLPTR { _Success_(return == (LPQUATERNION)(LPVOID)TRUE) })
PODNET_API
LPQUATERNION
QuaternionSubtract(
	_In_Opt_	LPQUATERNION 		lpquaDifference,
	_In_ 		LPQUATERNION 		lpquaMinuend,
	_In_ 		LPQUATERNION 		lpquaSubtrahend
);




/* If first parameter is NULLPTR, then a new quaternion will be allocated and return */
_Success_(return != NULLPTR, _Non_Locking_)
_When_(lpquaProduct != NULLPTR { _Success_(return == (LPQUATERNION)(LPVOID)TRUE) })
PODNET_API
LPQUATERNION
QuaternionMultiply(
	_In_Opt_	LPQUATERNION 		lpquaProduct,
	_In_ 		LPQUATERNION 		lpquaMultiplicand,
	_In_ 		LPQUATERNION 		lpquaMultiplier
);




/* If first parameter is NULLPTR, then a new quaternion will be allocated and return */
_Success_(return != NULLPTR, _Non_Locking_)
_When_(lpquaProduct != NULLPTR { _Success_(return == (LPQUATERNION)(LPVOID)TRUE) })
PODNET_API
LPQUATERNION
QuaternionMultiplyByScalar(
	_In_Opt_	LPQUATERNION 		lpquaProduct,
	_In_ 		LPQUATERNION 		lpquaMultiplicand,
	_In_ 		FLOAT 			fScalar
);




/* If first parameter is NULLPTR, then a new quaternion will be allocated and return */
_Success_(return != NULLPTR, _Non_Locking_)
_When_(lpquaQuotient != NULLPTR { _Success_(return == (LPQUATERNION)(LPVOID)TRUE) })
PODNET_API
LPQUATERNION
QuaternionDivide(
	_In_Opt_	LPQUATERNION 		lpquaQuotient,
	_In_ 		LPQUATERNION 		lpquaDividend,
	_In_ 		LPQUATERNION 		lpquaDivisor
);




/* If first parameter is NULLPTR, then a new quaternion will be allocated and return */
_Success_(return != NULLPTR, _Non_Locking_)
_When_(lpquaOut != NULLPTR { _Success_(return == (LPQUATERNION)(LPVOID)TRUE) })
PODNET_API
LPQUATERNION
QuaternionNegate(
	_In_Opt_ 	LPQUATERNION 		lpquaOut,
	_In_ 		LPQUATERNION 		lpquaToBeNegated
);




#define QuaternionNegateCurrent(QUAT)		QuaternionNegate(QUAT, QUAT)




_Success_(return != NAN32, _Non_Locking_)
PODNET_API
FLOAT 
QuaternionDotProduct(
	_In_ 		LPQUATERNION 		lpquaDoterand,
	_In_ 		LPQUATERNION		lpquaDoterplier
);




_Success_(return != NAN32, _Non_Locking_)
PODNET_API
FLOAT
QuaternionNorm(
	_In_		LPQUATERNION RESTRICT 	lpquaNormerand
);




_Success_(return != NAN32, _Non_Locking_)
PODNET_API
FLOAT
QuaternionNormalize(
	_In_		LPQUATERNION RESTRICT 	lpquaNormerand
);




/* If first parameter is NULLPTR, then a new quaternion will be allocated and returned */
_Success_(return != NULLPTR, _Non_Locking_)
_When_(lpquaInverted != NULLPTR { _Success_(return == (LPQUATERNION)(LPVOID)TRUE) })
PODNET_API
LPQUATERNION
QuaternionInverse(
	_Out_Opt_	LPQUATERNION  		lpquaInverted,
	_In_ 		LPQUATERNION 		lpquaInverterand
);




/* If first parameter is NULLPTR, then a new quaternion will be allocated and returned */
_Success_(return != NULLPTR, _Non_Locking_)
_When_(lpquaUnitInverted != NULLPTR { _Success_(return == (LPQUATERNION)(LPVOID)TRUE) })
PODNET_API
LPQUATERNION
QuaternionUnitInverse(
	_Out_Opt_	LPQUATERNION  		lpquaUnitInverted,
	_In_ 		LPQUATERNION 		lpquaUnitInverterand
);




/* If first parameter is NULLPTR, then a new quaternion will be allocated and returned */
_Success_(return != NULLPTR, _Non_Locking_)
_When_(lpquaExped != NULLPTR { _Success_(return == (LPQUATERNION)(LPVOID)TRUE) })
PODNET_API
LPQUATERNION
QuaternionExp(
	_Out_Opt_	LPQUATERNION  		lpquaExped,
	_In_ 		LPQUATERNION 		lpquaExperand
);




/* If first parameter is NULLPTR, then a new quaternion will be allocated and returned */
_Success_(return != NULLPTR, _Non_Locking_)
_When_(lpquaExped != NULLPTR { _Success_(return == (LPQUATERNION)(LPVOID)TRUE) })
PODNET_API
LPQUATERNION
QuaternionExpFast(
	_Out_Opt_	LPQUATERNION  		lpquaExped,
	_In_ 		LPQUATERNION 		lpquaExperand
);




/* If first parameter is NULLPTR, then a new quaternion will be allocated and returned */
_Success_(return != NULLPTR, _Non_Locking_)
_When_(lpquaLogout != NULLPTR { _Success_(return == (LPQUATERNION)(LPVOID)TRUE) })
PODNET_API
LPQUATERNION
QuaternionLog(
	_Out_Opt_	LPQUATERNION  		lpquaLogout,
	_In_ 		LPQUATERNION 		lpquaLogerand
);




/* If first parameter is NULLPTR, then a new quaternion will be allocated and returned */
_Success_(return != NULLPTR, _Non_Locking_)
_When_(lpquaLogout != NULLPTR { _Success_(return == (LPQUATERNION)(LPVOID)TRUE) })
PODNET_API
LPQUATERNION
QuaternionLogFast(
	_Out_Opt_	LPQUATERNION  		lpquaLogout,
	_In_ 		LPQUATERNION 		lpquaLogerand
);




/* If first parameter is NULLPTR, then a new quaternion will be allocated and returned */
_Success_(return != NULLPTR, _Non_Locking_)
_When_(lpquaLogout != NULLPTR { _Success_(return == (LPVECTOR3D)(LPVOID)TRUE) })
PODNET_API
LPVECTOR3D
QuaternionMultiplyByVector3D(
	_Out_Opt_ 	LPVECTOR3D RESTRICT 	lpv3Out,
	_In_ 		LPQUATERNION RESTRICT 	lpquaMultiplicand,
	_In_ 		LPVECTOR3D RESTRICT 	lpv3Multiplier
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
QuaternionEqual(
	_In_ 		LPQUATERNION RESTRICT 	lpquaLefterand,
	_In_ 		LPQUATERNION RESTRICT 	lpquaRighterand
);




/* If first parameter is NULLPTR, then a new quaternion will be allocated and returned */
_Success_(return != NULLPTR, _Non_Locking_)
_When_(lpquaLogout != NULLPTR { _Success_(return == (LPQUATERNION)(LPVOID)TRUE) })
PODNET_API
LPQUATERNION
QuaternionSlerp(
	_Out_Opt_ 	LPQUATERNION RESTRICT 	lpquaOut,
	_In_ 		LPQUATERNION RESTRICT 	lpquaP,
	_In_ 		LPQUATERNION RESTRICT 	lpquaQ,
	_In_ 		FLOAT 			fDistance,
	_In_ 		BOOL 			bShortestPath
);




/* If first parameter is NULLPTR, then a new quaternion will be allocated and returned */
_Success_(return != NULLPTR, _Non_Locking_)
_When_(lpquaLogout != NULLPTR { _Success_(return == (LPQUATERNION)(LPVOID)TRUE) })
PODNET_API
LPQUATERNION
QuaternionSlerpExtraSpins(
	_Out_Opt_ 	LPQUATERNION RESTRICT 	lpquaOut,
	_In_ 		LPQUATERNION RESTRICT 	lpquaP,
	_In_ 		LPQUATERNION RESTRICT 	lpquaQ,
	_In_ 		FLOAT 			fDistance,
	_In_ 		LONG 			lExtraSpins
);




_Success_(return != NAN32, _Non_Locking_)
PODNET_API
FLOAT
QuaternionGetRoll(
	_In_ 		LPQUATERNION RESTRICT 	lpquaRollerand,
	_In_ 		BOOL 			bReProject
);




_Success_(return != NAN32, _Non_Locking_)
PODNET_API
FLOAT
QuaternionGetPitch(
	_In_ 		LPQUATERNION RESTRICT 	lpquaPitcherand,
	_In_ 		BOOL 			bReProject
);




_Success_(return != NAN32, _Non_Locking_)
PODNET_API
FLOAT
QuaternionGetYaw(
	_In_ 		LPQUATERNION RESTRICT 	lpquaYawerand,
	_In_ 		BOOL 			bReProject
);


#endif 