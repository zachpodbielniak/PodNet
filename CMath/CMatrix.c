/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CMatrix.h"




typedef struct __MATRIX
{
        UARCHLONG               ualRows;
        UARCHLONG               ualColumns;
        LPVOID                  lpData;
        TYPE                    tyType;
        BOOL                    bIsVectorized;
} MATRIX, *LPMATRIX, **DLPMATRIX;


typedef enum __MATRIX_ALLOC_FLAGS
{
        MATRIX_ALLOC_VECTOR             = 0 << 0,
        MATRIX_ALLOC_TWO_DIMENSIONAL    = 1 << 1
};



_Success_(return != NULLPTR, _Non_Locking_)
INTERNAL_OPERATION
DLPVOID
__AllocMatrix(
        _In_            UARCHLONG               ualRows,
        _In_            UARCHLONG               ualColumns,
        _In_            UARCHLONG               ualObjectSize,
        _In_            ULONG                   ulFlags
){
        UNREFERENCED_PARAMETER(ulFlags);
        DLPVOID dlpOut, dlpIterator;
        dlpOut = NULLPTR;


        /* This is probably never going to be used... */
        if (ulFlags & MATRIX_ALLOC_TWO_DIMENSIONAL)
        {
                dlpOut = (DLPVOID)GlobalAllocAndZero(ualColumns * sizeof(LPVOID));
		/* cppcheck-suppress memleak */
                EXIT_IF_UNLIKELY_NULL(dlpOut, NULLPTR);

                for (
                        dlpIterator = dlpOut;
                        ualRows != 0;
                        ualRows--
                ){
                        *(DLPUARCHLONG)dlpIterator = (LPUARCHLONG)GlobalAlloc(ualRows * ualObjectSize);

			/* cppcheck-suppress memleak */
                        EXIT_IF_UNLIKELY_NULL(*(DLPUARCHLONG)dlpIterator, NULLPTR);
                        dlpIterator = (LPVOID)((UARCHLONG)dlpIterator + sizeof(LPVOID));
                }
        }

        else
        {
                dlpOut = (DLPVOID)GlobalAllocAndZero(ualRows * ualColumns * ualObjectSize);
		/* cppcheck-suppress memleak */
                EXIT_IF_UNLIKELY_NULL(dlpOut, NULLPTR);
        }


        return dlpOut;
}




_Result_Null_On_Failure_
_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HMATRIX
CreateMatrix(
        _In_            UARCHLONG               ualRows,
        _In_            UARCHLONG               ualColumns,
        _In_            TYPE                    tyType,
        _In_            UARCHLONG               ualSize,
        _In_Opt_        ULONG                   ulFlags
){
        UNREFERENCED_PARAMETER(ulFlags);
        LPMATRIX lpMatrix;
        lpMatrix = GlobalAlloc(sizeof(MATRIX));
	/* cppcheck-suppress memleak */
        EXIT_IF_UNLIKELY_NULL(lpMatrix, NULLPTR);

        lpMatrix->bIsVectorized = TRUE;
        lpMatrix->tyType = tyType;
        lpMatrix->ualColumns = ualColumns;
        lpMatrix->ualRows = ualRows;
        lpMatrix->lpData = (LPVOID)__AllocMatrix(ualRows, ualColumns, TypeToSize(tyType), MATRIX_ALLOC_VECTOR);
	/* cppcheck-suppress memleak */
        EXIT_IF_UNLIKELY_NULL(lpMatrix->lpData, NULLPTR);

        return (HMATRIX)lpMatrix;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
DestroyMatrix(
        _In_            HMATRIX                 hmMatrix
){
        EXIT_IF_UNLIKELY_NULL(hmMatrix, FALSE);
        FreeMemory(hmMatrix->lpData);
        FreeMemory(hmMatrix);
        return TRUE;
}




_Result_Null_On_Failure_
_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HMATRIX
CloneMatrix(
        _In_            HMATRIX                 hmMatrix
){
        EXIT_IF_UNLIKELY_NULL(hmMatrix, NULLPTR);
        LPMATRIX lpMatrix;
        UARCHLONG ualTypeSize;
        lpMatrix = GlobalAllocAndZero(sizeof(MATRIX));
	/* cppcheck-suppress memleak */
        EXIT_IF_UNLIKELY_NULL(lpMatrix, NULLPTR);
        
        CopyMemory(lpMatrix, hmMatrix, sizeof(MATRIX));
        ualTypeSize = TypeToSize(lpMatrix->tyType);

        lpMatrix->lpData = (LPVOID)__AllocMatrix(lpMatrix->ualRows, lpMatrix->ualColumns, ualTypeSize, MATRIX_ALLOC_VECTOR);
        CopyMemory(lpMatrix->lpData, hmMatrix->lpData, ualTypeSize * lpMatrix->ualRows * lpMatrix->ualColumns);

        return (HMATRIX)lpMatrix;
}




_Result_Null_On_Failure_
_Success_(return != BOOL, _Non_Locking_)
PODNET_API
BOOL
CloneMatrixEx(
        _In_            HMATRIX                 hmMatrixSource,
        _In_Out_        HMATRIX                 hmMatrixDestination
){
        EXIT_IF_UNLIKELY_NULL(hmMatrixSource, FALSE);
        EXIT_IF_UNLIKELY_NULL(hmMatrixDestination, FALSE);
        UARCHLONG ualTypeSize;

        FreeMemory(hmMatrixDestination->lpData);
        CopyMemory(hmMatrixDestination, hmMatrixSource, sizeof(MATRIX));
        ualTypeSize = TypeToSize(hmMatrixDestination->tyType);

        hmMatrixDestination->lpData = (LPVOID)__AllocMatrix(hmMatrixDestination->ualRows, hmMatrixDestination->ualColumns, ualTypeSize, MATRIX_ALLOC_VECTOR);
        EXIT_IF_UNLIKELY_NULL(hmMatrixDestination->lpData, FALSE);

        return TRUE;        
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPVOID
MatrixValueAtCell(
        _In_            HMATRIX                 hmMatrix,
        _In_            UARCHLONG               ualRow,
        _In_            UARCHLONG               ualColumn
){
        EXIT_IF_UNLIKELY_NULL(hmMatrix, NULLPTR);
        LPVOID lpRet;
        lpRet = hmMatrix->lpData;
        lpRet = (LPVOID)((UARCHLONG)lpRet + TypeToSize(hmMatrix->tyType) * ualRow * hmMatrix->ualColumns);
        lpRet = (LPVOID)((UARCHLONG)lpRet + TypeToSize(hmMatrix->tyType) * ualColumn);
        return lpRet;
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPVOID
MatrixRow(
        _In_            HMATRIX                 hmMatrix,
        _In_            UARCHLONG               ualRow,
        _Out_Opt_       LPUARCHLONG             lpualRowElements,
        _Out_Opt_       LPUARCHLONG             lpualElementSize
){
        EXIT_IF_UNLIKELY_NULL(hmMatrix, NULLPTR);
        LPVOID lpRet;
        lpRet = hmMatrix->lpData;
        lpRet = (LPVOID)((UARCHLONG)lpRet + TypeToSize(hmMatrix->tyType) * ualRow * hmMatrix->ualColumns);
        return lpRet;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
AddMatrices(
        _In_Out_        HMATRIX                 hmAugend,
        _In_            HMATRIX                 hmAddend,
        _In_Opt_        ULONG                   ulFlags
){
        UNREFERENCED_PARAMETER(ulFlags);
        EXIT_IF_UNLIKELY_NULL(hmAugend, FALSE);
        EXIT_IF_UNLIKELY_NULL(hmAddend, FALSE);
 
        /* Are the two matrices compatible? */
        EXIT_IF_NOT_VALUE(hmAugend->tyType, hmAddend->tyType, FALSE);
        EXIT_IF_NOT_VALUE(hmAugend->ualColumns, hmAddend->ualColumns, FALSE);
        EXIT_IF_NOT_VALUE(hmAugend->ualRows, hmAddend->ualRows, FALSE);
        
        UARCHLONG ualIterator, ualObjectSize, ualNumberOfElements;
        ualObjectSize = TypeToSize(hmAugend->tyType);
        ualNumberOfElements = (hmAugend->ualRows * hmAugend->ualColumns) / ualObjectSize;

        for (
                ualIterator = 0;
                ualIterator < ualNumberOfElements;
                ualIterator++
        ){
                switch (hmAugend->tyType)
                {
                        
                        case TYPE_CHAR:
                        {

                                break;
                        }

                        case TYPE_BYTE:
                        {

                                break;
                        }

                        case TYPE_SHORT:
                        {

                                break;
                        }

                        case TYPE_USHORT:
                        {

                                break;
                        }

                        case TYPE_WORD:
                        {

                                break;
                        }

                        case TYPE_LONG:
                        {

                                break;
                        }

                        case TYPE_ULONG:
                        {

                                break;
                        }

                        case TYPE_DWORD:
                        {

                                break;
                        }

                        case TYPE_LONGLONG:
                        {

                                break;
                        }

                        case TYPE_ULONGLONG:
                        {

                                break;
                        }

                        case TYPE_QWORD:
                        {

                                break;
                        }

                        case TYPE_ULTRALONG:
                        {

                                break;
                        }

                        case TYPE_UULTRALONG:
                        {

                                break;
                        }

                        case TYPE_ARCHLONG:
                        {

                                break;
                        }

                        case TYPE_UARCHLONG:
                        {

                                break;
                        }

                        case TYPE_LPVOID:
                        {

                                break;
                        }

                        case TYPE_DLPVOID:
                        {

                                break;
                        }

                        case TYPE_HANDLE:
                        {

                                break;       
                        }

                }
        }
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
AddScalarToMatrix(
        _In_Out_        HMATRIX                 hmAugend,
        _In_            LPVOID                  lpScalar,
        _In_Opt_        ULONG                   ulFlags
);

















/*


                        case TYPE_CHAR:
                        {

                                break;
                        }

                        case TYPE_BYTE:
                        {

                                break;
                        }

                        case TYPE_SHORT:
                        {

                                break;
                        }

                        case TYPE_USHORT:
                        {

                                break;
                        }

                        case TYPE_WORD:
                        {

                                break;
                        }

                        case TYPE_LONG:
                        {

                                break;
                        }

                        case TYPE_ULONG:
                        {

                                break;
                        }

                        case TYPE_DWORD:
                        {

                                break;
                        }

                        case TYPE_LONGLONG:
                        {

                                break;
                        }

                        case TYPE_ULONGLONG:
                        {

                                break;
                        }

                        case TYPE_QWORD:
                        {

                                break;
                        }

                        case TYPE_ULTRALONG:
                        {

                                break;
                        }

                        case TYPE_UULTRALONG:
                        {

                                break;
                        }

                        case TYPE_ARCHLONG:
                        {

                                break;
                        }

                        case TYPE_UARCHLONG:
                        {

                                break;
                        }

                        case TYPE_LPVOID:
                        {

                                break;
                        }

                        case TYPE_DLPVOID:
                        {

                                break;
                        }

                        case TYPE_HANDLE:
                        {

                                break;       
                        }


*/