/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CRandom.h"
#include "../CHandle/CHandleTable.h"


#define RANDOM_BYTE_MULT                        0x6A23
#define RANDOM_SHORT_MULT                       0x41C6F2B8U
#define RANDOM_LONG_MULT                        0x5851F42D4C957FULL
#define RANDOM_LONG_ALT_MULT                    0x8EE7AC7B6035330FULL




typedef struct __RANDOM8
{
        USHORT                  usState;
        USHORT                  usIncrease;
} RANDOM8, *LPRANDOM8; 




typedef struct __RANDOM16
{
        ULONG                   ulState;
        ULONG                   ulIncrease;
} RANDOM16, *LPRANDOM16;




typedef struct __RANDOM32
{
        ULONGLONG               ullState;
        ULONGLONG               ullIncrease;
} RANDOM32, *LPRANDOM32;




typedef struct __RANDOM64
{
        UULTRALONG              uultState;
        UULTRALONG              uultIncrease;
} RANDOM64, *LPRANDOM64;




typedef struct __RANDOM128
{
        UULTRALONG              uultState;
        UULTRALONG              uultIncrease;
} RANDOM128, *LPRANDOM128;




typedef struct __RANDOM_ARCH
{
        UULTRALONG               uultState;
        UULTRALONG               uultIncrease;
} RANDOM_ARCH, *LPRANDOM_ARCH;




typedef struct __RANDOM
{
        INHERITS_FROM_HANDLE();
        HANDLE          hLock;
        RANDOM_SIZE     rsType;
        LPVOID          lpChild;
        LPSTR           lpszName;
	BOOL 		bStringAlloc;
} RANDOM, *LPRANDOM;




GLOBAL_VARIABLE DONT_OPTIMIZE_OUT HANDLE hHandleTable = NULLPTR;
GLOBAL_VARIABLE DONT_OPTIMIZE_OUT LPRANDOM lpGlobalRandom = NULLPTR;




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__CreateHandleTable(
        VOID 
){
        if (NULLPTR == hHandleTable)
        {
                hHandleTable = CreateHandleTable(0x0A, NULLPTR, HANDLE_TABLE_HIGH_PERFORMANCE);
                EXIT_IF_UNLIKELY_NULL(hHandleTable, FALSE);
        }

        return TRUE;
}




_Call_Back_
_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__RandomNumberGeneratorTableLookup(
        _In_            HDERIVATIVE             hLookingFor,
        _In_            HDERIVATIVE             hComparand
){
        EXIT_IF_NULL(hComparand, FALSE);

        LPSTR lpszName;
        LPRANDOM lpRandom;
        lpRandom = (LPRANDOM)hComparand;

        lpszName = (LPSTR)hLookingFor;
        return (0 == StringCompare(lpszName, lpRandom->lpszName)) ? TRUE : FALSE;
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__NextRandom(
        _In_            LPRANDOM                lpRandom,
        _Out_           LPVOID                  lpVal,
        _In_            UARCHLONG               ualOutBufferSize
){
        EXIT_IF_UNLIKELY_NULL(lpRandom, FALSE);
        EXIT_IF_UNLIKELY_NULL(lpVal, FALSE);
        EXIT_IF_UNLIKELY_NULL(ualOutBufferSize, FALSE);

        switch (lpRandom->rsType)
        {

                case RANDOM_SIZE_BYTE:
                {
                        if (sizeof(BYTE) > ualOutBufferSize)
                        { return FALSE; }

                        LPRANDOM8 lpRand8;
                        BYTE byOut;
                        USHORT usOldState;

                        lpRand8 = (LPRANDOM8)lpRandom->lpChild;
                        usOldState = lpRand8->usState;
                        lpRand8->usState = usOldState * RANDOM_BYTE_MULT + (lpRand8->usIncrease | 0x0F);
                        byOut = (BYTE)(((usOldState >> 5U) ^ usOldState) >> 7U);
                        *(LPBYTE)lpVal = (byOut >> (usOldState >> 11U)) | (byOut << ((-(usOldState >> 11U)) & 0x07U)); 
                        break;
                }

                case RANDOM_SIZE_SHORT:
                {
                        if (sizeof(SHORT) > ualOutBufferSize)
                        { return FALSE; }

                        LPRANDOM16 lpRand16;
                        USHORT usOut;
                        ULONG ulOldState;

                        lpRand16 = (LPRANDOM16)lpRandom->lpChild;
                        ulOldState = lpRand16->ulState;
                        lpRand16->ulState = ulOldState * RANDOM_SHORT_MULT + (lpRand16->ulIncrease | 0xF0);
                        usOut = (USHORT)(((ulOldState >> 11U) ^ ulOldState) >> 15U);
                        *(LPUSHORT)lpVal = (usOut >> (ulOldState >> 29U)) | (usOut << ((-(ulOldState >> 29U)) & 0x11U));
                        break;
                }

                case RANDOM_SIZE_LONG:
                {
                        if (sizeof(LONG) > ualOutBufferSize)
                        { return FALSE; }

                        LPRANDOM32 lpRand32;
                        ULONG ulOut;
                        ULONGLONG ullOldState;

                        lpRand32 = (LPRANDOM32)lpRandom->lpChild;
                        ullOldState = lpRand32->ullState;
                        lpRand32->ullState = ullOldState * RANDOM_LONG_MULT + (lpRand32->ullIncrease | 0xF0F);
                        ulOut = (ULONG)(((ullOldState >> 18U) ^ ullOldState) >> 27U);
                        *(LPULONG)lpVal = (ulOut >> (ullOldState >> 59U)) | (ulOut << ((-(ullOldState >> 59U)) & 0x25U));
                        break;
                }

                case RANDOM_SIZE_LONGLONG:
                {
                        if (sizeof(LONGLONG) > ualOutBufferSize)
                        { return FALSE; }

                        LPRANDOM64 lpRand64;
                        ULONGLONG ullOut;
                        UULTRALONG uultOldState;

                        lpRand64 = (LPRANDOM64)lpRandom->lpChild;
                        uultOldState = lpRand64->uultState;
                        lpRand64->uultState = uultOldState * RANDOM_LONG_MULT + (lpRand64->uultIncrease | 0xF0F0F);

                        #ifdef __LP64__
                        ullOut = (ULONGLONG)(((uultOldState >> 35U) ^ uultOldState) >> 53U);
                        *(LPULONGLONG)lpVal = (ullOut >> (uultOldState >> 117U)) | (ullOut << ((-(uultOldState >> 117U)) & 0x4BU));
                        #else
                        ullOut = (ULONGLONG)(((uultOldState >> 18U) ^ uultOldState) >> 27U);
                        *(LPULONGLONG)lpVal = (ullOut >> (uultOldState >> 59U)) | (ullOut << ((-(uultOldState >> 59U)) & 0x25U));
                        #endif

                        break;
                }

                case RANDOM_SIZE_ULTRALONG:
                {
                        if (sizeof(ULTRALONG) > ualOutBufferSize)
                        { return FALSE; }

                        LPRANDOM128 lpRand128;
                        UULTRALONG uultOut, uultOldState;
                        
                        #ifdef __LP64__
                        ULTRALONG_UNION ultuUnion;
                        #endif

                        lpRand128 = (LPRANDOM128)lpRandom->lpChild;
                        uultOldState = lpRand128->uultIncrease;

                        /* Eh? */
                        #ifdef __LP64__
                        ultuUnion.uultX = uultOldState;
                        ultuUnion.ullX[0] *= RANDOM_LONG_MULT + ((ULONGLONG)(lpRand128->uultIncrease | 0xF0F0FF));
                        ultuUnion.ullX[1] *= RANDOM_LONG_ALT_MULT + ((ULONGLONG)(lpRand128->uultIncrease | 0xF0F00));
                        lpRand128->uultState = ultuUnion.uultX;
                        uultOut = (UULTRALONG)(((uultOldState >> 71U) ^ uultOldState) >> 105U);
                        uultOut = (uultOut >> (uultOldState >> 117U)) | (uultOut << ((-(uultOldState >> 117U)) & 0x4BU));
                        *(LPUULTRALONG)lpVal = (uultOut >> (uultOldState >> 117U)) | (uultOut << ((-(uultOldState >> 117U)) & 0x4BU)); 
                        #else
                        uultOut = (UULTRALONG)(((uultOldState >> 35U) ^ uultOldState) >> 53U);
                        *(LPUULTRALONG)lpVal = (uultOut >> (uultOldState >> 59U)) | (uultOut << ((-(uultOldState >> 59U)) & 0x25U)); 
                        #endif

                        break;
                }

                case RANDOM_SIZE_ARCHLONG:
                {
                        if (sizeof(ARCHLONG) > ualOutBufferSize)
                        { return FALSE; }

                        LPRANDOM_ARCH lpRandArch;
                        UARCHLONG ualOut;
                        UULTRALONG uultOldState;

                        lpRandArch = (LPRANDOM_ARCH)lpRandom->lpChild;
                        uultOldState = lpRandArch->uultState;
                        lpRandArch->uultState = uultOldState * RANDOM_LONG_MULT + (lpRandArch->uultIncrease | 0xF0F0F);

                        #ifdef __LP64__
                        ualOut = (ULONGLONG)(((uultOldState >> 35U) ^ uultOldState) >> 53U);
                        *(LPUARCHLONG)lpVal = (ualOut >> (uultOldState >> 117U)) | (ualOut << ((-(uultOldState >> 117U)) & 0x4BU));
                        #else
                        ualOut = (ULONG)(((uultOldState >> 18U) ^ uultOldState) >> 27U);
                        *(LPUARCHLONG)lpVal = (ualOut >> (uultOldState >> 59U)) | (ualOut << ((-(uultOldState >> 59U)) & 0x25U));
                        #endif

                        break;
                }

        }

        return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__DestroyRandomNumberGenerator(
        _In_            HDERIVATIVE             hDerivative,
        _In_Opt_        ULONG                   ulFlags
){
        UNREFERENCED_PARAMETER(ulFlags);
        EXIT_IF_UNLIKELY_NULL(hDerivative, FALSE);

        LPRANDOM lpRandom;
        lpRandom = (LPRANDOM)hDerivative;

        WaitForSingleObject(lpRandom->hLock, INFINITE_WAIT_TIME);
        DeregisterHandle(hHandleTable, lpRandom->hThis);
        FreeMemory(lpRandom->lpChild);
        DestroyHandle(lpRandom->hLock);
	if (TRUE == lpRandom->bStringAlloc)
	{ FreeMemory(lpRandom->lpszName); }
        FreeMemory(lpRandom);

        return TRUE;
}




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNET_API
HANDLE 
CreateRandomNumberGenerator(
        _In_            RANDOM_SIZE             rsBaseSize,
        _In_            LPVOID                  lpSeedValue,
        _In_Opt_        LPCSTR                  lpcszName    
){
        EXIT_IF_UNLIKELY_NULL(rsBaseSize, NULLPTR);
        EXIT_IF_UNLIKELY_NULL(lpSeedValue, NULLPTR);

        if (UNLIKELY(NULLPTR == hHandleTable))
        { EXIT_IF_UNLIKELY_NOT_VALUE(__CreateHandleTable(), TRUE, NULLPTR); }

        HANDLE hRng;
        LPRANDOM lpRandom;
        lpRandom = LocalAllocAndZero(sizeof(RANDOM));
	/* cppcheck-suppress memleak */
        EXIT_IF_UNLIKELY_NULL(lpRandom, NULLPTR);

        lpRandom->hLock = CreateSpinLock();
	if (NULL_OBJECT == lpRandom->hLock)
	{
		FreeMemory(lpRandom);
		return NULL_OBJECT;
	}

        lpRandom->rsType = rsBaseSize;
        lpRandom->lpszName = (LPSTR)lpcszName;


        switch (rsBaseSize)
        {

                case RANDOM_SIZE_BYTE:
                {
                        lpRandom->lpChild = LocalAllocAndZero(sizeof(RANDOM8));
			if (NULLPTR == lpRandom->lpChild)
			{
				DestroyObject(lpRandom->hLock);
				FreeMemory(lpRandom);
				return NULL_OBJECT;
			}

                        CopyMemory(&(((LPRANDOM8)lpRandom->lpChild)->usState), lpSeedValue, sizeof(SHORT)); 
                        CopyMemory(&(((LPRANDOM8)lpRandom->lpChild)->usIncrease), lpSeedValue, sizeof(SHORT)); 
                        ((LPRANDOM8)lpRandom->lpChild)->usIncrease = (USHORT)((FLOAT)(((LPRANDOM8)lpRandom->lpChild)->usIncrease) / GOLDEN_RATIO);
                        break;
                }

                case RANDOM_SIZE_SHORT:
                {
                        lpRandom->lpChild = LocalAllocAndZero(sizeof(RANDOM16));
			if (NULLPTR == lpRandom->lpChild)
			{
				DestroyObject(lpRandom->hLock);
				FreeMemory(lpRandom);
				return NULL_OBJECT;
			}

                        CopyMemory(&(((LPRANDOM16)lpRandom->lpChild)->ulState), lpSeedValue, sizeof(LONG)); 
                        CopyMemory(&(((LPRANDOM16)lpRandom->lpChild)->ulIncrease), lpSeedValue, sizeof(LONG)); 
                        ((LPRANDOM16)lpRandom->lpChild)->ulIncrease = (ULONG)((FLOAT)(((LPRANDOM16)lpRandom->lpChild)->ulIncrease) / GOLDEN_RATIO);
                        break;
                }

                case RANDOM_SIZE_LONG:
                {
                        lpRandom->lpChild = LocalAllocAndZero(sizeof(RANDOM32));
			if (NULLPTR == lpRandom->lpChild)
			{
				DestroyObject(lpRandom->hLock);
				FreeMemory(lpRandom);
				return NULL_OBJECT;
			}

                        CopyMemory(&(((LPRANDOM32)lpRandom->lpChild)->ullState), lpSeedValue, sizeof(LONGLONG)); 
                        CopyMemory(&(((LPRANDOM32)lpRandom->lpChild)->ullIncrease), lpSeedValue, sizeof(LONGLONG)); 
                        ((LPRANDOM32)lpRandom->lpChild)->ullIncrease = (ULONGLONG)((FLOAT)(((LPRANDOM32)lpRandom->lpChild)->ullIncrease) / GOLDEN_RATIO);
                        break;
                }

                case RANDOM_SIZE_LONGLONG:
                {
                        lpRandom->lpChild = LocalAllocAndZero(sizeof(RANDOM64));
			if (NULLPTR == lpRandom->lpChild)
			{
				DestroyObject(lpRandom->hLock);
				FreeMemory(lpRandom);
				return NULL_OBJECT;
			}

                        CopyMemory(&(((LPRANDOM64)lpRandom->lpChild)->uultState), lpSeedValue, sizeof(ULTRALONG)); 
                        CopyMemory(&(((LPRANDOM64)lpRandom->lpChild)->uultIncrease), lpSeedValue, sizeof(ULTRALONG)); 
                        ((LPRANDOM64)lpRandom->lpChild)->uultIncrease = (UULTRALONG)((DOUBLE)(((LPRANDOM64)lpRandom->lpChild)->uultIncrease) / GOLDEN_RATIO_D);
                        break;
                }

                case RANDOM_SIZE_ULTRALONG:
                {
                        lpRandom->lpChild = LocalAllocAndZero(sizeof(RANDOM128));
			if (NULLPTR == lpRandom->lpChild)
			{
				DestroyObject(lpRandom->hLock);
				FreeMemory(lpRandom);
				return NULL_OBJECT;
			}

                        CopyMemory(&(((LPRANDOM128)lpRandom->lpChild)->uultState), lpSeedValue, sizeof(ULTRALONG)); 
                        CopyMemory(&(((LPRANDOM128)lpRandom->lpChild)->uultIncrease), lpSeedValue, sizeof(ULTRALONG)); 
                        ((LPRANDOM128)lpRandom->lpChild)->uultIncrease = (UULTRALONG)((DOUBLE)(((LPRANDOM128)lpRandom->lpChild)->uultIncrease) / GOLDEN_RATIO_D);
                        break;
                }

                case RANDOM_SIZE_ARCHLONG:
                {
                        lpRandom->lpChild = LocalAllocAndZero(sizeof(RANDOM_ARCH));
			if (NULLPTR == lpRandom->lpChild)
			{
				DestroyObject(lpRandom->hLock);
				FreeMemory(lpRandom);
				return NULL_OBJECT;
			}

                        CopyMemory(&(((LPRANDOM_ARCH)lpRandom->lpChild)->uultState), lpSeedValue, sizeof(ULTRALONG)); 
                        CopyMemory(&(((LPRANDOM_ARCH)lpRandom->lpChild)->uultIncrease), lpSeedValue, sizeof(ULTRALONG)); 
                        ((LPRANDOM_ARCH)lpRandom->lpChild)->uultIncrease = (UULTRALONG)((DOUBLE)(((LPRANDOM_ARCH)lpRandom->lpChild)->uultIncrease) / GOLDEN_RATIO_D);
                        break;
                }

        }

        hRng = CreateHandleWithSingleInheritor(
                        lpRandom,
                        &(lpRandom->hThis),
                        HANDLE_TYPE_RNG,
                        __DestroyRandomNumberGenerator,
                        NULL,
                        NULLPTR,
                        NULL,
                        NULLPTR,
                        NULL,
                        &(lpRandom->ullId),
                        NULL 
        );

	if (NULL_OBJECT == hRng)
	{
		FreeMemory(lpRandom->lpChild);
		DestroyObject(lpRandom->hLock);
		FreeMemory(lpRandom);
		return NULL_OBJECT;
	}

        if (NULLPTR != lpcszName)
        { RegisterHandle(hHandleTable, hRng); }

        return hRng;
}




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNET_API
HANDLE 
CreateRandomNumberGeneratorByAtom(
	_In_ 		RANDOM_SIZE 		rsBaseSize,
	_In_ 		LPVOID RESTRICT 	lpSeedValue,
	_In_Opt_	HANDLE 			hAtomTable,
	_In_ 		ATOM 			atValue
){
	EXIT_IF_UNLIKELY_NULL(lpSeedValue, NULL_OBJECT);

	HANDLE hRng;
	UARCHLONG ualLength;	
	CHAR szBuffer[4096];
	LPRANDOM lpRand;
	ZeroMemory(szBuffer, sizeof(szBuffer));

	if (NULL_OBJECT == hAtomTable)
	{ GetGlobalAtomValue(atValue, szBuffer); }
	else
	{ GetAtomValue(hAtomTable, atValue, szBuffer); }

	ualLength = StringLength(szBuffer);

	hRng = CreateRandomNumberGenerator(rsBaseSize, lpSeedValue, NULL);

	lpRand = (LPRANDOM)GetHandleDerivative(hRng);
	if (NULLPTR == lpRand)
	{
		DestroyObject(hRng);
		return NULL_OBJECT;
	}

	lpRand->lpszName = GlobalAllocAndZero((sizeof(CHAR) * ualLength) + 0x02U);
	if (NULLPTR == lpRand->lpszName)
	{
		DestroyObject(hRng);
		return NULL_OBJECT;
	}

	lpRand->bStringAlloc = TRUE;
	CopyMemory(lpRand->lpszName, szBuffer, ualLength);
	
	return hRng;
}




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNET_API
HANDLE
GetRandomNumberGeneratorByName(
        _In_            LPCSTR                  lpcszName
){
        EXIT_IF_UNLIKELY_NULL(lpcszName, NULLPTR);
	HANDLE hRet;

	hRet = GrabHandleByDerivative(
		hHandleTable, 
		(LPVOID)lpcszName, 
		NULL, 
		__RandomNumberGeneratorTableLookup
	);

	if (NULL_OBJECT != hRet)
	{ IncrementObjectReferenceCount(hRet); }

	return hRet;
}




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNET_API
HANDLE
GetRandomNumberGeneratorByAtom(
        _In_            HANDLE                  hAtomTable,
        _In_            ATOM                    atValue
){
        EXIT_IF_UNLIKELY_NULL(atValue, NULLPTR);

	HANDLE hRet;
	CSTRING csBuffer[1024];
        ZeroMemory(csBuffer, sizeof(CHAR) * 1024);
        
        if (NULLPTR == hAtomTable)
        { GetGlobalAtomValue(atValue, (LPSTR)csBuffer); }
        else
        { GetAtomValue(hAtomTable, atValue, (LPSTR)csBuffer); }

        hRet = GrabHandleByDerivative(hHandleTable, (LPVOID)csBuffer, NULL, __RandomNumberGeneratorTableLookup);

	if (NULL_OBJECT != hRet)
	{ IncrementObjectReferenceCount(hRet); }

	return hRet;
}




_Success_(return != FALSE, _Acquires_Shared_Lock_(_Has_Lock_Kind_(_Lock_Kind_Spin_Lock_)))
PODNET_API
BOOL 
GetSingleRawRandom(
        _In_            HANDLE                  hRng,
        _Out_           LPVOID                  lpVal,
        _In_            UARCHLONG               ualOutBufferSize
){
        EXIT_IF_UNLIKELY_NULL(hRng, FALSE);
        EXIT_IF_UNLIKELY_NULL(lpVal, FALSE);
        EXIT_IF_UNLIKELY_NULL(ualOutBufferSize, FALSE);
        EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hRng), HANDLE_TYPE_RNG, FALSE);

	IncrementObjectReferenceCount(hRng);

        LPRANDOM lpRandom;
        BOOL bRet;
        lpRandom = (LPRANDOM)GetHandleDerivative(hRng);
        EXIT_IF_UNLIKELY_NULL(lpRandom, FALSE);

        WaitForSingleObject(lpRandom->hLock, INFINITE_WAIT_TIME);
        bRet = __NextRandom(lpRandom, lpVal, ualOutBufferSize);
        ReleaseSingleObject(lpRandom->hLock);

	DecrementObjectReferenceCount(hRng);

        return bRet;
}




_Success_(return != FALSE, _Acquires_Shared_Lock_(_Has_Lock_Kind_(_Lock_Kind_Spin_Lock_)))
PODNET_API
BOOL
GetMultipleRandoms(
        _In_            HANDLE                  hRng,
        _In_            UARCHLONG               ualNumberOfRandoms,
        _Out_           LPVOID                  lpOut,
        _In_            UARCHLONG               ualOutBufferSize,
        _Out_Opt_       LPUARCHLONG             lpualRandomsWritten
){
        EXIT_IF_UNLIKELY_NULL(hRng, FALSE);
        EXIT_IF_UNLIKELY_NULL(ualNumberOfRandoms, FALSE);
        EXIT_IF_UNLIKELY_NULL(lpOut, FALSE);
        EXIT_IF_UNLIKELY_NULL(ualOutBufferSize, FALSE);
        EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hRng), HANDLE_TYPE_RNG, FALSE);

	IncrementObjectReferenceCount(hRng);

        LPRANDOM lpRandom;
        UARCHLONG ualSize, ualIndex;
        BOOL bRet;
        lpRandom = (LPRANDOM)GetHandleDerivative(hRng);
        ualSize = NULL;
        bRet = TRUE;

        switch (lpRandom->rsType)
        {

                case RANDOM_SIZE_BYTE:
                {
                        ualSize = sizeof(BYTE);
                        break;
                }

                case RANDOM_SIZE_SHORT:
                {
                        ualSize = sizeof(SHORT);
                        break;
                }

                case RANDOM_SIZE_LONG:
                {
                        ualSize = sizeof(LONG);
                        break;
                }

                case RANDOM_SIZE_LONGLONG:
                {
                        ualSize = sizeof(LONGLONG);
                        break;
                }

                case RANDOM_SIZE_ULTRALONG:
                {
                        ualSize = sizeof(ULTRALONG);
                        break;
                }

                case RANDOM_SIZE_ARCHLONG:
                {
                        ualSize = sizeof(ARCHLONG);
                        break;
                }

        }        

        EXIT_IF_UNLIKELY_NULL(ualSize, FALSE);
        EXIT_IF_CONDITION(ualOutBufferSize < (ualSize * ualNumberOfRandoms), FALSE);

        WaitForSingleObject(lpRandom->hLock, INFINITE_WAIT_TIME);

        for (
                ualIndex = 0;
                ualIndex < ualNumberOfRandoms;
                ualIndex++
        ){ bRet &= __NextRandom(lpRandom, (LPVOID)(((UARCHLONG)lpOut) + ualSize * ualIndex), ualSize); }

        if (NULLPTR != lpualRandomsWritten)
        { *lpualRandomsWritten = ualIndex + 1; }

        ReleaseSingleObject(lpRandom->hLock);

	DecrementObjectReferenceCount(hRng);

        return bRet;
}




_Success_(return != FALSE, _Acquires_Shared_Lock_(_Has_Lock_Kind_(_Lock_Kind_Spin_Lock_)))
PODNET_API
BOOL
GetSingleRandomInRange(
        _In_            HANDLE                  hRng,
        _In_            UULTRALONG              uultLowerRange,
        _In_            UULTRALONG              uultUpperRange,
        _Out_           LPVOID                  lpOut,
        _In_            UARCHLONG               ualOutBufferSize
){
        EXIT_IF_UNLIKELY_NULL(hRng, FALSE);
        EXIT_IF_UNLIKELY_NULL(lpOut, FALSE);
        EXIT_IF_UNLIKELY_NULL(ualOutBufferSize, FALSE);
        EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hRng), HANDLE_TYPE_RNG, FALSE);

	IncrementObjectReferenceCount(hRng);

        LPRANDOM lpRandom;
        BOOL bRet;
        lpRandom = (LPRANDOM)GetHandleDerivative(hRng);
        EXIT_IF_UNLIKELY_NULL(lpRandom, FALSE);
        bRet = FALSE;

        WaitForSingleObject(lpRandom->hLock, INFINITE_WAIT_TIME);
 
        switch (lpRandom->rsType)
        {

                case RANDOM_SIZE_BYTE:
                {
                        BYTE byOut;
                        bRet = __NextRandom(lpRandom, &byOut, sizeof(BYTE));
                        if (TRUE == bRet)
                        { *(LPBYTE)lpOut = (byOut % ((BYTE)uultUpperRange)) + (USHORT)uultLowerRange; }
                        break;
                }

                case RANDOM_SIZE_SHORT:
                {
                        USHORT usOut;
                        bRet = __NextRandom(lpRandom, &usOut, sizeof(SHORT));
                        if (TRUE == bRet)
                        { *(LPUSHORT)lpOut = (usOut % ((USHORT)uultUpperRange) + (USHORT)uultLowerRange); }
                        break;
                }

                case RANDOM_SIZE_LONG:
                {
                        ULONG ulOut;
                        bRet = __NextRandom(lpRandom, &ulOut, sizeof(LONG));
                        if (TRUE == bRet)
                        { *(LPULONG)lpOut = (ulOut % ((ULONG)uultUpperRange) + (ULONG)uultLowerRange); }
                        break;
                }

                case RANDOM_SIZE_LONGLONG:
                {
                        ULONGLONG ullOut;
                        bRet = __NextRandom(lpRandom, &ullOut, sizeof(LONGLONG));
                        if (TRUE == bRet)
                        { *(LPULONGLONG)lpOut = (ullOut % ((ULONGLONG)uultUpperRange) + (ULONGLONG)uultLowerRange); }
                        break;
                }

                case RANDOM_SIZE_ULTRALONG:
                {
                        UULTRALONG uultOut;
                        bRet = __NextRandom(lpRandom, &uultOut, sizeof(ULTRALONG));
                        if (TRUE == bRet)
                        { *(LPUULTRALONG)lpOut = (uultOut % uultUpperRange) + uultLowerRange; }
                        break;
                }

                case RANDOM_SIZE_ARCHLONG:
                {
                        UARCHLONG ualOut;
                        bRet = __NextRandom(lpRandom, &ualOut, sizeof(UARCHLONG));
                        if (TRUE == bRet)
                        { *(LPUARCHLONG)lpOut = (ualOut % ((UARCHLONG)uultUpperRange) + (UARCHLONG)uultLowerRange); }
                        break;
                }

        }        
 
        ReleaseSingleObject(lpRandom->hLock);

	DecrementObjectReferenceCount(hRng);

        return bRet;
}




_Success_(return != FALSE, _Acquires_Shared_Lock_(_Has_Lock_Kind_(_Lock_Kind_Spin_Lock_)))
PODNET_API
BOOL
GetMultipleRandomsInRange(
        _In_            HANDLE                  hRng,
        _In_            UULTRALONG              uultLowerRange,
        _In_            UULTRALONG              uultUpperRange,
        _Out_           LPVOID                  lpOut,
        _In_            UARCHLONG               ualOutBufferSize,
        _Out_Opt_       LPUARCHLONG             lpualRandomsWritten
);




_Success_(return != FALSE, _Acquires_Shared_Lock_(_Has_Lock_Kind_(_Lock_Kind_Spin_Lock_)))
PODNET_API
BOOL
ReSeedRandomNumberGenerator(
        _In_            HANDLE                  hRng,
        _In_            LPVOID                  lpSeedValue
){
        EXIT_IF_UNLIKELY_NULL(hRng, FALSE);
        EXIT_IF_UNLIKELY_NULL(lpSeedValue, FALSE);
        EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hRng), HANDLE_TYPE_RNG, FALSE);

	IncrementObjectReferenceCount(hRng);

        LPRANDOM lpRandom;
        lpRandom = (LPRANDOM)GetHandleDerivative(hRng);
        EXIT_IF_UNLIKELY_NULL(lpRandom, FALSE);


        switch (lpRandom->rsType)
        {

                case RANDOM_SIZE_BYTE:
                {
                        lpRandom->lpChild = LocalAllocAndZero(sizeof(RANDOM8));
                        EXIT_IF_UNLIKELY_NULL(lpRandom->lpChild, FALSE);
                        CopyMemory(&(((LPRANDOM8)lpRandom->lpChild)->usState), lpSeedValue, sizeof(SHORT)); 
                        CopyMemory(&(((LPRANDOM8)lpRandom->lpChild)->usIncrease), lpSeedValue, sizeof(SHORT)); 
                        ((LPRANDOM8)lpRandom->lpChild)->usIncrease = (USHORT)((FLOAT)(((LPRANDOM8)lpRandom->lpChild)->usIncrease) / GOLDEN_RATIO);
                        break;
                }

                case RANDOM_SIZE_SHORT:
                {
                        lpRandom->lpChild = LocalAllocAndZero(sizeof(RANDOM16));
                        EXIT_IF_UNLIKELY_NULL(lpRandom->lpChild, FALSE);
                        CopyMemory(&(((LPRANDOM16)lpRandom->lpChild)->ulState), lpSeedValue, sizeof(LONG)); 
                        CopyMemory(&(((LPRANDOM16)lpRandom->lpChild)->ulIncrease), lpSeedValue, sizeof(LONG)); 
                        ((LPRANDOM16)lpRandom->lpChild)->ulIncrease = (ULONG)((FLOAT)(((LPRANDOM16)lpRandom->lpChild)->ulIncrease) / GOLDEN_RATIO);
                        break;
                }

                case RANDOM_SIZE_LONG:
                {
                        lpRandom->lpChild = LocalAllocAndZero(sizeof(RANDOM32));
                        EXIT_IF_UNLIKELY_NULL(lpRandom->lpChild, FALSE);
                        CopyMemory(&(((LPRANDOM32)lpRandom->lpChild)->ullState), lpSeedValue, sizeof(LONGLONG)); 
                        CopyMemory(&(((LPRANDOM32)lpRandom->lpChild)->ullIncrease), lpSeedValue, sizeof(LONGLONG)); 
                        ((LPRANDOM32)lpRandom->lpChild)->ullIncrease = (ULONGLONG)((FLOAT)(((LPRANDOM32)lpRandom->lpChild)->ullIncrease) / GOLDEN_RATIO);
                        break;
                }

                case RANDOM_SIZE_LONGLONG:
                {
                        lpRandom->lpChild = LocalAllocAndZero(sizeof(RANDOM64));
                        EXIT_IF_UNLIKELY_NULL(lpRandom->lpChild, FALSE);
                        CopyMemory(&(((LPRANDOM64)lpRandom->lpChild)->uultState), lpSeedValue, sizeof(ULTRALONG)); 
                        CopyMemory(&(((LPRANDOM64)lpRandom->lpChild)->uultIncrease), lpSeedValue, sizeof(ULTRALONG)); 
                        ((LPRANDOM64)lpRandom->lpChild)->uultIncrease = (UULTRALONG)((DOUBLE)(((LPRANDOM64)lpRandom->lpChild)->uultIncrease) / GOLDEN_RATIO_D);
                        break;
                }

                case RANDOM_SIZE_ULTRALONG:
                {
                        lpRandom->lpChild = LocalAllocAndZero(sizeof(RANDOM128));
                        EXIT_IF_UNLIKELY_NULL(lpRandom->lpChild, FALSE);
                        CopyMemory(&(((LPRANDOM128)lpRandom->lpChild)->uultState), lpSeedValue, sizeof(ULTRALONG)); 
                        CopyMemory(&(((LPRANDOM128)lpRandom->lpChild)->uultIncrease), lpSeedValue, sizeof(ULTRALONG)); 
                        ((LPRANDOM128)lpRandom->lpChild)->uultIncrease = (UULTRALONG)((DOUBLE)(((LPRANDOM128)lpRandom->lpChild)->uultIncrease) / GOLDEN_RATIO_D);
                        break;
                }

                case RANDOM_SIZE_ARCHLONG:
                {
                        lpRandom->lpChild = LocalAllocAndZero(sizeof(RANDOM_ARCH));
                        EXIT_IF_UNLIKELY_NULL(lpRandom->lpChild, FALSE);
                        CopyMemory(&(((LPRANDOM_ARCH)lpRandom->lpChild)->uultState), lpSeedValue, sizeof(ULTRALONG)); 
                        CopyMemory(&(((LPRANDOM_ARCH)lpRandom->lpChild)->uultIncrease), lpSeedValue, sizeof(ULTRALONG)); 
                        ((LPRANDOM_ARCH)lpRandom->lpChild)->uultIncrease = (UULTRALONG)((DOUBLE)(((LPRANDOM_ARCH)lpRandom->lpChild)->uultIncrease) / GOLDEN_RATIO_D);
                        break;
                }

        }

	DecrementObjectReferenceCount(hRng);

        return TRUE;
}




_Success_(return != FALSE, _Acquires_Shared_Lock_(_Has_Lock_Kind_(_Lock_Kind_Spin_Lock_)))
PODNET_API
BOOL 
GetSingleGlobalRawRandom(
        _Out_           LPULONGLONG             lpullOut,
        _Reserved_      ULONG                   ulFlags
){
        UNREFERENCED_PARAMETER(ulFlags);
        EXIT_IF_UNLIKELY_NULL(lpullOut, FALSE);
        LPRANDOM64 lpRandom64;
        BOOL bRet;

        if (UNLIKELY(NULLPTR == lpGlobalRandom))
        {
                lpGlobalRandom = (LPRANDOM)GlobalAllocAndZero(sizeof(RANDOM));
                EXIT_IF_UNLIKELY_NULL(lpGlobalRandom, FALSE);

                lpGlobalRandom->hLock = CreateSpinLock();
                EXIT_IF_UNLIKELY_NULL(lpGlobalRandom->hLock, FALSE);
                lpGlobalRandom->rsType = RANDOM_SIZE_LONGLONG;
                lpGlobalRandom->lpChild = GlobalAllocAndZero(sizeof(RANDOM64));
                EXIT_IF_UNLIKELY_NULL(lpGlobalRandom->lpChild, FALSE);
                lpRandom64 = (LPRANDOM64)lpGlobalRandom->lpChild;
                lpGlobalRandom->hThis = NULLPTR;
                lpGlobalRandom->ullId = NULL;

                lpRandom64->uultState = (UULTRALONG)time(NULLPTR);
                lpRandom64->uultState *= (UULTRALONG)(UARCHLONG)lpRandom64;
                lpRandom64->uultIncrease = (UULTRALONG)((DOUBLE)lpRandom64->uultState / GOLDEN_RATIO_D);
        }

        WaitForSingleObject(lpGlobalRandom->hLock, INFINITE_WAIT_TIME);
        bRet = __NextRandom(lpGlobalRandom, lpullOut, sizeof(ULONGLONG));
        ReleaseSingleObject(lpGlobalRandom->hLock);

        return bRet;
}




_Success_(return != FALSE, _Acquires_Shared_Lock_(_Has_Lock_Kind_(_Lock_Kind_Spin_Lock_)))
PODNET_API
BOOL
GetMultipleGlobalRawRandoms(
        _In_            UARCHLONG               ualNumberOfRandoms,
        _Out_           LPULONGLONG             lpullOutBuffer,
        _In_            UARCHLONG               ualSizeOfOutBuffer,
        _Out_Opt_       LPUARCHLONG             lpualNumberOfRandomsWritten,
        _Reserved_      ULONG                   ulFlags
){
        UNREFERENCED_PARAMETER(ulFlags);
        EXIT_IF_UNLIKELY_NULL(ualNumberOfRandoms, FALSE);
        EXIT_IF_UNLIKELY_NULL(lpullOutBuffer, FALSE);
        /* EXIT_IF_CONDITION(ualNumberOfRandoms * sizeof(ULONGLONG) > ualSizeOfOutBuffer, FALSE); */

        LPRANDOM64 lpRandom64;
        UARCHLONG ualIndex;
        BOOL bRet;

        if (UNLIKELY(NULLPTR == lpGlobalRandom))
        {
                lpGlobalRandom = (LPRANDOM)GlobalAllocAndZero(sizeof(RANDOM));
                EXIT_IF_UNLIKELY_NULL(lpGlobalRandom, FALSE);

                lpGlobalRandom->hLock = CreateSpinLock();
                EXIT_IF_UNLIKELY_NULL(lpGlobalRandom->hLock, FALSE);
                lpGlobalRandom->rsType = RANDOM_SIZE_LONGLONG;
                lpGlobalRandom->lpChild = GlobalAllocAndZero(sizeof(RANDOM64));
                EXIT_IF_UNLIKELY_NULL(lpGlobalRandom->lpChild, FALSE);
                lpRandom64 = (LPRANDOM64)lpGlobalRandom->lpChild;
                lpGlobalRandom->hThis = NULLPTR;
                lpGlobalRandom->ullId = NULL;

                lpRandom64->uultState = (UULTRALONG)time(NULLPTR);
                lpRandom64->uultState += (UULTRALONG)(UARCHLONG)lpRandom64;
                lpRandom64->uultIncrease = (UULTRALONG)((DOUBLE)lpRandom64->uultState / GOLDEN_RATIO_D);
        }


        WaitForSingleObject(lpGlobalRandom->hLock, INFINITE_WAIT_TIME);

        for (
                bRet = TRUE, ualIndex = 0, lpRandom64 = (LPRANDOM64)lpGlobalRandom->lpChild;
                ualIndex < (UARCHLONG)(ualSizeOfOutBuffer / sizeof(ULONGLONG));
                ualIndex++
        ){ bRet &= __NextRandom(lpGlobalRandom, (LPVOID)((UARCHLONG)lpullOutBuffer + (sizeof(ULONGLONG) * ualIndex)), sizeof(ULONGLONG)); }

        ReleaseSingleObject(lpGlobalRandom->hLock);

        if (NULLPTR != lpualNumberOfRandomsWritten)
        { InterlockedAcquireExchange(lpualNumberOfRandomsWritten, ualIndex); }

        return bRet;
}




_Success_(return != FALSE, _Acquires_Shared_Lock_(_Has_Lock_Kind_(_Lock_Kind_Spin_Lock_)))
PODNET_API
BOOL
GetSingleGlobalRandomInRange(
        _In_            ULONGLONG               ullLowerRange,
        _In_            ULONGLONG               ullUpperRange,
        _Out_           LPULONGLONG             lpullOut
){
        EXIT_IF_NULL(ullUpperRange, FALSE);
        EXIT_IF_CONDITION(ullLowerRange > ullUpperRange, FALSE);
        EXIT_IF_UNLIKELY_NULL(lpullOut, FALSE);

        ULONGLONG ullOut;
        BOOL bRet;

        WaitForSingleObject(lpGlobalRandom->hLock, INFINITE_WAIT_TIME);

        bRet = __NextRandom(lpGlobalRandom, &ullOut, sizeof(ULONGLONG));
        if (FALSE != bRet)
        {
                ullOut = (ullOut % (ullUpperRange - ullLowerRange)) + ullLowerRange;
                *lpullOut = ullOut;
        }

        ReleaseSingleObject(lpGlobalRandom->hLock);

        return bRet;
}




_Success_(return != FALSE, _Acquires_Shared_Lock_(_Has_Lock_Kind_(_Lock_Kind_Spin_Lock_)))
PODNET_API
BOOL
GetMultipleGlobalRandomsInRange(
        _In_            ULONGLONG               ullLowerRange,
        _In_            ULONGLONG               ullUpperRange,
        _In_            UARCHLONG               ualNumberOfRandoms,
        _Out_           LPULONGLONG             lpullOut,
        _In_            UARCHLONG               ualOutBufferSize,
        _Out_Opt_       LPUARCHLONG             lpualRandomsWritten
){
        EXIT_IF_NULL(ullUpperRange, FALSE);
        EXIT_IF_CONDITION(ullLowerRange > ullUpperRange, FALSE);
        EXIT_IF_UNLIKELY_NULL(lpullOut, FALSE);
        EXIT_IF_CONDITION(ualOutBufferSize < sizeof(ULONGLONG), FALSE);

        UARCHLONG ualIndex, ualCount;
        BOOL bRet;
        ualCount = (ualNumberOfRandoms > ualOutBufferSize / sizeof(ULONGLONG)) ? ualNumberOfRandoms : ualOutBufferSize / sizeof(ULONGLONG);

        WaitForSingleObject(lpGlobalRandom->hLock, INFINITE_WAIT_TIME);

        for (
                ualIndex = 0, bRet = TRUE;
                ualIndex < ualCount;
                ualIndex++
        ){ 
                bRet &= __NextRandom(lpGlobalRandom, (LPVOID)((UARCHLONG)lpullOut + (sizeof(ULONGLONG) * ualIndex)), sizeof(ULONGLONG)); 
                if (FALSE != bRet)
                {       /* Sigh... */
                        *(LPULONGLONG)((UARCHLONG)lpullOut + (sizeof(ULONGLONG) * ualIndex)) = 
                                (*(LPULONGLONG)((UARCHLONG)lpullOut + (sizeof(ULONGLONG) * ualIndex)) 
                                % (ullUpperRange - ullLowerRange)) + ullLowerRange;
                }
        }

        ReleaseSingleObject(lpGlobalRandom->hLock);

        if (NULLPTR != lpualRandomsWritten)
        { InterlockedAcquireExchange(lpualRandomsWritten, ualIndex); }

        return bRet;
}
