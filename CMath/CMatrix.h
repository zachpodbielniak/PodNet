/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CMATRIX_H
#define CMATRIX_H 


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../CSystem/CSystem.h"



typedef struct __MATRIX           *HMATRIX, **LPHMATRIX;


typedef enum __MATRIX_FLAGS
{
        MATRIX_USE_CPU          = 0 << 0,
        MATRIX_OFFLOAD_TO_GPU   = 1 << 0
} MATRIX_FLAGS;




_Result_Null_On_Failure_
_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HMATRIX
CreateMatrix(
        _In_            UARCHLONG               ualRows,
        _In_            UARCHLONG               ualColumns,
        _In_            TYPE                    tyType,
        _In_            UARCHLONG               ualSize,
        _In_Opt_        ULONG                   ulFlags
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
DestroyMatrix(
        _In_            HMATRIX                 hmMatrix
);




_Result_Null_On_Failure_
_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HMATRIX
CloneMatrix(
        _In_            HMATRIX                 hmMatrix
);




_Result_Null_On_Failure_
_Success_(return != BOOL, _Non_Locking_)
PODNET_API
BOOL
CloneMatrixEx(
        _In_            HMATRIX                 hmMatrixSource,
        _In_Out_        HMATRIX                 hmMatrixDestination
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPVOID
MatrixValueAtCell(
        _In_            HMATRIX                 hmMatrix,
        _In_            UARCHLONG               ualRow,
        _In_            UARCHLONG               ualColumn
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPVOID
MatrixRow(
        _In_            HMATRIX                 hmMatrix,
        _In_            UARCHLONG               ualRow,
        _Out_Opt_       LPUARCHLONG             lpualRowElements,
        _Out_Opt_       LPUARCHLONG             lpualElementSize
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
AddMatrices(
        _In_Out_        HMATRIX                 hmAugend,
        _In_            HMATRIX                 hmAddend,
        _In_Opt_        ULONG                   ulFlags
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
AddScalarToMatrix(
        _In_Out_        HMATRIX                 hmAugend,
        _In_            LPVOID                  lpScalar,
        _In_Opt_        ULONG                   ulFlags
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
SubtractMatrices(
        _In_Out_        HMATRIX                 hmMinuend,
        _In_            HMATRIX                 hmSubtrahend,
        _In_Opt_        ULONG                   ulFlags
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
SubtractScalarFromMatrix(
        _In_Out_        HMATRIX                 hmMinuend,
        _In_            LPVOID                  lpScalar,
        _In_Opt_        ULONG                   ulFlags
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
SubtractMatrixFromScalar(
        _In_Opt_        HMATRIX                 hmSubtrahend,
        _In_            LPVOID                  lpScalar,
        _In_Opt_        ULONG                   ulFlags
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
MultiplyMatrices(
        _In_Out_        HMATRIX                 hmMultiplier,
        _In_            HMATRIX                 hmMultiplicand,
        _In_Opt_        ULONG                   ulFlags
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
MultiplyMatrixByScalar(
        _In_Out_        HMATRIX                 hmMultiplier,
        _In_            LPVOID                  lpScalar,
        _In_Opt_        ULONG                   ulFlags
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
DivideMatrices(
        _In_Out_        HMATRIX                 hmDividend,
        _In_            HMATRIX                 hmDivisor,
        _In_Opt_        ULONG                   ulFlags
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
DivideMatrixByScalar(
        _In_Out_        HMATRIX                 hmDividend,
        _In_            LPVOID                  lpScalar,
        _In_Opt_        ULONG                   ulFlags
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
DivideScalarByMatrix(
        _In_Out_        HMATRIX                 hmDivisor,
        _In_            LPVOID                  lpScalar,
        _In_Opt_        ULONG                   ulFlags
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
ModulusMatrices(
        _In_Out_        HMATRIX                 hmDividend,
        _In_            HMATRIX                 hmDivisor,
        _In_Opt_        ULONG                   ulFlags
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
ModulusScalarByMatrix(
        _In_Out_        HMATRIX                 hmDividend,
        _In_            LPVOID                  lpScalar,
        _In_Opt_        ULONG                   ulFlags
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
ModulusMatrixByScalar(
        _In_Out_        HMATRIX                 hmDivisor,
        _In_            LPVOID                  lpScalar,
        _In_Opt_        ULONG                   ulFlags
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
RowEchelonForm(
        _In_Out_        HMATRIX                 hmMatrix
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
ReducedRowEchelonForm(
        _In_Out_        HMATRIX                 hmMatrix
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
MatrixInverse(
        _In_Out_        HMATRIX                 hmMatrix
);


#endif