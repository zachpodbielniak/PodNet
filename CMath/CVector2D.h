/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CVECTOR2D_H
#define CVECTOR2D_H


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../CMemory/CMemory.h"
#include "CFastApprox.h"


typedef struct __VECTOR2D
{
	union
	{
		FLOAT fVals[2];
		struct { FLOAT fX, fY; };
	};
} VECTOR2D, *LPVECTOR2D, **DLPVECTOR2D, ***TLPVECTOR2D;



_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPVECTOR2D
CreateVector2D(
	_In_ 		FLOAT 			fX,
	_In_ 		FLOAT 			fY
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPVECTOR2D
CreateVector2DFromArray(
	_In_ 		LPFLOAT RESTRICT 	lpfVals
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPVECTOR2D
CreateVector2DFromVector2D(
	_In_ 		LPVECTOR2D RESTRICT 	lpv2Vec
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
DestroyVector2D(
	_In_ 		LPVECTOR2D RESTRICT 	lpv2Vec
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
Vector2DSwap(
	_In_ 		LPVECTOR2D RESTRICT 	lpv2Vec1,
	_In_ 		LPVECTOR2D RESTRICT 	lpv2Vec2
);




/* If first parameter is NULLPTR, then a new quaternion will be allocated and return */
_Success_(return != NULLPTR, _Non_Locking_)
_When_(lpv2Out != NULLPTR { _Success_(return == (LPVECTOR2D)(LPVOID)TRUE) })
PODNET_API
LPVECTOR2D
Vector2DAdd(
	_Out_Opt_	LPVECTOR2D 		lpv2Out,
	_In_ 		LPVECTOR2D 		lpv2Vec1,
	_In_ 		LPVECTOR2D 		lpv2Vec2
);




/* If first parameter is NULLPTR, then a new quaternion will be allocated and return */
_Success_(return != NULLPTR, _Non_Locking_)
_When_(lpv2Out != NULLPTR { _Success_(return == (LPVECTOR2D)(LPVOID)TRUE) })
PODNET_API
LPVECTOR2D
Vector2DSubtract(
	_Out_Opt_	LPVECTOR2D 		lpv2Out,
	_In_ 		LPVECTOR2D 		lpv2Vec1,
	_In_ 		LPVECTOR2D 		lpv2Vec2
);




/* If first parameter is NULLPTR, then a new quaternion will be allocated and return */
_Success_(return != NULLPTR, _Non_Locking_)
_When_(lpv2Out != NULLPTR { _Success_(return == (LPVECTOR2D)(LPVOID)TRUE) })
PODNET_API
LPVECTOR2D
Vector2DMultiply(
	_Out_Opt_	LPVECTOR2D 		lpv2Out,
	_In_ 		LPVECTOR2D 		lpv2Vec1,
	_In_ 		LPVECTOR2D 		lpv2Vec2
);




/* If first parameter is NULLPTR, then a new quaternion will be allocated and return */
_Success_(return != NULLPTR, _Non_Locking_)
_When_(lpv2Out != NULLPTR { _Success_(return == (LPVECTOR2D)(LPVOID)TRUE) })
PODNET_API
LPVECTOR2D
Vector2DDivide(
	_Out_Opt_	LPVECTOR2D 		lpv2Out,
	_In_ 		LPVECTOR2D 		lpv2Vec1,
	_In_ 		LPVECTOR2D 		lpv2Vec2
);




/* If first parameter is NULLPTR, then a new quaternion will be allocated and return */
_Success_(return != NULLPTR, _Non_Locking_)
_When_(lpv2Out != NULLPTR { _Success_(return == (LPVECTOR2D)(LPVOID)TRUE) })
PODNET_API
LPVECTOR2D
Vector2DAddScalar(
	_Out_Opt_	LPVECTOR2D 		lpv2Out,
	_In_ 		LPVECTOR2D 		lpv2Vec1,
	_In_ 		FLOAT 			fScalar
);




/* If first parameter is NULLPTR, then a new quaternion will be allocated and return */
_Success_(return != NULLPTR, _Non_Locking_)
_When_(lpv2Out != NULLPTR { _Success_(return == (LPVECTOR2D)(LPVOID)TRUE) })
PODNET_API
LPVECTOR2D
Vector2DSubtractScalar(
	_Out_Opt_	LPVECTOR2D 		lpv2Out,
	_In_ 		LPVECTOR2D 		lpv2Vec1,
	_In_ 		FLOAT 			fScalar
);




/* If first parameter is NULLPTR, then a new quaternion will be allocated and return */
_Success_(return != NULLPTR, _Non_Locking_)
_When_(lpv2Out != NULLPTR { _Success_(return == (LPVECTOR2D)(LPVOID)TRUE) })
PODNET_API
LPVECTOR2D
Vector2DMultiplyScalar(
	_Out_Opt_	LPVECTOR2D 		lpv2Out,
	_In_ 		LPVECTOR2D 		lpv2Vec1,
	_In_ 		FLOAT 			fScalar
);




/* If first parameter is NULLPTR, then a new quaternion will be allocated and return */
_Success_(return != NULLPTR, _Non_Locking_)
_When_(lpv2Out != NULLPTR { _Success_(return == (LPVECTOR2D)(LPVOID)TRUE) })
PODNET_API
LPVECTOR2D
Vector2DDivideScalar(
	_Out_Opt_	LPVECTOR2D 		lpv2Out,
	_In_ 		LPVECTOR2D 		lpv2Vec1,
	_In_ 		FLOAT 			fScalar
);




_Success_(return != NAN32, _Non_Locking_)
PODNET_API
FLOAT
Vector2DLength(
	_In_ 		LPVECTOR2D RESTRICT 	lpv2Vec
);




_Success_(return != NAN32, _Non_Locking_)
PODNET_API
FLOAT
Vector2DSquaredLength(
	_In_ 		LPVECTOR2D RESTRICT 	lpv2Vec
);




_Success_(return != NAN32, _Non_Locking_)
PODNET_API
FLOAT
Vector2DDistance(
	_In_ 		LPVECTOR2D RESTRICT 	lpv2Vec1,
	_In_ 		LPVECTOR2D RESTRICT 	lpv2Vec2
);




_Success_(return != NAN32, _Non_Locking_)
PODNET_API
FLOAT
Vector2DSquaredDistance(
	_In_ 		LPVECTOR2D RESTRICT 	lpv2Vec1,
	_In_ 		LPVECTOR2D RESTRICT 	lpv2Vec2
);




_Success_(return != NAN32, _Non_Locking_)
PODNET_API
FLOAT
Vector2DDotProduct(
	_In_ 		LPVECTOR2D RESTRICT 	lpv2Vec
);




_Success_(return != NAN32, _Non_Locking_)
PODNET_API
FLOAT
Vector2DDotProductAbsolute(
	_In_ 		LPVECTOR2D RESTRICT 	lpv2Vec
);




/* If lpv3Out == NULLPTR, lpv3Vec will be the NORMALIZED VECTOR3D */
/* FLOAT return is the length */
_Success_(return != NAN32, _Non_Locking_)
PODNET_API
FLOAT
Vector2DNormalize(
	_Out_Opt_ 	LPVECTOR2D RESTRICT 	lpv2Out,
	_In_ 		LPVECTOR2D RESTRICT 	lpv2Vec
);




/* If first parameter is NULLPTR, then a new quaternion will be allocated and return */
_Success_(return != NULLPTR, _Non_Locking_)
_When_(lpv2Out != NULLPTR { _Success_(return == (LPVECTOR2D)(LPVOID)TRUE) })
PODNET_API
LPVECTOR2D
Vector2DMidPoint(
	_Out_Opt_ 	LPVECTOR2D 		lpv2Out,
	_In_ 		LPVECTOR2D 		lpv2Vec1,
	_In_ 		LPVECTOR2D 		lpv2Vec2
);






#endif
