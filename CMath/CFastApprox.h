/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CFASTAPPROX_H
#define CFASTAPPROX_H



#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../CError/CError.h"

/* Lulwut? */

#define FAST_APPROX_SIN(X) 			((FLOAT)((X) - ((0.1466666666667f) * (X) * (X) * (X))))
#define FAST_APPROX_COS(X) 			((FLOAT)(-(((X) * (X)) * 0.5f) + (0.0366668 * (X) * (X) * (X) *(X)) + 1.0f))
#define FAST_APPROX_TAN(X) 			((FLOAT)FAST_APPROX_SIN((X)) / FAST_APPROX_COS((X)))
#define FAST_APPROX_CSC(X) 			((FLOAT)1.0f/FAST_APPROX_SIN((X)))
#define FAST_APPROX_SEC(X) 			((FLOAT)1.0f/FAST_APPROX_COS((X)))
#define FAST_APPROX_COT(X) 			((FLOAT)FAST_APPROX_COS((X)) / FAST_APPROX_SIN((X)))


#define FAST_APPROX_SIN_D(X) 			((DOUBLE)((X) - ((0.1466666666667) * (X) * (X) * (X))))
#define FAST_APPROX_COS_D(X)  			((DOUBLE)(-(((X) * (X)) * 0.5) + (0.0366668 * (X) * (X) * (X) *(X)) + 1.0))
#define FAST_APPROX_TAN_D(X)  			((DOUBLE)FAST_APPROX_SIN_D((X)) / FAST_APPROX_COS_D((X)))
#define FAST_APPROX_CSC_D(X)  			((DOUBLE)1.0/FAST_APPROX_SIN_D((X)))
#define FAST_APPROX_SEC_D(X)  			((DOUBLE)1.0/FAST_APPROX_COS_D((X)))
#define FAST_APPROX_COT_D(X)  			((DOUBLE)FAST_APPROX_COS_D((X)) / FAST_APPROX_SIN_D((X)))


#define FAST_APPROX_ARCSIN(X) 			((FLOAT)((X) + ((0.1466666666667f) * (X) * (X) * (X))))
#define FAST_APPROX_ARCCOS(X) 			((FLOAT)PI_2 - FAST_APPROX_ARCSIN((X)))
#define FAST_APPROX_ARCTAN(X) 			((FLOAT)(X) - (((X) * (X) * (X)) * 0.33333333333f) + (((X) * (X) * (X) * (X) * (X)) * 0.2f))


#define FAST_APPROX_ARCSIN_D(X)  		((DOUBLE)((X) + ((0.1466666666667) * (X) * (X) * (X))))
#define FAST_APPROX_ARCCOS_D(X)  		((DOUBLE)PI_2_D - FAST_APPROX_ARCSIN_D((X)))
#define FAST_APPROX_ARCTAN_D(X)  		((DOUBLE)(X) - (((X) * (X) * (X)) * 0.33333333333) + (((X) * (X) * (X) * (X) * (X)) * 0.2))


/* 
	For a given range between -PI/2 and PI/2, this should be many times faster.
	Keep in mind, this is an approximation, and NOT an exact result.
	This should be used if and ONLY if (performance > accuracy). 
	See Documentation/Derivations/FastSin.nb for more info.
	From my findings, it seems to be within +/- 1% error.
*/

_Success_((-1 < return) && (return < 1), ...)
PODNET_API
FLOAT
FastApproxSin(
	_In_Range_(-PI_2, PI_2)       	FLOAT          	fX
);




_Success_((-1 < return) && (return < 1), ...)
PODNET_API
FLOAT
FastApproxCos(
	_In_Range_(-PI_2, PI_2)    	FLOAT         	fX
);



_Success_((-1 < return) && (return < 1), ...)
PODNET_API
FLOAT
FastApproxTan(
	_In_Range_(-PI_4, PI_4)		FLOAT 		fX 
);




_Success_((-1 < return) && (return < 1), ...)
PODNET_API
DOUBLE 
FastApproxSinD(
	_In_Range_(-PI_2, PI_2)       	DOUBLE         	dbX
);




_Success_((-1 < return) && (return < 1), ...)
PODNET_API
DOUBLE 
FastApproxCosD(
	_In_Range_(-PI_2, PI_2)       	DOUBLE         	dbX
);




_Success_((-1 < return) && (return < 1), ...)
PODNET_API
DOUBLE 
FastApproxTanD(
	_In_Range_(-PI_2, PI_2)       	DOUBLE         	dbX
);


/* 'Cants */

_Success_((-1 < return) && (return < 1), ...)
PODNET_API
FLOAT
FastApproxSec(
	_In_Range_(-PI_2, PI_2)		FLOAT 		fX
);




_Success_((-1 < return) && (return < 1), ...)
PODNET_API
FLOAT
FastApproxCsc(
	_In_Range_(-PI_2, PI_2)		FLOAT 		fX
);




_Success_((-1 < return) && (return < 1), ...)
PODNET_API
FLOAT
FastApproxCot(
	_In_Range_(-PI_2, PI_2)		FLOAT 		fX
);




_Success_((-1 < return) && (return < 1), ...)
PODNET_API
DOUBLE
FastApproxSecD(
	_In_Range_(-PI_2, PI_2)		DOUBLE 		dbX
);




_Success_((-1 < return) && (return < 1), ...)
PODNET_API
DOUBLE
FastApproxCscD(
	_In_Range_(-PI_2, PI_2)		DOUBLE 		dbX
);




_Success_((-1 < return) && (return < 1), ...)
PODNET_API
DOUBLE
FastApproxCotD(
	_In_Range_(-PI_2, PI_2)		DOUBLE 		dbX
);


/* Inverse */

_Success_((-PI_2 < return) && (return < PI_2), ...)
PODNET_API 
FLOAT 
FastApproxArcSin(
	_In_Range_(-PI_2, PI_2)		FLOAT 		fX
);




_Success_((-PI_2 < return) && (return < PI_2), ...)
PODNET_API 
FLOAT 
FastApproxArcCos(
	_In_Range_(-PI_2, PI_2)		FLOAT 		fX
);




_Success_((-PI_2 < return) && (return < PI_2), ...)
PODNET_API 
FLOAT 
FastApproxArcTan(
	_In_Range_(-PI_2, PI_2)		FLOAT 		fX
);




_Success_((-PI_2 < return) && (return < PI_2), ...)
PODNET_API 
DOUBLE  
FastApproxArcSinD(
	_In_Range_(-PI_2, PI_2)		DOUBLE 		dbX
);




_Success_((-PI_2 < return) && (return < PI_2), ...)
PODNET_API 
DOUBLE  
FastApproxArcCosD(
	_In_Range_(-PI_2, PI_2)		DOUBLE 		dbX
);




_Success_((-PI_2 < return) && (return < PI_2), ...)
PODNET_API 
DOUBLE  
FastApproxArcTanD(
	_In_Range_(-PI_2, PI_2)		DOUBLE 		dbX
);

#endif
