/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CCoordinates.h"

_Result_Null_On_Failure_ _When_(lpCoordinate != NULLPTR)
PODNET_API
LPCOORDINATE
CreateCoordinate(
        _Out_Opt_               LPCOORDINATE            lpCoordinate,
        _In_                    DOUBLE                  dX,
        _In_                    DOUBLE                  dY,
        _In_Opt_ _When_(3D)     DOUBLE                  dZ,
        _In_                    BOOL                    bIs3DCoordinate
){
        if (lpCoordinate != NULLPTR)
        {
                lpCoordinate->dX = dX;
                lpCoordinate->dY = dY;
                lpCoordinate->dZ = dZ;
                lpCoordinate->bIs3DCoordinate = bIs3DCoordinate;
        }
        else
        {
                LPCOORDINATE lpCoor;
  
                lpCoor = LocalAlloc(sizeof(COORDINATE));
		if (NULLPTR == lpCoor)
		{ return NULLPTR; }

                lpCoor->dX = dX;
                lpCoor->dY = dY;
                lpCoor->dZ = dZ;
                lpCoor->bIs3DCoordinate = bIs3DCoordinate;
                return lpCoor;
        }


        return lpCoordinate;
}




_Success_(return != 0, ...)
PODNET_API
BOOL
FreeCoordinate(
        _In_                    LPCOORDINATE            lpcorCoordinate
){
        EXIT_IF_NULL(lpcorCoordinate, FALSE);
        FreeMemory(lpcorCoordinate);
        return TRUE;
}


_Success_(_Inexpressible_(), ...)
PODNET_API
DOUBLE
Distance(
        _In_                    LPCOORDINATE            lpcorCoordinate1,
        _In_                    LPCOORDINATE            lpcorCoordinate2
){
        EXIT_IF_NULL(lpcorCoordinate1, NAN);
        EXIT_IF_NULL(lpcorCoordinate2, NAN);
        KEEP_IN_REGISTER DOUBLE x, y, z;
        x = (lpcorCoordinate2->dX - lpcorCoordinate1->dX);
        y = (lpcorCoordinate2->dY - lpcorCoordinate1->dY);
        z = (lpcorCoordinate2->dZ - lpcorCoordinate1->dZ);
        x *= x;
        y *= y;
        z *= z;
        return sqrt(x + y + z);
}


_Success_(_Inexpressible_(), ...)
PODNET_API
DOUBLE
DistanceBetweenCoordinateAndPoint(
        _In_                    LPCOORDINATE            lpcorCoordinate,
        _In_                    DOUBLE                  dX,
        _In_                    DOUBLE                  dY,
        _In_Opt_ _When_(3D)     DOUBLE                  dZ
){
        EXIT_IF_NULL(lpcorCoordinate, NAN);
        KEEP_IN_REGISTER DOUBLE x, y, z;
        x = (dX - lpcorCoordinate->dX);
        y = (dY - lpcorCoordinate->dY);
        z = (dZ - lpcorCoordinate->dZ);
        x *= x;
        y *= y;
        z *= z;
        return sqrt(x + y + z);
}

