/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CShapes2D.h"

_Result_Null_On_Failure_
PODNET_API
LPSHAPE
CreateShape(
        _In_                            DOUBLE          dbX,
        _In_                            DOUBLE          dbY,
        _In_Opt_                        DOUBLE          dbZ,
        _In_                            ULONG           ulShapeType
){
        LPSHAPE lpshpShape;
        lpshpShape = LocalAlloc(sizeof(SHAPE));
        EXIT_IF_NULL(lpshpShape, 0);

        lpshpShape->ulShapeType = ulShapeType;
        lpshpShape->dbRotation = 0.0;
        
        CreateCoordinate(&(lpshpShape->corCoordinate), 0.0, 0.0, 0.0, FALSE);

        switch (ulShapeType)
        {
                case SHAPE_NOT_A_SHAPE:
                {
			DestroyShape(lpshpShape);
                        return 0;
                        break;
                }
                case SHAPE_CIRCLE:
                {
                        lpshpShape->lpChild = LocalAlloc(sizeof(CIRCLE));
			if (NULLPTR == lpshpShape->lpChild)
			{ 
				DestroyShape(lpshpShape);
				return 0;
			}
                        ((LPCIRCLE)(lpshpShape->lpChild))->lpshpParent = lpshpShape;
                        ((LPCIRCLE)(lpshpShape->lpChild))->dbRadius = dbX;
                        break;
                }
                case SHAPE_RECTANGLE:
                {
                        lpshpShape->lpChild = LocalAlloc(sizeof(RECTANGLE));
			if (NULLPTR == lpshpShape->lpChild)
			{ 
				DestroyShape(lpshpShape);
				return 0;
			}
                        ((LPRECTANGLE)(lpshpShape->lpChild))->lpshpParent = lpshpShape;
                        ((LPRECTANGLE)(lpshpShape->lpChild))->dbHeight = dbX;
                        ((LPRECTANGLE)(lpshpShape->lpChild))->dbBase = dbY;
                        break;
                }
                case SHAPE_TRIANGLE:            /* This will only create an eqilateral triangle, call CreateShapeEx() for more options */
                {
                        lpshpShape->lpChild = LocalAlloc(sizeof(TRIANGLE));
			if (NULLPTR == lpshpShape->lpChild)
			{ 
				DestroyShape(lpshpShape);
				return 0;
			}
                        ((LPTRIANGLE)lpshpShape->lpChild)->lpshpParent = lpshpShape;
                        ((LPTRIANGLE)lpshpShape->lpChild)->dbHeight = dbX;
                        ((LPTRIANGLE)lpshpShape->lpChild)->dbBase = dbY;
                        ((LPTRIANGLE)lpshpShape->lpChild)->dbTheta = 60.0;
                        break;
                }
                case SHAPE_ELLIPSE:
                {
                        lpshpShape->lpChild = LocalAlloc(sizeof(ELLIPSE));
			if (NULLPTR == lpshpShape->lpChild)
			{ 
				DestroyShape(lpshpShape);
				return 0;
			}
                        ((LPELLIPSE)lpshpShape->lpChild)->lpshpParent = lpshpShape;
                        ((LPELLIPSE)lpshpShape->lpChild)->dbR1 = dbX;
                        ((LPELLIPSE)lpshpShape->lpChild)->dbR2 = dbY;
                        break;
                }
                case SHAPE_PARALLELOGRAM:
                {
                        lpshpShape->lpChild = LocalAlloc(sizeof(PARALLELOGRAM));
			if (NULLPTR == lpshpShape->lpChild)
			{ 
				DestroyShape(lpshpShape);
				return 0;
			}
                        ((LPPARALLELOGRAM)lpshpShape->lpChild)->lpshpParent = lpshpShape;
                        ((LPPARALLELOGRAM)lpshpShape->lpChild)->dbHeight = dbX;
                        ((LPPARALLELOGRAM)lpshpShape->lpChild)->dbBase = dbY;
                        ((LPPARALLELOGRAM)lpshpShape->lpChild)->dbTheta = dbZ;
                        break;
                }
                case SHAPE_TRAPEZOID:
                {
                        lpshpShape->lpChild = LocalAlloc(sizeof(TRAPEZOID));
			if (NULLPTR == lpshpShape->lpChild)
			{ 
				DestroyShape(lpshpShape);
				return 0;
			}
                        ((LPTRAPEZOID)lpshpShape->lpChild)->lpshpParent = lpshpShape;
                        ((LPTRAPEZOID)lpshpShape->lpChild)->dbHeight = dbX;
                        ((LPTRAPEZOID)lpshpShape->lpChild)->dbBase = dbY;
                        ((LPTRAPEZOID)lpshpShape->lpChild)->dbHead = dbZ;
                        ((LPTRAPEZOID)lpshpShape->lpChild)->dbTheta = 60.0;
                        break;
                }
                case SHAPE_OTHER:
                {
			DestroyShape(lpshpShape);
                        return 0;
                        break;
                }
        }
        return lpshpShape;
}



_Result_Null_On_Failure_
PODNET_API
LPSHAPE
CreateShapeEx(
        _In_                            DOUBLE          dbX,
        _In_                            DOUBLE          dbY,
        _In_Opt_                        DOUBLE          dbZ,
        _In_Opt_                        DOUBLE          dbMisc,
        _In_Opt_                        DOUBLE          dbTheta,
        _In_                            DOUBLE          dbCoorX,
        _In_                            DOUBLE          dbCoorY,
        _In_                            ULONG           ulShapeType,
        _In_Opt_                        ULONG           ulShapeFlags
){
        LPSHAPE lpshpShape;
        lpshpShape = LocalAlloc(sizeof(SHAPE));
        EXIT_IF_NULL(lpshpShape, 0);

        lpshpShape->ulShapeType = ulShapeType;
        lpshpShape->dbRotation = 0.0;
        
        CreateCoordinate(&(lpshpShape->corCoordinate), dbCoorX, dbCoorY, 0.0, FALSE);

        switch (ulShapeType)
        {
                case SHAPE_NOT_A_SHAPE:
                {
			FreeMemory(lpshpShape);
                        return 0;
                        break;
                }
                case SHAPE_CIRCLE:
                {
                        lpshpShape->lpChild = LocalAlloc(sizeof(CIRCLE));
			if (NULLPTR == lpshpShape->lpChild)
			{
				FreeMemory(lpshpShape);
				return NULLPTR;
			}

                        ((LPCIRCLE)(lpshpShape->lpChild))->lpshpParent = lpshpShape;
                        ((LPCIRCLE)(lpshpShape->lpChild))->dbRadius = dbX;
                        break;
                }
                case SHAPE_RECTANGLE:
                {
                        lpshpShape->lpChild = LocalAlloc(sizeof(RECTANGLE));
			if (NULLPTR == lpshpShape->lpChild)
			{
				FreeMemory(lpshpShape);
				return NULLPTR;
			}

                        ((LPRECTANGLE)(lpshpShape->lpChild))->lpshpParent = lpshpShape;
                        ((LPRECTANGLE)(lpshpShape->lpChild))->dbHeight = dbX;
                        ((LPRECTANGLE)(lpshpShape->lpChild))->dbBase = dbY;
                        break;
                }
                case SHAPE_TRIANGLE:            /* This will only create an eqilateral triangle, call CreateShapeEx() for more options */
                {
			if (0 == ulShapeFlags)
			{ 
				FreeMemory(lpshpShape);
				return NULLPTR;
			}

                        lpshpShape->lpChild = LocalAlloc(sizeof(TRIANGLE));
			if (NULLPTR == lpshpShape->lpChild)
			{
				FreeMemory(lpshpShape);
				return NULLPTR;
			}

                        ((LPTRIANGLE)lpshpShape->lpChild)->lpshpParent = lpshpShape;
                        ((LPTRIANGLE)lpshpShape->lpChild)->dbHeight = dbX;
                        ((LPTRIANGLE)lpshpShape->lpChild)->dbBase = dbY;
                        ((LPTRIANGLE)lpshpShape->lpChild)->dbTheta = dbTheta;
                        ((LPTRIANGLE)lpshpShape->lpChild)->ulType = ulShapeFlags;
                        break;
                }
                case SHAPE_ELLIPSE:
                {
                        lpshpShape->lpChild = LocalAlloc(sizeof(ELLIPSE));
			if (NULLPTR == lpshpShape->lpChild)
			{
				FreeMemory(lpshpShape);
				return NULLPTR;
			}

                        ((LPELLIPSE)lpshpShape->lpChild)->lpshpParent = lpshpShape;
                        ((LPELLIPSE)lpshpShape->lpChild)->dbR1 = dbX;
                        ((LPELLIPSE)lpshpShape->lpChild)->dbR2 = dbY;
                        break;
                }
                case SHAPE_PARALLELOGRAM:
                {
                        lpshpShape->lpChild = LocalAlloc(sizeof(PARALLELOGRAM));
			if (NULLPTR == lpshpShape->lpChild)
			{
				FreeMemory(lpshpShape);
				return NULLPTR;
			}

                        ((LPPARALLELOGRAM)lpshpShape->lpChild)->lpshpParent = lpshpShape;
                        ((LPPARALLELOGRAM)lpshpShape->lpChild)->dbHeight = dbX;
                        ((LPPARALLELOGRAM)lpshpShape->lpChild)->dbBase = dbY;
                        ((LPPARALLELOGRAM)lpshpShape->lpChild)->dbTheta = dbTheta;
                        break;
                }
                case SHAPE_TRAPEZOID:
                {
                        lpshpShape->lpChild = LocalAlloc(sizeof(TRAPEZOID));
			if (NULLPTR == lpshpShape->lpChild)
			{
				FreeMemory(lpshpShape);
				return NULLPTR;
			}

                        ((LPTRAPEZOID)lpshpShape->lpChild)->lpshpParent = lpshpShape;
                        ((LPTRAPEZOID)lpshpShape->lpChild)->dbHeight = dbX;
                        ((LPTRAPEZOID)lpshpShape->lpChild)->dbBase = dbY;
                        ((LPTRAPEZOID)lpshpShape->lpChild)->dbHead = dbZ;
                        ((LPTRAPEZOID)lpshpShape->lpChild)->dbTheta = dbTheta;
                        break;
                }
                case SHAPE_TRAPEZIUM:
                {
                        lpshpShape->lpChild = LocalAlloc(sizeof(TRAPEZIUM));
			if (NULLPTR == lpshpShape->lpChild)
			{
				FreeMemory(lpshpShape);
				return NULLPTR;
			}

                        LPTRAPEZIUM lpTrape;
                        lpTrape = ((LPTRAPEZIUM)lpshpShape->lpChild);
                        EXIT_IF_NULL(lpTrape, 0);
                        lpTrape->dbBase = dbX;
                        lpTrape->dbHead = dbY;
                        lpTrape->dbHeightLeft = dbMisc;
                        lpTrape->dbHeightRight = dbZ;
                        lpTrape->dbTheta = dbTheta;
                        break;
                }
                case SHAPE_ANNULUS:
                {
                        lpshpShape->lpChild = LocalAlloc(sizeof(ANNULUS));
			if (NULLPTR == lpshpShape->lpChild)
			{
				FreeMemory(lpshpShape);
				return NULLPTR;
			}

                        LPANNULUS lpAnn;
                        lpAnn = ((LPANNULUS)lpshpShape->lpChild);
                        EXIT_IF_NULL(lpAnn, 0);
                        lpAnn->dbInnerRadius = dbX;
                        lpAnn->dbOuterRadius = dbY;
                        break;
                }
                case SHAPE_POLYGON:
                {
                        lpshpShape->lpChild = LocalAlloc(sizeof(POLYGON));
			if (NULLPTR == lpshpShape->lpChild)
			{
				FreeMemory(lpshpShape);
				return NULLPTR;
			}

                        LPPOLYGON lpPoly;
                        lpPoly = ((LPPOLYGON)lpshpShape->lpChild);
                        EXIT_IF_NULL(lpPoly, 0);
                        lpPoly->dbSideLength = dbX;
                        lpPoly->dbRadius = dbY;
                        lpPoly->ulNumberOfSides = ulShapeFlags;
                        break;
                }
                case SHAPE_CIRCULAR_SECTOR:
                {
                        lpshpShape->lpChild = LocalAlloc(sizeof(CIRCULAR_SECTOR));
			if (NULLPTR == lpshpShape->lpChild)
			{
				FreeMemory(lpshpShape);
				return NULLPTR;
			}

                        LPCIRCULAR_SECTOR lpCirc;
                        lpCirc = ((LPCIRCULAR_SECTOR)lpshpShape->lpChild);
                        EXIT_IF_NULL(lpCirc, 0);
                        lpCirc->dbRadius = dbX;
                        lpCirc->dbTheta = dbTheta;
                        break;
                }
                case SHAPE_ANNULAR_SECTOR:
                {
                        lpshpShape->lpChild = LocalAlloc(sizeof(ANNULAR_SECTOR));
			if (NULLPTR == lpshpShape->lpChild)
			{
				FreeMemory(lpshpShape);
				return NULLPTR;
			}

                        LPANNULAR_SECTOR lpAnnSec;
                        lpAnnSec = ((LPANNULAR_SECTOR)lpshpShape->lpChild);
                        EXIT_IF_NULL(lpAnnSec, 0);
                        lpAnnSec->dbInnerRadius = dbX;
                        lpAnnSec->dbOuterRadius = dbY;
                        lpAnnSec->dbTheta = dbTheta;
                        break;
                }
                case SHAPE_FILLET:
                {
                        lpshpShape->lpChild = LocalAlloc(sizeof(FILLET));
			if (NULLPTR == lpshpShape->lpChild)
			{
				FreeMemory(lpshpShape);
				return NULLPTR;
			}

                        LPFILLET lpFillet;
                        lpFillet = ((LPFILLET)lpshpShape->lpChild);
                        EXIT_IF_NULL(lpFillet, 0);
                        lpFillet->dbRadius = dbX;
                        break;
                }
                case SHAPE_OTHER:
                {
			FreeMemory(lpshpShape);
                        return 0;
                        break;
                }
        }
        return lpshpShape;
}



_Success_(return != FALSE, {})
PODNET_API
BOOL
DestroyShape(
        _In_                            LPSHAPE         lpshpShape
){
        EXIT_IF_UNLIKELY_NULL(lpshpShape, FALSE);
        FreeMemory(lpshpShape->lpChild);
        FreeMemory(lpshpShape);
        return TRUE;
}



_Success_(return != 0.0, {})
PODNET_API
DOUBLE
Area(
        _In_                            LPSHAPE         lpshpShape
){
        EXIT_IF_UNLIKELY_NULL(lpshpShape, 0.0);
        DOUBLE dbArea;
	dbArea = 0.0;
        switch(lpshpShape->ulShapeType)
        {
                case SHAPE_NOT_A_SHAPE:
                        return 0.0;
                        break;
                case SHAPE_CIRCLE:
                {
                        dbArea = PI * ((LPCIRCLE)lpshpShape->lpChild)->dbRadius * ((LPCIRCLE)lpshpShape->lpChild)->dbRadius;
                        break;
                }
                case SHAPE_RECTANGLE:
                {
                        dbArea = ((LPRECTANGLE)lpshpShape->lpChild)->dbBase * ((LPRECTANGLE)lpshpShape->lpChild)->dbHeight;
                        break;
                }
                case SHAPE_TRIANGLE:
                {
                        dbArea = 0.5 * ((LPTRIANGLE)lpshpShape->lpChild)->dbBase * ((LPTRIANGLE)lpshpShape->lpChild)->dbHeight;
                        break;
                }
                case SHAPE_ELLIPSE:
                {
                        dbArea = PI * ((LPELLIPSE)lpshpShape->lpChild)->dbR1 * ((LPELLIPSE)lpshpShape->lpChild)->dbR2;
                        break;
                }
                case SHAPE_PARALLELOGRAM:
                {
                        dbArea = ((LPPARALLELOGRAM)lpshpShape->lpChild)->dbBase * ((LPPARALLELOGRAM)lpshpShape->lpChild)->dbHeight;
                        break;
                }
                case SHAPE_TRAPEZOID:
                {
                        dbArea = (((LPTRAPEZOID)lpshpShape->lpChild)->dbBase + ((LPTRAPEZOID)lpshpShape->lpChild)->dbHead) * 
                                        (((LPTRAPEZOID)lpshpShape->lpChild)->dbHeight / 2);
                        break;
                }
                case SHAPE_TRAPEZIUM:
                {
                        LPTRAPEZIUM lpTrap;
                        DOUBLE dbTemp;
                        lpTrap = ((LPTRAPEZIUM)lpshpShape->lpChild);
                        dbTemp = lpTrap->dbHeightRight / tan(DEGREES_TO_RADIANS(lpTrap->dbTheta));
                        dbArea = ((lpTrap->dbHeightRight * (lpTrap->dbBase - dbTemp)));
                        /* TODO: Finish this! */
                        break;
                }
                case SHAPE_ANNULUS:
                {
                        LPANNULUS lpAnn;
                        lpAnn = ((LPANNULUS)lpshpShape->lpChild);
                        dbArea = PI_D * (lpAnn->dbOuterRadius * lpAnn->dbOuterRadius - lpAnn->dbInnerRadius * lpAnn->dbInnerRadius);
                        break;
                }
                case SHAPE_POLYGON:
                {
                        LPPOLYGON lpPoly;
                        lpPoly = ((LPPOLYGON)lpshpShape->lpChild);
                        dbArea = (lpPoly->ulNumberOfSides * lpPoly->dbSideLength * lpPoly->dbRadius) * 0.5;
                        break;
                }
                case SHAPE_CIRCULAR_SECTOR:
                {
                        LPCIRCULAR_SECTOR lpCS;
                        lpCS = ((LPCIRCULAR_SECTOR)lpshpShape->lpChild);
                        dbArea = PI_D * lpCS->dbTheta * lpCS->dbRadius * lpCS->dbRadius * 0.00277778;
                        break;
                }
                case SHAPE_ANNULAR_SECTOR:
                {
                        LPANNULAR_SECTOR lpAnnSec;
                        lpAnnSec = ((LPANNULAR_SECTOR)lpshpShape->lpChild);
                        dbArea = PI_D * lpAnnSec->dbTheta * 0.00277778 * (lpAnnSec->dbOuterRadius * lpAnnSec->dbOuterRadius - lpAnnSec->dbInnerRadius * lpAnnSec->dbInnerRadius);
                        break;
                }
                case SHAPE_FILLET:
                {
                        LPFILLET lpFillet;
                        lpFillet = ((LPFILLET)lpshpShape->lpChild);
                        dbArea = 0.214602 * (lpFillet->dbRadius * lpFillet->dbRadius);
                        break;
                }
                case SHAPE_OTHER: return 0; break;
        }
        return dbArea;
}



_Success_(return != NAN, {})
PODNET_API
DOUBLE
Perimeter(
        _In_                            LPSHAPE         lpshpShape
){
        EXIT_IF_UNLIKELY_NULL(lpshpShape, NAN);
        EXIT_IF_UNLIKELY_NULL(lpshpShape->lpChild, NAN);
        DOUBLE dbPerimeter;
	dbPerimeter = 0.0;	        
	switch(lpshpShape->ulShapeType)
        {
                case SHAPE_NOT_A_SHAPE: return NAN;
                case SHAPE_CIRCLE:
                {        
                        dbPerimeter = 2 * PI * ((LPCIRCLE)lpshpShape->lpChild)->dbRadius;
                        break;
                }
                case SHAPE_RECTANGLE:
                {
                        dbPerimeter = (2 * ((LPRECTANGLE)lpshpShape->lpChild)->dbBase) + (2 * ((LPRECTANGLE)lpshpShape->lpChild)->dbHeight);
                        break;
                }
                case SHAPE_TRIANGLE:
                {
                        switch (((LPTRIANGLE)lpshpShape->lpChild)->ulType)
                        {
                                
                                case EQUILATERAL_TRIANGLE:
                                {
                                        dbPerimeter = 3 * ((LPTRIANGLE)lpshpShape->lpChild)->dbBase;
                                        break;
                                }
                                case RIGHT_TRIANGLE:    /* Use Pythagorean Theorem to find side C. SQRT(dbHeight^2 + dbBase^2) + dbHeight + dbBase */
                                {
                                        KEEP_IN_REGISTER LPTRIANGLE lpTri;
                                        lpTri = ((LPTRIANGLE)lpshpShape->lpChild);
                                        dbPerimeter = lpTri->dbBase * lpTri->dbBase;
                                        dbPerimeter += lpTri->dbHeight * lpTri->dbHeight;
                                        dbPerimeter = sqrt(dbPerimeter);
                                        dbPerimeter += lpTri->dbBase;
                                        dbPerimeter += lpTri->dbHeight;
                                        break;
                                }
                                case OBLIQUE_TRIANGLE:
                                {
                                        KEEP_IN_REGISTER LPTRIANGLE lpTri;
                                        DOUBLE dbHyp1, dbB1;
                                        lpTri = ((LPTRIANGLE)lpshpShape->lpChild);
                                        dbPerimeter = lpTri->dbBase;
                                        dbHyp1 = (lpTri->dbHeight * 1.0/sin(DEGREES_TO_RADIANS(lpTri->dbTheta)));
                                        dbB1 = sqrt((dbHyp1 * dbHyp1) - (lpTri->dbHeight * lpTri->dbHeight));
                                        dbPerimeter += dbHyp1;
                                        dbPerimeter += sqrt((lpTri->dbHeight * lpTri->dbHeight) + ((lpTri->dbBase - dbB1) * (lpTri->dbBase - dbB1)));
                                        return dbPerimeter;
                                        break;
                                }
                                break;
                        }
                        break;
                        
                }
                case SHAPE_ELLIPSE:
                {
                        KEEP_IN_REGISTER LPELLIPSE lpEll;
                        lpEll = ((LPELLIPSE)lpshpShape->lpChild);
                        dbPerimeter = PI * (1.5 * (lpEll->dbR1 + lpEll->dbR2) - sqrt(lpEll->dbR1 * lpEll->dbR2)); /* Approximation */
                        break;
                }
                case SHAPE_PARALLELOGRAM:
                {
                        KEEP_IN_REGISTER LPPARALLELOGRAM lpPar;
                        lpPar = ((LPPARALLELOGRAM)lpshpShape->lpChild);
                        dbPerimeter = 2 * (lpPar->dbBase + (lpPar->dbHeight / tan(DEGREES_TO_RADIANS(lpPar->dbTheta))));
                        break;
                }
                case SHAPE_TRAPEZOID:
                {
                        KEEP_IN_REGISTER LPTRAPEZOID lpTrap;
                        lpTrap = ((LPTRAPEZOID)lpshpShape->lpChild);
                        DOUBLE b1, b2;
                        b1 = lpTrap->dbHeight / tan(DEGREES_TO_RADIANS(lpTrap->dbTheta));
                        b2 = lpTrap->dbBase - b1;
                        dbPerimeter = lpTrap->dbBase + lpTrap->dbHead;
                        dbPerimeter += sqrt(lpTrap->dbHeight * lpTrap->dbHeight + b1 * b1);
                        dbPerimeter += sqrt(lpTrap->dbHeight * lpTrap->dbHeight + b2 * b2);
                        break;
                }
        }
        return dbPerimeter;
}




_Success_(return != NAN, ...)
PODNET_API
DOUBLE
FastPerimeter(
        _In_                            LPSHAPE         lpshpShape
){
        EXIT_IF_UNLIKELY_NULL(lpshpShape, NAN);
        EXIT_IF_UNLIKELY_NULL(lpshpShape->lpChild, NAN);
        DOUBLE dbPerimeter;
	dbPerimeter = 0.0;
	switch(lpshpShape->ulShapeType)
        {
                case SHAPE_NOT_A_SHAPE: return NAN;
                case SHAPE_CIRCLE:
                {        
                        dbPerimeter = 2 * PI * ((LPCIRCLE)lpshpShape->lpChild)->dbRadius;
                        break;
                }
                case SHAPE_RECTANGLE:
                {
                        dbPerimeter = (2 * ((LPRECTANGLE)lpshpShape->lpChild)->dbBase) + (2 * ((LPRECTANGLE)lpshpShape->lpChild)->dbHeight);
                        break;
                }
                case SHAPE_TRIANGLE:
                {
                        switch (((LPTRIANGLE)lpshpShape->lpChild)->ulType)
                        {
                                
                                case EQUILATERAL_TRIANGLE:
                                {
                                        dbPerimeter = 3 * ((LPTRIANGLE)lpshpShape->lpChild)->dbBase;
                                        break;
                                }
                                case RIGHT_TRIANGLE:    /* Use Pythagorean Theorem to find side C. SQRT(dbHeight^2 + dbBase^2) + dbHeight + dbBase */
                                {
                                        KEEP_IN_REGISTER LPTRIANGLE lpTri;
                                        lpTri = ((LPTRIANGLE)lpshpShape->lpChild);
                                        dbPerimeter = lpTri->dbBase * lpTri->dbBase;
                                        dbPerimeter += lpTri->dbHeight * lpTri->dbHeight;
                                        dbPerimeter = sqrt(dbPerimeter);
                                        dbPerimeter += lpTri->dbBase;
                                        dbPerimeter += lpTri->dbHeight;
                                        break;
                                }
                                case OBLIQUE_TRIANGLE:
                                {
                                        KEEP_IN_REGISTER LPTRIANGLE lpTri;
                                        DOUBLE dbHyp1, dbB1;
                                        lpTri = ((LPTRIANGLE)lpshpShape->lpChild);
                                        dbPerimeter = lpTri->dbBase;
                                        dbHyp1 = (lpTri->dbHeight * FAST_APPROX_CSC_D(DEGREES_TO_RADIANS(lpTri->dbTheta)));
                                        dbB1 = sqrt((dbHyp1 * dbHyp1) - (lpTri->dbHeight * lpTri->dbHeight));
                                        dbPerimeter += dbHyp1;
                                        dbPerimeter += sqrt((lpTri->dbHeight * lpTri->dbHeight) + ((lpTri->dbBase - dbB1) * (lpTri->dbBase - dbB1)));
                                        return dbPerimeter;
                                        break;
                                }
                                break;
                        }
                        break;
                        
                }
                case SHAPE_ELLIPSE:
                {
                        KEEP_IN_REGISTER LPELLIPSE lpEll;
                        lpEll = ((LPELLIPSE)lpshpShape->lpChild);
                        dbPerimeter = PI * (1.5 * (lpEll->dbR1 + lpEll->dbR2) - sqrt(lpEll->dbR1 * lpEll->dbR2)); /* Approximation */
                        break;
                }
                case SHAPE_PARALLELOGRAM:
                {
                        KEEP_IN_REGISTER LPPARALLELOGRAM lpPar;
                        lpPar = ((LPPARALLELOGRAM)lpshpShape->lpChild);
                        dbPerimeter = 2 * (lpPar->dbBase + (lpPar->dbHeight / FAST_APPROX_TAN_D(DEGREES_TO_RADIANS(lpPar->dbTheta))));
                        break;
                }
                case SHAPE_TRAPEZOID:
                {
                        KEEP_IN_REGISTER LPTRAPEZOID lpTrap;
                        lpTrap = ((LPTRAPEZOID)lpshpShape->lpChild);
                        DOUBLE b1, b2;
                        b1 = lpTrap->dbHeight / FAST_APPROX_TAN_D(DEGREES_TO_RADIANS(lpTrap->dbTheta));
                        b2 = lpTrap->dbBase - b1;
                        dbPerimeter = lpTrap->dbBase + lpTrap->dbHead;
                        dbPerimeter += sqrt(lpTrap->dbHeight * lpTrap->dbHeight + b1 * b1);
                        dbPerimeter += sqrt(lpTrap->dbHeight * lpTrap->dbHeight + b2 * b2);
                        break;
                }
        }
        return dbPerimeter;
}
