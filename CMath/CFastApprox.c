/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CFastApprox.h"


_Success_((-1 < return) && (return < 1), ...)
PODNET_API
FLOAT
FastApproxSin(
        _In_Range_(-PI_2, PI_2)       	FLOAT          	fX
){ return FAST_APPROX_SIN(fX); }




_Success_((-1 < return) && (return < 1), ...)
PODNET_API
FLOAT
FastApproxCos(
        _In_Range_(-PI_2, PI_2)    	FLOAT         	fX
){ return FAST_APPROX_COS(fX); }



_Success_((-1 < return) && (return < 1), ...)
PODNET_API
FLOAT
FastApproxTan(
	_In_Range_(-PI_2, PI_2)		FLOAT 		fX 
){ return FAST_APPROX_TAN(fX); }




_Success_((-1 < return) && (return < 1), ...)
PODNET_API
DOUBLE
FastApproxSinD(
        _In_Range_(-PI_2, PI_2)       	DOUBLE         	dbX
){ return FAST_APPROX_SIN_D(dbX); }




_Success_((-1 < return) && (return < 1), ...)
PODNET_API
DOUBLE
FastApproxCosD(
        _In_Range_(-PI_2, PI_2)       	DOUBLE         	dbX
){ return FAST_APPROX_COS_D(dbX); }



_Success_((-1 < return) && (return < 1), ...)
PODNET_API
DOUBLE
FastApproxTanD(
        _In_Range_(-PI_2, PI_2)       	DOUBLE         	dbX
){ return FAST_APPROX_TAN_D(dbX); }


/* 'Cants */

_Success_((-1 < return) && (return < 1), ...)
PODNET_API
FLOAT
FastApproxSec(
	_In_Range_(-PI_2, PI_2)		FLOAT 		fX
){ return FAST_APPROX_SEC(fX); }




_Success_((-1 < return) && (return < 1), ...)
PODNET_API
FLOAT
FastApproxCsc(
	_In_Range_(-PI_2, PI_2)		FLOAT 		fX
){ return FAST_APPROX_CSC(fX); }




_Success_((-1 < return) && (return < 1), ...)
PODNET_API
FLOAT
FastApproxCot(
	_In_Range_(-PI_2, PI_2)		FLOAT 		fX
){ return FAST_APPROX_COT(fX); }




_Success_((-1 < return) && (return < 1), ...)
PODNET_API
DOUBLE
FastApproxSecD(
	_In_Range_(-PI_2, PI_2)		DOUBLE 		dbX
){ return FAST_APPROX_SEC_D(dbX); }




_Success_((-1 < return) && (return < 1), ...)
PODNET_API
DOUBLE
FastApproxCscD(
	_In_Range_(-PI_2, PI_2)		DOUBLE 		dbX
){ return FAST_APPROX_CSC_D(dbX); }




_Success_((-1 < return) && (return < 1), ...)
PODNET_API
DOUBLE
FastApproxCotD(
	_In_Range_(-PI_2, PI_2)		DOUBLE 		dbX
){ return FAST_APPROX_COT_D(dbX); }


/* Inverse */

_Success_((-PI_2 < return) && (return < PI_2), ...)
PODNET_API 
FLOAT 
FastApproxArcSin(
	_In_Range_(-PI_2, PI_2)		FLOAT 		fX
){ return FAST_APPROX_ARCSIN(fX); }




_Success_((-PI_2 < return) && (return < PI_2), ...)
PODNET_API 
FLOAT 
FastApproxArcCos(
	_In_Range_(-PI_2, PI_2)		FLOAT 		fX
){ return FAST_APPROX_ARCCOS(fX); }




_Success_((-PI_2 < return) && (return < PI_2), ...)
PODNET_API 
FLOAT 
FastApproxArcTan(
	_In_Range_(-PI_2, PI_2)		FLOAT 		fX
){ return FAST_APPROX_ARCTAN(fX); }




_Success_((-PI_2 < return) && (return < PI_2), ...)
PODNET_API 
DOUBLE  
FastApproxArcSinD(
	_In_Range_(-PI_2, PI_2)		DOUBLE 		dbX
){ return FAST_APPROX_ARCSIN_D(dbX); }




_Success_((-PI_2 < return) && (return < PI_2), ...)
PODNET_API 
DOUBLE  
FastApproxArcCosD(
	_In_Range_(-PI_2, PI_2)		DOUBLE 		dbX
){ return FAST_APPROX_ARCCOS_D(dbX); }




_Success_((-PI_2 < return) && (return < PI_2), ...)
PODNET_API 
DOUBLE  
FastApproxArcTanD(
	_In_Range_(-PI_2, PI_2)		DOUBLE 		dbX
){ return FAST_APPROX_ARCTAN_D(dbX); }
