/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CCOORDINATES_H
#define CCOORDINATES_H

#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"

typedef struct __COORDINATE
{
        DOUBLE          dX;
        DOUBLE          dY;
        DOUBLE          dZ;
        BOOL            bIs3DCoordinate;
} COORDINATE, *LPCOORDINATE, **DLPCOORDINATE, ***TLPCOORDINATE;

/* Graphics pointer */
typedef COORDINATE              *GPCOORDINATE, **DGPCOORDINATE;


_Result_Null_On_Failure_ _When_(lpCoordinate != NULLPTR)
PODNET_API
LPCOORDINATE
CreateCoordinate(
        _Out_Opt_               LPCOORDINATE            lpCoordinate,
        _In_                    DOUBLE                  dX,
        _In_                    DOUBLE                  dY,
        _In_Opt_ _When_(3D)     DOUBLE                  dZ,
        _In_                    BOOL                    bIs3DCoordinate
);


_Success_(return != 0, {})
PODNET_API
BOOL
FreeCoordinate(
        _In_                    LPCOORDINATE            lpcorCoordinate
);


_Success_(_Inexpressible_(), {})
PODNET_API
DOUBLE
Distance(
        _In_                    LPCOORDINATE            lpcorCoordinate1,
        _In_                    LPCOORDINATE            lpcorCoordinate2
);


_Success_(_Inexpressible_(), {})
PODNET_API
DOUBLE
DistanceBetweenCoordinateAndPoint(
        _In_                    LPCOORDINATE            lpcorCoordinate,
        _In_                    DOUBLE                  dX,
        _In_                    DOUBLE                  dY,
        _In_Opt_ _When_(3D)     DOUBLE                  dZ
);

#endif
