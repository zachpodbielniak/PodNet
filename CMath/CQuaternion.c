/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CQuaternion.h"


#define QUATERNION_EPSILON 		1.0E-03f


GLOBAL_VARIABLE QUATERNION quatZero = {0.0f, 0.0f, 0.0f, 0.0f};
/* GLOBAL_VARIABLE QUATERNION quatIdentify = {1.0f, 0.0f, 0.0f, 0.0f}; */



_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPQUATERNION
CreateQuaternion(
	_In_ 		FLOAT 			fW,
	_In_ 		FLOAT 			fX,
	_In_ 		FLOAT 			fY,
	_In_ 		FLOAT 			fZ 
){
	LPQUATERNION lpquaRet;
	lpquaRet = (LPQUATERNION)GlobalAllocAndZero(sizeof(QUATERNION));
	/* cppcheck-suppress memleak */
	EXIT_IF_UNLIKELY_NULL(lpquaRet, NULLPTR);

	lpquaRet->fW = fW;
	lpquaRet->fX = fX;
	lpquaRet->fY = fY;
	lpquaRet->fZ = fZ;

	/* TODO: Handle Inverse Calculation Here */

	return lpquaRet;
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPQUATERNION
CreateQuaternionFromArray(
	_In_ 		LPFLOAT RESTRICT 	lpfVals
){
	LPQUATERNION lpquaRet;
	lpquaRet = (LPQUATERNION)GlobalAllocAndZero(sizeof(QUATERNION));
	/* cppcheck-suppress memleak */
	EXIT_IF_UNLIKELY_NULL(lpquaRet, NULLPTR);

	CopyMemory(lpquaRet->fVals, lpfVals, sizeof(FLOAT) * 4);

	/* TODO: Handle Inverse Calculation Here */

	return lpquaRet;
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
BOOL
DestroyQuaternion(
	_In_ 		LPQUATERNION RESTRICT		lpquaQuat
){
	EXIT_IF_UNLIKELY_NULL(lpquaQuat, FALSE);
	FreeMemory(lpquaQuat);
	return TRUE;
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPVECTOR3D
QuaternionGetAxisX(
	_In_ 		LPQUATERNION RESTRICT 		lpquaQuat
){
	FLOAT fTy, fTz;
	FLOAT fTwy, fTwz;
	FLOAT fTxy, fTxz;
	FLOAT fTyy, fTzz;

	fTy = 2.0f * lpquaQuat->fY;
	fTz = 2.0f * lpquaQuat->fZ;

	fTwy = fTy * lpquaQuat->fW;
	fTwz = fTz * lpquaQuat->fW;

	fTxy = fTy * lpquaQuat->fX;
	fTxz = fTz * lpquaQuat->fX;

	fTyy = fTy * lpquaQuat->fY;
	fTzz = fTz * lpquaQuat->fZ;

	return CreateVector3D(
		1.0f - (fTyy + fTzz), 
		fTxy + fTwz, 
		fTxz - fTwy
	);
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPVECTOR3D
QuaternionGetAxisY(
	_In_ 		LPQUATERNION RESTRICT 		lpquaQuat
){
	FLOAT fTx, fTy, fTz;
	FLOAT fTwx, fTwz;
	FLOAT fTxx, fTxy;
	FLOAT fTyz, fTzz;

	fTx = 2.0f * lpquaQuat->fX;
	fTy = 2.0f * lpquaQuat->fY;
	fTz = 2.0f * lpquaQuat->fZ;

	fTwx = fTx * lpquaQuat->fW;
	fTwz = fTz * lpquaQuat->fW;

	fTxx = fTx * lpquaQuat->fX;
	fTxy = fTy * lpquaQuat->fX;

	fTyz = fTz * lpquaQuat->fY;
	fTzz = fTz * lpquaQuat->fZ;

	return CreateVector3D(
		fTxy - fTwz, 
		1.0f - (fTxx + fTzz), 
		fTyz + fTwx)
	;
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPVECTOR3D
QuaternionGetAxisZ(
	_In_ 		LPQUATERNION RESTRICT		lpquaQuat
){
	FLOAT fTx, fTy, fTz;
	FLOAT fTwx, fTwy;
	FLOAT fTxx, fTxz;
	FLOAT fTyy, fTyz;

	fTx = 2.0f * lpquaQuat->fX;
	fTy = 2.0f * lpquaQuat->fY;
	fTz = 2.0f * lpquaQuat->fZ;

	fTwx = fTx * lpquaQuat->fW;
	fTwy = fTy * lpquaQuat->fW;

	fTxx = fTx * lpquaQuat->fX;
	fTxz = fTz * lpquaQuat->fX;

	fTyy = fTy * lpquaQuat->fY;
	fTyz = fTz * lpquaQuat->fY;

	return CreateVector3D(
		fTxz + fTwy,
		fTyz - fTwx,
		1.0f - (fTxx + fTyy)
	);
}




/* If first parameter is NULLPTR, then a new quaternion will be allocated and return */
_Success_(return != NULLPTR, _Non_Locking_)
_When_(lpquaSum != NULLPTR { _Success_(return == (LPQUATERNION)(LPVOID)TRUE) })
PODNET_API
LPQUATERNION
QuaternionAdd(
	_In_Opt_	LPQUATERNION 		lpquaSum,
	_In_ 		LPQUATERNION 		lpquaAugend,
	_In_ 		LPQUATERNION 		lpquaAddend
){
	EXIT_IF_UNLIKELY_NULL(lpquaAugend, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpquaAddend, NULLPTR);

	BOOL bRet;
	bRet = FALSE;

	if (NULLPTR == lpquaSum)
	{
		lpquaSum = CreateQuaternion(0.0f, 0.0f, 0.0f, 0.0f);
		EXIT_IF_UNLIKELY_NULL(lpquaSum, NULLPTR);
		bRet = TRUE;
	}

	lpquaSum->fW = lpquaAugend->fW + lpquaAddend->fW;
	lpquaSum->fX = lpquaAugend->fX + lpquaAddend->fX;
	lpquaSum->fY = lpquaAugend->fY + lpquaAddend->fY;
	lpquaSum->fZ = lpquaAugend->fZ + lpquaAddend->fZ;

	if (TRUE == bRet)
	{ return lpquaSum; }
	return (LPQUATERNION)(LPVOID)TRUE;
}




/* If first parameter is NULLPTR, then a new quaternion will be allocated and return */
_Success_(return != NULLPTR, _Non_Locking_)
_When_(lpquaDifference != NULLPTR { _Success_(return == (LPQUATERNION)(LPVOID)TRUE) })
PODNET_API
LPQUATERNION
QuaternionSubtract(
	_In_Opt_	LPQUATERNION 		lpquaDifference,
	_In_ 		LPQUATERNION 		lpquaMinuend,
	_In_ 		LPQUATERNION 		lpquaSubtrahend
){
	EXIT_IF_UNLIKELY_NULL(lpquaMinuend, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpquaSubtrahend, NULLPTR);

	BOOL bRet;
	bRet = FALSE;

	if (NULLPTR == lpquaDifference)
	{
		lpquaDifference = CreateQuaternion(0.0f, 0.0f, 0.0f, 0.0f);
		EXIT_IF_UNLIKELY_NULL(lpquaDifference, NULLPTR);
		bRet = TRUE;
	}

	lpquaDifference->fW = lpquaMinuend->fW - lpquaSubtrahend->fW;
	lpquaDifference->fX = lpquaMinuend->fX - lpquaSubtrahend->fX;
	lpquaDifference->fY = lpquaMinuend->fY - lpquaSubtrahend->fY;
	lpquaDifference->fZ = lpquaMinuend->fZ - lpquaSubtrahend->fZ;

	if (TRUE == bRet)
	{ return lpquaDifference; }
	return (LPQUATERNION)(LPVOID)TRUE;
}




/* If first parameter is NULLPTR, then a new quaternion will be allocated and return */
_Success_(return != NULLPTR, _Non_Locking_)
_When_(lpquaProduct != NULLPTR { _Success_(return == (LPQUATERNION)(LPVOID)TRUE) })
PODNET_API
LPQUATERNION
QuaternionMultiply(
	_In_Opt_	LPQUATERNION 		lpquaProduct,
	_In_ 		LPQUATERNION 		lpquaMultiplicand,
	_In_ 		LPQUATERNION 		lpquaMultiplier
){
	EXIT_IF_UNLIKELY_NULL(lpquaMultiplicand, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpquaMultiplier, NULLPTR);

	BOOL bRet;
	bRet = FALSE;

	if (NULLPTR == lpquaProduct)
	{
		lpquaProduct = CreateQuaternion(0.0f, 0.0f, 0.0f, 0.0f);
		EXIT_IF_UNLIKELY_NULL(lpquaProduct, NULLPTR);
		bRet = TRUE;
	}

	/* 
		Quaternions are NOT commutative.
		Most cases -> 
			p*q != q*p
		Where 'p' and 'q' are QUATERNIONS
	*/

	lpquaProduct->fW = (
		(lpquaMultiplicand->fW * lpquaMultiplier->fW) - 
		(lpquaMultiplicand->fX * lpquaMultiplier->fX) -
		(lpquaMultiplicand->fY * lpquaMultiplier->fY) -
		(lpquaMultiplicand->fZ * lpquaMultiplier->fZ)
	);

	lpquaProduct->fX = (
		(lpquaMultiplicand->fW * lpquaMultiplier->fX) +
		(lpquaMultiplicand->fX * lpquaMultiplier->fW) +
		(lpquaMultiplicand->fY * lpquaMultiplier->fZ) -
		(lpquaMultiplicand->fZ * lpquaMultiplier->fY)
	);

	lpquaProduct->fY = (
		(lpquaMultiplicand->fW * lpquaMultiplier->fY) +
		(lpquaMultiplicand->fY * lpquaMultiplier->fW) +
		(lpquaMultiplicand->fZ * lpquaMultiplier->fX) -
		(lpquaMultiplicand->fX * lpquaMultiplier->fZ)
	);

	lpquaProduct->fZ = (
		(lpquaMultiplicand->fW * lpquaMultiplier->fZ) +
		(lpquaMultiplicand->fZ * lpquaMultiplier->fW) +
		(lpquaMultiplicand->fX * lpquaMultiplier->fY) -
		(lpquaMultiplicand->fY * lpquaMultiplier->fX)
	);

	if (TRUE == bRet)
	{ return lpquaProduct; }
	return (LPQUATERNION)(LPVOID)TRUE;
}




/* If first parameter is NULLPTR, then a new quaternion will be allocated and return */
_Success_(return != FALSE, _Non_Locking_)
_When_(lpquaProduct != NULLPTR { _Success_(return == (LPQUATERNION)(LPVOID)TRUE) })
PODNET_API
LPQUATERNION
QuaternionMultiplyByScalar(
	_In_Opt_	LPQUATERNION 		lpquaProduct,
	_In_ 		LPQUATERNION 		lpquaMultiplicand,
	_In_ 		FLOAT 			fScalar
){
	EXIT_IF_UNLIKELY_NULL(lpquaMultiplicand, NULLPTR);
	EXIT_IF_UNLIKELY_VALUE(FLOAT_EQUAL(NAN32, fScalar), TRUE, NULLPTR);

	BOOL bRet;
	bRet = FALSE;

	if (NULLPTR == lpquaProduct)
	{
		lpquaProduct = CreateQuaternion(0.0f, 0.0f, 0.0f, 0.0f);
		EXIT_IF_UNLIKELY_NULL(lpquaProduct, NULLPTR);
		bRet = TRUE;
	}

	lpquaProduct->fW = lpquaMultiplicand->fW * fScalar;
	lpquaProduct->fX = lpquaMultiplicand->fX * fScalar;
	lpquaProduct->fY = lpquaMultiplicand->fY * fScalar;
	lpquaProduct->fZ = lpquaMultiplicand->fZ * fScalar;
	
	if (TRUE == bRet)
	{ return lpquaProduct; }
	return (LPQUATERNION)(LPVOID)TRUE;
}




/* If first parameter is NULLPTR, then a new quaternion will be allocated and return */
_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPQUATERNION
QuaternionDivide(
	_In_Opt_	LPQUATERNION 		lpquaQuotient,
	_In_ 		LPQUATERNION 		lpquaDividend,
	_In_ 		LPQUATERNION 		lpquaDivisor
);




/* If first parameter is NULLPTR, then a new quaternion will be allocated and return */
_Success_(return != FALSE, _Non_Locking_)
_When_(lpquaOut != NULLPTR { _Success_(return == (LPQUATERNION)(LPVOID)TRUE) })
PODNET_API
LPQUATERNION
QuaternionNegate(
	_In_Opt_ 	LPQUATERNION 		lpquaOut,
	_In_ 		LPQUATERNION 		lpquaToBeNegated
){
	EXIT_IF_UNLIKELY_NULL(lpquaToBeNegated, NULLPTR);

	BOOL bRet;
	bRet = FALSE;

	if (NULLPTR == lpquaOut)
	{
		lpquaOut = CreateQuaternion(0.0f, 0.0f, 0.0f, 0.0f);
		EXIT_IF_UNLIKELY_NULL(lpquaOut, NULLPTR);
		bRet = TRUE;
	}

	lpquaOut->fW = -(lpquaToBeNegated->fW);
	lpquaOut->fX = -(lpquaToBeNegated->fX);
	lpquaOut->fY = -(lpquaToBeNegated->fY);
	lpquaOut->fZ = -(lpquaToBeNegated->fZ);

	if (TRUE == bRet)
	{ return lpquaOut; }
	return (LPQUATERNION)(LPVOID)TRUE;
}




_Success_(return != NAN32, _Non_Locking_)
PODNET_API
FLOAT 
QuaternionDotProduct(
	_In_ 		LPQUATERNION 		lpquaDoterand,
	_In_ 		LPQUATERNION		lpquaDoterplier
){
	EXIT_IF_UNLIKELY_NULL(lpquaDoterand, NAN32);
	EXIT_IF_UNLIKELY_NULL(lpquaDoterplier, NAN32);
	return (
		(lpquaDoterand->fW * lpquaDoterplier->fW) +
		(lpquaDoterand->fX * lpquaDoterplier->fX) +
		(lpquaDoterand->fY * lpquaDoterplier->fY) +
		(lpquaDoterand->fZ * lpquaDoterplier->fZ)
	);
}




_Success_(return != NAN32, _Non_Locking_)
PODNET_API
FLOAT
QuaternionNorm(
	_In_		LPQUATERNION RESTRICT 	lpquaNormerand
){
	EXIT_IF_UNLIKELY_NULL(lpquaNormerand, NAN32);
	return (
		(lpquaNormerand->fW * lpquaNormerand->fW) +
		(lpquaNormerand->fX * lpquaNormerand->fX) +
		(lpquaNormerand->fY * lpquaNormerand->fY) +
		(lpquaNormerand->fZ * lpquaNormerand->fZ)
	);
}




_Success_(return != NAN32, _Non_Locking_)
PODNET_API
FLOAT
QuaternionNormalize(
	_In_		LPQUATERNION RESTRICT 	lpquaNormerand
){
	EXIT_IF_UNLIKELY_NULL(lpquaNormerand, NAN32);

	FLOAT fNorm, fFactor;

	fNorm = QuaternionNorm(lpquaNormerand);
	fFactor = 1.0f / sqrtf(fNorm);

	QuaternionMultiplyByScalar(lpquaNormerand, lpquaNormerand, fFactor);
	return fNorm;
}




/* If first parameter is NULLPTR, then a new quaternion will be allocated and returned */
_Success_(return != FALSE, _Non_Locking_)
_When_(lpquaInverted != NULLPTR { _Success_(return == (LPQUATERNION)(LPVOID)TRUE) })
PODNET_API
LPQUATERNION
QuaternionInverse(
	_Out_Opt_	LPQUATERNION  		lpquaInverted,
	_In_ 		LPQUATERNION 		lpquaInverterand
){
	EXIT_IF_UNLIKELY_NULL(lpquaInverterand, NULLPTR);

	FLOAT fNormal, fInvNormal;
	BOOL bRet;
	bRet = FALSE;

	if (NULLPTR == lpquaInverted)
	{
		lpquaInverted = CreateQuaternion(0.0f, 0.0f, 0.0f, 0.0f);
		EXIT_IF_UNLIKELY_NULL(lpquaInverted, NULLPTR);
		bRet = TRUE;
	}

	/* Calculate the Norm */
	fNormal = (
		(lpquaInverterand->fW * lpquaInverterand->fW) +
		(lpquaInverterand->fX * lpquaInverterand->fX) +
		(lpquaInverterand->fY * lpquaInverterand->fY) +
		(lpquaInverterand->fZ * lpquaInverterand->fZ)
	);

	if (0.0f < fNormal)
	{
		fInvNormal = 1.0f / fNormal;
		lpquaInverted->fW = lpquaInverterand->fW * fInvNormal;
		lpquaInverted->fX = -(lpquaInverterand->fX) * fInvNormal;
		lpquaInverted->fY = -(lpquaInverterand->fY) * fInvNormal;
		lpquaInverted->fZ = -(lpquaInverterand->fZ) * fInvNormal;
	}
	else if (FALSE == bRet)
	{ CopyMemory(lpquaInverted->fVals, quatZero.fVals, sizeof(FLOAT) * 4); }

	if (TRUE == bRet)
	{ return lpquaInverted; }
	return (LPQUATERNION)(LPVOID)TRUE;
}




/* If first parameter is NULLPTR, then a new quaternion will be allocated and returned */
_Success_(return != FALSE, _Non_Locking_)
_When_(lpquaUnitInverted != NULLPTR { _Success_(return == (LPQUATERNION)(LPVOID)TRUE) })
PODNET_API
LPQUATERNION
QuaternionUnitInverse(
	_Out_Opt_	LPQUATERNION  		lpquaUnitInverted,
	_In_ 		LPQUATERNION 		lpquaUnitInverterand
){
	EXIT_IF_UNLIKELY_NULL(lpquaUnitInverterand, NULLPTR);

	BOOL bRet;
	bRet = FALSE;

	if (NULLPTR == lpquaUnitInverted)
	{
		lpquaUnitInverted = CreateQuaternion(0.0f, 0.0f, 0.0f, 0.0f);
		EXIT_IF_UNLIKELY_NULL(lpquaUnitInverted, NULLPTR);
		bRet = TRUE;
	}

	lpquaUnitInverted->fW = lpquaUnitInverterand->fW;
	lpquaUnitInverted->fX = -(lpquaUnitInverterand->fX);
	lpquaUnitInverted->fY = -(lpquaUnitInverterand->fY);
	lpquaUnitInverted->fZ = -(lpquaUnitInverterand->fZ);

	if (TRUE == bRet)
	{ return lpquaUnitInverted; }
	return (LPQUATERNION)(LPVOID)TRUE;
}




/* If first parameter is NULLPTR, then a new quaternion will be allocated and returned */
_Success_(return != FALSE, _Non_Locking_)
_When_(lpquaExped != NULLPTR { _Success_(return == (LPQUATERNION)(LPVOID)TRUE) })
PODNET_API
LPQUATERNION
QuaternionExp(
	_Out_Opt_	LPQUATERNION  		lpquaExped,
	_In_ 		LPQUATERNION 		lpquaExperand
){
	EXIT_IF_UNLIKELY_NULL(lpquaExperand, NULLPTR);

	FLOAT fAngle, fSin, fCoefficient;
	BOOL bRet;
	bRet = FALSE;

	if (NULLPTR == lpquaExped)
	{
		lpquaExped = CreateQuaternion(0.0f, 0.0f, 0.0f, 0.0f);
		EXIT_IF_UNLIKELY_NULL(lpquaExped, NULLPTR);
		bRet = TRUE;
	}

	/* 
		Exp(q) = cos(A)+sin(A)*(x*i+y*j+z*k)
		
		If sin(A) is about <= QUATERNION_EPSILON, then we need to use
		Exp(q) = cos(A)+A*(x*i+y*j+z*k) since:
			limit A -> 0 (A/sin(A)) = 1
	*/

	fAngle = sqrtf(
		(lpquaExperand->fX * lpquaExperand->fX) +
		(lpquaExperand->fY * lpquaExperand->fY) +
		(lpquaExperand->fZ * lpquaExperand->fZ)
	);

	fSin = sinf(fAngle);

	lpquaExped->fW = cosf(fAngle);


	if (fabsf(fSin) > QUATERNION_EPSILON || FLOAT_EQUAL(fabsf(fSin), QUATERNION_EPSILON))
	{
		fCoefficient = fSin / fAngle; 
		lpquaExped->fX = fCoefficient * lpquaExperand->fX;
		lpquaExped->fY = fCoefficient * lpquaExperand->fY;
		lpquaExped->fZ = fCoefficient * lpquaExperand->fZ;
	}
	else
	{
		lpquaExped->fX = lpquaExperand->fX;
		lpquaExped->fY = lpquaExperand->fY;
		lpquaExped->fZ = lpquaExperand->fZ;
	}

	if (TRUE == bRet)
	{ return lpquaExped; }
	return (LPQUATERNION)(LPVOID)TRUE;
}




/* If first parameter is NULLPTR, then a new quaternion will be allocated and returned */
_Success_(return != FALSE, _Non_Locking_)
_When_(lpquaExped != NULLPTR { _Success_(return == (LPQUATERNION)(LPVOID)TRUE) })
PODNET_API
LPQUATERNION
QuaternionExpFast(
	_Out_Opt_	LPQUATERNION  		lpquaExped,
	_In_ 		LPQUATERNION 		lpquaExperand
){
	EXIT_IF_UNLIKELY_NULL(lpquaExperand, NULLPTR);

	FLOAT fAngle, fSin, fCoefficient;
	BOOL bRet;
	bRet = FALSE;

	if (NULLPTR == lpquaExped)
	{
		lpquaExped = CreateQuaternion(0.0f, 0.0f, 0.0f, 0.0f);
		EXIT_IF_UNLIKELY_NULL(lpquaExped, NULLPTR);
		bRet = TRUE;
	}

	/* 
		Exp(q) = cos(A)+sin(A)*(x*i+y*j+z*k)
		
		If sin(A) is about <= QUATERNION_EPSILON, then we need to use
		Exp(q) = cos(A)+A*(x*i+y*j+z*k) since:
			limit A -> 0 (A/sin(A)) = 1
	*/

	fAngle = sqrtf(
		(lpquaExperand->fX * lpquaExperand->fX) +
		(lpquaExperand->fY * lpquaExperand->fY) +
		(lpquaExperand->fZ * lpquaExperand->fZ)
	);

	fSin = FAST_APPROX_SIN(fAngle);

	lpquaExped->fW = FAST_APPROX_COS(fAngle);


	if (fabsf(fSin) > QUATERNION_EPSILON || FLOAT_EQUAL(fabsf(fSin), QUATERNION_EPSILON))
	{
		fCoefficient = fSin / fAngle; 
		lpquaExped->fX = fCoefficient * lpquaExperand->fX;
		lpquaExped->fY = fCoefficient * lpquaExperand->fY;
		lpquaExped->fZ = fCoefficient * lpquaExperand->fZ;
	}
	else
	{
		lpquaExped->fX = lpquaExperand->fX;
		lpquaExped->fY = lpquaExperand->fY;
		lpquaExped->fZ = lpquaExperand->fZ;
	}

	if (TRUE == bRet)
	{ return lpquaExped; }
	return (LPQUATERNION)(LPVOID)TRUE;
}




/* If first parameter is NULLPTR, then a new quaternion will be allocated and returned */
_Success_(return != FALSE, _Non_Locking_)
_When_(lpquaLogout != NULLPTR { _Success_(return == (LPQUATERNION)(LPVOID)TRUE) })
PODNET_API
LPQUATERNION
QuaternionLog(
	_Out_Opt_	LPQUATERNION  		lpquaLogout,
	_In_ 		LPQUATERNION 		lpquaLogerand
){
	EXIT_IF_UNLIKELY_NULL(lpquaLogerand, NULLPTR);

	FLOAT fAngle, fSin, fCoefficient;
	BOOL bRet;
	bRet = FALSE;

	if (NULLPTR == lpquaLogout)
	{
		lpquaLogout = CreateQuaternion(0.0f, 0.0f, 0.0f, 0.0f);
		EXIT_IF_UNLIKELY_NULL(lpquaLogout, NULLPTR);
		bRet = TRUE;
	}

	/* 
		Log(q) = A * (x*i + y*j + z*k). 

		If sin(A) is about zero we use:
		log(q) = sin(A) * (x*i + y*j + z*k) since:
			limit A -> 0 sin(A) / A = 1
	*/

	if (fabsf(lpquaLogerand->fW) < 1.0f)
	{
		fAngle = acosf(lpquaLogerand->fW);
		fSin = sinf(fAngle);

		if (fabsf(fSin) > QUATERNION_EPSILON || FLOAT_EQUAL(fabsf(fSin), QUATERNION_EPSILON))
		{
			fCoefficient = fAngle / fSin;
			lpquaLogout->fX = fCoefficient * lpquaLogerand->fX;
			lpquaLogout->fY = fCoefficient * lpquaLogerand->fY;
			lpquaLogout->fZ = fCoefficient * lpquaLogerand->fZ;

			if (TRUE == bRet)
			{ return lpquaLogout; }
			return (LPQUATERNION)(LPVOID)TRUE;
		}
	}

	lpquaLogout->fX = lpquaLogerand->fX;
	lpquaLogout->fY = lpquaLogerand->fY;
	lpquaLogout->fZ = lpquaLogerand->fZ;

	if (TRUE == bRet)
	{ return lpquaLogout; }
	return (LPQUATERNION)(LPVOID)TRUE;
}




/* If first parameter is NULLPTR, then a new quaternion will be allocated and returned */
_Success_(return != FALSE, _Non_Locking_)
_When_(lpquaLogout != NULLPTR { _Success_(return == (LPQUATERNION)(LPVOID)TRUE) })
PODNET_API
LPQUATERNION
QuaternionLogFast(
	_Out_Opt_	LPQUATERNION  		lpquaLogout,
	_In_ 		LPQUATERNION 		lpquaLogerand
){
	EXIT_IF_UNLIKELY_NULL(lpquaLogerand, NULLPTR);

	FLOAT fAngle, fSin, fCoefficient;
	BOOL bRet;
	bRet = FALSE;

	if (NULLPTR == lpquaLogout)
	{
		lpquaLogout = CreateQuaternion(0.0f, 0.0f, 0.0f, 0.0f);
		EXIT_IF_UNLIKELY_NULL(lpquaLogout, NULLPTR);
		bRet = TRUE;
	}

	/* 
		Log(q) = A * (x*i + y*j + z*k). 

		If sin(A) is about zero we use:
		log(q) = sin(A) * (x*i + y*j + z*k) since:
			limit A -> 0 sin(A) / A = 1
	*/

	if (fabsf(lpquaLogerand->fW) < 1.0f)
	{
		fAngle = FAST_APPROX_ARCCOS(lpquaLogerand->fW);
		fSin = FAST_APPROX_SIN(fAngle);

		if (fabsf(fSin) > QUATERNION_EPSILON || FLOAT_EQUAL(fabsf(fSin), QUATERNION_EPSILON))
		{
			fCoefficient = fAngle / fSin;
			lpquaLogout->fX = fCoefficient * lpquaLogerand->fX;
			lpquaLogout->fY = fCoefficient * lpquaLogerand->fY;
			lpquaLogout->fZ = fCoefficient * lpquaLogerand->fZ;

			if (TRUE == bRet)
			{ return lpquaLogout; }
			return (LPQUATERNION)(LPVOID)TRUE;
		}
	}

	lpquaLogout->fX = lpquaLogerand->fX;
	lpquaLogout->fY = lpquaLogerand->fY;
	lpquaLogout->fZ = lpquaLogerand->fZ;

	if (TRUE == bRet)
	{ return lpquaLogout; }
	return (LPQUATERNION)(LPVOID)TRUE;
}




/* If first parameter is NULLPTR, then a new quaternion will be allocated and returned */
_Success_(return != NULLPTR, _Non_Locking_)
_When_(lpquaLogout != NULLPTR { _Success_(return == (LPVECTOR3D)(LPVOID)TRUE) })
PODNET_API
LPVECTOR3D
QuaternionMultiplyByVector3D(
	_Out_Opt_ 	LPVECTOR3D RESTRICT 	lpv3Out,
	_In_ 		LPQUATERNION RESTRICT 	lpquaMultiplicand,
	_In_ 		LPVECTOR3D RESTRICT 	lpv3Multiplier
){
	EXIT_IF_UNLIKELY_NULL(lpquaMultiplicand, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpv3Multiplier, NULLPTR);


	VECTOR3D v3UV, v3UUV;
	LPVECTOR3D lpv3Q;
	BOOL bRet;
	bRet = FALSE;

	if (NULLPTR == lpv3Out)
	{
		lpv3Out = CreateVector3D(0.0f, 0.0f, 0.0f);
		EXIT_IF_UNLIKELY_NULL(lpv3Out, NULLPTR);
		bRet = TRUE;
	}
	else
	{ ZeroMemory(lpv3Out->fVals, sizeof(FLOAT) * 3); }

	lpv3Q = CreateVector3D(lpquaMultiplicand->fX, lpquaMultiplicand->fY, lpquaMultiplicand->fZ);
	if (NULLPTR == lpv3Q)
	{
		if (NULLPTR != lpv3Out)
		{ DestroyVector3D(lpv3Out); }

		return (LPVECTOR3D)(LPVOID)FALSE;
	}

	Vector3DCrossProduct(&v3UV, lpv3Q, lpv3Multiplier);
	Vector3DCrossProduct(&v3UUV, lpv3Q, &v3UV);

	Vector3DMultiplyScalar(&v3UV, &v3UV, 2.0f * lpquaMultiplicand->fW);
	Vector3DMultiplyScalar(&v3UUV, &v3UUV, 2.0f);

	Vector3DAdd(lpv3Out, &v3UV, &v3UUV);
	Vector3DAdd(lpv3Out, lpv3Out, lpv3Multiplier);

	FreeMemory(lpv3Q);

	if (TRUE == bRet)
	{ return lpv3Out; }
	return (LPVECTOR3D)(LPVOID)TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
QuaternionEqual(
	_In_ 		LPQUATERNION RESTRICT 	lpquaLefterand,
	_In_ 		LPQUATERNION RESTRICT 	lpquaRighterand
){
	EXIT_IF_UNLIKELY_NULL(lpquaLefterand, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpquaRighterand, FALSE);

	FLOAT fRad, fAbs;

	fRad = acosf(
		QuaternionDotProduct(lpquaLefterand, lpquaRighterand)
	);

	fAbs = fabsf(fRad);

	return (
		(fAbs < QUATERNION_EPSILON) |
		FLOAT_EQUAL(fRad, PI)
	);
}




/* If first parameter is NULLPTR, then a new quaternion will be allocated and returned */
_Success_(return != NULLPTR, _Non_Locking_)
_When_(lpquaLogout != NULLPTR { _Success_(return == (LPQUATERNION)(LPVOID)TRUE) })
PODNET_API
LPQUATERNION
QuaternionSlerp(
	_Out_Opt_ 	LPQUATERNION RESTRICT 	lpquaOut,
	_In_ 		LPQUATERNION RESTRICT 	lpquaP,
	_In_ 		LPQUATERNION RESTRICT 	lpquaQ,
	_In_ 		FLOAT 			fDistance,
	_In_ 		BOOL 			bShortestPath
){
	EXIT_IF_UNLIKELY_NULL(lpquaP, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpquaQ, NULLPTR);


	QUATERNION quaTemp, quaTemp2;
	FLOAT fCos;
	BOOL bRet;
	bRet = FALSE;

	if (NULLPTR == lpquaOut)
	{
		lpquaOut = CreateQuaternion(0.0f, 0.0f, 0.0f, 0.0f);
		EXIT_IF_UNLIKELY_NULL(lpquaOut, NULLPTR);
		bRet = TRUE;
	}
	else
	{ QUATERNION_ZERO(lpquaOut); }

	ZeroMemory(&quaTemp, sizeof(QUATERNION));
	ZeroMemory(&quaTemp2, sizeof(QUATERNION));

	fCos = QuaternionDotProduct(lpquaP, lpquaQ);

	if ((fCos < 0.0f) & bShortestPath)
	{
		fCos = -fCos;
		QuaternionNegate(&quaTemp, lpquaQ);
	}
	else
	{ QUATERNION_COPY(&quaTemp, lpquaQ); }

	if (fabsf(fCos) < (1.0f - QUATERNION_EPSILON))
	{
		/* Standard Case */
		FLOAT fSin, fInverseSin, fAngle, fCo1, fCo2;

		fSin = sqrtf(1.0f - (fCos * fCos));
		fInverseSin = 1.0f / fSin;
		fAngle = atan2f(fSin, fCos);
		fCo1 = sinf((1.0f - fDistance) * fAngle) * fInverseSin;
		fCo2 = sinf(fDistance * fAngle) * fInverseSin;

		/* Make the quaternion of (fCo1 * P) + (fCo2 * Q) */
		QuaternionMultiplyByScalar(lpquaOut, &quaTemp, fCo2);
		QuaternionMultiplyByScalar(&quaTemp2, lpquaP, fCo1);
		QuaternionAdd(lpquaOut, lpquaOut, &quaTemp2);

		if (TRUE == bRet)
		{ return lpquaOut; }
		return (LPQUATERNION)(LPVOID)TRUE;
	}
	else
	{
		QuaternionMultiplyByScalar(lpquaOut, &quaTemp, fDistance);
		QuaternionMultiplyByScalar(&quaTemp2, lpquaP, (1.0f - fDistance));
		QuaternionAdd(lpquaOut, lpquaOut, &quaTemp2);
		QuaternionNormalize(lpquaOut);
	}


	if (TRUE == bRet)
	{ return lpquaOut; }
	return (LPQUATERNION)(LPVOID)TRUE;
}




/* If first parameter is NULLPTR, then a new quaternion will be allocated and returned */
_Success_(return != NULLPTR, _Non_Locking_)
_When_(lpquaLogout != NULLPTR { _Success_(return == (LPQUATERNION)(LPVOID)TRUE) })
PODNET_API
LPQUATERNION
QuaternionSlerpExtraSpins(
	_Out_Opt_ 	LPQUATERNION RESTRICT 	lpquaOut,
	_In_ 		LPQUATERNION RESTRICT 	lpquaP,
	_In_ 		LPQUATERNION RESTRICT 	lpquaQ,
	_In_ 		FLOAT 			fDistance,
	_In_ 		LONG 			lExtraSpins
){

	EXIT_IF_UNLIKELY_NULL(lpquaP, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpquaQ, NULLPTR);


	QUATERNION quaTemp, quaTemp2;
	FLOAT fInverseSin, fCo1, fCo2, fRads, fPhase;
	BOOL bRet;
	bRet = FALSE;

	if (NULLPTR == lpquaOut)
	{
		lpquaOut = CreateQuaternion(0.0f, 0.0f, 0.0f, 0.0f);
		EXIT_IF_UNLIKELY_NULL(lpquaOut, NULLPTR);
		bRet = TRUE;
	}
	else
	{ QUATERNION_ZERO(lpquaOut); }

	ZeroMemory(&quaTemp, sizeof(QUATERNION));
	ZeroMemory(&quaTemp2, sizeof(QUATERNION));
	
	fRads = acosf(
		QuaternionDotProduct(lpquaP, lpquaQ)
	);

	fPhase = PI * fDistance * lExtraSpins;
	fInverseSin = 1.0f / sinf(fRads);
	fCo1 = sinf((1.0f - fDistance) * fRads - fPhase) * fInverseSin;
	fCo2 = sinf((fDistance * fRads) + fPhase) * fInverseSin;

	QuaternionMultiplyByScalar(&quaTemp, lpquaP, fCo1);
	QuaternionMultiplyByScalar(&quaTemp2, lpquaQ, fCo2);
	QuaternionAdd(lpquaOut, &quaTemp, &quaTemp2);
	
	if (TRUE == bRet)
	{ return lpquaOut; }
	return (LPQUATERNION)(LPVOID)TRUE;
}




_Success_(return != NAN32, _Non_Locking_)
PODNET_API
FLOAT
QuaternionGetRoll(
	_In_ 		LPQUATERNION RESTRICT 	lpquaRollerand,
	_In_ 		BOOL 			bReProject
){
	EXIT_IF_UNLIKELY_NULL(lpquaRollerand, NAN32);

	if (TRUE == bReProject)
	{
		FLOAT fY, fZ, fWz, fXy, fYy, fZz;
		fY = 2.0f * lpquaRollerand->fY;
		fZ = 2.0f * lpquaRollerand->fZ;
		fWz = fZ * lpquaRollerand->fW;
		fXy = fY * lpquaRollerand->fX;
		fYy = fY * lpquaRollerand->fY;
		fZz = fZ * lpquaRollerand->fZ;

		return atan2f(
			fXy + fWz,
			1.0f - (fYy + fZz)
		);
	}


	return (
		atan2(
			2.0f * (lpquaRollerand->fX * lpquaRollerand->fY + lpquaRollerand->fW * lpquaRollerand->fZ),
			(lpquaRollerand->fW * lpquaRollerand->fW) +
				(lpquaRollerand->fX * lpquaRollerand->fX) -
				(lpquaRollerand->fY * lpquaRollerand->fY) -
				(lpquaRollerand->fZ * lpquaRollerand->fZ)
		)
	);
}




_Success_(return != NAN32, _Non_Locking_)
PODNET_API
FLOAT
QuaternionGetPitch(
	_In_ 		LPQUATERNION RESTRICT 	lpquaPitcherand,
	_In_ 		BOOL 			bReProject
){
	EXIT_IF_UNLIKELY_NULL(lpquaPitcherand, NAN32);

	if (TRUE == bReProject)
	{
		FLOAT fX, fZ, fWx, fXx, fYz, fZz;
		fX = 2.0f * lpquaPitcherand->fX;
		fZ = 2.0f * lpquaPitcherand->fZ;
		fWx = fX * lpquaPitcherand->fW;
		fXx = fX * lpquaPitcherand->fX;
		fYz = fZ * lpquaPitcherand->fY;
		fZz = fZ * lpquaPitcherand->fZ;

		return atan2f(
			fYz + fWx,
			1.0f - (fXx + fZz)
		);
	}

	return (
		atan2(
			2.0f * (lpquaPitcherand->fY * lpquaPitcherand->fZ + lpquaPitcherand->fW * lpquaPitcherand->fX),
			(lpquaPitcherand->fW * lpquaPitcherand->fW) -
				(lpquaPitcherand->fX * lpquaPitcherand->fX) -
				(lpquaPitcherand->fY * lpquaPitcherand->fY) +
				(lpquaPitcherand->fZ * lpquaPitcherand->fZ)
		)
	);
}




_Success_(return != NAN32, _Non_Locking_)
PODNET_API
FLOAT
QuaternionGetYaw(
	_In_ 		LPQUATERNION RESTRICT 	lpquaYawerand,
	_In_ 		BOOL 			bReProject
){
	EXIT_IF_UNLIKELY_NULL(lpquaYawerand, NAN32);

	if (TRUE == bReProject)
	{
		FLOAT fX, fY, fZ, fWy, fXx, fXz, fYy;
		fX = 2.0f * lpquaYawerand->fX;
		fY = 2.0f * lpquaYawerand->fY;
		fZ = 2.0f * lpquaYawerand->fZ;
		fWy = fY * lpquaYawerand->fW;
		fXx = fX * lpquaYawerand->fX;
		fXz = fZ * lpquaYawerand->fX;
		fYy = fY * lpquaYawerand->fY;

		return atan2f(
			fXz + fWy,
			1.0f - (fXx + fYy)
		);
	}

	return (
		asinf(-2.0f * (lpquaYawerand->fX * lpquaYawerand->fZ - lpquaYawerand->fW * lpquaYawerand->fY))
	);
}