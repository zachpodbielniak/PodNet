/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CARBITRARY_H
#define CARBITRARY_H


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"


typedef struct __ARBITRARYLONG          HARBITRARYLONG;
typedef HARBITRARYLONG                  HARBLONG;



_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HARBLONG
CreateArbitraryLong(
        _In_Z_          LPCSTR          lpcszValue,
        _In_            LONG            lBase 
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
DestroyArbitraryLong(
        _In_            HARBLONG        halValue
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HARBLONG
AddArbitraryLong(
        _In_            HARBLONG        halAugend,
        _In_            HARBLONG        halAddend
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HARBLONG
SubtractArbitraryLong(
        _In_            HARBLONG        halMinuend,
        _In_            HARBLONG        halSubtrahend
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HARBLONG
MultiplyArbitraryLong(
        _In_            HARBLONG        halMultiplier,
        _In_            HARBLONG        halMultiplicand
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HARBLONG
DivideArbitraryLong(
        _In_            HARBLONG        halDividend,
        _In_            HARBLONG        halDivisor
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HARBLONG
AddAndMultiplyArbitraryLong(
        _In_            HARBLONG        halAugend,
        _In_            HARBLONG        halMultiplier,
        _In_            HARBLONG        halMultiplicand
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HARBLONG
SubtractAndMultiplyArbitraryLong(
        _In_            HARBLONG        halMinuend,
        _In_            HARBLONG        halMultiplier,
        _In_            HARBLONG        halMultiplicand
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HARBLONG
MultiplyArbitraryLongByPowerOfTwo(
        _In_            HARBLONG        halMultiplier,
        _In_            ULONGLONG       ullPower 
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HARBLONG
AbsoluteValueOfArbitraryLong(
        _In_            HARBLONG        halValue
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HARBLONG
NegateArbitraryLong(
        _In_            HARBLONG        halValue
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
ArbitraryLongToString(
        _In_                    HARBLONG        halValue,
        _In_Range_(-36,62)      LONG            lBase,            
        _In_                    DLPSTR          dlpszOut,
        _In_                    UARCHLONG       ualBufferSize
);




_Success_(return != NULL, _Non_Locking_)
PODNET_API
UARCHLONG
ArbitraryLongGetStringSize(
        _In_            HARBLONG        halValue,
        _In_            LONG            lBase
);




_Success_(_Inexpressible_, _Non_Locking_)
PODNET_API
UARCHLONG
ArbitraryLongToUnsignedArchLongWithTruncation(
        _In_            HARBLONG        halValue
);




_Success_(_Inexpressible_, _Non_Locking_)
ARCHLONG
ArbitraryLongToSignedArchLongWithTruncation(
        _In_            HARBLONG        halValue
);




_Success_(_Inexpressible_, _Non_Locking_)
DOUBLE
ArbitraryLongToDoubleWithTruncation(
        _In_            HARBLONG        halValue
);




/*
        This returns the base multiplier, and the 
        value 2 is raised to the power of. For example
        return(DOUBLE) * 2 ^ lpalExponentOut.
*/
_Success_(_Inexpressible_, _Non_Locking_)
DOUBLE
ArbitraryLongToDoubleMultiplierAndLogBaseTwoWithTruncation(
        _In_            HARBLONG        halValue,
        _Out_           LPARCHLONG      lpalExponentOut
);


#endif