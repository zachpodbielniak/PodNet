/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CVECTOR3D_H
#define CVECTOR3D_H


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../CMemory/CMemory.h"
#include "CFastApprox.h"

typedef struct __VECTOR3D
{
	union
	{
		FLOAT fVals[3];
		struct { FLOAT fX, fY, fZ; };
	};
} VECTOR3D, *LPVECTOR3D, **DLPVECTOR3D, ***TLPVECTOR3D;




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPVECTOR3D
CreateVector3D(
	_In_ 		FLOAT 			fX,
	_In_ 		FLOAT 			fY,
	_In_ 		FLOAT 			fZ
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPVECTOR3D
CreateVector3DFromArray(
	_In_ 		LPFLOAT RESTRICT 	lpfVals
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPVECTOR3D
CreateVector3DFromVector3D(
	_In_ 		LPVECTOR3D RESTRICT 	lpv3Vec
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
DestroyVector3D(
	_In_ 		LPVECTOR3D RESTRICT 	lpv3Vec
);




/* If lpv3Out == NULLPTR, lpv3Vec will be the NORMALIZED VECTOR3D */
_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPVECTOR3D
Vector3DAdd(
	_Out_Opt_ 	LPVECTOR3D  		lpv3Out,
	_In_ 		LPVECTOR3D  		lpv3Vec1,
	_In_ 		LPVECTOR3D		lpv3Vec2 
);




/* If lpv3Out == NULLPTR, lpv3Vec will be the NORMALIZED VECTOR3D */
_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPVECTOR3D
Vector3DSubtract(
	_Out_Opt_ 	LPVECTOR3D  		lpv3Out,
	_In_ 		LPVECTOR3D  		lpv3Vec1,
	_In_ 		LPVECTOR3D		lpv3Vec2 
);




/* If lpv3Out == NULLPTR, lpv3Vec will be the NORMALIZED VECTOR3D */
_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPVECTOR3D
Vector3DMultiply(
	_Out_Opt_ 	LPVECTOR3D  		lpv3Out,
	_In_ 		LPVECTOR3D  		lpv3Vec1,
	_In_ 		LPVECTOR3D		lpv3Vec2 
);




/* If lpv3Out == NULLPTR, lpv3Vec will be the NORMALIZED VECTOR3D */
_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPVECTOR3D
Vector3DDivide(
	_Out_Opt_ 	LPVECTOR3D  		lpv3Out,
	_In_ 		LPVECTOR3D  		lpv3Vec1,
	_In_ 		LPVECTOR3D		lpv3Vec2 
);




_Success_(return != NULLPTR, _Non_Locking_) 
PODNET_API
LPVECTOR3D
Vector3DAddScalar(
	_Out_Opt_ 	LPVECTOR3D 		lpv3Out,
	_In_ 		LPVECTOR3D 		lpv3Vec,
	_In_ 		FLOAT 			fScalar
);




_Success_(return != NULLPTR, _Non_Locking_) 
PODNET_API
LPVECTOR3D
Vector3DSubtractScalar(
	_Out_Opt_ 	LPVECTOR3D 		lpv3Out,
	_In_ 		LPVECTOR3D 		lpv3Vec,
	_In_ 		FLOAT 			fScalar
);




_Success_(return != NULLPTR, _Non_Locking_) 
PODNET_API
LPVECTOR3D
Vector3DMultiplyScalar(
	_Out_Opt_ 	LPVECTOR3D 		lpv3Out,
	_In_ 		LPVECTOR3D 		lpv3Vec,
	_In_ 		FLOAT 			fScalar
);




_Success_(return != NULLPTR, _Non_Locking_) 
PODNET_API
LPVECTOR3D
Vector3DDivideScalar(
	_Out_Opt_ 	LPVECTOR3D 		lpv3Out,
	_In_ 		LPVECTOR3D 		lpv3Vec,
	_In_ 		FLOAT 			fScalar
);




_Success_(return != NAN32, _Non_Locking_)
PODNET_API
FLOAT
Vector3DLength(
	_In_ 		LPVECTOR3D RESTRICT 	lpv3Vec
);




_Success_(return != NAN32, _Non_Locking_)
PODNET_API
FLOAT
Vector3DLengthSquared(
	_In_ 		LPVECTOR3D RESTRICT 	lpv3Vec
);




_Success_(return != NAN32, _Non_Locking_)
PODNET_API
BOOL 
Vector3DIsZeroLength(
	_In_ 		LPVECTOR3D RESTRICT 	lpv3Vec
);




_Success_(return != NAN32, _Non_Locking_)
PODNET_API
FLOAT
Vector3DDistance(
	_In_ 		LPVECTOR3D 		lpv3Vec1,
	_In_ 		LPVECTOR3D 		lpv3Vec2
);




_Success_(return != NAN32, _Non_Locking_)
PODNET_API
FLOAT
Vector3DDistanceSquared(
	_In_ 		LPVECTOR3D 		lpv3Vec1,
	_In_ 		LPVECTOR3D 		lpv3Vec2
);




_Success_(return != NAN32, _Non_Locking_)
PODNET_API
FLOAT
Vector3DDotProduct(
	_In_ 		LPVECTOR3D 		lpv3Vec1,
	_In_ 		LPVECTOR3D 		lpv3Vec2
);




_Success_(return != NAN32, _Non_Locking_)
PODNET_API
FLOAT
Vector3DDotProductAbsolute(
	_In_ 		LPVECTOR3D 		lpv3Vec1,
	_In_ 		LPVECTOR3D 		lpv3Vec2
);





/* If lpv3Out == NULLPTR, lpv3Vec will be the NORMALIZED VECTOR3D */
/* FLOAT return is the length */
_Success_(return != NAN32, _Non_Locking_)
PODNET_API
FLOAT
Vector3DNormalize(
	_Out_Opt_ 	LPVECTOR3D RESTRICT 	lpv3Out,
	_In_ 		LPVECTOR3D RESTRICT 	lpv3Vec
);




/* If lpv3Out == NULLPTR, return will be a newly alloc'd VECTOR3D */
_Success_(return != NULLPTR, _Non_Locking_)
_When_(lpv3Out != NULLPTR { _Success_(return = (LPVECTOR3D)(LPVOID)TRUE) })
PODNET_API
LPVECTOR3D
Vector3DCrossProduct(
	_Out_Opt_ 	LPVECTOR3D 		lpv3Out,
	_In_ 		LPVECTOR3D 		lpv3Vec1,
	_In_ 		LPVECTOR3D 		lpv3Vec2
);




/* If lpv3Out == NULLPTR, return will be a newly alloc'd VECTOR3D */
_Success_(return != NULLPTR, _Non_Locking_)
_When_(lpv3Out != NULLPTR { _Success_(return = (LPVECTOR3D)(LPVOID)TRUE) })
PODNET_API
LPVECTOR3D
Vector3DMidPoint(
	_Out_Opt_ 	LPVECTOR3D 		lpv3Out,
	_In_ 		LPVECTOR3D 		lpv3Vec1,
	_In_ 		LPVECTOR3D 		lpv3Vec2
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL 
Vector3DLessThan(
	_In_ 		LPVECTOR3D 		lpv3Left,
	_In_ 		LPVECTOR3D 		lpv3Right
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL 
Vector3DGreaterThan(
	_In_ 		LPVECTOR3D 		lpv3Left,
	_In_ 		LPVECTOR3D 		lpv3Right
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL 
Vector3DEqual(
	_In_ 		LPVECTOR3D 		lpv3Left,
	_In_ 		LPVECTOR3D 		lpv3Right
);




/* If lpv3Out == NULLPTR, return will be a newly alloc'd VECTOR3D */
_Success_(return != NULLPTR, _Non_Locking_)
_When_(lpv3Out != NULLPTR { _Success_(return = (LPVECTOR3D)(LPVOID)TRUE) })
PODNET_API
LPVECTOR3D
Vector3DPerpendicular(
	_Out_Opt_ 	LPVECTOR3D 		lpv3Out,
	_In_ 		LPVECTOR3D 		lpv3Vec
);




_Success_(return != NAN32, _Non_Locking_)
PODNET_API
FLOAT
Vector3DAngleBetween(
	_In_ 		LPVECTOR3D 		lpv3Vec1,
	_In_ 		LPVECTOR3D 		lpv3Vec2
);




_Success_(return != NAN32, _Non_Locking_)
PODNET_API
FLOAT
Vector3DAngleBetweenFast(
	_In_ 		LPVECTOR3D 		lpv3Vec1,
	_In_ 		LPVECTOR3D 		lpv3Vec2
);




/* If lpv3Out == NULLPTR, return will be a newly alloc'd VECTOR3D */
_Success_(return != NULLPTR, _Non_Locking_)
_When_(lpv3Out != NULLPTR { _Success_(return = (LPVECTOR3D)(LPVOID)TRUE) })
PODNET_API
LPVECTOR3D
Vector3DReflect(
	_Out_Opt_ 	LPVECTOR3D 		lpv3Out,
	_In_ 		LPVECTOR3D 		lpv3Vec1,
	_In_ 		LPVECTOR3D 		lpv3Vec2
);




/* If lpv3Out == NULLPTR, return will be a newly alloc'd VECTOR3D */
_Success_(return != NULLPTR, _Non_Locking_)
_When_(lpv3Out != NULLPTR { _Success_(return = (LPVECTOR3D)(LPVOID)TRUE) })
PODNET_API
LPVECTOR3D
Vector3DPrimaryAxis(
	_Out_Opt_ 	LPVECTOR3D RESTRICT 	lpv3Out,
	_In_		LPVECTOR3D RESTRICT 	lpv3Vec
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL 
Vector3DPositionEquals(
	_In_ 		LPVECTOR3D RESTRICT 	lpv3Vec1,
	_In_ 		LPVECTOR3D RESTRICT 	lpv3Vec2
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL 
Vector3DIsNotANumber(
	_In_ 		LPVECTOR3D RESTRICT 	lpv3Vec
);




#endif