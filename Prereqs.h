/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef PREREQS_H
#define PREREQS_H

#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <stdarg.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <wchar.h>
#include <errno.h>
#include <ctype.h>
#include <dirent.h>


#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/file.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/wait.h>

#ifdef __linux__
#include <sys/epoll.h>
#include <linux/spi/spidev.h>
#include <gps.h>
#include <asm/ioctl.h>
#endif

#ifdef __FreeBSD__
#include <sys/event.h>
#endif

#include <signal.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <termios.h>
#include <fcntl.h>
#include <pthread.h>
#ifndef __linux__ /* FreeBSD */
#include <pthread_np.h>
#endif
#ifndef __NO_UNISTD
#include <unistd.h>
#endif


#ifdef __linux__
#include <GeoIP.h>
#include <GeoIPCity.h>
#endif 

#include <curl/curl.h>

#ifdef BUILD_WITH_ONEAGENT_SDK
#include <onesdk/onesdk.h>
#endif


#endif
