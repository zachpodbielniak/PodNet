/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CSmtp.h"


#define SMTP_PROTOCOL_TO_STR(X)					\
	(SMTP_PROTOCOL_SSL == (X)) ? "smtps://" : "smtp://"




typedef struct __SMTP
{
	INHERITS_FROM_HANDLE();
	HANDLE 			hLock;
	HANDLE 			hEvent;

	HSTRING 		hsServer;
	HSTRING 		hsUsername;
	HSTRING 		hsPassword;
	
	HSTRING 		hsFrom;
	HSTRING 		hsTo;
	HSTRING			hsCc;
	HSTRING 		hsBcc;
	HSTRING 		hsSubject;
	HSTRING 		hsBody;

	HANDLE 			hPayload;
	UARCHLONG 		ualLength;
	UARCHLONG 		ualRead;

	SMTP_PROTOCOL		spType;
	CURL 			*cCtx;
	struct curl_slist	*csRecipients;
	CURLcode		ccStatus;
} SMTP, *LPSMTP;



_Success_(return != FALSE, _Interlocked_Operation_)
INTERNAL_OPERATION
BOOL
__DestroySmtpConnection(
	_In_ 			HDERIVATIVE	hDerivative,
	_In_Opt_ 		ULONG 		ulFlags
){
	UNREFERENCED_PARAMETER(ulFlags);

	LPSMTP lpSmtp;
	lpSmtp = (LPSMTP)hDerivative;

	WaitForSingleObject(lpSmtp->hLock, INFINITE_WAIT_TIME);

	if (NULLPTR != lpSmtp->hsServer)
	{ DestroyHeapString(lpSmtp->hsServer); }
	if (NULLPTR != lpSmtp->hsUsername)
	{ DestroyHeapString(lpSmtp->hsUsername); }
	if (NULLPTR != lpSmtp->hsPassword)
	{ DestroyHeapString(lpSmtp->hsPassword); }
	if (NULLPTR != lpSmtp->hsFrom)
	{ DestroyHeapString(lpSmtp->hsFrom); }
	if (NULLPTR != lpSmtp->hsTo)
	{ DestroyHeapString(lpSmtp->hsTo); }
	if (NULLPTR != lpSmtp->hsCc)
	{ DestroyHeapString(lpSmtp->hsCc); }
	if (NULLPTR != lpSmtp->hsBcc)
	{ DestroyHeapString(lpSmtp->hsBcc); }
	if (NULLPTR != lpSmtp->hsSubject)
	{ DestroyHeapString(lpSmtp->hsSubject); }
	if (NULLPTR != lpSmtp->hsBody)
	{ DestroyHeapString(lpSmtp->hsBody); }

	DestroyObject(lpSmtp->hLock);
	DestroyObject(lpSmtp->hEvent);

	if (NULLPTR != lpSmtp->csRecipients)
	{ curl_slist_free_all(lpSmtp->csRecipients); }

	if (NULLPTR != lpSmtp->cCtx)
	{ curl_easy_cleanup(lpSmtp->cCtx); }
	
	FreeMemory(lpSmtp);
	
	return TRUE;
}



_Call_Back_
_Success_(return != 0, _Non_Locking_)
INTERNAL_OPERATION
ARCHLONG
__ConstructMessage(
	_In_ 			LPVOID 		lpOut,
	_In_ 			ARCHLONG 	alIgnore,
	_In_			ARCHLONG 	alSize,
	_In_			LPVOID 		lpData 
){
	UNREFERENCED_PARAMETER(alIgnore);
	
	LPSMTP lpSmtp;
	LPVOID lpSend;
	UARCHLONG ualToBeRead;


	lpSmtp = (LPSMTP)lpData;
	lpSend = (LPVOID)((UARCHLONG)DynamicHeapRegionData(lpSmtp->hPayload, NULLPTR) + lpSmtp->ualRead);
	ualToBeRead = ((UARCHLONG)alSize < (lpSmtp->ualLength - lpSmtp->ualRead)) ? (UARCHLONG)alSize : (lpSmtp->ualLength - lpSmtp->ualRead);

	CopyMemory(
		lpOut,
		lpSend,
		ualToBeRead
	);

	lpSmtp->ualRead += ualToBeRead;
	return ualToBeRead;
}




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNET_API
HANDLE
CreateSmtpConnection(
	_In_			SMTP_PROTOCOL	spType,
	_In_Z_ 			LPCSTR RESTRICT	lpcszServer,
	_In_Opt_Z_ 		LPCSTR RESTRICT	lpcszUsername,
	_In_Opt_Z_ 		LPCSTR RESTRICT	lpcszPassword,
	_In_			BOOL 		bVerifyCerts,
	_In_ 			BOOL 		bUseIpv6
){
	EXIT_IF_UNLIKELY_NULL(lpcszServer, NULL_OBJECT);

	HANDLE hSmtp;
	LPSMTP lpSmtp;
	CSTRING csServer[512];

	lpSmtp = GlobalAllocAndZero(sizeof(SMTP));
	/* cppcheck-suppress memleak */
	EXIT_IF_UNLIKELY_NULL(lpSmtp, NULL_OBJECT);

	lpSmtp->hLock = CreateCriticalSectionAndSpecifySpinCount(0x0C00);
	lpSmtp->hEvent = CreateEvent(FALSE);

	/* Load default config file */
	if (NULLPTR == lpcszUsername || lpcszPassword)
	{	
		HANDLE hConfig;
		CSTRING csBuffer[512];
		CSTRING csUsername[256];
		CSTRING csPassword[256];
		LPSTR lpszBuffer;
		ARCHLONG alSize;

		hConfig = OpenFile("/etc/PodNet/SmtpCreds", FILE_PERMISSION_READ, 0);
		if (NULL_OBJECT == hConfig)
		{
			DestroyObject(lpSmtp->hLock);
			DestroyObject(lpSmtp->hEvent);
			FreeMemory(lpSmtp);
			return NULL_OBJECT;
		}

		ZeroMemory(csBuffer, sizeof(csBuffer));						
		ZeroMemory(csUsername, sizeof(csUsername));
		ZeroMemory(csPassword, sizeof(csPassword));
		lpszBuffer = (LPSTR)csBuffer;

		alSize = ReadLineFromFile(hConfig, &lpszBuffer, sizeof(csBuffer) - 1, 0);

		if (0 == alSize)
		{
			DestroyObject(lpSmtp->hLock);
			DestroyObject(lpSmtp->hEvent);
			FreeMemory(lpSmtp);
			DestroyObject(hConfig);
			return NULL_OBJECT;
		}

		StringScanFormat(
			csBuffer,
			SMTP_PASSWORD_FORMAT,
			csUsername,
			csPassword
		);

		lpSmtp->hsUsername = CreateHeapString(csUsername);
		lpSmtp->hsPassword = CreateHeapString(csPassword);

		DestroyObject(hConfig);
	}
	else
	{
		lpSmtp->hsUsername = CreateHeapString(lpcszUsername);
		lpSmtp->hsPassword = CreateHeapString(lpcszPassword);
	}

	ZeroMemory(csServer, sizeof(csServer));
	StringPrintFormatSafe(
		csServer,
		sizeof(csServer) - 1,
		"%s%s",
		SMTP_PROTOCOL_TO_STR(spType),
		lpcszServer
	);

	lpSmtp->hsServer = CreateHeapString(csServer);
	lpSmtp->spType = spType;

	if (
		NULLPTR == lpSmtp->hsServer ||
		NULLPTR == lpSmtp->hsUsername ||
		NULLPTR == lpSmtp->hsPassword
	){ 
		__DestroySmtpConnection(lpSmtp, 0); 
		return NULL_OBJECT;
	}

	/* Begin cURL Config */
	lpSmtp->cCtx = curl_easy_init();

	curl_easy_setopt(lpSmtp->cCtx, CURLOPT_USERNAME, HeapStringValue(lpSmtp->hsUsername));	
	curl_easy_setopt(lpSmtp->cCtx, CURLOPT_PASSWORD, HeapStringValue(lpSmtp->hsPassword));
	curl_easy_setopt(lpSmtp->cCtx, CURLOPT_URL, HeapStringValue(lpSmtp->hsServer));

	if (FALSE == bVerifyCerts)
	{
		curl_easy_setopt(lpSmtp->cCtx, CURLOPT_SSL_VERIFYPEER, 0L);
		curl_easy_setopt(lpSmtp->cCtx, CURLOPT_SSL_VERIFYHOST, 0L);
	}

	if (FALSE == bUseIpv6)
	{ curl_easy_setopt(lpSmtp->cCtx, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4); }
	else
	{ curl_easy_setopt(lpSmtp->cCtx, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V6); }

	/* curl_easy_setopt(lpSmtp->cCtx, CURLOPT_VERBOSE, 1L); */
	
	curl_easy_setopt(lpSmtp->cCtx, CURLOPT_READFUNCTION, __ConstructMessage);
	curl_easy_setopt(lpSmtp->cCtx, CURLOPT_READDATA, lpSmtp);
	curl_easy_setopt(lpSmtp->cCtx, CURLOPT_UPLOAD, 1L);

	hSmtp = CreateHandleWithSingleInheritor(
		lpSmtp,
		&(lpSmtp->hThis),
		HANDLE_TYPE_SMTP_CONNECTION,
		__DestroySmtpConnection,
		0,
		NULLPTR,
		0,
		NULLPTR,
		0,
		&(lpSmtp->ullId),
		0
	);

	return hSmtp;
}




_Success_(return != FALSE, _Interlocked_Operation_)
PODNET_API
BOOL
SendEmail(
	_In_ 			HANDLE 		hSmtpConnection,
	_In_Z_			LPCSTR RESTRICT	lpcszFrom,
	_In_Z_ 			LPCSTR RESTRICT	lpcszTo,
	_In_Z_ 			LPCSTR RESTRICT	lpcszCc,
	_In_Z_ 			LPCSTR RESTRICT	lpcszSubject,
	_In_Z_ 			LPCSTR RESTRICT	lpcszBody
){
	EXIT_IF_UNLIKELY_NULL(hSmtpConnection, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpcszFrom, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpcszTo, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpcszSubject, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpcszBody, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hSmtpConnection), HANDLE_TYPE_SMTP_CONNECTION, FALSE);


	LPSMTP lpSmtp;
	HSTRING hsBuffer;
	CSTRING csBuffer[8192];
	CSTRING csTime[512];
	time_t t;
	struct tm *lptmT;

	lpSmtp = OBJECT_CAST(hSmtpConnection, LPSMTP);
	EXIT_IF_UNLIKELY_NULL(lpSmtp, FALSE);

	ZeroMemory(csBuffer, sizeof(csBuffer));
	ZeroMemory(csTime, sizeof(csTime));

	time(&t);
	lptmT = localtime(&t);
	strftime(
		csTime,
		sizeof(csTime) - 1,
		"%a, %d %b %Y %X %z",
		lptmT
	);

	StringPrintFormatSafe(
		csBuffer,
		sizeof(csBuffer) - 1,
		"Date: %s\r\n",
		csTime
	);
	
	hsBuffer = CreateHeapString(csBuffer);
	ZeroMemory(csBuffer, sizeof(csBuffer));

	StringPrintFormatSafe(
		csBuffer,
		sizeof(csBuffer) - 1,
		"To: <%s>\r\nFrom: <%s>\r\nSubject: %s\r\n",
		lpcszTo,
		lpcszFrom,
		lpcszSubject
	);

	HeapStringAppend(hsBuffer, csBuffer);
	ZeroMemory(csBuffer, sizeof(csBuffer));
	
	if (NULLPTR != lpcszCc)
	{
		StringPrintFormatSafe(
			csBuffer, 
			sizeof(csBuffer) - 1,
			"Cc: <%s>\r\n",
			lpcszCc
		);

		HeapStringAppend(hsBuffer, csBuffer);
		ZeroMemory(csBuffer, sizeof(csBuffer));
	}

	StringPrintFormatSafe(
		csBuffer,
		sizeof(csBuffer) - 1,
		"\r\n%s\r\n",
		lpcszBody
	);
	HeapStringAppend(hsBuffer, csBuffer);



	/* Put FROM, TO In Correct Format */
	ZeroMemory(csBuffer, sizeof(csBuffer));
	StringPrintFormatSafe(
		csBuffer,
		sizeof(csBuffer) - 1,
		"<%s>",
		lpcszFrom
	);
	lpSmtp->hsFrom = CreateHeapString(csBuffer);
	
	ZeroMemory(csBuffer, sizeof(csBuffer));
	StringPrintFormatSafe(
		csBuffer,
		sizeof(csBuffer) - 1,
		"<%s>",
		lpcszTo
	);
	lpSmtp->hsTo = CreateHeapString(csBuffer);

	if (NULLPTR != lpcszCc)
	{
		ZeroMemory(csBuffer, sizeof(csBuffer));
		StringPrintFormatSafe(
			csBuffer,
			sizeof(csBuffer) - 1,
			"<%s>",
			lpcszCc
		);
		lpSmtp->hsCc = CreateHeapString(csBuffer);
	}




	/* Create The Dynamic Region */
	if (NULL_OBJECT != lpSmtp->hPayload)
	{ DestroyObject(lpSmtp->hPayload); }

	lpSmtp->hPayload = CreateDynamicHeapRegion(
		NULL_OBJECT,
		HeapStringLength(hsBuffer),
		FALSE,
		0
	);

	DynamicHeapRegionAppend(
		lpSmtp->hPayload, 
		(LPVOID)HeapStringValue(hsBuffer),
		HeapStringLength(hsBuffer)
	);
	lpSmtp->ualLength = HeapStringLength(hsBuffer);
	DestroyHeapString(hsBuffer);


	/* Finishing Touches */
	lpSmtp->csRecipients = curl_slist_append(lpSmtp->csRecipients, HeapStringValue(lpSmtp->hsTo));

	if (NULLPTR != lpcszCc)
	{ lpSmtp->csRecipients = curl_slist_append(lpSmtp->csRecipients, HeapStringValue(lpSmtp->hsCc)); }

	curl_easy_setopt(lpSmtp->cCtx, CURLOPT_MAIL_FROM, HeapStringValue(lpSmtp->hsFrom));
	curl_easy_setopt(lpSmtp->cCtx, CURLOPT_MAIL_RCPT, lpSmtp->csRecipients);
	lpSmtp->ccStatus = curl_easy_perform(lpSmtp->cCtx);
	
	curl_slist_free_all(lpSmtp->csRecipients);
	lpSmtp->csRecipients = NULLPTR;

	return (0 == lpSmtp->ccStatus) ? TRUE : FALSE;
}	
