/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CHTTP_H
#define CHTTP_H



#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../CMemory/CDynamicHeapRegion.h"



typedef enum __HTTP_REQUEST_TYPE
{
	HTTP_REQUEST_INVALID 		= 0,
	HTTP_REQUEST_GET,
	HTTP_REQUEST_PUT,
	HTTP_REQUEST_POST
} HTTP_REQUEST_TYPE;


typedef enum __HTTP_VERSION
{
	HTTP_VERSION_INVALID		= 0,
	HTTP_VERSION_1_0,
	HTTP_VERSION_1_1,
	HTTP_VERSION_2_0
} HTTP_VERSION;




#define HTTP_REQUEST_TYPE_TO_STRING(T)			\
	((HTTP_REQUEST_GET == T) ? "GET" :		\
	(HTTP_REQUEST_PUT == T) ? "PUT" :		\
	(HTTP_REQUEST_POST == T) ? "POST" : "")

#define HTTP_VERSION_TO_STRING(V)			\
	((HTTP_VERSION_1_0 == V) ? "HTTP/1.0" :		\
	(HTTP_VERSION_1_1 == V) ? "HTTP/1.1" : 		\
	(HTTP_VERSION_2_0 == V) ? "HTTP/2" : "")




#define HTTP_ACCEPT_ANY				"*/*"
#define HTTP_ACCEPT_PLAIN_TEXT 			"text/plain"
#define HTTP_ACCEPT_HTML			"text/html"
#define HTTP_ACCEPT_TEXT_XML 			"text/xml"
#define HTTP_ACCEPT_HTML 			"text/html"
#define HTTP_ACCEPT_JSON 			"application/json"
#define HTTP_ACCEPT_DNS_JSON			"application/dns-json"
#define HTTP_ACCEPT_JAVASCRIPT 			"application/javascript"
#define HTTP_ACCEPT_XML 			"application/xml"


#define HTTP_CONNECTION_CLOSE 			"close"
#define HTTP_CONNECTION_KEEP_ALIVE 		"keep-alive"




typedef struct __HTTP_REQUEST_HEADER
{
	LPSTR 		lpszHeader;
	LPSTR		lpszValue;
} HTTP_REQUEST_HEADER, *LPHTTP_REQUEST_HEADER, **DLPHTTP_REQUEST_HEADER;




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPSTR
StripHttpHeader(
	_In_ 		LPCSTR RESTRICT 	lpcszData
);




#endif