/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CWebServer.h"


typedef struct _WEBSERVER
{
        INHERITS_FROM_HANDLE();
        HANDLE                  hCritSec;
        HANDLE                  hSocket;
        HANDLE                  hLog;
        HANDLE                  hThread;
        LPHANDLE                lphFile;
        DLPSTR                  dlpszPathes;
        LPSTR                   lpszHttpHeader;
        DLPSTR                  dlpszHtmlBuffer;
        ULONG                   ulMaxNumberOfFiles;
        ULONG                   ulNumberOfFiles;
        
} WEBSERVER, *LPWEBSERVER, **DLPWEBSERVER, ***TLPWEBSERVER;



_Success_(return != FALSE, _Acquires_Shared_Lock_(hDerivative->hCritSec, _Has_Lock_Kind_(_Lock_Kind_Critical_Section_)))
INTERNAL_OPERATION
BOOL
__DestroyWebServer(
        _In_                                    HDERIVATIVE             hDerivative,
        _In_Opt_                                ULONG                   ulFlags
){
        EXIT_IF_UNLIKELY_NULL(hDerivative, FALSE);
        EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hDerivative), HANDLE_TYPE_WEBSERVER, FALSE);
        LPWEBSERVER lpwsWeb;
        BOOL bRet;
        lpwsWeb = (LPWEBSERVER)GetHandleDerivative(hDerivative);
        bRet = TRUE;

        EnterCriticalSection(lpwsWeb->hCritSec);
        if (ulFlags & WEBSERVER_DESTROY_CLOSE_FILE_HANDLES)
        {
                ULONG ulIndex;
                for (
                        ulIndex = 0;
                        ulIndex < lpwsWeb->ulNumberOfFiles;
                        ulIndex++
                ){
                        bRet &= CloseHandle(lpwsWeb->lphFile[ulIndex]);
                        FreeMemory(lpwsWeb->dlpszPathes[ulIndex]);
                        FreeMemory(lpwsWeb->dlpszHtmlBuffer[ulIndex]);
                }
        }
        else
        {
                ULONG ulIndex;
                for (
                        ulIndex = 0;
                        ulIndex < lpwsWeb->ulNumberOfFiles;
                        ulIndex++
                ){ 
                        FreeMemory(lpwsWeb->dlpszPathes[ulIndex]);
                        FreeMemory(lpwsWeb->dlpszHtmlBuffer[ulIndex]);
                }       
        }

        FreeMemory(lpwsWeb->dlpszPathes);
        FreeMemory(lpwsWeb->dlpszHtmlBuffer);
        FreeMemory(lpwsWeb->lpszHttpHeader);
        FreeMemory(lpwsWeb);
        ExitCriticalSection(lpwsWeb->hCritSec);
        bRet &= DestroyHandle(lpwsWeb->hCritSec);
        bRet &= DestroyHandle(lpwsWeb->hSocket);
        return bRet;
}


_Call_Back_
INTERNAL_OPERATION
BOOL
__WebServerProc(
        _In_                            HANDLE                          hHostSocket,
        _In_                            HANDLE                          hClientSocket,
        _In_                            LPVOID                          lpParam,
        _Reserved_Must_Be_Null_         LPVOID                          lpReserved,
        _In_                            UULTRALONG                      uulConnectionCountNumber
){
        EXIT_IF_UNLIKELY_NULL(hHostSocket, FALSE);
        EXIT_IF_UNLIKELY_NULL(hClientSocket, FALSE);
        EXIT_IF_UNLIKELY_NULL(lpParam, FALSE);
        UNREFERENCED_PARAMETER(lpReserved);
        EXIT_IF_NOT_VALUE(GetHandleDerivativeType((HDERIVATIVE)lpParam), HANDLE_TYPE_WEBSERVER, FALSE);

        LPWEBSERVER lpwsWeb;
        LPSTR lpszBuffer, lpszRequest;
        CSTRING cstrRequestedFile[128];
        ULONG ulLength, ulIndex, ulBuffer;

        lpwsWeb = (LPWEBSERVER)GetHandleDerivative((HANDLE)lpParam);
        ulBuffer = MAX_ULONG;
	lpszRequest = NULLPTR;


        /*
                We will recieve on a particular socket, and get back the HTTP
                Request from the client. Next up, we MUST enter the Critical
                Section so we LOCK the WebServer so nothing else can change.
                We will then scan the string returned and search for the 
                'FILE' that is request. After we have that we will do a for 
                loop to run through the WebServer's dlpszPathes to MAP the Path to the corresponding HtmlBuffer.
        */


        ReceiveOnSocket(hHostSocket, lpszRequest, sizeof(CHAR) * 256);

        EnterCriticalSection(lpwsWeb->hCritSec);

        ulLength = StringLength(lpwsWeb->lpszHttpHeader);        
        ulLength += StringLength(lpwsWeb->dlpszHtmlBuffer[0]);
        lpszBuffer = LocalAllocAndZero(sizeof(CHAR) * ulLength + 16);
        lpszRequest = LocalAllocAndZero(sizeof(CHAR) * 256);
	/* cppcheck-suppress memleak */
        EXIT_IF_NULL(lpszBuffer, FALSE);
	/* cppcheck-suppress memleak */
        EXIT_IF_NULL(lpszRequest, FALSE);
        
        StringScanFormat(lpszRequest, "GET %s HTTP/1.1\r\n\r\n", cstrRequestedFile);

        for (
                ulIndex = 0; 
                ulIndex < lpwsWeb->ulNumberOfFiles; 
                ulIndex++
        ){
                if (StringCompare(lpwsWeb->dlpszPathes[ulIndex], lpszRequest) == 0)
                {
                        ulBuffer = ulIndex;
                        break;
                }
        }
        EXIT_IF_VALUE(ulBuffer, MAX_ULONG, FALSE);

        StringCopy(lpszBuffer, lpwsWeb->lpszHttpHeader);
        StringConcatenate(lpszBuffer, lpwsWeb->dlpszHtmlBuffer[ulBuffer]);

        SendToClient(hClientSocket, lpszBuffer, StringLength(lpszBuffer) * sizeof(CHAR) + sizeof(CHAR));

        ExitCriticalSection(lpwsWeb->hCritSec);

        FreeMemory(lpszBuffer);
        return TRUE;
}


_Result_Null_On_Failure_
PODNET_API
HANDLE
CreateWebServer(
        _In_                                    ULONG                   ulPort,
        _In_Opt_                                HANDLE                  hIndexPage            /* If NULL serves up memory */
){
        EXIT_IF_NULL(ulPort, NULLPTR);
        HANDLE hWebServer;
        LPWEBSERVER lpwsWeb;
        ULONGLONG ullID;
        lpwsWeb = LocalAlloc(sizeof(WEBSERVER));
        EXIT_IF_NULL(lpwsWeb, NULLPTR);
        ZeroMemory(lpwsWeb, sizeof(WEBSERVER));

        lpwsWeb->hCritSec = CreateCriticalSection();
	/* cppcheck-suppress memleak */
        EXIT_IF_NULL(lpwsWeb->hCritSec, NULLPTR);

        /* 
                Enter the Critical Section so that we can set info for
                this webserver, and make sure it is entered correctly
                before the thread that is spawned by calling
                ListenOnBoundSocketAndAcceptConnection() enters this
                very same Critical Section; thus, removing the chance
                for a race condition.
        */

        EnterCriticalSection(lpwsWeb->hCritSec);

        lpwsWeb->hSocket = CreateSocket(ulPort);
	/* cppcheck-suppress memleak */
        EXIT_IF_NULL(lpwsWeb->hSocket, NULLPTR);

        lpwsWeb->lphFile = LocalAlloc(sizeof(HANDLE) * 10);
        lpwsWeb->ulMaxNumberOfFiles = 10;
        lpwsWeb->dlpszPathes = LocalAlloc(sizeof(LPSTR) * lpwsWeb->ulMaxNumberOfFiles);
        lpwsWeb->dlpszHtmlBuffer = LocalAlloc(sizeof(LPSTR) * lpwsWeb->ulMaxNumberOfFiles);
        lpwsWeb->lpszHttpHeader = "HTTP/1.1 200 OK\r\n\n";

        hWebServer = CreateHandleWithSingleInheritor(
                        lpwsWeb,
                        &(lpwsWeb->hThis),
                        HANDLE_TYPE_WEBSERVER,
                        __DestroyWebServer,
                        0,
                        NULLPTR,
                        0,
                        NULLPTR,
                        0,
                        &ullID,
                        0
        );
        EXIT_IF_NULL(hWebServer, NULLPTR);

        if (hIndexPage != NULLPTR)
        {
                lpwsWeb->lphFile[0] = hIndexPage;
                lpwsWeb->dlpszPathes[0] = "/";
        }

        lpwsWeb->hThread = ListenOnBoundSocketAndAcceptConnection(
                        lpwsWeb->hSocket,
                        0xFF,
                        __WebServerProc,
                        (LPVOID)hWebServer,
                        NULLPTR,
                        0
        );

        ExitCriticalSection(lpwsWeb->hCritSec);

        return hWebServer;
}




_Success_(return != FALSE, ...)
PODNET_API
BOOL
DefineWebPage(
        _In_                                    HANDLE                  hWebServer,
        _In_Z_                                  LPCSTR                  lpcszPath,
        _In_Opt_ _When_(lpcszHtmlBody == NULL)  HANDLE                  hFile,
        _In_Opt_Z_ _When_(hFile == NULL)        LPCSTR                  lpcszHtmlBody,
        _In_Opt_ _When_(hFile == NULL)          ULONG                   ulSizeOfHtmlBody,
        _Reserved_Must_Be_Null_                 LPVOID                  lpReserved
){
        EXIT_IF_UNLIKELY_NULL(hWebServer, FALSE);
        EXIT_IF_NULL(lpcszPath, FALSE);
        EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hWebServer), HANDLE_TYPE_WEBSERVER, FALSE);
        if (hFile == NULL && lpcszHtmlBody == NULL) return FALSE;
        if (hFile == NULL && lpcszHtmlBody != NULL && ulSizeOfHtmlBody == 0) return FALSE;
        UNREFERENCED_PARAMETER(lpReserved);

        LPWEBSERVER lpwsWeb;
        BOOL bRet;
        lpwsWeb = (LPWEBSERVER)GetHandleDerivative(hWebServer);
        bRet = TRUE;

        /*
                Add Support for FILE HANDLES
        */

        EnterCriticalSection(lpwsWeb->hCritSec);

        if (hFile)
        {
                LPVOID lpOut;
                bRet &= ReadFile(hFile, &lpOut, 0);
        }
        else
        {
                /* Handle ReAlloc if need be */
                if (lpwsWeb->ulNumberOfFiles >= lpwsWeb->ulMaxNumberOfFiles)
                {
                        lpwsWeb->dlpszHtmlBuffer = LocalReAlloc(lpwsWeb->dlpszHtmlBuffer, 2 * lpwsWeb->ulMaxNumberOfFiles);
                        lpwsWeb->dlpszPathes = LocalReAlloc(lpwsWeb->dlpszPathes, 2 * lpwsWeb->ulMaxNumberOfFiles);
                        lpwsWeb->ulMaxNumberOfFiles *= 2;
                }
                ULONG ulIndex;
                ulIndex = lpwsWeb->ulNumberOfFiles;
                lpwsWeb->dlpszPathes[ulIndex] = lpcszPath;
                lpwsWeb->dlpszHtmlBuffer[ulIndex] = LocalAllocAndZero(sizeof(LPSTR) * ulSizeOfHtmlBody + 16);
                EXIT_IF_NULL(lpwsWeb->dlpszHtmlBuffer, FALSE);
                StringCopy(lpwsWeb->dlpszHtmlBuffer[ulIndex], lpcszHtmlBody);
        }
                

        ExitCriticalSection(lpwsWeb->hCritSec);

        /* TODO: Add better returning */
        return bRet;
}





_Success_(return != FALSE, ...)
PODNET_API
BOOL
EmbedLogIntoWebPage(
        _In_                                    HANDLE                  hWebServer,
        _In_                                    HANDLE                  hLog,
        _In_Z_                                  LPCSTR                  lpcszPath
);





_Success_(return != FALSE, ...)
PODNET_API
BOOL
ReloadWebServer(
        _In_                                    HANDLE                  hWebServer
);




_Success_(return != FALSE, ...)
PODNET_API
BOOL
ReCacheWebPage(
        _In_                                    HANDLE                  hWebServer,
        _In_Z_                                  LPCSTR                  lpcszPath
);