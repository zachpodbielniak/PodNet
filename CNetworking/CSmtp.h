/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CSMTP_H
#define CSMTP_H


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../CompilationFlags.h"
#include "../CHandle/CHandle.h"
#include "../CMemory/CMemory.h"
#include "../CMemory/CDynamicHeapRegion.h"
#include "../CString/CString.h"
#include "../CString/CHeapString.h"



typedef enum __SMTP_PROTOCOL
{
	SMTP_PROTOCOL_PLAIN	= 1,
	SMTP_PROTOCOL_SSL,
	SMTP_PROTOCOL_TLS
} SMTP_PROTOCOL;



_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNET_API
HANDLE
CreateSmtpConnection(
	_In_			SMTP_PROTOCOL	spType,
	_In_Z_ 			LPCSTR RESTRICT	lpcszServer,
	_In_Opt_Z_ 		LPCSTR RESTRICT	lpcszUsername,
	_In_Opt_Z_ 		LPCSTR RESTRICT	lpcszPassword,
	_In_			BOOL 		bVerifyCerts,
	_In_ 			BOOL 		bUseIpv6
);




_Success_(return != FALSE, _Interlocked_Operation_)
PODNET_API
BOOL
SendEmail(
	_In_ 			HANDLE 		hSmtpConnection,
	_In_Z_			LPCSTR RESTRICT	lpcszFrom,
	_In_Z_ 			LPCSTR RESTRICT	lpcszTo,
	_In_Z_ 			LPCSTR RESTRICT	lpcszCc,
	_In_Z_ 			LPCSTR RESTRICT	lpcszSubject,
	_In_Z_ 			LPCSTR RESTRICT	lpcszBody
);


#endif