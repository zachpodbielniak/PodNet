/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CSOCKET6_H
#define CSOCKET6_H

#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../Annotation.h"
#include "../CHandle/CHandle.h"
#include "../CThread/CThread.h"
#include "../CSystem/CSystem.h"
#include "CIpv6.h"


#ifndef SOCKET_TYPE
#define SOCKET_TYPE
typedef struct sockaddr                         SOCKET_ADDRESS, *LPSOCKET_ADDRESS, **DLPSOCKET_ADDRESS, ***TLPSOCKET_ADDRESS;
#endif
typedef struct sockaddr_in6                     SOCKET_ADDRESS_IN6, *LPSOCKET_ADDRESS_IN6, **DLPSOCKET_ADDRESS_IN6, ***TLPSOCKET_ADDRESS_IN6;

typedef _Call_Back_ BOOL (* LPFN_CLIENT_CONNECTION_PROC)(
        _In_                            HANDLE                          hHostSocket,
        _In_                            HANDLE                          hClientSocket,
        _In_                            LPVOID                          lpParam,
        _Reserved_Must_Be_Null_         LPVOID                          lpReserved,
        _In_                            UULTRALONG                      uulConnectionCountNumber
);

typedef LPFN_CLIENT_CONNECTION_PROC             *DLPFN_CLIENT_CONNECTION_PROC, **TLPFN_CLIENT_CONNECTION_PROC;





_Result_Null_On_Failure_
PODNET_API
HANDLE
CreateSocket6(
        _In_                            USHORT                          usPort
);




_Result_Null_On_Failure_
PODNET_API
HANDLE
CreateSocketWithAddress6(
        _In_                            USHORT                          usPort,
	_In_ 				LPIPV6_ADDRESS			lpip6Address
);




_Result_Null_On_Failure_
PODNET_API
HANDLE
CreateSocketEx6(
        _In_ 				USHORT 				usPort,
	_In_ 				LPIPV6_ADDRESS			ip6Address,
	_In_ 				LONG 				lDomain,
	_In_ 				LONG 				lType,
	_In_ 				LONG 				lProtocol
);




_Success_(return != FALSE, ...)
PODNET_API
BOOL
DefineSocket6(
        _In_                            HANDLE                          hSocket,
        _In_                            USHORT                          usPort,
        _In_                            LPIPV6_ADDRESS			lpip6Address
);




_Success_(return != FALSE, ...)
PODNET_API
BOOL
BindOnSocket6(
        _In_                            HANDLE                          hSocket
);




_Success_(return != FALSE, ...)
PODNET_API
BOOL
ListenOnBoundSocket6(
        _In_                            HANDLE                          hSocket,
        _In_                            ULONG                           ulMaxNumberOfClientsInWaitingQueue
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
SocketConnect6(
	_In_ 				HANDLE 				hSocket
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
SocketClose6(
	_In_ 				HANDLE 				hSocket
);




_Success_(return != 0, _Non_Locking_)
PODNET_API
UARCHLONG
SocketSend6(
	_In_ 				HANDLE 				hSocket,
	_In_ 				LPVOID 				lpData,
	_In_ 				UARCHLONG 			ualSize
);




_Success_(return != 0, _Non_Locking_)
PODNET_API
UARCHLONG
SocketReceive6(
	_In_ 				HANDLE 				hSocket,
	_Out_ 				LPVOID 				lpOut,
	_In_ 				UARCHLONG 			ualSize
);




_Result_Null_On_Failure_
PODNET_API
HANDLE
AcceptConnection6(
        _In_                            HANDLE                          hSocket
);


/*      
        TODO: Add function that allows an ASYNC call to Accept Connection. 
        Possibly use a PROMISE and FUTURE?
*/



/* 
        Returns HANDLE to Spawned Thread 
        The thread that is spawned from this will automatically destroy its
        HANDLE if it ever reaches the end. That being said, it will be in 
        best practice to close out this HANDLE, no harm in doing so.
*/

_Success_(return != FALSE, _Spawns_New_Thread_Async_Call_)
PODNET_API
HANDLE
ListenOnBoundSocketAndAcceptConnection6(
        _In_                            HANDLE                          hSocket,
        _In_                            ULONG                           ulMaxNumberOfClientsInWaitingQueue,
        _In_                            LPFN_CLIENT_CONNECTION_PROC     lpfnClientConnectionProc,
        _In_Opt_                        LPVOID                          lpParam,
        _Reserved_Must_Be_Null_         LPVOID                          lpReserved,
        _In_Opt_                        UULTRALONG                      uulNumberOfTimesToRecieve                       /* Pass in 0 for infinite loop */
);




_Success_(return != FALSE, ...)
PODNET_API
BOOL
SendToClient6(
        _In_                            HANDLE                          hClientSocket,
        _In_                            LPCVOID                         lpData,
        _In_                            ULONG                           ulSizeOfData
);




_Success_(return != 0, ...)
PODNET_API
BOOL
ReceiveOnSocket6(
        _In_                            HANDLE                          hServerSocket,
        _In_Out_Writes_(*lpOutBuffer)   LPVOID                          lpOutBuffer,                                    /* This CAN NOT be an UNALLOC'D buffer */
        _In_                            ULONG                           ulSizeOfBuffer
);




_Success_(return != 0, _Non_Locking_)
PODNET_API
UARCHLONG
SocketBytesInQueue6(
	_In_ 				HANDLE 				hSocket
);




_Success_(return != 0, _Non_Locking_)
PODNET_API
LPIPV6_ADDRESS
GetSocketIpAddress6(
	_In_ 				HANDLE 				hSocket
);




_Success_(return != 0, _Non_Locking_)
PODNET_API
USHORT
GetSocketPort6(
	_In_ 				HANDLE 				hSocket
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPIPV6_ADDRESS
GetIpAddressByHost6(
	_In_ 				LPCSTR RESTRICT 		lpcszHostname
);




/* Query /proc/net/dev_mcast for interface ID */
_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
JoinMulticastGroup6(
	_In_ 				HANDLE 				hSocket,
	_In_ 				LPIPV6_ADDRESS			lpip6MulticastAddress,
	_In_ 				ULONG				ulInterface
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
LeaveMulticastGroup6(
	_In_ 				HANDLE 				hSocket,
	_In_ 				LPIPV6_ADDRESS			lpip6MulticastAddress,
	_In_ 				ULONG				ulInterface
);












#endif