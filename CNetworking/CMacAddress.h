/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CMACADDRESS_H
#define CMACADDRESS_H


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"



typedef struct __MAC_ADDRESS
{
	union
	{
		ULONGLONG ullData : 48;
		struct
		{ BYTE byData[6]; };
	};
} MAC_ADDRESS, *LPMAC_ADDRESS;



#define MAC_ADDRESS_STRING_LENGTH 		0x12

#define MAC_ADDRESS_EQUAL(X,Y) 			(((X).ullData == (Y).ullData) ? TRUE : FALSE)


_Success_(return != NULL, _Non_Locking_)
PODNET_API
MAC_ADDRESS
CreateMacAddress(
	_In_ 	BYTE 		byFirstOctet,
	_In_ 	BYTE 		bySecondOctet,
	_In_ 	BYTE 		byThirdOctet,
	_In_ 	BYTE 		byFourthOctet,
	_In_ 	BYTE 		byFifthOctet,
	_In_ 	BYTE 		bySixthOctet
);



_Success_(return != NULL, _Non_Locking_)
PODNET_API
MAC_ADDRESS
CreateMacAddressDirect(
	_In_ 	ULONGLONG 	ullData
);



_Success_(return != NULL, _Non_Locking_)
PODNET_API
MAC_ADDRESS
CreateMacAddressFromString(
	_In_Z_ 	LPCSTR 	RESTRICT	lpcszMacAddress
);



_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
MacAddressToString(
	_In_ 	MAC_ADDRESS 		maAddress,
	_Out_ 	LPSTR 	RESTRICT 	lpszOutBuffer,
	_In_ 	ULONG 			ulSizeOfBuffer
);



#endif