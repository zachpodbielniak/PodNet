/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CHttp.h"




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPSTR
StripHttpHeader(
	_In_ 		LPCSTR RESTRICT 	lpcszData
){
	EXIT_IF_UNLIKELY_NULL(lpcszData, NULLPTR);
	LPSTR lpszOut;

	lpszOut = StringInString(lpcszData, "\r\n\r\n");
	
	if (NULLPTR != lpszOut)
	{ lpszOut = (LPVOID)((UARCHLONG)lpszOut + 0x04U); }
	
	return lpszOut;
}


