/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CGEO4_H
#define CGEO4_H



#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "CGeo.h"
#include "CIpv4.h"




_Success_(return != NULLPTR, _Interlocked_Operation_)
PODNET_API
LPGEOIPRECORD
GetRecordByIpv4Address(
	_In_ 		IPV4_ADDRESS		ip4Address
);




_Success_(return != NULLPTR, _Interlocked_Operation_)
PODNET_API
LPGEOIPRECORD
GetRecordByIpv4String(
	_In_Z_ 		LPCSTR 			lpcszAddress
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
DestroyIpv4Record(
	_In_ 		LPGEOIPRECORD 		lpgipRecord
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
DLPSTR 
GetNetRangeByIpv4Address(
	_In_ 		IPV4_ADDRESS 		ip4Address
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
DLPSTR 
GetNetRangeByIpv4String(
	_In_Z_ 		LPCSTR 			lpcszAddress
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
DestroyIpv4NetRange(
	_In_ 		DLPSTR 			dlpszRange
);




_Success_(return != NULLPTR, _Interlocked_Operation_)
PODNET_API
LPCSTR
GetCountryByIpv4Address(
	_In_ 		IPV4_ADDRESS 		ip4Address
);




_Success_(return != NULLPTR, _Interlocked_Operation_)
PODNET_API
LPCSTR
GetCountryByIpv4String(
	_In_Z_ 		LPCSTR 			lpcszAddress
);




_Success_(return != NULLPTR, _Interlocked_Operation_)
PODNET_API
LPCSTR
GetNameByIpv4Address(
	_In_ 		IPV4_ADDRESS		ip4Adress
);




_Success_(return != NULLPTR, _Interlocked_Operation_)
PODNET_API
LPCSTR
GetNameByIpv4String(
	_In_Z_ 		LPCSTR 			lpcszAddress
);




_Success_(return != FALSE, _Interlocked_Operation_)
PODNET_API
BOOL
GetCityByIpv4Address(
	_In_ 		IPV4_ADDRESS		ip4Adress,
	_Out_Z_ 	LPSTR 			lpszOut,
	_In_ 		UARCHLONG 		ualBufferSize
);




_Success_(return != FALSE, _Interlocked_Operation_)
PODNET_API
BOOL
GetCityByIpv4String(
	_In_Z_ 		LPCSTR 			lpcszAddress,
	_Out_Z_ 	LPSTR 			lpszOut,
	_In_ 		UARCHLONG 		ualBufferSize
);







#endif
