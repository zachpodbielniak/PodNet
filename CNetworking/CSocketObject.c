/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CSOCKETOBJECT_C
#define CSOCKETOBJECT_C


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../CHandle/CHandle.h"
#include "../CNetworking/CSocket4.h"
#include "../CNetworking/CSocket6.h"




typedef struct __SOCKET
{
        INHERITS_FROM_HANDLE();
        LONG                    lDescriptor;
        SOCKET_ADDRESS_IN       saSocketAddress;
	LONG 			lDomain;
	LONG 			lType;
	LONG 			lProtocol;
        BOOL                    bBound;
        BOOL                    bListen;
        BOOL                    bIsClient;
	BOOL 			bCreated;
	BOOL			bMultiCast;
	IPV4_ADDRESS		ip4MultiCastGroup;
	IPV4_ADDRESS		ip4MultiCastBind;
        UULTRALONG              uulTotalNumberOfAcceptedConnections;
} SOCKET, *LPSOCKET, **DLPSOCKET, ***TLPSOCKET;




typedef struct __SOCKET6
{
	INHERITS_FROM_HANDLE();
	LONG                    lDescriptor;
	SOCKET_ADDRESS_IN6      saSocketAddress;
	LONG 			lDomain;
	LONG 			lType;
	LONG 			lProtocol;
	BOOL                    bBound;
	BOOL                    bListen;
	BOOL                    bIsClient;
	BOOL 			bCreated;
	BOOL 			bMulticast;
	IPV6_ADDRESS		ip6MulticastAddress;
	ULONG 			ulMulticastInterface;
	UULTRALONG              uulTotalNumberOfAcceptedConnections;
} SOCKET6, *LPSOCKET6, **DLPSOCKET6, ***TLPSOCKET6;




#endif