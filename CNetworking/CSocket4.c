/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CSocket4.h"
#include "CSocketObject.c"




_Success_(return != FALSE, ...)
INTERNAL_OPERATION
BOOL
__DestroySocket4(
        _In_            HDERIVATIVE             hDerivative,
        _In_Opt_        ULONG                   ulFlags
){
        EXIT_IF_UNLIKELY_NULL(hDerivative, FALSE);
        UNREFERENCED_PARAMETER(ulFlags);
        LPSOCKET lpsckSocket = (LPSOCKET)hDerivative;
        if (lpsckSocket->lDescriptor != 0) 
        { close(lpsckSocket->lDescriptor); }
        FreeMemory(lpsckSocket);
        return TRUE;
}




_Result_Null_On_Failure_
_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNET_API
HANDLE
CreateSocket4(
        _In_            USHORT                  usPort
){
        HANDLE hSocket;
        LPSOCKET lpsckSocket;
        ULONGLONG ullID;
	LONG lEnableReuse;
        lpsckSocket = LocalAlloc(sizeof(SOCKET));
        EXIT_IF_NULL(lpsckSocket, NULLPTR);

        ZeroMemory(lpsckSocket, sizeof(SOCKET));

        lpsckSocket->lDescriptor = socket(AF_INET, SOCK_STREAM, 0);
	lpsckSocket->lDomain = AF_INET;	
	lpsckSocket->lType = SOCK_STREAM;
	lpsckSocket->lProtocol = 0;
	lpsckSocket->bCreated = TRUE;
        lpsckSocket->saSocketAddress.sin_family = AF_INET;
        lpsckSocket->saSocketAddress.sin_port = HostToNetworkShort(usPort);
        lpsckSocket->saSocketAddress.sin_addr.s_addr = INADDR_ANY;
        lpsckSocket->bIsClient = TRUE;

	lEnableReuse = 1;
	setsockopt(
		lpsckSocket->lDescriptor,
		SOL_SOCKET,
		SO_REUSEPORT,
		&lEnableReuse,
		sizeof(LONG)
	);

        hSocket = CreateHandleWithSingleInheritor(
                        lpsckSocket,
                        &(lpsckSocket->hThis),
                        HANDLE_TYPE_SOCKET,
                        __DestroySocket4,
                        0,
                        NULLPTR,
                        0,
                        NULLPTR,
                        0,
                        &ullID,
                        0
        );
        EXIT_IF_NULL(hSocket, NULLPTR);

        return hSocket;
};




_Result_Null_On_Failure_
_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNET_API
HANDLE
CreateUdpSocket4(
	_In_ 				USHORT 				usPort
){
        HANDLE hSocket;
        LPSOCKET lpsckSocket;
        ULONGLONG ullID;
	LONG lEnableReuse;
        lpsckSocket = LocalAlloc(sizeof(SOCKET));
        EXIT_IF_NULL(lpsckSocket, NULLPTR);

        ZeroMemory(lpsckSocket, sizeof(SOCKET));

        lpsckSocket->lDescriptor = socket(AF_INET, SOCK_DGRAM, 0);
	lpsckSocket->lDomain = AF_INET;	
	lpsckSocket->lType = SOCK_DGRAM;
	lpsckSocket->lProtocol = 0;
	lpsckSocket->bCreated = TRUE;
        lpsckSocket->saSocketAddress.sin_family = AF_INET;
        lpsckSocket->saSocketAddress.sin_port = HostToNetworkShort(usPort);
        lpsckSocket->saSocketAddress.sin_addr.s_addr = INADDR_ANY;
        lpsckSocket->bIsClient = TRUE;

	lEnableReuse = 1;
	setsockopt(
		lpsckSocket->lDescriptor,
		SOL_SOCKET,
		SO_REUSEPORT,
		&lEnableReuse,
		sizeof(LONG)
	);

        hSocket = CreateHandleWithSingleInheritor(
                        lpsckSocket,
                        &(lpsckSocket->hThis),
                        HANDLE_TYPE_SOCKET,
                        __DestroySocket4,
                        0,
                        NULLPTR,
                        0,
                        NULLPTR,
                        0,
                        &ullID,
                        0
        );
        EXIT_IF_NULL(hSocket, NULLPTR);

        return hSocket;
}




_Result_Null_On_Failure_
_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNET_API
HANDLE
CreateSocketWithAddress4(
        _In_                            USHORT                          usPort,
	_In_ 				IPV4_ADDRESS			ip4Address
){
        HANDLE hSocket;
        LPSOCKET lpsckSocket;
        ULONGLONG ullID;
	LONG lEnableReuse;
        lpsckSocket = LocalAlloc(sizeof(SOCKET));
        EXIT_IF_NULL(lpsckSocket, NULLPTR);

        ZeroMemory(lpsckSocket, sizeof(SOCKET));

        lpsckSocket->lDescriptor = socket(AF_INET, SOCK_STREAM, 0);
	lpsckSocket->lDomain = AF_INET;	
	lpsckSocket->lType = SOCK_STREAM;
	lpsckSocket->lProtocol = 0;
	lpsckSocket->bCreated = TRUE;
        lpsckSocket->saSocketAddress.sin_family = AF_INET;
        lpsckSocket->saSocketAddress.sin_port = HostToNetworkShort(usPort);
        lpsckSocket->saSocketAddress.sin_addr.s_addr = ip4Address.ulData;
        lpsckSocket->bIsClient = TRUE;

	lEnableReuse = 1;
	setsockopt(
		lpsckSocket->lDescriptor,
		SOL_SOCKET,
		SO_REUSEPORT,
		&lEnableReuse,
		sizeof(LONG)
	);

        hSocket = CreateHandleWithSingleInheritor(
                        lpsckSocket,
                        &(lpsckSocket->hThis),
                        HANDLE_TYPE_SOCKET,
                        __DestroySocket4,
                        0,
                        NULLPTR,
                        0,
                        NULLPTR,
                        0,
                        &ullID,
                        0
        );
        EXIT_IF_NULL(hSocket, NULLPTR);

        return hSocket;
};




_Result_Null_On_Failure_
_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNET_API
HANDLE
CreateUdpSocketWithAddress4(
	_In_                            USHORT                          usPort,
	_In_ 				IPV4_ADDRESS			ip4Address
){
        HANDLE hSocket;
        LPSOCKET lpsckSocket;
        ULONGLONG ullID;
	LONG lEnableReuse;
        lpsckSocket = LocalAlloc(sizeof(SOCKET));
        EXIT_IF_NULL(lpsckSocket, NULLPTR);

        ZeroMemory(lpsckSocket, sizeof(SOCKET));

        lpsckSocket->lDescriptor = socket(AF_INET, SOCK_DGRAM, 0);
	lpsckSocket->lDomain = AF_INET;	
	lpsckSocket->lType = SOCK_DGRAM;
	lpsckSocket->lProtocol = 0;
	lpsckSocket->bCreated = TRUE;
        lpsckSocket->saSocketAddress.sin_family = AF_INET;
        lpsckSocket->saSocketAddress.sin_port = HostToNetworkShort(usPort);
        lpsckSocket->saSocketAddress.sin_addr.s_addr = ip4Address.ulData;
        lpsckSocket->bIsClient = TRUE;

	lEnableReuse = 1;
	setsockopt(
		lpsckSocket->lDescriptor,
		SOL_SOCKET,
		SO_REUSEPORT,
		&lEnableReuse,
		sizeof(LONG)
	);

        hSocket = CreateHandleWithSingleInheritor(
                        lpsckSocket,
                        &(lpsckSocket->hThis),
                        HANDLE_TYPE_SOCKET,
                        __DestroySocket4,
                        0,
                        NULLPTR,
                        0,
                        NULLPTR,
                        0,
                        &ullID,
                        0
        );
        EXIT_IF_NULL(hSocket, NULLPTR);

        return hSocket;
}




_Result_Null_On_Failure_
_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNET_API
HANDLE
CreateSocketEx4(
        _In_ 				USHORT 				usPort,
	_In_ 				IPV4_ADDRESS			ip4Address,
	_In_ 				LONG 				lDomain,
	_In_ 				LONG 				lType,
	_In_ 				LONG 				lProtocol
){
        HANDLE hSocket;
        LPSOCKET lpsckSocket;
        ULONGLONG ullID;
	LONG lEnableReuse;
        lpsckSocket = LocalAlloc(sizeof(SOCKET));
        EXIT_IF_NULL(lpsckSocket, NULLPTR);

        ZeroMemory(lpsckSocket, sizeof(SOCKET));

        lpsckSocket->lDescriptor = socket(lDomain, lType, lProtocol);
	lpsckSocket->lDomain = lDomain;	
	lpsckSocket->lType = lType;
	lpsckSocket->lProtocol = lProtocol;
	lpsckSocket->bCreated = TRUE;
        lpsckSocket->saSocketAddress.sin_family = lDomain;
        lpsckSocket->saSocketAddress.sin_port = HostToNetworkShort(usPort);
        lpsckSocket->saSocketAddress.sin_addr.s_addr = ip4Address.ulData;
        lpsckSocket->bIsClient = TRUE;

	lEnableReuse = 1;
	setsockopt(
		lpsckSocket->lDescriptor,
		SOL_SOCKET,
		SO_REUSEPORT,
		&lEnableReuse,
		sizeof(LONG)
	);

        hSocket = CreateHandleWithSingleInheritor(
                        lpsckSocket,
                        &(lpsckSocket->hThis),
                        HANDLE_TYPE_SOCKET,
                        __DestroySocket4,
                        0,
                        NULLPTR,
                        0,
                        NULLPTR,
                        0,
                        &ullID,
                        0
        );
        EXIT_IF_NULL(hSocket, NULLPTR);
	
        return hSocket;
}




_Success_(return != FALSE, ...)
PODNET_API
BOOL
DefineSocket4(
        _In_            HANDLE                  hSocket,
        _In_            USHORT                  usPort,
        _In_            IPV4_ADDRESS		ip4Address
){
        EXIT_IF_UNLIKELY_NULL(hSocket, FALSE);
        EXIT_IF_UNLIKELY_NULL(usPort, FALSE);
        EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hSocket), HANDLE_TYPE_SOCKET, FALSE);

        LPSOCKET lpsckSocket;
        lpsckSocket = (LPSOCKET)GetHandleDerivative(hSocket);
        EXIT_IF_NULL(lpsckSocket, FALSE);

        lpsckSocket->saSocketAddress.sin_port = HostToNetworkShort(usPort);
        lpsckSocket->saSocketAddress.sin_addr.s_addr = ip4Address.ulData;
	
        return TRUE;
}



_Success_(return != FALSE, ...)
PODNET_API
BOOL
BindOnSocket4(
        _In_                            HANDLE                          hSocket
){
        EXIT_IF_UNLIKELY_NULL(hSocket, FALSE);
        EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hSocket), HANDLE_TYPE_SOCKET, FALSE);

        LPSOCKET lpsckSocket;
	LONG lRet;
        lpsckSocket = (LPSOCKET)GetHandleDerivative(hSocket);
        EXIT_IF_UNLIKELY_NULL(lpsckSocket, FALSE);

        lRet = bind(lpsckSocket->lDescriptor, (LPSOCKET_ADDRESS)&(lpsckSocket->saSocketAddress), sizeof(SOCKET_ADDRESS_IN));

        if (0 == lRet)
	{ lpsckSocket->bBound = TRUE; }

        return (0 <= lRet) ? TRUE : FALSE;
}       





_Success_(return != FALSE, ...)
PODNET_API
BOOL
ListenOnBoundSocket4(
        _In_                            HANDLE                          hSocket,
        _In_                            ULONG                           ulMaxNumberOfClientsInWaitingQueue
){
        EXIT_IF_UNLIKELY_NULL(hSocket, FALSE);
        EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hSocket), HANDLE_TYPE_SOCKET, FALSE);

        LPSOCKET lpsckSocket;
        lpsckSocket = (LPSOCKET)GetHandleDerivative(hSocket);
        EXIT_IF_UNLIKELY_NULL(lpsckSocket, FALSE);
        EXIT_IF_NOT_VALUE(lpsckSocket->bBound, TRUE, FALSE);
        EXIT_IF_NOT_VALUE(lpsckSocket->bListen, FALSE, FALSE);

        if (ulMaxNumberOfClientsInWaitingQueue == 0) 
        { ulMaxNumberOfClientsInWaitingQueue = 1; }

        listen(lpsckSocket->lDescriptor, ulMaxNumberOfClientsInWaitingQueue);
        lpsckSocket->bListen = TRUE;

        return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
SocketConnect4(
	_In_ 				HANDLE 				hSocket
){
	EXIT_IF_UNLIKELY_NULL(hSocket, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hSocket), HANDLE_TYPE_SOCKET, FALSE);

	LPSOCKET lpsckSocket;
	LONG lRet;
	lpsckSocket = (LPSOCKET)GetHandleDerivative(hSocket);
	EXIT_IF_UNLIKELY_NULL(lpsckSocket, FALSE);

	if (FALSE == lpsckSocket->bCreated)
	{ 
		lpsckSocket->lDescriptor = socket(AF_INET, SOCK_STREAM, 0); 
		if (0 > lpsckSocket->lDescriptor)
		{ return FALSE; }
		lpsckSocket->bCreated = TRUE;
	}

	lRet = connect(
		lpsckSocket->lDescriptor, 
		(struct sockaddr *)&(lpsckSocket->saSocketAddress), 
		sizeof(lpsckSocket->saSocketAddress)
	);

	return (0 > lRet) ? FALSE : TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
SocketClose4(
	_In_ 				HANDLE 				hSocket
){
	EXIT_IF_UNLIKELY_NULL(hSocket, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hSocket), HANDLE_TYPE_SOCKET, FALSE);

	LPSOCKET lpsckSocket;
	LONG lRet;
	lpsckSocket = (LPSOCKET)GetHandleDerivative(hSocket);
	EXIT_IF_UNLIKELY_NULL(lpsckSocket, FALSE);

	lRet = close(lpsckSocket->lDescriptor);
	lpsckSocket->bCreated = FALSE;

	return (0 == lRet) ? TRUE : FALSE;
}




_Success_(return != 0, _Non_Locking_)
PODNET_API
UARCHLONG
SocketSend4(
	_In_ 				HANDLE 				hSocket,
	_In_ 				LPVOID 				lpData,
	_In_ 				UARCHLONG 			ualSize
){
	EXIT_IF_UNLIKELY_NULL(hSocket, 0);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hSocket), HANDLE_TYPE_SOCKET, 0);

	LPSOCKET lpsckSocket;
	lpsckSocket = (LPSOCKET)GetHandleDerivative(hSocket);
	EXIT_IF_UNLIKELY_NULL(lpsckSocket, 0);

	return (UARCHLONG)send(lpsckSocket->lDescriptor, lpData, ualSize, 0);
}




_Success_(return != 0, _Non_Locking_)
PODNET_API
UARCHLONG
SocketSendTo4(
	_In_ 				HANDLE 				hSocket,
	_In_ 				LPVOID 				lpData,
	_In_ 				UARCHLONG 			ualSize
){
	EXIT_IF_UNLIKELY_NULL(hSocket, 0);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hSocket), HANDLE_TYPE_SOCKET, 0);

	LPSOCKET lpsckSocket;
	lpsckSocket = (LPSOCKET)GetHandleDerivative(hSocket);
	EXIT_IF_UNLIKELY_NULL(lpsckSocket, 0);

	return (UARCHLONG)sendto(
		lpsckSocket->lDescriptor, 
		lpData, 
		ualSize, 
		0, 
		(LPSOCKET_ADDRESS)&(lpsckSocket->saSocketAddress), 
		sizeof(SOCKET_ADDRESS_IN)
	);
}




_Success_(return != 0, _Non_Locking_)
PODNET_API
UARCHLONG
SocketReceive4(
	_In_ 				HANDLE 				hSocket,
	_Out_ 				LPVOID 				lpOut,
	_In_ 				UARCHLONG 			ualSize
){
	EXIT_IF_UNLIKELY_NULL(hSocket, 0);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hSocket), HANDLE_TYPE_SOCKET, 0);

	LPSOCKET lpsckSocket;
	lpsckSocket = (LPSOCKET)GetHandleDerivative(hSocket);
	EXIT_IF_UNLIKELY_NULL(lpsckSocket, 0);

	return (UARCHLONG)recv(lpsckSocket->lDescriptor, lpOut, ualSize, 0);
}




_Result_Null_On_Failure_
PODNET_API
HANDLE
AcceptConnection4(
        _In_                            HANDLE                          hSocket
){
        EXIT_IF_UNLIKELY_NULL(hSocket, NULLPTR);
        EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hSocket), HANDLE_TYPE_SOCKET, NULLPTR);

        HANDLE hClient;
        LPSOCKET lpsckSocket, lpsckClient;
        ULONGLONG ullID;
        lpsckSocket = (LPSOCKET)GetHandleDerivative(hSocket);
        EXIT_IF_UNLIKELY_NULL(lpsckSocket, NULLPTR);
        EXIT_IF_NOT_VALUE(lpsckSocket->bBound, TRUE, NULLPTR);
        EXIT_IF_NOT_VALUE(lpsckSocket->bListen, TRUE, NULLPTR);

        lpsckClient = LocalAlloc(sizeof(SOCKET));
        EXIT_IF_NULL(lpsckClient, 0);
        ZeroMemory(lpsckClient, sizeof(SOCKET));

        lpsckClient->lDescriptor = accept(lpsckSocket->lDescriptor, 0, 0);
        lpsckClient->bIsClient = TRUE;

        hClient = CreateHandleWithSingleInheritor(
                        lpsckClient,
                        &(lpsckSocket->hThis),
                        HANDLE_TYPE_SOCKET,
                        __DestroySocket4,
                        0,
                        NULLPTR,
                        0,
                        NULLPTR,
                        0,
                        &ullID,
                        0
        );
        EXIT_IF_NULL(hClient, NULLPTR);

        return hClient;
}


typedef struct _CLIENT_PROC_PARAMS
{
        HANDLE                          hServerSocket;
        HANDLE                          hThisThread;
        ULONG                           ulMaxNumberOfClientsInWaitingQueue;
        LPFN_CLIENT_CONNECTION_PROC     lpfnClientConnectionProc;
        LPVOID                          lpParam;
        LPVOID                          lpReserved;
        UULTRALONG                      uulNumberOfTimesToRecieve;
} CLIENT_PROC_PARAMS, *LPCLIENT_PROC_PARAMS;



_Thread_Proc_
INTERNAL_OPERATION
LPVOID
__ClientConnectionProc4(
        _In_                            LPVOID                          lpParam
){
        EXIT_IF_NULL(lpParam, NULLPTR);
        HANDLE hClientSocket;
        LPCLIENT_PROC_PARAMS lpcppParams;
        BOOL bRet;
        UULTRALONG uulCount;
        lpcppParams = (LPCLIENT_PROC_PARAMS)lpParam;

        EXIT_IF_NULL(lpcppParams->hServerSocket, NULLPTR);
        EXIT_IF_NULL(lpcppParams->lpfnClientConnectionProc, NULLPTR); /* TODO: Raise Handle Alarm */

        bRet = ListenOnBoundSocket4(lpcppParams->hServerSocket, lpcppParams->ulMaxNumberOfClientsInWaitingQueue);
        
        /* TODO: Add better error handling! Possibly raise handle alarm! */
        if (bRet == FALSE)
        { exit(1); }
        uulCount = 0;
        if (lpcppParams->uulNumberOfTimesToRecieve == 0)
        {
                INFINITE_LOOP_WITH_COUNTER(uulCount)
                {
                        hClientSocket = AcceptConnection4(lpcppParams->hServerSocket);
                        bRet = lpcppParams->lpfnClientConnectionProc(
                                        lpcppParams->hServerSocket,
                                        hClientSocket,
                                        lpcppParams->lpParam,
                                        lpcppParams->lpReserved,
                                        uulCount
                        );
                        if (bRet == FALSE) exit(1);
                }
        }
        else
        {
                for (; uulCount < lpcppParams->uulNumberOfTimesToRecieve; uulCount++)
                {
                        hClientSocket = AcceptConnection4(lpcppParams->hServerSocket);
                        bRet = lpcppParams->lpfnClientConnectionProc(
                                        lpcppParams->hServerSocket,
                                        hClientSocket,
                                        lpcppParams->lpParam,
                                        lpcppParams->lpReserved,
                                        uulCount
                        );
                        if (bRet == FALSE) exit(1);
                }
        }
        Sleep(3000);
        /* DestroyHandle(lpcppParams->hThisThread); */
        KillThread(lpcppParams->hThisThread);
        return NULLPTR;
}




_Success_(return != FALSE, _Spawns_New_Thread_Async_Call_)
PODNET_API
HANDLE
ListenOnBoundSocketAndAcceptConnection4(
        _In_                            HANDLE                          hSocket,
        _In_                            ULONG                           ulMaxNumberOfClientsInWaitingQueue,
        _In_                            LPFN_CLIENT_CONNECTION_PROC     lpfnClientConnectionProc,
        _In_Opt_                        LPVOID                          lpParam,
        _Reserved_Must_Be_Null_         LPVOID                          lpReserved,
        _In_                            UULTRALONG                      uulNumberOfTimesToRecieve                       /* Pass in 0 for infinite loop */
){
        UNREFERENCED_PARAMETER(lpReserved);
        EXIT_IF_UNLIKELY_NULL(hSocket, NULLPTR);
        EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hSocket), HANDLE_TYPE_SOCKET, NULLPTR);

        LPSOCKET lpsckSocket;
        LPCLIENT_PROC_PARAMS lpcppParams;
        lpsckSocket = (LPSOCKET)GetHandleDerivative(hSocket);
        EXIT_IF_NULL(lpsckSocket, NULLPTR);
 
        lpcppParams = LocalAlloc(sizeof(CLIENT_PROC_PARAMS));
	/* cppcheck-suppress memleak */
        EXIT_IF_NULL(lpcppParams, NULLPTR);
	
        ZeroMemory(lpcppParams, sizeof(CLIENT_PROC_PARAMS));

        lpcppParams->hServerSocket = hSocket;
        lpcppParams->ulMaxNumberOfClientsInWaitingQueue = ulMaxNumberOfClientsInWaitingQueue;
        lpcppParams->lpfnClientConnectionProc = lpfnClientConnectionProc;
        lpcppParams->lpParam = lpParam;
        lpcppParams->lpReserved = NULLPTR;
        lpcppParams->uulNumberOfTimesToRecieve = uulNumberOfTimesToRecieve;

        lpcppParams->hThisThread = CreateThread(__ClientConnectionProc4, (LPVOID)lpcppParams, 0);
        
	/* cppcheck-suppress memleak */
        return lpcppParams->hThisThread;
}




_Success_(return != FALSE, ...)
PODNET_API
BOOL
SendToClient4(
        _In_                            HANDLE                          hClientSocket,
        _In_                            LPCVOID                         lpData,
        _In_                            ULONG                           ulSizeOfData
){
        EXIT_IF_UNLIKELY_NULL(hClientSocket, FALSE);
        EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hClientSocket), HANDLE_TYPE_SOCKET, FALSE);

        LPSOCKET lpsckClient;
        lpsckClient = (LPSOCKET)GetHandleDerivative(hClientSocket);
        EXIT_IF_NULL(lpsckClient, FALSE);

        send(lpsckClient->lDescriptor, lpData, ulSizeOfData, 0);
        return TRUE;        
}





_Success_(return != 0, ...)
PODNET_API
BOOL
ReceiveOnSocket4(
        _In_                            HANDLE                          hServerSocket,
        _In_Out_Writes_(*lpOutBuffer)   LPVOID                          lpOutBuffer,           
        _In_                            ULONG                           ulSizeOfBuffer
){
        EXIT_IF_UNLIKELY_NULL(hServerSocket, FALSE);
        EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hServerSocket), HANDLE_TYPE_SOCKET, FALSE);
        EXIT_IF_UNLIKELY_NULL(lpOutBuffer, FALSE);
        EXIT_IF_NULL(ulSizeOfBuffer, FALSE);

        LPSOCKET lpSock;
        lpSock = (LPSOCKET)GetHandleDerivative(hServerSocket);

        recv(lpSock->lDescriptor, lpOutBuffer, ulSizeOfBuffer, 0);        

        return TRUE;
}




_Success_(return != 0, _Non_Locking_)
PODNET_API
UARCHLONG
SocketBytesInQueue4(
	_In_ 				HANDLE 				hSocket
){
        EXIT_IF_UNLIKELY_NULL(hSocket, FALSE);
        EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hSocket), HANDLE_TYPE_SOCKET, FALSE);
	
	LPSOCKET lpsckSocket;
	UARCHLONG ualBytes;
	lpsckSocket = (LPSOCKET)GetHandleDerivative(hSocket);
	ualBytes = 0;

	ioctl(lpsckSocket->lDescriptor, FIONREAD, &ualBytes);

	return ualBytes;
}




_Success_(return != 0, _Non_Locking_)
PODNET_API
IPV4_ADDRESS
GetSocketIpAddress4(
	_In_ 				HANDLE 				hSocket
){
	IPV4_ADDRESS ip4Address;
	ip4Address.ulData = 0;
	EXIT_IF_UNLIKELY_NULL(hSocket, ip4Address);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hSocket), HANDLE_TYPE_SOCKET, ip4Address);

	LPSOCKET lpsckSocket;
	socklen_t sLen;
	struct sockaddr_storage sAddr;
	LPSOCKET_ADDRESS_IN lpS;
	lpsckSocket = (LPSOCKET)GetHandleDerivative(hSocket);
	sLen = sizeof(sAddr);

	getpeername(
		lpsckSocket->lDescriptor,
		(struct sockaddr *)&sAddr, 
		&sLen
	);

	if (AF_INET6 == sAddr.ss_family)
	{ return ip4Address; }

	lpS = (LPSOCKET_ADDRESS_IN)&sAddr;
	ip4Address.ulData = (ULONG)lpS->sin_addr.s_addr;

	return ip4Address;
}




_Success_(return != 0, _Non_Locking_)
PODNET_API
USHORT
GetSocketPort4(
	_In_ 				HANDLE 				hSocket
){
	EXIT_IF_UNLIKELY_NULL(hSocket, 0);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hSocket), HANDLE_TYPE_SOCKET, 0);

	LPSOCKET lpsckSocket;
	socklen_t sLen;
	struct sockaddr_storage sAddr;
	LPSOCKET_ADDRESS_IN lpS;
	lpsckSocket = (LPSOCKET)GetHandleDerivative(hSocket);
	sLen = sizeof(sAddr);

	getpeername(
		lpsckSocket->lDescriptor,
		(struct sockaddr *)&sAddr, 
		&sLen
	);

	if (AF_INET6 == sAddr.ss_family)
	{ return 0; }

	lpS = (LPSOCKET_ADDRESS_IN)&sAddr;

	return NetworkToHostShort(lpS->sin_port);
}




_Success_(return != 0, _Non_Locking_)
PODNET_API
IPV4_ADDRESS
GetIpAddressByHost4(
	_In_ 				LPCSTR RESTRICT 		lpcszHostname
){
	struct hostent *heHostList;
	struct in_addr **iaAddressList;
	ULONG ulIndex;
	IPV4_ADDRESS ip4Address;

	ip4Address.ulData = 0;
	EXIT_IF_UNLIKELY_NULL(lpcszHostname, ip4Address);

	if (NULL == (heHostList = gethostbyname(lpcszHostname)))
	{ return ip4Address; }

	iaAddressList = (struct in_addr **)heHostList->h_addr_list;

	for (
		ulIndex = 0;
		iaAddressList[ulIndex] != NULLPTR;
		ulIndex++
	){	
		/* Just return first one for now */
		ip4Address.ulData = (ULONG)iaAddressList[ulIndex]->s_addr;
		return ip4Address;
	}

	/* Should not happen */
	return ip4Address;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
JoinMulticastGroup4(
	_In_ 				HANDLE 				hSocket,
	_In_ 				IPV4_ADDRESS			ip4MulticastAddress,
	_In_ 				IPV4_ADDRESS			ip4BindingAddress
){
	EXIT_IF_UNLIKELY_NULL(hSocket, FALSE);
	EXIT_IF_UNLIKELY_NULL(ip4MulticastAddress.ulData, FALSE);
	EXIT_IF_UNLIKELY_VALUE(IPV4_ADDRESS_IS_CLASS_D(ip4MulticastAddress), FALSE, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hSocket), HANDLE_TYPE_SOCKET, FALSE);

	LPSOCKET lpsckSocket;
	struct ip_mreq ipmRequest;

	lpsckSocket = (LPSOCKET)GetHandleDerivative(hSocket);
	EXIT_IF_UNLIKELY_NULL(lpsckSocket, FALSE);
	EXIT_IF_NOT_VALUE(lpsckSocket->bMultiCast, FALSE, FALSE);
	EXIT_IF_NOT_VALUE(lpsckSocket->lType, SOCK_DGRAM, FALSE); /* UDP */

	if (0 == ip4BindingAddress.ulData)
	{ ip4BindingAddress.ulData = lpsckSocket->saSocketAddress.sin_addr.s_addr; }

	ipmRequest.imr_multiaddr.s_addr = ip4MulticastAddress.ulData;
	ipmRequest.imr_interface.s_addr = ip4BindingAddress.ulData;

	/* TODO: Add error reporting here */
	if (0 > setsockopt(lpsckSocket->lDescriptor, IPPROTO_IP, IP_ADD_MEMBERSHIP, &ipmRequest, sizeof(ipmRequest)))
	{ return FALSE; }

	lpsckSocket->bMultiCast = TRUE;
	lpsckSocket->ip4MultiCastGroup = ip4MulticastAddress;
	lpsckSocket->ip4MultiCastBind = ip4BindingAddress;

	return TRUE;		
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
LeaveMulticastGroup4(
	_In_ 				HANDLE 				hSocket,
	_In_ 				IPV4_ADDRESS			ip4MulticastAddress,
	_In_ 				IPV4_ADDRESS			ip4BindingAddress
){
	EXIT_IF_UNLIKELY_NULL(hSocket, FALSE);
	EXIT_IF_UNLIKELY_NULL(ip4MulticastAddress.ulData, FALSE);
	EXIT_IF_UNLIKELY_VALUE(IPV4_ADDRESS_IS_CLASS_D(ip4MulticastAddress), FALSE, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hSocket), HANDLE_TYPE_SOCKET, FALSE);

	LPSOCKET lpsckSocket;
	struct ip_mreq ipmRequest;

	lpsckSocket = (LPSOCKET)GetHandleDerivative(hSocket);
	EXIT_IF_UNLIKELY_NULL(lpsckSocket, FALSE);
	EXIT_IF_NOT_VALUE(lpsckSocket->bMultiCast, TRUE, FALSE);
	EXIT_IF_NOT_VALUE(lpsckSocket->lType, SOCK_DGRAM, FALSE); /* UDP */

	if (0 == ip4BindingAddress.ulData)
	{ ip4BindingAddress.ulData = lpsckSocket->saSocketAddress.sin_addr.s_addr; }

	ipmRequest.imr_multiaddr.s_addr = ip4MulticastAddress.ulData;
	ipmRequest.imr_interface.s_addr = ip4BindingAddress.ulData;

	/* TODO: Add error reporting here */
	if (0 > setsockopt(lpsckSocket->lDescriptor, IPPROTO_IP, IP_DROP_MEMBERSHIP, &ipmRequest, sizeof(ipmRequest)))
	{ return FALSE; }

	lpsckSocket->bMultiCast = FALSE;
	lpsckSocket->ip4MultiCastGroup.ulData = 0;
	lpsckSocket->ip4MultiCastBind.ulData = 0;

	return TRUE;		
}