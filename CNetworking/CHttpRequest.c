/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CHttpRequest.h"
#include "../CompilationFlags.h"


typedef struct __HTTP_CALLBACK_DATA
{
	HANDLE 			hParent;
	HANDLE 			hDynamicRegion;
	UARCHLONG 		ualLength;
} HTTP_CALLBACK_DATA, *LPHTTP_CALLBACK_DATA;




typedef struct __HTTP_REQUEST
{
	INHERITS_FROM_HANDLE();
	HTTP_REQUEST_TYPE	htrtType;
	HTTP_VERSION 		htvVersion;
	HANDLE 			hEvent;
	CSTRING 		csUrl[HTTP_REQUEST_BUFFER_SIZE];
	LPSTR 			lpszAccept;
	LPSTR 			lpszConnection;
	LPSTR 			lpszUserAgent;
	USHORT 			usPort;
	HTTP_CALLBACK_DATA	hcdResponse;
	HVECTOR 		hvHeaders;	/* Response Headers */
	UARCHLONG 		ualResponseCode;

	DLPSTR 			dlpszHeaders;
	UARCHLONG 		ualNumberOfHeaders;
	
	DLPSTR 			dlpszPostData;
	UARCHLONG 		ualNumberOfPostEntries;

	#ifdef BUILD_WITH_ONEAGENT_SDK
	onesdk_tracer_handle_t	othOutgoingWebRequest;
	#endif

	CURL 			*cCtx;
	struct curl_slist 	*cslHeaders;
	CURLcode 		ccRet;
} HTTP_REQUEST, *LPHTTP_REQUEST;





_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__DestroyHttpRequest(
	_In_ 		HDERIVATIVE 		hHttpRequest,
	_In_Opt_	ULONG 			ulFlags
){
	UNREFERENCED_PARAMETER(ulFlags);
	EXIT_IF_UNLIKELY_NULL(hHttpRequest, FALSE);

	LPHTTP_REQUEST lphrData;
	lphrData = (LPHTTP_REQUEST)hHttpRequest;

	curl_slist_free_all(lphrData->cslHeaders);
	curl_easy_cleanup(lphrData->cCtx);

	if (NULL_OBJECT != lphrData->hcdResponse.hDynamicRegion)
	{ DestroyObject(lphrData->hcdResponse.hDynamicRegion); }

	DestroyObject(lphrData->hEvent);

	if (NULLPTR != lphrData->lpszAccept)
	{ FreeMemory(lphrData->lpszAccept); }

	if (NULLPTR != lphrData->lpszConnection)
	{ FreeMemory(lphrData->lpszConnection); }

	if (NULLPTR != lphrData->lpszUserAgent)
	{ FreeMemory(lphrData->lpszUserAgent); }

	if (NULLPTR != lphrData->hvHeaders)
	{
		UARCHLONG ualIndex;
		for (
			ualIndex = 0;
			ualIndex < VectorSize(lphrData->hvHeaders);
			ualIndex++
		){
			LPHTTP_REQUEST_HEADER lphrhData;
			lphrhData = VectorAt(lphrData->hvHeaders, ualIndex);

			if (NULLPTR == lphrhData)
			{ continue; }

			if (NULLPTR != lphrhData->lpszHeader)
			{ FreeMemory(lphrhData->lpszHeader); }

			if (NULLPTR != lphrhData->lpszValue)
			{ FreeMemory(lphrhData->lpszValue); }
		}

		DestroyVector(lphrData->hvHeaders);
	}

	FreeMemory(lphrData);
	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__WaitForHttpRequest(
	_In_ 		HANDLE 			hHttpRequest,
	_In_ 		ULONGLONG		ullMilliSeconds
){
	EXIT_IF_UNLIKELY_NULL(hHttpRequest, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hHttpRequest), HANDLE_TYPE_HTTP_REQUEST, FALSE);

	LPHTTP_REQUEST lphrData;
	lphrData = (LPHTTP_REQUEST)GetHandleDerivative(hHttpRequest);
	EXIT_IF_UNLIKELY_NULL(lphrData, FALSE);

	return WaitForSingleObject(lphrData->hEvent, ullMilliSeconds);
}



_Call_Back_
_Success_(return != 0, _Non_Locking_)
INTERNAL_OPERATION
ARCHLONG
__HttpWriteCallback(
	_In_		LPVOID			lpSlice,
	_In_ 		ARCHLONG		alIgnore,
	_In_ 		ARCHLONG 		alSize,
	_In_Opt_ 	LPVOID 			lpData
){
	UNREFERENCED_PARAMETER(alIgnore);
	EXIT_IF_UNLIKELY_NULL(lpData, 0);

	LPHTTP_CALLBACK_DATA lphcdData;
	lphcdData = (LPHTTP_CALLBACK_DATA)lpData;

	if (NULLPTR == lphcdData->hDynamicRegion)
	{ lphcdData->hDynamicRegion = CreateDynamicHeapRegion(NULL_OBJECT, (UARCHLONG)alSize, FALSE, 0); }

	DynamicHeapRegionAppend(lphcdData->hDynamicRegion, lpSlice, (UARCHLONG)alSize);
	return alSize;
}




_Call_Back_
_Success_(return != 0, _Non_Locking_)
INTERNAL_OPERATION
ARCHLONG
__HeaderWriteCallback(
	_In_		LPVOID			lpSlice,
	_In_ 		ARCHLONG		alIgnore,
	_In_ 		ARCHLONG 		alSize,
	_In_Opt_ 	LPVOID 			lpData
){
	UNREFERENCED_PARAMETER(alIgnore);
	EXIT_IF_UNLIKELY_NULL(lpData, 0);

	LPHTTP_REQUEST lphrData;
	HTTP_REQUEST_HEADER hrhData;
	CSTRING csBuffer[0x400];
	LPSTR lpszIndex;
	UARCHLONG ualLength;

	lphrData = lpData;
	ZeroMemory(&hrhData, sizeof(hrhData));
	ZeroMemory(csBuffer, sizeof(csBuffer));

	if (NULLPTR == lphrData->hvHeaders)
	{ lphrData->hvHeaders = CreateVector(0x08, sizeof(HTTP_REQUEST_HEADER), 0); }

	StringCopySafe(csBuffer, (LPCSTR)lpSlice, sizeof(csBuffer) - 0x01U);

	lpszIndex = StringInString(csBuffer, ":");
	if (NULLPTR == lpszIndex)
	{ return alSize; }

	*lpszIndex = '\0';
	lpszIndex = (LPSTR)((UARCHLONG)lpszIndex + 0x01U);

	while (*lpszIndex == ' ')
	{ lpszIndex = (LPSTR)((UARCHLONG)lpszIndex + 0x01U); }

	ualLength = StringLength(lpszIndex);
	lpszIndex[ualLength-2] = '\0';

	hrhData.lpszHeader = StringDuplicate((LPCSTR)csBuffer);
	hrhData.lpszValue = StringDuplicate((LPCSTR)lpszIndex);

	VectorPushBack(lphrData->hvHeaders, &hrhData, 0);

	#ifdef BUILD_WITH_ONEAGENT_SDK
	onesdk_outgoingwebrequesttracer_add_response_header(
		lphrData->othOutgoingWebRequest,
		onesdk_asciistr(hrhData.lpszHeader),
		onesdk_asciistr(hrhData.lpszValue)
	);
	#endif

	return alSize;
}




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNET_API
HANDLE
CreateHttpRequest(
	_In_ 		HTTP_REQUEST_TYPE 	htrtType,
	_In_ 		HTTP_VERSION 		htvVersion,
	_In_Z_ 		LPCSTR RESTRICT		lpcszHost,
	_In_ 		USHORT 			usPort,
	_In_Z_ 		LPCSTR RESTRICT		lpcszPath,
	_In_Z_ 		LPCSTR RESTRICT		lpcszUserAgent,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszAccept,
	_In_Opt_ 	BOOL 			bUseIpv6
){
	EXIT_IF_UNLIKELY_NULL(htrtType, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NULL(htvVersion, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NULL(lpcszHost, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NULL(lpcszPath, NULL_OBJECT);

	HANDLE hHttpRequest;
	LPHTTP_REQUEST lphrData;

	CSTRING csUrl[0x2000];
	CSTRING csUserAgent[0x400];
	CSTRING csAccept[0x400];
	


	ZeroMemory(csUrl, sizeof(csUrl));
	ZeroMemory(csUserAgent, sizeof(csUserAgent));
	ZeroMemory(csAccept, sizeof(csAccept));

	lphrData = GlobalAllocAndZero(sizeof(HTTP_REQUEST));
	if (NULLPTR == lphrData)
	{ return NULL_OBJECT; }
	
	lphrData->htrtType = htrtType;

	StringPrintFormatSafe(
		csUrl,
		sizeof(csUrl) - 1,
		"http://%s%s",
		lpcszHost,
		lpcszPath
	);

	if (NULLPTR != lpcszUserAgent)
	{
		StringPrintFormatSafe(
			csUserAgent,
			sizeof(csUserAgent) - 1,
			"%s: %s",
			"User-Agent",
			lpcszUserAgent
		);
		
		lphrData->lpszUserAgent = StringDuplicate(csAccept);
	}

	if (NULLPTR != lpcszAccept)
	{
		StringPrintFormatSafe(
			csAccept,
			sizeof(csAccept) - 1,
			"%s: %s",
			"Accept",
			lpcszAccept
		);
		
		lphrData->lpszAccept = StringDuplicate(csAccept);
	}

	StringCopySafe(
		lphrData->csUrl,
		csUrl,
		sizeof(lphrData->csUrl) - 0x01U
	);


	/* Begin CURL Stuff */
	lphrData->cCtx = curl_easy_init();

	curl_easy_setopt(lphrData->cCtx, CURLOPT_CUSTOMREQUEST, HTTP_REQUEST_TYPE_TO_STRING(htrtType));
	curl_easy_setopt(lphrData->cCtx, CURLOPT_URL, csUrl);
	curl_easy_setopt(lphrData->cCtx, CURLOPT_PORT, usPort);
	curl_easy_setopt(lphrData->cCtx, CURLOPT_WRITEFUNCTION, __HttpWriteCallback);
	curl_easy_setopt(lphrData->cCtx, CURLOPT_WRITEDATA, &(lphrData->hcdResponse)); 
	curl_easy_setopt(lphrData->cCtx, CURLOPT_HEADERFUNCTION, __HeaderWriteCallback);
	curl_easy_setopt(lphrData->cCtx, CURLOPT_HEADERDATA, lphrData);

	if (FALSE == bUseIpv6)
	{ curl_easy_setopt(lphrData->cCtx, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4); }
	else
	{ curl_easy_setopt(lphrData->cCtx, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V6); }
	
	hHttpRequest = CreateHandleWithSingleInheritor(
		lphrData,
		&(lphrData->hThis),
		HANDLE_TYPE_HTTP_REQUEST,
		__DestroyHttpRequest,
		0,
		NULLPTR,
		0,
		NULLPTR,
		0,
		&(lphrData->ullId),
		0
	);

	SetHandleEventWaitFunction(hHttpRequest, __WaitForHttpRequest, 0);

	return hHttpRequest;
}




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNET_API
HANDLE 
CreateHttpRequestEx(
	_In_ 		HTTP_REQUEST_TYPE 	htrtType,
	_In_		HTTP_VERSION 		htvVersion,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszHost,
	_In_ 		USHORT 			usPort,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszPath,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszConnectionOption,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszUserAgent,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszAccept,
	_In_ 		DLPCSTR RESTRICT 	dlpcszHeaders,
	_In_ 		UARCHLONG		ualNumberOfHeaders,
	_In_Opt_ 	DLPCSTR RESTRICT 	dlpcszPostData,
	_In_Opt_ 	UARCHLONG 		ualNumberOfPostEntries,
	_In_		BOOL 			bUseIpv6
){
	EXIT_IF_UNLIKELY_NULL(htrtType, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NULL(htvVersion, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NULL(lpcszHost, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NULL(lpcszPath, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NULL(lpcszConnectionOption, NULL_OBJECT);

	HANDLE hHttpsRequest;
	LPHTTP_REQUEST lphrData;

	CSTRING csUrl[0x2000];
	CSTRING csUserAgent[0x400];
	CSTRING csAccept[0x400];
	CSTRING csConnection[0x400];
	CSTRING csOption[0x400];

	ZeroMemory(csUrl, sizeof(csUrl));
	ZeroMemory(csUserAgent, sizeof(csUserAgent));
	ZeroMemory(csAccept, sizeof(csAccept));
	ZeroMemory(csConnection, sizeof(csConnection));
	ZeroMemory(csOption, sizeof(csOption));

	lphrData = GlobalAllocAndZero(sizeof(HTTP_REQUEST));
	if (NULLPTR == lphrData)
	{ return NULL_OBJECT; }

	lphrData->htrtType = htrtType;
	lphrData->dlpszHeaders = (DLPSTR)dlpcszHeaders;
	lphrData->ualNumberOfHeaders = ualNumberOfHeaders;
	lphrData->dlpszPostData = (DLPSTR)dlpcszPostData;
	lphrData->ualNumberOfPostEntries = ualNumberOfPostEntries;

	StringPrintFormatSafe(
		csUrl,
		sizeof(csUrl) - 1,
		"http://%s:%hu%s",
		lpcszHost,
		usPort,
		lpcszPath
	);

	StringPrintFormatSafe(
		csConnection,
		sizeof(csConnection) - 1,
		"Connection: %s",
		lpcszConnectionOption
	);

	lphrData->lpszConnection = StringDuplicate(csConnection);

	if (NULLPTR != lpcszUserAgent)
	{
		StringPrintFormatSafe(
			csUserAgent,
			sizeof(csUserAgent) - 1,
			"%s: %s",
			"User-Agent",
			lpcszUserAgent
		);
		
		lphrData->lpszUserAgent = StringDuplicate(csAccept);
	}

	if (NULLPTR != lpcszAccept)
	{
		StringPrintFormatSafe(
			csAccept,
			sizeof(csAccept) - 1,
			"%s: %s",
			"Accept",
			lpcszAccept
		);
		
		lphrData->lpszAccept = StringDuplicate(csAccept);
	}

	StringCopySafe(
		lphrData->csUrl,
		csUrl,
		sizeof(lphrData->csUrl) - 0x01U
	);

	/* Begin CURL Stuff */
	lphrData->cCtx = curl_easy_init();

	curl_easy_setopt(lphrData->cCtx, CURLOPT_CUSTOMREQUEST, HTTP_REQUEST_TYPE_TO_STRING(htrtType));
	curl_easy_setopt(lphrData->cCtx, CURLOPT_URL, csUrl);
	curl_easy_setopt(lphrData->cCtx, CURLOPT_PORT, usPort);
	curl_easy_setopt(lphrData->cCtx, CURLOPT_WRITEFUNCTION, __HttpWriteCallback);
	curl_easy_setopt(lphrData->cCtx, CURLOPT_WRITEDATA, &(lphrData->hcdResponse)); 
	curl_easy_setopt(lphrData->cCtx, CURLOPT_HEADERFUNCTION, __HeaderWriteCallback);
	curl_easy_setopt(lphrData->cCtx, CURLOPT_HEADERDATA, lphrData);

	if (FALSE == bUseIpv6)
	{ curl_easy_setopt(lphrData->cCtx, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4); }
	else
	{ curl_easy_setopt(lphrData->cCtx, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V6); }
	
	hHttpsRequest = CreateHandleWithSingleInheritor(
		lphrData,
		&(lphrData->hThis),
		HANDLE_TYPE_HTTP_REQUEST,
		__DestroyHttpRequest,
		0,
		NULLPTR,
		0,
		NULLPTR,
		0,
		&(lphrData->ullId),
		0
	);

	SetHandleEventWaitFunction(hHttpsRequest, __WaitForHttpRequest, 0);
	
	return hHttpsRequest;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
ExecuteHttpRequest(
	_In_ 		HANDLE 			hHttpRequest,
	_Out_ 		LPVOID 			lpOut,
	_In_ 		UARCHLONG 		ualOutSize,
	_Out_ 		LPUARCHLONG 		lpualBytesWritten
){
	EXIT_IF_UNLIKELY_NULL(hHttpRequest, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpOut, FALSE);
	EXIT_IF_UNLIKELY_NULL(ualOutSize, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hHttpRequest), HANDLE_TYPE_HTTP_REQUEST, FALSE);

	LPHTTP_REQUEST lphrData;
	UARCHLONG ualSize, ualLength;
	LPVOID lpData;
	UARCHLONG ualIndex;

	lphrData = OBJECT_CAST(hHttpRequest, LPHTTP_REQUEST);

	#ifdef BUILD_WITH_ONEAGENT_SDK
	LPSTR lpszIndex;
	CSTRING csTrace[0x100];
	CSTRING csHeaderSplit[0x400];

	lphrData->othOutgoingWebRequest = onesdk_outgoingwebrequesttracer_create(
		onesdk_asciistr(lphrData->csUrl),
		onesdk_asciistr(HTTP_REQUEST_TYPE_TO_STRING(lphrData->htrtType))
	);
	
	ZeroMemory(csTrace, sizeof(csTrace));

	onesdk_tracer_get_outgoing_dynatrace_string_tag(
		lphrData->othOutgoingWebRequest,
		csTrace,
		sizeof(csTrace) - 0x01U,
		NULLPTR
	);

	if ('\0' != csTrace[0])
	{
		onesdk_outgoingwebrequesttracer_add_request_header(
			lphrData->othOutgoingWebRequest,
			onesdk_asciistr(ONESDK_DYNATRACE_HTTP_HEADER_NAME),
			onesdk_asciistr(csTrace)
		);
	}

	if (NULLPTR != lphrData->lpszUserAgent)
	{
		onesdk_outgoingwebrequesttracer_add_request_header(
			lphrData->othOutgoingWebRequest,
			onesdk_asciistr("User-Agent"),
			onesdk_asciistr(lphrData->lpszUserAgent)
		);
	}

	if (NULLPTR != lphrData->lpszAccept)
	{
		onesdk_outgoingwebrequesttracer_add_request_header(
			lphrData->othOutgoingWebRequest,
			onesdk_asciistr("Accept"),
			onesdk_asciistr(lphrData->lpszAccept)
		);
	}

	if (NULLPTR != lphrData->lpszConnection)
	{
		onesdk_outgoingwebrequesttracer_add_request_header(
			lphrData->othOutgoingWebRequest,
			onesdk_asciistr("Connection"),
			onesdk_asciistr(lphrData->lpszConnection)
		);
	}

	if (NULLPTR != lphrData->dlpszHeaders)
	{
		for (
			ualIndex = 0;
			ualIndex < lphrData->ualNumberOfHeaders;
			ualIndex++
		){
			ZeroMemory(csHeaderSplit, sizeof(csHeaderSplit));
			StringCopySafe(csHeaderSplit, lphrData->dlpszHeaders[ualIndex], sizeof(csHeaderSplit) - 0x01U);

			lpszIndex = StringInString(csHeaderSplit, ":");
			if (NULLPTR == lpszIndex)
			{ continue; }

			*lpszIndex = '\0';
			lpszIndex = (LPSTR)((UARCHLONG)lpszIndex + 0x01U);

			while (*lpszIndex == ' ')
			{ lpszIndex = (LPSTR)((UARCHLONG)lpszIndex + 0x01U); }

			onesdk_outgoingwebrequesttracer_add_request_header(
				lphrData->othOutgoingWebRequest,
				onesdk_asciistr(csHeaderSplit),
				onesdk_asciistr(lpszIndex)
			);
		}
	}
	
	onesdk_tracer_start(lphrData->othOutgoingWebRequest);
	#endif
	
	if (NULLPTR != lphrData->cslHeaders)
	{ 
		curl_slist_free_all(lphrData->cslHeaders); 
		lphrData->cslHeaders = NULLPTR;
	}

	/* Create The HTTP Headers */
	if (NULLPTR != lphrData->lpszUserAgent)
	{ lphrData->cslHeaders = curl_slist_append(lphrData->cslHeaders, lphrData->lpszUserAgent); }
	if (NULLPTR != lphrData->lpszAccept)
	{ lphrData->cslHeaders = curl_slist_append(lphrData->cslHeaders, lphrData->lpszAccept); }
	if (NULLPTR != lphrData->lpszConnection)
	{ lphrData->cslHeaders = curl_slist_append(lphrData->cslHeaders, lphrData->lpszConnection); }

	#ifdef BUILD_WITH_ONEAGENT_SDK
	CSTRING csTag[0x200];
	ZeroMemory(csTag, sizeof(csTag));

	StringPrintFormatSafe(
		csTag,
		sizeof(csTag) - 0x01U,
		"%s: %s",
		ONESDK_DYNATRACE_HTTP_HEADER_NAME,
		csTrace
	);

	if ('\0' != csTrace[0])
	{ lphrData->cslHeaders = curl_slist_append(lphrData->cslHeaders, csTag); }
	#endif
	
	if (NULLPTR != lphrData->dlpszHeaders)
	{
		for (
			ualIndex = 0;
			ualIndex < lphrData->ualNumberOfHeaders;
			ualIndex++
		){ lphrData->cslHeaders = curl_slist_append(lphrData->cslHeaders, lphrData->dlpszHeaders[ualIndex]); }
	}
	
	if (HTTP_REQUEST_POST == lphrData->htrtType)
	{
		if (NULLPTR != lphrData->dlpszPostData)
		{
			for (
				ualIndex = 0;
				ualIndex < lphrData->ualNumberOfPostEntries;
				ualIndex++
			){ curl_easy_setopt(lphrData->cCtx, CURLOPT_POSTFIELDS, lphrData->dlpszPostData[ualIndex]); }
		}
	}

	if (NULL_OBJECT != lphrData->hcdResponse.hDynamicRegion)
	{
		DestroyObject(lphrData->hcdResponse.hDynamicRegion);
		lphrData->hcdResponse.hDynamicRegion = NULL_OBJECT;
	}

	if (NULLPTR != lphrData->hvHeaders)
	{
		for (
			ualIndex = 0;
			ualIndex < VectorSize(lphrData->hvHeaders);
			ualIndex++
		){
			LPHTTP_REQUEST_HEADER lphrhData;
			lphrhData = VectorAt(lphrData->hvHeaders, ualIndex);

			if (NULLPTR == lphrhData)
			{ continue; }

			if (NULLPTR != lphrhData->lpszHeader)
			{ FreeMemory(lphrhData->lpszHeader); }

			if (NULLPTR != lphrhData->lpszValue)
			{ FreeMemory(lphrhData->lpszValue); }
		}

		lphrData->hvHeaders = NULLPTR;
	}

	curl_easy_setopt(lphrData->cCtx, CURLOPT_HTTPHEADER, lphrData->cslHeaders);
	lphrData->ccRet = curl_easy_perform(lphrData->cCtx);
	
	if (CURLE_OK != lphrData->ccRet)
	{ return FALSE; }

	curl_easy_getinfo(lphrData->cCtx, CURLINFO_RESPONSE_CODE, &(lphrData->ualResponseCode));

	#ifdef BUILD_WITH_ONEAGENT_SDK
	onesdk_outgoingwebrequesttracer_set_status_code(
		lphrData->othOutgoingWebRequest,
		(onesdk_int32_t)lphrData->ualResponseCode
	);
	#endif

	lpData = DynamicHeapRegionData(lphrData->hcdResponse.hDynamicRegion, &ualLength);
	if (NULLPTR == lpData)
	{ return FALSE; }
	ualSize = (ualOutSize < ualLength) ? ualOutSize : ualLength;

	CopyMemory(
		lpOut,
		lpData,
		ualSize
	);

	if (NULLPTR != lpualBytesWritten)
	{ *lpualBytesWritten = ualSize; }
	
	#ifdef BUILD_WITH_ONEAGENT_SDK
	onesdk_tracer_end(lphrData->othOutgoingWebRequest);
	lphrData->othOutgoingWebRequest = ONESDK_INVALID_HANDLE;
	#endif

	SignalEvent(lphrData->hEvent);

	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API 
BOOL 
ExecuteHttpRequestEx(
	_In_ 		HANDLE 			hHttpRequest,
	_Out_Opt_ 	LPUARCHLONG 		lpualResponseCode,
	_Out_ 		LPVOID 			lpDataOut,
	_In_ 		UARCHLONG		ualOutSize,
	_Out_Opt_	LPUARCHLONG 		lpualContentLength,
	_Out_Opt_	LPHVECTOR		lphvHeaders,
	_Reserved_	LPVOID 			lpReserved,
	_In_Opt_	ULONG 			ulFlags
){
	UNREFERENCED_PARAMETER(lpReserved);
	UNREFERENCED_PARAMETER(ulFlags);

	EXIT_IF_UNLIKELY_NULL(hHttpRequest, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpDataOut, FALSE);
	EXIT_IF_UNLIKELY_NULL(ualOutSize, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hHttpRequest), HANDLE_TYPE_HTTP_REQUEST, FALSE);

	LPHTTP_REQUEST lphrData;
	UARCHLONG ualSize, ualLength;
	UARCHLONG ualIndex;
	LPVOID lpData;
	LPVOID lpBody;

	lphrData = OBJECT_CAST(hHttpRequest, LPHTTP_REQUEST);

	#ifdef BUILD_WITH_ONEAGENT_SDK
	LPSTR lpszIndex;
	CSTRING csTrace[0x100];
	CSTRING csHeaderSplit[0x400];

	lphrData->othOutgoingWebRequest = onesdk_outgoingwebrequesttracer_create(
		onesdk_asciistr(lphrData->csUrl),
		onesdk_asciistr(HTTP_REQUEST_TYPE_TO_STRING(lphrData->htrtType))
	);
	
	ZeroMemory(csTrace, sizeof(csTrace));

	onesdk_tracer_get_outgoing_dynatrace_string_tag(
		lphrData->othOutgoingWebRequest,
		csTrace,
		sizeof(csTrace) - 0x01U,
		NULLPTR
	);

	if ('\0' != csTrace[0])
	{
		onesdk_outgoingwebrequesttracer_add_request_header(
			lphrData->othOutgoingWebRequest,
			onesdk_asciistr(ONESDK_DYNATRACE_HTTP_HEADER_NAME),
			onesdk_asciistr(csTrace)
		);
	}

	if (NULLPTR != lphrData->lpszUserAgent)
	{
		onesdk_outgoingwebrequesttracer_add_request_header(
			lphrData->othOutgoingWebRequest,
			onesdk_asciistr("User-Agent"),
			onesdk_asciistr(lphrData->lpszUserAgent)
		);
	}

	if (NULLPTR != lphrData->lpszAccept)
	{
		onesdk_outgoingwebrequesttracer_add_request_header(
			lphrData->othOutgoingWebRequest,
			onesdk_asciistr("Accept"),
			onesdk_asciistr(lphrData->lpszAccept)
		);
	}

	if (NULLPTR != lphrData->lpszConnection)
	{
		onesdk_outgoingwebrequesttracer_add_request_header(
			lphrData->othOutgoingWebRequest,
			onesdk_asciistr("Connection"),
			onesdk_asciistr(lphrData->lpszConnection)
		);
	}

	if (NULLPTR != lphrData->dlpszHeaders)
	{
		for (
			ualIndex = 0;
			ualIndex < lphrData->ualNumberOfHeaders;
			ualIndex++
		){
			ZeroMemory(csHeaderSplit, sizeof(csHeaderSplit));
			StringCopySafe(csHeaderSplit, lphrData->dlpszHeaders[ualIndex], sizeof(csHeaderSplit) - 0x01U);

			lpszIndex = StringInString(csHeaderSplit, ":");
			if (NULLPTR == lpszIndex)
			{ continue; }

			*lpszIndex = '\0';
			lpszIndex = (LPSTR)((UARCHLONG)lpszIndex + 0x01U);

			while (*lpszIndex == ' ')
			{ lpszIndex = (LPSTR)((UARCHLONG)lpszIndex + 0x01U); }

			onesdk_outgoingwebrequesttracer_add_request_header(
				lphrData->othOutgoingWebRequest,
				onesdk_asciistr(csHeaderSplit),
				onesdk_asciistr(lpszIndex)
			);
		}
	}
	
	onesdk_tracer_start(lphrData->othOutgoingWebRequest);
	#endif
	
	if (NULLPTR != lphrData->cslHeaders)
	{ 
		curl_slist_free_all(lphrData->cslHeaders); 
		lphrData->cslHeaders = NULLPTR;
	}

	/* Create The HTTP Headers */
	if (NULLPTR != lphrData->lpszUserAgent)
	{ lphrData->cslHeaders = curl_slist_append(lphrData->cslHeaders, lphrData->lpszUserAgent); }
	if (NULLPTR != lphrData->lpszAccept)
	{ lphrData->cslHeaders = curl_slist_append(lphrData->cslHeaders, lphrData->lpszAccept); }
	if (NULLPTR != lphrData->lpszConnection)
	{ lphrData->cslHeaders = curl_slist_append(lphrData->cslHeaders, lphrData->lpszConnection); }

	#ifdef BUILD_WITH_ONEAGENT_SDK
	CSTRING csTag[0x200];
	ZeroMemory(csTag, sizeof(csTag));

	StringPrintFormatSafe(
		csTag,
		sizeof(csTag) - 0x01U,
		"%s: %s",
		ONESDK_DYNATRACE_HTTP_HEADER_NAME,
		csTrace
	);

	if ('\0' != csTrace[0])
	{ lphrData->cslHeaders = curl_slist_append(lphrData->cslHeaders, csTag); }
	#endif
	
	if (NULLPTR != lphrData->dlpszHeaders)
	{
		for (
			ualIndex = 0;
			ualIndex < lphrData->ualNumberOfHeaders;
			ualIndex++
		){ lphrData->cslHeaders = curl_slist_append(lphrData->cslHeaders, lphrData->dlpszHeaders[ualIndex]); }
	}
	
	if (HTTP_REQUEST_POST == lphrData->htrtType)
	{
		if (NULLPTR != lphrData->dlpszPostData)
		{
			for (
				ualIndex = 0;
				ualIndex < lphrData->ualNumberOfPostEntries;
				ualIndex++
			){ curl_easy_setopt(lphrData->cCtx, CURLOPT_POSTFIELDS, lphrData->dlpszPostData[ualIndex]); }
		}
	}

	if (NULL_OBJECT != lphrData->hcdResponse.hDynamicRegion)
	{
		DestroyObject(lphrData->hcdResponse.hDynamicRegion);
		lphrData->hcdResponse.hDynamicRegion = NULL_OBJECT;
	}
	
	if (NULLPTR != lphrData->hvHeaders)
	{
		for (
			ualIndex = 0;
			ualIndex < VectorSize(lphrData->hvHeaders);
			ualIndex++
		){
			LPHTTP_REQUEST_HEADER lphrhData;
			lphrhData = VectorAt(lphrData->hvHeaders, ualIndex);

			if (NULLPTR == lphrhData)
			{ continue; }

			if (NULLPTR != lphrhData->lpszHeader)
			{ FreeMemory(lphrhData->lpszHeader); }

			if (NULLPTR != lphrhData->lpszValue)
			{ FreeMemory(lphrhData->lpszValue); }
		}
		
		lphrData->hvHeaders = NULLPTR;
	}

	curl_easy_setopt(lphrData->cCtx, CURLOPT_HTTPHEADER, lphrData->cslHeaders);
	lphrData->ccRet = curl_easy_perform(lphrData->cCtx);
	
	if (CURLE_OK != lphrData->ccRet)
	{ return FALSE; }

	curl_easy_getinfo(lphrData->cCtx, CURLINFO_RESPONSE_CODE, &(lphrData->ualResponseCode));

	#ifdef BUILD_WITH_ONEAGENT_SDK
	onesdk_outgoingwebrequesttracer_set_status_code(
		lphrData->othOutgoingWebRequest,
		(onesdk_int32_t)lphrData->ualResponseCode
	);
	#endif

	if (NULLPTR != lpualResponseCode)
	{ *lpualResponseCode = lphrData->ualResponseCode; }

	lpData = DynamicHeapRegionData(lphrData->hcdResponse.hDynamicRegion, &ualLength);
	if (NULLPTR == lpData)
	{ return FALSE; }

	/* Get The Body */
	lpBody = StripHttpHeader(lpData);
	ualSize = ualLength - ((UARCHLONG)lpBody - (UARCHLONG)lpData);
	ualSize = (ualOutSize < ualSize) ? ualOutSize : ualSize;

	CopyMemory(
		lpDataOut,
		lpBody,
		ualSize
	);

	if (NULLPTR != lpualContentLength)
	{ *lpualContentLength = ualSize; }
	
	if (NULLPTR != lphvHeaders)
	{ *lphvHeaders = lphrData->hvHeaders; }

	#ifdef BUILD_WITH_ONEAGENT_SDK
	onesdk_tracer_end(lphrData->othOutgoingWebRequest);
	lphrData->othOutgoingWebRequest = ONESDK_INVALID_HANDLE;
	#endif

	SignalEvent(lphrData->hEvent);

	return TRUE;
}
