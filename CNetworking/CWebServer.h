/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CWEBSERVER_H
#define CWEBSERVER_H


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../Annotation.h"
#include "../CHandle/CHandle.h"
#include "../CCritSec/CCritSec.h"
#include "../CThread/CThread.h"
#include "../CFile/CFile.h"
#include "CSocket.h"


#define WEBSERVER_DESTROY_CLOSE_FILE_HANDLES            1 << 0


_Result_Null_On_Failure_
PODNET_API
HANDLE
CreateWebServer(
        _In_                                    ULONG                   ulPort,
        _In_Opt_                                HANDLE                  hIndexPage            /* If NULL serves up memory */
);



_Success_(return != FALSE, ...)
PODNET_API
BOOL
DefineWebPage(
        _In_                                    HANDLE                  hWebServer,
        _In_Z_                                  LPCSTR                  lpcszPath,
        _In_Opt_ _When_(lpcszHtmlBody == NULL)  HANDLE                  hFile,
        _In_Opt_Z_ _When_(hFile == NULL)        LPCSTR                  lpcszHtmlBody,
        _In_Opt_ _When_(hFile == NULL)          ULONG                   ulSizeOfHtmlBody,
        _Reserved_Must_Be_Null_                 LPVOID                  lpReserved
);


_Success_(return != FALSE, ...)
PODNET_API
BOOL
EmbedLogIntoWebPage(
        _In_                                    HANDLE                  hWebServer,
        _In_                                    HANDLE                  hLog,
        _In_Z_                                  LPCSTR                  lpcszPath
);


_Success_(return != FALSE, ...)
PODNET_API
BOOL
ReloadWebServer(
        _In_                                    HANDLE                  hWebServer
);


_Success_(return != FALSE, ...)
PODNET_API
BOOL
ReCacheWebPage(
        _In_                                    HANDLE                  hWebServer,
        _In_Z_                                  LPCSTR                  lpcszPath
);



#endif