/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CIpv4.h"
#include "../CompilationFlags.h"





_Success_(return != NULL, _Non_Locking_)
PODNET_API
IPV4_ADDRESS
CreateIpv4Address(
	_In_ 	BYTE 		byFirstOctet,
	_In_ 	BYTE 		bySecondOctet,
	_In_ 	BYTE 		byThirdOctet,
	_In_ 	BYTE 		byFourthOctet
){
	/* cppcheck-suppress uninitvar */
	IPV4_ADDRESS ipv4Ret;
	ipv4Ret.byData[0] = byFirstOctet;
	ipv4Ret.byData[1] = bySecondOctet;
	ipv4Ret.byData[2] = byThirdOctet;
	ipv4Ret.byData[3] = byFourthOctet;
	return ipv4Ret;
}




_Success_(return != NULL, _Non_Locking_)
PODNET_API
IPV4_ADDRESS
CreateIpv4AddressFromString(
	_In_Z_ 	LPCSTR 	RESTRICT 	lpcszIpAddress
){
	IPV4_ADDRESS ipv4Ret;
	StringScanFormat(
		lpcszIpAddress,
		"%hhu.%hhu.%hhu.%hhu",
		/* cppcheck-suppress uninitvar */
		&(ipv4Ret.byData[0]),
		/* cppcheck-suppress uninitvar */
		&(ipv4Ret.byData[1]),
		/* cppcheck-suppress uninitvar */
		&(ipv4Ret.byData[2]),
		/* cppcheck-suppress uninitvar */
		&(ipv4Ret.byData[3])
	);
	/* cppcheck-suppress uninitvar */
	return ipv4Ret;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL 
ConvertIpv4AddressToString(
	_In_ 	IPV4_ADDRESS 		ipv4Address,
	_Out_ 	LPSTR 	RESTRICT 	lpszBufferOut,
	_In_ 	ULONG 			ulBufferSize
){
	EXIT_IF_CONDITION(IPV4_ADDRESS_STRING_LENGTH > ulBufferSize, FALSE);
	StringPrintFormatSafe(
		lpszBufferOut,
		ulBufferSize,
		"%hhu.%hhu.%hhu.%hhu",
		ipv4Address.byData[0],
		ipv4Address.byData[1],
		ipv4Address.byData[2],
		ipv4Address.byData[3]
	);
	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
IPV4_ADDRESS_CLASS
GetIpv4AddressClass(
	_In_ 	IPV4_ADDRESS 		ipv4Address
){
	if (TRUE == IPV4_ADDRESS_IS_CLASS_A(ipv4Address))
	{ return IPV4_CLASS_A; }
	
	if (TRUE == IPV4_ADDRESS_IS_CLASS_B(ipv4Address))
	{ return IPV4_CLASS_B; }
	
	if (TRUE == IPV4_ADDRESS_IS_CLASS_C(ipv4Address))
	{ return IPV4_CLASS_C; }
	
	if (TRUE == IPV4_ADDRESS_IS_CLASS_D(ipv4Address))
	{ return IPV4_CLASS_D; }
	
	if (TRUE == IPV4_ADDRESS_IS_CLASS_E(ipv4Address))
	{ return IPV4_CLASS_E; }

	return IPV4_CLASS_UNKNOWN;
}




_Success_(return != 0, _Non_Locking_)
PODNET_API
IPV4_ADDRESS
GetCurrentPublicIpv4Address(
	VOID
){
	HANDLE hRequest;
	CSTRING csResponse[4096];
	CSTRING csBuffer[32];
	LPSTR lpszOut;

	ZeroMemory(csResponse, sizeof(csResponse));
	ZeroMemory(csBuffer, sizeof(csBuffer));
	lpszOut = (LPSTR)csResponse;

	hRequest = CreateHttpsRequest(
		HTTP_REQUEST_GET,
		HTTP_VERSION_1_1,
		HTTP_GET_IPV4_SERVER,
		443,
		"/",
		HTTP_CONNECTION_CLOSE,
		NULLPTR,
		HTTP_ACCEPT_PLAIN_TEXT,
		FALSE,
		TRUE
	);

	ExecuteHttpsRequest(hRequest, csResponse, sizeof(csResponse), NULLPTR);
	CopyMemory(csBuffer, lpszOut, StringLength(lpszOut) - 0x01U);

	DestroyObject(hRequest);
	return CreateIpv4AddressFromString(csBuffer);
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
IsIpv4Address(
	_In_Z_	LPCSTR RESTRICT		lpcszIpAddress
){
	EXIT_IF_UNLIKELY_NULL(lpcszIpAddress, FALSE);
	BOOL bRet;
	bRet = TRUE;

	if (NULLPTR != StringInString(lpcszIpAddress, ":"))
	{ bRet = FALSE; }
	
	return bRet;
}