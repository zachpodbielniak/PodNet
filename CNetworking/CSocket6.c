/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CSocket6.h"
#include "CSocketObject.c"





_Success_(return != FALSE, ...)
INTERNAL_OPERATION
BOOL
__DestroySocket6(
	_In_            HDERIVATIVE             hDerivative,
	_In_Opt_        ULONG                   ulFlags
){
	EXIT_IF_UNLIKELY_NULL(hDerivative, FALSE);
	UNREFERENCED_PARAMETER(ulFlags);
	LPSOCKET6 lpsckSocket = (LPSOCKET6)hDerivative;
	if (lpsckSocket->lDescriptor != 0) 
	{ close(lpsckSocket->lDescriptor); }
	FreeMemory(lpsckSocket);
	return TRUE;
}




_Result_Null_On_Failure_
PODNET_API
HANDLE
CreateSocket6(
	_In_            USHORT                  usPort
){
	HANDLE hSocket;
	LPSOCKET6 lpsckSocket;
	ULONGLONG ullID;
	LONG lEnableReuse;

	lpsckSocket = LocalAlloc(sizeof(SOCKET6));
	EXIT_IF_NULL(lpsckSocket, NULLPTR);

	ZeroMemory(lpsckSocket, sizeof(SOCKET6));

	lpsckSocket->lDescriptor = socket(AF_INET6, SOCK_STREAM, 0);
	lpsckSocket->lDomain = AF_INET6;	
	lpsckSocket->lType = SOCK_STREAM;
	lpsckSocket->lProtocol = 0;
	lpsckSocket->bCreated = TRUE;
	lpsckSocket->saSocketAddress.sin6_family = AF_INET6;
	lpsckSocket->saSocketAddress.sin6_port = HostToNetworkShort(usPort);
	lpsckSocket->saSocketAddress.sin6_addr = in6addr_any;
	lpsckSocket->bIsClient = TRUE;

	lEnableReuse = 1;
	setsockopt(
		lpsckSocket->lDescriptor,
		SOL_SOCKET,
		SO_REUSEPORT,
		&lEnableReuse,
		sizeof(LONG)
	);

	hSocket = CreateHandleWithSingleInheritor(
			lpsckSocket,
			&(lpsckSocket->hThis),
			HANDLE_TYPE_SOCKET6,
			__DestroySocket6,
			0,
			NULLPTR,
			0,
			NULLPTR,
			0,
			&ullID,
			0
	);
	EXIT_IF_NULL(hSocket, NULLPTR);

	return hSocket;
};




_Result_Null_On_Failure_
PODNET_API
HANDLE
CreateSocketWithAddress6(
	_In_                            USHORT                          usPort,
	_In_ 				LPIPV6_ADDRESS			lpip6Address
){
	HANDLE hSocket;
	LPSOCKET6 lpsckSocket;
	ULONGLONG ullID;
	LONG lEnableReuse;
	lpsckSocket = LocalAlloc(sizeof(SOCKET6));
	EXIT_IF_NULL(lpsckSocket, NULLPTR);

	ZeroMemory(lpsckSocket, sizeof(SOCKET6));

	lpsckSocket->lDescriptor = socket(AF_INET6, SOCK_STREAM, 0);
	lpsckSocket->lDomain = AF_INET6;	
	lpsckSocket->lType = SOCK_STREAM;
	lpsckSocket->lProtocol = 0;
	lpsckSocket->bCreated = TRUE;
	lpsckSocket->saSocketAddress.sin6_family = AF_INET6;
	lpsckSocket->saSocketAddress.sin6_port = HostToNetworkShort(usPort);
	lpsckSocket->saSocketAddress.sin6_addr = *(struct in6_addr *)lpip6Address;
	lpsckSocket->bIsClient = TRUE;

	lEnableReuse = 1;
	setsockopt(
		lpsckSocket->lDescriptor,
		SOL_SOCKET,
		SO_REUSEPORT,
		&lEnableReuse,
		sizeof(LONG)
	);

	hSocket = CreateHandleWithSingleInheritor(
			lpsckSocket,
			&(lpsckSocket->hThis),
			HANDLE_TYPE_SOCKET6,
			__DestroySocket6,
			0,
			NULLPTR,
			0,
			NULLPTR,
			0,
			&ullID,
			0
	);
	EXIT_IF_NULL(hSocket, NULLPTR);

	return hSocket;
};




_Result_Null_On_Failure_
PODNET_API
HANDLE
CreateSocketEx6(
	_In_ 				USHORT 				usPort,
	_In_ 				LPIPV6_ADDRESS			lpip6Address,
	_In_ 				LONG 				lDomain,
	_In_ 				LONG 				lType,
	_In_ 				LONG 				lProtocol
){
	HANDLE hSocket;
	LPSOCKET6 lpsckSocket;
	ULONGLONG ullID;
	LONG lEnableReuse;
	lpsckSocket = LocalAlloc(sizeof(SOCKET6));
	EXIT_IF_NULL(lpsckSocket, NULLPTR);

	ZeroMemory(lpsckSocket, sizeof(SOCKET6));

	lpsckSocket->lDescriptor = socket(lDomain, lType, lProtocol);
	lpsckSocket->lDomain = lDomain;	
	lpsckSocket->lType = lType;
	lpsckSocket->lProtocol = lProtocol;
	lpsckSocket->bCreated = TRUE;
	lpsckSocket->saSocketAddress.sin6_family = lDomain;
	lpsckSocket->saSocketAddress.sin6_port = HostToNetworkShort(usPort);
	lpsckSocket->saSocketAddress.sin6_addr = *(struct in6_addr *)lpip6Address;
	lpsckSocket->bIsClient = TRUE;

	lEnableReuse = 1;
	setsockopt(
		lpsckSocket->lDescriptor,
		SOL_SOCKET,
		SO_REUSEPORT,
		&lEnableReuse,
		sizeof(LONG)
	);

	hSocket = CreateHandleWithSingleInheritor(
			lpsckSocket,
			&(lpsckSocket->hThis),
			HANDLE_TYPE_SOCKET6,
			__DestroySocket6,
			0,
			NULLPTR,
			0,
			NULLPTR,
			0,
			&ullID,
			0
	);
	EXIT_IF_NULL(hSocket, NULLPTR);
	
	return hSocket;
}




_Success_(return != FALSE, ...)
PODNET_API
BOOL
DefineSocket6(
	_In_            HANDLE                  hSocket,
	_In_            USHORT                  usPort,
	_In_            LPIPV6_ADDRESS		lpip6Address
){
	EXIT_IF_UNLIKELY_NULL(hSocket, FALSE);
	EXIT_IF_UNLIKELY_NULL(usPort, FALSE);
	EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hSocket), HANDLE_TYPE_SOCKET6, FALSE);

	LPSOCKET6 lpsckSocket;
	lpsckSocket = (LPSOCKET6)GetHandleDerivative(hSocket);
	EXIT_IF_NULL(lpsckSocket, FALSE);

	lpsckSocket->saSocketAddress.sin6_port = HostToNetworkShort(usPort);
	lpsckSocket->saSocketAddress.sin6_addr = *(struct in6_addr *)lpip6Address;
	
	return TRUE;
}




_Success_(return != FALSE, ...)
PODNET_API
BOOL
BindOnSocket6(
	_In_                            HANDLE                          hSocket
){
	EXIT_IF_UNLIKELY_NULL(hSocket, FALSE);
	EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hSocket), HANDLE_TYPE_SOCKET6, FALSE);

	LPSOCKET6 lpsckSocket;
	LONG lRet;
	lpsckSocket = (LPSOCKET6)GetHandleDerivative(hSocket);
	EXIT_IF_UNLIKELY_NULL(lpsckSocket, FALSE);

	if (TRUE == lpsckSocket->bBound)
	{ return FALSE; }

	lRet = bind(
		lpsckSocket->lDescriptor, 
		(LPSOCKET_ADDRESS)&(lpsckSocket->saSocketAddress), 
		sizeof(SOCKET_ADDRESS_IN6)
	);

	if (0 == lRet)
	{
		lpsckSocket->bBound = TRUE;
		return TRUE;
	}

	return FALSE;
}       




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
ListenOnBoundSocket6(
	_In_		HANDLE		hSocket,
	_In_		ULONG 		ulMaxNumberOfClientsInWaitingQueue
){
	EXIT_IF_UNLIKELY_NULL(hSocket, FALSE);
	EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hSocket), HANDLE_TYPE_SOCKET6, FALSE);

	LPSOCKET6 lpsckSocket;
	lpsckSocket = (LPSOCKET6)GetHandleDerivative(hSocket);
	EXIT_IF_UNLIKELY_NULL(lpsckSocket, FALSE);
	EXIT_IF_NOT_VALUE(lpsckSocket->bBound, TRUE, FALSE);
	EXIT_IF_NOT_VALUE(lpsckSocket->bListen, FALSE, FALSE);

	if (ulMaxNumberOfClientsInWaitingQueue == 0) 
	{ ulMaxNumberOfClientsInWaitingQueue = 1; }

	listen(lpsckSocket->lDescriptor, ulMaxNumberOfClientsInWaitingQueue);
	lpsckSocket->bListen = TRUE;

	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
SocketConnect6(
	_In_ 		HANDLE 		hSocket
){
	EXIT_IF_UNLIKELY_NULL(hSocket, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hSocket), HANDLE_TYPE_SOCKET6, FALSE);

	LPSOCKET6 lpsckSocket;
	LONG lRet;
	lpsckSocket = (LPSOCKET6)GetHandleDerivative(hSocket);
	EXIT_IF_UNLIKELY_NULL(lpsckSocket, FALSE);

	if (FALSE == lpsckSocket->bCreated)
	{ 
		lpsckSocket->lDescriptor = socket(AF_INET, SOCK_STREAM, 0); 
		if (0 > lpsckSocket->lDescriptor)
		{ return FALSE; }
		lpsckSocket->bCreated = TRUE;
	}

	lRet = connect(
		lpsckSocket->lDescriptor, 
		(struct sockaddr *)&(lpsckSocket->saSocketAddress), 
		sizeof(lpsckSocket->saSocketAddress)
	);

	return (0 > lRet) ? FALSE : TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
SocketClose6(
	_In_ 		HANDLE		hSocket
){
	EXIT_IF_UNLIKELY_NULL(hSocket, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hSocket), HANDLE_TYPE_SOCKET6, FALSE);

	LPSOCKET6 lpsckSocket;
	LONG lRet;
	lpsckSocket = (LPSOCKET6)GetHandleDerivative(hSocket);
	EXIT_IF_UNLIKELY_NULL(lpsckSocket, FALSE);

	lRet = close(lpsckSocket->lDescriptor);
	lpsckSocket->bCreated = FALSE;

	return (0 == lRet) ? TRUE : FALSE;
}




_Success_(return != 0, _Non_Locking_)
PODNET_API
UARCHLONG
SocketSend6(
	_In_ 		HANDLE 		hSocket,
	_In_ 		LPVOID 		lpData,
	_In_ 		UARCHLONG	ualSize
){
	EXIT_IF_UNLIKELY_NULL(hSocket, 0);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hSocket), HANDLE_TYPE_SOCKET6, 0);

	LPSOCKET6 lpsckSocket;
	lpsckSocket = (LPSOCKET6)GetHandleDerivative(hSocket);
	EXIT_IF_UNLIKELY_NULL(lpsckSocket, 0);

	return (UARCHLONG)send(lpsckSocket->lDescriptor, lpData, ualSize, 0);
}




_Success_(return != 0, _Non_Locking_)
PODNET_API
UARCHLONG
SocketReceive6(
	_In_ 		HANDLE 		hSocket,
	_Out_ 		LPVOID 		lpOut,
	_In_ 		UARCHLONG 	ualSize
){
	EXIT_IF_UNLIKELY_NULL(hSocket, 0);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hSocket), HANDLE_TYPE_SOCKET6, 0);

	LPSOCKET6 lpsckSocket;
	lpsckSocket = (LPSOCKET6)GetHandleDerivative(hSocket);
	EXIT_IF_UNLIKELY_NULL(lpsckSocket, 0);

	return (UARCHLONG)recv(lpsckSocket->lDescriptor, lpOut, ualSize, 0);
}




_Result_Null_On_Failure_
PODNET_API
HANDLE
AcceptConnection6(
	_In_ 		HANDLE		hSocket
){
	EXIT_IF_UNLIKELY_NULL(hSocket, NULLPTR);
	EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hSocket), HANDLE_TYPE_SOCKET6, NULLPTR);

	HANDLE hClient;
	LPSOCKET6 lpsckSocket, lpsckClient;
	ULONGLONG ullID;
	lpsckSocket = (LPSOCKET6)GetHandleDerivative(hSocket);
	EXIT_IF_UNLIKELY_NULL(lpsckSocket, NULLPTR);
	EXIT_IF_NOT_VALUE(lpsckSocket->bBound, TRUE, NULLPTR);
	EXIT_IF_NOT_VALUE(lpsckSocket->bListen, TRUE, NULLPTR);

	lpsckClient = LocalAlloc(sizeof(SOCKET6));
	EXIT_IF_NULL(lpsckClient, 0);
	ZeroMemory(lpsckClient, sizeof(SOCKET6));

	lpsckClient->lDescriptor = accept(lpsckSocket->lDescriptor, 0, 0);
	lpsckClient->bIsClient = TRUE;

	hClient = CreateHandleWithSingleInheritor(
			lpsckClient,
			&(lpsckSocket->hThis),
			HANDLE_TYPE_SOCKET6,
			__DestroySocket6,
			0,
			NULLPTR,
			0,
			NULLPTR,
			0,
			&ullID,
			0
	);
	EXIT_IF_NULL(hClient, NULLPTR);

	return hClient;
}




typedef struct _CLIENT_PROC_PARAMS
{
	HANDLE                          hServerSocket;
	HANDLE                          hThisThread;
	ULONG                           ulMaxNumberOfClientsInWaitingQueue;
	LPFN_CLIENT_CONNECTION_PROC     lpfnClientConnectionProc;
	LPVOID                          lpParam;
	LPVOID                          lpReserved;
	UULTRALONG                      uulNumberOfTimesToRecieve;
} CLIENT_PROC_PARAMS, *LPCLIENT_PROC_PARAMS;




_Thread_Proc_
INTERNAL_OPERATION
LPVOID
__ClientConnectionProc6(
	_In_		LPVOID		lpParam
){
	EXIT_IF_NULL(lpParam, NULLPTR);
	HANDLE hClientSocket;
	LPCLIENT_PROC_PARAMS lpcppParams;
	BOOL bRet;
	UULTRALONG uulCount;
	lpcppParams = (LPCLIENT_PROC_PARAMS)lpParam;

	EXIT_IF_NULL(lpcppParams->hServerSocket, NULLPTR);
	EXIT_IF_NULL(lpcppParams->lpfnClientConnectionProc, NULLPTR); /* TODO: Raise Handle Alarm */

	bRet = ListenOnBoundSocket6(lpcppParams->hServerSocket, lpcppParams->ulMaxNumberOfClientsInWaitingQueue);
	
	/* TODO: Add better error handling! Possibly raise handle alarm! */
	if (bRet == FALSE)
	{ exit(1); }
	uulCount = 0;
	if (lpcppParams->uulNumberOfTimesToRecieve == 0)
	{
		INFINITE_LOOP_WITH_COUNTER(uulCount)
		{
			hClientSocket = AcceptConnection6(lpcppParams->hServerSocket);
			bRet = lpcppParams->lpfnClientConnectionProc(
					lpcppParams->hServerSocket,
					hClientSocket,
					lpcppParams->lpParam,
					lpcppParams->lpReserved,
					uulCount
			);
			if (bRet == FALSE) exit(1);
		}
	}
	else
	{
		for (; uulCount < lpcppParams->uulNumberOfTimesToRecieve; uulCount++)
		{
			hClientSocket = AcceptConnection6(lpcppParams->hServerSocket);
			bRet = lpcppParams->lpfnClientConnectionProc(
					lpcppParams->hServerSocket,
					hClientSocket,
					lpcppParams->lpParam,
					lpcppParams->lpReserved,
					uulCount
			);
			if (bRet == FALSE) exit(1);
		}
	}
	Sleep(3000);
	/* DestroyHandle(lpcppParams->hThisThread); */
	KillThread(lpcppParams->hThisThread);
	return NULLPTR;
}




_Success_(return != FALSE, _Spawns_New_Thread_Async_Call_)
PODNET_API
HANDLE
ListenOnBoundSocketAndAcceptConnection6(
	_In_                    	HANDLE                          hSocket,
	_In_                   		ULONG                           ulMaxNumberOfClientsInWaitingQueue,
	_In_                    	LPFN_CLIENT_CONNECTION_PROC     lpfnClientConnectionProc,
	_In_Opt_                	LPVOID                          lpParam,
	_Reserved_Must_Be_Null_ 	LPVOID                          lpReserved,
	_In_                    	UULTRALONG                      uulNumberOfTimesToRecieve                       /* Pass in 0 for infinite loop */
){
	UNREFERENCED_PARAMETER(lpReserved);
	EXIT_IF_UNLIKELY_NULL(hSocket, NULLPTR);
	EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hSocket), HANDLE_TYPE_SOCKET6, NULLPTR);

	LPSOCKET6 lpsckSocket;
	LPCLIENT_PROC_PARAMS lpcppParams;
	lpsckSocket = (LPSOCKET6)GetHandleDerivative(hSocket);
	EXIT_IF_NULL(lpsckSocket, NULLPTR);
	lpcppParams = LocalAlloc(sizeof(CLIENT_PROC_PARAMS));
	EXIT_IF_NULL(lpcppParams, NULLPTR);
	ZeroMemory(lpcppParams, sizeof(CLIENT_PROC_PARAMS));

	lpcppParams->hServerSocket = hSocket;
	lpcppParams->ulMaxNumberOfClientsInWaitingQueue = ulMaxNumberOfClientsInWaitingQueue;
	lpcppParams->lpfnClientConnectionProc = lpfnClientConnectionProc;
	lpcppParams->lpParam = lpParam;
	lpcppParams->lpReserved = NULLPTR;
	lpcppParams->uulNumberOfTimesToRecieve = uulNumberOfTimesToRecieve;

	lpcppParams->hThisThread = CreateThread(__ClientConnectionProc6, (LPVOID)lpcppParams, 0);
	
	return lpcppParams->hThisThread;
}




_Success_(return != FALSE, ...)
PODNET_API
BOOL
SendToClient6(
	_In_                            HANDLE                          hClientSocket,
	_In_                            LPCVOID                         lpData,
	_In_                            ULONG                           ulSizeOfData
){
	EXIT_IF_UNLIKELY_NULL(hClientSocket, FALSE);
	EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hClientSocket), HANDLE_TYPE_SOCKET6, FALSE);

	LPSOCKET6 lpsckClient;
	lpsckClient = (LPSOCKET6)GetHandleDerivative(hClientSocket);
	EXIT_IF_NULL(lpsckClient, FALSE);

	send(lpsckClient->lDescriptor, lpData, ulSizeOfData, 0);

	return TRUE;        
}




_Success_(return != 0, ...)
PODNET_API
BOOL
ReceiveOnSocket6(
	_In_                            HANDLE                          hServerSocket,
	_In_Out_Writes_(*lpOutBuffer)   LPVOID                          lpOutBuffer,           
	_In_                            ULONG                           ulSizeOfBuffer
){
	EXIT_IF_UNLIKELY_NULL(hServerSocket, FALSE);
	EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hServerSocket), HANDLE_TYPE_SOCKET6, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpOutBuffer, FALSE);
	EXIT_IF_NULL(ulSizeOfBuffer, FALSE);

	LPSOCKET6 lpSock;
	lpSock = (LPSOCKET6)GetHandleDerivative(hServerSocket);

	recv(lpSock->lDescriptor, lpOutBuffer, ulSizeOfBuffer, 0);        

	return TRUE;
}




_Success_(return != 0, _Non_Locking_)
PODNET_API
UARCHLONG
SocketBytesInQueue6(
	_In_ 				HANDLE 				hSocket
){
        EXIT_IF_UNLIKELY_NULL(hSocket, FALSE);
        EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hSocket), HANDLE_TYPE_SOCKET6, 0);
	
	LPSOCKET6 lpsckSocket;
	UARCHLONG ualBytes;
	lpsckSocket = (LPSOCKET6)GetHandleDerivative(hSocket);
	ualBytes = 0;

	ioctl(lpsckSocket->lDescriptor, FIONREAD, &ualBytes);

	return ualBytes;
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPIPV6_ADDRESS
GetSocketIpAddress6(
	_In_ 				HANDLE 				hSocket
){
	LPIPV6_ADDRESS lpip6Address;
	EXIT_IF_UNLIKELY_NULL(hSocket, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hSocket), HANDLE_TYPE_SOCKET6, NULLPTR);

	LPSOCKET6 lpsckSocket;
	socklen_t sLen;
	struct sockaddr_storage sAddr;
	LPSOCKET_ADDRESS_IN6 lpS;
	lpsckSocket = (LPSOCKET6)GetHandleDerivative(hSocket);
	sLen = sizeof(sAddr);

	getpeername(
		lpsckSocket->lDescriptor,
		(struct sockaddr *)&sAddr, 
		&sLen
	);

	if (AF_INET == sAddr.ss_family)
	{ return NULLPTR; }

	lpS = (LPSOCKET_ADDRESS_IN6)&sAddr;

	lpip6Address = CreateIpv6AddressFromString("::");

	#ifdef __FreeBSD__
	lpip6Address->ultuData = *(LPULTRALONG_UNION)&(lpS->sin6_addr.__u6_addr);
	#else
	lpip6Address->ultuData = *(LPULTRALONG_UNION)&(lpS->sin6_addr.__in6_u);
	#endif

	return lpip6Address;
}




_Success_(return != 0, _Non_Locking_)
PODNET_API
USHORT
GetSocketPort6(
	_In_ 				HANDLE 				hSocket
){
	EXIT_IF_UNLIKELY_NULL(hSocket, 0);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hSocket), HANDLE_TYPE_SOCKET6, 0);

	LPSOCKET6 lpsckSocket;
	socklen_t sLen;
	struct sockaddr_storage sAddr;
	LPSOCKET_ADDRESS_IN6 lpS;
	lpsckSocket = (LPSOCKET6)GetHandleDerivative(hSocket);
	sLen = sizeof(sAddr);

	getpeername(
		lpsckSocket->lDescriptor,
		(struct sockaddr *)&sAddr, 
		&sLen
	);

	if (AF_INET == sAddr.ss_family)
	{ return 0; }

	lpS = (LPSOCKET_ADDRESS_IN6)&sAddr;

	return NetworkToHostShort(lpS->sin6_port);
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPIPV6_ADDRESS
GetIpAddressByHost6(
	_In_ 				LPCSTR RESTRICT 		lpcszHostname
){
	EXIT_IF_UNLIKELY_NULL(lpcszHostname, NULLPTR);
	struct hostent *heHostList;
	struct in6_addr **iaAddressList;
	LPIPV6_ADDRESS lpip6Address;
	ULONG ulIndex;

	if (NULL == (heHostList = gethostbyname(lpcszHostname)))
	{ return NULLPTR; }

	iaAddressList = (struct in6_addr **)heHostList->h_addr_list;
	lpip6Address = CreateIpv6AddressFromString("::");
	
	for (
		ulIndex = 0;
		iaAddressList[ulIndex] != NULLPTR;
		ulIndex++
	){	
		/* Just return first one for now */
		#ifdef __FreeBSD__
		lpip6Address->ultuData = *(LPULTRALONG_UNION)&(iaAddressList[ulIndex]->__u6_addr);
		#else
		lpip6Address->ultuData = *(LPULTRALONG_UNION)&(iaAddressList[ulIndex]->__in6_u);
		#endif

		return lpip6Address;
	}

	/* Should not happen */
	return NULLPTR;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
JoinMulticastGroup6(
	_In_ 				HANDLE 				hSocket,
	_In_ 				LPIPV6_ADDRESS			lpip6MulticastAddress,
	_In_ 				ULONG				ulInterface
){
	EXIT_IF_UNLIKELY_NULL(hSocket, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpip6MulticastAddress, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hSocket), HANDLE_TYPE_SOCKET6, FALSE);

	LPSOCKET6 lpsckSocket;
	struct ipv6_mreq ipmRequest;

	lpsckSocket = (LPSOCKET6)GetHandleDerivative(hSocket);
	EXIT_IF_UNLIKELY_NULL(lpsckSocket, FALSE);
	EXIT_IF_NOT_VALUE(lpsckSocket->bMulticast, FALSE, FALSE);
	EXIT_IF_NOT_VALUE(lpsckSocket->lType, SOCK_DGRAM, FALSE); /* UDP */

	#ifdef __FreeBSD__
	CopyMemory(&(ipmRequest.ipv6mr_multiaddr.__u6_addr), lpip6MulticastAddress, sizeof(IPV6_ADDRESS));
	#else
	CopyMemory(&(ipmRequest.ipv6mr_multiaddr.__in6_u), lpip6MulticastAddress, sizeof(IPV6_ADDRESS));
	#endif

	ipmRequest.ipv6mr_interface = ulInterface;

	/* TODO: Add error reporting here */
	if (0 > setsockopt(lpsckSocket->lDescriptor, IPPROTO_IPV6, IPV6_JOIN_GROUP, &ipmRequest, sizeof(ipmRequest)))
	{ return FALSE; }

	lpsckSocket->bMulticast = TRUE;
	CopyMemory(&(lpsckSocket->ip6MulticastAddress), lpip6MulticastAddress, sizeof(IPV6_ADDRESS));
	lpsckSocket->ulMulticastInterface = ulInterface;

	return TRUE;		
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
LeaveMulticastGroup6(
	_In_ 				HANDLE 				hSocket,
	_In_ 				LPIPV6_ADDRESS			lpip6MulticastAddress,
	_In_ 				ULONG				ulInterface
){
	EXIT_IF_UNLIKELY_NULL(hSocket, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpip6MulticastAddress, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hSocket), HANDLE_TYPE_SOCKET6, FALSE);

	LPSOCKET6 lpsckSocket;
	struct ipv6_mreq ipmRequest;

	lpsckSocket = (LPSOCKET6)GetHandleDerivative(hSocket);
	EXIT_IF_UNLIKELY_NULL(lpsckSocket, FALSE);
	EXIT_IF_NOT_VALUE(lpsckSocket->bMulticast, TRUE, FALSE);
	EXIT_IF_NOT_VALUE(lpsckSocket->lType, SOCK_DGRAM, FALSE); /* UDP */

	#ifdef __FreeBSD__
	CopyMemory(&(ipmRequest.ipv6mr_multiaddr.__u6_addr), lpip6MulticastAddress, sizeof(IPV6_ADDRESS));
	#else
	CopyMemory(&(ipmRequest.ipv6mr_multiaddr.__in6_u), lpip6MulticastAddress, sizeof(IPV6_ADDRESS));
	#endif

	ipmRequest.ipv6mr_interface = ulInterface;

	/* TODO: Add error reporting here */
	if (0 > setsockopt(lpsckSocket->lDescriptor, IPPROTO_IPV6, IPV6_LEAVE_GROUP, &ipmRequest, sizeof(ipmRequest)))
	{ return FALSE; }

	lpsckSocket->bMulticast = FALSE;
	ZeroMemory(&(lpsckSocket->ip6MulticastAddress), sizeof(IPV6_ADDRESS));
	lpsckSocket->ulMulticastInterface = 0;

	return TRUE;		
}