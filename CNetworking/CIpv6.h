/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CIPV6_H
#define CIPV6_H


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../CMemory/CMemory.h"
#include "CIpv4.h"



#ifdef __LP64__ /* 64 - Bit */

typedef struct __IPV6_ADDRESS
{
	union
	{
		UULTRALONG uultData;
		ULTRALONG_UNION ultuData;
	};
} IPV6_ADDRESS, *LPIPV6_ADDRESS, **DLPIPV6_ADDRESS, ***TLPIPV6_ADDRESS;


#else /* 32 - Bit */

typedef struct __IPV6_ADDRESS
{
	ULTRALONG_UNION ultuData;
} IPV6_ADDRESS, *LPIPV6_ADDRESS, **DLPIPV6_ADDRESS, ***TLPIPV6_ADDRESS;

#endif



typedef enum __IPV6_ADDRESS_TYPE
{
	IPV6_ADDRESS_NOT_VALID		= 0,
	IPV6_ADDRESS_NULL,
	IPV6_ADDRESS_LOOPBACK,
	IPV6_ADDRESS_IPV4_TRANSLATION,
	IPV6_ADDRESS_DISCARD_PREFIX,
	IPV6_ADDRESS_TOREDO,
	IPV6_ADDRESS_ORCHIDv2,
	IPV6_ADDRESS_DOCUMENTATION,
	IPV6_ADDRESS_6TO4,
	IPV6_ADDRESS_UNIQUE_LOCAL,
	IPV6_ADDRESS_LINK_LOCAL,
	IPV6_ADDRESS_MULTICAST
} IPV6_ADDRESS_TYPE;




#define IPV6_ADDRESS_STRING_NULL		"::"
#define IPV6_ADDRESS_STRING_LOOPBACK		"::1"




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPIPV6_ADDRESS
CreateIpv6AddressFromString(
	_In_Z_ 		LPCSTR RESTRICT		lpcszAddress
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
DestroyIpv6Address(
	_In_		LPIPV6_ADDRESS RESTRICT	lpip6Address
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
InitializeIpv6Address(
	_In_ 		LPIPV6_ADDRESS RESTRICT	lpip6Address,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszAddress
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
ConvertIpv6AddressToString(
	_In_ 		LPIPV6_ADDRESS RESTRICT lpip6Address,
	_Out_ 		LPSTR 			lpszOut,
	_In_ 		ULONG 			ulBufferSize
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPSTR 
ConvertIpv6AddressToStringByAlloc(
	_In_ 		LPIPV6_ADDRESS RESTRICT lpip6Address
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
ConvertIpv6AddressToCompressedString(
	_In_ 		LPIPV6_ADDRESS RESTRICT lpip6Address,
	_Out_ 		LPSTR 			lpszOut,
	_In_ 		ULONG 			ulBufferSize
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
CompressIpv6AddressString(
	_In_ 		LPCSTR RESTRICT 	lpcszAddress,
	_Out_ 		LPSTR RESTRICT 		lpszOut,
	_In_ 		ULONG 			ulBufferSize	
);




_Success_(return != IPV6_ADDRESS_NOT_VALID, _Non_Locking_)
PODNET_API
IPV6_ADDRESS_TYPE
GetIpv6AddressType(
	_In_ 		LPIPV6_ADDRESS RESTRICT lpip6Address
);




_Success_(return != IPV6_ADDRESS_NOT_VALID, _Non_Locking_)
PODNET_API
IPV6_ADDRESS_TYPE
GetIpv6AddressTypeByString(
	_In_ 		LPCSTR RESTRICT 	lpcsz6Address
);




_Success_(return != IPV4_NULL_ADDRESS, _Non_Locking_)
PODNET_API 
IPV4_ADDRESS 
ConvertIpv6AddressToIpv4(
	_In_ 		LPIPV6_ADDRESS 		lpip6Address
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPIPV6_ADDRESS
ConvertIpv4AddressToIpv6ByCreate(
	_In_ 		IPV4_ADDRESS		ip4Address
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
ConvertIpv4AddressToIpv6(
	_In_		IPV4_ADDRESS 		ip4Address,
	_Out_ 		LPIPV6_ADDRESS		lpip6Address
);




_Success_(return != 0, _Non_Locking_)
PODNET_API
LPIPV6_ADDRESS
GetCurrentPublicIpv6Address(
	VOID
);



#endif