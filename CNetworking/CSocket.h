/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CSOCKET_H
#define CSOCKET_H


#include "CSocket4.h"
#include "CSocket6.h"
#include "CSocketUnix.h"


_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
BindOnSocket(
	_In_		HANDLE			hSocket
);




_Success_(return != FALSE, ...)
PODNET_API
BOOL
ListenOnBoundSocket(
	_In_		HANDLE			hSocket,
	_In_		ULONG			ulMaxNumberOfClientsInWaitingQueue
);




_Result_Null_On_Failure_
PODNET_API
HANDLE
AcceptConnection(
	_In_		HANDLE			hSocket
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
SocketConnect(
	_In_ 				HANDLE 				hSocket
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
SocketClose(
	_In_ 				HANDLE 				hSocket
);




_Success_(return != 0, _Non_Locking_)
PODNET_API
UARCHLONG
SocketSend(
	_In_ 				HANDLE 				hSocket,
	_In_ 				LPVOID 				lpData,
	_In_ 				UARCHLONG 			ualSize
);




_Success_(return != 0, _Non_Locking_)
PODNET_API
UARCHLONG
SocketReceive(
	_In_ 				HANDLE 				hSocket,
	_Out_ 				LPVOID 				lpOut,
	_In_ 				UARCHLONG 			ualSize
);




#endif