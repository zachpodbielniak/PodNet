/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CGeo4.h"
#include "../CompilationFlags.h"


typedef GeoIP 		GEOIP, *LPGEOIP;

GLOBAL_VARIABLE DONT_OPTIMIZE_OUT LPGEOIP lpGip = NULLPTR;
GLOBAL_VARIABLE DONT_OPTIMIZE_OUT LPGEOIP lpGipCountry = NULLPTR;

GLOBAL_VARIABLE DONT_OPTIMIZE_OUT LPCSTR lpcszNullCity = "\0";




#define PREPARE_GEOIP()										\
	if (NULLPTR == InterlockedAcquire(&lpGip))						\
	{											\
		BOOL bHappened;									\
		LPGEOIP lpGipTemp;								\
												\
		__Redo:										\
		lpGipTemp = GeoIP_open(GEO_IPV4_DATABASE, GEOIP_MEMORY_CACHE);			\
		if (NULLPTR == lpGipTemp)							\
		{ JUMP(__Redo); }								\
												\
		bHappened = InterlockedCompareExchangeHappened(&lpGip, NULLPTR, lpGipTemp);	\
		if (FALSE == bHappened)								\
		{ GeoIP_delete(lpGipTemp); }							\
	}											




#define PREPARE_GEOIP_COUNTRY()										\
	if (NULLPTR == InterlockedAcquire(&lpGipCountry))						\
	{											\
		BOOL bHappened;									\
		LPGEOIP lpGipTemp;								\
												\
		__Redo:										\
		lpGipTemp = GeoIP_open(GEO_IPV4_CITY_DATABASE, GEOIP_MEMORY_CACHE);			\
		if (NULLPTR == lpGipTemp)							\
		{ JUMP(__Redo); }								\
												\
		bHappened = InterlockedCompareExchangeHappened(&lpGipCountry, NULLPTR, lpGipTemp);	\
		if (FALSE == bHappened)								\
		{ GeoIP_delete(lpGipTemp); }							\
	}											




_Success_(return != NULLPTR, _Interlocked_Operation_)
PODNET_API
LPGEOIPRECORD
GetRecordByIpv4Address(
	_In_ 		IPV4_ADDRESS		ip4Address
){
	PREPARE_GEOIP_COUNTRY();
	return GeoIP_record_by_ipnum(lpGipCountry, LITTLE_TO_BIG_ENDIAN_32(ip4Address.ulData));
}




_Success_(return != NULLPTR, _Interlocked_Operation_)
PODNET_API
LPGEOIPRECORD
GetRecordByIpv4String(
	_In_Z_ 		LPCSTR 			lpcszAddress
){
	EXIT_IF_UNLIKELY_NULL(lpcszAddress, NULLPTR);
	PREPARE_GEOIP_COUNTRY();
	return GeoIP_record_by_addr(lpGipCountry, lpcszAddress);
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
DestroyIpv4Record(
	_In_ 		LPGEOIPRECORD 		lpgipRecord
){
	EXIT_IF_UNLIKELY_NULL(lpgipRecord, FALSE);
	GeoIPRecord_delete(lpgipRecord);
	return TRUE;
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
DLPSTR 
GetNetRangeByIpv4Address(
	_In_ 		IPV4_ADDRESS 		ip4Address
){
	CSTRING csBuffer[0x10];
	PREPARE_GEOIP_COUNTRY();

	ZeroMemory(csBuffer, sizeof(csBuffer));
	ConvertIpv4AddressToString(ip4Address, csBuffer, sizeof(csBuffer));
	return GeoIP_range_by_ip(lpGipCountry, (LPCSTR)csBuffer);
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
DLPSTR 
GetNetRangeByIpv4String(
	_In_Z_ 		LPCSTR 			lpcszAddress
){
	EXIT_IF_UNLIKELY_NULL(lpcszAddress, NULLPTR);
	PREPARE_GEOIP_COUNTRY();

	return GeoIP_range_by_ip(lpGipCountry, lpcszAddress);
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
DestroyIpv4NetRange(
	_In_ 		DLPSTR 			dlpszRange
){
	EXIT_IF_UNLIKELY_NULL(dlpszRange, FALSE);
	GeoIP_range_by_ip_delete(dlpszRange);
	return TRUE;
}




_Success_(return != NULLPTR, _Interlocked_Operation_)
PODNET_API
LPCSTR
GetCountryByIpv4Address(
	_In_ 		IPV4_ADDRESS 		ip4Address
){
	PREPARE_GEOIP();
	return GeoIP_country_code3_by_ipnum(lpGip, LITTLE_TO_BIG_ENDIAN_32(ip4Address.ulData));
}




_Success_(return != NULLPTR, _Interlocked_Operation_)
PODNET_API
LPCSTR
GetCountryByIpv4String(
	_In_Z_ 		LPCSTR 			lpcszAddress
){
	EXIT_IF_UNLIKELY_NULL(lpcszAddress, NULLPTR);
	PREPARE_GEOIP();
	return GeoIP_country_code3_by_addr(lpGip, lpcszAddress);
}




_Success_(return != NULLPTR, _Interlocked_Operation_)
PODNET_API
LPCSTR
GetNameByIpv4Address(
	_In_ 		IPV4_ADDRESS		ip4Adress
){
	PREPARE_GEOIP();
	return GeoIP_name_by_ipnum(lpGip, LITTLE_TO_BIG_ENDIAN_32(ip4Adress.ulData));
}




_Success_(return != NULLPTR, _Interlocked_Operation_)
PODNET_API
LPCSTR
GetNameByIpv4String(
	_In_Z_ 		LPCSTR 			lpcszAddress
){
	EXIT_IF_UNLIKELY_NULL(lpcszAddress, NULLPTR);
	PREPARE_GEOIP();
	return GeoIP_name_by_addr(lpGip, lpcszAddress);
}




_Success_(return != FALSE, _Interlocked_Operation_)
PODNET_API
BOOL
GetCityByIpv4Address(
	_In_ 		IPV4_ADDRESS		ip4Adress,
	_Out_Z_ 	LPSTR 			lpszOut,
	_In_ 		UARCHLONG 		ualBufferSize
){
	EXIT_IF_UNLIKELY_NULL(lpszOut, FALSE);
	EXIT_IF_UNLIKELY_NULL(ualBufferSize, FALSE);
	PREPARE_GEOIP_COUNTRY();
	GeoIPRecord *lpGir;

	ZeroMemory(lpszOut, sizeof(CHAR) * ualBufferSize);
	ualBufferSize--;

	lpGir = GeoIP_record_by_ipnum(lpGipCountry, LITTLE_TO_BIG_ENDIAN_32(ip4Adress.ulData));
	if (NULLPTR != lpGir->city)
	{ 
		UARCHLONG ualCopy, ualLength;
		ualLength = StringLength(lpGir->city); 
		ualCopy = (ualBufferSize >= ualLength) ? ualLength : ualBufferSize;
		CopyMemory(lpszOut, lpGir->city, ualCopy);
	}
	
	GeoIPRecord_delete(lpGir);
	return TRUE;
}




_Success_(return != FALSE, _Interlocked_Operation_)
PODNET_API
BOOL
GetCityByIpv4String(
	_In_Z_ 		LPCSTR 			lpcszAddress,
	_Out_Z_ 	LPSTR 			lpszOut,
	_In_ 		UARCHLONG 		ualBufferSize
){
	EXIT_IF_UNLIKELY_NULL(lpcszAddress, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpszOut, FALSE);
	EXIT_IF_UNLIKELY_NULL(ualBufferSize, FALSE);
	PREPARE_GEOIP_COUNTRY();
	GeoIPRecord *lpGir;

	ZeroMemory(lpszOut, sizeof(CHAR) * ualBufferSize);
	ualBufferSize--;

	lpGir = GeoIP_record_by_addr(lpGipCountry, lpcszAddress);
	if (NULLPTR != lpGir->city)
	{ 
		UARCHLONG ualCopy, ualLength;
		ualLength = StringLength(lpGir->city); 
		ualCopy = (ualBufferSize >= ualLength) ? ualLength : ualBufferSize;
		CopyMemory(lpszOut, lpGir->city, ualCopy);
	}
	
	GeoIPRecord_delete(lpGir);
	return TRUE;
}