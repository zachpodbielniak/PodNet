/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CHTTPREQUEST_H
#define CHTTPREQUEST_H



#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../CHandle/CHandle.h"
#include "../CString/CHeapString.h"
#include "../CString/CString.h"
#include "../CContainers/CVector.h"
#include "CHttp.h"
#include "CSocket4.h"
#include "CIpv4.h"
#include "CIpv6.h"





_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNET_API
HANDLE
CreateHttpRequest(
	_In_ 		HTTP_REQUEST_TYPE 	htrtType,
	_In_ 		HTTP_VERSION 		htvVersion,
	_In_Z_ 		LPCSTR RESTRICT		lpcszHost,
	_In_ 		USHORT 			usPort,
	_In_Z_ 		LPCSTR RESTRICT		lpcszPath,
	_In_Z_ 		LPCSTR RESTRICT		lpcszUserAgent,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszAccept,
	_In_Opt_ 	BOOL 			bUseIpv6
);




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNET_API
HANDLE 
CreateHttpRequestEx(
	_In_ 		HTTP_REQUEST_TYPE 	htrtType,
	_In_		HTTP_VERSION 		htvVersion,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszHost,
	_In_ 		USHORT 			usPort,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszPath,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszConnectionOption,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszUserAgent,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszAccept,
	_In_ 		DLPCSTR RESTRICT 	dlpcszHeaders,
	_In_ 		UARCHLONG		ualNumberOfHeaders,
	_In_Opt_ 	DLPCSTR RESTRICT 	dlpcszPostData,
	_In_Opt_ 	UARCHLONG 		ualNumberOfPostEntries,
	_In_		BOOL 			bUseIpv6
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
ExecuteHttpRequest(
	_In_ 		HANDLE 			hHttpRequest,
	_Out_ 		LPVOID 			lpOut,
	_In_ 		UARCHLONG 		ualOutSize,
	_Out_Opt_	LPUARCHLONG 		lpualBytesWritten
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API 
BOOL 
ExecuteHttpRequestEx(
	_In_ 		HANDLE 			hHttpRequest,
	_Out_Opt_ 	LPUARCHLONG 		lpualResponseCode,
	_Out_ 		LPVOID 			lpDataOut,
	_In_ 		UARCHLONG		ualOutSize,
	_Out_Opt_	LPUARCHLONG 		lpualContentLength,
	_Out_Opt_	LPHVECTOR		lphvHeaders,
	_Reserved_	LPVOID 			lpReserved,
	_In_Opt_	ULONG 			ulFlags
);



#endif