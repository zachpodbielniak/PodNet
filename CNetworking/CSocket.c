/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CSocket.h"


_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
BindOnSocket(
	_In_		HANDLE			hSocket
){
	EXIT_IF_UNLIKELY_NULL(hSocket, FALSE); 
	BOOL bRet;
	bRet = FALSE;

	switch (GetHandleDerivativeType(hSocket))
	{
		case HANDLE_TYPE_SOCKET:
		{
			bRet = BindOnSocket4(hSocket);
			break;
		}

		case HANDLE_TYPE_SOCKET6:
		{
			bRet = BindOnSocket6(hSocket);
			break;
		}

		case HANDLE_TYPE_SOCKETUNIX:
		{
			bRet = BindOnSocketUnix(hSocket);
			break;
		}
	}

	return bRet;
}




_Success_(return != FALSE, ...)
PODNET_API
BOOL
ListenOnBoundSocket(
	_In_		HANDLE			hSocket,
	_In_		ULONG			ulMaxNumberOfClientsInWaitingQueue
){
	EXIT_IF_UNLIKELY_NULL(hSocket, FALSE);
	BOOL bRet;
	bRet = FALSE;

	switch (GetHandleDerivativeType(hSocket))
	{
		case HANDLE_TYPE_SOCKET:
		{
			bRet = ListenOnBoundSocket4(hSocket, ulMaxNumberOfClientsInWaitingQueue);
			break;
		}

		case HANDLE_TYPE_SOCKET6:
		{
			bRet = ListenOnBoundSocket6(hSocket, ulMaxNumberOfClientsInWaitingQueue);
			break;
		}

		case HANDLE_TYPE_SOCKETUNIX:
		{
			bRet = ListenOnBoundSocketUnix(hSocket, ulMaxNumberOfClientsInWaitingQueue);
			break;
		}
	}

	return bRet;
}




_Result_Null_On_Failure_
PODNET_API
HANDLE
AcceptConnection(
	_In_		HANDLE			hSocket
){
	EXIT_IF_UNLIKELY_NULL(hSocket, FALSE); 
	HANDLE hRet;
	hRet = NULL_OBJECT;

	switch (GetHandleDerivativeType(hSocket))
	{
		case HANDLE_TYPE_SOCKET:
		{
			hRet = AcceptConnection4(hSocket);
			break;
		}

		case HANDLE_TYPE_SOCKET6:
		{
			hRet = AcceptConnection6(hSocket);
			break;
		}

		case HANDLE_TYPE_SOCKETUNIX:
		{
			hRet = AcceptConnectionUnix(hSocket);
			break;
		}
	}

	return hRet;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
SocketConnect(
	_In_ 				HANDLE 				hSocket
){
	EXIT_IF_UNLIKELY_NULL(hSocket, FALSE); 
	BOOL bRet;
	bRet = FALSE;

	switch (GetHandleDerivativeType(hSocket))
	{
		case HANDLE_TYPE_SOCKET:
		{
			bRet = SocketConnect4(hSocket);
			break;
		}

		case HANDLE_TYPE_SOCKET6:
		{
			bRet = SocketConnect6(hSocket);
			break;
		}

		case HANDLE_TYPE_SOCKETUNIX:
		{
			bRet = SocketConnectUnix(hSocket);
			break;
		}
	}

	return bRet;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
SocketClose(
	_In_ 				HANDLE 				hSocket
){
	EXIT_IF_UNLIKELY_NULL(hSocket, FALSE); 
	BOOL bRet;
	bRet = FALSE;

	switch (GetHandleDerivativeType(hSocket))
	{
		case HANDLE_TYPE_SOCKET:
		{
			bRet = SocketClose4(hSocket);
			break;
		}

		case HANDLE_TYPE_SOCKET6:
		{
			bRet = SocketClose6(hSocket);
			break;
		}

		case HANDLE_TYPE_SOCKETUNIX:
		{
			bRet = SocketCloseUnix(hSocket);
			break;
		}
	}

	return bRet;
}




_Success_(return != 0, _Non_Locking_)
PODNET_API
UARCHLONG
SocketSend(
	_In_ 				HANDLE 				hSocket,
	_In_ 				LPVOID 				lpData,
	_In_ 				UARCHLONG 			ualSize
){
	EXIT_IF_UNLIKELY_NULL(hSocket, FALSE); 
	UARCHLONG ualRet;
	ualRet = (UARCHLONG)-1;

	switch (GetHandleDerivativeType(hSocket))
	{
		case HANDLE_TYPE_SOCKET:
		{
			ualRet = SocketSend4(hSocket, lpData, ualSize);
			break;
		}

		case HANDLE_TYPE_SOCKET6:
		{
			ualRet = SocketSend6(hSocket, lpData, ualSize);
			break;
		}

		case HANDLE_TYPE_SOCKETUNIX:
		{
			ualRet = SocketSendUnix(hSocket, lpData, ualSize);
			break;
		}
	}

	return ualRet;
}




_Success_(return != 0, _Non_Locking_)
PODNET_API
UARCHLONG
SocketReceive(
	_In_ 				HANDLE 				hSocket,
	_Out_ 				LPVOID 				lpOut,
	_In_ 				UARCHLONG 			ualSize
){
	EXIT_IF_UNLIKELY_NULL(hSocket, FALSE); 
	UARCHLONG ualRet;
	ualRet = (UARCHLONG)-1;

	switch (GetHandleDerivativeType(hSocket))
	{
		case HANDLE_TYPE_SOCKET:
		{
			ualRet = SocketReceive4(hSocket, lpOut, ualSize);
			break;
		}

		case HANDLE_TYPE_SOCKET6:
		{
			ualRet = SocketReceive6(hSocket, lpOut, ualSize);
			break;
		}

		case HANDLE_TYPE_SOCKETUNIX:
		{
			ualRet = SocketReceiveUnix(hSocket, lpOut, ualSize);
			break;
		}
	}

	return ualRet;
}


