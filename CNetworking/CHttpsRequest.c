/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/bio.h>
#include "CHttpsRequest.h"
#include "../CompilationFlags.h"




typedef struct __HTTPS_CALLBACK_DATA
{
	HANDLE 			hParent;
	HANDLE 			hDynamicRegion;
	HSTRING			hsResponse;
	UARCHLONG 		ualLength;
} HTTPS_CALLBACK_DATA, *LPHTTPS_CALLBACK_DATA;




typedef struct __HTTPS_REQUEST
{
	INHERITS_FROM_HANDLE();
	HTTP_REQUEST_TYPE	htrtType;
	HTTP_VERSION		htvVersion;
	HANDLE 			hEvent;
	CSTRING 		csUrl[HTTP_REQUEST_BUFFER_SIZE];
	LPSTR 			lpszAccept;
	LPSTR 			lpszConnection;
	LPSTR 			lpszUserAgent;
	USHORT 			usPort;
	HTTPS_CALLBACK_DATA	hscdResponse;
	HVECTOR 		hvHeaders;	/* Response Headers */
	UARCHLONG 		ualResponseCode;
	
	DLPSTR 			dlpszHeaders;
	UARCHLONG 		ualNumberOfHeaders;

	DLPSTR 			dlpszPostData;
	UARCHLONG 		ualNumberOfPostEntries;
	
	#ifdef BUILD_WITH_ONEAGENT_SDK
	onesdk_tracer_handle_t	othOutgoingWebRequest;
	#endif

	CURL			*cCtx;
	struct curl_slist 	*cslHeaders;
	CURLcode 		ccRet;
} HTTPS_REQUEST, *LPHTTPS_REQUEST;




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__DestroyHttpsRequest(
	_In_ 		HDERIVATIVE		hHttpsRequest,
	_In_Opt_ 	ULONG 			ulFlags
){
	UNREFERENCED_PARAMETER(ulFlags);
	EXIT_IF_UNLIKELY_NULL(hHttpsRequest, FALSE);

	LPHTTPS_REQUEST lphsrData;
	lphsrData = (LPHTTPS_REQUEST)hHttpsRequest;

	curl_slist_free_all(lphsrData->cslHeaders);
	curl_easy_cleanup(lphsrData->cCtx);

	if (NULLPTR != lphsrData->hscdResponse.hsResponse)
	{ DestroyHeapString(lphsrData->hscdResponse.hsResponse); }

	if (NULL_OBJECT != lphsrData->hscdResponse.hDynamicRegion)
	{ DestroyObject(lphsrData->hscdResponse.hDynamicRegion); }

	DestroyObject(lphsrData->hEvent);
	
	if (NULLPTR != lphsrData->lpszAccept)
	{ FreeMemory(lphsrData->lpszAccept); }

	if (NULLPTR != lphsrData->lpszConnection)
	{ FreeMemory(lphsrData->lpszConnection); }

	if (NULLPTR != lphsrData->lpszUserAgent)
	{ FreeMemory(lphsrData->lpszUserAgent); }
	
	if (NULLPTR != lphsrData->hvHeaders)
	{
		UARCHLONG ualIndex;
		for (
			ualIndex = 0;
			ualIndex < VectorSize(lphsrData->hvHeaders);
			ualIndex++
		){
			LPHTTP_REQUEST_HEADER lphrhData;
			lphrhData = VectorAt(lphsrData->hvHeaders, ualIndex);

			if (NULLPTR == lphrhData)
			{ continue; }

			if (NULLPTR != lphrhData->lpszHeader)
			{ FreeMemory(lphrhData->lpszHeader); }

			if (NULLPTR != lphrhData->lpszValue)
			{ FreeMemory(lphrhData->lpszValue); }
		}

		DestroyVector(lphsrData->hvHeaders);
	}

	FreeMemory(lphsrData);
	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__WaitForHttpsRequest(
	_In_ 		HANDLE 			hHttpsRequest,
	_In_ 		ULONGLONG 		ullMilliSeconds
){
	EXIT_IF_UNLIKELY_NULL(hHttpsRequest, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hHttpsRequest), HANDLE_TYPE_HTTPS_REQUEST, FALSE);

	LPHTTPS_REQUEST lphtsRequest;
	lphtsRequest = (LPHTTPS_REQUEST)(hHttpsRequest);
	EXIT_IF_UNLIKELY_NULL(lphtsRequest, FALSE);

	return WaitForSingleObject(lphtsRequest->hEvent, ullMilliSeconds);
}




_Call_Back_
_Success_(return != 0, _Non_Locking_)
INTERNAL_OPERATION
ARCHLONG
__HttpsWriteCallback(
	_In_		LPVOID			lpSlice,
	_In_ 		ARCHLONG		alIgnore,
	_In_ 		ARCHLONG 		alSize,
	_In_Opt_ 	LPVOID 			lpData
){
	UNREFERENCED_PARAMETER(alIgnore);
	EXIT_IF_UNLIKELY_NULL(lpData, 0);

	LPHTTPS_CALLBACK_DATA lphscdData;
	lphscdData = (LPHTTPS_CALLBACK_DATA)lpData;

	if (NULLPTR == lphscdData->hDynamicRegion)
	{ lphscdData->hDynamicRegion = CreateDynamicHeapRegion(NULL_OBJECT, (UARCHLONG)alSize, FALSE, 0); }

	DynamicHeapRegionAppend(lphscdData->hDynamicRegion, lpSlice, (UARCHLONG)alSize);
	return alSize;
}




_Call_Back_
_Success_(return != 0, _Non_Locking_)
INTERNAL_OPERATION
ARCHLONG
__HeaderWriteCallback(
	_In_		LPVOID			lpSlice,
	_In_ 		ARCHLONG		alIgnore,
	_In_ 		ARCHLONG 		alSize,
	_In_Opt_ 	LPVOID 			lpData
){
	UNREFERENCED_PARAMETER(alIgnore);
	EXIT_IF_UNLIKELY_NULL(lpData, 0);

	LPHTTPS_REQUEST lphsrData;
	HTTP_REQUEST_HEADER hrhData;
	CSTRING csBuffer[0x400];
	LPSTR lpszIndex;
	UARCHLONG ualLength;

	lphsrData = lpData;
	ZeroMemory(&hrhData, sizeof(hrhData));
	ZeroMemory(csBuffer, sizeof(csBuffer));

	if (NULLPTR == lphsrData->hvHeaders)
	{ lphsrData->hvHeaders = CreateVector(0x08, sizeof(HTTP_REQUEST_HEADER), 0); }

	StringCopySafe(csBuffer, (LPCSTR)lpSlice, sizeof(csBuffer) - 0x01U);

	lpszIndex = StringInString(csBuffer, ":");
	if (NULLPTR == lpszIndex)
	{ return alSize; }

	*lpszIndex = '\0';
	lpszIndex = (LPSTR)((UARCHLONG)lpszIndex + 0x01U);

	while (*lpszIndex == ' ')
	{ lpszIndex = (LPSTR)((UARCHLONG)lpszIndex + 0x01U); }

	ualLength = StringLength(lpszIndex);
	lpszIndex[ualLength-2] = '\0';

	hrhData.lpszHeader = StringDuplicate((LPCSTR)csBuffer);
	hrhData.lpszValue = StringDuplicate((LPCSTR)lpszIndex);

	VectorPushBack(lphsrData->hvHeaders, &hrhData, 0);

	#ifdef BUILD_WITH_ONEAGENT_SDK
	onesdk_outgoingwebrequesttracer_add_response_header(
		lphsrData->othOutgoingWebRequest,
		onesdk_asciistr(hrhData.lpszHeader),
		onesdk_asciistr(hrhData.lpszValue)
	);
	#endif

	return alSize;
}




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNET_API
HANDLE
CreateHttpsRequest(
	_In_ 		HTTP_REQUEST_TYPE	htrtType,
	_In_ 		HTTP_VERSION 		htvVersion,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszHost,
	_In_		USHORT 			usPort,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszPath,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszConnectionOption,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszUserAgent,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszAccept,
	_In_ 		BOOL			bUseIpv6,
	_In_ 		BOOL 			bVerifyCertificate
){
	EXIT_IF_UNLIKELY_NULL(htrtType, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NULL(htvVersion, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NULL(lpcszHost, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NULL(lpcszPath, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NULL(lpcszConnectionOption, NULL_OBJECT);

	HANDLE hHttpsRequest;
	LPHTTPS_REQUEST lphsrData;

	CSTRING csUrl[0x2000];
	CSTRING csUserAgent[0x400];
	CSTRING csAccept[0x400];
	CSTRING csConnection[0x400];


	ZeroMemory(csUrl, sizeof(csUrl));
	ZeroMemory(csUserAgent, sizeof(csUserAgent));
	ZeroMemory(csAccept, sizeof(csAccept));
	ZeroMemory(csConnection, sizeof(csConnection));

	lphsrData = GlobalAllocAndZero(sizeof(HTTPS_REQUEST));
	if (NULLPTR == lphsrData)
	{ return NULL_OBJECT; }
	
	lphsrData->htrtType = htrtType;

	StringPrintFormatSafe(
		csUrl,
		sizeof(csUrl) - 1,
		"https://%s%s",
		lpcszHost,
		lpcszPath
	);

	StringPrintFormatSafe(
		csConnection,
		sizeof(csConnection) - 1,
		"Connection: %s",
		lpcszConnectionOption
	);

	if (NULLPTR != lpcszUserAgent)
	{
		StringPrintFormatSafe(
			csUserAgent,
			sizeof(csUserAgent) - 1,
			"%s: %s",
			"User-Agent",
			lpcszUserAgent
		);
	}

	if (NULLPTR != lpcszAccept)
	{
		StringPrintFormatSafe(
			csAccept,
			sizeof(csAccept) - 1,
			"%s: %s",
			"Accept",
			lpcszAccept
		);
	}

	StringCopySafe(
		lphsrData->csUrl,
		csUrl,
		HTTP_REQUEST_BUFFER_SIZE
	);

	/* Begin CURL Stuff */
	lphsrData->cCtx = curl_easy_init();

	curl_easy_setopt(lphsrData->cCtx, CURLOPT_CUSTOMREQUEST, HTTP_REQUEST_TYPE_TO_STRING(htrtType));
	curl_easy_setopt(lphsrData->cCtx, CURLOPT_URL, csUrl);
	curl_easy_setopt(lphsrData->cCtx, CURLOPT_PORT, usPort);
	curl_easy_setopt(lphsrData->cCtx, CURLOPT_WRITEFUNCTION, __HttpsWriteCallback);
	curl_easy_setopt(lphsrData->cCtx, CURLOPT_WRITEDATA, &(lphsrData->hscdResponse)); 
	curl_easy_setopt(lphsrData->cCtx, CURLOPT_HEADERFUNCTION, __HeaderWriteCallback);
	curl_easy_setopt(lphsrData->cCtx, CURLOPT_HEADERDATA, lphsrData);

	if (FALSE == bUseIpv6)
	{ curl_easy_setopt(lphsrData->cCtx, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4); }
	else
	{ curl_easy_setopt(lphsrData->cCtx, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V6); }
	
	if (FALSE == bVerifyCertificate)
	{ 
		curl_easy_setopt(lphsrData->cCtx, CURLOPT_SSL_VERIFYHOST, 0L); 
		curl_easy_setopt(lphsrData->cCtx, CURLOPT_SSL_VERIFYPEER, 0L);
	}
	else
	{ 
		curl_easy_setopt(lphsrData->cCtx, CURLOPT_SSL_VERIFYHOST, 2L); 
		curl_easy_setopt(lphsrData->cCtx, CURLOPT_SSL_VERIFYPEER, 1L);
	}

	hHttpsRequest = CreateHandleWithSingleInheritor(
		lphsrData,
		&(lphsrData->hThis),
		HANDLE_TYPE_HTTPS_REQUEST,
		__DestroyHttpsRequest,
		0,
		NULLPTR,
		0,
		NULLPTR,
		0,
		&(lphsrData->ullId),
		0
	);

	SetHandleEventWaitFunction(hHttpsRequest, __WaitForHttpsRequest, 0);

	return hHttpsRequest;
}




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNET_API
HANDLE 
CreateHttpsRequestEx(
	_In_ 		HTTP_REQUEST_TYPE 	htrtType,
	_In_		HTTP_VERSION 		htvVersion,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszHost,
	_In_ 		USHORT 			usPort,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszPath,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszConnectionOption,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszUserAgent,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszAccept,
	_In_ 		DLPCSTR RESTRICT 	dlpcszHeaders,
	_In_ 		UARCHLONG		ualNumberOfHeaders,
	_In_Opt_ 	DLPCSTR RESTRICT 	dlpcszPostData,
	_In_Opt_ 	UARCHLONG 		ualNumberOfPostEntries,
	_In_		BOOL 			bUseIpv6,
	_In_ 		BOOL 			bVerifyCertificate
){
	EXIT_IF_UNLIKELY_NULL(htrtType, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NULL(htvVersion, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NULL(lpcszHost, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NULL(lpcszPath, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NULL(lpcszConnectionOption, NULL_OBJECT);

	HANDLE hHttpsRequest;
	LPHTTPS_REQUEST lphsrData;

	CSTRING csUrl[0x2000];
	CSTRING csUserAgent[0x400];
	CSTRING csAccept[0x400];
	CSTRING csConnection[0x400];

	ZeroMemory(csUrl, sizeof(csUrl));
	ZeroMemory(csUserAgent, sizeof(csUserAgent));
	ZeroMemory(csAccept, sizeof(csAccept));
	ZeroMemory(csConnection, sizeof(csConnection));

	lphsrData = GlobalAllocAndZero(sizeof(HTTPS_REQUEST));
	if (NULLPTR == lphsrData)
	{ return NULL_OBJECT; }

	lphsrData->htrtType = htrtType;
	lphsrData->dlpszPostData = (DLPSTR)dlpcszPostData;
	lphsrData->ualNumberOfPostEntries = ualNumberOfPostEntries;
	lphsrData->dlpszHeaders = (DLPSTR)dlpcszHeaders;
	lphsrData->ualNumberOfHeaders = ualNumberOfHeaders;

	StringPrintFormatSafe(
		csUrl,
		sizeof(csUrl) - 1,
		"https://%s%s",
		lpcszHost,
		lpcszPath
	);

	StringPrintFormatSafe(
		csConnection,
		sizeof(csConnection) - 1,
		"Connection: %s",
		lpcszConnectionOption
	);

	if (NULLPTR != lpcszUserAgent)
	{
		StringPrintFormatSafe(
			csUserAgent,
			sizeof(csUserAgent) - 1,
			"%s: %s",
			"User-Agent",
			lpcszUserAgent
		);
	}

	if (NULLPTR != lpcszAccept)
	{
		StringPrintFormatSafe(
			csAccept,
			sizeof(csAccept) - 1,
			"%s: %s",
			"Accept",
			lpcszAccept
		);
	}

	StringCopySafe(
		lphsrData->csUrl,
		csUrl,
		HTTP_REQUEST_BUFFER_SIZE
	);

	/* Begin CURL Stuff */
	lphsrData->cCtx = curl_easy_init();

	curl_easy_setopt(lphsrData->cCtx, CURLOPT_CUSTOMREQUEST, HTTP_REQUEST_TYPE_TO_STRING(htrtType));
	curl_easy_setopt(lphsrData->cCtx, CURLOPT_URL, csUrl);
	curl_easy_setopt(lphsrData->cCtx, CURLOPT_PORT, usPort);
	curl_easy_setopt(lphsrData->cCtx, CURLOPT_WRITEFUNCTION, __HttpsWriteCallback);
	curl_easy_setopt(lphsrData->cCtx, CURLOPT_WRITEDATA, &(lphsrData->hscdResponse)); 
	curl_easy_setopt(lphsrData->cCtx, CURLOPT_HEADERFUNCTION, __HeaderWriteCallback);
	curl_easy_setopt(lphsrData->cCtx, CURLOPT_HEADERDATA, lphsrData);

	if (FALSE == bUseIpv6)
	{ curl_easy_setopt(lphsrData->cCtx, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4); }
	else
	{ curl_easy_setopt(lphsrData->cCtx, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V6); }
	
	if (FALSE == bVerifyCertificate)
	{ 
		curl_easy_setopt(lphsrData->cCtx, CURLOPT_SSL_VERIFYHOST, 0L); 
		curl_easy_setopt(lphsrData->cCtx, CURLOPT_SSL_VERIFYPEER, 0L);
	}
	else
	{ 
		curl_easy_setopt(lphsrData->cCtx, CURLOPT_SSL_VERIFYHOST, 2L); 
		curl_easy_setopt(lphsrData->cCtx, CURLOPT_SSL_VERIFYPEER, 1L);
	}

	hHttpsRequest = CreateHandleWithSingleInheritor(
		lphsrData,
		&(lphsrData->hThis),
		HANDLE_TYPE_HTTPS_REQUEST,
		__DestroyHttpsRequest,
		0,
		NULLPTR,
		0,
		NULLPTR,
		0,
		&(lphsrData->ullId),
		0
	);

	SetHandleEventWaitFunction(hHttpsRequest, __WaitForHttpsRequest, 0);

	return hHttpsRequest;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
ExecuteHttpsRequest(
	_In_ 		HANDLE 			hHttpsRequest,
	_Out_		LPVOID 			lpOut,
	_In_ 		UARCHLONG 		ualOutSize,
	_Out_Opt_	LPUARCHLONG 		lpualBytesWritten
){
	EXIT_IF_UNLIKELY_NULL(hHttpsRequest, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpOut, FALSE);
	EXIT_IF_UNLIKELY_NULL(ualOutSize, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hHttpsRequest), HANDLE_TYPE_HTTPS_REQUEST, FALSE);

	LPHTTPS_REQUEST lphsrData;
	UARCHLONG ualSize, ualLength;
	UARCHLONG ualIndex;
	LPVOID lpData;

	lphsrData = OBJECT_CAST(hHttpsRequest, LPHTTPS_REQUEST);


	
	#ifdef BUILD_WITH_ONEAGENT_SDK
	LPSTR lpszIndex;
	CSTRING csTrace[0x100];
	CSTRING csHeaderSplit[0x400];

	lphsrData->othOutgoingWebRequest = onesdk_outgoingwebrequesttracer_create(
		onesdk_asciistr(lphsrData->csUrl),
		onesdk_asciistr(HTTP_REQUEST_TYPE_TO_STRING(lphsrData->htrtType))
	);
	
	ZeroMemory(csTrace, sizeof(csTrace));

	onesdk_tracer_get_outgoing_dynatrace_string_tag(
		lphsrData->othOutgoingWebRequest,
		csTrace,
		sizeof(csTrace) - 0x01U,
		NULLPTR
	);
	
	if ('\0' != csTrace[0])
	{
		onesdk_outgoingwebrequesttracer_add_request_header(
			lphsrData->othOutgoingWebRequest,
			onesdk_asciistr(ONESDK_DYNATRACE_HTTP_HEADER_NAME),
			onesdk_asciistr(csTrace)
		);
	}

	if (NULLPTR != lphsrData->lpszUserAgent)
	{
		onesdk_outgoingwebrequesttracer_add_request_header(
			lphsrData->othOutgoingWebRequest,
			onesdk_asciistr("User-Agent"),
			onesdk_asciistr(lphsrData->lpszUserAgent)
		);
	}

	if (NULLPTR != lphsrData->lpszAccept)
	{
		onesdk_outgoingwebrequesttracer_add_request_header(
			lphsrData->othOutgoingWebRequest,
			onesdk_asciistr("Accept"),
			onesdk_asciistr(lphsrData->lpszAccept)
		);
	}

	if (NULLPTR != lphsrData->lpszConnection)
	{
		onesdk_outgoingwebrequesttracer_add_request_header(
			lphsrData->othOutgoingWebRequest,
			onesdk_asciistr("Connection"),
			onesdk_asciistr(lphsrData->lpszConnection)
		);
	}

	if (NULLPTR != lphsrData->dlpszHeaders)
	{
		for (
			ualIndex = 0;
			ualIndex < lphsrData->ualNumberOfHeaders;
			ualIndex++
		){
			ZeroMemory(csHeaderSplit, sizeof(csHeaderSplit));
			StringCopySafe(csHeaderSplit, lphsrData->dlpszHeaders[ualIndex], sizeof(csHeaderSplit) - 0x01U);

			lpszIndex = StringInString(csHeaderSplit, ":");
			if (NULLPTR == lpszIndex)
			{ continue; }

			*lpszIndex = '\0';
			lpszIndex = (LPSTR)((UARCHLONG)lpszIndex + 0x01U);

			while (*lpszIndex == ' ')
			{ lpszIndex = (LPSTR)((UARCHLONG)lpszIndex + 0x01U); }

			onesdk_outgoingwebrequesttracer_add_request_header(
				lphsrData->othOutgoingWebRequest,
				onesdk_asciistr(csHeaderSplit),
				onesdk_asciistr(lpszIndex)
			);
		}
	}
	
	onesdk_tracer_start(lphsrData->othOutgoingWebRequest);
	#endif
	
	if (NULLPTR != lphsrData->cslHeaders)
	{ 
		curl_slist_free_all(lphsrData->cslHeaders); 
		lphsrData->cslHeaders = NULLPTR;
	}

	/* Create The HTTP Headers */
	if (NULLPTR != lphsrData->lpszUserAgent)
	{ lphsrData->cslHeaders = curl_slist_append(lphsrData->cslHeaders, lphsrData->lpszUserAgent); }
	if (NULLPTR != lphsrData->lpszAccept)
	{ lphsrData->cslHeaders = curl_slist_append(lphsrData->cslHeaders, lphsrData->lpszAccept); }
	if (NULLPTR != lphsrData->lpszConnection)
	{ lphsrData->cslHeaders = curl_slist_append(lphsrData->cslHeaders, lphsrData->lpszConnection); }

	#ifdef BUILD_WITH_ONEAGENT_SDK
	CSTRING csTag[0x200];
	ZeroMemory(csTag, sizeof(csTag));

	StringPrintFormatSafe(
		csTag,
		sizeof(csTag) - 0x01U,
		"%s: %s",
		ONESDK_DYNATRACE_HTTP_HEADER_NAME,
		csTrace
	);

	lphsrData->cslHeaders = curl_slist_append(lphsrData->cslHeaders, csTag);
	#endif
	
	if (NULLPTR != lphsrData->dlpszHeaders)
	{
		for (
			ualIndex = 0;
			ualIndex < lphsrData->ualNumberOfHeaders;
			ualIndex++
		){ lphsrData->cslHeaders = curl_slist_append(lphsrData->cslHeaders, lphsrData->dlpszHeaders[ualIndex]); }
	}
	
	if (HTTP_REQUEST_POST == lphsrData->htrtType)
	{
		if (NULLPTR != lphsrData->dlpszPostData)
		{
			for (
				ualIndex = 0;
				ualIndex < lphsrData->ualNumberOfPostEntries;
				ualIndex++
			){ curl_easy_setopt(lphsrData->cCtx, CURLOPT_POSTFIELDS, lphsrData->dlpszPostData[ualIndex]); }
		}
	}


	if (NULL_OBJECT != lphsrData->hscdResponse.hDynamicRegion)
	{
		DestroyObject(lphsrData->hscdResponse.hDynamicRegion);
		lphsrData->hscdResponse.hDynamicRegion = NULLPTR;
	}
	
	if (NULLPTR != lphsrData->hvHeaders)
	{
		for (
			ualIndex = 0;
			ualIndex < VectorSize(lphsrData->hvHeaders);
			ualIndex++
		){
			LPHTTP_REQUEST_HEADER lphrhData;
			lphrhData = VectorAt(lphsrData->hvHeaders, ualIndex);

			if (NULLPTR == lphrhData)
			{ continue; }

			if (NULLPTR != lphrhData->lpszHeader)
			{ FreeMemory(lphrhData->lpszHeader); }

			if (NULLPTR != lphrhData->lpszValue)
			{ FreeMemory(lphrhData->lpszValue); }
		}

		lphsrData->hvHeaders = NULLPTR;
	}
	
	curl_easy_setopt(lphsrData->cCtx, CURLOPT_HTTPHEADER, lphsrData->cslHeaders);
	lphsrData->ccRet = curl_easy_perform(lphsrData->cCtx);
	
	if (CURLE_OK != lphsrData->ccRet)
	{ return FALSE; }
	
	#ifdef BUILD_WITH_ONEAGENT_SDK
	onesdk_outgoingwebrequesttracer_set_status_code(
		lphsrData->othOutgoingWebRequest,
		(onesdk_int32_t)lphsrData->ualResponseCode
	);
	#endif

	lpData = DynamicHeapRegionData(lphsrData->hscdResponse.hDynamicRegion, &ualLength);
	if (NULLPTR == lpData)
	{ return FALSE; }

	ualSize = (ualOutSize < ualLength) ? ualOutSize : ualLength;

	CopyMemory(
		lpOut,
		lpData,
		ualSize
	);

	if (NULLPTR != lpualBytesWritten)
	{ *lpualBytesWritten = ualSize; }
	
	#ifdef BUILD_WITH_ONEAGENT_SDK
	onesdk_tracer_end(lphsrData->othOutgoingWebRequest);
	lphsrData->othOutgoingWebRequest = ONESDK_INVALID_HANDLE;
	#endif

	SignalEvent(lphsrData->hEvent);

	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
ExecuteHttpsRequestEx(
	_In_ 		HANDLE 			hHttpsRequest,
	_Out_Opt_	LPUARCHLONG 		lpualResponseCode,
	_Out_		LPVOID 			lpDataOut,
	_In_ 		UARCHLONG 		ualOutSize,
	_Out_Opt_	LPUARCHLONG 		lpualContentWritten,
	_Out_Opt_ 	LPHVECTOR		lphvHeaders,
	_Reserved_	LPVOID 			lpReserved,
	_In_Opt_ 	ULONG 			ulFlags
){
	UNREFERENCED_PARAMETER(lpReserved);
	UNREFERENCED_PARAMETER(ulFlags);

	EXIT_IF_UNLIKELY_NULL(hHttpsRequest, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpDataOut, FALSE);
	EXIT_IF_UNLIKELY_NULL(ualOutSize, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hHttpsRequest), HANDLE_TYPE_HTTPS_REQUEST, FALSE);

	LPHTTPS_REQUEST lphsrData;
	UARCHLONG ualSize, ualLength;
	UARCHLONG ualIndex;
	LPVOID lpData;
	LPVOID lpBody;

	lphsrData = OBJECT_CAST(hHttpsRequest, LPHTTPS_REQUEST);
	
	#ifdef BUILD_WITH_ONEAGENT_SDK
	LPSTR lpszIndex;
	CSTRING csTrace[0x100];
	CSTRING csHeaderSplit[0x400];

	lphsrData->othOutgoingWebRequest = onesdk_outgoingwebrequesttracer_create(
		onesdk_asciistr(lphsrData->csUrl),
		onesdk_asciistr(HTTP_REQUEST_TYPE_TO_STRING(lphsrData->htrtType))
	);
	
	ZeroMemory(csTrace, sizeof(csTrace));

	onesdk_tracer_get_outgoing_dynatrace_string_tag(
		lphsrData->othOutgoingWebRequest,
		csTrace,
		sizeof(csTrace) - 0x01U,
		NULLPTR
	);
	
	if ('\0' != csTrace[0])
	{
		onesdk_outgoingwebrequesttracer_add_request_header(
			lphsrData->othOutgoingWebRequest,
			onesdk_asciistr(ONESDK_DYNATRACE_HTTP_HEADER_NAME),
			onesdk_asciistr(csTrace)
		);
	}

	if (NULLPTR != lphsrData->lpszUserAgent)
	{
		onesdk_outgoingwebrequesttracer_add_request_header(
			lphsrData->othOutgoingWebRequest,
			onesdk_asciistr("User-Agent"),
			onesdk_asciistr(lphsrData->lpszUserAgent)
		);
	}

	if (NULLPTR != lphsrData->lpszAccept)
	{
		onesdk_outgoingwebrequesttracer_add_request_header(
			lphsrData->othOutgoingWebRequest,
			onesdk_asciistr("Accept"),
			onesdk_asciistr(lphsrData->lpszAccept)
		);
	}

	if (NULLPTR != lphsrData->lpszConnection)
	{
		onesdk_outgoingwebrequesttracer_add_request_header(
			lphsrData->othOutgoingWebRequest,
			onesdk_asciistr("Connection"),
			onesdk_asciistr(lphsrData->lpszConnection)
		);
	}

	if (NULLPTR != lphsrData->dlpszHeaders)
	{
		for (
			ualIndex = 0;
			ualIndex < lphsrData->ualNumberOfHeaders;
			ualIndex++
		){
			ZeroMemory(csHeaderSplit, sizeof(csHeaderSplit));
			StringCopySafe(csHeaderSplit, lphsrData->dlpszHeaders[ualIndex], sizeof(csHeaderSplit) - 0x01U);

			lpszIndex = StringInString(csHeaderSplit, ":");
			if (NULLPTR == lpszIndex)
			{ continue; }

			*lpszIndex = '\0';
			lpszIndex = (LPSTR)((UARCHLONG)lpszIndex + 0x01U);

			while (*lpszIndex == ' ')
			{ lpszIndex = (LPSTR)((UARCHLONG)lpszIndex + 0x01U); }

			onesdk_outgoingwebrequesttracer_add_request_header(
				lphsrData->othOutgoingWebRequest,
				onesdk_asciistr(csHeaderSplit),
				onesdk_asciistr(lpszIndex)
			);
		}
	}
	
	onesdk_tracer_start(lphsrData->othOutgoingWebRequest);
	#endif
	
	if (NULLPTR != lphsrData->cslHeaders)
	{ 
		curl_slist_free_all(lphsrData->cslHeaders); 
		lphsrData->cslHeaders = NULLPTR;
	}

	/* Create The HTTP Headers */
	if (NULLPTR != lphsrData->lpszUserAgent)
	{ lphsrData->cslHeaders = curl_slist_append(lphsrData->cslHeaders, lphsrData->lpszUserAgent); }
	if (NULLPTR != lphsrData->lpszAccept)
	{ lphsrData->cslHeaders = curl_slist_append(lphsrData->cslHeaders, lphsrData->lpszAccept); }
	if (NULLPTR != lphsrData->lpszConnection)
	{ lphsrData->cslHeaders = curl_slist_append(lphsrData->cslHeaders, lphsrData->lpszConnection); }

	#ifdef BUILD_WITH_ONEAGENT_SDK
	CSTRING csTag[0x200];
	ZeroMemory(csTag, sizeof(csTag));

	StringPrintFormatSafe(
		csTag,
		sizeof(csTag) - 0x01U,
		"%s: %s",
		ONESDK_DYNATRACE_HTTP_HEADER_NAME,
		csTrace
	);

	lphsrData->cslHeaders = curl_slist_append(lphsrData->cslHeaders, csTag);
	#endif
	
	if (NULLPTR != lphsrData->dlpszHeaders)
	{
		for (
			ualIndex = 0;
			ualIndex < lphsrData->ualNumberOfHeaders;
			ualIndex++
		){ lphsrData->cslHeaders = curl_slist_append(lphsrData->cslHeaders, lphsrData->dlpszHeaders[ualIndex]); }
	}
	
	if (HTTP_REQUEST_POST == lphsrData->htrtType)
	{
		if (NULLPTR != lphsrData->dlpszPostData)
		{
			for (
				ualIndex = 0;
				ualIndex < lphsrData->ualNumberOfPostEntries;
				ualIndex++
			){ curl_easy_setopt(lphsrData->cCtx, CURLOPT_POSTFIELDS, lphsrData->dlpszPostData[ualIndex]); }
		}
	}

	if (NULL_OBJECT != lphsrData->hscdResponse.hDynamicRegion)
	{
		DestroyObject(lphsrData->hscdResponse.hDynamicRegion);
		lphsrData->hscdResponse.hDynamicRegion = NULLPTR;
	}
	
	if (NULLPTR != lphsrData->hvHeaders)
	{
		for (
			ualIndex = 0;
			ualIndex < VectorSize(lphsrData->hvHeaders);
			ualIndex++
		){
			LPHTTP_REQUEST_HEADER lphrhData;
			lphrhData = VectorAt(lphsrData->hvHeaders, ualIndex);

			if (NULLPTR == lphrhData)
			{ continue; }

			if (NULLPTR != lphrhData->lpszHeader)
			{ FreeMemory(lphrhData->lpszHeader); }

			if (NULLPTR != lphrhData->lpszValue)
			{ FreeMemory(lphrhData->lpszValue); }
		}

		lphsrData->hvHeaders = NULLPTR;
	}

	curl_easy_setopt(lphsrData->cCtx, CURLOPT_HTTPHEADER, lphsrData->cslHeaders);
	lphsrData->ccRet = curl_easy_perform(lphsrData->cCtx);
	
	if (CURLE_OK != lphsrData->ccRet)
	{ return FALSE; }
	
	#ifdef BUILD_WITH_ONEAGENT_SDK
	onesdk_outgoingwebrequesttracer_set_status_code(
		lphsrData->othOutgoingWebRequest,
		(onesdk_int32_t)lphsrData->ualResponseCode
	);
	#endif

	if (NULLPTR != lpualResponseCode)
	{ *lpualResponseCode = lphsrData->ualResponseCode; }

	lpData = DynamicHeapRegionData(lphsrData->hscdResponse.hDynamicRegion, &ualLength);
	if (NULLPTR == lpData)
	{ return FALSE; }

	lpBody = StripHttpHeader(lpData);
	ualSize = ualLength - ((UARCHLONG)lpBody - (UARCHLONG)lpData);
	ualSize = (ualOutSize < ualSize) ? ualOutSize : ualSize;

	CopyMemory(
		lpDataOut,
		lpBody,
		ualSize
	);

	if (NULLPTR != lpualContentWritten)
	{ *lpualContentWritten = ualSize; }
	
	#ifdef BUILD_WITH_ONEAGENT_SDK
	onesdk_tracer_end(lphsrData->othOutgoingWebRequest);
	lphsrData->othOutgoingWebRequest = ONESDK_INVALID_HANDLE;
	#endif

	SignalEvent(lphsrData->hEvent);

	if (NULLPTR != lphvHeaders)
	{ *lphvHeaders = lphsrData->hvHeaders; }

	return TRUE;
}
