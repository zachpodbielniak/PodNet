/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CSocketUnix.h"


typedef struct __SOCKET_UNIX
{
	INHERITS_FROM_HANDLE();
	LONG 			lDescriptor;
	SOCKET_ADDRESS_UNIX	sauSocketAddress;
	LONG 			lDomain; 
	LONG 			lType; 
	LONG 			lProtocol;
	BOOL 			bBound; 
	BOOL 			bListen;
	BOOL 			bIsClient;
	UULTRALONG		uulTotalNumberOfAcceptedConnections;
	LPSTR 			lpszPath;
} SOCKET_UNIX, *LPSOCKET_UNIX;




_Call_Back_
_Success_(return != FALSE, _Interlocked_Operation_)
INTERNAL_OPERATION
BOOL
__DestroySocketUnix(
        _In_            HDERIVATIVE             hDerivative,
        _In_Opt_        ULONG                   ulFlags
){
        EXIT_IF_UNLIKELY_NULL(hDerivative, FALSE);
        UNREFERENCED_PARAMETER(ulFlags);

        LPSOCKET_UNIX lpsuSocket = (LPSOCKET_UNIX)hDerivative;

        if (lpsuSocket->lDescriptor != 0) 
        { close(lpsuSocket->lDescriptor); }

	FreeMemory(lpsuSocket->lpszPath);
        FreeMemory(lpsuSocket);
        return TRUE;
}




_Result_Null_On_Failure_
_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNET_API
HANDLE
CreateSocketUnix(
	_In_Z_ 		LPCSTR 			lpcszPath
){
	EXIT_IF_UNLIKELY_NULL(lpcszPath, NULL_OBJECT);

	HANDLE hSocket;
	LPSOCKET_UNIX lpsuSck;
	
	lpsuSck = GlobalAllocAndZero(sizeof(SOCKET_UNIX));
	EXIT_IF_UNLIKELY_NULL(lpsuSck, NULL_OBJECT);

	lpsuSck->lDomain = AF_UNIX;
	lpsuSck->sauSocketAddress.sun_family = AF_UNIX;
	/* Hidden */
	if ('\0' == lpcszPath[0])
	{
		lpsuSck->sauSocketAddress.sun_path[0] = '\0';
		StringCopySafe(
			(LPSTR)((UARCHLONG)lpsuSck->sauSocketAddress.sun_path + 0x01U),
			(LPSTR)((UARCHLONG)lpcszPath + 0x01U),
			sizeof(lpsuSck->sauSocketAddress.sun_path) - 2
		);
	}
	else 
	{
		StringCopySafe(
			lpsuSck->sauSocketAddress.sun_path,
			lpcszPath, 
			sizeof(lpsuSck->sauSocketAddress.sun_path) - 1
		);
	
		/* unlink(lpcszPath); */
	}


	lpsuSck->lDescriptor = socket(AF_UNIX, SOCK_STREAM, 0);
	if (-1 == lpsuSck->lDescriptor)
	{ 
		FreeMemory(lpsuSck);
		return NULL_OBJECT;
	}
	
	lpsuSck->lpszPath = GlobalAllocAndZero(StringLength(lpcszPath));
	if (NULLPTR == lpsuSck->lpszPath)
	{
		close(lpsuSck->lDescriptor);
		FreeMemory(lpsuSck);
		return NULL_OBJECT;
	}

	StringCopySafe(lpsuSck->lpszPath, lpcszPath, StringLength(lpcszPath));

	hSocket = CreateHandleWithSingleInheritor(
		lpsuSck,
		&(lpsuSck->hThis),
		HANDLE_TYPE_SOCKETUNIX,
		__DestroySocketUnix,
		0,
		NULLPTR,
		0,
		NULLPTR,
		0,
		&(lpsuSck->ullId),
		0
	);

	if (NULL_OBJECT == hSocket)
	{ 
		FreeMemory(lpsuSck->lpszPath);
		close(lpsuSck->lDescriptor);
		FreeMemory(lpsuSck);
		return NULL_OBJECT;
	}

	return hSocket;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
BindOnSocketUnix(
	_In_		HANDLE			hSocket
){
	EXIT_IF_UNLIKELY_NULL(hSocket, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hSocket), HANDLE_TYPE_SOCKETUNIX, FALSE);

	LPSOCKET_UNIX lpsuSck;
	LONG lRet;

	lpsuSck = OBJECT_CAST(hSocket, LPSOCKET_UNIX);
	EXIT_IF_UNLIKELY_NULL(lpsuSck, FALSE);

	if (FALSE != lpsuSck->bBound)
	{ return FALSE; }
	
	unlink(lpsuSck->lpszPath);
	lRet = bind(lpsuSck->lDescriptor, (LPSOCKET_ADDRESS)&lpsuSck->sauSocketAddress, sizeof(SOCKET_ADDRESS_UNIX));
	lpsuSck->bBound = (0 == lRet) ? TRUE : FALSE;

	if (FALSE != lpsuSck->bBound)
	{ fchmod(lpsuSck->lDescriptor, 0644); }

	return lpsuSck->bBound;
}




_Success_(return != FALSE, ...)
PODNET_API
BOOL
ListenOnBoundSocketUnix(
	_In_		HANDLE			hSocket,
	_In_		ULONG			ulMaxNumberOfClientsInWaitingQueue
){
	EXIT_IF_UNLIKELY_NULL(hSocket, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hSocket), HANDLE_TYPE_SOCKETUNIX, FALSE);

	LPSOCKET_UNIX lpsuSck;
	LONG lRet;

	lpsuSck = OBJECT_CAST(hSocket, LPSOCKET_UNIX);
	EXIT_IF_UNLIKELY_NULL(lpsuSck, FALSE);

	if (FALSE != lpsuSck->bListen || FALSE == lpsuSck->bBound)
	{ return FALSE; }

	lRet = listen(lpsuSck->lDescriptor, (LONG)ulMaxNumberOfClientsInWaitingQueue);
	lpsuSck->bListen = (0 == lRet) ? TRUE : FALSE;

	return lpsuSck->bListen;
}




_Result_Null_On_Failure_
PODNET_API
HANDLE
AcceptConnectionUnix(
	_In_		HANDLE			hSocket
){
	EXIT_IF_UNLIKELY_NULL(hSocket, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hSocket), HANDLE_TYPE_SOCKETUNIX, FALSE);

	HANDLE hClient;
	LPSOCKET_UNIX lpsuSck;
	LPSOCKET_UNIX lpsuClient;
	LONG lDescriptor;
	socklen_t slSize;

	lpsuSck = OBJECT_CAST(hSocket, LPSOCKET_UNIX);
	EXIT_IF_UNLIKELY_NULL(lpsuSck, FALSE);

	if (FALSE == lpsuSck->bListen || FALSE == lpsuSck->bBound)
	{ return NULL_OBJECT; }

	lpsuClient = GlobalAllocAndZero(sizeof(SOCKET_UNIX));
	if (NULLPTR == lpsuClient)
	{ return NULL_OBJECT; }

	lDescriptor = accept(lpsuSck->lDescriptor, (LPSOCKET_ADDRESS)&(lpsuClient->sauSocketAddress), &slSize);
	if (-1 == lDescriptor)
	{ 
		FreeMemory(lpsuClient);
		return NULL_OBJECT;
	}

	lpsuClient->lDescriptor = lDescriptor;
	lpsuClient->bIsClient = TRUE;
	lpsuClient->lDomain = AF_UNIX;

	hClient = CreateHandleWithSingleInheritor(
		lpsuClient,
		&(lpsuClient->hThis),
		HANDLE_TYPE_SOCKETUNIX,
		__DestroySocketUnix,
		0,
		NULLPTR,
		0,
		NULLPTR,
		0,
		&(lpsuClient->ullId),
		0
	);

	if (NULL_OBJECT == hClient)
	{ 
		close(lpsuClient->lDescriptor);
		FreeMemory(lpsuClient);
		return NULL_OBJECT;
	}

	return hClient;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
SocketConnectUnix(
	_In_ 				HANDLE 				hSocket
){
	EXIT_IF_UNLIKELY_NULL(hSocket, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hSocket), HANDLE_TYPE_SOCKETUNIX, FALSE);

	LPSOCKET_UNIX lpsuSck;
	LONG lRet;

	lpsuSck = OBJECT_CAST(hSocket, LPSOCKET_UNIX);
	EXIT_IF_UNLIKELY_NULL(lpsuSck, FALSE);

	if (-1 == lpsuSck->lDescriptor)
	{ return FALSE; }

	lRet = connect(lpsuSck->lDescriptor, (LPSOCKET_ADDRESS)&(lpsuSck->sauSocketAddress), sizeof(SOCKET_ADDRESS_UNIX));
	lpsuSck->bIsClient = (0 == lRet) ? TRUE : FALSE;

	return lpsuSck->bIsClient;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
SocketCloseUnix(
	_In_ 				HANDLE 				hSocket
){
	EXIT_IF_UNLIKELY_NULL(hSocket, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hSocket), HANDLE_TYPE_SOCKETUNIX, FALSE);

	LPSOCKET_UNIX lpsuSck;
	LONG lRet;

	lpsuSck = OBJECT_CAST(hSocket, LPSOCKET_UNIX);
	EXIT_IF_UNLIKELY_NULL(lpsuSck, FALSE);

	if (-1 == lpsuSck->lDescriptor)
	{ return FALSE; }

	lRet = close(lpsuSck->lDescriptor);
	lpsuSck->bIsClient = (0 == lRet) ? TRUE : FALSE;

	return lpsuSck->bIsClient;
}




_Success_(return != 0, _Non_Locking_)
PODNET_API
UARCHLONG
SocketSendUnix(
	_In_ 				HANDLE 				hSocket,
	_In_ 				LPVOID 				lpData,
	_In_ 				UARCHLONG 			ualSize
){
	EXIT_IF_UNLIKELY_NULL(hSocket, (UARCHLONG)-1);
	EXIT_IF_UNLIKELY_NULL(lpData, (UARCHLONG)-1);
	EXIT_IF_UNLIKELY_NULL(ualSize, (UARCHLONG)-1);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hSocket), HANDLE_TYPE_SOCKETUNIX, FALSE);

	LPSOCKET_UNIX lpsuSck;
	ARCHLONG alRet;

	lpsuSck = OBJECT_CAST(hSocket, LPSOCKET_UNIX);
	EXIT_IF_UNLIKELY_NULL(lpsuSck, FALSE);

	if (-1 == lpsuSck->lDescriptor)
	{ return FALSE; }

	alRet = send(lpsuSck->lDescriptor, lpData, ualSize, 0);

	return (UARCHLONG)alRet;
}




_Success_(return != 0, _Non_Locking_)
PODNET_API
UARCHLONG
SocketReceiveUnix(
	_In_ 				HANDLE 				hSocket,
	_Out_ 				LPVOID 				lpOut,
	_In_ 				UARCHLONG 			ualSize
){
	EXIT_IF_UNLIKELY_NULL(hSocket, (UARCHLONG)-1);
	EXIT_IF_UNLIKELY_NULL(lpOut, (UARCHLONG)-1);
	EXIT_IF_UNLIKELY_NULL(ualSize, (UARCHLONG)-1);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hSocket), HANDLE_TYPE_SOCKETUNIX, FALSE);

	LPSOCKET_UNIX lpsuSck;
	ARCHLONG alRet;

	lpsuSck = OBJECT_CAST(hSocket, LPSOCKET_UNIX);
	EXIT_IF_UNLIKELY_NULL(lpsuSck, FALSE);

	if (-1 == lpsuSck->lDescriptor)
	{ return FALSE; }

	alRet = read(lpsuSck->lDescriptor, lpOut, ualSize);

	return (UARCHLONG)alRet;
}