/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CIpv6.h"
#include "../Ascii.h"
#include "../CompilationFlags.h"






_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPIPV6_ADDRESS
CreateIpv6AddressFromString(
	_In_Z_ 		LPCSTR RESTRICT		lpcszAddress
){
	EXIT_IF_UNLIKELY_NULL(lpcszAddress, NULLPTR);

	LPIPV6_ADDRESS lpip6Address;
	ULTRALONG_UNION ultuTemp;
	BYTE byIndex, byLength;
	CHAR cInput;
	lpip6Address = GlobalAllocAndZero(sizeof(IPV6_ADDRESS));
	/* cppcheck-suppress memleak */
	EXIT_IF_UNLIKELY_NULL(lpip6Address, NULLPTR);

	/* Check for two "::" */
	{
		LPSTR lpszTemp;
		lpszTemp = StringInString(lpcszAddress, "::");

		if (NULLPTR != lpszTemp)
		{
			lpszTemp = (LPVOID)((UARCHLONG)lpszTemp + 0x02U);
			if (ASCII_NULL_TERMINATOR != *lpszTemp)
			{
				lpszTemp = StringInString(lpszTemp, "::");
				if (NULLPTR != lpszTemp)
				{ 
					FreeMemory(lpip6Address);
					return NULLPTR; 
				}
			}
		}
	}


	/* Zero Address */
	if (0 == StringCompare(lpcszAddress, IPV6_ADDRESS_STRING_NULL))
	{ return lpip6Address; }

	/* Loopback */
	if (0 == StringCompare(lpcszAddress, IPV6_ADDRESS_STRING_LOOPBACK))
	{
		lpip6Address->ultuData.ullX[1] = 0x100000000000000;
		lpip6Address->ultuData.ullX[0] = 0x00;
		return lpip6Address;
	}

	/* Try StringScanFormat If Full IP. */
	if (8 == StringScanFormat(
		lpcszAddress,
		"%hx:%hx:%hx:%hx:%hx:%hx:%hx:%hx",
		&(lpip6Address->ultuData.usX[7]),
		&(lpip6Address->ultuData.usX[6]),
		&(lpip6Address->ultuData.usX[5]),
		&(lpip6Address->ultuData.usX[4]),
		&(lpip6Address->ultuData.usX[3]),
		&(lpip6Address->ultuData.usX[2]),
		&(lpip6Address->ultuData.usX[1]),
		&(lpip6Address->ultuData.usX[0])
	)){ JUMP(__Convert); } 

	/* Failed, Re-Zero It */
	ZeroMemory(lpip6Address, sizeof(IPV6_ADDRESS));
	byLength = StringLength(lpcszAddress);



	/* Begin Main Lexer Loops */

	/*
		We will start from the beginning. Checking for a colon, 
		when we run into a colon, we will set bSet to TRUE. If 
		another colon is found while bSet is TRUE, we will 
		handle the logic for the '::'. 
		As each block is read in, we keep track as to where each
		one begins, and ends. We fill up a temp buffer, and read
		it into the IPv6 Struct.
	*/

	BOOL bSet, bHadDouble;
	BYTE byOffset, byHextet;
	CSTRING csTemp[16] = {0};
	CSTRING csReverse[16] = {0};

	byOffset = 0;
	byHextet = 7; 
	bSet = FALSE;
	bHadDouble = FALSE;

	for (
		byIndex = 0;
		byIndex < byLength && byHextet < 8;
		byIndex++
	){
		cInput = lpcszAddress[byIndex];
		
		if (byIndex == byLength - 1)
		{
			BYTE byTemp, byTempLength;

			/* Zero the buffer */
			ZeroMemory(csTemp, sizeof(csTemp));

			LPSTR lpszTemp;
			
			/* This is much more efficient way of getting the last hextet */ 
			lpszTemp = (LPSTR)((UARCHLONG)lpcszAddress + (UARCHLONG)byOffset);
			byTempLength = StringLength(lpszTemp);

			/* Copy data from a constant string, so we can manipulate */
			StringCopySafe(
				csTemp,
				lpszTemp,
				sizeof(csTemp)
			);

			/* Stack Smashing Prevention 9000 */
			if (byTempLength > 4)
			{ byTempLength = 4; }

			/* Check For NULL Terminator */
			for (
				byTemp = 0;
				byTemp < byTempLength;
				byTemp ++
			){ 
				if (ASCII_NULL_TERMINATOR == csTemp[byTemp])
				{ csTemp[byTemp] = ASCII_0; }
			}
			
			/* Store the USHORT value */
			StringScanFormat(
				csTemp,
				"%hx",
				&(lpip6Address->ultuData.usX[byHextet])
			);
			
			bSet = TRUE;
			byOffset = byIndex + 1;
			byHextet--;
			break;
		}

		switch (cInput)
		{
			case ASCII_COLON:
			{
				/* Handle :: logic */
				if (TRUE == bSet)
				{
					/* Can only have 1 Compression */
					if (TRUE == bHadDouble)
					{ 
						FreeMemory(lpip6Address);
						return NULLPTR; 
					}

					BYTE byColonCount;
					LPSTR lpszNext;

					/*
						Reset the byHextet to 0
						Counts the number of ":" 
						Adds that to offset onto what
						hextet we are on
					*/
					byHextet = 0;		
					lpszNext = (LPSTR)LocateCharacterInMemory(
						(LPVOID)((UARCHLONG)lpcszAddress + (UARCHLONG)byIndex),
						(LONG)ASCII_COLON,
						StringLength((LPSTR)(LPVOID)lpcszAddress + (UARCHLONG)byIndex)
					);

					/* Just Return */
					if (ASCII_NULL_TERMINATOR == *lpszNext)
					{ return lpip6Address; }

					/* Must be 0 */
					byColonCount = 0;

					/* Get the number of ":" */
					while (NULLPTR != (lpszNext = (LPSTR)LocateCharacterInMemory(
						(LPVOID)((UARCHLONG)lpszNext + (UARCHLONG)0x01),
						(LONG)ASCII_COLON,
						StringLength(lpszNext)
					))){ byColonCount++; }

					byHextet += byColonCount;
					byOffset++;
					bSet = FALSE;
					bHadDouble = TRUE;
					break;
				}
				

				/* Get the hextet */
				BYTE byTemp;
				
				ZeroMemory(csTemp, sizeof(csTemp));
				ZeroMemory(csReverse, sizeof(csReverse));

				for (
					byTemp = byIndex - byOffset;
					byTemp > 0 && byTemp < 64;
					byTemp--
				){ 	csTemp[byTemp-1] = lpcszAddress[byIndex - byTemp];}
				
				csReverse[0] = csTemp[3];
				csReverse[1] = csTemp[2];
				csReverse[2] = csTemp[1];
				csReverse[3] = csTemp[0];

				/* Check For NULL Terminator */
				for (
					byTemp = 0;
					byTemp < 4;
					byTemp ++
				){ 
					if (ASCII_NULL_TERMINATOR == csReverse[byTemp])
					{ csReverse[byTemp] = ASCII_0; }
				}

				StringScanFormat(
					csReverse,
					"%hx",
					&(lpip6Address->ultuData.usX[byHextet])
				);
					
				bSet = TRUE;
				byOffset = byIndex + 1;
				byHextet--;

				break;
			}
			default:
			{ 
				bSet = FALSE;
				break; 
			}
		}
	}


	__Convert:
	/* Convert Backwards */

	ultuTemp.usX[0] = lpip6Address->ultuData.usX[7];
	ultuTemp.usX[1] = lpip6Address->ultuData.usX[6];
	ultuTemp.usX[2] = lpip6Address->ultuData.usX[5];
	ultuTemp.usX[3] = lpip6Address->ultuData.usX[4];
	ultuTemp.usX[4] = lpip6Address->ultuData.usX[3];
	ultuTemp.usX[5] = lpip6Address->ultuData.usX[2];
	ultuTemp.usX[6] = lpip6Address->ultuData.usX[1];
	ultuTemp.usX[7] = lpip6Address->ultuData.usX[0];

	ultuTemp.usX[0] = LITTLE_TO_BIG_ENDIAN_16(ultuTemp.usX[0]);
	ultuTemp.usX[1] = LITTLE_TO_BIG_ENDIAN_16(ultuTemp.usX[1]);
	ultuTemp.usX[2] = LITTLE_TO_BIG_ENDIAN_16(ultuTemp.usX[2]);
	ultuTemp.usX[3] = LITTLE_TO_BIG_ENDIAN_16(ultuTemp.usX[3]);
	ultuTemp.usX[4] = LITTLE_TO_BIG_ENDIAN_16(ultuTemp.usX[4]);
	ultuTemp.usX[5] = LITTLE_TO_BIG_ENDIAN_16(ultuTemp.usX[5]);
	ultuTemp.usX[6] = LITTLE_TO_BIG_ENDIAN_16(ultuTemp.usX[6]);
	ultuTemp.usX[7] = LITTLE_TO_BIG_ENDIAN_16(ultuTemp.usX[7]);

	lpip6Address->ultuData = ultuTemp;

	return lpip6Address;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
DestroyIpv6Address(
	_In_		LPIPV6_ADDRESS RESTRICT	lpip6Address
){
	EXIT_IF_UNLIKELY_NULL(lpip6Address, FALSE);
	FreeMemory(lpip6Address);
	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
InitializeIpv6Address(
	_In_ 		LPIPV6_ADDRESS RESTRICT	lpip6Address,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszAddress
){
	EXIT_IF_UNLIKELY_NULL(lpcszAddress, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpip6Address, FALSE);

	ULTRALONG_UNION ultuTemp;
	BYTE byIndex, byLength;
	CHAR cInput;

	ZeroMemory(lpip6Address, sizeof(IPV6_ADDRESS));

	/* Check for two "::" */
	{
		LPSTR lpszTemp;
		lpszTemp = StringInString(lpcszAddress, "::");

		if (NULLPTR != lpszTemp)
		{
			lpszTemp = (LPVOID)((UARCHLONG)lpszTemp + 0x02U);
			if (ASCII_NULL_TERMINATOR != *lpszTemp)
			{
				lpszTemp = StringInString(lpszTemp, "::");
				if (NULLPTR != lpszTemp)
				{ return FALSE; }
			}
		}
	}


	/* Zero Address */
	if (0 == StringCompare(lpcszAddress, IPV6_ADDRESS_STRING_NULL))
	{ return TRUE; }

	/* Loopback */
	if (0 == StringCompare(lpcszAddress, IPV6_ADDRESS_STRING_LOOPBACK))
	{
		lpip6Address->ultuData.ullX[1] = 0x100000000000000;
		lpip6Address->ultuData.ullX[0] = 0x00;
		return TRUE;
	}

	/* Try StringScanFormat If Full IP. */
	if (8 == StringScanFormat(
		lpcszAddress,
		"%hx:%hx:%hx:%hx:%hx:%hx:%hx:%hx",
		&(lpip6Address->ultuData.usX[7]),
		&(lpip6Address->ultuData.usX[6]),
		&(lpip6Address->ultuData.usX[5]),
		&(lpip6Address->ultuData.usX[4]),
		&(lpip6Address->ultuData.usX[3]),
		&(lpip6Address->ultuData.usX[2]),
		&(lpip6Address->ultuData.usX[1]),
		&(lpip6Address->ultuData.usX[0])
	)){ JUMP(__Convert); } 

	/* Failed, Re-Zero It */
	ZeroMemory(lpip6Address, sizeof(IPV6_ADDRESS));
	byLength = StringLength(lpcszAddress);



	/* Begin Main Lexer Loops */

	/*
		We will start from the beginning. Checking for a colon, 
		when we run into a colon, we will set bSet to TRUE. If 
		another colon is found while bSet is TRUE, we will 
		handle the logic for the '::'. 
		As each block is read in, we keep track as to where each
		one begins, and ends. We fill up a temp buffer, and read
		it into the IPv6 Struct.
	*/

	BOOL bSet, bHadDouble;
	BYTE byOffset, byHextet;
	CSTRING csTemp[16] = {0};
	CSTRING csReverse[16] = {0};

	byOffset = 0;
	byHextet = 7; 
	bSet = FALSE;
	bHadDouble = FALSE;

	for (
		byIndex = 0;
		byIndex < byLength && byHextet < 8;
		byIndex++
	){
		cInput = lpcszAddress[byIndex];
		
		if (byIndex == byLength - 1)
		{
			BYTE byTemp, byTempLength;

			/* Zero the buffer */
			ZeroMemory(csTemp, sizeof(csTemp));

			LPSTR lpszTemp;
			
			/* This is much more efficient way of getting the last hextet */ 
			lpszTemp = (LPSTR)((UARCHLONG)lpcszAddress + (UARCHLONG)byOffset);
			byTempLength = StringLength(lpszTemp);

			/* Copy data from a constant string, so we can manipulate */
			StringCopySafe(
				csTemp,
				lpszTemp,
				sizeof(csTemp)
			);

			/* Stack Smashing Prevention 9000 */
			if (byTempLength > 4)
			{ byTempLength = 4; }

			/* Check For NULL Terminator */
			for (
				byTemp = 0;
				byTemp < byTempLength;
				byTemp ++
			){ 
				if (ASCII_NULL_TERMINATOR == csTemp[byTemp])
				{ csTemp[byTemp] = ASCII_0; }
			}
			
			/* Store the USHORT value */
			StringScanFormat(
				csTemp,
				"%hx",
				&(lpip6Address->ultuData.usX[byHextet])
			);
			
			bSet = TRUE;
			byOffset = byIndex + 1;
			byHextet--;
			break;
		}

		switch (cInput)
		{
			case ASCII_COLON:
			{
				/* Handle :: logic */
				if (TRUE == bSet)
				{
					/* Can only have 1 Compression */
					if (TRUE == bHadDouble)
					{ return FALSE; }

					BYTE byColonCount;
					LPSTR lpszNext;

					/*
						Reset the byHextet to 0
						Counts the number of ":" 
						Adds that to offset onto what
						hextet we are on
					*/
					byHextet = 0;		
					lpszNext = (LPSTR)LocateCharacterInMemory(
						(LPVOID)((UARCHLONG)lpcszAddress + (UARCHLONG)byIndex),
						(LONG)ASCII_COLON,
						StringLength((LPSTR)(LPVOID)lpcszAddress + (UARCHLONG)byIndex)
					);

					/* Just Return */
					if (ASCII_NULL_TERMINATOR == *lpszNext)
					{ return TRUE; }

					/* Must be 0 */
					byColonCount = 0;

					/* Get the number of ":" */
					while (NULLPTR != (lpszNext = (LPSTR)LocateCharacterInMemory(
						(LPVOID)((UARCHLONG)lpszNext + (UARCHLONG)0x01),
						(LONG)ASCII_COLON,
						StringLength(lpszNext)
					))){ byColonCount++; }

					byHextet += byColonCount;
					byOffset++;
					bSet = FALSE;
					bHadDouble = TRUE;
					break;
				}
				

				/* Get the hextet */
				BYTE byTemp;
				
				ZeroMemory(csTemp, sizeof(csTemp));
				ZeroMemory(csReverse, sizeof(csReverse));

				for (
					byTemp = byIndex - byOffset;
					byTemp > 0 && byTemp < 64;
					byTemp--
				){ 	csTemp[byTemp-1] = lpcszAddress[byIndex - byTemp];}
				
				csReverse[0] = csTemp[3];
				csReverse[1] = csTemp[2];
				csReverse[2] = csTemp[1];
				csReverse[3] = csTemp[0];

				/* Check For NULL Terminator */
				for (
					byTemp = 0;
					byTemp < 4;
					byTemp ++
				){ 
					if (ASCII_NULL_TERMINATOR == csReverse[byTemp])
					{ csReverse[byTemp] = ASCII_0; }
				}

				StringScanFormat(
					csReverse,
					"%hx",
					&(lpip6Address->ultuData.usX[byHextet])
				);
					
				bSet = TRUE;
				byOffset = byIndex + 1;
				byHextet--;

				break;
			}
			default:
			{ 
				bSet = FALSE;
				break; 
			}
		}
	}


	__Convert:
	/* Convert Backwards */

	ultuTemp.usX[0] = lpip6Address->ultuData.usX[7];
	ultuTemp.usX[1] = lpip6Address->ultuData.usX[6];
	ultuTemp.usX[2] = lpip6Address->ultuData.usX[5];
	ultuTemp.usX[3] = lpip6Address->ultuData.usX[4];
	ultuTemp.usX[4] = lpip6Address->ultuData.usX[3];
	ultuTemp.usX[5] = lpip6Address->ultuData.usX[2];
	ultuTemp.usX[6] = lpip6Address->ultuData.usX[1];
	ultuTemp.usX[7] = lpip6Address->ultuData.usX[0];

	ultuTemp.usX[0] = LITTLE_TO_BIG_ENDIAN_16(ultuTemp.usX[0]);
	ultuTemp.usX[1] = LITTLE_TO_BIG_ENDIAN_16(ultuTemp.usX[1]);
	ultuTemp.usX[2] = LITTLE_TO_BIG_ENDIAN_16(ultuTemp.usX[2]);
	ultuTemp.usX[3] = LITTLE_TO_BIG_ENDIAN_16(ultuTemp.usX[3]);
	ultuTemp.usX[4] = LITTLE_TO_BIG_ENDIAN_16(ultuTemp.usX[4]);
	ultuTemp.usX[5] = LITTLE_TO_BIG_ENDIAN_16(ultuTemp.usX[5]);
	ultuTemp.usX[6] = LITTLE_TO_BIG_ENDIAN_16(ultuTemp.usX[6]);
	ultuTemp.usX[7] = LITTLE_TO_BIG_ENDIAN_16(ultuTemp.usX[7]);

	lpip6Address->ultuData = ultuTemp;

	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
ConvertIpv6AddressToString(
	_In_ 		LPIPV6_ADDRESS RESTRICT lpip6Address,
	_In_ 		LPSTR 			lpszOut,
	_In_ 		ULONG 			ulBufferSize
){
	EXIT_IF_UNLIKELY_NULL(lpip6Address, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpszOut, FALSE);

	CSTRING csAddress[48], csBuffer[8];
	BYTE byIndex;
	ULONG ulLength;

	ULTRALONG_UNION ultuTemp;
	ultuTemp.usX[0] = lpip6Address->ultuData.usX[7];
	ultuTemp.usX[1] = lpip6Address->ultuData.usX[6];
	ultuTemp.usX[2] = lpip6Address->ultuData.usX[5];
	ultuTemp.usX[3] = lpip6Address->ultuData.usX[4];
	ultuTemp.usX[4] = lpip6Address->ultuData.usX[3];
	ultuTemp.usX[5] = lpip6Address->ultuData.usX[2];
	ultuTemp.usX[6] = lpip6Address->ultuData.usX[1];
	ultuTemp.usX[7] = lpip6Address->ultuData.usX[0];

	ultuTemp.usX[0] = BIG_TO_LITTLE_ENDIAN_16(ultuTemp.usX[0]);
	ultuTemp.usX[1] = BIG_TO_LITTLE_ENDIAN_16(ultuTemp.usX[1]);
	ultuTemp.usX[2] = BIG_TO_LITTLE_ENDIAN_16(ultuTemp.usX[2]);
	ultuTemp.usX[3] = BIG_TO_LITTLE_ENDIAN_16(ultuTemp.usX[3]);
	ultuTemp.usX[4] = BIG_TO_LITTLE_ENDIAN_16(ultuTemp.usX[4]);
	ultuTemp.usX[5] = BIG_TO_LITTLE_ENDIAN_16(ultuTemp.usX[5]);
	ultuTemp.usX[6] = BIG_TO_LITTLE_ENDIAN_16(ultuTemp.usX[6]);
	ultuTemp.usX[7] = BIG_TO_LITTLE_ENDIAN_16(ultuTemp.usX[7]);


	ZeroMemory(csAddress, sizeof(csAddress));
	ZeroMemory(csBuffer, sizeof(csBuffer));

	for (
		byIndex = 7;
		byIndex > 0;
		byIndex--
	){
		StringPrintFormatSafe(
			csBuffer,
			sizeof(csBuffer),
			"%hx:",
			ultuTemp.usX[byIndex]
		);

		StringConcatenate(
			csAddress,
			csBuffer
		);
	}

	/* Do the last one */
	StringPrintFormatSafe(
		csBuffer,
		sizeof(csBuffer),
		"%hx",
		ultuTemp.usX[0]
	);

	StringConcatenate(
		csAddress,
		csBuffer
	);

	ulLength = StringLength(csAddress);

	if (ulLength > ulBufferSize)
	{ return FALSE; }
	
	CopyMemory(lpszOut, csAddress, sizeof(CHAR) * ulLength);
	return TRUE; 
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPSTR 
ConvertIpv6AddressToStringByAlloc(
	_In_ 		LPIPV6_ADDRESS RESTRICT lpip6Address
){
	CSTRING csAddress[48], csBuffer[8];
	BYTE byIndex;
	ULONG ulLength;
	LPSTR lpszOut;

	ULTRALONG_UNION ultuTemp;
	ultuTemp.usX[0] = lpip6Address->ultuData.usX[7];
	ultuTemp.usX[1] = lpip6Address->ultuData.usX[6];
	ultuTemp.usX[2] = lpip6Address->ultuData.usX[5];
	ultuTemp.usX[3] = lpip6Address->ultuData.usX[4];
	ultuTemp.usX[4] = lpip6Address->ultuData.usX[3];
	ultuTemp.usX[5] = lpip6Address->ultuData.usX[2];
	ultuTemp.usX[6] = lpip6Address->ultuData.usX[1];
	ultuTemp.usX[7] = lpip6Address->ultuData.usX[0];

	ultuTemp.usX[0] = BIG_TO_LITTLE_ENDIAN_16(ultuTemp.usX[0]);
	ultuTemp.usX[1] = BIG_TO_LITTLE_ENDIAN_16(ultuTemp.usX[1]);
	ultuTemp.usX[2] = BIG_TO_LITTLE_ENDIAN_16(ultuTemp.usX[2]);
	ultuTemp.usX[3] = BIG_TO_LITTLE_ENDIAN_16(ultuTemp.usX[3]);
	ultuTemp.usX[4] = BIG_TO_LITTLE_ENDIAN_16(ultuTemp.usX[4]);
	ultuTemp.usX[5] = BIG_TO_LITTLE_ENDIAN_16(ultuTemp.usX[5]);
	ultuTemp.usX[6] = BIG_TO_LITTLE_ENDIAN_16(ultuTemp.usX[6]);
	ultuTemp.usX[7] = BIG_TO_LITTLE_ENDIAN_16(ultuTemp.usX[7]);


	ZeroMemory(csAddress, sizeof(csAddress));
	ZeroMemory(csBuffer, sizeof(csBuffer));

	for (
		byIndex = 7;
		byIndex > 0;
		byIndex--
	){
		StringPrintFormatSafe(
			csBuffer,
			sizeof(csBuffer),
			"%hx:",
			ultuTemp.usX[byIndex]
		);

		StringConcatenate(
			csAddress,
			csBuffer
		);
	}

	/* Do the last one */
	StringPrintFormatSafe(
		csBuffer,
		sizeof(csBuffer),
		"%hx",
		ultuTemp.usX[0]
	);

	StringConcatenate(
		csAddress,
		csBuffer
	);

	ulLength = StringLength(csAddress);
	lpszOut = GlobalAllocAndZero(sizeof(CHAR) * ulLength + 0x01U);
	EXIT_IF_UNLIKELY_NULL(lpszOut, NULLPTR);

	CopyMemory(lpszOut, csAddress, ulLength);
	return lpszOut;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
ConvertIpv6AddressToCompressedString(
	_In_ 		LPIPV6_ADDRESS RESTRICT lpip6Address,
	_In_ 		LPSTR 			lpszOut,
	_In_ 		ULONG 			ulBufferSize
){
	EXIT_IF_UNLIKELY_NULL(lpip6Address, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpszOut, FALSE);

	CHAR csAddress[48], csBuffer[8];
	BYTE byHextet, byZeroCount;
	BOOL bDoubleColon, bJustPrintedDouble;
	ULONG ulSize;

	ULTRALONG_UNION ultuTemp;
	ultuTemp.usX[0] = lpip6Address->ultuData.usX[7];
	ultuTemp.usX[1] = lpip6Address->ultuData.usX[6];
	ultuTemp.usX[2] = lpip6Address->ultuData.usX[5];
	ultuTemp.usX[3] = lpip6Address->ultuData.usX[4];
	ultuTemp.usX[4] = lpip6Address->ultuData.usX[3];
	ultuTemp.usX[5] = lpip6Address->ultuData.usX[2];
	ultuTemp.usX[6] = lpip6Address->ultuData.usX[1];
	ultuTemp.usX[7] = lpip6Address->ultuData.usX[0];

	ultuTemp.usX[0] = BIG_TO_LITTLE_ENDIAN_16(ultuTemp.usX[0]);
	ultuTemp.usX[1] = BIG_TO_LITTLE_ENDIAN_16(ultuTemp.usX[1]);
	ultuTemp.usX[2] = BIG_TO_LITTLE_ENDIAN_16(ultuTemp.usX[2]);
	ultuTemp.usX[3] = BIG_TO_LITTLE_ENDIAN_16(ultuTemp.usX[3]);
	ultuTemp.usX[4] = BIG_TO_LITTLE_ENDIAN_16(ultuTemp.usX[4]);
	ultuTemp.usX[5] = BIG_TO_LITTLE_ENDIAN_16(ultuTemp.usX[5]);
	ultuTemp.usX[6] = BIG_TO_LITTLE_ENDIAN_16(ultuTemp.usX[6]);
	ultuTemp.usX[7] = BIG_TO_LITTLE_ENDIAN_16(ultuTemp.usX[7]);
	
	for (
		byHextet = 7, byZeroCount = 0;
		byHextet <= 7;
		byHextet--
	){
		if (0 == ultuTemp.usX[byHextet])
		{ byZeroCount++;}
	}
	
	if (byZeroCount <= 1)
	{ return ConvertIpv6AddressToString(lpip6Address, lpszOut, ulBufferSize); }
	
	ZeroMemory(csAddress, sizeof(csAddress));
	ZeroMemory(csBuffer, sizeof(csBuffer));
	bDoubleColon = FALSE;
	bJustPrintedDouble = FALSE;

	/* Print Null Address */
	if (0 == ultuTemp.ullX[0] && 0 == ultuTemp.ullX[1])
	{
		StringPrintFormatSafe(
			csAddress,
			sizeof(csAddress),
			"::"
		);

		ulSize = (sizeof(csAddress) < ulBufferSize) ? sizeof(csAddress) : ulBufferSize;
		CopyMemory(lpszOut, csAddress, ulSize);
		return TRUE;
	}

	byZeroCount = 0;

	for (
		byHextet = 7, byZeroCount = 0;
		byHextet <= 7;
		byHextet--
	){
		switch (ultuTemp.usX[byHextet])
		{
			
			/* Handle A Null Hextet */
			case 0:
			{
				byZeroCount++;
				if (0 == byHextet)
				{ JUMP(__DefaultCase); }
				break;
			}

			default:
			{
				__DefaultCase:
				ZeroMemory(csBuffer, sizeof(csBuffer));

				if (1 == byZeroCount)
				{
					/* Don't Prepend */
					if (0 == byHextet)
					{
						StringPrintFormatSafe(
							csBuffer,
							sizeof(csBuffer),
							"%hx",
							0
						);
					}
					else
					{
						StringPrintFormatSafe(
							csBuffer,
							sizeof(csBuffer),
							":%hx:%hx",
							0,
							ultuTemp.usX[byHextet]
						);
					}

					StringConcatenate(csAddress, csBuffer);
					byZeroCount = 0;
				}
				else if (1 < byZeroCount)
				{
					/* Already had '::' */
					if (TRUE == bDoubleColon)
					{ 
						for (
							;
							byZeroCount > 0;
							byZeroCount--
						){ 
							StringPrintFormatSafe(
								csBuffer,
								sizeof(csBuffer),
								":%hx",
								0
							);

							StringConcatenate(csAddress, csBuffer);
						}
					}
					/* Add '::' */
					else
					{
						StringPrintFormatSafe(
							csBuffer,
							sizeof(csBuffer),
							"%s",
							"::"
						);

						StringConcatenate(csAddress, csBuffer);
						bDoubleColon = TRUE;
						bJustPrintedDouble = TRUE;
						byZeroCount = 0;
					}
					
					if (0 == byHextet && 0 == ultuTemp.usX[byHextet])
					{ ZeroMemory(csBuffer, sizeof(csBuffer)); } 
					/* Don't Prepend : */
					else if (7 == byHextet)
					{
						StringPrintFormatSafe(
							csBuffer,
							sizeof(csBuffer),
							"%hx",
							ultuTemp.usX[byHextet]
						);
					}
					else if (TRUE == bJustPrintedDouble)
					{
						StringPrintFormatSafe(
							csBuffer,
							sizeof(csBuffer),
							"%hx",
							ultuTemp.usX[byHextet]
						);
						bJustPrintedDouble = FALSE;
					}
					else
					{
						StringPrintFormatSafe(
							csBuffer,
							sizeof(csBuffer),
							":%hx",
							ultuTemp.usX[byHextet]
						);
					}
					
					StringConcatenate(csAddress, csBuffer);
				}
				else
				{
					/* Don't Prepend : */
					if (7 == byHextet)
					{
						StringPrintFormatSafe(
							csBuffer,
							sizeof(csBuffer),
							"%hx",
							ultuTemp.usX[byHextet]
						);
					}
					else if (TRUE == bJustPrintedDouble)
					{
						StringPrintFormatSafe(
							csBuffer,
							sizeof(csBuffer),
							"%hx",
							ultuTemp.usX[byHextet]
						);
						bJustPrintedDouble = FALSE;
					}
					else
					{
						StringPrintFormatSafe(
							csBuffer,
							sizeof(csBuffer),
							":%hx",
							ultuTemp.usX[byHextet]
						);
					}
					
					StringConcatenate(csAddress, csBuffer);
				}
			}

		}
	}

	ulSize = (sizeof(csAddress) < ulBufferSize) ? sizeof(csAddress) : ulBufferSize;
	CopyMemory(lpszOut, csAddress, ulSize);
	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
CompressIpv6AddressString(
	_In_ 		LPCSTR RESTRICT 	lpcszAddress,
	_Out_ 		LPSTR RESTRICT 		lpszOut,
	_In_ 		ULONG 			ulBufferSize	
){
	EXIT_IF_UNLIKELY_NULL(lpcszAddress, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpszOut, FALSE);
	
	LPIPV6_ADDRESS lpip6Address;
	BOOL bRet;

	lpip6Address = CreateIpv6AddressFromString(lpcszAddress);
	EXIT_IF_UNLIKELY_NULL(lpip6Address, FALSE);

	bRet = ConvertIpv6AddressToCompressedString(lpip6Address, lpszOut, ulBufferSize);

	FreeMemory(lpip6Address);
	return bRet;
}




_Success_(return != IPV6_ADDRESS_NOT_VALID, _Non_Locking_)
PODNET_API
IPV6_ADDRESS_TYPE
GetIpv6AddressType(
	_In_ 		LPIPV6_ADDRESS RESTRICT lpip6Address
){
	EXIT_IF_UNLIKELY_NULL(lpip6Address, IPV6_ADDRESS_NOT_VALID);

	if (0 == lpip6Address->ultuData.ullX[0] && 0 == lpip6Address->ultuData.ullX[1])
	{ return IPV6_ADDRESS_NULL; }

	if (1 == lpip6Address->ultuData.ullX[0] && 0 == lpip6Address->ultuData.ullX[1])
	{ return IPV6_ADDRESS_LOOPBACK; }

	if (0xFFFF0000 == lpip6Address->ultuData.ulX[2] && 
		0 == lpip6Address->ultuData.ulX[0] && 
		0 == lpip6Address->ultuData.ulX[1]
	){ return IPV6_ADDRESS_IPV4_TRANSLATION; }

	if (0x100000000000000ULL == lpip6Address->ultuData.ullX[1])
	{ return IPV6_ADDRESS_DISCARD_PREFIX; }

	return IPV6_ADDRESS_NOT_VALID;
}




_Success_(return != IPV6_ADDRESS_NOT_VALID, _Non_Locking_)
PODNET_API
IPV6_ADDRESS_TYPE
GetIpv6AddressTypeByString(
	_In_ 		LPCSTR RESTRICT 	lpcsz6Address
){
	EXIT_IF_UNLIKELY_NULL(lpcsz6Address, IPV6_ADDRESS_NOT_VALID);

	IPV6_ADDRESS_TYPE ip6atType;
	LPIPV6_ADDRESS lpip6Address;
	
	lpip6Address = CreateIpv6AddressFromString(lpcsz6Address);
	EXIT_IF_UNLIKELY_NULL(lpip6Address, IPV6_ADDRESS_NOT_VALID);

	ip6atType = GetIpv6AddressType(lpip6Address);
	DestroyIpv6Address(lpip6Address);

	return ip6atType;
}




_Success_(return != IPV4_NULL_ADDRESS, _Non_Locking_)
PODNET_API 
IPV4_ADDRESS 
ConvertIpv6AddressToIpv4(
	_In_ 		LPIPV6_ADDRESS 		lpip6Address
){
	EXIT_IF_UNLIKELY_NULL(lpip6Address, IPV4_NULL_ADDRESS);
	EXIT_IF_NOT_VALUE(
		GetIpv6AddressType(lpip6Address), 
		IPV6_ADDRESS_IPV4_TRANSLATION, 
		IPV4_NULL_ADDRESS
	);

	IPV4_ADDRESS ip4Address;

	ip4Address.ulData = lpip6Address->ultuData.ulX[3];
	return ip4Address;
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
LPIPV6_ADDRESS
ConvertIpv4AddressToIpv6ByCreate(
	_In_ 		IPV4_ADDRESS		ip4Address
){
	LPIPV6_ADDRESS lpip6Address;

	lpip6Address = CreateIpv6AddressFromString("::ffff:0:0");
	lpip6Address->ultuData.ulX[3] = ip4Address.ulData;
	return lpip6Address;
}




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
ConvertIpv4AddressToIpv6(
	_In_		IPV4_ADDRESS 		ip4Address,
	_Out_ 		LPIPV6_ADDRESS		lpip6Address
){
	EXIT_IF_UNLIKELY_NULL(lpip6Address, FALSE);

	lpip6Address->ultuData.ulX[0] = 0;
	lpip6Address->ultuData.ulX[1] = 0;
	lpip6Address->ultuData.ulX[2] = 0xFFFF0000;
	lpip6Address->ultuData.ulX[3] = ip4Address.ulData;

	return TRUE;
}




_Success_(return != 0, _Non_Locking_)
PODNET_API
LPIPV6_ADDRESS
GetCurrentPublicIpv6Address(
	VOID
){
	HANDLE hRequest;
	CSTRING csResponse[4096];
	CSTRING csBuffer[32];
	LPSTR lpszOut;

	ZeroMemory(csResponse, sizeof(csResponse));
	ZeroMemory(csBuffer, sizeof(csBuffer));
	lpszOut = (LPSTR)csResponse;

	hRequest = CreateHttpsRequest(
		HTTP_REQUEST_GET,
		HTTP_VERSION_1_1, 
		HTTP_GET_IPV6_SERVER,
		443,
		"/",
		HTTP_CONNECTION_CLOSE,
		NULLPTR,
		HTTP_ACCEPT_PLAIN_TEXT,
		TRUE,
		TRUE
	);

	ExecuteHttpsRequest(hRequest, csResponse, sizeof(csResponse), NULLPTR);
	CopyMemory(csBuffer, lpszOut, StringLength(lpszOut) - 0x01U);

	DestroyObject(hRequest);
	return CreateIpv6AddressFromString(csBuffer);
}