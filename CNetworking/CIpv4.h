/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CIPV4_H
#define CIPV4_H


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../CMemory/CMemory.h"




typedef struct __IPV4_ADDRESS
{
	union
	{
		ULONG 	ulData;	
		struct
		{ BYTE byData[4]; };
	};
} IPV4_ADDRESS, *LPIPV4_ADDRESS;

/* Must be after typedef */
#include "CSocket4.h"
#include "CHttpsRequest.h"

/* 000.000.000.000 + NULLTERM = 0x10 */
#define IPV4_ADDRESS_STRING_LENGTH 	0x10

#define IPV4_NULL_ADDRESS		((IPV4_ADDRESS){0})

#define IPV4_ADDRESSES_EQUAL(X,Y)	(((X).ulData == (Y).ulData) ? TRUE : FALSE)


typedef enum __IPV4_ADDRESS_CLASS
{
	IPV4_CLASS_A 		= 1,
	IPV4_CLASS_B,
	IPV4_CLASS_C,
	IPV4_CLASS_D,
	IPV4_CLASS_E,
	IPV4_CLASS_UNKNOWN
} IPV4_ADDRESS_CLASS;


#define IPV4_ADDRESS_IS_CLASS_A(X) 	(((X).byData[0] < 127) ? TRUE : FALSE)
#define IPV4_ADDRESS_IS_CLASS_B(X) 	(((X).byData[0] > 127 && (X).byData[0] < 192) ? TRUE : FALSE)
#define IPV4_ADDRESS_IS_CLASS_C(X)	(((X).byData[0] > 191 && (X).byData[0] < 223) ? TRUE : FALSE)
#define IPV4_ADDRESS_IS_CLASS_D(X) 	(((X).byData[0] > 223 && (X).byData[0] < 240) ? TRUE : FALSE)
#define IPV4_ADDRESS_IS_CLASS_E(X) 	(((X).byData[0] > 239 && (X).byData[0] <= 255) ? TRUE : FALSE)



_Success_(return != NULL, _Non_Locking_)
PODNET_API
IPV4_ADDRESS
CreateIpv4Address(
	_In_ 	BYTE 		byFirstOctet,
	_In_ 	BYTE 		bySecondOctet,
	_In_ 	BYTE 		byThirdOctet,
	_In_ 	BYTE 		byFourthOctet
);




_Success_(return != NULL, _Non_Locking_)
PODNET_API
IPV4_ADDRESS
CreateIpv4AddressFromString(
	_In_Z_ 	LPCSTR 	RESTRICT 	lpcszIpAddress
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL 
ConvertIpv4AddressToString(
	_In_ 	IPV4_ADDRESS 		ipv4Address,
	_Out_ 	LPSTR 	RESTRICT 	lpszBufferOut,
	_In_ 	ULONG 			ulBufferSize
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
IPV4_ADDRESS_CLASS
GetIpv4AddressClass(
	_In_ 	IPV4_ADDRESS 		ipv4Address
);




_Success_(return != 0, _Non_Locking_)
PODNET_API
IPV4_ADDRESS
GetCurrentPublicIpv4Address(
	VOID
);




_Success_(return != FALSE, _Non_Locking_)
PODNET_API
BOOL
IsIpv4Address(
	_In_Z_	LPCSTR RESTRICT		lpcszIpAddress
);




#endif