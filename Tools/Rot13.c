#include "../CAlgorithms/CRot13.h"


LONG Main(LONG lArgCount, DLPSTR dlpszArgValues)
{
        UNREFERENCED_PARAMETER(lArgCount);
        UNREFERENCED_PARAMETER(dlpszArgValues);
        CSTRING csIn[256];
        StringCopy(csIn, dlpszArgValues[1]);
        Rot13(csIn);
        PrintFormat("%s\n", csIn);

        return 0;
}