#include "../../CThread/CThread.h"
#include "../../CSystem/CSystem.h"


LPVOID
SpinLockProc(
	_In_ 		LPVOID 		lpParam
){
	UNREFERENCED_PARAMETER(lpParam);
	INFINITE_LOOP() {}
	return NULLPTR;
}



LONG Main(LONG lArgCount, DLPSTR dlpszArgValues)
{
	if (2 > lArgCount)
	{
		PrintFormat("Please specify the number of threads\n");
		return 1;
	}

	UARCHLONG ualIndex;
	LONG lThreads;
	HANDLE hThread;	

	StringScanFormat(dlpszArgValues[1], "%d", &lThreads);


	for (
		ualIndex = 0;
		ualIndex < lThreads;
		ualIndex++
	){ hThread = CreateThread(SpinLockProc, NULLPTR, NULLPTR); }
	
	WaitForSingleObject(hThread, INFINITE_WAIT_TIME);

	return 0;
}
