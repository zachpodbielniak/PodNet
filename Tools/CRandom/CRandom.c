#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void PrintStr(char *cString)
{
  int i;
  i = 0;
  //putchar('.');
  //putchar('/');
  while (cString[i])
  {
    if (cString[i] == 0x20)
      putchar('\\');
    putchar(cString[i]);
    i++;
  }
  putchar('\n');
}

int main(int argc, char **argv)
{
  int iRandom;
  srand(time(NULL));
  iRandom = (rand() % (argc-1)) + 1;
 // printf("%s\n", argv[iRandom]);
  PrintStr(argv[iRandom]);
  fflush(stdout);
  return 0;
}
