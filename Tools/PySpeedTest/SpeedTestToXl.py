#!/usr/bin/python
#Need speedtest-cli and speedtest-cli-extras installed
import sys
import datetime
import openpyxl

fproto = "/home/zach/Documents/Data/SpeedTest/SpeedTest-"
now = datetime.datetime.now()
year = now.year
month = now.month
d = str(month) + "-" + str(year)
f = open("results.txt", "r")
wb = openpyxl.load_workbook(fproto + d + ".xlsx")
data = wb.get_sheet_by_name("Data")
s = f.read().split("\t")
mx = data.max_row + 1
miles = float(s[5]) * 0.6214
s[5] = "%.2f" % miles
for i in range(0,8):
        if (i > 4):
                data.cell(row=mx, column=i+1).value = float(s[i])
        else:
                data.cell(row=mx, column=i+1).value = s[i]

wb.save(fproto + d + ".xlsx")
wb.close()            
