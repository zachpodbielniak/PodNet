#include <stdio.h>

void PrintFib(long long *lNumbers)
{
	printf("%d %d ", lNumbers[0], lNumbers[1]);
}

int main(int argc, char **argv)
{
	long long lNumberOfTimes;
	if (argc < 2)
		lNumberOfTimes = 23;
	else lNumberOfTimes = (long)argv[1];
	long long lNumbers[3] = {0, 1, 1};
	long long lCount;
	for (lCount = 0; lCount < lNumberOfTimes; lCount++)
	{
		PrintFib(lNumbers);
		lNumbers[0] = lNumbers[1];
		lNumbers[1] = lNumbers[2];
		lNumbers[2] = lNumbers[0] + lNumbers[1];
		
		lNumbers[0] = lNumbers[1];
		lNumbers[1] = lNumbers[2];
		lNumbers[2] = lNumbers[0] + lNumbers[1];
		
	}
	return 0;
}
