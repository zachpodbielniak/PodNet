/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CMutex.h"



typedef struct _MUTEX
{
	INHERITS_FROM_HANDLE();
	HANDLE 		hLockingThread;
	pthread_mutex_t	ptmMutex;
	BOOL		bCurrentlyLocked;
} MUTEX, *LPMUTEX;



_Call_Back_
INTERNAL_OPERATION
BOOL
__DestroyMutex(
	_In_				HDERIVATIVE			hDerivative,
	_In_Opt_			ULONG				ulFlags
){
	UNREFERENCED_PARAMETER(ulFlags);
	SET_ERROR_AND_EXIT_IF_NULL(hDerivative, ERROR_MUTEX_DATA_IS_NON_EXISTENT, FALSE);
	LPMUTEX lpmtxMutex;
	lpmtxMutex = (LPMUTEX)hDerivative;

	pthread_mutex_destroy(&(lpmtxMutex->ptmMutex));
	FreeMemory(lpmtxMutex);
	return TRUE;
}




_Call_Back_ 
INTERNAL_OPERATION
BOOL
__WaitForMutex(
	_In_		HANDLE			hBase,
	_In_Opt_	ULONGLONG		ullMilliseconds
){
	SET_ERROR_AND_EXIT_IF_NULL(hBase, ERROR_MUTEX_HANDLE_SUPPLIED_WAS_NULL, FALSE);
	SET_ERROR_AND_EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hBase), HANDLE_TYPE_MUTEX, ERROR_MUTEX_HANDLE_SUPPLIED_WAS_NOT_MUTEX_HANDLE, FALSE);
	LPMUTEX lpmtxMutex;
	LONG lRet;
	BOOL bRet;
	lpmtxMutex = (LPMUTEX)GetHandleDerivative(hBase);
	SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(lpmtxMutex, ERROR_MUTEX_DATA_IS_NON_EXISTENT, FALSE);

	/* If this is already locked by the current thread, allow relocking by current thread */
	if (TRUE == lpmtxMutex->bCurrentlyLocked && GetCurrentThread() == lpmtxMutex->hLockingThread)
	{ bRet = TRUE; }
	else 
	{
		if (ullMilliseconds == INFINITE_WAIT_TIME)
		{ lRet = pthread_mutex_lock(&(lpmtxMutex->ptmMutex)); }
		else
		{
			TIMESPEC tmSpec;
			CreateTimeSpec(&tmSpec, ullMilliseconds);
			lRet = pthread_mutex_timedlock(&(lpmtxMutex->ptmMutex), &tmSpec);
		}

		bRet = (lRet == 0) ? TRUE : FALSE;

		if (TRUE == bRet)
		{ 
			lpmtxMutex->hLockingThread = GetCurrentThread(); 
			lpmtxMutex->bCurrentlyLocked = TRUE;
		}
	}

	return bRet;
}





_Result_Null_On_Failure_
_Success_(return != FALSE, _Non_Locking_)
PODNET_API
HANDLE
CreateMutex(
	VOID
){
	HANDLE hMutex;
	LPMUTEX lpmtxMutex;
	lpmtxMutex = (LPMUTEX)LocalAllocAndZero(sizeof(MUTEX));
	SET_ERROR_AND_EXIT_IF_NULL(lpmtxMutex, ERROR_MUTEX_COULD_NOT_ALLOC_DATA, NULLPTR);

	pthread_mutex_init(&(lpmtxMutex->ptmMutex), NULLPTR);

	hMutex = CreateHandleWithSingleInheritor(
			lpmtxMutex,
			&(lpmtxMutex->hThis),
			HANDLE_TYPE_MUTEX,
			__DestroyMutex,
			NULL,
			NULLPTR,
			NULL,
			NULLPTR,
			NULL,
			&(lpmtxMutex->ullId),
			NULL
	);
	SET_ERROR_AND_EXIT_IF_NULL(hMutex, ERROR_MUTEX_COULD_NOT_CREATE_HANDLE, NULLPTR);

	SetHandleEventWaitFunction(hMutex, __WaitForMutex, NULL);
	SetHandleReleaseFunction(hMutex, ReleaseMutex);

	return hMutex;
}




_Success_(return != FALSE, _Releases_Lock_(_Has_Lock_Kind_(_Lock_Kind_Mutex_)))
PODNET_API
BOOL
ReleaseMutex(
	_In_					HANDLE			hMutex
){

	SET_ERROR_AND_EXIT_IF_NULL(hMutex, ERROR_MUTEX_HANDLE_SUPPLIED_WAS_NULL, FALSE);
	SET_ERROR_AND_EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hMutex), HANDLE_TYPE_MUTEX, ERROR_MUTEX_HANDLE_SUPPLIED_WAS_NOT_MUTEX_HANDLE, FALSE);
	LPMUTEX lpmtxMutex;
	LONG lRet;
	BOOL bRet;
	HANDLE hTestThread;
	lpmtxMutex = (LPMUTEX)GetHandleDerivative(hMutex);
	SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(lpmtxMutex, ERROR_MUTEX_DATA_IS_NON_EXISTENT, FALSE);

	hTestThread = GetCurrentThread();
	SET_ERROR_AND_EXIT_IF_NOT_VALUE(hTestThread, lpmtxMutex->hLockingThread, ERROR_MUTEX_THREAD_ATTEMPTING_UNLOCK_WAS_NOT_LOCKING_THREAD, FALSE);
	DecrementObjectReferenceCount(hTestThread);

	lRet = pthread_mutex_unlock(&(lpmtxMutex->ptmMutex));

	bRet = (0 == lRet) ? TRUE : FALSE;

	if (TRUE == bRet)
	{ 
		lpmtxMutex->bCurrentlyLocked = FALSE;
		DecrementObjectReferenceCount(lpmtxMutex->hLockingThread); 
	}

	return bRet;
}