/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CCRITSEC_H
#define CCRITSEC_H

#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../CHandle/CHandle.h"
#include "../CError/CError.h"
#include "CSemaphore.h"


#include <sys/types.h>
#include <sys/shm.h>
#include <semaphore.h>
#include <fcntl.h>



_Result_Null_On_Failure_
_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HANDLE
CreateCriticalSection(
	VOID
);




_Result_Null_On_Failure_
_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HANDLE
CreateCriticalSectionAndSpecifySpinCount(
	_In_		ULONG		ulSpinCount
);




_Success_(return != FALSE, _Acquires_Lock_(hCritSec, _Lock_Kind_Semaphore_))
PODNET_API
BOOL
EnterCriticalSection(
	_In_		HANDLE		hCritSec
);




_Success_(return != FALSE, _Acquires_Lock_(hCritSec, _Lock_Kind_Semaphore_))
PODNET_API
BOOL
TryToEnterCriticalSection(
	_In_		HANDLE		hCritSec
);




_Success_(return != FALSE, _Releases_Lock_(hCritSec, _Lock_Kind_Semaphore_))
PODNET_API
BOOL
ExitCriticalSection(
	_In_		HANDLE		hCritSec
);




#endif