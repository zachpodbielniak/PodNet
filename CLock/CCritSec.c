/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CCritSec.h"
#include "../CClock/CClock.h"

#define DEFAULT_CRIT_SEC_SPIN_LOCK_VALUE		1024




typedef struct _CRIT_SEC
{
	INHERITS_FROM_HANDLE();
	HANDLE 		hSemaphore;
	ULONG		ulSpinLock;
} CRIT_SEC, *LPCRIT_SEC;




_Call_Back_
INTERNAL_OPERATION
BOOL
__WaitForCriticalSection(
	_In_			HANDLE		hCritSec,
	_In_Opt_		ULONGLONG	ullMilliSeconds
){
	EXIT_IF_NULL(hCritSec, FALSE);
	EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hCritSec), HANDLE_TYPE_CRIT_SEC, FALSE);
	LPCRIT_SEC lpCritSec;
	BOOL bRet;
	TIMESPEC tsClock;
	lpCritSec = (LPCRIT_SEC)GetHandleDerivative(hCritSec);
	EXIT_IF_NULL(lpCritSec, FALSE);
	HighResolutionClock(&tsClock);

 	bRet = WaitForSingleObject(lpCritSec->hSemaphore, 0);

	/* Try Lock Operation */
	if (0 == ullMilliSeconds)
	{ return bRet; }

	if (bRet != TRUE)
	{
		ULONG ulCount;
		ulCount = lpCritSec->ulSpinLock;
		for (; ulCount > 0; ulCount--)
		{
			bRet = WaitForSingleObject(lpCritSec->hSemaphore, 0);
			if (bRet == TRUE) goto Entered;
		}
		if (INFINITE_WAIT_TIME == ullMilliSeconds)
		{ bRet = WaitForSingleObject(lpCritSec->hSemaphore, INFINITE_WAIT_TIME); }
		else 
		{ bRet = WaitForSingleObject(lpCritSec->hSemaphore, ullMilliSeconds - HighResolutionClockAndGetMilliSeconds(&tsClock)); }
	}
	Entered:
	return bRet;
}




_Call_Back_
INTERNAL_OPERATION
BOOL
__ReleaseCriticalSection(
	_In_			HANDLE		hCritSec
){
	EXIT_IF_NULL(hCritSec, FALSE);
	EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hCritSec), HANDLE_TYPE_CRIT_SEC, FALSE);
	LPCRIT_SEC lpCritSec;
	lpCritSec = (LPCRIT_SEC)GetHandleDerivative(hCritSec);
	EXIT_IF_NULL(hCritSec, FALSE);
	return ReleaseSemaphore(lpCritSec->hSemaphore);
}




INTERNAL_OPERATION
BOOL
__DestroyCriticalSection(HDERIVATIVE hDerivative, ULONG ulFlags)
{
	UNREFERENCED_PARAMETER(ulFlags);
	EXIT_IF_NULL(hDerivative, FALSE);
	LPCRIT_SEC lpCritSec;
	lpCritSec = (LPCRIT_SEC)hDerivative;
	DestroyHandle(lpCritSec->hSemaphore);
	FreeMemory(lpCritSec);
	return TRUE;
}




_Result_Null_On_Failure_
_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HANDLE
CreateCriticalSection(
	VOID
){
	HANDLE hHandle;
	LPCRIT_SEC lpCritSec;
	ULONGLONG ullID;

	lpCritSec = LocalAlloc(sizeof(CRIT_SEC));
	EXIT_IF_NULL(lpCritSec, 0);
	
	lpCritSec->hSemaphore = CreateSemaphore(1);

	lpCritSec->ulSpinLock = DEFAULT_CRIT_SEC_SPIN_LOCK_VALUE;

	hHandle = CreateHandleWithSingleInheritor(	(HDERIVATIVE)lpCritSec,
							&(lpCritSec->hThis),
							HANDLE_TYPE_CRIT_SEC,
							__DestroyCriticalSection,
							0,
							NULLPTR,
							0,
							NULLPTR,
							0,
							&ullID,
							0);

	EXIT_IF_NULL(hHandle, 0);

	SetHandleEventWaitFunction(hHandle, __WaitForCriticalSection, NULL);
	SetHandleReleaseFunction(hHandle, __ReleaseCriticalSection);

	return hHandle;
}




_Result_Null_On_Failure_
_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HANDLE
CreateCriticalSectionAndSpecifySpinCount(
	_In_					ULONG 			ulSpinCount
){
	HANDLE hHandle;
	LPCRIT_SEC lpCritSec;

	if (ulSpinCount < 1) ulSpinCount = 1;

	hHandle = CreateCriticalSection();
	EXIT_IF_NULL(hHandle, 0)

	lpCritSec = (LPCRIT_SEC)GetHandleDerivative(hHandle);
	EXIT_IF_NULL(hHandle, 0);

	lpCritSec->ulSpinLock = ulSpinCount;
	return hHandle;
}




_Success_(return != FALSE, _Acquires_Lock_(hCritSec, _Lock_Kind_Semaphore_))
PODNET_API
BOOL
EnterCriticalSection(
	_In_					HANDLE 			hCritSec
){
	EXIT_IF_NULL(hCritSec, FALSE);
	LPCRIT_SEC lpCritSec;
	BOOL bRet;
	lpCritSec = (LPCRIT_SEC)GetHandleDerivative(hCritSec);
	EXIT_IF_NULL(lpCritSec, FALSE);

 	bRet = WaitForSingleObject(lpCritSec->hSemaphore, 0);
	if (bRet != TRUE)
	{
		ULONG ulCount;
		ulCount = lpCritSec->ulSpinLock;
		for (; ulCount > 0; ulCount--)
		{
			bRet = WaitForSingleObject(lpCritSec->hSemaphore, 0);
			if (bRet == TRUE) goto Entered;
		}
		bRet = WaitForSingleObject(lpCritSec->hSemaphore, INFINITE_WAIT_TIME);
	}
	Entered:
	return bRet;
}




_Success_(return != FALSE, _Acquires_Lock_(hCritSec, _Lock_Kind_Semaphore_))
PODNET_API
BOOL
TryToEnterCriticalSection(
	_In_					HANDLE 			hCritSec
){
	EXIT_IF_NULL(hCritSec, FALSE)
	LPCRIT_SEC lpCritSec;
	BOOL bRet;
	lpCritSec = (LPCRIT_SEC)GetHandleDerivative(hCritSec);
	EXIT_IF_NULL(lpCritSec, FALSE);

	bRet = WaitForSingleObject(lpCritSec->hSemaphore, 0);
	if (bRet == FALSE)
	{
		ULONG ulCount;
		ulCount = lpCritSec->ulSpinLock;
		for (; ulCount > 0; ulCount--)
		{
			bRet = WaitForSingleObject(lpCritSec->hSemaphore, 0);
			if (bRet == TRUE) goto Entered;
		}
		bRet = WaitForSingleObject(lpCritSec->hSemaphore, INFINITE_WAIT_TIME);
	}
	Entered:
	return bRet;
}





_Success_(return != FALSE, _Releases_Lock_(hCritSec, _Lock_Kind_Semaphore_))
PODNET_API
BOOL
ExitCriticalSection(
	_In_					HANDLE 			hCritSec
){
	EXIT_IF_NULL(hCritSec, FALSE);
	LPCRIT_SEC lpCritSec;
	lpCritSec = (LPCRIT_SEC)GetHandleDerivative(hCritSec);
	EXIT_IF_NULL(hCritSec, FALSE);
	return ReleaseSemaphore(lpCritSec->hSemaphore);
}































