/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CEvent.h"


#define DEFAULT_EVENT_POLL_TIME 	0x05



typedef struct __EVENT
{
	INHERITS_FROM_HANDLE();
	ULONG				ulSet;
	LPCSTR				lpszName;
	LPFN_EVENT_CALLBACK_PROC	lpfnCallback;
	LPVOID				lpParam;
	ULONGLONG 			ullPollTime;
	BOOL				bBinaryEvent;
} EVENT, *LPEVENT;




_Success_(return != FALSE, ...)
INTERNAL_OPERATION
BOOL
__DestroyEvent(HDERIVATIVE hDerivative, ULONG ulFlags)
{
	UNREFERENCED_PARAMETER(ulFlags);
	EXIT_IF_NULL(hDerivative, FALSE);
	LPEVENT lpEvent;
	lpEvent = (LPEVENT)hDerivative;
	FreeMemory(lpEvent);
	return TRUE;
}




_Success_(return != FALSE, ...)
INTERNAL_OPERATION
BOOL
__WaitForEvent(
	_In_		HANDLE				hEvent,
	_In_		ULONGLONG			ullMilliSeconds
){
	/* TODO: Add error handling */
	LPEVENT lpEvent;
	BOOL bStatus;
	TIMESPEC tmWait;
	KEEP_IN_REGISTER ULONGLONG ullWaitTime;

	lpEvent = (LPEVENT)GetHandleDerivative(hEvent);
	EXIT_IF_UNLIKELY_NULL(lpEvent, FALSE);
	
	bStatus = FALSE;	
	ullWaitTime = lpEvent->ullPollTime;

	HighResolutionClock(&tmWait);
	if (INFINITE_WAIT_TIME == ullMilliSeconds)
	{	
		INFINITE_LOOP()
		{
			bStatus = (BOOL)InterlockedAcquire(&(lpEvent->ulSet));
			if (TRUE == bStatus)
			{ JUMP(Acquired); }
			Sleep(ullWaitTime);
		}
	}
	else
	{
		while (HighResolutionClockAndGetMilliSeconds(&tmWait) < ullMilliSeconds)
		{
			bStatus = (BOOL)InterlockedAcquire(&(lpEvent->ulSet));
			if (TRUE == bStatus) 
			{ JUMP(Acquired); }
			Sleep(ullWaitTime); /* This still allows for up to 200 fps if waiting on an event */
		}
	}

	Acquired:
	return bStatus;
}




/* This exists purely to satisfy a call from ReleaseSingleObject. */
_Call_Back_
INTERNAL_OPERATION
BOOL
__ReleaseEvent(
	_In_		HANDLE				hEvent
){
	EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hEvent), HANDLE_TYPE_EVENT, FALSE);
	return TRUE;
}




_Result_Null_On_Failure_
_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HANDLE
CreateEvent(
	_In_		BOOL			bInitialState
){
	HANDLE hHandle;
	LPEVENT lpEvent;
	ULONGLONG ullID;

	lpEvent = LocalAllocAndZero(sizeof(EVENT));
	/* cppcheck-suppress memleak */
	SET_ERROR_AND_EXIT_IF_NULL(lpEvent, ERROR_EVENT_COULD_NOT_BE_CREATED, NULLPTR);
	
	lpEvent->ulSet = (ULONG)bInitialState;
	lpEvent->ullPollTime = DEFAULT_EVENT_POLL_TIME;

	hHandle = CreateHandleWithSingleInheritor(	
			(HDERIVATIVE)lpEvent,
			&(lpEvent->hThis),
			HANDLE_TYPE_EVENT,
			__DestroyEvent,
			NULL,
			NULLPTR,
			NULL,
			NULLPTR,
			NULL,
			&ullID,
			NULL
	);	

	if (NULL_OBJECT == hHandle)
	{
		SetLastGlobalError(ERROR_EVENT_HANDLE_COULD_NOT_BE_CREATED);
		FreeMemory(lpEvent);
		return NULL_OBJECT;
	}
	
	SetHandleEventWaitFunction(hHandle, __WaitForEvent, NULL);
	SetHandleReleaseFunction(hHandle, __ReleaseEvent);

	/* cppcheck-suppress memleak */
	return hHandle;
}




_Result_Null_On_Failure_
_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HANDLE
CreateBinaryEvent(
	VOID
){
	HANDLE hEvent;
	LPEVENT lpEvent;
	hEvent = CreateEvent(FALSE);
	lpEvent = (LPEVENT)GetHandleDerivative(hEvent);
	EXIT_IF_NULL(lpEvent, NULLPTR);
	lpEvent->bBinaryEvent = TRUE;
	lpEvent->ullPollTime = DEFAULT_EVENT_POLL_TIME;
	return hEvent;
}




_Result_Null_On_Failure_
_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HANDLE
CreateEventEx(
	_In_		BOOL				bInitialState,
	_In_		LPFN_EVENT_CALLBACK_PROC 	lpfnCallback,
	_In_Opt_	LPVOID				lpParam
){
	HANDLE hHandle;
	LPEVENT lpEvent;
	ULONGLONG ullID;

	lpEvent = LocalAllocAndZero(sizeof(EVENT));
	/* cppcheck-suppress memleak */
	SET_ERROR_AND_EXIT_IF_NULL(lpEvent, ERROR_EVENT_COULD_NOT_BE_CREATED, NULLPTR);

	lpEvent->ulSet = (ULONG)bInitialState;
	lpEvent->lpfnCallback = lpfnCallback;
	lpEvent->lpParam = lpParam;
	lpEvent->ullPollTime = DEFAULT_EVENT_POLL_TIME;

	hHandle = CreateHandleWithSingleInheritor(	
			(HDERIVATIVE)lpEvent,
			&(lpEvent->hThis),
			HANDLE_TYPE_EVENT,
			__DestroyEvent,
			NULL,
			NULLPTR,
			NULL,
			NULLPTR,
			NULL,
			&ullID,
			NULL
	);	

	if (NULL_OBJECT == hHandle)
	{
		SetLastGlobalError(ERROR_EVENT_HANDLE_COULD_NOT_BE_CREATED);
		FreeMemory(lpEvent);
		return NULL_OBJECT;
	}

	/* cppcheck-suppress memleak */
	return hHandle;
}




_Success_(return != FALSE, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Event_)))
PODNET_API
BOOL
SignalEvent(
	_In_ 		HANDLE 			hEvent)
{
	EXIT_IF_NULL(hEvent, FALSE);
	LPEVENT lpEvent;
	BOOL bSet;

	lpEvent = (LPEVENT)GetHandleDerivative(hEvent);
	EXIT_IF_NULL(lpEvent, FALSE);
	
	bSet = InterlockedCompareExchangeHappened(&(lpEvent->ulSet), 0, 1);
	if (TRUE == bSet)
		if (NULLPTR != lpEvent->lpfnCallback)
			lpEvent->lpfnCallback(lpEvent->lpParam);
	return bSet;
}




_Success_(return != FALSE, _Calls_Call_Back_ && _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Event_)))
PODNET_API
BOOL
SignalEventEx(
	_In_		HANDLE			hEvent,
	_In_Opt_	LPVOID			lpParam
){

	EXIT_IF_NULL(hEvent, FALSE);
	LPEVENT lpEvent;
	BOOL bSet;

	lpEvent = (LPEVENT)GetHandleDerivative(hEvent);
	EXIT_IF_NULL(lpEvent, FALSE);
	
	bSet = InterlockedCompareExchangeHappened(&(lpEvent->ulSet), 0, 1);
	
	if (TRUE == bSet)
		if (NULLPTR != lpEvent->lpfnCallback)
			lpEvent->lpfnCallback(lpParam);
	return bSet;
}




_Success_(return != FALSE, _Releases_Lock_(_Has_Lock_Kind_(_Lock_Kind_Event_)))
PODNET_API
BOOL
ResetEvent(
	_In_		HANDLE			hEvent
){
	EXIT_IF_NULL(hEvent, FALSE);
	LPEVENT lpEvent;
	BOOL bReset;

	lpEvent = (LPEVENT)GetHandleDerivative(hEvent);
	EXIT_IF_NULL(lpEvent, FALSE);
	if (lpEvent->bBinaryEvent == TRUE)
		return FALSE;
	bReset = InterlockedCompareExchangeHappened(&(lpEvent->ulSet), 1, 0);
	return bReset;
}




_Success_(return == (FALSE || !FALSE), _Interlocked_Operation_)
PODNET_API
BOOL
GetEvent(
	_In_		HANDLE			hEvent
){
	EXIT_IF_NULL(hEvent, FALSE);
	LPEVENT lpEvent;

	lpEvent = (LPEVENT)GetHandleDerivative(hEvent);
	EXIT_IF_NULL(lpEvent, FALSE);
	
	return (BOOL)InterlockedAcquire(&(lpEvent->ulSet));
}




_Success_(return != FALSE, _Interlocked_Operation_)
PODNET_API
BOOL
SetEventPollTime(
	_In_ 		HANDLE RESTRICT 	hEvent,
	_In_ 		ULONGLONG 		ullPollTime
){
	EXIT_IF_UNLIKELY_NULL(hEvent, FALSE);
	EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hEvent), HANDLE_TYPE_EVENT, FALSE);

	LPEVENT lpEvent;
	lpEvent = (LPEVENT)GetHandleDerivative(hEvent);
	EXIT_IF_UNLIKELY_NULL(lpEvent, FALSE);

	return (BOOL)InterlockedAcquireExchangeHappened(&(lpEvent->ullPollTime), ullPollTime);
}