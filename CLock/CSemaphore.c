/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CSemaphore.h"


typedef struct __SEMAPHORE
{
	INHERITS_FROM_HANDLE();
	sem_t 		Semaphore;
} SEMAPHORE, *LPSEMAPHORE;



_Success_(return != FALSE, ...)
INTERNAL_OPERATION
BOOL
__DestroySemaphore(
	_In_						HDERIVATIVE			hDerivative,
	_In_Opt_					ULONG				ulFlags
){
	UNREFERENCED_PARAMETER(ulFlags);
	LPSEMAPHORE lpSem;
	lpSem = (LPSEMAPHORE)hDerivative;
	sem_destroy(&(lpSem->Semaphore));
	FreeMemory(lpSem);
	return TRUE;
}



_Success_(return != FALSE, ...)
INTERNAL_OPERATION
BOOL
__WaitForSemaphore(
	_In_						HANDLE				hSemaphore,
	_In_						ULONGLONG			ullMilliSeconds
){
	LPSEMAPHORE lpSem;
	LONG lRet;
	lpSem = (LPSEMAPHORE)GetHandleDerivative(hSemaphore);

	if (0 == ullMilliSeconds)
		lRet = sem_trywait(&(lpSem->Semaphore));
	else if (INFINITE_WAIT_TIME == ullMilliSeconds)
		lRet = sem_wait(&(lpSem->Semaphore));
	else
	{
		TIMESPEC tmSpec;
		CreateTimeSpec(&tmSpec, ullMilliSeconds);
		lRet = sem_timedwait(&(lpSem->Semaphore), &tmSpec);
	}
	return (lRet == 0) ? TRUE : FALSE;
}



_Result_Null_On_Failure_
_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HANDLE
CreateSemaphore(
	_In_Range_(1, MAX_ULONG)			ULONG				ulInitialCount
){
	HANDLE hSemaphore;
	LPSEMAPHORE lpSem;
	ULONGLONG ullID;
	lpSem = LocalAlloc(sizeof(SEMAPHORE));
	sem_init(&(lpSem->Semaphore), 0, ulInitialCount);

	hSemaphore = CreateHandleWithSingleInheritor(
			lpSem,
			&(lpSem->hThis),
			HANDLE_TYPE_SEMAPHORE,
			__DestroySemaphore,
			NULL,
			NULLPTR,
			NULL,
			NULLPTR,
			NULL,
			&ullID,
			NULL
	);	

	SetHandleEventWaitFunction(hSemaphore, __WaitForSemaphore, NULL);
	SetHandleReleaseFunction(hSemaphore, ReleaseSemaphore);

	return hSemaphore;
}




_Success_(return != FALSE, _Releases_Lock_(hSemaphore, _Has_Lock_Kind_(_Lock_Kind_Semaphore_)))
PODNET_API
BOOL
ReleaseSemaphore(
	_In_						HANDLE				hSemaphore
){
	LPSEMAPHORE lpSem;
	lpSem = (LPSEMAPHORE)GetHandleDerivative(hSemaphore);
	return (sem_post(&(lpSem->Semaphore)) == 0) ? TRUE : FALSE;
}




_Success_(return != MAX_ULONG, _Non_Locking_)
PODNET_API
LONG
SemaphoreCount(
	_In_						HANDLE				hSemaphore
){
	LPSEMAPHORE lpSem;
	LONG lSemValue;
	lpSem = (LPSEMAPHORE)GetHandleDerivative(hSemaphore);
	sem_getvalue(&(lpSem->Semaphore), &lSemValue);
	return lSemValue;
}
