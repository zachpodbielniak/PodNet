/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CLOCK_H
#define CLOCK_H


#include "CCritSec.h"
#include "CEvent.h"
#include "CMutex.h"
#include "CSemaphore.h"
#include "CSpinLock.h"


#ifndef __LOCK_TYPE_DEF
#define __LOCK_TYPE_DEF

typedef enum __LOCK_TYPE 
{
	LOCK_TYPE_INVALID 		= 0,
	LOCK_TYPE_CRITICAL_SECTION 	= 1,
	LOCK_TYPE_EVENT 		= 2,
	LOCK_TYPE_MUTEX 		= 3,
	LOCK_TYPE_SEMAPHORE 		= 4,
	LOCK_TYPE_SPIN_LOCK 		= 5,
	LOCK_TYPE_SIZE
} LOCK_TYPE;

#endif







#endif