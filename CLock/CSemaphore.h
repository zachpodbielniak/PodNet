/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CSEMAPHORE_H
#define CSEMPAHORE_H


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../CHandle/CHandle.h"
#include "../CError/CError.h"
#include "../CClock/CClock.h"




_Result_Null_On_Failure_
_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HANDLE
CreateSemaphore(
	_In_Range_(1, MAX_ULONG)			ULONG				ulInitialCount
);




_Success_(return != FALSE, _Releases_Lock_(hSemaphore, _Has_Lock_Kind_(_Lock_Kind_Semaphore_)))
PODNET_API
BOOL
ReleaseSemaphore(
	_In_						HANDLE				hSemaphore
);




_Success_(return != MAX_ULONG, _Non_Locking_)
PODNET_API
LONG
SemaphoreCount(
	_In_						HANDLE				hSemaphore
);



#endif