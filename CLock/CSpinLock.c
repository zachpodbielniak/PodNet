/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CSpinLock.h"


typedef struct _SPINLOCK
{
        INHERITS_FROM_HANDLE();
        ULONG   ulAcquired;
} SPINLOCK, *LPSPINLOCK;



_Call_Back_
INTERNAL_OPERATION
BOOL
__WaitForSpinLock(
	_In_		HANDLE			hSpinLock,
        _In_Opt_	ULONGLONG		ullMilliseconds
){
        SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(hSpinLock, ERROR_SPINLOCK_HANDLE_SUPPLIED_WAS_NULL, FALSE);
        SET_ERROR_AND_EXIT_IF_UNLLIKELY_NOT_VALUE(GetHandleDerivativeType(hSpinLock), HANDLE_TYPE_SPINLOCK, ERROR_SPINLOCK_HANDLE_SUPPLIED_IS_NOT_SPINLOCK_HANDLE_TYPE, FALSE);
        LPSPINLOCK lpslLock;
        BOOL bRet;
	TIMESPEC tsClock; 
	lpslLock = (LPSPINLOCK)GetHandleDerivative(hSpinLock);
        SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(lpslLock, ERROR_SPINLOCK_DATA_IS_NON_EXISTENT, FALSE);

	HighResolutionClock(&tsClock);

        bRet = FALSE;
        if (INFINITE_WAIT_TIME == ullMilliseconds)
        {
                INFINITE_LOOP()
                {
                        bRet = InterlockedCompareExchangeHappened(&(lpslLock->ulAcquired), 0, 1);
                        if (bRet == TRUE) goto Acquired;
                }
        }
        else if (ullMilliseconds == 0)
        { bRet = InterlockedCompareExchangeHappened(&(lpslLock->ulAcquired), 0, 1); }
        else
        {
                while (HighResolutionClockAndGetMilliSeconds(&tsClock) <= ullMilliseconds)
                {
                        bRet = InterlockedCompareExchangeHappened(&(lpslLock->ulAcquired), 0, 1);
                        if (bRet == TRUE) goto Acquired;
                }
        }
        Acquired:
        return bRet;
}




_Call_Back_
INTERNAL_OPERATION
BOOL
__DestroySpinLock(
        _In_            HDERIVATIVE             hDerivative,
        _In_Opt_        ULONG                   ulFlags
){
        UNREFERENCED_PARAMETER(ulFlags);
        SET_ERROR_AND_EXIT_IF_NULL(hDerivative, ERROR_SPINLOCK_DATA_IS_NON_EXISTENT, FALSE);
        LPSPINLOCK lpslLock;
        lpslLock = (LPSPINLOCK)hDerivative;

        FreeMemory(lpslLock);
        return TRUE;
}



_Result_Null_On_Failure_
_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HANDLE
CreateSpinLock(
        VOID
){
        HANDLE hSpinLock;
        LPSPINLOCK lpslLock;
        ULONGLONG ullID;
        lpslLock = LocalAllocAndZero(sizeof(SPINLOCK));
        SET_ERROR_AND_EXIT_IF_NULL(lpslLock, ERROR_SPINLOCK_COULD_NOT_ALLOC_DATA, NULLPTR);

        hSpinLock = CreateHandleWithSingleInheritor(
                        lpslLock,
                        &(lpslLock->hThis),
                        HANDLE_TYPE_SPINLOCK,
                        __DestroySpinLock,
                        NULL,
                        NULLPTR,
                        NULL,
                        NULLPTR,
                        NULL,
                        &ullID,
                        NULL
        );
        SET_ERROR_AND_EXIT_IF_NULL(hSpinLock, ERROR_SPINLOCK_COULD_NOT_CREATE_HANDLE, NULLPTR);

        SetHandleEventWaitFunction(hSpinLock, __WaitForSpinLock, NULL);
        SetHandleReleaseFunction(hSpinLock, ReleaseSpinLock);

        return hSpinLock;
}


_Success_(return != NULL, _Interlocked_)
PODNET_API
BOOL
ReleaseSpinLock(
        _In_                    HANDLE                  hSpinLock
){
        SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(hSpinLock, ERROR_SPINLOCK_HANDLE_SUPPLIED_WAS_NULL, FALSE);
        SET_ERROR_AND_EXIT_IF_UNLLIKELY_NOT_VALUE(GetHandleDerivativeType(hSpinLock), HANDLE_TYPE_SPINLOCK, ERROR_SPINLOCK_HANDLE_SUPPLIED_IS_NOT_SPINLOCK_HANDLE_TYPE, FALSE);
        LPSPINLOCK lpslLock;
        lpslLock = (LPSPINLOCK)GetHandleDerivative(hSpinLock);
        SET_ERROR_AND_EXIT_IF_UNLIKELY_NULL(lpslLock, ERROR_SPINLOCK_DATA_IS_NON_EXISTENT, FALSE);

        return InterlockedCompareExchangeHappened(&(lpslLock->ulAcquired), 1, 0);
}
