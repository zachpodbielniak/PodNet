/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../CHandle/CHandle.h"
#include "../CThread/CThread.h"


typedef _Call_Back_ LPVOID (* LPFN_EVENT_CALLBACK_PROC)(
	_In_Opt_		LPVOID		lpParam
);




_Result_Null_On_Failure_
_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HANDLE
CreateEvent(
	_In_		BOOL			bInitialState
);




_Result_Null_On_Failure_
_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HANDLE
CreateBinaryEvent(
	VOID
);




_Result_Null_On_Failure_
_Success_(return != NULLPTR, _Non_Locking_)
PODNET_API
HANDLE
CreateEventEx(
	_In_		BOOL				bInitialState,
	_In_		LPFN_EVENT_CALLBACK_PROC 	lpfnCallback,
	_In_Opt_	LPVOID				lpParam
);




_Success_(return != FALSE, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Event_)))
PODNET_API
BOOL
SignalEvent(
	_In_		HANDLE			hEvent
);




_Success_(return != FALSE, _Calls_Call_Back_ && _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Event_)))
PODNET_API
BOOL
SignalEventEx(
	_In_		HANDLE			hEvent,
	_In_Opt_	LPVOID			lpParam
);




_Success_(return != FALSE, _Releases_Lock_(_Has_Lock_Kind_(_Lock_Kind_Event_)))
PODNET_API
BOOL
ResetEvent(
	_In_		HANDLE			hEvent
);




_Success_(return == (FALSE || !FALSE), _Interlocked_Operation_)
PODNET_API
BOOL
GetEvent(
	_In_		HANDLE			hEvent
);




_Success_(return != FALSE, _Interlocked_Operation_)
PODNET_API
BOOL
SetEventPollTime(
	_In_ 		HANDLE RESTRICT 	hEvent,
	_In_ 		ULONGLONG 		ullPollTime
);