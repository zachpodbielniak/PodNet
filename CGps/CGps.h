/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef CGPS_H
#define CGPS_H


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"
#include "../CHandle/CHandle.h"
#include "../CMath/CCoordinates.h"



_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNET_API
HANDLE
CreateGpsInterface(
	_In_Z_ 		LPCSTR RESTRICT 	lpcszServer,
	_In_ 		USHORT 			usPort
);




_Success_(return != FALSE, _Acquires_Shared_Lock_(_Lock_Kind_Critical_Section_))
PODNET_API
BOOL 
GpsGetCoordinates(
	_In_ 		HANDLE 			hGps,
	_In_ 		LPCOORDINATE 		lpcorCoordinates
);




_Success_(return != NAN64, _Acquires_Shared_Lock_(_Lock_Kind_Critical_Section_))
PODNET_API
DOUBLE 
GpsGetLatitude(
	_In_ 		HANDLE 			hGps
);




_Success_(return != NAN64, _Acquires_Shared_Lock_(_Lock_Kind_Critical_Section_))
PODNET_API
DOUBLE 
GpsGetLongitude(
	_In_ 		HANDLE 			hGps
);




_Success_(return != NAN64, _Acquires_Shared_Lock_(_Lock_Kind_Critical_Section_))
PODNET_API
DOUBLE 
GpsGetAltitude(
	_In_ 		HANDLE 			hGps
);

#endif