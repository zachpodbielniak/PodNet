/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CGps.h"


typedef struct __GPS 
{
	INHERITS_FROM_HANDLE();
	HANDLE 			hCritSec;
	struct gps_data_t 	gpsdData;
	LPSTR 			lpszServer;
	USHORT 			usPort;
} GPS, *LPGPS;



_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__WaitForGps(
	_In_		HANDLE 			hGps,
	_In_ 		ULONGLONG 		ullMilliSeconds
){
	EXIT_IF_UNLIKELY_NULL(hGps, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hGps), HANDLE_TYPE_GPS, FALSE);

	LPGPS lpGps;
	ULONGLONG ullMicroSeconds;
	lpGps = (LPGPS)GetHandleDerivative(hGps);
	EXIT_IF_UNLIKELY_NULL(lpGps, FALSE);

	ullMicroSeconds = ullMilliSeconds * 1000; 
	if (ullMicroSeconds <= ullMilliSeconds)
	{ ullMicroSeconds = ULONG_MAX; }
	
	return gps_waiting(&(lpGps->gpsdData), (ULONG)ullMicroSeconds);
}




_Success_(return != FALSE, _Acquires_Shared_Lock_(_Lock_Kind_Critical_Section_))
INTERNAL_OPERATION
__DestroyGps(
	_In_ 		HDERIVATIVE 		hDerivative,
	_In_Opt_ 	ULONG 			ulFlags
){
	UNREFERENCED_PARAMETER(ulFlags);
	EXIT_IF_UNLIKELY_NULL(hDerivative, FALSE);

	LPGPS lpGps;
	lpGps = (LPGPS)hDerivative;

	WaitForSingleObject(lpGps->hCritSec, INFINITE_WAIT_TIME);

	gps_close(&(lpGps->gpsdData));

	DestroyObject(lpGps->hCritSec);
	FreeMemory(lpGps->lpszServer);
	FreeMemory(lpGps);

	return TRUE;
}




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNET_API
HANDLE
CreateGpsInterface(
	_In_Z_ 		LPCSTR RESTRICT 	lpcszServer,
	_In_ 		USHORT 			usPort
){
	EXIT_IF_UNLIKELY_NULL(lpcszServer, NULL_OBJECT);

	HANDLE hGps;
	LPGPS lpGps;
	LONG lRet;
	CSTRING csPort[8];

	ZeroMemory(csPort, sizeof(csPort));

	if (0 == usPort)
	{ usPort = 2947; }

	lpGps = GlobalAllocAndZero(sizeof(GPS));
	/* cppcheck-suppress memleak */
	EXIT_IF_UNLIKELY_NULL(lpGps, NULL_OBJECT);

	lpGps->usPort = usPort;
	lpGps->lpszServer = GlobalAllocAndZero((sizeof(CHAR) * StringLength(lpcszServer)) + 1);
	if (NULLPTR == lpGps->lpszServer)
	{
		FreeMemory(lpGps);
		return NULL_OBJECT;
	}

	StringCopySafe(lpGps->lpszServer, lpcszServer, (sizeof(CHAR) * StringLength(lpcszServer)) + 1);

	lpGps->hCritSec = CreateCriticalSectionAndSpecifySpinCount(0xC00);

	StringPrintFormatSafe(
		csPort,
		sizeof(csPort),
		"%hu",
		usPort
	);

	hGps = CreateHandleWithSingleInheritor(
		lpGps,
		&(lpGps->hThis),
		HANDLE_TYPE_GPS,
		__DestroyGps,
		NULL,
		NULLPTR,
		NULL,
		NULLPTR,
		NULL,
		&(lpGps->ullId),
		NULL
	);

	SetHandleEventWaitFunction(hGps, __WaitForGps, NULL);

	lRet = gps_open(lpcszServer, csPort, &(lpGps->gpsdData));
	EXIT_IF_UNLIKELY_NOT_ZERO(lRet, NULL_OBJECT);

	gps_stream(&(lpGps->gpsdData), WATCH_ENABLE | WATCH_JSON, NULLPTR);

	return hGps;
}




_Success_(return != FALSE, _Acquires_Shared_Lock_(_Lock_Kind_Critical_Section_))
PODNET_API
BOOL 
GpsGetCoordinates(
	_In_ 		HANDLE 			hGps,
	_In_ 		LPCOORDINATE 		lpcorCoordinates
){
	EXIT_IF_UNLIKELY_NULL(hGps, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpcorCoordinates, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hGps), HANDLE_TYPE_GPS, FALSE);

	LPGPS lpGps;
	lpGps = (LPGPS)GetHandleDerivative(hGps);
	EXIT_IF_UNLIKELY_NULL(lpGps, FALSE);

	/* Deadlock Prevention */
	EXIT_IF_UNLIKELY_VALUE(WaitForSingleObject(lpGps->hCritSec, 100), FALSE, FALSE);

	lpcorCoordinates->bIs3DCoordinate = TRUE;
	lpcorCoordinates->dX = lpGps->gpsdData.fix.longitude;
	lpcorCoordinates->dY = lpGps->gpsdData.fix.latitude;
	lpcorCoordinates->dZ = lpGps->gpsdData.fix.altitude;

	ReleaseSingleObject(lpGps->hCritSec);

	return TRUE;
}




_Success_(return != NAN64, _Acquires_Shared_Lock_(_Lock_Kind_Critical_Section_))
PODNET_API
DOUBLE 
GpsGetLatitude(
	_In_ 		HANDLE 			hGps
){
	EXIT_IF_UNLIKELY_NULL(hGps, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hGps), HANDLE_TYPE_GPS, FALSE);

	LPGPS lpGps;
	DOUBLE dbValue;
	lpGps = (LPGPS)GetHandleDerivative(hGps);
	EXIT_IF_UNLIKELY_NULL(lpGps, FALSE);

	/* Deadlock Prevention */
	EXIT_IF_UNLIKELY_VALUE(WaitForSingleObject(lpGps->hCritSec, 100), FALSE, FALSE);

	dbValue = lpGps->gpsdData.fix.latitude;

	ReleaseSingleObject(lpGps->hCritSec);

	return dbValue;
}




_Success_(return != NAN64, _Acquires_Shared_Lock_(_Lock_Kind_Critical_Section_))
PODNET_API
DOUBLE 
GpsGetLongitude(
	_In_ 		HANDLE 			hGps
){
	EXIT_IF_UNLIKELY_NULL(hGps, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hGps), HANDLE_TYPE_GPS, FALSE);

	LPGPS lpGps;
	DOUBLE dbValue;
	lpGps = (LPGPS)GetHandleDerivative(hGps);
	EXIT_IF_UNLIKELY_NULL(lpGps, FALSE);

	/* Deadlock Prevention */
	EXIT_IF_UNLIKELY_VALUE(WaitForSingleObject(lpGps->hCritSec, 100), FALSE, FALSE);

	dbValue = lpGps->gpsdData.fix.longitude;

	ReleaseSingleObject(lpGps->hCritSec);

	return dbValue;
}




_Success_(return != NAN64, _Acquires_Shared_Lock_(_Lock_Kind_Critical_Section_))
PODNET_API
DOUBLE 
GpsGetAltitude(
	_In_ 		HANDLE 			hGps
){
	EXIT_IF_UNLIKELY_NULL(hGps, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hGps), HANDLE_TYPE_GPS, FALSE);

	LPGPS lpGps;
	DOUBLE dbValue;
	lpGps = (LPGPS)GetHandleDerivative(hGps);
	EXIT_IF_UNLIKELY_NULL(lpGps, FALSE);

	/* Deadlock Prevention */
	EXIT_IF_UNLIKELY_VALUE(WaitForSingleObject(lpGps->hCritSec, 100), FALSE, FALSE);

	dbValue = lpGps->gpsdData.fix.altitude;

	ReleaseSingleObject(lpGps->hCritSec);

	return dbValue;
}
