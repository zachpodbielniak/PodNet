# PodNet Library

This library is being actively developed, and maintained by Zach Podbielniak. This sets out to be a general-purpose library. Most of it is written in C. 

The Linux library is in a state reminiscent of a beta state...for the most part. The library is programmed in a very strict coding style, which can be viewed in the documentation, located under Linux/Documentation/PodNetDocumentation.pdf . 

This library is used in all of my other projects, and serves as a nice base layer to
any project in C, or even Python. 

## License

![AGPLv3](https://www.gnu.org/graphics/agplv3-with-text-162x68.png)

```
 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```

PodNet is licensed under the AGPLv3 License. Don't like it? Go else where.

## Donate
Like PodMQ? You use it yourself? Why not donate to help make it better! I really appreciate any and all donations.

+ [PayPal](https://paypal.me/ZPodbielniak)
+ **Bitcoin** - 3C6Fc9WPH54GoVC91Sq4JTWa5C9ijKMA23
+ **Litecoin** - MVjvGjfmp3gkLniBSnreFb3SNEXq1FRxbW
+ **Ethereum** - 0xE58bAEd820308038092F732151c162f530361B59

## Docker
There is an official docker image, which is built many times per week: **zachpodbielniak/podnet:latest**

## Software Using PodNet

+ [Podomation](https://gitlab.com/zachpodbielniak/Podomation)
+ [PodMQ](https://gitlab.com/zachpodbielniak/PodMQ)
+ [CRock](https://gitlab.com/zachpodbielniak/CRock)
+ [Castle](https://gitlab.com/zachpodbielniak/Castle)
+ [CInventory](https://gitlab.com/zachpodbielniak/CInventory)
+ [CookieCutter](https://gitlab.com/zachpodbielniak/CookieCutter)
+ [PodNetEngine](https://gitlab.com/zachpodbielniak/PodNetEngine)
+ [GetPublicIpAddress()](https://gitlab.com/zachpodbielniak/GetPublicIpAddress) 


## Features of the PodNet Library

+ Near full port of C++ STL Library to C, and other nice features (Generic Structures):
	+ Vector
	+ HashTable
	+ BinarySearchTree
	+ Linked List (Single AND Double)
	+ Queue
	+ Stack
+ HANDLE (Object) system, similar to Win32 API.
	+ Single AND Multiple Object Inheritance
	+ Encapsulation
	+ Virtual Functions
	+ WaitForSingleObject, etc.
	+ Casting to object types.
+ Custom UnitTest Framework
	+ Dead simple. Single file based, using preprocessor magic.
+ Multistage Parser
	+ Lexer
	+ Token Table
	+ Parser
	+ INI File Parser
+ Multiple Lock Types:
	+ MUTEX
	+ SEMAPHORE
	+ CRITICAL SECTION
	+ SPIN LOCK
	+ EVENT
+ Promise and Future
+ Easy Threading Library
+ Lua Support
	+ This is currently being worked on
+ Nearly 100% exposed to Python, with easy Python Package Installer through Makefile
+ Easy Networking
	+ Multicast Support
	+ Dual Stack (IPv4 / IPv6)
	+ TCP and UDP. Support for UNIX sockets coming in the future.
	+ Geolocation with use of GeoIP.
+ Logging Library
+ Nice File I/O
+ Once Callable Functions
+ Advanced Timing 
	+ Second, MilliSecond, MicroSecond, and NanoSecond resolutions.
+ Atom Table
	+ Global Atom Table
	+ Infinite Amount of User Defined Atom Tables
+ Device Wrappers
	+ GPIO for Raspberry Pi and Similar Devices
	+ SPI
	+ I2C
	+ Serial Communication
+ Error Reporting
+ Fast Approximate Math Library
	+ Self Derived Fast Trig Functions
+ Shapes
	+ Represent states of shapes, in a OO way.
+ Mass Unit Converter
+ CUDA Support
	+ This is going to be worked on more shortly

## Install

+ Dependencies
	+ Linux 5.x is recommended
	+ Lua 5.3
	+ WiringPi
	+ GCC
	+ NASM (For x86_64 hand-tuned optimizations)
	+ GeoIP Library

```
make -j$(nproc)
sudo make install
sudo make install_headers
sudo make install_python
```

## Docker
There is an official Docker Image built daily. zachpodbielniak/podnet:latest