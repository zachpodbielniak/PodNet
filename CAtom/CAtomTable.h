/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/



#ifndef CATOMTABLE_H
#define CATOMTABLE_H

#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../Macros.h"


typedef USHORT                  ATOM, *LPATOM, **DLPATOM, ***TLPATOM;

#include "../CMemory/CHeap.h"

#define INVALID_ATOM            0xFFFF

/*
        Other Library includes must come after this comment,
        otherwise they don't know what an ATOM is.
*/

#include "../CHandle/CHandle.h"


_Result_Null_On_Failure_
PODNET_API
HANDLE
CreateAtomTable(
        _In_                                                    USHORT                  usAtomBufferSize,
        _In_Range_(1, MAX_USHORT)                               USHORT                  usMaxAtoms
);




_Success_(return != INVALID_ATOM, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Critical_Section_)))
PODNET_API
ATOM
PushAtom(
        _In_                                                    HANDLE                  hAtomTable,
        _In_                                                    LPCSTR                  lpszString
);




_Success_(return != FALSE, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Critical_Section_)))
PODNET_API
BOOL
GetAtomValue(
        _In_                                                    HANDLE                  hAtomTable,
        _In_                                                    ATOM                    atAtom,
        _Out_Writes_Bytes_(usAtomBufferSize * sizeof(CHAR))     LPSTR                   lpszOut
);




_Success_(return != INVALID_ATOM, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Critical_Section_)))
PODNET_API
ATOM
PushGlobalAtom(
        _In_                                                    LPCSTR                  lpszString
);




_Success_(return != FALSE, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Critical_Section_)))
PODNET_API
BOOL
GetGlobalAtomValue(
        _In_                                                    ATOM                    atAtom,
        _Out_Writes_Bytes_(256 * sizeof(CHAR))                  LPSTR                   lpszOut
);




#endif
