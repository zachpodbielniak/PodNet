/*


 ____           _ _   _      _   
|  _ \ ___   __| | \ | | ___| |_ 
| |_) / _ \ / _` |  \| |/ _ \ __|
|  __/ (_) | (_| | |\  |  __/ |_ 
|_|   \___/ \__,_|_| \_|\___|\__|


General Purpose C Library.
Copyright (C) 2017-2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "CAtomTable.h"

GLOBAL_VARIABLE DONT_OPTIMIZE_OUT HANDLE hGlobalAtom = NULLPTR;



typedef struct __ATOM_TABLE
{
        INHERITS_FROM_HANDLE();
        DLPSTR          dlpszAtoms;
        USHORT          usNumberOfAtoms;
        USHORT          usSizeOfAtom;
        USHORT          usMaxAtoms;
        HANDLE          hCritSec;
} ATOM_TABLE, *LPATOM_TABLE;



#define DEFAULT_ATOM_BUFFER_SIZE                128;




_Call_Back_
INTERNAL_OPERATION
BOOL
__DestroyAtomTable(
        _In_                                                    HDERIVATIVE             hDerivative,
        _In_Opt_                                                ULONG                   ulFlags
){
        UNREFERENCED_PARAMETER(ulFlags);
        SET_ERROR_AND_EXIT_IF_NULL(hDerivative, ERROR_ATOM_TABLE_NON_EXISTENT, FALSE);
        LPATOM_TABLE lpatTable;
        USHORT usIndex;
        lpatTable = (LPATOM_TABLE)hDerivative;
        EnterCriticalSection(lpatTable->hCritSec);
        for (usIndex = 0; usIndex < lpatTable->usNumberOfAtoms; usIndex++)
                FreeMemory(lpatTable->dlpszAtoms[usIndex]);
        /* FreeMemory(lpatTable->dlpszAtoms); */
	ReturnPage(lpatTable->dlpszAtoms, MAX_USHORT * sizeof(LPSTR));
        ExitCriticalSection(lpatTable->hCritSec);
        DestroyHandle(lpatTable->hCritSec);
        FreeMemory(lpatTable);
        return TRUE;
}





_Result_Null_On_Failure_
PODNET_API
HANDLE
CreateAtomTable(
        _In_                                                    USHORT                  usAtomBufferSize,
        _In_Range_(1, MAX_USHORT)                               USHORT                  usMaxAtoms
){
        HANDLE hAtomTable;
        LPATOM_TABLE lpatmTable;
        ULONGLONG ullID;
        if (usAtomBufferSize == 0) usAtomBufferSize = DEFAULT_ATOM_BUFFER_SIZE;
        lpatmTable = LocalAllocAndZero(sizeof(ATOM_TABLE));
        SET_ERROR_AND_EXIT_IF_NULL(lpatmTable, ERROR_ATOM_TABLE_COULD_NOT_BE_ALLOC, NULLPTR);


        hAtomTable = CreateHandleWithSingleInheritor(
                        lpatmTable,
                        &(lpatmTable->hThis),
                        HANDLE_TYPE_ATOM_TABLE,
                        __DestroyAtomTable,
                        NULL,
                        NULLPTR,
                        NULL,
                        NULLPTR,
                        NULL,
                        &ullID,
                        NULL
        );
 
       	SET_ERROR_AND_EXIT_IF_NULL(hAtomTable, ERROR_ATOM_TABLE_COULD_NOT_CREATE_HANDLE, NULLPTR);
        
        lpatmTable->usSizeOfAtom = (USHORT)usAtomBufferSize;
        lpatmTable->usMaxAtoms = usMaxAtoms;
        /* lpatmTable->dlpszAtoms = LocalAllocAndZero((usMaxAtoms) * sizeof(LPSTR)); */
     	lpatmTable->dlpszAtoms = RequestPage((MAX_USHORT) * sizeof(LPSTR));
	SET_ERROR_AND_EXIT_IF_NULL(lpatmTable->dlpszAtoms, ERROR_ATOM_TABLE_COULD_ALLOC_ATOM_BUFFER, NULLPTR);
        lpatmTable->hCritSec = CreateCriticalSection();
        SET_ERROR_AND_EXIT_IF_NULL(lpatmTable->hCritSec, ERROR_ATOM_TABLE_COULD_NOT_CREATE_HANDLE, NULLPTR);

	return hAtomTable;
}




_Success_(return != 0, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Critical_Section_)))
PODNET_API
ATOM
PushAtom(
        _In_                                                    HANDLE                  hAtomTable,
        _In_                                                    LPCSTR                  lpszString
){
        SET_ERROR_AND_EXIT_IF_NULL(hAtomTable, ERROR_ATOM_TABLE_HANDLE_SUPPLIED_WAS_NULL, INVALID_ATOM);
        SET_ERROR_AND_EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hAtomTable), HANDLE_TYPE_ATOM_TABLE, ERROR_ATOM_TABLE_HANDLE_SUPPLIED_WAS_NOT_ATOM_TABLE_HANDLE, INVALID_ATOM);
        SET_ERROR_AND_EXIT_IF_NULL(lpszString, ERROR_ATOM_TABLE_NO_STRING_PROVIDED, INVALID_ATOM);
        LPATOM_TABLE lpatTable;
        ATOM atRet;
        lpatTable = (LPATOM_TABLE)GetHandleDerivative(hAtomTable);
        /*SET_ERROR_AND_EXIT_IF_NULL(lpatTable, ERROR_HANDLE_DERIVATIVE_WAS_NULL, INVALID_ATOM);*/
        EnterCriticalSection(lpatTable->hCritSec);

        if (lpatTable->usNumberOfAtoms >= lpatTable->usMaxAtoms)
                SET_ERROR_AND_EXIT(ERROR_ATOM_TABLE_FULL, 0);

        lpatTable->dlpszAtoms[lpatTable->usNumberOfAtoms] = LocalAllocAndZero(sizeof(CHAR) * lpatTable->usSizeOfAtom);

	if (NULLPTR == lpatTable->dlpszAtoms[lpatTable->usNumberOfAtoms])
	{
		ReleaseSingleObject(lpatTable->hCritSec);
		return 0;
	}
        StringCopy(lpatTable->dlpszAtoms[lpatTable->usNumberOfAtoms], lpszString);
        lpatTable->usNumberOfAtoms++;
        atRet = lpatTable->usNumberOfAtoms;
        ExitCriticalSection(lpatTable->hCritSec);
        return atRet;
}




_Success_(return != FALSE, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Critical_Section_)))
PODNET_API
BOOL
GetAtomValue(
        _In_                                                    HANDLE                  hAtomTable,
        _In_                                                    ATOM                    atAtom,
        _Out_Writes_Bytes_(usAtomBufferSize * sizeof(CHAR))     LPSTR                   lpszOut
){
        SET_ERROR_AND_EXIT_IF_NULL(hAtomTable, ERROR_ATOM_TABLE_HANDLE_SUPPLIED_WAS_NULL, FALSE);
        SET_ERROR_AND_EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hAtomTable), HANDLE_TYPE_ATOM_TABLE, ERROR_ATOM_TABLE_HANDLE_SUPPLIED_WAS_NOT_ATOM_TABLE_HANDLE, FALSE);
        SET_ERROR_AND_EXIT_IF_NULL(lpszOut, ERROR_ATOM_TABLE_NO_STRING_PROVIDED, FALSE);
        LPATOM_TABLE lpatTable;
        lpatTable = (LPATOM_TABLE)GetHandleDerivative(hAtomTable);
        SET_ERROR_AND_EXIT_IF_NULL(lpatTable, ERROR_HANDLE_DERIVATIVE_WAS_NULL, FALSE);

        if (atAtom <= lpatTable->usNumberOfAtoms)
                StringCopy(lpszOut, lpatTable->dlpszAtoms[atAtom - 1]);
        else SET_ERROR_AND_EXIT(ERROR_ATOM_TABLE_ATOM_BUFFER_WAS_NULL, FALSE);

        return TRUE;
}




_Success_(return != MAX_USHORT, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Critical_Section_)))
PODNET_API
ATOM
PushGlobalAtom(
        _In_                                                    LPCSTR                  lpszString
){ 
        if (hGlobalAtom == NULLPTR)
                hGlobalAtom = CreateAtomTable(256, 2048);
        return PushAtom(hGlobalAtom, lpszString); 
 }




_Success_(return != FALSE, _Acquires_Lock_(_Has_Lock_Kind_(_Lock_Kind_Critical_Section_)))
PODNET_API
BOOL
GetGlobalAtomValue(
        _In_                                                    ATOM                    atAtom,
        _Out_Writes_Bytes_(256 * sizeof(CHAR))                  LPSTR                   lpszOut
){ return GetAtomValue(hGlobalAtom, atAtom, lpszOut); }
